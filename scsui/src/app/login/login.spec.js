/**
 * Tests sit right alongside the file they are testing, which is more intuitive
 * and portable than separating `src` and `test` directories. Additionally, the
 * build process will exclude all `.spec.js` files from the build
 * automatically.
 */
describe( 'Login section', function() {
	beforeEach( module('rpsmarf.config') );
	beforeEach( module( 'rpsmarf.login') );
	beforeEach( module( 'ui.router' ) );
	beforeEach(module('taskcreate/taskcreate.tpl.html'));
	beforeEach(module('login/login.tpl.html'));
	beforeEach(module('rpsmarf.user.service'));

	describe('LoginCtrl on login/logout state', function(){

		var scope;


		it( 'should not show logout notification on login', inject( function($rootScope, $controller) {
			
			scope = $rootScope.$new();
			$controller('LoginCtrl', {$scope: scope, logout: false});
			expect( scope.logout ).toBe(false);
		}));

		it( 'should show logout notification on logout', inject( function($rootScope, $controller) {
			scope = $rootScope.$new();
			$controller('LoginCtrl', {$scope: scope, logout: true});
			expect( scope.logout ).toBe(true);
		}));

	});

	describe('LoginCtrl form functions', function(){
		var scope, controller, $httpBackend, SmUser;
		var $state = {go:function(name){}};
		beforeEach(inject(function($rootScope, $controller, _SmUser_){
			scope = $rootScope.$new();
			SmUser = _SmUser_;
			controller = $controller('LoginCtrl',{$scope: scope, logout: false, $state:$state, SmUser: SmUser});
		}));

		it('should validate email and password', function(){
			scope.email = '';
			scope.loginProcess();
			expect(scope.emailError).toEqual(['has-error']);
			expect(scope.errorMessage).toBeTruthy();

			scope.resetForm();

			scope.email = 'myemail';
			scope.loginProcess();
			expect(scope.emailError).toEqual(['has-error']);
			expect(scope.errorMessage).toBeTruthy();

			scope.resetForm();

			scope.email = 'rpmsarF1@rpsmarf.ca';
			scope.loginProcess();
			expect(scope.emailError).toEqual([]);
			expect(scope.passwordError).toEqual(['has-error']);
			expect(scope.errorMessage).toBeTruthy();

			scope.password = 'password';
			scope.loginProcess();
			expect(scope.emailError).toEqual([]);
			expect(scope.passwordError).toEqual([]);
			expect(scope.errorMessage).toBeFalsy();

		});

		it('should handle authentication results and set the properties for the user', inject(function(_$httpBackend_, SMARF_URL){
			var email = 'rpsmarf@sce.carleton.ca';
			var password = 'fail';
			var data = {userid:35,email: 'test@test.com',firstname:'firstname', lastname:'lastname',community:{id:12,name:'the community', name_key:'the_community', description:'this is the community'}, imageUrl: 'http://www.avatar.com/', uiSettingsJson: JSON.stringify({something: true}), disqusInfo:{valid: false}};
			var u = {id:35,email: 'test@test.com',firstname:'firstname', lastname:'lastname', state:1, community:{id:12,name:'the community', name_key:'the_community', description:'this is the community'}, imageUrl: 'http://www.avatar.com/', uiSettings: {something: true}};
			$httpBackend = _$httpBackend_;

			
			scope.email = email;
			scope.password = password;
			scope.loginProcess();

			$httpBackend.expect('POST',SMARF_URL+'/scs/authentication/','email='+email+'&password='+password,{'Content-Type': 'application/x-www-form-urlencoded','Accept':'application/json, text/plain, */*'}).
				respond(401,null,null,'Unauthorized access.');

			$httpBackend.flush();

			expect(scope.authenticationError).toBe(true);

			password = 'canarie';
			scope.password = password;

			scope.loginProcess();

			$httpBackend.expect('POST',SMARF_URL+'/scs/authentication/','email='+email+'&password='+password,{'Content-Type': 'application/x-www-form-urlencoded','Accept':'application/json, text/plain, */*'}).
				respond(200,data,null,'Ok');

			
			$httpBackend.flush();

			

			expect(scope.authenticationError).toBe(false);

			var user = SmUser.getObject();
			expect(user).toEqual(u);

		}));
	});
  
});

