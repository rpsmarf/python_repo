'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 22, 2014

    @author: rpsmarf
'''
import logging
from smraicemsgs import SmRemAgSyncOps  # @UnresolvedImport
from smcommon.tasks.task_desc import SmTaskDesc
from smcommon.exceptions.sm_exc import SmException
import json
from smcommon.task_runners.task_runner_shell import SmTaskRunnerShell

logger = logging.getLogger(__name__)


class SmIceSyncOpsServer(SmRemAgSyncOps):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        pass
    
    def doOperation(self, opName, opParamsJson, current=None):
        try:
            if opName == "shellcommand":
                taskDesc = SmTaskDesc.makeFromJson(opParamsJson)
                taskRunner = SmTaskRunnerShell("", None, taskDesc)
                logger.debug("Running %s\n", opParamsJson)
                resp = taskRunner.runSyncCommandFromTaskDesc()
                logger.debug("Results: code=%d, stdout=%s stderr=%s",
                             resp[0], resp[1], resp[2])
                return json.dumps({"resultcode": resp[0],
                                   "stdout": resp[1].decode('utf-8'),
                                   "stderr": resp[2].decode('utf-8')})
            else:
                raise SmException("Unknown opType '" + opName + "' in doOperation")
        except Exception as e:
            logger.error("Error during doOperation start", exc_info=True)
            raise e
        
