'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 12, 2014

    @author: rpsmarf
'''
import logging
import os
from stat import S_ISDIR
from smcommon.constants.sm_const import SM_DATA_CONTAINER_PROTOCOL_LOCAL
from smcommon.exceptions.sm_exc import SmException
from smcommon.file_access.data_access_desc import smFileEntryCreate
from smcommon.exceptions.file_access_exception import smFsRaiseFileExcFomEnvError,\
    SmFileAccessException, smFsRaiseFileExecNotConnected,\
    smFsRaiseFileExecAlreadyConnected
import stat

logger = logging.getLogger(__name__)
smMyProcessUidGids = {"uid": os.geteuid(),
                      "gid_list": os.getgroups()
                      }


def sm_path_join(p1, p2):
    if not p2.startswith("/"):
        return p1 + "/" + p2
    else:
        return p1 + p2
        

class SmFileAccessorLocal(object):
    '''
    classdocs
    '''

    def __init__(self, dataContainer):
        '''
        Constructor
        '''
        self.dataContainer = dataContainer
        if (self.dataContainer.getProtocol() != SM_DATA_CONTAINER_PROTOCOL_LOCAL):
            raise SmException("Incorrect protocol " + self.dataContainer.protocol)
        self.connected = False
        
    def connectSession(self):
        if (self.connected):
            smFsRaiseFileExecAlreadyConnected()
        self.connected = True
    
    def disconnectSession(self):
        self.connected = False
        
    def deleteFileInfos(self, fileInfosToDelete):
        if (not self.connected):
            smFsRaiseFileExecNotConnected()
            
        try:
            parentPath = self.dataContainer.getPath()
            for f in fileInfosToDelete:
                fname = f["name"]
                # Remove a leading / if present to avoid escaping container to
                # the filesystem root
                if fname.startswith("/"):
                    fname = fname[1:]
                p2 = os.path.join(parentPath, fname) 
                if fname.endswith("/"):
                    os.rmdir(p2)
                else:
                    os.remove(p2)
        except Exception as e:  
            smFsRaiseFileExcFomEnvError(e)

    def deletePaths(self, pathsToDelete):
        '''
        Note that the paths must be sent in reverse list order
        '''
        if (not self.connected):
            smFsRaiseFileExecNotConnected()

        try:
            parentPath = self.dataContainer.getPath()
            for f in pathsToDelete:
                p2 = os.path.join(parentPath, f)
                logger.debug("Deleting path %s", p2) 
                if f.endswith("/"):
                    os.rmdir(p2)
                else:
                    os.remove(p2)
        except Exception as e:  
            smFsRaiseFileExcFomEnvError(e)

    def listLevelRecursively(self, returnList, path, recursiveDepth, maxEntries, lastPathReturned, showHidden):
        recursiveDepth -= 1
        absolute_path_for_this_level = os.path.join(self.dataContainer.getPath(), path)
        logger.debug("ListRecusively path " + path + " abs path " + absolute_path_for_this_level)
        try:
            fileList = os.listdir(absolute_path_for_this_level)
            
            logger.debug("Got " + str(len(fileList)) + " files")
            for f in sorted(fileList):
                # If file/folder name starts with '.' and not showHidden, skip it
                _, fname = os.path.split(f) 
                if not showHidden:
                    if fname.startswith("."):
                        logger.debug("Skipping path %s because it starts with '.'", fname)
                        continue
                    
                statInfo = os.stat(os.path.join(absolute_path_for_this_level, f))
                isDir = S_ISDIR(statInfo.st_mode)
                if isDir:
                    extendedRelativePath = os.path.join(path, f) + "/"
                    size = 0
                else:
                    extendedRelativePath = os.path.join(path, f)
                    size = statInfo.st_size
    
                if (lastPathReturned is None):
                    returnList.append(smFileEntryCreate(extendedRelativePath, size, statInfo.st_mtime,
                                                        isDir, 
                                                        statInfo.st_mode,
                                                        statInfo.st_uid,
                                                        statInfo.st_gid,
                                                        smMyProcessUidGids))
                    maxEntries -= 1
                    if (maxEntries == 0):
                        return 0, None
                elif (extendedRelativePath == lastPathReturned):
                        lastPathReturned = None
                if S_ISDIR(statInfo.st_mode):
                    if (recursiveDepth > 0) and (maxEntries > 0):
                        maxEntries, lastPathReturned = self.listLevelRecursively(returnList, extendedRelativePath, 
                                                                                 recursiveDepth, maxEntries, 
                                                                                 lastPathReturned, showHidden)
                if (maxEntries == 0):
                    return 0, None
    
            return maxEntries, lastPathReturned
        except Exception as e:  
            smFsRaiseFileExcFomEnvError(e)
    
    def list(self, path, recursiveDepth, maxEntries, lastPathReturned, showHidden):
        if (not self.connected):
            smFsRaiseFileExecNotConnected()
        if lastPathReturned == "":
            lastPathReturned = None
        returnList = []
        addTopEntry = False
        if (path.endswith("/")):
            if (path == "/"):
                path = ""
            elif (path.endswith("//")):
                addTopEntry = True
                path = path.replace("//", "/")
            self.listLevelRecursively(returnList, path, recursiveDepth, maxEntries, lastPathReturned, showHidden)
        else:
            addTopEntry = True
            
        if (addTopEntry):
            absolute_path_for_this_level = os.path.join(self.dataContainer.getPath(), path)
            try:
                statInfo = os.stat(absolute_path_for_this_level)
            except Exception as e:
                smFsRaiseFileExcFomEnvError(e)
                
            isDir = S_ISDIR(statInfo.st_mode)
            if isDir:
                size = 0
                if not path.endswith("/"):
                    path = path + "/"
            else:
                size = statInfo.st_size
            returnList.insert(0, smFileEntryCreate(path, size, statInfo.st_mtime, 
                                                   isDir, 
                                                   statInfo.st_mode,
                                                   statInfo.st_uid,
                                                   statInfo.st_gid,
                                                   smMyProcessUidGids))

        return returnList
    
    def mkdir(self, path, makeParentFolders=False):
        if (not self.connected):
            smFsRaiseFileExecNotConnected()
        
        full_path = os.path.join(self.dataContainer.getPath(), path)
        try:
            logger.debug("Creating folder %s", full_path)
            if makeParentFolders:
                os.makedirs(full_path)
            else:
                os.mkdir(full_path)
            return {"result": "ok", "reason": ""}        
        except FileNotFoundError:
            raise SmFileAccessException(422, "ENOENT", full_path, "No such file or directory")
        except Exception as e:  
            smFsRaiseFileExcFomEnvError(e)
                    
    def du(self, path, includeHidden=True):
        if (not self.connected):
            smFsRaiseFileExecNotConnected()
        
        try:
            listDict = self.list(path, 100, 10000000, None, includeHidden)
            totalSize = 0
            for f in listDict:
                totalSize += f["size"]
                
            return {"result": "ok", "sizeInBytes": totalSize, "fileCount": len(listDict)}        
        except FileNotFoundError:
            raise SmFileAccessException(422, "ENOENT", "No such file or directory")
        except Exception as e:  
            smFsRaiseFileExcFomEnvError(e)
                    
    def move(self, oldpath, newpath):
        if (not self.connected):
            smFsRaiseFileExecNotConnected()
        
        full_oldpath = os.path.join(self.dataContainer.getPath(), oldpath)
        full_newpath = os.path.join(self.dataContainer.getPath(), newpath)
        try:
            os.rename(full_oldpath, full_newpath)
            return {"result": "ok", "reason": ""}        
        except FileNotFoundError:
            raise SmFileAccessException(422, "ENOENT", full_oldpath, "No such file or directory")
        except Exception as e:  
            smFsRaiseFileExcFomEnvError(e)
                    
    def open(self, path, access_mode):
        if (not self.connected):
            smFsRaiseFileExecNotConnected()

        p = os.path.join(self.dataContainer.getPath(), path)
        logger.debug("Opening file %s in mode %s", p, access_mode)
        try:
            # Disallow opening existing files in write mode (but this is OK for named pipes (FIFO)
            if "w" in access_mode:
                if os.path.exists(p) and not stat.S_ISFIFO(os.stat(p).st_mode) and "u" not in access_mode:
                    raise SmFileAccessException(422, "EEXIST", p, "File already exists")
                    
            # Remove "u" if present
            access_mode = access_mode.replace("u", "")
            return open(p, access_mode)
        except Exception as e:  
            parentPath = os.path.dirname(p)
            if not os.path.exists(parentPath):
                raise SmFileAccessException(422, "ENOENT", p, "Parent folder does not exist")
            smFsRaiseFileExcFomEnvError(e)
        