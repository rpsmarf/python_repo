angular.module( 'rpsmarf.taskprogress', [
  'rpsmarf.config',
  'rpsmarf.confirm'
  
])

.controller( 'TaskProgressCtrl', ['$scope', '$modalInstance', 'taskObj', '$interval','TASK_STATE_ENUM','modalTitle','interactive','$modal','TASK_STATE_FINISHED','TASK_STATE_RUNNING', 
  function($scope, $modalInstance, taskObj, $interval,TASK_STATE_ENUM, modalTitle, interactive, $modal, TASK_STATE_FINISHED, TASK_STATE_RUNNING){
  $scope.status = ['active','progress-striped'];
  $scope.percentComplete = 0;
  $scope.title = modalTitle;
  $scope.interactive = interactive;

  $scope.disabled = false;
  var stop;
  $scope.taskState = TASK_STATE_ENUM['init'];
  if(interactive){
    
    $scope.disabled = true;
    stop = $interval(function() {
      taskObj.get().then(function(response){
        
        if(response.progressJson !== ''){
          $scope.percentComplete = JSON.parse(response.progressJson).progress;
        }

        if(response.state === TASK_STATE_RUNNING){
          $scope.percentComplete = 100;
          $modalInstance.close('ok');
        }
        $scope.taskState = TASK_STATE_ENUM[response.state];

      });
    },1000);
    $modalInstance.result.then(function(){
      $interval.cancel(stop);
      startInteractiveTool();
    }, function(){
      $interval.cancel(stop);
    });
  }
  else{
    stop = $interval(function() {
      taskObj.get().then(function(response){
        var pc ='';
        if(response.progressJson !== ''){
          pc = JSON.parse(response.progressJson).progress;
          if(pc > 100){
            pc = 100;
          }
          $scope.percentComplete = pc;
        }

        if(response.state === TASK_STATE_FINISHED){
          $scope.percentComplete = 100;
        }
        $scope.taskState = TASK_STATE_ENUM[response.state];
      });
    },1000);

    $scope.$watch(function(){
        return $scope.percentComplete >= 100 && $scope.taskState === TASK_STATE_ENUM['finished'];
      },function(newValue, oldValue){
      if(newValue){
          $interval.cancel(stop);
          $scope.status = ['progress-striped'];
        }
    });

    $modalInstance.result.then(function(){
      $interval.cancel(stop);
    }, function(){
      $interval.cancel(stop);
    });

  }
  
  
  
  $scope.ok = function(){
    $modalInstance.close('ok');
    //$interval.cancel(stop);
  };

  $scope.cancel = function(){
    $modalInstance.dismiss('cancel');
    //$interval.cancel(stop);
  };

  function startInteractiveTool(){
    var $interactiveModalConf = $modal.open({
      templateUrl: 'confirm/confirm.tpl.html',
      controller: 'ConfirmCtrl',
      resolve: {
        title: function(){return 'Confirm Interactive Task';},
        body: function(){return 'Your task is ready to start in interactive mode. Start interactive mode now? ';}

      }

    });

    $interactiveModalConf.result.then(function(){
      taskObj.get().then(function(response){
        var json;
        if(response.runningDisplayJson){
          json = JSON.parse(response.runningDisplayJson);
          window.open(json.taskView.url);
        }
        
      });
    }, function(){
      console.log('Interactive mode cancelled :\'(');
    });
  }

}]);