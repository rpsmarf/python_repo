angular.module( 'rpsmarf.upload', [
	'ui.bootstrap',
	'rpsmarf.user.service'
])

.controller( 'UploadCtrl', ['$scope', '$modalInstance', 'url', '$modal' , 'SmUser',
	function($scope, $modalInstance, url, $modal, SmUser){

		/***VARIABLES***/
		/*Standard*/
		var xhr;
		var patt = /(\\|\/|:|\*|\?|\"|<|>|\|)/;
		var csrf = SmUser.getProperty('csrf');
		/*Scope*/
		$scope.formError = false;
		$scope.uploading = false;
		$scope.formErrorNoFile = false;
		$scope.formErrorCharacters = false;
		$scope.percentComplete = 0;

		/***FUNCTIONS***/
		/*Scope*/
		$scope.upload = function(){
			var input = angular.element('#upload-form input#upload-selected-file');
			var fileName='x', message;
			if(!input.val() || input.val() === ''){
				$scope.formError = true;
				$scope.formErrorNoFile = true;
				return;
			}

			fileName = input.val().split(/(\\|\/)/g).pop();
			if($scope.checkFileName(fileName)){
				return;
			}

			$scope.uploading = true;
			var form = angular.element('#upload-form').get(0);
			var formData = new FormData(form);
			xhr = new XMLHttpRequest();
			xhr.addEventListener('error', onload, false);
			
			xhr.upload.addEventListener('progress', onprogress, false);
			xhr.upload.addEventListener('error', onload, false);
			xhr.addEventListener('load', onload, false);
			xhr.open('POST', url+encodeURIComponent(fileName)+'/upload/', true);
			xhr.setRequestHeader('X-CSRFToken',csrf);
			xhr.responseType = 'json';
			xhr.send(formData);

		};

		$scope.cancel = function(){
			$modalInstance.dismiss('cancel');
		};

		$scope.resetForm = function(){
			$scope.uploading = false;
			$scope.formError = false;
			$scope.formErrorNoFile = false;
			$scope.formErrorCharacters = false;
		};

		$scope.checkFileName = function(fileName){

			if(patt.test(fileName)){
				$scope.formError = true;
				$scope.formErrorCharacters = true;
				return true;
			}
			else{
				return false;
			}
		};

		/***STANDARD VARIABLES***/
		var onload = function(e){
			if(xhr.status == 200){
				$scope.$apply('percentComplete = 100');	
			}
			else{
				if(xhr.response && xhr.response.result === 'failure'){
					message = 'Could not upload the file due to the following error:<br>' + xhr.response.description;
				}
				else{
					message = 'An error occured while uploading the file';
				}
				$modal.open({
					controller: 'AlertCtrl',
					templateUrl: 'alert/alert.tpl.html',
					resolve: {
						message: function(){ return message; },
						title: function(){ return 'Error'; }
					}
				});
			}
			
		};

		var onprogress = function(e){
			if(e.lengthComputable && xhr.status == 200){
				$scope.$apply(function(scope){ scope.percentComplete = ((e.loaded / e.total) * 100).toFixed(2); });
			}
		};

}])
;
