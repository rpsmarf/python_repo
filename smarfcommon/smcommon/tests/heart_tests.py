'''
Created on Sep 8, 2014

@author: rpsmarf
'''
import unittest
from smcommon.heartbeats.heart import SmHeart
import time
from smcommon.heartbeats.heart_monitor import SmHeartMonitor
from smcommon.logging.sm_logger import smLoggingSetupForTest


class TestHeartNotification(object):
    
    def __init__(self):
        self.startCount = 0
        self.stopCount = 0
        self.unknownHeartCount = 0
        self.lastHeartId = ""
        
    def heartStopped(self, heartId):
        self.stopCount += 1
        self.lastHeartId = heartId
    
    def heartStarted(self, heartId):
        self.startCount += 1
        self.lastHeartId = heartId
        
    def unknownHeart(self, heartId):
        self.unknownHeartCount += 1
        self.lastHeartId = heartId

    def getStopCount(self):
        return self.stopCount
    
    def getStartCount(self):
        return self.startCount
    
    def getUnknownHeartCount(self):
        return self.unknownHeartCount
    
    
class HeartTest(unittest.TestCase):

    def setUp(self):
        self.lastBeatCount = -1
        self.lastHeartId = -1
        smLoggingSetupForTest()
        
    def receiveHeartbeat(self, heartId, beatCount):
        self.lastBeatCount = beatCount
        self.lastHeartId = heartId
    
    def waitUntilCount(self, value, func):
        count = 20
        while count > 0:
            count -= 1
            if (func() >= value):
                return
            time.sleep(0.1)
        raise Exception("Timeout waiting for " + str(func) + " to equal " + str(value) + " value is " + str(func()))
        
    def testHeart(self):
        h1 = SmHeart("11", 0.05, self.receiveHeartbeat)
        h1.start()
        time.sleep(0.2)
        self.assertTrue(h1.stopHeart())
        beatCount = h1.beatCount
        self.assertGreater(beatCount, 1)
        self.assertLess(beatCount, 6)
        self.assertEqual("11", self.lastHeartId)
    
        # Test that stop worked  
        time.sleep(0.1)
        self.assertEqual(beatCount, h1.beatCount)

    def testHeartMonitor(self):
        h1 = h2 = h3 = h4 = None
        hm = None
        try:
            notif = TestHeartNotification()
            hm = SmHeartMonitor(0.1, notif, 0.2, 0.1)
            hm.addHeart("11")
            hm.addHeart("22")
            hm.start()
            
            # Start 2 hearts with heart in monitor
            h1 = SmHeart("11", 0.05, hm.heartbeat)
            h1.start()
            h2 = SmHeart("22", 0.05, hm.heartbeat)
            h2.start()
            
            # Observe both hearts in monitor
            time.sleep(0.2)
            self.assertGreater(hm.getHeartStatus("11").lastBeatCount, 1)
            self.assertGreater(hm.getHeartStatus("22").lastBeatCount, 1)
            self.assertEqual(2, notif.getStartCount())
            self.assertEqual(2, notif.getStartCount())
    
            # Add another heart
            hm.addHeart("33")
            
            # Will not report stopped until 0.2 seconds because 
            # it is just added
            time.sleep(0.1)
            self.assertEqual(0, notif.getStopCount())
    
            # Observe report about this stopped heart
            self.waitUntilCount(1, notif.getStopCount)
            self.assertEqual("33", notif.lastHeartId)
            
            # Start the heart - observe report
            notif.startCount = 0
            h3 = SmHeart("33", 0.05, hm.heartbeat)
            h3.start()
            self.waitUntilCount(1, notif.getStartCount)
            self.assertEqual("33", notif.lastHeartId)
            
            # Stop the first heart - observe report of failure
            h1.stopHeart()
            self.waitUntilCount(2, notif.getStopCount)
            self.assertEqual("11", notif.lastHeartId)
            
            # Start the first heart - observe report of recovery
            h1 = SmHeart("11", 0.05, hm.heartbeat)
            h1.start()
            self.waitUntilCount(2, notif.getStartCount)
            self.assertEqual("11", notif.lastHeartId)
    
            # Simulate restart of second heart by resetting the beat count - observe restart report
            h2.beatCount = 0
            self.waitUntilCount(3, notif.getStopCount)
            self.assertEqual("22", notif.lastHeartId)
            self.waitUntilCount(3, notif.getStartCount)
            self.assertEqual("22", notif.lastHeartId)
            
            # Start unregistered heart, observe report of unknown heart
            h4 = SmHeart("44", 0.05, hm.heartbeat)
            h4.start()
            self.waitUntilCount(1, notif.getUnknownHeartCount)
            self.assertEqual("44", notif.lastHeartId)

        finally:
            # Stop all hearts
            if h1:
                h1.stopHeart()
            if h2:
                h2.stopHeart()
            if h3:
                h3.stopHeart()
            if h4:
                h4.stopHeart()
            if hm:
                hm.stopMonitor()
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()