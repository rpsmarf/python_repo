#!/bin/bash

if [ "$SCS_URL" == "" ]
then 
    SCS_URL=localhost
fi

if [ "$SCS_RESOURCE" == "" ]
then 
    SCS_RESOURCE=1
fi

URL_BASE=$SCS_URL/scs/resource/$SCS_RESOURCE/
SHOW_CODE="-w \\nHTTP_CODE:%{http_code}\\n"
echo "Hello world" > /tmp/f1

echo ""
echo "This script runs through a number of steps to show off the "
echo "operation of the RP-SMARF Cloud Storage RPI"
echo ""
echo "This demo is accessing $SCS_URL"
echo ""
echo "Hit ENTER to continue..."
read text
echo "Listing resource/$SCS_RESOURCE... via $URL_BASE/list/"
curl $URL_BASE/list/
read text
echo "Uploading file 'testfile'" to $URL_BASE/testfile/upload/
curl $SHOW_CODE -F uploadfile=@/tmp/f1 $URL_BASE/testfile/upload/
read text
echo "Listing resource/$SCS_RESOURCE..., expecting to see testfile"
curl $URL_BASE/list/
read text
echo "Downloading file from  $URL_BASE/testfile/download/."
echo "Content start:----------"
curl $URL_BASE/testfile/download/
echo "---------- Content end."
read text
echo "Renaming testfile to x by $URL_BASE/testfile/rename/?newName=x "
curl $SHOW_CODE $URL_BASE/testfile/rename/?newName=x
read text
echo "Listing resource/$SCS_RESOURCE..., expecting to see 'x'"
curl $URL_BASE/list/
read text
echo "Deleting file x via URL $URL_BASE/x/file/"
curl $SHOW_CODE -X DELETE $URL_BASE/x/file/
read text
echo "Listing resource/$SCS_RESOURCE..."
curl $URL_BASE/list/
read text
