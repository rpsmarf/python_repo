'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 12, 2014

    @author: rpsmarf
'''
import logging
import paramiko
from smcommon.exceptions.sm_exc import SmException
import os
from stat import S_ISDIR
from smcommon.constants.sm_const import SM_DATA_CONTAINER_PROTOCOL_SFTP,\
    SM_DATA_CONTAINER_PROTOCOL_SSH
from smcommon.file_access.data_access_desc import smFileEntryCreate
from smcommon.exceptions.file_access_exception import smFsRaiseFileExcFomEnvError,\
    SmFileAccessException, smFsRaiseFileExecNotConnected
from smcommon.utils.net_utils import smLoadKeyFile

logger = logging.getLogger(__name__)


def _getname(statInfo):
    return statInfo.filename


class SmFileAccessorSftp(object):
    '''
    classdocs
    '''

    def __init__(self, dataContainer):
        '''
        Constructor
        '''
        self.dataContainer = dataContainer
        if (self.dataContainer.getProtocol() != SM_DATA_CONTAINER_PROTOCOL_SFTP) \
                and (self.dataContainer.getProtocol() != SM_DATA_CONTAINER_PROTOCOL_SSH):
            raise SmException("Incorrect protocol " + self.dataContainer.getProtocol())
        self.ssh = None
        self.ftp = None
        self.connected = False
        self.mySftpUid = 0
        uid = -1
        if self.dataContainer.getParam("uid") is not None:
            uid = int(self.dataContainer.getParam("uid"))
        self.myProcessInfo = {"uid": uid}
        
        gid_list = []
        if self.dataContainer.getParam("gid_list") is not None:
            gid_list_strings = self.dataContainer.getParam("gid_list").split("_")
            for gid_string in gid_list_strings:
                gid_list.append(int(gid_string))
        self.myProcessInfo["gid_list"] = gid_list
       
    def connectSession(self):
        if (self.connected):
            raise SmFileAccessException(422, "ALREADYCONNECTED", "", "Client is already connected")
        
        try:
            #mykey = smLoadKeyFile(self.keyfile)
            self.ssh = paramiko.SSHClient()
            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            password = self.dataContainer.getParam("password")
            mykey = smLoadKeyFile(self.dataContainer.getParam("keyfile"))
            logger.debug("Connecting to %s:%d as %s", self.dataContainer.getHost(), self.dataContainer.getPort(),
                         self.dataContainer.getUsername())
            self.ssh.connect(self.dataContainer.getHost(), 
                             username=self.dataContainer.getUsername(), 
                             port=self.dataContainer.getPort(),
                             pkey=mykey,
                             password=password)
            #logger.debug("Connect ok")
            # Do local to SFTP copy
            self.ftp = self.ssh.open_sftp()
            #logger.debug("open_sftp ok")
            self.connected = True
        except Exception as e:
            logger.warning("Exception in connection", exc_info=True)
            raise e
            
    def disconnectSession(self):
        if (self.connected):
            self.ftp.close()
            self.ssh.close()
            self.connected = False
        
    def deleteFileInfos(self, fileInfosToDelete):
        if (not self.connected):
            smFsRaiseFileExecNotConnected()

        try:
            parentPath = self.dataContainer.getPath()
            for f in fileInfosToDelete:
                fname = f["name"]
                p2 = os.path.join(parentPath, fname) 
                if fname.endswith("/"):
                    self.ftp.rmdir(p2)
                else:
                    self.ftp.remove(p2)
        except Exception as e:  
            smFsRaiseFileExcFomEnvError(e)
    
    def deletePaths(self, pathsToDelete):
        if (not self.connected):
            smFsRaiseFileExecNotConnected()

        try:
            parentPath = self.dataContainer.getPath()
            for fname in pathsToDelete:
                p2 = os.path.join(parentPath, fname) 
                if fname.endswith("/"):
                    self.ftp.rmdir(p2)
                else:
                    self.ftp.remove(p2)         
        except Exception as e:  
            smFsRaiseFileExcFomEnvError(e)
            
    def listLevelRecursively(self, returnList, path, recursiveDepth, maxEntries, lastPathReturned, showHidden):
        recursiveDepth -= 1
        absolute_path_for_this_level = os.path.join(self.dataContainer.getPath(), path)
        logger.debug("Abs path in  list is %s", absolute_path_for_this_level)
        try:
            fileList = self.ftp.listdir_attr(absolute_path_for_this_level)
        except Exception as e:  
            smFsRaiseFileExcFomEnvError(e)
        for f in sorted(fileList, key=_getname):
            # If file/folder name starts with '.' and not showHidden, skip it
            _, fname = os.path.split(f.filename) 
            if not showHidden:
                if fname.startswith("."):
                    continue
                
            if S_ISDIR(f.st_mode):
                size = 0
                extendedRelativePath = os.path.join(path, f.filename) + "/"
            else:
                size = f.st_size
                extendedRelativePath = os.path.join(path, f.filename)

            if (lastPathReturned is None):
                returnList.append(smFileEntryCreate(extendedRelativePath, size, 
                                                    f.st_mtime, 
                                                    S_ISDIR(f.st_mode),
                                                    f.st_mode,
                                                    f.st_uid,
                                                    f.st_gid,
                                                    self.myProcessInfo))
                maxEntries -= 1
                if (maxEntries == 0):
                    return 0, None
            elif (extendedRelativePath == lastPathReturned):
                    lastPathReturned = None
            if S_ISDIR(f.st_mode):
                if (recursiveDepth > 0) and (maxEntries > 0):
                    maxEntries, lastPathReturned = self.listLevelRecursively(returnList, extendedRelativePath, 
                                                                             recursiveDepth, maxEntries, 
                                                                             lastPathReturned, showHidden)
            if (maxEntries == 0):
                return 0, None
        return maxEntries, lastPathReturned

    def list(self, path, recursiveDepth, maxEntries, lastPathReturned, showHidden):
        if (not self.connected):
            smFsRaiseFileExecNotConnected()

        # ICE Sends "" not None, we fix this here
        if (lastPathReturned == ""):
            lastPathReturned = None
        returnList = []
        addTopEntry = False
        if (path.endswith("/")):
            if (path == "/"):
                path = ""
            elif (path.endswith("//")):
                addTopEntry = True
                path = path.replace("//", "/")
            self.listLevelRecursively(returnList, path, recursiveDepth, maxEntries, lastPathReturned, showHidden)
            
        else:
            addTopEntry = True
            
        if (addTopEntry):
            absolute_path_for_this_level = os.path.join(self.dataContainer.getPath(), path)
            try:
                statInfo = self.ftp.stat(absolute_path_for_this_level)
            except Exception as e:
                smFsRaiseFileExcFomEnvError(e)

            returnList.insert(0, smFileEntryCreate(path, statInfo.st_size, 
                                                   statInfo.st_mtime,
                                                   S_ISDIR(statInfo.st_mode),
                                                   statInfo.st_mode,
                                                   statInfo.st_uid,
                                                   statInfo.st_gid,
                                                   self.myProcessInfo))

        return returnList

    def mkdir(self, path, makeParentFolders):
        if (not self.connected):
            smFsRaiseFileExecNotConnected()

        try:
            if makeParentFolders:
                # Attempt to create parent folders
                tokens = path.split("/")
                p2 = ""
                for token in tokens:
                    p2 = os.path.join(p2, token)
                    self.ftp.mkdir(p2)
                # Redo mkdir
                full_path2 = os.path.join(self.dataContainer.getPath(), path)
                logger.debug("Creating parent folder: %s", full_path2)
                self.ftp.mkdir(full_path2)
            full_path = os.path.join(self.dataContainer.getPath(), path)
            logger.debug("Creating folder: %s", full_path)
            self.ftp.mkdir(full_path)
        except Exception as e:  
            smFsRaiseFileExcFomEnvError(e)
                        
    def du(self, path, showHidden=True):
        if (not self.connected):
            smFsRaiseFileExecNotConnected()

        totalSize = 0
        try:
            listDict = self.list(path, 100, 10000000, None, showHidden)
            for f in listDict:
                totalSize += f["size"]
                
            return {"result": "ok", "sizeInBytes": totalSize}        
        except Exception as e:  
            smFsRaiseFileExcFomEnvError(e)
                        
    def move(self, oldpath, newpath):
        if (not self.connected):
            smFsRaiseFileExecNotConnected()

        try:
            fullOldPath = os.path.join(self.dataContainer.getPath(), oldpath)
            fullNewPath = os.path.join(self.dataContainer.getPath(), newpath)
            self.ftp.rename(fullOldPath, fullNewPath)
        except Exception as e:  
            smFsRaiseFileExcFomEnvError(e)
                        
    def open(self, path, access_mode):
        if (not self.connected):
            smFsRaiseFileExecNotConnected()

        try:
            #if "w" in access_mode:
            #    access_mode += "x"  # Forces failure if file is not being created
            full_path = os.path.join(self.dataContainer.getPath(), path)
            return self.ftp.open(full_path,
                                 access_mode)
        except Exception as e:  
            logger.exception("Error in open with path %s and mode %s", full_path, access_mode)
            smFsRaiseFileExcFomEnvError(e, full_path)
        