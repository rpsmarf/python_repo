'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 23, 2015

    @author: rpsmarf
'''
import logging
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_ID,\
    SCS_CLOUDIF_STATE, SCS_CLOUDIF_STATE_RUNNING, SCS_CLOUDIF_HOST

logger = logging.getLogger(__name__)


class ScsCloudifMock(object):
    '''
    This implements a mock version of the cloud interface to allow 
    more complete and faster testing 
    '''
    def __init__(self):
        '''
        Constructor
        '''
        self.create_server_exception = None
        self.terminate_server_exception = None
        self.get_server_list_exception = None
        self.get_server_info_exception = None
        self.serverDict = {}
        self.lastId = 100
        self.initialServerState = SCS_CLOUDIF_STATE_RUNNING
        
    def setLastCloudInfo(self, cloud_info, cloud_account_info):
        self.last_cloud_info = cloud_info
        self.last_cloud_account_info = cloud_account_info
        
    def _makeId(self):
        self.lastId += 1
        return str(self.lastId)    
    
    def create_server(self, paramDict):
        if self.create_server_exception is not None:
            raise self.create_server_exception
        serverId = self._makeId()
        serverInfoDict = {SCS_CLOUDIF_ID: serverId,
                          SCS_CLOUDIF_HOST: "localhost",
                          SCS_CLOUDIF_STATE: self.initialServerState}
        self.serverDict[serverId] = serverInfoDict
        return serverInfoDict
    
    def terminate_server(self, serverId):
        if self.terminate_server_exception is not None:
            raise self.terminate_server_exception
        del self.serverDict[serverId]
    
    def get_server_list(self):
        if self.get_server_list_exception is not None:
            raise self.get_server_list_exception
        return self.serverDict.values()
    
    def get_server_info(self, serverId):
        if self.get_server_info_exception is not None:
            raise self.get_server_info_exception
        if serverId not in self.serverDict:
            return None
        return self.serverDict[serverId]    
    
    def is_server_up(self, serverId, serverInfoDict=None):
        if serverInfoDict is None:
            if serverId not in self.serverDict:
                return False
            serverInfoDict = self.serverDict[serverId]
        return serverInfoDict[SCS_CLOUDIF_STATE] == SCS_CLOUDIF_STATE_RUNNING
    
    def testif_setServerState(self, serverId, newState):
        self.serverDict[serverId][SCS_CLOUDIF_STATE] = newState
            
