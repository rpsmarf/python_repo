This folder contains files which are for the RP-SMARF software
distribution node.

The key folder is /var/www because it contains the Apache HTML files
which support:
- the software distribution via apt-get
- the ICE documentation files
