'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Aug 26, 2015

    @author: rpsmarf
'''

import logging
from smarfcontrolserver.init.envvar import scsGetEnvVar,\
    SCS_ENV_VAR_DISQUS_SECRET_KEY, SCS_ENV_VAR_DISQUS_PUBLIC_KEY
import json

logger = logging.getLogger(__name__)

import base64
import hashlib
import hmac
import time
 
DISQUS_SECRET_KEY = scsGetEnvVar(SCS_ENV_VAR_DISQUS_SECRET_KEY)
DISQUS_PUBLIC_KEY = scsGetEnvVar(SCS_ENV_VAR_DISQUS_PUBLIC_KEY)
 
 
def get_disqus_sso(user):
    if DISQUS_SECRET_KEY is None or DISQUS_PUBLIC_KEY is None:
        logger.warning("At least one of %s and %s missing", DISQUS_SECRET_KEY, DISQUS_PUBLIC_KEY)
        # create a JSON packet of our data attributes
        return {"remote_auth_s3": "NO DISQUS KEYS SET ON SERVER",
                "api_key": "NO DISQUS KEYS SET ON SERVER",
                "valid": False} 
    data = json.dumps({
        'id': user,
        'username': user,
        'email': user,
    })
    # encode the data to base64
    message = base64.b64encode(data.encode('utf-8'))
    # generate a timestamp for signing the message
    timestamp = int(time.time())
    # generate our hmac signature
    sig = hmac.HMAC(DISQUS_SECRET_KEY.encode('utf-8'), 
                    ('%s %s' % (message.decode('utf-8'), timestamp)).encode('utf-8'), 
                    hashlib.sha1).hexdigest()
 
    return {"remote_auth_s3": "{} {} {}".format(message.decode("utf-8"), sig, timestamp),
            "api_key": DISQUS_PUBLIC_KEY,
            "valid": True}
