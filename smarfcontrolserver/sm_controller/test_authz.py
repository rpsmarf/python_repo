'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 10, 2015

    @author: rpsmarf
'''

from datetime import timedelta
import json
import unittest

from django.contrib.auth.models import User
from django.test.testcases import TransactionTestCase
from guardian.shortcuts import assign_perm

from sm_controller.models import SmModelResource, SmModelCommunity, \
    SmModelTaskType, SmModelTask, SmModelResourceTypeTaskType, SmModelAgent, \
    SmModelResourceType, SmModelContainer, SmModelPropertyType, SmModelPathList,\
    SmModelFileSet
from sm_controller.signal_handlers import smTriggerSignalInstall
from sm_controller.test_auth_helper import TestVisibleToAllEditOnlyByOwner, \
    TestVisibleToAllEditOnlySuperuser, TestAuthReadOnlyFieldChecker, \
    TestAuthHelperUserSettings, TestVisibleOnlyToSelf, \
    TestVisibleToAllEditOnlyWithPerm, TestAuthHelperResourceTest, \
    TestAuthHelperUserObject, \
    TestVisibleToAllCreateWithPermEditByOwner, \
    TestVisibleToAllCreateDeleteByResOrCommOwner
from sm_controller.test_helper import ScsTestHelper
from smcommon.utils.date_utils import sm_date_now


smTriggerSignalInstall()


class AuthTest(TransactionTestCase):

    def setUp(self):
        self.helper = ScsTestHelper(self)
        self.helper.setupTempFolder()
        self.helper.setupModel()
        self.helper.setupRestAuth("root")
        self.helper.setupRemoteAgentViaIce()
        self.count = 0
        
    def tearDown(self):
        self.helper.tearDown()
        TransactionTestCase.tearDown(self)
        
    def newsItemDictGen(self, name):
        self.count += 1
        return {"release_date_time": str(sm_date_now()),
                "community": self.communityPath,
                "headline": "xxx" + str(self.count),
                "external_link": "http://a.b.com/x",
                "body": "body body body"}
        
    def test_newsItemAuth(self):
        community = SmModelCommunity.objects.first()
        self.communityPath = "/scs/community/" + str(community.id) + "/"
        authTester = TestVisibleToAllEditOnlyByOwner(self,
                                                     "news_item",
                                                     "headline", "new_headline",
                                                     self.newsItemDictGen)
        authTester.runTest()
        
        # Check that a community owner can also edit a news item
        ## Create 3 users usera, userb and userc
        usera = User(username="usera", first_name="oth2", last_name="er2")
        usera.save()
        userb = User(username="userb", first_name="oth2", last_name="er2")
        userb.save()
        userc = User(username="userc", first_name="oth2", last_name="er2")
        userc.save()
        
        # Create a community owner by usera
        self.helper.setupRestAuth("root")
        permission_name = "sm_controller.add_smmodelcommunity"
        self.helper.objGetViaRest("/scs/user/" + str(usera.id) + 
                             "/setperm/?action=assign&perm=" +
                             permission_name)
        self.helper.setupRestAuth("usera")
        community_path = self.helper.objCreateViaRest("/scs/community/",
                                                      self.communityAuthDictGen("aaa"))        
        # Create a new items ownerd by userb
        self.helper.setupRestAuth("userb")
        news_path = self.helper.objCreateViaRest("/scs/news_item/",
                                                 {"headline": "h1",
                                                  "community": community_path})
        # Modify news item as usera - expect OK
        self.helper.setupRestAuth("usera")
        self.helper.objModifyViaRest(news_path, {"body": "abc"})
        
        # Modify news item as userb - expect OK
        self.helper.setupRestAuth("userb")
        self.helper.objModifyViaRest(news_path, {"body": "def"})
        
        # Modify news item as userc - expect failure
        self.helper.setupRestAuth("userc")
        self.assertRaises(AssertionError, self.helper.objModifyViaRest, news_path,
                          {"body": "This is another long description"})

        # Check read only fields
        readonlyFieldTester = TestAuthReadOnlyFieldChecker(self,
                                                           "news_item",
                                                           self.newsItemDictGen,
                                                           {"owner": "garbage"})
        readonlyFieldTester.runTest()
                    
    def test_userSettingAuth(self):
        community = SmModelCommunity.objects.first()
        self.communityPath = "/scs/community/" + str(community.id) + "/"
        authTester = TestAuthHelperUserSettings(self)
        authTester.runTest()

    def favResDictGen(self, name):
        return {"resource": self.resourcePath}
        
    def test_favouriteResourceAuth(self):
        resource = SmModelResource.objects.first()
        self.resourcePath = "/scs/resource/" + str(resource.id) + "/"
        authTester = TestVisibleOnlyToSelf(self,
                                           "favourite_resource",
                                           "resource", self.resourcePath,
                                           self.favResDictGen)
        authTester.runTest()
        readonlyFieldTester = TestAuthReadOnlyFieldChecker(self,
                                                  "favourite_resource",
                                                  self.favResDictGen,
                                                  {"owner": "garbage"})
        readonlyFieldTester.runTest()

    def favTaskTypeDictGen(self, name):
        return {"task_type": self.task_type_path}
        
    def test_favouriteTaskTypeAuth(self):
        task_type = SmModelTaskType.objects.first()
        self.task_type_path = "/scs/task_type/" + str(task_type.id) + "/"
        authTester = TestVisibleOnlyToSelf(self,
                                           "favourite_task_type",
                                           "task_type", self.task_type_path,
                                           self.favTaskTypeDictGen)
        authTester.runTest()
        readonlyFieldTester = TestAuthReadOnlyFieldChecker(self,
                                                           "favourite_task_type",
                                                           self.favTaskTypeDictGen,
                                                           {"owner": "garbage"})
        readonlyFieldTester.runTest()

    def taskDictGen(self, name):
        self.count += 1
        return {"task_type": self.task_type_path,
                "community": "abc",
                "parametersJson": "{}"}
        
    def test_taskAuth(self):
        task_type = SmModelTaskType.objects.first()
        self.task_type_path = "/scs/task_type/" + str(task_type.id) + "/"
        authTester = TestVisibleOnlyToSelf(self,
                                           "task",
                                           "state", "finished",
                                           self.taskDictGen)
        authTester.runTest()

        readonlyFieldTester = TestAuthReadOnlyFieldChecker(self,
                                                           "task",
                                                           self.taskDictGen,
                                                           {"state": "garbage",
                                                            "completion": "garbage",
                                                            "completionReason": "garbage",
                                                            "completionDescription": "garbage",
                                                            "stdout": "garbage",
                                                            "stderr": "garbage",
                                                            "owner": "garbage"
                                                            })
        readonlyFieldTester.runTest()

    def taskResourceDictGen(self, name):
        self.count += 1
        t = SmModelTask()
        t.owner = User.objects.get(username="owner")
        t.task_type = SmModelTaskType.objects.first()
        t.status = "init"
        t.parametersJson = json.dumps({})
        t.start_time = str(sm_date_now())
        t.end_time = str(sm_date_now())
        t.save()
        task_path = "/scs/task/" + str(t.id) + "/"
        return {"task": task_path,
                "resource": self.resource_path,
                "resource_type_task_type": self.rttt_path,
                "parametersJson": "{}"}
        
    def test_taskResourceAuth(self):
        resource = SmModelResource.objects.first()
        self.resource_path = "/scs/resource/" + str(resource.id) + "/"
        rttt = SmModelResourceTypeTaskType.objects.first()
        self.rttt_path = "/scs/resource_type_task_type/" + str(rttt.id) + "/"

        authTester = TestVisibleOnlyToSelf(self,
                                           "task_resource",
                                           "parametersJson", '{"a": "b"}',
                                           self.taskResourceDictGen)
        authTester.runTest()

    def taskNoteDictGen(self, name):
        self.count += 1
        return {"task": "/scs/task/" + str(self.task.pk) + "/",
                "note": "xxx" + str(self.count)
                }
        
    def taskTypeNoteDictGen(self, name):
        self.count += 1
        return {"task_type": "/scs/task_type/" + str(self.task_type.pk) + "/",
                "note": "xxx" + str(self.count)
                }
        
    def test_taskTypeNoteAuth(self):
        self.task_type = SmModelTaskType.objects.all()[0]
        authTester = TestVisibleOnlyToSelf(self,
                                           "task_type_note",
                                           "note", "new_note_value",
                                           self.taskTypeNoteDictGen)
        authTester.runTest()

    def resourceNoteDictGen(self, name):
        self.count += 1
        return {"resource": "/scs/resource/" + str(self.resource.pk) + "/",
                "note": "xxx" + str(self.count)
                }
        
    def test_resourceNoteAuth(self):
        self.resource = SmModelResource.objects.all()[0]
        authTester = TestVisibleOnlyToSelf(self,
                                           "resource_note",
                                           "note", "new_note_value",
                                           self.resourceNoteDictGen)
        authTester.runTest()

    def agentAuthDictGen(self, name):
        self.count += 1
        return {"guid": "GUID" + str(self.count),
                "name": "name" + str(self.count),
                "description": "Description which is very very long",
                "agentUrl": "http://a.b.c/" + str(self.count)
                }
        
    def test_agentAuth(self):
        authTester = TestVisibleToAllEditOnlyWithPerm(self,
                                                      "agent",
                                                      "description",
                                                      "new_description which is long enough",
                                                      self.agentAuthDictGen,
                                                      "add_smmodelagent")
        authTester.runTest()

        readonlyFieldTester = TestAuthReadOnlyFieldChecker(self,
                                                           "agent",
                                                           self.agentAuthDictGen,
                                                           {"status": "garbage",
                                                            "last_change": "garbage",
                                                            "owner": "garbage"})
        readonlyFieldTester.runTest()
        
    def containerAuthDictGen(self, name):
        self.count += 1
        return {"name": "name" + str(self.count),
                # "name_key": "namekey" + str(self.count),
                "description": "Description which is very very long",
                "containerUrl": "http://a.b.c/" + str(self.count),
                "agent": "/scs/agent/" + str(self.agent.id) + "/"
                }
        
    def test_containerAuth(self):
        self.agent = SmModelAgent.objects.first()
        authTester = TestVisibleToAllEditOnlyWithPerm(self,
                                                      "container",
                                                      "description",
                                                      "new_description which is long enough",
                                                      self.containerAuthDictGen,
                                                      "add_smmodelcontainer")
        authTester.runTest()

        readonlyFieldTester = TestAuthReadOnlyFieldChecker(self,
                                                           "container",
                                                           self.containerAuthDictGen,
                                                           {"status": "garbage",
                                                            "owner": "garbage"})
        readonlyFieldTester.runTest()
        
    def scriptAuthDictGen(self, name):
        self.count += 1
        return {"key": "xxx" + str(self.count),
                "value": "value " + str(self.count)
                }
        
    def test_scriptAuth(self):
        self.agent = SmModelAgent.objects.first()
        authTester = TestVisibleToAllEditOnlyWithPerm(self,
                                                      "script",
                                                      "value",
                                                      "new value which is long enough",
                                                      self.scriptAuthDictGen,
                                                      "add_smmodeltasktype")
        authTester.runTest()    

    def resourceAuthDictGen(self, name):
        self.count += 1
        return {
                "owner": "/scs/user/99",  # Should be overriden to be the invoking user
                "name": "name 1",
                "resource_type": "/scs/resource_type/" + str(SmModelResourceType.objects.first().id) + "/",
                "description": "My description is long enough",
                "container": "/scs/container/" + str(SmModelContainer.objects.first().id) + "/",
                "nature": "data",
                "parametersJson": '{ "folder": "r1/" }',
                "status": "down",
                "personalFolder": False,
                "capacityJson": "{}"
        }
        
    def test_resourceAuth(self):
        self.helper.setupZipUnzipTaskTypes()
        authTester = TestAuthHelperResourceTest(self)
        authTester.runTest()

        readonlyFieldTester = TestAuthReadOnlyFieldChecker(self,
                                                           "resource",
                                                           self.resourceAuthDictGen,
                                                           {"status": "garbage",
                                                            "owner": "garbage"})
        readonlyFieldTester.runTest()
        
    def resourceTypeAuthDictGen(self, name):
        self.count += 1
        return {"name": "name" + str(self.count),
                "description": "Description which is very very long",
                "code_module": "a",
                "code_classname": "b",
                "nature": "data",
                "configuration": "{}"
                }
        
    def test_resourceTypeAuth(self):
        authTester = TestVisibleToAllEditOnlySuperuser(self,
                                                       "resource_type",
                                                       "description",
                                                       "new_description which is long enough",
                                                       self.resourceTypeAuthDictGen)
        authTester.runTest()

    def resourceTagAuthDictGen(self, name):
        self.count += 1
        return {"tag": self.authTester.tag_path,
                "resource": self.authTester.resource_path
                }
        
    def test_resourceTagAuth(self):        
        self.authTester = TestVisibleToAllCreateDeleteByResOrCommOwner(self,
                                                                       "resource_tag",
                                                                       self.resourceTagAuthDictGen)
        self.authTester.runTest()

    def resourceTypeTaskTypeAuthDictGen(self, name):
        self.count += 1
        return {"name": "name" + str(self.count),
                "description": "Description which is very very long",
                "resource_type": self.resource_type_path,
                "task_type": self.task_type_path,
                "role": "role",
                "nature": "data"
                }
        
    def test_resourceTypeTaskTypeAuth(self):
        task_type = SmModelTaskType.objects.first()
        self.task_type_path = "/scs/task_type/" + str(task_type.id) + "/"
        resource_type = SmModelResourceType.objects.first()
        self.resource_type_path = "/scs/resource_type/" + str(resource_type.id) + "/"
        authTester = TestVisibleToAllEditOnlySuperuser(self,
                                                       "resource_type_task_type",
                                                       "description",
                                                       "new_description which is long enough",
                                                       self.resourceTypeTaskTypeAuthDictGen)
        authTester.runTest()

    def test_userAuth(self):
        authTester = TestAuthHelperUserObject(self,
                                              "user",
                                              "first_name",
                                              "new_name")
        authTester.runTest()

    def communityAuthDictGen(self, name):
        self.count += 1
        return {"name": "name" + str(self.count),
                "description": "Description which is very very long"
                }
        
    def test_communityAuth(self):
        authTester = TestVisibleToAllCreateWithPermEditByOwner(self,
                                                               "community",
                                                               "description",
                                                               "new_description which is long enough",
                                                               self.communityAuthDictGen,
                                                               "add_smmodelcommunity")
        authTester.runTest()

        readonlyFieldTester = TestAuthReadOnlyFieldChecker(self,
                                                  "community",
                                                  self.communityAuthDictGen,
                                                  {"owner": "garbage"})
        readonlyFieldTester.runTest()
           
    def faqAuthDictGen(self, name):
        self.count += 1
        return {"question": "name" + str(self.count),
                "answer": "Description which is very very long",
                "target": "/scs/resource/1/"
                }
        
    def test_faqAuth(self):
        authTester = TestVisibleToAllEditOnlyByOwner(self,
                                                     "faq",
                                                     "answer",
                                                     "answer which is long enough",
                                                     self.faqAuthDictGen)
        authTester.runTest()

        readonlyFieldTester = TestAuthReadOnlyFieldChecker(self,
                                                           "community",
                                                           self.communityAuthDictGen,
                                                           {"owner": "garbage"})
        readonlyFieldTester.runTest()
           
    def propertyAuthDictGen(self, name):
        self.count += 1
        return {"key": "key" + str(self.count),
                "value": "value" + str(self.count),
                }
        
    def test_propertyAuth(self):
        self.agent = SmModelAgent.objects.first()
        authTester = TestVisibleToAllEditOnlySuperuser(self,
                                                       "property",
                                                       "value",
                                                       "value which is long enough",
                                                       self.propertyAuthDictGen)
        authTester.runTest()

    def taskTypeTagAuthDictGen(self, name):
        self.count += 1
        return {"tag": self.authTester.tag_path,
                "task_type": self.authTester.task_type_path
                }
        
    def test_taskTypeTagAuth(self):
        self.tag_path = self.helper.objCreateViaRest("/scs/tag/", {"tag_name": "123"})
        self.authTester = TestVisibleToAllCreateDeleteByResOrCommOwner(self,
                                                                       "task_type_tag",
                                                                       self.taskTypeTagAuthDictGen,
                                                                       )
        self.authTester.runTest()

    def reservationAuthDictGen(self, name):
        self.count += 1
        return {"resource": self.resource_path,
                "start_time": str(sm_date_now() + timedelta(seconds=(20 * self.count))),
                "end_time": str(sm_date_now() + timedelta(seconds=((20 * self.count) + 10))),
                "description": "This is the description"
                }

    def addReservationPermsissions(self, uOwner, uOther):
        assign_perm("execute_on_resource", uOwner, self.resource)
            
    def test_reservationAuth(self):
        self.resource = SmModelResource.objects.first()
        self.resource_path = "/scs/resource/" + str(self.resource.id) + "/"
        
        authTester = TestVisibleToAllEditOnlyByOwner(self,
                                                     "reservation",
                                                     "description",
                                                     "abcdef",
                                                     self.reservationAuthDictGen)
        authTester.postUserSetupCallback = self.addReservationPermsissions
        authTester.runTest()
        readonlyFieldTester = TestAuthReadOnlyFieldChecker(self,
                                                           "reservation",
                                                           self.reservationAuthDictGen,
                                                           {"owner": "garbage"})
        readonlyFieldTester.runTest()

    def schemaDictGen(self, name):
        self.count += 1
        return {"name": "name" + str(self.count),
                "name_key": "namekey" + str(self.count),
                "namespace": "http://123/" + str(self.count),
                "community": "/scs/community/" + str(SmModelCommunity.objects.first().id) + "/",
                "description": "d1",
                "resource_type": self.helper.resource_type_path}
        
    def test_mdSchema(self):
        self.helper.setupMetadataModel()
        authTester = TestVisibleToAllEditOnlyByOwner(self,
                                                     "md_schema",
                                                     "name", "new_name",
                                                     self.schemaDictGen)
        authTester.runTest()
            
    def mdPropTypeDictGen(self, name):
        self.count += 1
        return {"name": "name" + str(self.count),
                "name_key": "namekey" + str(self.count),
                "description": "d1",
                "property_type": "INT",
                "schema": self.helper.schema_path}
       
    def test_mdPropType(self):
        self.helper.setupMetadataModel()
        self.resource = SmModelResource.objects.first()
        self.resource_path = "/scs/resource/" + str(self.resource.id) + "/"
        authTester = TestVisibleToAllEditOnlySuperuser(self,
                                                       "md_property_type",
                                                       "name", "new_name",
                                                       self.mdPropTypeDictGen)
        authTester.runTest()
            
    def smdPathDictGen(self, name):
        self.count += 1
        return {"relative_path": "path" + str(self.count),
                "repo": self.helper.repo_path,
                "resource": self.helper.resource_path}
        
    def test_mdPath(self):
        self.helper.setupMetadataModel()
        self.resource = SmModelResource.objects.first()
        self.resource_path = "/scs/resource/" + str(self.resource.id) + "/"
        authTester = TestVisibleToAllEditOnlySuperuser(self,
                                                       "md_path",
                                                       "name", "new_name",
                                                       self.smdPathDictGen)
        authTester.runTest()
            
    def mdPropDictGen(self, name):
        self.count += 1
        return {"path": "/scs/md_path/" + str(SmModelPathList.objects.first().id) + "/",
                "property_type": "/scs/md_property_type/" + str(SmModelPropertyType.objects.first().id) + "/",
                "value": str(self.count)}
       
    def test_mdProp(self):
        self.helper.setupMetadataModel()
        self.resource = SmModelResource.objects.first()
        self.resource_path = "/scs/resource/" + str(self.resource.id) + "/"
        authTester = TestVisibleToAllEditOnlySuperuser(self,
                                                       "md_property",
                                                       "value", "99",
                                                       self.mdPropDictGen)
        authTester.runTest()
            
    def repoDictGen(self, name):
        self.count += 1
        return {"name": "name" + str(self.count),
                "description": "d1",
                "resources": [self.resource_path],
                "schema": self.helper.schema_path}
   
    def test_mdRepo(self):
        self.helper.setupMetadataModel()
        self.resource = SmModelResource.objects.first()
        self.resource_path = "/scs/resource/" + str(self.resource.id) + "/"
        authTester = TestVisibleToAllEditOnlyByOwner(self,
                                                     "md_repo",
                                                     "name", "new_name",
                                                     self.repoDictGen)
        authTester.runTest()
            
    def extractorTypeDictGen(self, name):
        self.count += 1
        return {"task_type": self.helper.ttype_path,
                "name": "abc" + str(self.count),
                "name_key": "abc_key" + str(self.count),
                "schema": self.helper.schema_path,
                "resource_type": self.helper.resource_type_path,
                "description": "sdjflskdjflksdj flksj dflkjs dlfkj sldfk"
                }
        
    def test_mdExtractorType(self):
        self.helper.setupMetadataModel()
        authTester = TestVisibleToAllEditOnlySuperuser(self,
                                                       "md_extractor_type",
                                                       "name", "def",
                                                       self.extractorTypeDictGen)
        authTester.runTest()
            
    def extractorDictGen(self, name):
        self.count += 1
        return {"repo": self.helper.repo_path,
                "extractor_type": self.helper.extractor_type_path,
                "compute_resource": self.helper.resource_path}
        
    def test_mdExtractor(self):
        self.helper.setupMetadataModel()
        
        authTester = TestVisibleToAllEditOnlyByOwner(self,
                                                     "md_extractor",
                                                     "compute_resource", self.helper.resource_path,                                                               
                                                     self.extractorDictGen)
        authTester.runTest()
            
    def cloudDictGen(self, name):
        self.count += 1
        return {"name": "abc" + str(self.count),
                "name_key": "abc_key" + str(self.count),
                "cloud_type": "aws",
                "description": "sdjflskdjflksdj flksj dflkjs dlfkj sldfk"
                }
        
    def test_cloud(self):
        authTester = TestVisibleToAllEditOnlySuperuser(self,
                                                       "cloud",
                                                       "name", "def",
                                                       self.cloudDictGen)
        authTester.runTest()
    
    def cloudAccountDictGen(self, name):
        self.count += 1
        return {"name": "abc" + str(self.count),
                "name_key": "abc_key" + str(self.count),
                "cloud": self.helper.cloud_path,
                "accountInfoJson": json.dumps({"a": "b"})
                }
        
    def test_cloud_account(self):
        self.helper.setupAwsCloudModel()
        authTester = TestVisibleToAllEditOnlyByOwner(self,
                                                     "cloud_account",
                                                     "accountInfoJson", json.dumps({"key": "123"}),
                                                     self.cloudAccountDictGen)
        authTester.runTest()
        
        # Test that reading accountInfoJson just returns a message
        resp = self.helper.objGetViaRest(self.helper.cloud_account_path)
        accountInfo = resp["accountInfoJson"]
        self.assertTrue("message" in accountInfo)
        self.assertFalse("test" in accountInfo)
        
    def cloudServerDictGen(self, name):
        self.count += 1
        return {"name": "abc" + str(self.count),
                "name_key": "abc_key" + str(self.count),
                "cloud_account": self.helper.cloud_account_path,
                "container": self.helper.cloud_container_path,
                "host": "1.2.3.4"
                }
        
    def test_cloud_server(self):
        self.helper.setupAwsCloudModel()

        authTester = TestVisibleOnlyToSelf(self,
                                           "cloud_server",
                                           "host", "9.8.7.6",
                                           self.cloudServerDictGen)
        authTester.runTest()
    
        readonlyFieldTester = TestAuthReadOnlyFieldChecker(self,
                                                  "cloud_server",
                                                  self.cloudServerDictGen,
                                                  {"status": "garbage"})
        readonlyFieldTester.runTest()
    
    def file_set_DictGen(self, name):
        self.count += 1
        return {"name": "abc" + str(self.count),
                "name_key": "abc_key" + str(self.count),
                "source": "search",
                "source_info": '{"a": "b"}',
                }
        
    def test_file_set(self):
        authTester = TestVisibleOnlyToSelf(self,
                                           "file_set",
                                           "source_info", '{}',
                                           self.file_set_DictGen)
        authTester.runTest()
    
        readonlyFieldTester = TestAuthReadOnlyFieldChecker(self,
                                                  "file_set",
                                                  self.file_set_DictGen,
                                                  {"create_date_time": "2001-01-01",
                                                   "source": "abc"})
        readonlyFieldTester.runTest()
    
    def file_set_item_DictGen(self, name):
        self.count += 1
        self.file_set = SmModelFileSet(owner=User.objects.get(username="owner"),
                                       name="A",
                                       name_key="a",
                                       source="search",
                                       source_info="{}")
        self.file_set.save()
        self.file_set_path = "/scs/file_set/" + str(self.file_set.id) + "/"
        return {"resource": self.helper.resource_path,
                "file_set": self.file_set_path,
                "relative_path": "path" + str(self.count),
                }
        
    def test_file_set_item(self):
        self.helper.setupMetadataModel()
        authTester = TestVisibleOnlyToSelf(self,
                                           "file_set_item",
                                           "relative_path", "p2",
                                           self.file_set_item_DictGen)
        authTester.runTest()
    
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()