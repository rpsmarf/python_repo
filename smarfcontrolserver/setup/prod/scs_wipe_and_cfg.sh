#!/bin/bash

function smGetApiKey() {
   curl -k -s --data 'email=rpsmarf@sce.carleton.ca&password=smarF.2014' $SCS_URL/scs/authentication/ | sed -e 's/^.*apikey\": \"//' | sed -e 's/\".*$//'
}

function smSetApiKey() {
  export SMARF_AUTH_HDR="ApiKey rpsmarf:`smGetApiKey`"
}



echo "SCS_URL is $SCS_URL (should contain https)"
echo "SMARF_CERT_CHECK is $SMARF_CERT_CHECK (should be OFF)"
echo "SMARF_AUTH_HDR is $SMARF_AUTH_HDR (should be 'ApiKey rpsmarf:<value from tastypie_apikey DB table>)'"

scsInstallAndRestart.sh
scsWipeAndInit.py
sleep 2

smSetApiKey

python scs_agent_container_setup.py

for i in scs_task_type*; do
    python $i
done

scsRestart.sh