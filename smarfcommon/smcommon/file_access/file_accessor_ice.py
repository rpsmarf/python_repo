'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 12, 2014

    @author: rpsmarf
'''
import logging
import urllib.parse
import json
import smraicemsgs
from smcommon.globals.sm_global_context import smGlobalContextGet
from smcommon.exceptions.file_access_exception import SM_FS_EXC_NOT_CONN,\
    SmFileAccessException, SM_FS_EXC_ALREADY_CONN, SM_FS_EXC_CONN_FAIL
from statsd.defaults.env import statsd
from smcommon.metrics.sm_metric_names import SM_FILE_BYTES_WRITTEN_TO_ICE,\
    SM_FILE_BYTES_READ_FROM_ICE

logger = logging.getLogger(__name__)

LIMIT = 500000

class SmFileReadWriterWrapperIce(object):
    '''
    '''
    def __init__(self, fas_id, fsOps, fileId, access_mode):
        self.fasId = fas_id
        self.fileId = fileId
        self.access_mode = access_mode
        self.fsOps = fsOps

    def read(self, maxBytes):
        bytesRead = self.fsOps.readData (self.fasId, self.fileId, maxBytes)
        statsd.incr(SM_FILE_BYTES_READ_FROM_ICE, bytesRead)
        return bytesRead
    
    def write(self, data):
        dataSize = len(data)
        statsd.incr(SM_FILE_BYTES_WRITTEN_TO_ICE, dataSize)
        if dataSize <= LIMIT:
            return self.fsOps.writeData (self.fasId, self.fileId, data)
        else:
            bytesSent = 0
            while bytesSent < dataSize:
                if dataSize - bytesSent < LIMIT:
                    bytesToSend =  dataSize - bytesSent
                else:
                    bytesToSend = LIMIT
                self.fsOps.writeData (self.fasId, self.fileId, data[bytesSent:bytesSent + bytesToSend])
                bytesSent += bytesToSend
                
    def close(self, ):
        return self.fsOps.close (self.fasId, self.fileId)


class SmFileAccessorIce(object):
    '''
    classdocs
    '''

    def __init__(self, dataContainer):
        '''
        Constructor
        '''
        self.dataContainer = dataContainer
        self.fasId = None
        self.connected = False
        self.urlInfo = urllib.parse.urlparse(dataContainer.getAgentUri())
        self.ic = smGlobalContextGet().ic
        netloc = self.urlInfo.netloc
        host,port = netloc.split(":")
        proxyString = "FileOps:tcp -h " + host + " -p " + port + " -t 15000"
        #logger.debug("Creating FileAccessorIce with proxyString %s", proxyString)
        base = self.ic.stringToProxy(proxyString)
        try:
            self.fsOps = smraicemsgs.SmRemAgFileOpsPrx.checkedCast(base)  # @UndefinedVariable
        except Exception as e:
            raise Exception("Failed to connect to {}:{} (exception {})".format(host, port, str(e)))
        if not self.fsOps:
            raise RuntimeError("Invalid proxy") 
        
    def connectSession(self):
        if (self.connected): 
            raise SmFileAccessException(422, SM_FS_EXC_ALREADY_CONN, "", "Client is already connected")
        try:
            self.fasId = self.fsOps.connect(self.dataContainer.toJson(), 10000)
        except:
            raise SmFileAccessException(422, SM_FS_EXC_CONN_FAIL, "",
                                        "Failed to connect to remote agent at " 
                                        + str(self.urlInfo))
            
        self.connected = True
    
    def disconnectSession(self):
        if self.fasId:
            self.fsOps.disconnect(self.fasId)
        self.connected = False
        
    def deleteFileInfos(self, fileInfosToDelete):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, "", "Delete of paths failed because connect operation was not done")
        
        pathsToDelete = []
        for f in fileInfosToDelete:
            pathsToDelete.append(f["name"])
        try:
            self.fsOps.delete(self.fasId, json.dumps(pathsToDelete))
        except smraicemsgs.FsError as e:  # @UndefinedVariable
            raise SmFileAccessException(e.httpCodeToReturn, e.reasonCode, "<multiple>", e.description)

    def deletePaths(self, pathsToDelete):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, "<multiple>", "Delete of paths failed because connect operation was not done")
        
        try:
            self.fsOps.delete(self.fasId, json.dumps(pathsToDelete))
        except smraicemsgs.FsError as e:  # @UndefinedVariable
            raise SmFileAccessException(e.httpCodeToReturn, e.reasonCode, e.path, e.description)
        
    def list(self, path, recursiveDepth, maxEntries, lastPathReturned, showHidden):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, path, "List failed because connect operation was not done")
        
        try:
            returnListJson  = self.fsOps.list(self.fasId, path, recursiveDepth, maxEntries, lastPathReturned, showHidden)
        except smraicemsgs.FsError as e:  # @UndefinedVariable
            raise SmFileAccessException(e.httpCodeToReturn, e.reasonCode, path, e.description)
        return json.loads(returnListJson)
    
    def mkdir(self, path, makeParentFolders = False):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, path, "Mkdir failed because connect operation was not done")

        try:
            return json.loads(self.fsOps.mkdir(self.fasId, path, makeParentFolders))
        except smraicemsgs.FsError as e:  # @UndefinedVariable
            raise SmFileAccessException(e.httpCodeToReturn, e.reasonCode, path, e.description)
        
    def du(self, path, showHidden=True):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, path, "du failed because connect operation was not done")

        try:
            return json.loads(self.fsOps.du(self.fasId, path, showHidden))
        except smraicemsgs.FsError as e:  # @UndefinedVariable
            raise SmFileAccessException(e.httpCodeToReturn, e.reasonCode, path, e.description)
        
    def move(self, oldpath, newpath):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, oldpath, "Move failed because connect operation was not done")

        try:
            return json.loads(self.fsOps.move(self.fasId, oldpath, newpath))
        except smraicemsgs.FsError as e:  # @UndefinedVariable
            raise SmFileAccessException(e.httpCodeToReturn, e.reasonCode, oldpath, e.description)
        
    def open(self, path, access_mode):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, "Open of path " + path + " failed because connect operation was not done")

        if "r" in access_mode:
            fileId = self.fsOps.openForRead(self.fasId, path)
        else:
            fileId = self.fsOps.openForWrite(self.fasId, path)
                
        try:
            logger.debug("ICE open returned fileId %s for fasId %s", str(fileId), self.fasId)
            return SmFileReadWriterWrapperIce(self.fasId, self.fsOps, fileId, access_mode)
        except smraicemsgs.FsError as e:  # @UndefinedVariable
            logger.warning("Exception in open")
            raise SmFileAccessException(e.httpCodeToReturn, e.reasonCode, path, e.description)
        
    
