angular.module("rpsmarf.foldernavpane.dragdrop", [
	'rpsmarf.foldernavpane',
	'ui.bootstrap'

])

.directive('smDropAreaOld', [ '$rootScope', function($rootScope){

	return {
		restrict: 'A',
		scope: {
			entries: '=',
			resource: '=',
			smondrop: '&'
		},
		link: function($scope, $element, $attrs){
			//console.log('drop scope id ' + $scope.$id);

			$scope.$on('sm-drag-start',function(){
				console.log('drag-start scope');
				if($element.closest('foldernavpane').hasClass('sm-drag-src')){
					return;
				}
				console.log('adding class');
				$element.addClass('sm-drop-area');
				
			});

			$scope.$on('sm-drag-end',function(){
				console.log('in drag end');
				if($element.closest('foldernavpane').hasClass('sm-drag-src')){
					return;
				}
				$element.removeClass('sm-drop-area');

			});

			$element.on('dragover', function(event){
	
				if($element.closest('foldernavpane').hasClass('sm-drag-src')){
					return;
				}
				$element.removeClass('sm-drop-area');
				$element.addClass('sm-drop-allow');
				if(event.originalEvent){
					event.originalEvent.preventDefault();
					event.originalEvent.dataTransfer.dropEffect = 'move';
				}
			});

			$element.on('dragleave', function(event){

				if($element.closest('foldernavpane').hasClass('sm-drag-src')){
					return;
				}
				$element.removeClass('sm-drop-allow');
				$element.addClass('sm-drop-area');
			});

			$element.on('drop', function(event){
				console.log('droppped!');
				var e;
				if(event.originalEvent){
					e = event.originalEvent;

					e.preventDefault();
					e.stopPropagation();
				}

				//var data = e.dataTransfer.getData('text/html');
				var srcPane = angular.element('foldernavpane.sm-drag-src');

				var srcEntries = srcPane.isolateScope().entries;
				var srcResource = srcPane.isolateScope().resource;
				var dstResource = $scope.resource;

				var dstEntries = $scope.entries;
				var paneBody = srcPane.find('div.panel-body');

				
				var entry = paneBody.data('drag-object');



				var newObject = JSON.parse(JSON.stringify(entry));
				
				$scope.smondrop({srce: srcEntries, dste: dstEntries, entryObj: entry, srcResource: srcResource, dstResource: dstResource});

			});

			$scope.$on('sm-drop-success', function(){

				
				if($element.closest('foldernavpane').hasClass('sm-drag-src')){
					$element.closest('foldernavpane').removeClass('sm-drag-src');
					return;
				}

							

				$element.removeClass('sm-drop-area');
				$element.removeClass('sm-drop-allow');
			});



		}


	};

}])

.directive('smDraggableOld',['$rootScope', function($rootScope){

	return {
		restrict: 'A',
		scope: false,
		link: function($scope, $element, $attrs){

			//console.log('draggable parent scope id ' + $scope.$parent.$parent.$parent.$parent.$id);
			$element.attr('draggable', 'true');

			var pane;
			$element.on("dragstart", function(e) {
				
				$element.closest('foldernavpane').addClass('sm-drag-src');
				pane = $element.closest('div.panel-body');

				if(e.originalEvent){
					e.originalEvent.dataTransfer.setData('text/html',$element.attr('id'));
				}
				
				pane.data('drag-object', $scope.entry);


				$rootScope.$broadcast('sm-drag-start');
			});

			$element.on("dragend", function(e){
				
				pane.data('drag-object',null);
				$element.closest('foldernavpane').removeClass('sm-drag-src');
				$rootScope.$broadcast('sm-drag-end');
			});
			
			
		}
		
		
	};




}])

;