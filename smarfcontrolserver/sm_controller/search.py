from rdflib import Graph, Literal, BNode, Namespace, RDF
from rdflib.term import URIRef
from rdflib.exceptions import ParserError
from sm_controller.models import SmModelSchema, SmModelPropertyType,\
    SmModelPathList

import logging
logger = logging.getLogger(__name__)


class MetadataSearchEngine(object):
    FILTER_OPERATIONS = {
        'equals': '=',
        'lt': '<',
        'lte': '<=',
        'gt': '>',
        'gte': '>=',
    }

    def search(self, filters, repo):
        """Search for resources that meet the criteria specified by the filters
    
        User defined properties are converted into an RDF graph.  The list of filters
        is converted into a SPARQL expression that is applied to the graph, resulting
        in a list of resources.
    
        Args:
            filters: A list of filters.  Filters are of the form:
                "schema:property__operator=value" (inclusive) or "-schema:property__operator=value" (exclusive)
                Operators include "equals", "gt", "gte", "lt", and "lte"
        
        Returns:
            A list of resource ids that match the search criteria.
        """
        graph = self._build_graph(repo)
        results = graph.query(self._build_search_expression(filters))

        resource_ids = []
        for r in results:
            resource_ids.append(r.id)

        resources = SmModelPathList.objects.filter(pk__in=resource_ids)

        return resources
    
    def _build_graph(self, repo):
        g = Graph()

        self._bind_namespaces(g)

        for resource in SmModelPathList.objects.all().select_related():
            bnode = BNode()
            if str(resource.repo.id) == repo:
                g.add((bnode, RDF.ID, Literal(resource.id)))
                for p in resource.properties.all():
                    if p.property_type.property_type == 'REF':
                        t = (bnode, URIRef(p.property_type.fqdn), URIRef(p.value))
                    else:
                        t = (bnode, URIRef(p.property_type.fqdn), Literal(p.value, datatype=p.property_type.datatype))
    
                    g.add(t)
        return g

    def _build_search_expression(self, filters):
        """Create a SPARQL expression from the list of filters."""

        parsed_filters = []
        for filterx, value in filters:
            parsed_filter = self._parse_filter(filterx)
            if parsed_filter is None:
                continue

            parsed_filters.append(parsed_filter + (value,))

        query = """SELECT DISTINCT ?id
            WHERE {
                ?resource rdf:ID ?id .
            """

        var_count = 0
        for schema, propertyx, operation, exclusion, value in parsed_filters:
            query += '?resource %s:%s ?p%d .\n' % (schema, propertyx.name_key, var_count)

            filter_expression = '?p%d %s "%s"^^%s' % (var_count, operation, value, propertyx.datatype_str)
            if exclusion:
                filter_expression = '!(%s)' % filter_expression

            query += 'FILTER (%s)\n' % filter_expression
            var_count += 1
            
        query += "}"

        logger.debug(query)

        return query

    def _bind_namespaces(self, graph):
        """Create namespaces from schemas and bind to the graph."""

        for schema in SmModelSchema.objects.all():
            graph.bind(schema.name_key, Namespace(schema.namespace))

    # Parses filter of form "[-]schema:property__operation" into tuple
    # of the form (schema, property, exclusion)
    #
    # Exclusion is a boolean; True if the "-" is present at the beginning.
    #
    
    def _parse_filter(self, filter_string):
        """Parse a filter string and return a tuple used to construct the query expression."""

        exclusion = False
        try:
            if filter_string[0] == '-':
                filter_string = filter_string[1:]
                exclusion = True

            property_name = filter_string
            operation = '='

            if '__' in filter_string:
                property_name, operation = filter_string.split('__', 1)
                operation = self.FILTER_OPERATIONS[operation]

            # Filter must have schema:property format
            if ':' in filter_string:
                schema, propertyTypeWoSchema = property_name.split(':', 1)

                #self._check_property(schema, propertyTypeWoSchema)
                prop = self._get_property(schema, propertyTypeWoSchema)

                return (schema, prop, operation, exclusion)
            else:
                raise ParserError('Invalid property name format (requires schema:property).')

        except ParserError:
            raise
        except Exception:
            raise ParserError('Error parsing filter: %s' % filter_string)

    def _check_property(self, schema, propertyType):
        prop = SmModelPropertyType.objects.filter(name_key=propertyType)
        if not prop.filter(schema__name_key=schema).select_related('schema').exists():
            raise ParserError('PropertyType "%s:%s" does not exist.' % (schema, propertyType))
        
    def _get_property(self, schema, name):
        return SmModelPropertyType.objects.get(name_key=name, schema__name_key=schema)