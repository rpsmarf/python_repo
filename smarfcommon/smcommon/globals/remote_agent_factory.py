'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Nov 10, 2014

    @author: rpsmarf
'''
from smcommon.globals.envvar import smCommonGetEnvVarBool,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE
from smcommon.globals.sm_global_context import smGlobalContextGet
import smraicemsgs


class RemoteAgentFsFactory(object):
    '''
    This factory is used to return objects to access a remote filesystem.
    The currently supported options are:
    - via ICE - used in production
    - locally - used in unit testing.  In this mode the file operations all complete
    but they are performed on the local filesystem
    '''

    def __init__(self):
        '''
        Constructor
        '''
        pass
    
    def makeFsAccessor(self, remoteAgentUrl):
        pass
        
       
class RemoteAgentAsyncOperationFactory(object):
    '''
    This factory is used to return objects to perform remote operations.
    The currently supported options are:
    - via ICE - used in production
    - locally - used in unit testing.  For these versions, all requests
    are passed to a defined "handler" which will generate appropriate
    responses, record the requests received, etc...
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self.mockAsyncOpsServer = None
        self.mockSyncOpsServer = None

    def setMockAsyncOpsServer(self, mockAsyncOpsServer):
        self.mockAsyncOpsServer = mockAsyncOpsServer
    
    def setMockSyncOpsServer(self, mockSyncOpsServer):
        self.mockSyncOpsServer = mockSyncOpsServer
    
    def makeAsyncOperationObject(self, remoteAgentUrl):
        if smCommonGetEnvVarBool(SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE):
            return self.mockAsyncOpsServer
        else:
            base = smGlobalContextGet().ic.stringToProxy("SmRemAgAsyncOps:tcp -h " + remoteAgentUrl.hostname + 
                                                         " -p " + str(remoteAgentUrl.port) + " -t 30000")
            raOps = smraicemsgs.SmRemAgAsyncOpsPrx.checkedCast(base) # @UndefinedVariable  
            return raOps
    
    def makeSyncOperationObject(self, remoteAgentUrl):
        if smCommonGetEnvVarBool(SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE):
            return self.mockSyncOpsServer
        else:
            base = smGlobalContextGet().ic.stringToProxy("SmRemAgSyncOps:tcp -h " + remoteAgentUrl.hostname + 
                                                         " -p " + str(remoteAgentUrl.port) + " -t 30000")
            raOps = smraicemsgs.SmRemAgSyncOpsPrx.checkedCast(base) # @UndefinedVariable  
            return raOps
    
smRemoteAgentAsyncOperationFactory = RemoteAgentAsyncOperationFactory()
