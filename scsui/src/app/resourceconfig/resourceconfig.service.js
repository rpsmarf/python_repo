angular.module('rpsmarf.resourceconfig.service',[

])
.service('SmResourceConfig', [function(){

	var resourceSelected = false;
	var resourceId = null;

	var isResourceSelected = function(){
		return resourceSelected;
	};

	var getResourceId = function(){
		return resourceId;
	};

	var setResourceSelected = function(rid){
		resourceId = rid;
		resourceSelected = true;
	};

	var reset = function(){
		resourceId = null;
		resourceSelected= false;
	};

	return {
		isResourceSelected: isResourceSelected,
		getResourceId: getResourceId,
		setResourceSelected: setResourceSelected,
		reset: reset
	};
}]);