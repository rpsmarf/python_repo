describe( 'Task listing page (/tasklist)', function() {
	beforeEach (module('rpsmarf.config'));
	
	beforeEach(module('rpsmarf.tasklist'));
	beforeEach(module('ui.router'));
	beforeEach(module('restangular'));
	beforeEach(module('rpsmarf.task.create.service'));
	beforeEach(module('rpsmarf.user.service'));
	beforeEach(module('confirm/confirm.tpl.html'));
	beforeEach(module('simpleinput/simpleinput.tpl.html'));
	
	beforeEach(module('taskprogress/taskprogress.tpl.html'));
	//$scope, $modalInstance, taskObj, $interval,TASK_STATE_ENUM, modalTitle, interactive, $modal, TASK_STATE_FINISHED, TASK_STATE_RUNNING
	var $scope, $controller, SmTaskCreate, $state, $interval, $modalInstance, $modal, smarfurl, taskObject, $httpBackend, responseObj, prefix = 'src/app/tasklist/';
	var SmUser, $rootScope, Restangular;
	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	//init, starting, prep, running, cleanup and finished
	

	beforeEach(  inject(function(_$controller_, _$rootScope_, _$interval_, _Restangular_, SMARF_URL, _$modal_, _SmTaskCreate_, _$state_, _$httpBackend_, _SmUser_){
		smarfurl = SMARF_URL+'/scs/';
		Restangular = _Restangular_;
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
		SmTaskCreate = _SmTaskCreate_;
		$interval = _$interval_;
		$httpBackend = _$httpBackend_;
		$rootScope = _$rootScope_;
		$scope = $rootScope.$new();
		$modal = _$modal_;
		$controller = _$controller_;
		SmUser = _SmUser_;
		responseObj = JSON.parse(__html__[prefix+'tasklist.spec.json']);
		//The order by is ignored for the mock return results
		$httpBackend.expect('GET',smarfurl+'task/?aggregateParam=True&owner__id='+SmUser.getProperty('id')+'&orderByIdDesc=True').respond(200,responseObj);
		$state = {go:function(a,b){}};

		//$scope, Restangular, $modal, SmUser, $state, $interval, SmTaskCreate
		$controller('TaskListCtrl', {$scope: $scope, $modal:$modal, Restangular:Restangular, SmUser: SmUser, $state:$state , $interval:$interval, SmTaskCreate:SmTaskCreate });
		$httpBackend.flush();
		
	})) ;

	it('should create the list of tasks (for uiVisible=true) and set their appropriate info (completed tasks only) using the taskUpdate filter', inject(['taskUpdateFilter', function(taskUpdateFilter){
		//Deep copy task using parse
		var task = JSON.parse(JSON.stringify(responseObj.objects[0]));
		var rp,p;
		var paramsJson = JSON.parse(task.parametersJson);
		var resJson = JSON.parse(task.resultsJson);
		var resParams = paramsJson.resources;
		task.name = 'test name';
		task.toolName = '3-Dimensional Line Generation Tool';
		task.duration = '58s';
		task.startTimeNorm = 'Jan 23 2015 22:51';
		task.status = 'success';
		task.output = 'output';
		task.outputMessage = 'No output produced by this task';
		task.completionInfo = 'Task completed successfully';
		task.paramsJson = paramsJson;
		task.taskTypeImageUrl = 'assets/icon-default-tool.png';
		task.version = 'latest';
		
		//$scope.updateTask($scope.tasks[0]);
		expect(taskUpdateFilter(angular.copy([$scope.tasks[0]]))).toEqual([task]);
		expect($scope.tasks.length).toEqual(responseObj.objects.length-1);

		task = JSON.parse(JSON.stringify(responseObj.objects[0]));
		task.selected = true;
		task.paramsJson = paramsJson;
		//task.valParams = paramsJson.parameters;
		if(resParams && resParams.length >0){
			rp = [];
			for(i=0;i<resParams.length;i++){
				p = resParams[i];
				if(!p.name || p.name === '' && p.nature === 'compute'){
					p.name = 'Compute';
					p.resourcePathEntry = '--';
				}
				else if(p.path){
					p.resourcePathEntry = p.path;
				}
				rp.push(p);
			}

			task.resParams = rp;
		}

		
		task.alteredPaths = [{original: 'hello', 'new': 'hello2' }];
		task.hasAlteredPaths = true;

		$scope.clickTask($scope.tasks[0]);

		expect($scope.selectedTask).toEqual(task);



	}]));

	it('should set tasks as selected and deselect tasks', inject([function(){
		$scope.clickTask($scope.tasks[0]);
		expect($scope.tasks[0].selected).toBe(true);
		//expect($scope.selectedTasks).toEqual([$scope.tasks[0]]);


		$scope.clickTask($scope.tasks[1]);
		expect($scope.tasks[0].selected).toBe(false);
		expect($scope.tasks[1].selected).toBe(true);
		//expect($scope.selectedTasks).toEqual([$scope.tasks[1]]);

		//$scope.taskCheckboxClicked($scope.tasks[0],{stopPropagation:function(){}});
		//expect($scope.tasks[0].selected).toBe(true);
		//expect($scope.tasks[1].selected).toBe(true);
		//expect($scope.selectedTasks).toEqual([$scope.tasks[1],$scope.tasks[0]]);


	}]));

	it('should delete tasks', inject([function(){
		var l = $scope.tasks.length;
		var t = $scope.tasks[0];
		var found;
		angular.element('body').children().remove();
		$httpBackend.expect('DELETE',smarfurl+'task/'+$scope.tasks[0].id+'/').respond(200);
		//$scope.clickTask($scope.tasks[0]);
		$scope.deleteTasks($scope.tasks[0]);
		$rootScope.$apply();
		angular.element('body .modal-footer .btn-primary').click();

		$rootScope.$apply();
		$httpBackend.flush();
		for(var i=$scope.tasks.length-1;i>=0; i--){
			if(t.id == $scope.tasks[i].id){
				found = true;
				break;
			}
		}
		expect($scope.tasks.length).toEqual(l-1);
		expect(found).toBeFalsy();
   
	}]));

	it('should be able to rerun tasks', inject(['taskUpdateFilter', 'SmUser', function(taskUpdateFilter, SmUser){
		var updateTask = function(task, rps){
			var p;
			if(rps.length >0){
				rp = [];
				for(i=0;i<rps.length;i++){
					p = rps[i];
					if(!p.name || p.name === '' && p.nature === 'compute'){
						p.name = 'Compute';
						p.resourcePathEntry = '--';
					}
					else if(p.path){
						p.resourcePathEntry = p.path;
					}
					rp.push(p);
				}

				task.rps = rp;
			}
		};

		angular.element('body').children().remove();
		taskUpdateFilter($scope.tasks);
		var t = angular.copy($scope.tasks[0]);
		var pj = JSON.parse(t.parametersJson);
		
		var valParams = pj.parameters;
		var resParams = pj.resources;
		var uid = SmUser.getProperty('id');
		var tid= 15;
		t.selected = true;
		updateTask(t,resParams);
		
		var now = new Date();
		var newTaskName = pj.task_type_name + '_' + months[now.getMonth()].substring(0,3) + '_' + now.getDate() + '_' + now.getFullYear();

		$httpBackend.expect('POST',smarfurl+'task/',{owner:'/scs/user/'+uid+'/', task_type:'/scs/task_type/'+4+'/', status: "init", 
			parametersJson: JSON.stringify({input_ids:pj.input_ids}), name: 'mytestname'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200,{headers:function(val){ return '/scs/task/'+tid+'/'; }},{Location:'/scs/task/'+tid+'/'});
	
		//Compute resource
		$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'scs/task/'+tid+'/', resource: '/scs/resource/'+6+'/', 
			resource_type_task_type: '/scs/resource_type_task_type/'+7+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);
		
		//Dest resource
		$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'scs/task/'+tid+'/', resource: '/scs/resource/'+1+'/', 
			resource_type_task_type: '/scs/resource_type_task_type/'+6+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

		//Source resource
		$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'scs/task/'+tid+'/', resource: '/scs/resource/'+4+'/', 
			resource_type_task_type: '/scs/resource_type_task_type/'+5+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

		

		$httpBackend.expect('GET',smarfurl+'task/'+tid+'/?action=start').respond(200);

		$httpBackend.expect('GET',smarfurl+'task/?aggregateParam=True&owner__id='+SmUser.getProperty('id')+'&orderByIdDesc=True').respond(200,responseObj);

		spyOn(SmTaskCreate,'rerunTask').andCallThrough();

		spyOn($scope,'$emit');
		$scope.clickTask($scope.tasks[0]);
		expect($scope.$emit).toHaveBeenCalled();
		t = $scope.selectedTask;
		//t = $scope.updateSelectedTask(t);
		//$scope.updateSelectedTask($scope.selectedTasks[0]);
		updateTask(t,resParams);
		$scope.rerun($scope.tasks[0]);
		$rootScope.$apply();
		angular.element('body .modal-footer .btn-primary').click();
		$rootScope.$apply();

		angular.element('body .modal-body input').val('mytestname');
		angular.element('body .modal-body input').trigger('input');
		angular.element('body .modal-footer .btn-primary').click();

		expect(SmTaskCreate.rerunTask).toHaveBeenCalledWith(t, 'mytestname');
		$httpBackend.flush();
		$rootScope.$apply();
		expect($scope.$emit).toHaveBeenCalledWith('SmTaskStart',jasmine.any(Object),'mytestname', undefined);


		

	}]));

	it('should be able to cancel tasks', inject(['taskUpdateFilter', 'SmUser', function(taskUpdateFilter, SmUser){

		var l = $scope.tasks.length;
		var t = $scope.tasks[0];
		var found;
		angular.element('body').children().remove();
		$httpBackend.expect('GET',smarfurl+'task/'+$scope.tasks[0].id+'/?action=cancel').respond(200);
		//$scope.clickTask($scope.tasks[0]);
		$scope.cancelTask($scope.tasks[0]);
		$rootScope.$apply();
		angular.element('body .modal-footer .btn-primary').click();

		$rootScope.$apply();
		$httpBackend.flush();
		

	}]));

	describe('taskUpdateFilter functionality', function(){
		var tuf;
		beforeEach(inject(function(taskUpdateFilter){
			tuf = taskUpdateFilter;
		}));
		it('should set the values for progressType and progress appropriately', inject([function(){
			var task = JSON.parse(JSON.stringify(responseObj.objects[0]));
			task.state = 'running';
			task.progressJson = '{"progress":35.5}';
			var taskInput = JSON.parse(JSON.stringify(task));
			var rp,p;
			var paramsJson = JSON.parse(task.parametersJson);
			var resJson = JSON.parse(task.resultsJson);
			var resParams = paramsJson.resources;
			task.name = 'test name';
			task.toolName = '3-Dimensional Line Generation Tool';
			task.duration = '---';
			task.startTimeNorm = 'Jan 23 2015 22:51';
			task.status = 'running';
			task.output = 'progress';
			task.completionInfo = 'Task is currently running';
			task.paramsJson = paramsJson;
			task.taskTypeImageUrl = 'assets/icon-default-tool.png';
			task.version = 'latest';
			task.progressType = 'progressbar';
			task.progress = 35.5;
			task.progressInterval = jasmine.any(Object);

			expect(tuf([taskInput])).toEqual([task]);

			//task = JSON.parse(JSON.stringify(responseObj.objects[0]));
			delete task.progressInterval;
			task.state = 'running';
			task.progressJson = '{"outputSize":5212326}';
			taskInput = JSON.parse(JSON.stringify(task));
			
			task.progressType = 'outputsize';
			task.progress = 4.97 + ' MB';
			task.progressInterval = jasmine.any(Object);

			expect(tuf([taskInput])).toEqual([task]);

			delete task.progressInterval;
			task.state = 'running';
			task.progressJson = '{"runtime":4500}';
			taskInput = JSON.parse(JSON.stringify(task));
			
			task.progressType = 'runtime';
			task.progress = 1.25 + ' hr(s)';
			task.progressInterval = jasmine.any(Object);

			expect(tuf([taskInput])).toEqual([task]);
			
		}]));
	});

});

