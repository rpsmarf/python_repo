/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'rpsmarf.tasklist', [
  'rpsmarf.config',
  'restangular',
  'ui.router',
  'ui.bootstrap',
  'ngSanitize',
  'rpsmarf.user.service',
  'rpsmarf.confirm',
  'rpsmarf.task.create.service',
  'rpsmarf.interval.service',
  'rpsmarf.taskoutput',
  'rpsmarf.helpers.service',
  'rpsmarf.simpleinput'
  
])
.config(['$stateProvider',function ( $stateProvider, RestangularProvider ) {
  $stateProvider.state( 'tasklist', {
    url: '/tasklist/',
    views: {
      'main': {
        controller: 'TaskListCtrl',
        templateUrl: 'tasklist/tasklist.tpl.html'
      },
      'footer': {
        controller: 'TaskListInfoPanelCtrl',
        templateUrl: 'tasklist/tasklist.infopanel.tpl.html'
      }
    },
    data:{ pageTitle: 'Task Status' }
  })
  ;

}])
.filter('taskUpdate', ['$interval', 'Restangular', 'TASK_STATE_FINISHED', 'TASK_STATE_RUNNING','SmInterval', 'TASK_STATE_QUEUED',
 function($interval, Restangular, TASK_STATE_FINISHED, TASK_STATE_RUNNING, SmInterval, TASK_STATE_QUEUED){

  var monthNames = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

  var getTaskDuration = function(task){
    var sd = task.start_time;
    var ed = task.end_time;
    var sdm;
    var edm;
    var diff;
    var duration="";

    if(task.state === TASK_STATE_RUNNING){
      return '---';
    }

    if(sd[sd.length-1] !== 'Z'){
      sd += 'Z';
    }
    if(ed[ed.length-1] !== 'Z'){
      ed += 'Z';
    }

    sdm = Date.parse(sd);
    edm = Date.parse(ed);

    diff = edm-sdm;
    if(diff === 0)
    {
      duration = '0s';
    }
    else{
      //Hours
      duration = diff/3600000 > 1 ? Math.floor(diff/3600000) +'h': '';
      //Minutes
      diff = diff%3600000;
      duration += diff/60000 > 1 ? Math.floor(diff/60000) +'m': '';
      //Seconds
      diff = diff%60000;
      duration += diff/1000 >= 1 ? Math.ceil(diff/1000) + 's':'';
      //milliseconds
      //diff = diff%1000;
      //duration += diff > 0 ?  diff > 1000 ? '.'+diff.substring(0,3)+'s' : '.'+diff+'s' : '';
    }
    

    if(duration === '' || duration === 's' || task.state === TASK_STATE_QUEUED){
      duration = '---';
    }

    return duration;
    
  };

  var getTaskOutput = function(task){
    var progressJson;
    var size;

    if(task.state === TASK_STATE_QUEUED){
      task.outputMessage = 'No output at this time';
      return 'No output at this time';
    }
    else if(task.state !== TASK_STATE_FINISHED){
      if(task.progressInterval){
        return 'progress';
      }
      
      return 'progress';
    }
    else{
      if(task.progressInterval){
        $interval.cancel(task.progressInterval);
        //task.progress = 100;
        
      }
      
      if(task.progressJson && task.progressJson !== ''){
        progressJson = JSON.parse(task.progressJson);
        if(progressJson.outputSize){
          size = progressJson.outputSize/1024;
          if(size > 1024){
            size = (size/1024).toFixed(2) + " MB";
          }
          else{
            size = size.toFixed(2) + " KB";
          }
          task.outputMessage = size;
        }
        else{
          task.outputMessage = 'No output produced by this task';
        }
      }
      else {
        task.outputMessage = 'No output produced by this task';
      }
      
      return 'output';
    }
  };

  var getTaskStatus = function(task){
    if(task.state === TASK_STATE_FINISHED){
      if(task.completion === 'completedWithoutError'){
        return 'success';
      }
      else if(task.completion === 'completedWithError'){
        if(task.completionReason === 'cancelled'){
          return 'cancelled';
        }
        else {
          return 'fail';  
        }
        
      }
    }
    else{
      return task.state.toLowerCase();
    }
  };

  var getStartTimeNormalized = function(starttime){
    var date = new Date(starttime);
    // var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

    // var offset = date.getTimezoneOffset() / 60;
    // var hours = date.getHours();

    // newDate.setHours(hours - offset);

    return monthNames[date.getMonth()] + " " + date.getDate() + (" "+date.getFullYear()).substring(date.getFullYear().length-2) + " " + date.toTimeString().substring(0,5);  
  };

  var getTaskCompletionInfo = function(task){
    var resJson,desc='Unknown error';
    if(task.state === TASK_STATE_FINISHED){
      if(task.completion === 'completedWithError'){
        if(task.resultsJson !== ""){
          resJson = JSON.parse(task.resultsJson);
          if(resJson.description){
            desc = resJson.description;
          }
        }
        return 'Task completion failed with error message: '+desc;
      }
      else{
        return 'Task completed successfully';
      }
    }
    else if(task.state === TASK_STATE_QUEUED){
      return 'Task is queued for execution';
    }
    else{
      return 'Task is currently running';
    }
  };

  var getProgress = function(task){
    var pj,i;
    if(task.progressJson && task.progressJson !== ''){
      pj = JSON.parse(task.progressJson);
      if(pj.progress && pj.progress != -1){
        task.progressType = 'progressbar';
        return pj.progress;
      }
      else if(pj.outputSize && pj.outputSize !== -1){
        task.progressType = 'outputsize';
        return (pj.outputSize/(1024*1024)).toFixed(2) + ' MB';
      }
      else if(pj.runtime && pj.runtime !== -1){
        task.progressType = 'runtime';
        runtime = pj.runtime;
        if(runtime > 60 && runtime < 3600){
          runtime = (runtime/60).toFixed(2) + ' min(s)';
        }
        else{
          runtime = (runtime/3600).toFixed(2) + ' hr(s)';
        }
        return runtime;
        
      }
      else {
        task.progressType = 'none';
        return '-----';
      }
    }
    else{
      task.progressType = 'none';
      return '-----';
    }
  };

  var setProgressInterval = function(task){
    return $interval(function(){
      Restangular.one('task',task.id+'/?aggregateParam=True').get().then(function(response){
        var pj;
        if(task.state !== response.state){
          task.state = response.state;
          task.completion = response.completion;
          task.paramsJson = JSON.parse(response.parametersJson);
          task.end_time = response.end_time;
        }
        if(response.state === TASK_STATE_FINISHED){
          //task.progress = 100;
          $interval.cancel(task.progressInterval);
          //task.state = TASK_STATE_FINISHED;
        }
        else if(response.progressJson && response.progressJson !== ''){
          pj = JSON.parse(response.progressJson);
          if(pj.progress && pj.progress != -1){
            task.progressType = 'progressbar';
            task.progress = pj.progress;
          }
          else if(pj.outputSize && pj.outputSize !== -1){
            task.progressType = 'outputsize';
            task.progress = (pj.outputSize/(1024*1024)).toFixed(2) + ' MB';
          }
          else if(pj.runtime && pj.runtime !== -1){
            task.progressType = 'runtime';
            runtime = pj.runtime;
            if(runtime > 60 && runtime < 3600){
              runtime = (runtime/60).toFixed(2) + ' min(s)';
            }
            else{
              runtime = (runtime/3600).toFixed(2) + ' hr(s)';
            }
            task.progress = runtime;
            
          }
          else {
            task.progressType = 'none';
            task.progress = '-----';
          }
        }
        else{
          task.progressType = 'none';
          task.progress = '-----';
        }
      });
    },3000);
  };

  return function(tasks){
    var task, paramsJson;
    for(var i=tasks.length-1; i>=0; i--){
      task = tasks[i];
      
      paramsJson = JSON.parse(task.parametersJson);
      task.toolName = paramsJson.task_type_name;
      task.duration = getTaskDuration(task);
      task.startTimeNorm = getStartTimeNormalized(task.start_time);
      task.status = getTaskStatus(task);
      task.output = getTaskOutput(task);
      task.completionInfo = getTaskCompletionInfo(task);
      task.paramsJson = paramsJson;
      task.taskTypeImageUrl = task.paramsJson.taskTypeImageUrl;
      task.version = task.paramsJson.version || 'latest';

      if(task.output === 'progress' && !task.progressInterval){
        task.progress = getProgress(task);
        task.progressInterval = setProgressInterval(task);
        SmInterval.addInterval(task.progressInterval);
        
      }
    }
    
    return tasks;
  };
}])
.controller( 'TaskListCtrl', ['$scope', 'Restangular', '$modal', 'SmUser','$state', 'TASK_STATE_FINISHED', '$interval', 'SmTaskCreate', 'filterFilter', 'SmInterval', 'SmHelpers',
  function ( $scope, Restangular, $modal, SmUser, $state, TASK_STATE_FINISHED, $interval, SmTaskCreate, filterFilter, SmInterval, SmHelpers) {

  /***VARIABLES***/
  /*Scope*/
  
  $scope.selectedTask = null;
  //$scope.selectedTasks = [];
  $scope.tasks = [];
  //$scope.allTasksSelected = false;
  $scope.tasksPerPage = 20;
  $scope.currentPage = 1;
  $scope.totalTasks = 0;
  $scope.TASK_STATE_FINISHED = TASK_STATE_FINISHED;

  /*Standard*/
  var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  var taskApi = Restangular.one('task/?aggregateParam=True&owner__id='+SmUser.getProperty('id')+'&orderByIdDesc=True','');

  /***FUNCTIONS***/
  /*Scope*/
  $scope.clickTask  = function(task){
    var element;
    var rows;
    var i;
    var index;

    //var numSelected = $scope.selectedTasks.length;

   
    if(!task.selected){
      setTasksSelectedAs($scope.tasks, false);
      task.selected = true;
      
      //$scope.selectedTasks.push(task);
      $scope.selectedTask = updateSelectedTask(task);
      $scope.$emit('SmObjectSelectInfoPanel',$scope.selectedTask);
    }
    else {
      task.selected = false;
      
      //$scope.selectedTasks.splice(0,1);
      $scope.selectedTask = null;
      $scope.$emit('SmObjectSelectInfoPanel',null);
    }
    // else{
    //   setTasksSelectedAs($scope.selectedTasks, false);
    //   $scope.selectedTasks.splice(0,$scope.selectedTasks.length);
    //   task.selected = true;
    //   $scope.selectedTasks.push(task);
    //   $scope.selectedTask = updateSelectedTask(task);
    // }
    //$scope.allTasksSelected = false;
  };

  $scope.taskCheckboxClicked = function(task, $event){
    $event.stopPropagation();
    var index;
    task.selected = !task.selected;
    if(task.selected){
      $scope.selectedTasks.push(task);
      if($scope.selectAllTasks.length === 1){
        $scope.selectedTask = updateSelectedTask(task);
      }
      else{
        $scope.selectedTask = null;
      }
    }
    else{
      index = $scope.selectedTasks.indexOf(task);
      $scope.selectedTasks.splice(index,1);
      $scope.allTasksSelected = false;

      if($scope.selectedTasks.length === 1){
        $scope.selectedTask = updateSelectedTask($scope.selectedTasks[0]);
      }
      else {
        $scope.selectedTask = null;
      }
    }

  };

  $scope.selectAllTasks = function(){

    var val;
    if($scope.allTasksSelected){
      val = false;
      $scope.selectedTasks = [];
      $scope.allTasksSelected = false;
      $scope.selectedTask = null;
    }
    else{
      val = true;
      $scope.selectedTasks = $scope.tasks;
      $scope.allTasksSelected = true;
      $scope.selectedTask = null;
    }
    setTasksSelectedAs($scope.tasks, val);
    //for(i = $scope.tasks.length-1; i>=0; i--){
     //   $scope.tasks[i].selected = val;
    //}
    //$scope.selectedTask = null;
    
  };

  var setTasksSelectedAs = function(tasks,value){

    for(var i=tasks.length-1; i>=0; i--){
      tasks[i].selected = value;
    }
  };
  

  

  $scope.updateTask = function(task){
    var paramsJson = JSON.parse(task.parametersJson);
    task.name = paramsJson.task_type_name;
    task.duration = getTaskDuration(task);
    task.startTimeNorm = getStartTimeNormalized(task.start_time);
    task.status = getTaskStatus(task);
    task.output = getTaskOutput(task);
    task.completionInfo = getTaskCompletionInfo(task);
    task.parametersJson = paramsJson;

  };

  $scope.deleteTasks = function(task, index){
    var body;
   
    body = 'Delete task? (Cannot be undone)';
  
    var modalInstance = $modal.open({
      controller: 'ConfirmCtrl',
      templateUrl: 'confirm/confirm.tpl.html',
      resolve:{
        title: function(){ return 'Confirm Deletion';},
        body: function(){ return body;}
      }
    });

    modalInstance.result.then(function(){
      deleteTask(task,index);
      
    },
    function(){
      console.log('task deletion cancelled');
    });
    
  };

  $scope.rerun = function(task){
    if(!task.valParams && !task.resParams){
      task = updateSelectedTask(task);
    }
    var title = 'Rerun task configuration?';
    var body = 'This task was run with the following configuration:\n';
    var modalInstance;
    var valParams = task.valParams;
    var resParams = task.resParams;
    if(valParams && valParams.length >0){
      body += '<table class="table"><thead><tr><th>Parameter Name</th><th>Value</th></tr></thead><tbody>';
      for(i=0;i<valParams.length;i++){
        p = valParams[i];
        body += '<tr><td>'+p.name+'</td><td>'+p.value+'</td></tr>';
      }
      body += '</tbody></table>';
    }
    if(resParams && resParams.length >0){
      body += '<table class="table"><thead><tr><th>Parameter Name</th><th>Resource Name</th><th>Path</th></tr></thead><tbody>';
      for(i=0;i<resParams.length;i++){
        p = resParams[i];
        if(p.nature === 'compute'){
          body += '<tr><td>'+p.name+'</td><td>'+p.resourceName+'</td><td>--</td></tr>';
        }
        else{
          body += '<tr><td>'+p.name+'</td><td>'+p.resourceName+'</td><td>'+p.path+'</td></tr>';
        }
      }
      body += '</tbody></table>';      
    }

    modalInstance = $modal.open({
      controller: 'ConfirmCtrl',
      templateUrl: 'confirm/confirm.tpl.html',
      resolve:{
        title: function(){return title;},
        body: function(){return body;}
      }
    });

    modalInstance.result.then(function(){
      var now = new Date();
      var nameModal = $modal.open({
        controller: 'SimpleInputCtrl',
        templateUrl: 'simpleinput/simpleinput.tpl.html',
        resolve: {
          name: function(){return task.toolName + '_' + months[now.getMonth()].substring(0,3) + '_' + now.getDate() + '_' + now.getFullYear();}
        }
      });
    
      nameModal.result.then(function(name){
        var p = SmTaskCreate.rerunTask(task,name);
        p.then(function(response){
          $scope.$emit('SmTaskStart',Restangular.one('task',response.taskId+'/'),name, response.interactive);
          //Shouldn't be needed but just in case
          SmTaskCreate.reset();
          taskApi.get().then(function(response){
            $scope.totalTasks = response.meta.total_count;
            $scope.tasks = filterFilter(response.objects,function(val, index){ return val.uiVisible;});

          });
        },function(error){
          SmHelpers.handleErrorOnTaskStart(error, {toolName: task.toolName, taskName: task.name,interactive: task.paramsJson.interactive},$scope);
        });
      });
      
    }, function(){
      console.log('rerun was cancelled');
    });

  };

  $scope.reconfigure = function(task, $event){
    if(task.state != TASK_STATE_FINISHED){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }

    var modalInstance = $modal.open({
      controller: 'ConfirmCtrl',
      templateUrl: 'confirm/confirm.tpl.html',
      resolve:{
        title: function(){return 'Confirm';},
        body: function(){return 'Do you want to reconfigure this task? This will take you to another page.';}
      }
    });

    modalInstance.result.then(function(){
      var p = SmTaskCreate.reconfigureTask(task);
    

      p.then(function(){
        var tt = SmTaskCreate.getTaskType();
        $state.go('taskstart',{tooltask_namekey:tt.name_key});
      });

    },function(){
      console.log('reconfigured cancelled.');
    });
    
  };

  $scope.pageChanged = function(){
    console.log($scope.currentPage);
    SmInterval.stopAllIntervals();
    Restangular.one('task/?aggregateParam=True&owner__id='+SmUser.getProperty('id')+'&orderByIdDesc=True&limit=20&offset='+(($scope.currentPage-1)*20),'').get().then(function(response){
      $scope.totalTasks = response.meta.total_count;
      $scope.tasks = filterFilter(response.objects,function(val, index){ return val.uiVisible;});

    });
  };

  $scope.viewTaskOutput = function(task, $event){
    if(task.state != TASK_STATE_FINISHED){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    $modal.open({
      controller: 'TaskOutputCtrl',
      templateUrl: 'taskoutput/taskoutput.tpl.html',
      size: 'lg',
      resolve: {
        task: function(){ return task; }
      }
    });
  };

  $scope.cancelTask = function(task){
    var modalInstance = $modal.open({
      controller: 'ConfirmCtrl',
      templateUrl: 'confirm/confirm.tpl.html',
      resolve:{
        title: function(){ return 'Confirm Cancellation';},
        body: function(){ return 'Are you sure you want to cancel this task? (may take a few minutes)';}
      }
      
    });

    modalInstance.result.then(function(){
      Restangular.one('task/'+task.id+'/?action=cancel').get().then(function(){
        console.log('task cancelled');
      });
    }, function(){
      console.log('task cancellation cancelled');
    });
  };
  /*STANDARD VARIABLES*/

  var deleteTask = function(task,index){
    if(task.progressInterval){
      $interval.cancel(task.progressInterval);
    }
    Restangular.one('task',task.id+'/').remove().then(function(){
      $scope.tasks.splice(index,1);
    });
  };

  var updateSelectedTask = function(task){
    if(!task.paramsJson){
      task.paramsJson = JSON.parse(task.parametersJson);
    }
    var valParams = task.paramsJson.parameters;
    var resParams = task.paramsJson.resources;
    var inputIds = task.paramsJson.input_ids;
    var vp, rp, p, i, resJson, key, val;

    if(valParams && valParams.length >0){
      vp = [];
      for(i=0;i<valParams.length;i++){
        p = valParams[i];
        vp.push(p);
      }

      task.valParams = vp;
    }
    if(resParams && resParams.length >0){
      rp = [];
      for(i=0;i<resParams.length;i++){
        p = resParams[i];
        if(!p.name || p.name === '' && p.nature === 'compute'){
          p.name = 'Compute';
          p.resourcePathEntry = '--';
        }
        else if(p.path){
          p.resourcePathEntry = p.path;
        }
        rp.push(p);
      }

      task.resParams = rp;
    }

    if(task.resultsJson){
      resJson = JSON.parse(task.resultsJson);
      if(resJson.description){
        task.hasErrors = true;
        task.errorMessage = 'An error occurred during task execution: '+ resJson.description;
      }
      if(resJson.alteredPaths){
        task.alteredPaths = [];
        for(key in resJson.alteredPaths){
          val = resJson.alteredPaths[key];
          task.alteredPaths.push({original: key, 'new': val});
        }

        if(task.alteredPaths.length > 0){
          task.hasAlteredPaths = true;
        }
        
      }
    }

    return task;

  };

  

  /***EXE***/
  taskApi.get().then(function(response){
    $scope.totalTasks = response.meta.total_count;
    $scope.tasks = filterFilter(response.objects,function(val, index){ return val.uiVisible;});

  });
  
}])
.controller('TaskListInfoPanelCtrl', ['$scope', 'Restangular', '$modal', 'SmUser','$state', 'TASK_STATE_FINISHED', '$interval', 'SmTaskCreate', 'filterFilter', 'SmInterval',
  function ( $scope, Restangular, $modal, SmUser, $state, TASK_STATE_FINISHED, $interval, SmTaskCreate, filterFilter, SmInterval) {
    /***SCOPE VARIABLES***/
    $scope.selectedTask = null;
    //$scope.infoPanel = {collapsed: true};

    /***SCOPE FUNCTIONS***/
    $scope.infoPanelCollapseToggle = function(){
      $scope.infoPanel.collapsed = !$scope.infoPanel.collapsed;
      $scope.$emit('SmInfoPanelToggle', $scope.infoPanel.collapsed);
    };

    /***EXE***/
    $scope.$on('SmInfoPanelUpdate', function(event, selectedTask){
      $scope.selectedTask = selectedTask;

      if(!$scope.selectedTask){
        $scope.infoPanel.collapsed = true;
        $scope.$emit('SmInfoPanelToggle', $scope.infoPanel.collapsed);
      }
    });
  }
 ]);



