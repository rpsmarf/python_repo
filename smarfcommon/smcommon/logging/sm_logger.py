'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''

import logging.config
import tempfile
from os.path import os
import json


def smLoggerGetRootLogger():
    return logging.getLogger("")
 
DEFAULT_LOGGING_DICT = {
               'version': 1,
               'disable_existing_loggers': False,
               'formatters': 
               {
                    'verbose': 
                    {
                        'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
                        'datefmt': "%d/%b/%Y %H:%M:%S"
                    },
               },
               'handlers': 
               {
                    'console': 
                    {
                         'level': 'CRITICAL',
                         'class': 'logging.StreamHandler',
                         'formatter': 'verbose'
                    },
                    "file_handler": 
                    {
                         "class": "logging.handlers.RotatingFileHandler",
                         "level": "DEBUG",
                         "formatter": "verbose",
                         "filename": "/tmp/common_unittest.log",
                         "maxBytes": 10485760,
                         "backupCount": 20,
                         "encoding": "utf8"
                    },
               },
               'loggers': {
                           '': 
                           {
                               'handlers': ['console', 'file_handler'],
                               'propagate': True,
                               'level': 'DEBUG',
                           },
                          }
              }

FIRST_TIME = True


def smLoggingGetForTest():
    unitestLoggingPath = os.path.join(tempfile.gettempdir(), "unittestLogging.conf")
    if (not os.path.exists(unitestLoggingPath)): # Write file
        loggingDict = DEFAULT_LOGGING_DICT
        with open(unitestLoggingPath, 'w') as the_file:
            the_file.write(json.dumps(loggingDict, sort_keys=True, indent=4, separators=(',', ': ')))
        print("Saving unittest logging config to " + unitestLoggingPath)
    else:
        with open(unitestLoggingPath, 'r') as the_file:
            data = the_file.read(100000)
        loggingDict = json.loads(data)
        global FIRST_TIME
        if (FIRST_TIME):
            print("Loading unittest logging config from " + unitestLoggingPath)
            FIRST_TIME = False
    return loggingDict


def smLoggingSetupForTest():
    loggingDict = smLoggingGetForTest()
                              
    logging.config.dictConfig(loggingDict)
    logging.captureWarnings(True)

