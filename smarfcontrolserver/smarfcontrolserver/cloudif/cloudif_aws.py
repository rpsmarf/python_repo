'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 23, 2015

    @author: rpsmarf
'''
import logging
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_ID,\
    SCS_CLOUDIF_HOST, SCS_CLOUDIF_STATE, \
    SCS_CLOUDIF_STATE_RUNNING, SCS_CLOUDIF_AWS_PARAM_IMAGE_ID,\
    SCS_CLOUDIF_AWS_PARAM_KEYPAIR, SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID,\
    SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY,\
    SCS_CLOUDIF_AWS_PARAM_REGION_NAME, SCS_CLOUDIF_AWS_PARAM_INSTANCE_TYPE,\
    SCS_CLOUDIF_STATE_BOOTING, SCS_CLOUDIF_STATE_STOPPING,\
    SCS_CLOUDIF_STATE_STOPPED, SCS_CLOUDIF_STATE_ERROR 
import boto3
import botocore.exceptions

logger = logging.getLogger(__name__)


scsAwsStateToScsStateDict = {"pending": SCS_CLOUDIF_STATE_BOOTING,
                             "running": SCS_CLOUDIF_STATE_RUNNING,
                             "shutting-down": SCS_CLOUDIF_STATE_STOPPING,
                             "terminated": SCS_CLOUDIF_STATE_STOPPED,
                             "stopping": SCS_CLOUDIF_STATE_STOPPING,
                             "stopped": SCS_CLOUDIF_STATE_STOPPED}


class ScsCloudifAws(object):
    '''
    This implements the Amazon Web Services (e.g. EC2) version of the cloud interface
    to allow AWS servers to be able to be created and destroyed. 
    '''
    def __init__(self, cloud_dict, cloud_account_dict):
        '''
        Constructor
        '''
        k = SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY
        self.session = boto3.session.Session(aws_access_key_id=cloud_account_dict[SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID],
                                             aws_secret_access_key=cloud_account_dict[k],
                                             region_name=cloud_dict[SCS_CLOUDIF_AWS_PARAM_REGION_NAME],
                                             )
        self.ec2 = self.session.resource('ec2')
    
    def getScsState(self, state):
        if state not in scsAwsStateToScsStateDict:
            logger.warning("Unexpected state from AWS: %s", state)
            return SCS_CLOUDIF_STATE_ERROR
        return scsAwsStateToScsStateDict[state]
    
    def _makeDictFromInstance(self, inst):
        serverInfoDict = {SCS_CLOUDIF_ID: inst.id, 
                          SCS_CLOUDIF_HOST: inst.public_dns_name, 
                          SCS_CLOUDIF_STATE: self.getScsState(inst.state["Name"])}
        
        return serverInfoDict

    def create_server(self, paramDict):
        try:
            inst = self.ec2.create_instances(ImageId=paramDict[SCS_CLOUDIF_AWS_PARAM_IMAGE_ID],
                                             MinCount=1,
                                             MaxCount=1,
                                             KeyName=paramDict[SCS_CLOUDIF_AWS_PARAM_KEYPAIR],
                                             InstanceType=paramDict[SCS_CLOUDIF_AWS_PARAM_INSTANCE_TYPE]
                                             )
            
            self.ec2.create_tags(Resources=[inst[0].id], Tags=[{'Key': "rpsmarf", 'Value': "True"}])
            return self._makeDictFromInstance(inst[0])
        except Exception:
            logger.exception("Exception during create_server on AWS")
            raise
    
    def terminate_server(self, serverId):
        instance = self.ec2.Instance(serverId)
        try:
            instance.stop()
        except botocore.exceptions.ClientError:  # May occur due to terminate before running
            pass
        instance.terminate()
    
    def get_server_list(self):
        instances = self.ec2.instances.all()
        serverList = []
        for inst in instances:
            if inst.tags is not None:
                for tag in inst.tags:
                    if tag["Key"] == "rpsmarf":
                        serverList.append(self._makeDictFromInstance(inst))
                        break
                
        return serverList
    
    def get_server_info(self, serverId):
        inst = self.ec2.Instance(serverId)
        return self._makeDictFromInstance(inst)
    
    def is_server_up(self, serverId, serverInfoDict=None):
        inst = self.ec2.Instance(serverId)
        return inst.state["Name"] == "running"
    
    