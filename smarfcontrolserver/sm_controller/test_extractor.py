'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on August 17, 2015

    @author: rpsmarf
'''

import unittest
from django.test.testcases import TransactionTestCase
from sm_controller.test_helper import ScsTestHelper
from sm_controller.signal_handlers import smTriggerSignalInstall
import logging
from smcommon.globals.sm_copy_controller import SmCopyController
from smcommon.globals.sm_global_context import smGlobalContextInit
from smcommon.globals.remote_agent_factory import smRemoteAgentAsyncOperationFactory
from sm_controller.api_task import SmTaskUtilsAsyncResps
from smcommon.task_runners.mock_async_ops_server import SmMockAsyncOpsServer,\
    SmMockOpHandlerBase
import json
import threading
from smcommon.task_runners.task_runner import TASK_STATE_SUCCESS
import time
from sm_controller.api_extract import SCS_API_EXTRACTION_RESULT_OK,\
    SCS_API_EXTRACTION_RESULT_RUNNING, SCS_API_EXTRACTION_RESULT_CANCELLED,\
    SCS_API_EXTRACTION_STATE_DONE, SCS_API_EXTRACTION_STATE_EXTRACTING,\
    SCS_API_EXTRACTION_RESULT_FAILED, SCS_API_EXTRACTION_STATE_RUNNING,\
    SCS_API_EXTRACTION_STATE_PURGING, SCS_API_EXTRACTION_STATE_SCANNING,\
    scsApiExtractRestartExtract, SCS_API_EXTRACTION_STATE_CANCELLING
import os
from smcommon.globals.envvar import SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE,\
    smCommonClearEnvVar
from smarfcontrolserver.init.envvar import SCS_ENV_VAR_EXTRACT_DONE_TIMEOUT,\
    SCS_ENV_VAR_NUM_FILES_PER_JOB
from smarfcontrolserver.utils.mailer import ScsMockMailer, scsSetGlobalMailer,\
    EXTRACTION_SUCCESSFUL_SUBJECT, EXTRACTION_FAILURE_SUBJECT

logger = logging.getLogger(__name__) 
smTriggerSignalInstall()

SM_MOCK_OP_HANDLER_THREAD_COUNT = 1


_testObject = None


class SmExtractTestMockOpHandlerResponderThread(threading.Thread):
    
    def __init__(self, smMockOpHandlerResponder):
        global SM_MOCK_OP_HANDLER_THREAD_COUNT
        threading.Thread.__init__(self)
        self.name = "mockRespThread-" + str(SM_MOCK_OP_HANDLER_THREAD_COUNT)
        SM_MOCK_OP_HANDLER_THREAD_COUNT += 1 
        self.smMockOpHandlerResponder = smMockOpHandlerResponder
        
    def run(self):
        self.smMockOpHandlerResponder._runInThread()


class SmExtractTestMockOpHandlerResponder(SmMockOpHandlerBase):
    
    def __init__(self, smApiAsyncResps, opId, opType, opParamsJson):
        SmMockOpHandlerBase.__init__(self, smApiAsyncResps)
        self.quit = False
        self.opId = opId
        self.opType = opType
        logger.debug("Got params: %s", opParamsJson)
        self.opParamsDict = json.loads(opParamsJson)
        self.thread = SmExtractTestMockOpHandlerResponderThread(self)
        self.result_to_send_back = TASK_STATE_SUCCESS
        self.helper = _testObject.helper
        self.testObject = _testObject
            
    def _runInThread(self):
        logger.debug("Thread started")
        if self.testObject.skipDone:
            logger.debug("Exiting thread because skipDone is set")
            return
        
        # Post the metadata to the URL provided
        data_file = self.helper.tempFolder.mkAbsolutePath("f1")
        args = self.opParamsDict["taskSpecificParamDict"]["args"]
        data = {}
        path_list = args[3]["dad"]["path"]
        prefix = args[2]["value"]
        for path_with_prefix in path_list:
            path = path_with_prefix.replace(prefix, "", 1)
            data[path] = {"string_prop": path}
        url_path = args[1]["value"].replace("http://localhost", "")
        
        self.helper.tempFolder.writeFile("f1", json.dumps({"data": data}))
        with open(data_file, "rb") as fp:
            resp = self.testObject.client.post(url_path,
                                    {'properties': fp})    
        self.testObject.assertEqual(resp.status_code, 200, resp.content)
        self.announceDone(self.result_to_send_back)        
        logger.debug("Thread finished")
        
    def startOp(self):
        logger.debug("Handling request with json: %s", json.dumps(self.opParamsDict))
        
        if self.testObject.mockHandlerExceptions > 0:
            self.testObject.mockHandlerExceptions -= 1
            raise Exception("Exception generated because self.testObject.mockHandlerExceptions was set")
            
        if self.testObject.allowMockHandlerRuns > 0:
            self.testObject.allowMockHandlerRuns -= 1
            
        if self.testObject.allowMockHandlerRuns <= 0:
            logger.debug("Pausing mock handler")
            while self.testObject.allowMockHandlerRuns <= 0:
                time.sleep(0.1)
            logger.debug("Resuming mock handler")
         
        self.thread.start() # Start the thread
    
    def cancelOp(self):
        self.quit = True
    
    def getStatus(self):
        return json.dumps(self.progressDict)
                
                
class ExtractorTest(TransactionTestCase):

    def setUp(self):
        self.helper = ScsTestHelper(self)
        global _testObject
        _testObject = self
        self.helper.setupModel()
        self.helper.setupRestAuth("root")
        self.helper.setupMetadataModel()
        self.copyController = SmCopyController(["111", "testagentid"])
        smGlobalContextInit(self.copyController)
        asyncRespObj = SmTaskUtilsAsyncResps()
        mockRaServer = SmMockAsyncOpsServer(asyncRespObj)
        mockRaServer.handlerClass = SmExtractTestMockOpHandlerResponder
        smRemoteAgentAsyncOperationFactory.setMockAsyncOpsServer(mockRaServer)
        self.helper.tempFolder.mkdir("xxx")
        os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE] = "True"
        self.allowMockHandlerRuns = 1000
        self.mockHandlerExceptions = 0
        self.skipDone = False
        
    def tearDown(self):
        self.helper.tearDown()
        TransactionTestCase.tearDown(self)
        smCommonClearEnvVar()
        
    def waitUntilExtractorState(self, extractor_path, expected_state, expected_result=None):    
        endTime = time.time() + 5.0
        resp = {"state": "unset"}
        while time.time() < endTime:
            try:
                resp = self.helper.objGetViaRest(extractor_path)
                logger.debug("Waiting for %s, got %s", expected_state, json.dumps(resp))
                if resp["state"] == expected_state:
                    break
                time.sleep(0.1)
            except:
                logger.exception("Error in get")    
        self.assertEqual(resp["state"], expected_state)
        if expected_result is not None:
            self.assertEqual(resp["last_extract_result"], expected_result, json.dumps(resp))

    def test_extractCycle(self):
        # Check properties there
        resp = self.helper.objGetViaRest("/scs/md_property/?repo=" 
                                         + self.helper.repo_path)
        self.assertEqual(4, len(resp["objects"]))
        self.waitUntilExtractorState(self.helper.extractor_path, "init")
        
        # Do rebuild with 0 files - expect no properties
        self.helper.objGetViaRest(self.helper.extractor_path + "/rebuild/")
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_DONE,
                                     SCS_API_EXTRACTION_RESULT_OK)
    
        resp = self.helper.objGetViaRest("/scs/md_property/?repo=" 
                                         + self.helper.repo_path)
        self.assertEqual(0, len(resp["objects"]))
        
        # Setup for 3 files
        self.helper.tempFolder.mktree(["xxx/a.txt",
                                       "xxx/b/",
                                       "xxx/b/b.txt",
                                       "xxx/c/",
                                       "xxx/c/d/",
                                       "xxx/c/d/e.txt"])
        # Do rebuild - expect 3 properties
        self.helper.objGetViaRest(self.helper.extractor_path + "/rebuild/")
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_DONE,
                                     SCS_API_EXTRACTION_RESULT_OK)

        # Check that property objects created
        resp = self.helper.objGetViaRest("/scs/md_property/?path__repo=" 
                                         + str(self.helper.repo.id))
        self.assertEqual(3, len(resp["objects"]))
        
        # Check that path objects created
        resp = self.helper.objGetViaRest("/scs/md_path/?repo=" 
                                         + str(self.helper.repo.id))
        self.assertEqual(3, len(resp["objects"]), json.dumps(resp))
        for path_object in resp["objects"]:
            self.assertEqual("loaded", path_object["state"])
                       
    def test_extractStatesAndProgress(self):
        # Setup for 3 files
        self.helper.tempFolder.mktree(["xxx/a.txt",
                                       "xxx/b/",
                                       "xxx/b/b.txt",
                                       "xxx/c/",
                                       "xxx/c/d/",
                                       "xxx/c/d/e.txt"])
        
        # Do one file per extract job
        os.environ[SCS_ENV_VAR_NUM_FILES_PER_JOB] = "1"
        
        # Do rebuild - expect 3 properties
        self.allowMockHandlerRuns = 0 # Stop mock handler from completing
        self.helper.objGetViaRest(self.helper.extractor_path + "/rebuild/")
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_EXTRACTING,
                                     SCS_API_EXTRACTION_RESULT_RUNNING)
    
        # Check that 3 files were found, but 0 processed
        resp = self.helper.objGetViaRest(self.helper.extractor_path)
        progressDict = json.loads(resp["progressJson"])
        self.assertEqual(3, progressDict["files_to_process"])
        self.assertEqual(0, progressDict["files_processed"])
        self.assertTrue("files_with_no_metadata_at_done" not in progressDict)
        
        self.allowMockHandlerRuns = 1000 # Allow mock handler to complete
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_DONE,
                                     SCS_API_EXTRACTION_RESULT_OK)
        resp = self.helper.objGetViaRest(self.helper.extractor_path)
        self.assertEqual(resp["last_extract_errors"], "")
    
        # Check that 3 files were found, and 3 processed
        progressDict = json.loads(resp["progressJson"])
        self.assertEqual(3, progressDict["files_to_process"], resp["progressJson"])
        self.assertEqual(3, progressDict["files_processed"], resp["progressJson"])
        self.assertEqual(0, progressDict["files_with_no_metadata_at_done"], resp["progressJson"])

    def test_update(self):
        '''
        Test that if new files are added, an update operation will add metrics for them  
        '''
        self.helper.tempFolder.mktree(["xxx/a.txt",
                                       "xxx/b.txt",
                                       "xxx/c.txt"])
        self.helper.objGetViaRest(self.helper.extractor_path + "/rebuild/")
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_DONE,
                                     SCS_API_EXTRACTION_RESULT_OK)
        
    def test_extractCancel(self):
        # Setup for 3 files
        self.helper.tempFolder.mktree(["xxx/a.txt",
                                       "xxx/b/",
                                       "xxx/b/b.txt",
                                       "xxx/c/",
                                       "xxx/c/d/",
                                       "xxx/c/d/e.txt"])
        
        # Do rebuild - expect 3 properties
        self.allowMockHandlerRuns = 0 # Stop mock handler from completing
        self.helper.objGetViaRest(self.helper.extractor_path + "/rebuild/")
        self.waitUntilExtractorState(self.helper.extractor_path, 
                                     SCS_API_EXTRACTION_STATE_EXTRACTING,
                                     SCS_API_EXTRACTION_RESULT_RUNNING)
        resp = self.helper.objGetViaRest(self.helper.extractor_path)
    
        # Check that 3 files were found, but 0 processed
        progressDict = json.loads(resp["progressJson"])
        self.assertEqual(3, progressDict["files_to_process"])
        self.assertEqual(0, progressDict["files_processed"])
        self.assertTrue("files_with_no_metadata_at_done" not in progressDict)
    
        # Cancel the extraction
        self.helper.objGetViaRest(self.helper.extractor_path + "/cancel/")
        resp = self.helper.objGetViaRest(self.helper.extractor_path)
        self.assertEqual(SCS_API_EXTRACTION_STATE_CANCELLING, resp["state"])
        self.allowMockHandlerRuns = 1 # Allow mock handler to complete
        for _ in range(10):
            resp = self.helper.objGetViaRest(self.helper.extractor_path)
            if resp["state"] == SCS_API_EXTRACTION_STATE_DONE:
                break
            time.sleep(0.1)

        self.assertEqual(SCS_API_EXTRACTION_STATE_DONE, resp["state"])
        self.assertEqual(SCS_API_EXTRACTION_RESULT_CANCELLED, resp["last_extract_result"])
        endTime = resp["last_extract_end"]
        
        # Check that the end time does not change
        resp = self.helper.objGetViaRest(self.helper.extractor_path)

        self.assertEqual(resp["last_extract_end"], endTime, "Prev end time is " + endTime)
        self.assertEqual(SCS_API_EXTRACTION_STATE_DONE, resp["state"])
        self.assertEqual(SCS_API_EXTRACTION_RESULT_CANCELLED, resp["last_extract_result"])

    def test_remoteAgentFailures(self):
        # Test exception from RA
        self.mockHandlerExceptions = 1 # generate a single exception - extract will recover
        # Setup for 3 files
        self.helper.tempFolder.mktree(["xxx/a.txt",
                                       "xxx/b/",
                                       "xxx/b/b.txt",
                                       "xxx/c/",
                                       "xxx/c/d/",
                                       "xxx/c/d/e.txt"])
        # Do rebuild - expect 3 properties
        self.helper.objGetViaRest(self.helper.extractor_path + "/rebuild/")
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_DONE,
                                     SCS_API_EXTRACTION_RESULT_OK)
        resp = self.helper.objGetViaRest(self.helper.extractor_path)
        progressDict = json.loads(resp["progressJson"])
        self.assertEqual(3, progressDict["files_to_process"], resp["progressJson"])
        self.assertEqual(3, progressDict["files_processed"], resp["progressJson"])
        self.assertEqual(0, progressDict["files_with_no_metadata_at_done"], resp["progressJson"])
    
        # Check that property objects created
        resp = self.helper.objGetViaRest("/scs/md_property/?path__repo=" 
                                         + str(self.helper.repo.id))
        self.assertEqual(3, len(resp["objects"]))
            
        # Test exception from RA
        self.mockHandlerExceptions = 2 # generate a double exception - extract will fail
        # Do rebuild - expect 3 properties
        self.helper.objGetViaRest(self.helper.extractor_path + "/rebuild/")
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_DONE,
                                     SCS_API_EXTRACTION_RESULT_FAILED)
        resp = self.helper.objGetViaRest(self.helper.extractor_path)
        progressDict = json.loads(resp["progressJson"], resp["progressJson"])
        self.assertEqual(3, progressDict["files_to_process"], resp["progressJson"])
        self.assertEqual(3, progressDict["files_processed"], resp["progressJson"])
        self.assertEqual(3, progressDict["files_with_no_metadata_at_done"], resp["progressJson"])
        self.assertTrue("error" in resp["last_extract_errors"].lower(), resp["last_extract_errors"])
    
        # Check that property objects not created
        resp = self.helper.objGetViaRest("/scs/md_property/?path__repo=" 
                                         + str(self.helper.repo.id))
        self.assertEqual(0, len(resp["objects"]))
            
        # Test timeout from RA
        self.skipDone = True # Stop mock handler from completing
        os.environ[SCS_ENV_VAR_EXTRACT_DONE_TIMEOUT] = "0.2"
        # Do rebuild - expect 3 properties
        self.helper.objGetViaRest(self.helper.extractor_path + "/rebuild/")
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_DONE,
                                     SCS_API_EXTRACTION_RESULT_FAILED)
        resp = self.helper.objGetViaRest(self.helper.extractor_path)
        progressDict = json.loads(resp["progressJson"], resp["progressJson"])
        self.assertEqual(3, progressDict["files_to_process"], resp["progressJson"])
        self.assertEqual(3, progressDict["files_processed"], resp["progressJson"])
        self.assertEqual(3, progressDict["files_with_no_metadata_at_done"], resp["progressJson"])
        self.assertTrue("timeout" in resp["last_extract_errors"].lower(), resp["last_extract_errors"])
        
    def test_scsRestart(self):
        # State is init - no start expected
        self.assertFalse(scsApiExtractRestartExtract(self.helper.extractor))
        
        # State is purging - start expected
        self.helper.extractor.state = SCS_API_EXTRACTION_STATE_PURGING
        self.assertTrue(scsApiExtractRestartExtract(self.helper.extractor))
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_DONE,
                                     SCS_API_EXTRACTION_RESULT_OK)

        # State is purging - start expected
        self.helper.extractor.state = SCS_API_EXTRACTION_STATE_SCANNING
        self.assertTrue(scsApiExtractRestartExtract(self.helper.extractor))
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_DONE,
                                     SCS_API_EXTRACTION_RESULT_OK)
        
        # State is purging - start expected
        self.helper.extractor.state = SCS_API_EXTRACTION_STATE_EXTRACTING
        self.assertTrue(scsApiExtractRestartExtract(self.helper.extractor))
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_DONE,
                                     SCS_API_EXTRACTION_RESULT_OK)
        
        # State is purging - start expected
        self.helper.extractor.state = SCS_API_EXTRACTION_STATE_RUNNING
        self.assertTrue(scsApiExtractRestartExtract(self.helper.extractor))
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_DONE,
                                     SCS_API_EXTRACTION_RESULT_OK)
        
        # State is done - no start expected
        self.helper.extractor.state = SCS_API_EXTRACTION_STATE_DONE
        self.assertFalse(scsApiExtractRestartExtract(self.helper.extractor))
        
    def test_email(self):
        mockMailer = ScsMockMailer()
        scsSetGlobalMailer(mockMailer)
        
        # Enable email sending
        self.helper.extractor.emailWhenDone = True
        self.helper.extractor.save()
        
        # Setup for 3 files
        self.helper.tempFolder.mktree(["xxx/a.txt",
                                       "xxx/b/",
                                       "xxx/b/b.txt",
                                       "xxx/c/",
                                       "xxx/c/d/",
                                       "xxx/c/d/e.txt"])
        # Do successful extraction
        self.helper.objGetViaRest(self.helper.extractor_path + "/rebuild/")
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_DONE)
        resp = self.helper.objGetViaRest(self.helper.extractor_path)
        self.assertEqual(SCS_API_EXTRACTION_RESULT_OK, resp["last_extract_result"], 
                         "Resp is :" + json.dumps(resp))
        
        # Check email recorded
        self.assertEqual(1, len(mockMailer.email_sent_list))
        email = mockMailer.email_sent_list[0]
        del mockMailer.email_sent_list[0]
        self.assertEqual(email["subject"], EXTRACTION_SUCCESSFUL_SUBJECT)
        self.assertTrue("successfully" in email["body"])
        self.assertTrue("a@b.com" in email["email"])
                
        # Do failed extraction
        self.mockHandlerExceptions = 2 # generate exception on first and second try - extract will fail
        self.helper.objGetViaRest(self.helper.extractor_path + "/rebuild/")
        self.waitUntilExtractorState(self.helper.extractor_path, SCS_API_EXTRACTION_STATE_DONE)
        resp = self.helper.objGetViaRest(self.helper.extractor_path)
        self.assertEqual(SCS_API_EXTRACTION_RESULT_FAILED, resp["last_extract_result"], 
                         "Resp is :" + json.dumps(resp))
        
        # Check email recorded
        self.assertEqual(1, len(mockMailer.email_sent_list))
        email = mockMailer.email_sent_list[0]
        del mockMailer.email_sent_list[0]
        self.assertEqual(email["subject"], EXTRACTION_FAILURE_SUBJECT)
        self.assertTrue("failed" in email["body"])
        self.assertTrue("a@b.com" in email["email"])
        
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
