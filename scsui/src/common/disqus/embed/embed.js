angular.module('rpsmarf.disqus.embed',[
	'rpsmarf.config'
])
.service('SmDisqus', ['DISQUS_FORUM_SHORTNAME', function(DISQUS_FORUM_SHORTNAME){

	var disqus_shortname = DISQUS_FORUM_SHORTNAME;
	//Config contains the parameters for contacting and validating with disqus
	//such as remote_auth_s3 and api_key, this is returned from the server upon log in
	var config, init=false;
	var setConfig = function(c){
		config = c;
	};

	var getConfig = function(){

		return config;
	};

	var isConfigSet = function(){
		return config && config.valid;
	};

	var getEmbedInitialized = function(){
		return init;
	};



	var initDisqusEmbed = function(identifier, title, url){

		window.disqus_config = function(){
			this.page.remote_auth_s3 = config.remote_auth_s3;
			this.page.api_key = config.api_key;
			//window.page.identifier =identifier;
			//window.page.title = title;
			//window.page.url = url;
		};
		window.disqus_identifier = identifier;
		window.disqus_title = title;
		window.disqus_url = url;
		//console.log(config);

		(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
		init = true;
	};

	return {
		setConfig: setConfig,
		getConfig: getConfig,
		isConfigSet: isConfigSet,
		getEmbedInitialized: getEmbedInitialized,
		initDisqusEmbed: initDisqusEmbed
	};
}])
.directive('disqusembed', ['DISQUS_FORUM_SHORTNAME','SmDisqus', function(DISQUS_FORUM_SHORTNAME,SmDisqus){

	return {
		restrict: 'E',
		scope: false,
		link: function($scope, $element, $attrs){
			var disqus_shortname;

			$scope.initDisqusEmbedOld = function(){
				(function() {
					var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
					dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
					(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
				})();
				SmDisqus.setEmbedInitialized();
			};


			disqus_shortname = DISQUS_FORUM_SHORTNAME;

			
			

			
			
			
		}
	};
}]);