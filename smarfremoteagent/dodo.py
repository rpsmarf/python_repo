'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import glob
import os
import fnmatch
import datetime

pyFiles = []
pyFilesToCheck = []
for root, dirnames, filenames in os.walk('.'):
    for filename in fnmatch.filter(filenames, '*.py'):
        pyFiles.append(os.path.join(root, filename))
        if (not (filename.endswith("_ice.py") or filename.startswith("__"))):
            pyFilesToCheck.append(os.path.join(root, filename))
    for filename in fnmatch.filter(filenames, '*.sh'):
        pyFiles.append(os.path.join(root, filename))


installFiles = glob.glob("install/*")
debFiles = installFiles
debFiles.append('../build_output/sra_python.zip')
debFiles.append('../build_output/smarf_common.zip')

COVERAGE_REPORT_OMIT_LIST = "smremoteagent/init/sra_main.py"

SMARF_HOST = "unset"
SMARF_USER = "unset"
if ("SMARF_HOST" in os.environ):
    SMARF_HOST = os.environ["SMARF_HOST"]
if ("SMARF_GITDIR" in os.environ):
    SMARF_GITDIR = os.environ["SMARF_GITDIR"]
if ("SMARF_USER" in os.environ):
    SMARF_USER = os.environ["SMARF_USER"]
t = datetime.datetime.today()
SMARF_VERSION = t.strftime("%Y%m%d%H%M%S") + "-" + SMARF_USER
    
SSH_CMD = ("ssh  -o UserKnownHostsFile=/dev/null " + 
"-o StrictHostKeyChecking=no " + 
"-i ../tools/keys/smarf_dair_keys.pem ubuntu@" + SMARF_HOST)
FLAKE8_OPTIONS = "--ignore=W292,E291,E261,E262,E265,E292,E128,W293,W291,E123,E126,W391 --max-line-length=128"

DOIT_CONFIG = {'default_tasks': ['show']}


def task_show():
    """show all available tasks"""
    return {'actions': ['doit list'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }


def task_keysetup():
    """show all available tasks"""
    return {'actions': ['rm -f ../build_output/smarf_dair_keys.pem',
                        'cp ../tools/keys/smarf_dair_keys.pem ../build_output/smarf_dair_keys.pem',
                        'chmod 400 ../build_output/smarf_dair_keys.pem'],
            'file_dep': ["../tools/keys/smarf_dair_keys.pem"],
            'targets': ["../build_output/smarf_dair_keys.pem"],
            'verbosity': 2,
            }


def task_vlist():
    """show all available versions of smarf-sra on software server"""
    return {'actions': ['sudo apt-get update > /dev/null',
                        'apt-cache show smarf-sra | grep Version'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }


def task_checker():
    """run flake8 on all project files"""
    for module in pyFilesToCheck:
        yield {'actions': ['../tools/flake8_wrapper smarfremoteagent ' + FLAKE8_OPTIONS + ' %(dependencies)s '
                           ],
               'name': module,
               'file_dep': [module],
               'verbosity': 0,
               }


def task_commonzip():
    """create ZIP file with smcommon SMARF code."""
    return {'actions': [
            'doit --dir ../smarfcommon zip',
            ],
            'targets': ['../build_output/smarf_common.zip'],
            'verbosity': 2,
            'clean': ["rm -f ../build_output/smarf_common.zip"
                      ],
            }


def task_zip():
    """create ZIP file with SRA Python code.  
    Clean deletes all pycache folders"""
    return {'actions': [
            'rm -f ../build_output/sra_python.zip',
            'zip -q -r ../build_output/sra_python.zip . '
            '--exclude \*__pycache__\* '
            '--exclude .\* '
            '--exclude dodo.py '
            '--exclude install/\*'],
            'targets': ['../build_output/sra_python.zip'],
            'file_dep': pyFiles,
            'task_dep': ['checker', 'commonzip'],
            'verbosity': 2,
            'clean': ["rm -f ../build_output/sra_python.zip",
                      "find . -name __pycache__ -prune -exec rm -r {} \;"
                      ],
            }


def task_deb():
    """create .deb file for the SRA"""
    return {'actions': [
            'rm -rf ../build_output/smarf-sra',
            'mkdir ../build_output/smarf-sra',
            'mkdir ../build_output/smarf-sra/DEBIAN',
            'sed -e s/@VERSION/' + SMARF_VERSION + 
'/ install/control > ../build_output/smarf-sra/DEBIAN/control',
            'cp install/conffiles ../build_output/smarf-sra/DEBIAN',
            'cp install/postinst ../build_output/smarf-sra/DEBIAN',
            'chmod 755 ../build_output/smarf-sra/DEBIAN/postinst',
            'mkdir -p ../build_output/smarf-sra/etc/init',
            'cp install/etc_init_smarf-sra.conf ../build_output/smarf-sra/etc/init/smarf-sra.conf ',
            'mkdir -p ../build_output/smarf-sra/etc/nginx/sites-enabled',
            'cp install/etc_nginx_sites-enabled_smarf-sra_log.conf '
            '../build_output/smarf-sra/etc/nginx/sites-enabled/smarf-sra_log.conf',
            'mkdir ../build_output/smarf-sra/etc/smarf-sra',
            'cp install/sra.conf ../build_output/smarf-sra/etc/smarf-sra',
            'cp install/logging.conf ../build_output/smarf-sra/etc/smarf-sra',
            'mkdir -p ../build_output/smarf-sra/var/run/smarf-sra',
            'mkdir ../build_output/smarf-sra/etc/smarf-sra/keys',
            'cp keys/* ../build_output/smarf-sra/etc/smarf-sra/keys',
            'cp ../tools/keys/guac_key.public ../build_output/smarf-sra/etc/smarf-sra/keys',
            'chmod 400 ../build_output/smarf-sra/etc/smarf-sra/keys/*',
            'sudo chown www-data.www-data ../build_output/smarf-sra/var/run/smarf-sra',
            'mkdir -p ../build_output/smarf-sra/var/log/smarf-sra',
            'sudo chown rpsmarf ../build_output/smarf-sra/var/log/smarf-sra',
            'mkdir -p ../build_output/smarf-sra/opt/smarf-sra/sra',
            'unzip ../build_output/sra_python.zip -d ../build_output/smarf-sra/opt/smarf-sra/sra',
            'unzip -o ../build_output/smarf_common.zip -d ../build_output/smarf-sra/opt/smarf-sra/sra',
            'mkdir -p ../build_output/smarf-sra/usr/local/bin',
            'cp install/LICENSE ../build_output/smarf-sra/opt/smarf-sra',
            'cp scripts/* ../build_output/smarf-sra/usr/local/bin',
            'chmod 755 ../build_output/smarf-sra/usr/local/bin/*',
            'cd ../build_output;dpkg -b smarf-sra',
            ],
            'targets': ['../build_output/smarf-sra.deb'],
            'file_dep': debFiles,
            'clean': True,
            }


def task_install():
    """install SRA DEB package locally"""
    return {'actions': ['sudo dpkg -r smarf-sra',
                        'sudo dpkg -i ../build_output/smarf-sra.deb'],
            'file_dep': ['../build_output/smarf-sra.deb'],
            'targets': ['/etc/init.d/smarf-sra'],
            'verbosity': 2,
            'clean': ["sudo dpkg --purge smarf-sra"],
            }


def task_test():
    """run unit test for the SMARF remote agent"""
    return {'actions': ["export PYTHONPATH=../smarfcommon;" +
                        "coverage run --source smremoteagent -m unittest discover -s smremoteagent.tests -p '*tests.py'",
                        "coverage report --omit " + COVERAGE_REPORT_OMIT_LIST +
                        "|grep TOTAL|sed -e 's/TOTAL                    /CODE COVERAGE/'",

                        ],
            'file_dep': ['../build_output/sra_python.zip'],
            'verbosity': 2,
            'clean': ["rm -f /tmp/unittestLogging.conf",
                      ],
            }


def task_jenkins():
    """run unit test for the SMARF remote agent and report XML for jenkins build"""
    return {'actions': ["export PYTHONPATH=../smarfcommon:;" +
                        "for f in smremoteagent/tests/*_tests.py; do echo Processing $f file..;" +
                        "coverage run --source smremoteagent $f; done",
                        "coverage report --omit " + COVERAGE_REPORT_OMIT_LIST +
                        "|grep TOTAL|sed -e 's/TOTAL                    /CODE COVERAGE/'",

                        ],
            'file_dep': ['../build_output/sra_python.zip'],
            'verbosity': 2,
            'clean': ["rm -f /tmp/unittestLogging.conf",
                      ],
            }


def task_coverage():
    """view code coverage for the SMARF remote agent"""
    return {'actions': ["rm -rf /tmp/htmlcoverage",
                        "coverage html  --omit " + COVERAGE_REPORT_OMIT_LIST +
                        " -d /tmp/htmlcoverage",
                        "firefox /tmp/htmlcoverage/index.html",
                        ],
            'task_dep': ['test'],
            'verbosity': 2,
            }


def task_rinstall():
    """install the currently building SRA DEB package remotely"""
    return {'actions': [
            'scp -q -o UserKnownHostsFile=/dev/null '
'-o StrictHostKeyChecking=no '
'-i ../tools/keys/smarf_dair_keys.pem ../build_output/smarf-sra.deb ubuntu@' + 
SMARF_HOST + ':/tmp',
            SSH_CMD + ' "sudo dpkg -i /tmp/smarf-sra.deb"',
            ],
            'file_dep': ['../build_output/smarf-sra.deb',
                         '../build_output/smarf_dair_keys.pem'],
            'verbosity': 2,
            }


def task_rrestart():
    """restart the SRA on the remote server defined by SMARF_HOST"""
    return {'actions': [
            SSH_CMD + ' "sudo service smarf-sra restart"',
            ],
            'uptodate': [False],
            'file_dep': ['../build_output/smarf_dair_keys.pem'],
            'verbosity': 2,
            }


def task_rstop():
    """stop the SRA on the remote server defined by SMARF_HOST"""
    return {'actions': [
            SSH_CMD + ' "sudo service smarf-sra stop"',
            ],
            'uptodate': [False],
            'file_dep': ['../build_output/smarf_dair_keys.pem'],
            'verbosity': 2,
            }


def task_rstart():
    """start the SRA on the remote server defined by SMARF_HOST"""
    return {'actions': [
            SSH_CMD + ' "sudo service smarf-sra start"',
            ],
            'uptodate': [False],
            'file_dep': ['../build_output/smarf_dair_keys.pem'],
            'verbosity': 2,
            }


def task_rclearlog():
    """Clear logs on the remote server defined by SMARF_HOST"""
    return {'actions': [
            SSH_CMD + ' "sudo rm -f /var/log/smarf-sra/*"',
            ],
            'uptodate': [False],
            'file_dep': ['../build_output/smarf_dair_keys.pem'],
            'verbosity': 2,
            }


def task_i():
    '''
    Alias of rinstall
    '''
    return task_rinstall()


def task_c():
    '''
    Alias of rclearlog
    '''
    return task_rclearlog()


def task_r():
    '''
    Alias of rrestart
    '''
    return task_rrestart()


def task_debup():
    """create and upload .deb file for the SRA"""
    return {'actions': 
            ['scp -q -o UserKnownHostsFile=/dev/null '
             '-o StrictHostKeyChecking=no '
            '-i ../tools/keys/smarf_dair_keys.pem '
            '../build_output/smarf-sra.deb '
            'ubuntu@sw.rpsmarf.ca:/var/www/debs/all/smarf-sra-' + 
             SMARF_VERSION + '.deb',
           'ssh  -o UserKnownHostsFile=/dev/null '
            '-o StrictHostKeyChecking=no '
            '-i ../tools/keys/smarf_dair_keys.pem ubuntu@sw.rpsmarf.ca '
            '"cd /var/www/debs;dpkg-scanpackages '
            '-m all| gzip -9c > all/Packages.gz"'
             ],
            'file_dep': ['../build_output/smarf-sra.deb',
                         '../build_output/smarf_dair_keys.pem'],
            'targets': [],
            'verbosity': 2,
            }


def task_patch():
    """replace the python SRA files on the host referred to by SMARF_HOST"""
    return {
        'actions': 
        ['scp -q '
         '-o UserKnownHostsFile=/dev/null '
         '-o StrictHostKeyChecking=no '
         '-i ../tools/keys/smarf_dair_keys.pem '
         '../build_output/sra_python.zip ubuntu@' + SMARF_HOST + ':/tmp',
         'ssh  -o UserKnownHostsFile=/dev/null '
         '-o StrictHostKeyChecking=no '
         '-i ../tools/keys/smarf_dair_keys.pem '
         'ubuntu@' + SMARF_HOST + 
         ' sudo unzip -o /tmp/sra_python.zip -d /opt/smarf-sra/sra'
         ],
        'file_dep': ['../build_output/sra_python.zip',
                     '../build_output/smarf_dair_keys.pem'],
        'verbosity': 2,
           }


def task_restart():
    """restart the SRA on the local node"""
    return {'actions': [
            'sudo service smarf-sra restart',
            ],
            'uptodate': [False],
            'verbosity': 2,
            }


def task_clearlog():
    """Clears local logs"""
    return {'actions': [
            'sudo rm -f /var/log/smarf-sra/*',
            ],
            'uptodate': [False],
            'verbosity': 2,
            }

