import json
import sys
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath, scsGetToJson
from scripts.scs_obj_create import scsCreateObjectPath

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

user_a = scsCreateObjectPath("user", {"username": "a",
                                      "email": "a@a.a",
                                      "password": "a",
                                      "first_name": "Albert",
                                      "last_name": "Allen"})

user_b = scsCreateObjectPath("user", {"username": "b",
                                      "email": "b@b.b",
                                      "password": "b",
                                      "first_name": "Bjorn",
                                      "last_name": "Borg"})

user_c = scsCreateObjectPath("user", {"username": "c",
                                      "email": "c@c.c",
                                      "password": "c",
                                      "first_name": "Charlie",
                                      "last_name": "Chaplin"})

root_path = scsGetObjectPath("user", "username=rpsmarf")

community_path = scsCreateObjectPath("community",
                                     {"name": "Everything",
                                      "name_key": "all_resources",
                                      "description": "This community represents all tools",
                                      "leader": root_path,
                                      "admin": root_path,
                                      })
scsCreateObjectPath("user_setting",
                    {"user": root_path,
                     "community": community_path})

user_path = scsGetObjectPath("user", "username=a")

scsCreateObjectPath("user_setting",
                    {"user": user_path,
                     "community": community_path})

rt_compute_path = scsCreateObjectPath("resource_type", {"name": "c1", "nature": "compute",
                                                        "description": "This is the compute resource"})

TASK_TYPE_SLEEP_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", "name": "Argument", "value": "sleep"},
                                     {"type": "string", 
                                      "name": "sec_to_sleep", 
                                      "input_id": 1,
                                      "description": "The number of seconds to sleep."},
                                     ],
                            "outputProgress": {"outputArg": 1},
                            "computeResourceRole": "compute"
                       }

tt_path = scsCreateObjectPath("task_type", {"name": "sleep",
                                            "code_module": "smcommon.task_runners.task_runner_shell",
                                            "code_classname": "SmTaskRunnerShell",
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_SLEEP_CFG_DICT),
                                            "description": "Allows the user to crun a delay"})
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

scsCreateObjectPath("resource_type_task_type", {"role": "compute", "name": "sleep_compute",
                                                "task_type": tt_path, "resource_type": rt_compute_path,
                                                "description": "This is the compute resource for sleep task"})

TASK_TYPE_COPY_CFG_DICT = {
                            "args": [ 
                                     {"type": "resource", 
                                      "name": "Source file", 
                                      "pathType": "file", 
                                      "input_id": 1,
                                      "direction": "input", 
                                      "role": "src", 
                                      "description": "A file to copy."},
                                     {"type": "resource", 
                                      "name": "Destination file", 
                                      "pathType": "file", 
                                      "input_id": 2,
                                      "direction": "output", 
                                      "role": "dest", 
                                      "description": "A filename to copy data to"},
                                     ],
                            "outputProgress": {"outputArg": 1},
                            "computeResourceRole": "dest",
                            "skipRunPermCheck": True,
                            "preExistingOutputFolderOk": True
                       }

TASK_TYPE_COPY_CFG = json.dumps(TASK_TYPE_COPY_CFG_DICT)
tt_path = scsCreateObjectPath("task_type", {"name": "copy",
                                            "code_module": "smcommon.task_runners.task_runner_copy",
                                            "code_classname": "SmTaskRunnerCopy",
                                            "owner": root_path,
                                            "configurationJson": TASK_TYPE_COPY_CFG,
                                            "description": "Allows the user to copy files or folders from one " + 
                                            "resource/folder to another resource/folder"})
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

rt_data_path = scsCreateObjectPath("resource_type", {"name": "d1", "nature": "data",
                                                    "description": "This is the resource type for the cp"})
scsCreateObjectPath("resource_type_task_type", {"role": "src",
                                                "name": "cp_src",
                                                "task_type": tt_path,
                                                "resource_type": rt_data_path,
                                                "description": "This is the source data for the cp command"})
scsCreateObjectPath("resource_type_task_type", {"role": "dest",
                                                "name": "cp_dest",
                                                "task_type": tt_path,
                                                "resource_type": rt_data_path,
                                                "description": "This is the destination data for the cp command"})

TASK_TYPE_COPY_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", "name": "Argument", 
                                      "value": "/opt/smarf-demos/demos/cp_many/cp_many.sh"},
                                     {"type": "resource", 
                                      "name": "First source file", 
                                      "pathType": "file", 
                                      "input_id": 1,
                                      "direction": "input", 
                                      "role": "src1", 
                                      "description": "A first file to copy."},
                                     {"type": "resource", 
                                      "name": "First destination folder", 
                                      "pathType": "folder", 
                                      "input_id": 2,
                                      "direction": "output", 
                                      "role": "dest1", 
                                      "description": "A folder to copy the first files to"},
                                     {"type": "string", 
                                      "name": "Number of first files to copy", 
                                      "input_id": 3,
                                      "description": "Times to copy the first input file into the output folder."},
                                     {"type": "string", 
                                      "name": "First file name base", 
                                      "input_id": 4,
                                      "description": "The base name for the file to copy"},
                                     {"type": "resource", 
                                      "name": "Second source file", 
                                      "pathType": "file", 
                                      "input_id": 5,
                                      "direction": "input", 
                                      "role": "src2", 
                                      "description": "A file to copy."},
                                     {"type": "resource", 
                                      "name": "Second destination folder", 
                                      "pathType": "folder", 
                                      "input_id": 6,
                                      "direction": "output", 
                                      "role": "dest2", 
                                      "description": "A folder to copy data to"},
                                     {"type": "string", 
                                      "name": "Number of second files to copy", 
                                      "input_id": 7,
                                      "description": "The number of times to copy the input file into the output folder."},
                                     {"type": "string", 
                                      "name": "Second file name base", 
                                      "input_id": 8,
                                      "description": "The base name for the file to copy"},
                                     ],
                            "outputProgress": {"outputArg": 1},
                            "computeResourceRole": "compute"
                       }

TASK_TYPE_COPY_CFG = json.dumps(TASK_TYPE_COPY_CFG_DICT)
tt_path = scsCreateObjectPath("task_type", {"name": "Many Argument File Copy",
                                            "name_key": "cp_many",
                                            "code_module": "smcommon.task_runners.task_runner_shell",
                                            "code_classname": "SmTaskRunnerShell",
                                            "owner": root_path,
                                            "configurationJson": TASK_TYPE_COPY_CFG,
                                            "description": "Allows the user to copy files with many " +
                                            "arguments to test argument handling",
                                            })
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

scsCreateObjectPath("resource_type_task_type", {"role": "src1",
                                                "name": "many_cp_src1",
                                                "task_type": tt_path,
                                                "resource_type": rt_data_path,
                                                "description": "Source data for the many argument copy command"})
scsCreateObjectPath("resource_type_task_type", {"role": "dest1",
                                                "name": "many_cp_dest1",
                                                "task_type": tt_path,
                                                "resource_type": rt_data_path,
                                                "description": "Destination data for the many argument copy command"})
scsCreateObjectPath("resource_type_task_type", {"role": "src2",
                                                "name": "many_cp_src2",
                                                "task_type": tt_path,
                                                "resource_type": rt_data_path,
                                                "description": "This is the source data for the many argument copy command"})
scsCreateObjectPath("resource_type_task_type", {"role": "dest2",
                                                "name": "many_cp_dest2",
                                                "task_type": tt_path,
                                                "resource_type": rt_data_path,
                                                "description": "Destination data for the many argument copy command"})
scsCreateObjectPath("resource_type_task_type", {"role": "compute", "name": "many_cp_compute",
                                                "task_type": tt_path, "resource_type": rt_compute_path,
                                                "description": "This is the compute resource for many_cp"})

TASK_TYPE_SHELL_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", "name": "Argument", "value": "cp"},
                                     {"type": "resource", 
                                      "name": "This is the file which is copied", 
                                      "pathType": "file", 
                                      "input_id": 1, 
                                      "direction": "input",
                                      "role": "src", 
                                      "name": "Copy Source", 
                                      "description": "A file to copy."},
                                     {"type": "resource", 
                                      "description": "This is the folder into which the files are copied",
                                      "pathType": "folder", 
                                      "input_id": 2, 
                                      "direction": "output",
                                      "role": "dest", 
                                      "name": "Copy Destination", 
                                      "description": "A folder to copy data to."},
                                     ],
                            "outputProgress": {"outputArg": 2},
                            "computeResourceRole": "dest"
                       }
tt_path = scsCreateObjectPath("task_type", {"name": "shell_cp_file_to_folder",
                                     "code_module": "smcommon.task_runners.task_runner_shell",
                                     "code_classname": "SmTaskRunnerShell",
                                     "description": "Task to copy data from a filename to a folder",
                                     "owner": root_path,
                                     "configurationJson": json.dumps(TASK_TYPE_SHELL_CFG_DICT)}
                              )
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

scsCreateObjectPath("resource_type_task_type", {"role": "src", "name": "Source File For Copy To Folder", "task_type": 
                                                tt_path, "resource_type": rt_data_path,
                                                "description": "This is the source data for cp file to folder"})
scsCreateObjectPath("resource_type_task_type", {"role": "dest", "name": "Dest Folder For Copy To Folder", "task_type": 
                                                tt_path, "resource_type": rt_data_path,
                                                "description": "This is the dest data for cp file to folder"})

TASK_TYPE_SHELL_CP_R_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", "name": "Argument", "value": "cp"},
                                     {"type": "string", "name": "Argument", "value": "-r"},
                                     {"type": "resource", 
                                      "name": "This is the folder which is copied", 
                                      "pathType": "folder", 
                                      "input_id": 1, 
                                      "direction": "input",
                                      "role": "src", 
                                      "name": "Copy Source", 
                                      "description": "A file to copy."},
                                     {"type": "resource", 
                                      "description": "This is the folder into which the files are copied",
                                      "pathType": "folder", 
                                      "input_id": 2, 
                                      "direction": "output",
                                      "role": "dest", 
                                      "name": "Copy Destination", 
                                      "description": "A folder to copy data to."},
                                     ],
                            "outputProgress": {"outputArg": 2},
                            "computeResourceRole": "dest"
                       }
tt_path = scsCreateObjectPath("task_type", {"name": "shell_cp_folder_to_folder",
                                     "code_module": "smcommon.task_runners.task_runner_shell",
                                     "code_classname": "SmTaskRunnerShell",
                                     "description": "Task to copy data from a folder to a folder",
                                     "owner": root_path,
                                     "configurationJson": json.dumps(TASK_TYPE_SHELL_CP_R_CFG_DICT)}
                              )
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

scsCreateObjectPath("resource_type_task_type", {"role": "src", "name": "Source Folder For Copy To Folder", "task_type": 
                                                tt_path, "resource_type": rt_data_path,
                                                "description": "This is the source data for cp file to folder"})
scsCreateObjectPath("resource_type_task_type", {"role": "dest", "name": "Dest Folder For Folder Copy", "task_type": 
                                                tt_path, "resource_type": rt_data_path,
                                                "description": "This is the dest data for cp file to folder"})


TASK_TYPE_SHELL_CFG_DICT2 = {
                            "args": [ 
                                     {"type": "string", "name": "Argument", "value": "cp"},
                                     {"type": "resource", 
                                      "pathType": "file", 
                                      "input_id": 1, 
                                      "direction": "input",
                                      "role": "src",
                                      "name": "Copy Source", 
                                      "description": "A file to copy."},
                                     {"type": "resource", 
                                      "pathType": "file", 
                                      "input_id": 2, "direction": "output",
                                      "role": "dest", 
                                      "name": "Copy Destination", 
                                      "description": "A folder to copy data to."},
                                     ],
                            "outputProgress": {"outputArg": 2},
                            "computeResourceRole": "dest"
                       }
tt_path = scsCreateObjectPath("task_type", {"name": "shell_cp_file_to_file",
                                     "code_module": "smcommon.task_runners.task_runner_shell",
                                     "code_classname": "SmTaskRunnerShell",
                                     "description": "Task to copy a file to another filename",
                                     "owner": root_path,
                                     "configurationJson": json.dumps(TASK_TYPE_SHELL_CFG_DICT2)})
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

scsCreateObjectPath("resource_type_task_type", {"role": "src", "name": "src", "task_type": 
                                                tt_path, "resource_type": rt_data_path,
                                                "description": "This is the source data for the shell lines"})
scsCreateObjectPath("resource_type_task_type", {"role": "dest", "name": "dest", "task_type": 
                                                tt_path, "resource_type": rt_data_path,
                                                "description": "This is the dest data for the shell lines"})

TASK_TYPE_SHELL_LINES_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", "name": "Argument", "value": "/opt/smarf-demos/demos/lines/lines.sh"},
                                     {"type": "resource", 
                                      "name": "Input control file", 
                                      "pathType": "file", 
                                      "input_id": 1,
                                      "direction": "input", 
                                      "role": "src",
                                      "description": "An input file to control the line generation"},
                                     {"type": "resource", 
                                      "name": "Output movie folder", 
                                      "pathType": "folder", 
                                      "input_id": 2,
                                      "direction": "output", 
                                      "role": "dest",
                                      "description": "The folder into which the output file is put"},
                                     ],
                            "outputProgress": {"outputArg": 2, "maxSize": 991928},
                            "computeResourceRole": "compute"
                       }
tt_path = scsCreateObjectPath("task_type", {"name": "shell_lines",
                                     "code_module": "smcommon.task_runners.task_runner_shell",
                                     "code_classname": "SmTaskRunnerShell",
                                     "description": "Task to make movie",
                                     "owner": root_path,
                                     "configurationJson": json.dumps(TASK_TYPE_SHELL_LINES_CFG_DICT)})
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")


scsCreateObjectPath("resource_type_task_type", {"role": "src", "name": "c1_src", "task_type": tt_path,
                                                "metadataJson": json.dumps({"folder": False}),
                                                "resource_type": rt_data_path,
                                                "description": "This is the source resource"})
scsCreateObjectPath("resource_type_task_type", {"role": "dest", "name": "c1_dest", "task_type": tt_path,
                                                "metadataJson": json.dumps({"folder": True}),
                                                "resource_type": rt_data_path,
                                                "description": "This is the destination resource"})
scsCreateObjectPath("resource_type_task_type", {"role": "compute", "name": "c1_compute",
                                                "task_type": tt_path, "resource_type": rt_compute_path,
                                                "description": "This is the compute resource"})


agent_path = scsCreateObjectPath("agent", {"guid": "LOCAL_AGENT",
                                           "name": "Local Agent",
                                           "name_key": "local_agent",
                                           "owner": root_path,
                                           "last_change": '2013-01-01',
                                           "agentUrl": "ice://localhost:9001",
                                           "description": "The local cloud files agent"
                                           }
                                 )

container_path = scsCreateObjectPath("container", {"name": "fs1",
                                                   "name_key": "user_storage",
                                                   "containerUrl": "local://localhost/tmp/",
                                                   "agent": agent_path,
                                                   "status": "up",
                                                   "description": "This is the local cloud storage"})
res_copy_src = scsCreateObjectPath("resource", {"name": "copy_src",
                                                "resource_type": rt_data_path,
                                                "owner": root_path,
                                                "container": container_path,
                                                "nature": "data",
                                                "parametersJson": json.dumps({"folder": "src/"}),
                                                "description": "This is a resource for the copy command in /tmp/src."
                                                }
                                   )
scsGetToJson(res_copy_src + "setperm/?action=assign&user=" + user_c + "&perm=x")
scsGetToJson(res_copy_src + "setperm/?action=assign&user=" + user_b + "&perm=x")
 
res_path = scsCreateObjectPath("resource", {"name": "copy_dest",
                                            "resource_type": rt_data_path,
                                            "owner": root_path,
                                            "container": container_path,
                                            "nature": "data",
                                            "parametersJson": json.dumps({"folder": "dest/"}),
                                            "description": "This is a long description of this resource." + 
                                            " The main purpose of this description is to see what effects " +
                                            "it will have on the UI." + 
                                            " This folder lives witin the /tmp of your local file system. It " +
                                            "should show an empty" + 
                                            " folder on page load, but thanks to the copy task we should be " +
                                            "able to copy a file/folder" + 
                                            " from the src resource."
                                            }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=rw")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=r")


res_path = scsCreateObjectPath("resource", {"name": "BART_runner",
                                            "resource_type": rt_compute_path,
                                            "owner": root_path,
                                            "container": container_path,
                                            "parametersJson":            
                                            json.dumps({"taskViewBaseUrl": 
                                                        "http://localhost:8080/guacamole" +
                                                        "/#/client/c/default"}),
                                            "description": "This is the compute resource"
                                            }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=x")


container_ssh_path = scsCreateObjectPath("container", {"name": "ssh1",
                                                       "name_key": "user_storage_ssh",
                                                       "description": "This is the container for SSH testing",
                                                       "containerUrl": "sftp://rpsmarf@localhost/tmp/?keyfile=dropbear_key",
                                                       "agent": agent_path,
                                                       "status": "up"})
res_path = scsCreateObjectPath("resource", {"name": "ssh",
                                            "resource_type": rt_data_path,
                                            "owner": root_path,
                                            "nature": "data",
                                            "container": container_path,
                                            "parametersJson": json.dumps({"folder": "ssh/"}),
                                            "description": "This is an SSH-accessible resource at /tmp/ssh/"
                                            }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=rw")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=r")

# Line resource

res_path = scsCreateObjectPath("resource", {"name": "c1_src",
                                            "resource_type": rt_data_path,
                                            "owner": root_path,
                                            "container": container_path,
                                            "parametersJson": json.dumps({"folder": "src/"}),
                                            "description": "This is an input resource for the line tool."
                                            }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=rw")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=r")
 
res_path = scsCreateObjectPath("resource", {"name": "c1_dest",
                                            "resource_type": rt_data_path,
                                            "owner": root_path,
                                            "container": container_path,
                                            "parametersJson": json.dumps({"folder": "dest/"}),
                                            "description": "This is a long description of this resource." + 
                                            " The main purpose of this description is to see what effects " +
                                            "it will have on the UI." + 
                                            " This folder lives witin the /tmp of your local file system. " + 
                                            "It should show an empty" + 
                                            " folder on page load, but thanks to the copy task we should be " + 
                                            "able to copy a file/folder" + 
                                            " from the src resource."
                                            }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=rw")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=r")

res_path = scsCreateObjectPath("resource", {"name": "c1_compute",
                                            "resource_type": rt_compute_path,
                                            "owner": root_path,
                                            "container": container_path,
                                            "nature": "compute",
                                            "parametersJson": 
                                            json.dumps({"taskViewBaseUrl": 
                                                        "http://testab1.rpsmarf.ca:8080/guacamole" +
                                                        "/#/client/c/default"}),
                                            "description": "This is a long description of this resource." + 
                                            " The main purpose of this description is to see what effects " +
                                            "it will have on the UI." + 
                                            " This folder lives witin the /tmp of your local file system. It " +
                                            "should show an empty" + 
                                            " folder on page load, but thanks to the copy task we should be " +
                                            " able to copy a file/folder from the src resource."}
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

# Setting up common task types

rt_generic_storage = scsCreateObjectPath("resource_type",
                                         {"name": "Generic Storage",
                                          "name_key": "generic_storage",
                                          "nature": "data",
                                          "description": "This is a resource type for use by generic storage operations" + 
                                          " such as delete, zip, unzip, etc..." 
                                          })

TASK_TYPE_ZIP_DICT = {
                           "args": [ 
                                     {"type": "string", "name": "Argument", "value": "zip"},
                                     {"type": "string", "name": "Argument", "value": "-rdc"},
                                     {"type": "resource", 
                                      "name": "File name to create", 
                                      "description": "This is zip file which is created",
                                      "pathType": "file", 
                                      "input_id": 1,
                                      "direction": "output", "role": "zip"},
                                     {"type": "resource", 
                                      "name": "Folder to zip", 
                                      "description": "This is the folder whose contents will be added to the zip file",
                                      "pathType": "folder", 
                                      "input_id": 2,
                                      "direction": "input", 
                                      "role": "zip", 
                                      "modifier": "filename"},
                                     ],
                           "computeResourceRole": "zip",
                           "skipRunPermCheck": True,
                           "workingDir": {"type": "resource", "input_id": 2, "role": "zip", "modifier": "parent"}
                            }

tt_zip = scsCreateObjectPath("task_type", {"name": "zip",
                                  "code_module": "smcommon.task_runners.task_runner_zip",
                                  "code_classname": "SmTaskRunnerZip",
                                  "description": "Task to zip up files",
                                  "uiVisible": "False",
                                  "owner": root_path,
                                  "configurationJson": json.dumps(TASK_TYPE_ZIP_DICT)})
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

scsCreateObjectPath("resource_type_task_type", {"role": "zip", "task_type": tt_zip,
                                                "resource_type": rt_generic_storage,
                                                "nature": "data",
                                                "name": "data_zip",
                                                "description": "This links the zip task type to all storage resources"})


TASK_TYPE_UNZIP_DICT = {
                            "args": [ 
                                     {"type": "string", "name": "Argument", "value": "unzip"},
                                     {"type": "string", "name": "Argument", "value": "-d"},
                                     {"type": "resource", 
                                      "name": "Folder to unzip to", 
                                      "description": "This is the folder to unzip to",
                                      "pathType": "folder",
                                      "input_id": 2,
                                      "direction": "output", 
                                      "role": "unzip"},
                                     {"type": "resource", 
                                      "name": "File to unzip", 
                                      "description": "This is the file to unzip",
                                      "pathType": "file",
                                      "input_id": 1,
                                      "direction": "input", 
                                      "role": "unzip"},
                                     ],
                            "computeResourceRole": "unzip",
                            "skipRunPermCheck": True,
                       }

tt_unzip = scsCreateObjectPath("task_type", {"name": "unzip",
                                  "code_module": "smcommon.task_runners.task_runner_shell",
                                  "code_classname": "SmTaskRunnerShell",
                                  "description": "Task to unzip data",
                                  "uiVisible": "False",
                                  "owner": root_path,
                                  "configurationJson": json.dumps(TASK_TYPE_UNZIP_DICT)})
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

scsCreateObjectPath("resource_type_task_type", {"role": "unzip", "task_type": tt_unzip,
                                                "resource_type": rt_generic_storage,
                                                "nature": "data",
                                                "name": "unzip data",
                                                "description": "This links the unzip task type to all storage resources"})

TASK_TYPE_DELETE_DICT = {
                            "args": [ 
                                     {"type": "resource", 
                                      "name": "Folder to delete",
                                      "description": "This is the folder which will be deleted",
                                      "input_id": 1,
                                      "direction": "input",
                                      "pathType": "folder",
                                      "role": "storage"},
                                     ],
                            "computeResourceRole": "storage",
                            "skipRunPermCheck": True
                       }
tt_delete = scsCreateObjectPath("task_type", {"name": "Recusive Delete",
                                             "name_key": "delete_recusive",
                                             "code_module": "smcommon.task_runners.task_runner_delete",
                                             "code_classname": "SmTaskRunnerDelete",
                                             "description": "Task to delete data asynchronously",
                                             "uiVisible": "False",
                                             "owner": root_path,
                                             "configurationJson": json.dumps(TASK_TYPE_DELETE_DICT)})
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

scsCreateObjectPath("resource_type_task_type", {"role": "storage", "task_type": tt_delete,
                                                "resource_type": rt_generic_storage,
                                                "nature": "data",
                                                "name": "Delete data",
                                                "description": "This links the delete task type to all storage resources"})

# Setting up Asteroids demo
TASK_TYPE_ASTEROIDS_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", "name": "Argument", "value": "bash"},
                                     {"type": "string", "name": "Argument", "value": "asteroids.sh"},
                                     ],
                            "computeResourceRole": "compute",
                            "interactive": True,
                            "workingDir": {"type": "string", "value": "/opt/smarf-demos/demos/git_demo/asteroids_git_demo"},
                            "versioning": "git"
                       }

tt_path = scsCreateObjectPath("task_type", {"name": "Asteroids Game",
                                            "name_key": "asteroids",
                                            "code_module": "smcommon.task_runners.task_runner_shell",
                                            "code_classname": "SmTaskRunnerShell",
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_ASTEROIDS_CFG_DICT),
                                            "description": "Allows the user to run a game with different versions"})
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

scsCreateObjectPath("resource_type_task_type", {"role": "compute", "name": "asteroids_compute_rt3",
                                                "task_type": tt_path, "resource_type": rt_compute_path,
                                                "description": "This is the compute rt3 for the asteroids task"})

# Setting up metadata...
# Add a schema
schema_path = scsCreateObjectPath("md_schema", {"name": "Sample Schema",
                                                "description": "This is the example schema",
                                                "owner": root_path,
                                                "resource_type": rt_generic_storage,
                                                "community": community_path
                                                })


# Add 2 property types
ptype1_path = scsCreateObjectPath("md_property_type", {"name": "Wind Speed",
                                                       "property_type": "INT",
                                                       "schema": schema_path
                                                       })
ptype2_path = scsCreateObjectPath("md_property_type", {"name": "Temperature",
                                                       "property_type": "FLOAT",
                                                       "schema": schema_path
                                                       })

# Add 2 repos
repo1_path = scsCreateObjectPath("md_repo", 
                                 {"name": "Metadata repo 1",
                                  "description": "This is the first example metadata repository",
                                  "owner": root_path,
                                  "resources": [res_copy_src],
                                  "community": community_path,
                                  "schema": schema_path
                                  })

repo2_path = scsCreateObjectPath("md_repo", 
                                 {"name": "Metadata repo 2",
                                  "description": "This is the second example metadata repository",
                                  "owner": root_path,
                                  "resources": [res_copy_src],
                                  "community": community_path,
                                  "schema": schema_path
                                  })

# Add 2 files to first repo
path1_path = scsCreateObjectPath("md_path", 
                                 {"relative_path": "dir/f1",
                                  "repo": repo1_path,
                                  "resource": res_copy_src,
                                  })
path2_path = scsCreateObjectPath("md_path", 
                                 {"relative_path": "dir/f2",
                                  "repo": repo1_path,
                                  "resource": res_copy_src,
                                  })

# Add 2 attrbibutes to each file
scsCreateObjectPath("md_property", 
                    {"path": path1_path,
                    "property_type": ptype1_path,
                    "value": "23"})
scsCreateObjectPath("md_property", 
                    {"path": path1_path,
                    "property_type": ptype2_path,
                    "value": "-5"})
scsCreateObjectPath("md_property", 
                    {"path": path2_path,
                    "property_type": ptype1_path,
                    "value": "74"})
scsCreateObjectPath("md_property", 
                    {"path": path2_path,
                    "property_type": ptype2_path,
                    "value": "31"})


print("Core objects setup.")
