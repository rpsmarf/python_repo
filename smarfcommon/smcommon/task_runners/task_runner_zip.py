'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Dec 9, 2014

    @author: rpsmarf
'''
import logging
from smcommon.task_runners.task_runner_shell import SmTaskRunnerShell

logger = logging.getLogger(__name__)


class SmTaskRunnerZip(SmTaskRunnerShell):
    '''
    classdocs
    '''
    
    def __init__(self, taskId, doneCallback, taskDesc):
        SmTaskRunnerShell.__init__(self, taskId, doneCallback, taskDesc)
        logger.info("Starting SmTaskRunnerZip")
        self.output = ""
        
    def getLastLineOfOutput(self):
        lastCrIndex = self.output.rfind("\n")
        if lastCrIndex == -1:
            return None
        nextLastCrIndex = self.output.rfind("\n", 0, lastCrIndex - 1)
        if nextLastCrIndex == -1:
            return None
        return self.output[nextLastCrIndex + 1:lastCrIndex]
    
    def annouceTaskOutput(self, outputType, outputData):
        logger.debug("Got task output[%s]=%s", outputType, outputData)
        try:
            self.output += outputData
            if self._outputCallback:
                self._outputCallback(self.taskId, outputType, outputData)
            lastLine = self.getLastLineOfOutput()
            # The line is of the form 
            #   1/145 updating: smarfcontrolserver/.coverage (deflated 53%)
            if '/' not in lastLine:
                return
            parts = lastLine.split("/")
            done = int(parts[0].strip().split(" ", 1)[0])
            todo = int(parts[1].strip().split(" ", 1)[0])
            total = done + todo
            progress = {}
            progress["progress"] = round((done * 100.0) / total, 1)
            self.announceProgress(progress)
        except Exception as e:
            logger.debug("Error in output callback %s", str(e))            

    def progressCallback(self, copier, progress):
        # Suppress normal shell progress
        pass
        

