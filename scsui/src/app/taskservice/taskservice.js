angular.module( 'rpsmarf.taskservice', [
 'rpsmarf.config',
 'restangular'
])
.service('SmTask', [ 'DEBUG', 'Restangular', function( DEBUG, Restangular){

	var tasks = {
		copy:{
			src:null,
			dest:null,
			init: false,
			name_key: "copy",
			task_type_id: null
		}
	};

	if(!tasks.copy.init){
		//Get copy task info
		Restangular.all('task_type').get('?name_key='+tasks.copy.name_key).then(function(response){
			tasks.copy.task_type_id = response.objects[0].id;

			Restangular.one('resource_type_task_type/').get({task_type__id:tasks.copy.task_type_id}).then(function(object){
				var rttt;
				for(var i = object.objects.length -1; i>=0; i--){
					rttt = object.objects[i];
					tasks.copy[rttt.role] = rttt;
				}

				tasks.copy.init = true;
			});
		});
		
	}

	var getTaskInfo = function(taskName){
		if(!tasks[taskName]){
			return false;
		}else{
			return tasks[taskName];
		}
	};

	return {getTaskInfo: getTaskInfo};
}])
;