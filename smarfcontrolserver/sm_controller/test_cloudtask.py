'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 10, 2015

    @author: rpsmarf
'''

import unittest
from django.test.testcases import TransactionTestCase
from sm_controller.test_helper import ScsTestHelper, scsAwsTestKeysDefined,\
    scsTerminataAllAwsCloudServers, DUMMY_DESCRIPTION
from sm_controller.signal_handlers import smTriggerSignalInstall
import logging
from smcommon.globals.sm_copy_controller import SmCopyController
from smcommon.globals.sm_global_context import smGlobalContextInit
from smarfcontrolserver.pollers.cloud_state_poller import ScsCloudStatePoller

from smarfcontrolserver.cloudif.cloudif_mock import ScsCloudifMock
from smarfcontrolserver.cloudif.cloudif_factory import scsCloudServerIfFactoryGet
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_STATE,\
    SCS_CLOUDIF_STATE_STOPPED
import time
from sm_controller.models import SmModelTask, SmModelResourceTypeTaskType,\
    SmModelTaskType, SmModelCloudAccount
from smarfcontrolserver.init.envvar import SCS_ENV_VAR_CLOUD_SERVER_SHUTDOWN_HOLDOFF_SEC,\
    scsClearEnvVar, scsGetEnvVarBool, SCS_ENV_VAR_TEST_DISABLE_SLOW_TESTS
import os
import json
from sm_controller.api_cloud import scsApiCloudServerUsable
from smcommon.task_runners.mock_async_ops_server import SmMockAsyncOpsServer,\
    SmMockOpHandlerResponder
from sm_controller.api_task import SmTaskUtilsAsyncResps
from smcommon.globals.remote_agent_factory import smRemoteAgentAsyncOperationFactory
from smcommon.globals.envvar import SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE,\
    smCommonClearEnvVar, SM_ENV_VAR_COMMON_MIN_TIME_BETWEEN_PROGRESS,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_PREP_COUNT,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RUNNING_COUNT,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_CLEANUP_COUNT
from smarfcontrolserver.pollers.QueuedTaskStarter import ScsQueuedTaskStarter

logger = logging.getLogger(__name__) 
smTriggerSignalInstall()


class CloudTaskTest(TransactionTestCase):
    '''
    This task tests that starting a task which is configured
    to run on on a dynamic cloud server runs and will reused
    running cloud servers
    '''
    
    def makeTaskType(self, name, argList, computeResourceName, resource_type):
        tt = SmModelTaskType()
        tt.name = name
        tt.code_module = "smcommon.task_runners.task_runner_shell"
        tt.code_classname = "SmTaskRunnerShell"
        tt.configurationJson = json.dumps(
            {
                "args": argList,
                "computeResourceRole": computeResourceName,
                "skipRunPermCheck": False})
        tt.description = DUMMY_DESCRIPTION
        tt.owner = self.helper.superuser
        tt.save()
        rttt = SmModelResourceTypeTaskType()
        rttt.resource_type = resource_type
        rttt.task_type = tt
        rttt.role = "compute"
        rttt.name = name
        rttt.description = DUMMY_DESCRIPTION
        rttt.save()  
        return tt, rttt

    def setUp(self):
        smCommonClearEnvVar()
        scsClearEnvVar()
        self.helper = ScsTestHelper(self)
        self.helper.setupModel()
        self.helper.setupAwsCloudModel()
        self.helper.setupRestAuth("root")
        self.copyController = SmCopyController(["111", "testagentid"])
        smGlobalContextInit(self.copyController)
        self.factory = scsCloudServerIfFactoryGet()
        self.mockCloudIf = ScsCloudifMock()
        self.factory.setMockCloudIf(self.mockCloudIf) 
        self.cloudStatePoller = ScsCloudStatePoller(0.2, scsApiCloudServerUsable)
        self.cloudStatePoller.start()
        self.cloud_rt, self.cloud_resource = self.helper.makeCloudRes("cloud_name")
        self.cloud_task_type_ls, self.cloud_ls_rt3 = self.makeTaskType("cloud_ls",
                                                                       [{"type": "string", 
                                                                         "name": "Command",
                                                                         "value": "ls"}, 
                                                                        {"type": "string", 
                                                                         "name": "Argument",
                                                                         "value": "-dl"}, 
                                                                        {"type": "string", 
                                                                         "name": "Argument",
                                                                         "value": "/tmp"}, 
                                                                        {"type": "string", 
                                                                         "name": "Argument",
                                                                         "value": "/tmp"}, 
                                                                        ], "compute", self.cloud_rt)
            
    def tearDown(self):
        self.cloudStatePoller.stop()
        if scsAwsTestKeysDefined(None): 
            scsTerminataAllAwsCloudServers()
        self.helper.tearDown()
        TransactionTestCase.tearDown(self)
        
    def waitUntilServerState(self, cloud_server_path, expectedState, maxTime=1.0, sleepTime=0.1):    
        endTime = time.time() + maxTime
        while time.time() < endTime:
            try:
                resp = self.helper.objGetViaRest(cloud_server_path)
                if resp[SCS_CLOUDIF_STATE] == expectedState:
                    break
                time.sleep(sleepTime)
            except:
                break    
        self.assertEqual(resp[SCS_CLOUDIF_STATE], expectedState)

    def waitUntilTaskState(self, task_path, expectedState, maxTime=1.0, sleepTime=0.1):    
        endTime = time.time() + maxTime
        while time.time() < endTime:
            try:
                resp = self.helper.objGetViaRest(task_path)
                if resp[SCS_CLOUDIF_STATE] == expectedState:
                    break
                time.sleep(sleepTime)
            except:
                break    
        self.assertEqual(resp["state"], expectedState)

    def commonRunTaskTest(self, wait):
        # Create task
        self.task = SmModelTask(owner=self.helper.superuser, 
                                task_type=self.cloud_task_type_ls)
        self.task.parametersJson = json.dumps({"input_ids": {"1": {"path": "/tmp"}}})
        self.task.save()

        # Create task resources
        self.helper.makeTaskResource(self.task, self.cloud_resource, 
                              self.cloud_ls_rt3, self.helper.superuser, "compute")
        
        # Start task to create a file
        self.helper.objGetViaRest("/scs/task/" + str(self.task.id) + "/?action=start")
        
        # Wait until task is done
        self.helper.waitForTaskDone(self.task.id, wait[0], wait[1])
        
        self.task = SmModelTask.objects.get(id=self.task.id)
        self.assertEqual("success", self.task.completionReason)
    
    def test_awsCloudTask(self):
        return  # Commented out for speed
        self.assertTrue(os.path.exists("/etc/smarf-sra/keys/cloud_server_test.pem"),
                        "cloud_server_test.pem must be present in the folder /etc/smarf_sra/keys/cloud_server_test.pem")
        if scsGetEnvVarBool(SCS_ENV_VAR_TEST_DISABLE_SLOW_TESTS):
            logger.warning("test_awsCloudTask skipped because environnment variable %s is True",
                           SCS_ENV_VAR_TEST_DISABLE_SLOW_TESTS)
            return
        self.assertTrue(scsAwsTestKeysDefined("test_awsCloudTask cannot run"))
        self.factory.setMockCloudIf(None) 
        self.helper.setupRemoteAgentViaIce()
        self.commonRunTaskTest((90, 3))
        self.assertTrue("root" in self.task.stdout)
        self.assertTrue("/tmp" in self.task.stdout)
        
    def test_mockCloudTask(self): 
        os.environ[SM_ENV_VAR_COMMON_MIN_TIME_BETWEEN_PROGRESS] = "0"
        os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE] = "True"
        asyncRespObj = SmTaskUtilsAsyncResps()
        mockRaServer = SmMockAsyncOpsServer(asyncRespObj)
        mockRaServer.handlerClass = SmMockOpHandlerResponder
        smRemoteAgentAsyncOperationFactory.setMockAsyncOpsServer(mockRaServer)
        self.commonRunTaskTest((1, 0.1))

        resp = self.helper.objGetViaRest("/scs/resource/" + str(self.cloud_resource.id)
                                         + "/?extForUser=" + str(self.helper.superuser.id))
        self.assertEqual("unused", resp["ext_user_cloud_acct"])    

        # Change from default account with container to per-user cloud account
        self.helper.cloud_container.cloud_account = None
        self.helper.cloud_container.save()
        
        # Run again - will use cloud account linked to user
        self.commonRunTaskTest((1, 0.1))        
        resp = self.helper.objGetViaRest("/scs/resource/" + str(self.cloud_resource.id)
                                         + "/?extForUser=" + str(self.helper.superuser.id))
        self.assertEqual("used", resp["ext_user_cloud_acct"])    

        # Delete user's cloud account
        SmModelCloudAccount.objects.get(owner=self.helper.superuser).delete()
        resp = self.helper.objGetViaRest("/scs/resource/" + str(self.cloud_resource.id)
                                         + "/?extForUser=" + str(self.helper.superuser.id))
        self.assertEqual("missing", resp["ext_user_cloud_acct"])    
   
    def pathFromTask(self, task):
        return "/scs/task/" + str(task.id) + "/"
    
    def test_cloudServerReuse(self):
        # Commented out for speed
        return
        queuedTaskStarter = ScsQueuedTaskStarter(0.2)
        queuedTaskStarter.start()
        try:
            cloud_task_type_sleep, cloud_sleep_rt3 = self.makeTaskType("cloud_sleep",
                                                                       [{"type": "string", 
                                                                         "name": "Command",
                                                                         "value": "sleep"}, 
                                                                        {"type": "int", 
                                                                         "name": "Argument",
                                                                         "input_id": 1}, 
                                                                        ], "compute", self.cloud_rt)
            os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_PREP_COUNT] = "1"
            os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RUNNING_COUNT] = "1"
            os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_CLEANUP_COUNT] = "1"
            os.environ[SM_ENV_VAR_COMMON_MIN_TIME_BETWEEN_PROGRESS] = "0.5"
            os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE] = "True"
            asyncRespObj = SmTaskUtilsAsyncResps()
            mockRaServer = SmMockAsyncOpsServer(asyncRespObj)
            mockRaServer.handlerClass = SmMockOpHandlerResponder
            smRemoteAgentAsyncOperationFactory.setMockAsyncOpsServer(mockRaServer)
    
            # Delete the starting cloud server
            self.helper.objDeleteViaRest(self.helper.cloud_server_path)
            
            # Create sleep task
            paramsDict = {"input_ids": {"1": {"value": 1}}}   
            task = SmModelTask(owner=self.helper.superuser, 
                               task_type=cloud_task_type_sleep,
                               parametersJson=json.dumps(paramsDict))
            task.save()
            self.helper.makeTaskResource(task, self.cloud_resource, 
                                         cloud_sleep_rt3, self.helper.superuser,
                                         "compute")
                                       
            # Start task
            self.helper.objGetViaRest(self.pathFromTask(task) + "/?action=start")
              
            # Wait for it to finish
            self.waitUntilTaskState(self.pathFromTask(task), "finished", 10, 0.5)
            
            # Create sleep task
            task = SmModelTask(owner=self.helper.superuser, 
                               task_type=cloud_task_type_sleep,
                               parametersJson=json.dumps(paramsDict))
            task.save()
            self.helper.makeTaskResource(task, self.cloud_resource, 
                                         cloud_sleep_rt3, self.helper.superuser,
                                         "compute")
                                                                                                                                         
            # Start task
            self.helper.objGetViaRest(self.pathFromTask(task) + "/?action=start")
                                                   
            # Wait for it to finish
            self.waitUntilTaskState(self.pathFromTask(task), "finished", 10, 0.5)
            
            # Check that only one cloud_server was created and that the tasks
            # use the same cloud server
            resp = self.helper.objGetViaRest("/scs/cloud_server/")
            self.assertEqual(1, resp["meta"]["total_count"])
            first_cloud_server_path = resp["objects"][0]["resource_uri"]
            
            # Create 3 sleep tasks for 10 seconds
            paramsDict = {"input_ids": {"1": {"value": 10}}}   
                 
            task_path_list = []
            os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RUNNING_COUNT] = "6"
            for _ in range(0, 3):
                task = SmModelTask(owner=self.helper.superuser, 
                                   task_type=cloud_task_type_sleep,
                                   parametersJson=json.dumps(paramsDict))
                task.save()
                self.helper.makeTaskResource(task, self.cloud_resource, 
                                             cloud_sleep_rt3, self.helper.superuser,
                                             "compute")
                task_path_list.append(self.pathFromTask(task))
                
            # Start tasks
            for task_path in task_path_list:
                self.helper.objGetViaRest(task_path + "/?action=start")
       
            # Wait for them to complete
            for task_path in task_path_list:
                self.waitUntilTaskState(task_path, "finished", 15, 0.5)
    
            # Check that 2 additional cloud servers were created (3 in total)
            resp = self.helper.objGetViaRest("/scs/cloud_server/")
            self.assertEqual(3, resp["meta"]["total_count"])
            self.assertTrue(first_cloud_server_path in 
                            [resp["objects"][0]["resource_uri"],
                             resp["objects"][1]["resource_uri"],
                             resp["objects"][2]["resource_uri"]])
    
            # Wait 5 seconds to see shutdown
            os.environ[SCS_ENV_VAR_CLOUD_SERVER_SHUTDOWN_HOLDOFF_SEC] = "2"
            resp = self.helper.objGetViaRest("/scs/cloud_server/")
            time.sleep(3)
            
            for task_path in task_path_list:
                resp = self.helper.objGetViaRest(task_path)
                self.waitUntilServerState(resp["cloud_server"], SCS_CLOUDIF_STATE_STOPPED, 5, 1)
        finally:
            queuedTaskStarter.stop()
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()