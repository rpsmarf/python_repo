#!/bin/bash

if [ "$2" == "" ]; then
    echo "Syntax is $0 <dest folder> <count of files to generate>"
    echo " For example $0 ../../data/extractor_dataset1/ 1000"
    exit 1
fi

declare -A WIND_DIRS=( ["0"]="N" ["1"]="E" ["2"]="S" ["3"]="W")

DIR=$1
COUNT=$2


for i in `seq 1 $COUNT`;
do
    FILE=`printf data_%06d.dat $i`
    PATH=$DIR/$FILE
    echo Creating file $PATH
    TEMP=$(($i % 100))
    WIND_DIR=${WIND_DIRS["$(($i % 4))"]}
    echo " \"$FILE\": { \"name\": \"$FILE\", \"temperature\": $TEMP, \"winddir\": \"$WIND_DIR\"}" > $PATH
done