'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Feb 5, 2015

    @author: rpsmarf
'''
import logging
from django.contrib.auth.models import User, Group
from sm_controller.models import SmModelCommunity, SmModelTag, SmModelAgent,\
    SmModelContainer, SmModelTaskType, SmModelResourceType,\
    SmModelResourceTypeTaskType, SmModelResource, SmModelTask,\
    SmModelTaskResource, SmModelResourceTag, SmModelScript, SmModelMdExtractor,\
    SmModelMdRepo, SmModelPathList, SmModelMdProperty, SmModelMdExtractorType,\
    SmModelSchema, SmModelPropertyType, SmModelCloud, SmModelCloudAccount,\
    SmModelCloudServer
from smcommon.constants.sm_const import SM_COMMUNITY_ALL_NAME_KEY,\
    SM_TASK_TYPE_DELETE_RECURSIVE_NAME_KEY
from smcommon.utils.date_utils import sm_date_now
import json
from smcommon.globals.sm_global_context import smGlobalContextGet,\
    smGlobalContextInit, smGlobalContextClear
from smcommon.task_runners.ice_async_ops_server import SmIceAsyncOpsServer
from sm_controller.api_task import SmTaskUtilsAsyncResps
from smcommon.utils.net_utils import smGetFreePort
from smarfcontrolserver.init.envvar import scsClearEnvVar,\
    SCS_ENV_VAR_TEST_AWS_KEY_ID, scsGetEnvVar, SCS_ENV_VAR_TEST_AWS_SECRET_KEY,\
    scsLoadAwsKeys, SCS_ENV_VAR_TEST_DAIR_PASSWORD, scsLoadDairKeys
from smcommon.tests.dad_test_utils import TempFolder
from urllib.parse import urlparse
from smcommon.logging.sm_logger import smLoggingSetupForTest
from smcommon.globals.sm_copy_controller import SmCopyController
import time
from tastypie.models import ApiKey
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_AWS_PARAM_REGION_NAME,\
    SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID,\
    SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY,\
    SCS_CLOUDIF_AWS_PARAM_INSTANCE_TYPE, SCS_CLOUDIF_AWS_PARAM_KEYPAIR,\
    SCS_CLOUDIF_AWS_PARAM_IMAGE_ID, SCS_CLOUDIF_ID,\
    SCS_CLOUDIF_SWIFT_ACC_PASSWORD, SCS_CLOUDIF_SWIFT_ACC_USERNAME,\
    SCS_CLOUDIF_SWIFT_ACC_TOKEN_URL, SCS_CLOUDIF_SWIFT_ACC_TENANT
from smarfcontrolserver.cloudif.cloudif_aws import ScsCloudifAws
import urllib.parse

logger = logging.getLogger(__name__)
DUMMY_DESCRIPTION = "This is a dummy description which meets validation rules"


def scsAwsTestKeysDefined(message):
    scsLoadAwsKeys()
    if scsGetEnvVar(SCS_ENV_VAR_TEST_AWS_KEY_ID) == "unset":
        if message is not None:
            logger.warning("%s because AWS key id was not set as an environment variable (%s)",
                           message,
                           SCS_ENV_VAR_TEST_AWS_KEY_ID)
        return False
            
    if scsGetEnvVar(SCS_ENV_VAR_TEST_AWS_SECRET_KEY) == "unset":
        if message is not None:
            logger.warning("%s because AWS secret key was not set as an environment variable (%s)",
                           message,
                           SCS_ENV_VAR_TEST_AWS_SECRET_KEY)
        return False
    
    return True


def scsTerminataAllAwsCloudServers():
    logger.debug("Terminating all cloud servers...")
    cloud_info = {SCS_CLOUDIF_AWS_PARAM_REGION_NAME: "us-east-1"}
    cloud_account_info = {SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID: 
                          scsGetEnvVar(SCS_ENV_VAR_TEST_AWS_KEY_ID),
                          SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY: 
                          scsGetEnvVar(SCS_ENV_VAR_TEST_AWS_SECRET_KEY)
                          }
    cloudifObject = ScsCloudifAws(cloud_info, cloud_account_info)
    server_list = cloudifObject.get_server_list()
    for server in server_list:
        cloudifObject.terminate_server(server[SCS_CLOUDIF_ID])
    logger.debug("Terminated all cloud servers")
            

class ScsTestHelper(object):
    '''
    This object contains many useful methods for testing.
    It also contains various "setup" methods and a single "teardown"
    method which automatically destroys all things which were setup. 
    
    Key things this object provides:
    - 
    '''

    def __init__(self, testObject):
        '''
        Constructor
        '''
        scsClearEnvVar()       
        self.tempFolder = None
        self.testObject = testObject
        self.ic = None
        self.setupModelDone = False
        smLoggingSetupForTest()
    
    def setupTempFolder(self):
        if self.tempFolder is None:
            self.tempFolder = TempFolder()
    
    def setupModel(self):
        if self.setupModelDone:
            return
        
        self.setupTempFolder()
        
        Group.objects.create(name="all_users")
        
        """ Create set of objects"""
        superuser = User.objects.create_superuser("root", "a@b.com", "hello")
        self.superuser = superuser
        
        community = SmModelCommunity()
        community.name = 'All Community'
        community.name_key = SM_COMMUNITY_ALL_NAME_KEY
        community.owner = superuser
        community.description = "This is the global community to which all resources belong"
        community.save()
    
        # Create tag for community
        community_tag = SmModelTag()
        community_tag.tag_name = "community:" + SM_COMMUNITY_ALL_NAME_KEY
        community_tag.save()
            
        self.group1 = Group(name="g1")
        self.group1.save()
        
        self.user = User(username="userxxx", email="a@b.c")
        self.user.save()
        self.user.groups.add(self.group1)
        
        agent = SmModelAgent()
        agent.guid = "111"
        agent.name = "test_agent"
        agent.owner = self.user
        agent.last_change = str(sm_date_now())
        agent.status = "up"
        agent.agentUrl = "ice://localhost:123"
        agent.description = DUMMY_DESCRIPTION
        agent.save()
        
        container = SmModelContainer()
        container.agent = agent
        container.name = "User Storage"
        container.name_key = "user_storage"
        container.containerUrl = "local://localhost" + self.tempFolder.folder + "/"
        container.description = "Container storage object for user storage"
        container.owner = self.user
        container.save()
    
        container = SmModelContainer()
        container.agent = agent
        container.name = "test_container"
        container.description = DUMMY_DESCRIPTION
        container.containerUrl = "local://localhost" + self.tempFolder.folder + "/"
        container.owner = self.user
        container.status = "up"
        container.save()
    
        ttype = SmModelTaskType()
        ttype.name = "Main Task Type"
        ttype.configurationJson = json.dumps({"args": [],
                                              "computeResourceRole": "compute"})
        ttype.description = DUMMY_DESCRIPTION
        ttype.owner = self.user
        ttype.save()
        self.ttype = ttype
        self.ttype_path = "/scs/task_type/" + str(ttype.id) + "/"
        
        deleteTt = SmModelTaskType()
        deleteTt.name = "Delete"
        deleteTt.name_key = SM_TASK_TYPE_DELETE_RECURSIVE_NAME_KEY
        deleteTt.owner = self.user
        deleteTt.description = DUMMY_DESCRIPTION
        deleteTt.code_module = "smcommon.task_runners.task_runner_delete"
        deleteTt.code_classname = "SmTaskRunnerDelete"
        deleteTt.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"type": "resource",
                                      "name": "File to delete",
                                      "input_id": 1,
                                      "direction": "input", 
                                      "role": "storage"},
                                     ],
                            "computeResourceRole": "storage",
                            "skipRunPermCheck": True
                       }
                   )
        deleteTt.owner = self.user
        deleteTt.save()
    
        rtype = SmModelResourceType()
        rtype.name = "Generic Storage"
        rtype.name_key = "generic_storage"
        rtype.nature = "data"
        rtype.description = DUMMY_DESCRIPTION
        rtype.save()
        
        rtype = SmModelResourceType()
        rtype.name = "Resource Type Xxx"
        rtype.name_key = "default_resource"
        rtype.nature = "data"
        rtype.description = DUMMY_DESCRIPTION
        rtype.save()
        
        rttt = SmModelResourceTypeTaskType()
        rttt.resource_type = rtype
        rttt.task_type = deleteTt
        rttt.role = "storage"
        rttt.name = "storage"
        rttt.description = DUMMY_DESCRIPTION
        rttt.save()    
            
        rttt = SmModelResourceTypeTaskType()
        rttt.resource_type = rtype
        rttt.task_type = ttype
        rttt.role = "compute"
        rttt.name = "compute_name"
        rttt.description = DUMMY_DESCRIPTION
        rttt.save()
        
        rttt_src = SmModelResourceTypeTaskType()
        rttt_src.resource_type = rtype
        rttt_src.task_type = ttype
        rttt_src.role = "src"
        rttt_src.name = "src_name"
        rttt_src.description = DUMMY_DESCRIPTION
        rttt_src.save()
        
        rttt_dest = SmModelResourceTypeTaskType()
        rttt_dest.resource_type = rtype
        rttt_dest.task_type = ttype
        rttt_dest.role = "dest"
        rttt_dest.name = "dest_name"
        rttt_dest.description = DUMMY_DESCRIPTION
        rttt_dest.save()
        
        r = SmModelResource()
        r.resource_type = rtype
        r.name = "resourcexxx"
        r.name_key = "resourcexxx"
        r.parametersJson = json.dumps({"folder": "xxx/"})
        r.capacityJson = json.dumps({})
        r.owner = self.user
        r.container = container
        r.description = DUMMY_DESCRIPTION
        r.status = "up"
        r.save()
        self.resourceSleep = r
        
        t = SmModelTask()
        t.owner = self.user
        t.task_type = ttype
        t.status = "init"
        t.parametersJson = json.dumps({})
        t.start_time = str(sm_date_now())
        t.end_time = str(sm_date_now())
        t.save()
        
        taskResource = SmModelTaskResource()
        taskResource.task = t
        taskResource.resource = r
        taskResource.resource_type_task_type = rttt
        taskResource.owner = self.user
        taskResource.save()
        
        tag2 = SmModelTag()
        tag2.tag_name = "tagxxx"
        tag2.save()
        
        rtag = SmModelResourceTag()
        rtag.resource = r
        rtag.tag = tag2
        rtag.save()
        
        self.setupModelDone = True

    def setupZipUnzipTaskTypes(self):
        resource_type = SmModelResourceType.objects.get(name_key="default_resource")
        zipScript = SmModelScript()
        zipScript.key = "zip_script"
        zipScript.value = "mkdir -p `dirname $1`\n" + "cd `dirname $2`\n" +\
            "echo Doing zip -rdc `basename $2` $1\n" +\
            "zip -rdc $1 `basename $2`\n"
        zipScript.owner = self.user
        zipScript.save()
        zipTt = SmModelTaskType()
        zipTt.name = "zip"
        zipTt.code_module = "smcommon.task_runners.task_runner_zip"
        zipTt.code_classname = "SmTaskRunnerZip"
        zipTt.configurationJson = json.dumps(
            {
                "args": [
                    {"type": "string", 
                     "name": "Argument",
                     "value": "bash"}, 
                    {"type": "script", 
                     "name": "Argument",
                     "value": "zip_script"}, 
                    {"type": "resource", 
                     "name": "Argument",
                     "input_id": 1, 
                     "direction": "output", 
                     "role": "zip"}, 
                    {"type": "resource", 
                     "name": "Argument",
                     "input_id": 2, 
                     "direction": "input", 
                     "role": "zip"}], 
                "computeResourceRole": "zip",
                "skipRunPermCheck": True, 
                "workingDir": {"type": "resource", "input_id": 2, "role": "zip", "modifier": "parent"}})
        zipTt.description = DUMMY_DESCRIPTION
        zipTt.owner = self.user
        zipTt.save()
        rttt = SmModelResourceTypeTaskType()
        rttt.resource_type = resource_type
        rttt.task_type = zipTt
        rttt.role = "zip"
        rttt.name = "zip"
        rttt.description = DUMMY_DESCRIPTION
        rttt.save()
        unzipScript = SmModelScript()
        unzipScript.key = "unzip_script"
        unzipScript.value = "mkdir -p $1\n" + "cd $1\n" + "unzip $2\n"
        unzipScript.owner = self.user
        unzipScript.save()
        unzipTt = SmModelTaskType()
        unzipTt.name = "unzip"
        unzipTt.code_module = "smcommon.task_runners.task_runner_shell"
        unzipTt.code_classname = "SmTaskRunnerShell"
        unzipTt.configurationJson = json.dumps(
            {
                "args": [
                    {"type": "string", 
                     "name": "Argument",
                     "value": "bash"}, 
                    {"type": "script", 
                     "name": "Argument",
                     "value": "unzip_script"}, 
                    {"type": "resource", 
                     "name": "Argument",
                     "input_id": 1, 
                     "direction": "output", 
                     "role": "unzip"}, 
                    {"type": "resource", 
                     "name": "Argument",
                     "input_id": 2, 
                     "direction": "input", 
                     "role": "unzip"}], 
                "computeResourceRole": "unzip",
                "skipRunPermCheck": True})
        unzipTt.description = DUMMY_DESCRIPTION
        unzipTt.owner = self.user
        unzipTt.save()
        rttt = SmModelResourceTypeTaskType()
        rttt.resource_type = resource_type
        rttt.task_type = unzipTt
        rttt.role = "unzip"
        rttt.name = "unzip"
        rttt.description = DUMMY_DESCRIPTION
        rttt.save()
        
    def setupMetadataModel(self):
        if not self.setupModelDone:
            self.setupModel()
        
        resource = SmModelResource.objects.first()
        self.resource_path = "/scs/resource/" + str(resource.id) + "/"
        self.resource_type_path = "/scs/resource_type/" + str(resource.resource_type.id) + "/"
        
        # Create schema
        schema = SmModelSchema()
        schema.name = 'schema'
        schema.name_key = 'schema'
        schema.owner = self.superuser
        schema.namespace = str(time.time())
        schema.community = SmModelCommunity.objects.get(name_key=SM_COMMUNITY_ALL_NAME_KEY)
        schema.description = "lkasjdflkajsdlkasjdlasd"
        schema.resource_type = resource.resource_type
        schema.save()
        self.schema_path = "/scs/md_schema/" + str(schema.id) + "/"
        
        # Create prop types
        ptype1 = SmModelPropertyType()
        ptype1.name = "intProp"
        ptype1.name_key = "int_prop"
        ptype1.property_type = "INT"
        ptype1.schema = schema
        ptype1.save()
        ptype2 = SmModelPropertyType()
        ptype2.name = "stringProp"
        ptype2.name_key = "string_prop"
        ptype2.property_type = "STRING"
        ptype2.schema = schema
        ptype2.save()
        
        ttype = SmModelTaskType()
        ttype.name = "Extract Task Type"
        ttype.configurationJson = json.dumps({"args": [{"type": "string",
                                                        "name": "Command to run",
                                                        "value": "dummy"},
                                                       {"type": "string", 
                                                        "name": "URL", 
                                                        "input_id": 2},
                                                       {"type": "string", 
                                                        "name": "prefix", 
                                                        "input_id": 3},
                                                       {"type": "resource", 
                                                        "name": "File of control parameters", 
                                                        "pathType": "file", 
                                                        "input_id": 1, 
                                                        "direction": "input", 
                                                        "role": "src", 
                                                        "name": "Data to analyze",
                                                        "description": 
        "This specifies the folder of data to submit for analysis"},
                                                       ],
                                              "computeResourceRole": "compute"})
        ttype.description = DUMMY_DESCRIPTION
        ttype.owner = self.user
        ttype.save()
        
        # Create extractor_type
        et = SmModelMdExtractorType()
        et.name = "extr_type"
        et.name_key = "extr_type"
        et.description = "sldfkjsldkfjlks sdf sd fs df sdfdjf"
        et.schema = schema
        et.resource_type = resource.resource_type
        et.task_type = ttype
        et.save()
        self.extractor_type_path = "/scs/md_extractor_type/" + str(et.id) + "/"

        # Create repo
        repo = SmModelMdRepo()
        repo.name = "repo"
        repo.name_key = "repo"
        repo.owner = self.superuser
        repo.schema = schema
        repo.save()
        repo.resources.add(resource)
        repo.save()
        self.repo = repo
        self.repo_path = "/scs/md_repo/" + str(repo.id) + "/"
        
        # Create extractor
        ex = SmModelMdExtractor()
        ex.owner = self.superuser
        ex.repo = repo
        ex.extractor_type = et
        ex.compute_resource = resource
        ex.save()
        self.extractor_path = "/scs/md_extractor/" + str(ex.id) + "/"
        self.extractor = ex
        
        # Create 2 paths
        # Add 2 files with 2 properties each
        for i in range(0, 2):
            path = SmModelPathList()
            path.relative_path = "dir/f" + str(i)
            path.repo = repo
            path.resource = resource
            path.save()
            
            prop = SmModelMdProperty()
            prop.property_type = ptype1
            prop.path = path
            prop.value = str(i)
            prop.save()

            prop = SmModelMdProperty()
            prop.property_type = ptype2
            prop.path = path
            prop.value = 'AAA' + str(i)
            prop.save()
        
    def setupAwsCloudModel(self):
        if not self.setupModelDone:
            self.setupModel()
        scsLoadAwsKeys()
        
        # Create cloud
        cloud = SmModelCloud(name="Test", name_key="test",
                             description="Test cloud", cloud_type="aws")
        cloud.save()
        self.cloud_path = "/scs/cloud/" + str(cloud.id) + "/"
        
        # Create cloud account
        cloud_account = SmModelCloudAccount(owner=self.superuser, cloud=cloud,
                                            accountInfoJson=json.dumps(
                                                                       {SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID: 
                                                                        scsGetEnvVar(SCS_ENV_VAR_TEST_AWS_KEY_ID),
                                                                        SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY: 
                                                                        scsGetEnvVar(SCS_ENV_VAR_TEST_AWS_SECRET_KEY)}))
        cloud_account.save()
        self.cloud_account = cloud_account
        self.cloud_account_path = "/scs/cloud_account/" + str(cloud_account.id) + "/"
        
        container = SmModelContainer(agent=SmModelAgent.objects.first(),
                                     name="cloud_container",
                                     description="Container for cloud resources in Amazon Virginia DC",
                                     owner=self.superuser,
                                     cloud=cloud,
                                     containerUrl="ssh://server/tmp",
                                     cloud_account=cloud_account,
                                     parametersJson=json.dumps({SCS_CLOUDIF_AWS_PARAM_IMAGE_ID: "ami-d05e75b8",
                                                                SCS_CLOUDIF_AWS_PARAM_REGION_NAME: "us-east-1",
                                                                SCS_CLOUDIF_AWS_PARAM_INSTANCE_TYPE: "t2.micro",
                                                                SCS_CLOUDIF_AWS_PARAM_KEYPAIR: "cloud_server_test"}))
        container.save()
        self.cloud_container_path = "/scs/container/" + str(container.id) + "/"
        self.cloud_container = container
        
        # Create cloud server
        cloud_server = SmModelCloudServer(owner=self.superuser,
                                          cloud_account=cloud_account,
                                          host="4.5.6.7",
                                          container=container)
        cloud_server.save()
        self.cloud_server_path = "/scs/cloud_server/" + str(cloud_server.id) + "/"
                   
    def setupDairCloudModel(self):
        if not self.setupModelDone:
            self.setupModel()
        scsLoadDairKeys()
        
        # Create cloud
        cloud = SmModelCloud(name="DAIR", name_key="dair",
                             description="Test DAIR cloud", cloud_type="openstack")
        cloud.save()
        self.cloud_path = "/scs/cloud/" + str(cloud.id) + "/"
        
        # Create cloud account
        tenantString = urllib.parse.quote("NEP-Carleton University & Solana Networks")
        authTokenUrl = urllib.parse.quote("https://nova-ab.dair-atir.canarie.ca:35357/v2.0/")
        cloud_account = SmModelCloudAccount(owner=self.superuser, cloud=cloud,
                                            accountInfoJson=json.dumps(
                                                                       {SCS_CLOUDIF_SWIFT_ACC_PASSWORD: 
                                                                        scsGetEnvVar(SCS_ENV_VAR_TEST_DAIR_PASSWORD),
                                                                        SCS_CLOUDIF_SWIFT_ACC_USERNAME: 
                                                                        "Omelendez",
                                                                        SCS_CLOUDIF_SWIFT_ACC_TENANT: 
                                                                        tenantString,
                                                                        SCS_CLOUDIF_SWIFT_ACC_TOKEN_URL: 
                                                                        authTokenUrl,
                                                                        }))
        cloud_account.save()
        self.cloud_account = cloud_account
        self.cloud_account_path = "/scs/cloud_account/" + str(cloud_account.id) + "/"
        
        container = SmModelContainer(agent=SmModelAgent.objects.first(),
                                     name="cloud_container",
                                     description="Container for cloud resources in Amazon Virginia DC",
                                     owner=self.superuser,
                                     cloud=cloud,
                                     containerUrl="swift" + 
                                     "://swift-ab.dair-atir.canarie.ca:8080/v1" +
                                     "/AUTH_4c8f4711207b4b6bb74a1c200e8ac18b",
                                     cloud_account=cloud_account,
                                     parametersJson=json.dumps({}))
        container.save()
        self.cloud_container_path = "/scs/container/" + str(container.id) + "/"
        self.cloud_container = container
        
        # Create cloud server
        cloud_server = SmModelCloudServer(owner=self.superuser,
                                          cloud_account=cloud_account,
                                          host="4.5.6.7",
                                          container=container)
        cloud_server.save()
        self.cloud_server_path = "/scs/cloud_server/" + str(cloud_server.id) + "/"
                   
    def setupRemoteAgentViaIce(self):
        self.copyController = SmCopyController(["111", "testagentid"])
        smGlobalContextInit(self.copyController)
        self.ic = smGlobalContextGet().ic
        self.icePort = smGetFreePort()
        adapter = self.ic.createObjectAdapterWithEndpoints("smarf", "default -p " + str(self.icePort))
        self.asyncObj = SmIceAsyncOpsServer(self.ic, "SmRemAgAsyncOpResps:default -p " + str(self.icePort))
        adapter.add(self.asyncObj, self.ic.stringToIdentity("SmRemAgAsyncOps"))
        self.asyncRespObj = SmTaskUtilsAsyncResps()
        adapter.add(self.asyncRespObj, self.ic.stringToIdentity("SmRemAgAsyncOpResps"))
        adapter.activate()
        self.agent = SmModelAgent.objects.get(name="test_agent")
        self.agent.agentUrl = "ice://localhost:" + str(self.icePort)
        self.agent.save()

    def setupRestAuth(self, username):
        user = User.objects.get(username=username)
        api_key = ApiKey.objects.get(user_id=user).key
        self.auth_header = {"HTTP_AUTHORIZATION": "ApiKey " + user.username + ":" + api_key}
    
    def tearDown(self):
        scsClearEnvVar()       
        if self.ic is not None:
            # Clean up
            self.ic.destroy()
        smGlobalContextClear()
        if self.tempFolder is not None:
            self.tempFolder.cleanup()

    def objCreateViaRest(self, objectName, fieldDict):
        resp = self.testObject.client.post(objectName, json.dumps(fieldDict),
                                           "application/json", **self.auth_header)
        self.testObject.assertEqual(resp.status_code, 201, resp.content)
        loc = resp.get("location")
        obj_path = urlparse(loc).path
        return obj_path

    def objModifyViaRest(self, objectName, fieldDict):
        resp = self.testObject.client.patch(objectName, json.dumps(fieldDict),
                                            "application/json", **self.auth_header)
        self.testObject.assertEqual(resp.status_code, 202, resp.content)
        return resp

    def objDeleteViaRest(self, objectPath):
        resp = self.testObject.client.delete(objectPath, json.dumps({}),
                                             "application/json", **self.auth_header)
        self.testObject.assertEqual(resp.status_code, 204)

    def objGetViaRest(self, objectPath, **kwargs):
        if "data_to_upload" in kwargs:
            resp = self.testObject.client.get(objectPath, data=kwargs["data_to_upload"], **self.auth_header)
        else:            
            resp = self.testObject.client.get(objectPath, **self.auth_header)
        self.testObject.assertEqual(resp.status_code, 200, resp.content.decode("utf-8"))
        con = resp.content.decode("utf-8")
        respDict = json.loads(con)
        return respDict
       
    def getIdFromPath(self, path):
        return int(path.rsplit("/", 2)[1])
        
    def getTask(self, taskid):
        resp = self.testObject.client.get("/scs/task/" + str(taskid) + "/", **self.auth_header)
        task = json.loads(resp.content.decode("latin-1"))
        return task
    
    def waitForTaskDone(self, taskid, maxWait=3, sleepTime=0.1):
        count = maxWait / sleepTime
        while True:
            time.sleep(sleepTime)
            taskState = self.getTask(taskid)["state"]
            logger.debug("Got state %s for task %d", taskState, taskid)
            if taskState == "finished":
                break
            count -= 1
            if count == 0:
                raise Exception("Timeout waiting for task to complete")

    def makeTaskTypeSleep(self):
        taskType = SmModelTaskType()
        taskType.name = "sleeper"
        taskType.code_module = "smcommon.tests.task_runner_tester"
        taskType.code_classname = "SmTaskRunningTester"
        taskType.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"type": "float", 
                                      "name": "Argument",
                                      "input_id": 1},
                                     {"type": "boolean", 
                                      "name": "Argument",
                                      "input_id": 2},     
                                     ],
                            "computeResourceRole": "compute"
                       }
                   )
        taskType.description = DUMMY_DESCRIPTION
        taskType.owner = self.user
        taskType.save()
        self.taskTypeSleep = taskType
        
        rttt_storage = SmModelResourceTypeTaskType()
        rttt_storage.resource_type = self.resourceSleep.resource_type
        rttt_storage.task_type = self.taskTypeSleep
        rttt_storage.role = "compute"
        rttt_storage.name = "compute_sleep_name"
        rttt_storage.description = DUMMY_DESCRIPTION
        rttt_storage.save()
        self.rtttSleep = rttt_storage
        
    def makeSleepTask(self, user, waitTime):
        task = SmModelTask()
        task.owner = user
        task.task_type = self.taskTypeSleep
        task.parametersJson = json.dumps({"input_ids": {
                                                        "1": {"value": waitTime}, 
                                                        "2": {"value": True}}})
        task.save()
        
        taskResource = SmModelTaskResource()
        taskResource.task = task
        taskResource.resource = self.resourceSleep
        taskResource.resource_type_task_type = self.rtttSleep
        taskResource.role = "compute"
        taskResource.owner = user
        taskResource.save()
        
        return task
    
    def makeCloudRes(self, base_name):
        '''
        Method to make a cloud compute resource type and cloud resource, connected to the 
        cloud container created by the setupAwsCloudModel
        @return a tuple of resource type and resource objects
        '''
        rtype = SmModelResourceType()
        rtype.name = base_name
        rtype.name_key = base_name
        rtype.nature = "compute"
        rtype.description = DUMMY_DESCRIPTION
        rtype.save()
        r = SmModelResource()
        r.resource_type = rtype
        r.name = base_name
        r.name_key = base_name
        r.parametersJson = json.dumps({"folder": "xxx/", "keyfile": "cloud_server_test.pem",
                                       "user": "ubuntu"})
        r.capacityJson = json.dumps({})
        r.owner = self.user
        r.container = self.cloud_container
        r.description = DUMMY_DESCRIPTION
        r.status = "up"
        r.save()
        return rtype, r    
    
    def makeTaskResource(self, task, resource, rttt, user, role=None):
        taskResource = SmModelTaskResource()
        taskResource.task = task
        taskResource.resource = resource
        taskResource.resource_type_task_type = rttt
        if role is not None:
            taskResource.role = role
        taskResource.owner = user
        taskResource.save()
        return taskResource

    