describe('Metadata search functionality', function(){

	beforeEach(module('rpsmarf.metadata'));
	beforeEach(module('rpsmarf.config'));
	beforeEach(module('ui.router'));
	beforeEach(module('ui.bootstrap'));
	beforeEach(module('restangular'));
	beforeEach(module('rpsmarf.user.service'));

	var Restangular, $scope, $state, dataObj, SmUser, user, smarfurl, $httpBackend, $scopeInfoPanel;
	var dataPath = 'src/app/metadata/metadataSearch.spec.json', repo, props;

	//$scope, Restangular, $modal, SmUser, $state, USER_STATE, filterFilter, DEFAULT_METADATA_ICON_URL
	beforeEach(inject(function($rootScope, _Restangular_, _$state_, $controller, _SmUser_, SMARF_URL, _$httpBackend_){

		$scope = $rootScope.$new();
		$scope.metadataCollapsed = {value: true};
		smarfurl = SMARF_URL+'/scs/';
		Restangular = _Restangular_;
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
		$state = _$state_;
		SmUser = _SmUser_;
		user = SmUser.getObject();
		user.id = 2;
		user.community = {id: 3, name_key :'test_community'};
		$httpBackend = _$httpBackend_;
		dataObj = JSON.parse(__html__[dataPath]);

		$httpBackend.expect('GET', smarfurl + 'md_repo/?schema__community=3').respond(200,dataObj.repos);
		$controller('MetadataSearchCtrl', {$scope:$scope, Restangular: Restangular});

		$httpBackend.flush();

		repo  = angular.copy($scope.repos[0]);
		repo.selected = true;
		repo.owner_info = {name: 'Henry Tory', email: 'support@rpsmarf.ca'};

		props = angular.copy(dataObj.property_types.objects);

		repo.schemaPropertyTypes = props;

	}));

	it('should perform metadata search functionality', inject(function(){

		

		spyOn($scope, '$emit');
		$httpBackend.expect('GET', smarfurl+'md_property_type/?schema__id=1&limit=0').respond(200, dataObj.property_types);
		$httpBackend.expect('GET', smarfurl+'user/1/').respond(200, dataObj.repo_owner);
		$scope.selectRepo($scope.repos[0]);
		$httpBackend.flush();
		expect($scope.selectedRepo).toEqual(repo);
		expect($scope.availablePropertyTypesMaster).toEqual(props);
		expect($scope.repoSelected ).toBe('true');
		expect($scope.$emit).toHaveBeenCalledWith('SmObjectSelectInfoPanel', repo);

		//Perform search operations
		$scope.searchFilters[0].propertyType.selected = props[0];
		$scope.searchFilters[0].operationType.selected = $scope.availableOperationTypesMaster[0];
		$scope.searchFilters[0].propertyValue = '18';

		
		$scope.search();

		expect($scope.$emit).toHaveBeenCalledWith('SmMDSearch','?wind_speed__equals=18');

		$scope.addFilter();

		$scope.searchFilters[1].filterType.selected = 'Exclude';
		$scope.searchFilters[1].propertyType.selected = props[1];
		$scope.searchFilters[1].operationType.selected = $scope.availableOperationTypesMaster[3];
		$scope.searchFilters[1].propertyValue = '1';

		console.log($scope.searchFilters);
		$scope.search();

		expect($scope.$emit).toHaveBeenCalledWith('SmMDSearch','?wind_speed__equals=18&-temperature__lt=1');



	}));


	describe('info panel functionality', function(){
		var $scopeInfoPanel, searchResults, fileSetItems;
		beforeEach(inject(function($controller, $rootScope){
			$scopeInfoPanel = $rootScope.$new();
			$controller('MetadataSearchInfoPanelCtrl',{$scope: $scopeInfoPanel, Restangular: Restangular});
			searchResults = angular.copy(dataObj.search_results); 
			fileSetItems = angular.copy(dataObj.file_set_items.objects);
			fileSetItems[0].resourceName = 'copy_src';
			fileSetItems[1].resourceName = 'copy_src';
		}));

		it('should update the footer info panel', inject(function(){

			//Selecting a repo
			$httpBackend.expect('GET', smarfurl+'md_schema_full/1/').respond(200,dataObj.schema_full);
			$scopeInfoPanel.$broadcast('SmInfoPanelUpdate', repo);
			expect($scopeInfoPanel.mdSearchResults).toBe(null);
			expect($scopeInfoPanel.queryTriggered).toBe('false');
			expect($scopeInfoPanel.selectedRepo).toEqual(repo);
			$httpBackend.flush();
			expect($scopeInfoPanel.selectedSchema.owner.name).toBe('Henry Tory');
			expect($scopeInfoPanel.select.first).toBe(true);

			//Performing a search
			$httpBackend.expect('GET', smarfurl+'md_repo/1/search/').respond(200,dataObj.search_results);
			$httpBackend.expect('GET', smarfurl+'file_set_item/?file_set=1&offset=0&limit=10&showProps=true').respond(200, dataObj.file_set_items);
			$httpBackend.expect('GET', smarfurl+'resource/1/').respond(200, dataObj.file_set_resource);
			$scopeInfoPanel.$broadcast('SmMDSearchUpdate', '');
			expect($scopeInfoPanel.select.second).toBe(true);
			expect($scopeInfoPanel.queryTriggered).toBe('true');
			expect($scopeInfoPanel.select.first).toBe(false);
			expect($scopeInfoPanel.query).toBe('');
			$httpBackend.flush();
			expect($scopeInfoPanel.fileSet).toEqual(searchResults.file_set);
			expect($scopeInfoPanel.records).toBe(2);
			expect($scopeInfoPanel.resultsPage.value).toBe(1);
			
			expect($scopeInfoPanel.fileSetItems).toEqual(fileSetItems);

			expect($scopeInfoPanel.resultsInfo()).toBe('Showing results 1 - 2 of 2');

		}));
		
	});

	describe('results actions functionality', function(){

		var $scopeResultsDownload, fileSet, $scopeResourceCopy,copyResources;
		beforeEach(inject(function($rootScope, $controller, filterFilter){
			$scopeResultsDownload = $rootScope.$new();
			$scopeResourceCopy = $rootScope.$new();
			fileSet = {id:45};
			copyResources = angular.copy(filterFilter(dataObj.copy_resources.objects,function(value,index){return value.nature === 'data' && value.ext_perms.indexOf('w') >=0;},false));
			$controller('MetadataSearchDownloadCtrl',{$scope: $scopeResultsDownload, Restangular: Restangular, fileSet:fileSet, $modalInstance: {}});
			//Restangular.all('resource/?communityFilter='+community+'&extForUser='+uid+'&limit=0&locked=false&status=up').get('').then(function(response){
			$httpBackend.expect('GET', smarfurl+'resource/?communityFilter=test_community&extForUser=2&limit=0&locked=false&status=up').respond(200, dataObj.copy_resources );
			$controller('MetadataSearchCopyToResourceCtrl',{$scope: $scopeResourceCopy, Restangular: Restangular, fileSet: fileSet, records: 46, $modalInstance: {} });
			$httpBackend.flush();
		}));

		it('should download the results in different formats', inject(function(){
			spyOn($window, 'open');
			$scopeResultsDownload.downloadType = 'json';
			$scopeResultsDownload.download();			
			expect($window.open).toHaveBeenCalledWith(smarfurl+'file_set_item/?file_set__id=45&downloadAsFile=True');

			$scopeResultsDownload.downloadType = 'csv';
			$scopeResultsDownload.download();
			expect($window.open).toHaveBeenCalledWith(smarfurl+'file_set_item/?file_set__id=45&format=csv');

			$scopeResultsDownload.downloadType = 'zip';
			$scopeResultsDownload.download();
			expect($window.open).toHaveBeenCalledWith(smarfurl+'file_set/45/downloadzip/');

		}));

		it('should copy the results to a resource', inject(function(){
			
			expect($scopeResourceCopy.resources).toEqual(copyResources);
			expect($scopeResourceCopy.records).toBe(46);

			$scopeResourceCopy.selectedResource.value = copyResources[2];
			//Restangular.all('resource/'+$scope.selectedResource.value.id+'/copy_from_file_set/?file_set='+fileSet.id).get('').then(function(response){
			$httpBackend.expect('GET', smarfurl+'resource/7/copy_from_file_set/?file_set=45').respond(200, dataObj.copy_to_resource);
			$scopeResourceCopy.copyToResource();
			$httpBackend.flush();
			expect($scopeResourceCopy.copyStarted).toBe(true);
			expect($scopeResourceCopy.copy.dest_path).toBe('file_set_20150826T145805Z');
			expect($scopeResourceCopy.copy.tasks.length).toBe(2);

		}));
	});
	




});