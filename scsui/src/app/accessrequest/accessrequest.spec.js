describe('access request module (rpsmarf.accessrequest)', function(){

	beforeEach(module('rpsmarf.config'));
	beforeEach(module('rpsmarf.accessrequest'));
	beforeEach(module('rpsmarf.user.service'));
	beforeEach(module('restangular'));

	var $scope,object, SmUser, user, resource, task_type, Restangular, smarfurl, $httpBackend, $controller;

	resource = {
				"capacityJson": "{}",
				"container": "/scs/container/1/",
				"description": "This is your cloud files storage.",
				"gid": 19,
				"id": 1,
				"imageUrl": "https://www.rpsmarf.ca/icons-scs/defaultResourceIcon.png",
				"locked": false,
				"name": "Common Cloud Storage",
				"name_key": "demo_cloud_files",
				"nature": "data",
				"owner": "/scs/user/1/",
				"parametersJson": "{\"folder\": \"files/\"}",
				"personalFolder": false,
				"resource_type": "/scs/resource_type/1/",
				"resource_uri": "/scs/resource/1/",
				"status": "up"
				};

	task_type = {
				"code_classname": "SmTaskRunnerZip",
				"code_module": "smcommon.task_runners.task_runner_zip",
				"configurationJson": "{\"workingDir\": {\"type\": \"resource\", \"role\": \"zip\", \"modifier\": \"parent\", \"input_id\": 2}, \"args\": [{\"type\": \"string\", \"value\": \"zip\", \"name\": \"Argument\"}, {\"type\": \"string\", \"value\": \"-rdc\", \"name\": \"Argument\"}, {\"direction\": \"output\", \"name\": \"ZIP file to create\", \"role\": \"zip\", \"type\": \"resource\", \"pathType\": \"file\", \"description\": \"The name of the zip file which is created by the zip task\", \"input_id\": 1}, {\"modifier\": \"filename\", \"direction\": \"input\", \"name\": \"Folder to zip up\", \"role\": \"zip\", \"type\": \"resource\", \"input_id\": 2, \"description\": \"The name of the folder whose contents are zipped into a file\", \"pathType\": \"folder\"}], \"computeResourceRole\": \"storage\"}",
				"description": "Task to zip up files",
				"gid": 20,
				"id": 1,
				"imageUrl": "https://www.rpsmarf.ca/icons-scs/defaultTaskTypeIcon.png",
				"locked": false,
				"name": "zip",
				"name_key": "zip",
				"owner": "/scs/user/1/",
				"resource_uri": "/scs/task_type/1/",
				"uiVisible": false
				};
	beforeEach(inject(function($rootScope,_$controller_,_SmUser_, _Restangular_, SMARF_URL, _$httpBackend_){

		$scope = $rootScope.$new();
		SmUser = _SmUser_;
		smarfurl = SMARF_URL+'/scs/';
		Restangular = _Restangular_;
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
		user = SmUser.getObject();
		user.id='32', user.firstname='Henry', user.lastname='Tory';
		$httpBackend = _$httpBackend_;
		$controller = _$controller_;

	}));

	it('should send the proper request access messages', inject(function(){
		var reason;
		//Request access to resource
		object = angular.copy(resource);
		object.userPerm = [{name:'read', hasPerm: false},{name:'write', hasPerm: true},{name:'execute', hasPerm: false}];
		$controller('AccessRequestCtrl', {$scope:$scope, object:object, type: 'resource', Restangular: Restangular, $modalInstance: null });

		expect($scope.state).toBe('requesting');
		expect($scope.requestReason.text).toBe('Hi, my name is Henry Tory and I would like access to Common Cloud Storage in order to....');

		$scope.request.read = true;
		$scope.request.execute = true;
		reason = 'My reason';
		$scope.requestReason.text = reason;

		$httpBackend.expect('GET', smarfurl+'resource/1/requestaccess/?access=rx&reason='+reason.replace(/\s/g,'+')+'&userid=32').respond(200);
		$scope.request();
		$httpBackend.flush();

		expect($scope.state).toBe('complete');

		//Request access to task_type
		object = angular.copy(task_type);
		$controller('AccessRequestCtrl', {$scope:$scope, object:object, type: 'task_type', Restangular: Restangular, $modalInstance: null });

		expect($scope.state).toBe('requesting');
		expect($scope.requestReason.text).toBe('Hi, my name is Henry Tory and I would like access to zip in order to....');

		reason = 'My reason for task_type';
		$scope.requestReason.text = reason;

		$httpBackend.expect('GET', smarfurl+'task_type/1/requestaccess/?access=x&reason='+reason.replace(/\s/g,'+')+'&userid=32').respond(200);
		$scope.request();
		$httpBackend.flush();

		expect($scope.state).toBe('complete');

	}));
});