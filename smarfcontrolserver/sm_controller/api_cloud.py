'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 28, 2015

    @author: rpsmarf
'''

import logging
import json
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_TYPE,\
    SCS_CLOUDIF_AWS_PARAM_REGION_NAME, SCS_CLOUDIF_STATE_ERROR, SCS_CLOUDIF_ID,\
    SCS_CLOUDIF_HOST, SCS_CLOUDIF_STATE, SCS_CLOUDIF_STATE_STOPPED,\
    SCS_CLOUDIF_STATE_USABLE
from tastypie.http import HttpBadRequest
from tastypie.exceptions import ImmediateHttpResponse
from smarfcontrolserver.cloudif.cloudif_factory import scsCloudServerIfFactoryGet
from sm_controller.models import SmModelTask
import sm_controller

logger = logging.getLogger(__name__)


def scsMakeCloudInfo(cloud_server):
    cloud = cloud_server.cloud_account.cloud
    try:
        containerParamsDict = json.loads(cloud_server.container.parametersJson)
        cloud_info = {SCS_CLOUDIF_TYPE: cloud.cloud_type,
                      SCS_CLOUDIF_AWS_PARAM_REGION_NAME: containerParamsDict[SCS_CLOUDIF_AWS_PARAM_REGION_NAME]
                      }
        cloud_account_info = json.loads(cloud_server.cloud_account.accountInfoJson)
        return (cloud_info, cloud_account_info)
    except:
        logger.exception("Invalid parametersJson field %s in container '%s'", 
                         cloud_server.container.parametersJson,
                         str(cloud_server.container))
        raise


def scsMakeCloudParamInfo(cloud_server):
    try:
        containerParamsDict = json.loads(cloud_server.container.parametersJson)
        return containerParamsDict
    except:
        logger.exception("Invalid parametersJson field %s in container '%s'", 
                         cloud_server.container.parametersJson,
                         str(cloud_server.container))
        raise


def scsCloudServerStart(cloud_server_object):
    logger.debug("Starting cloud server for object %s", str(cloud_server_object))
    cloud_info, cloud_account_info = scsMakeCloudInfo(cloud_server_object)
    cloudIfObj = scsCloudServerIfFactoryGet().getCloudServerIf(cloud_info, cloud_account_info)
    paramDict = scsMakeCloudParamInfo(cloud_server_object)
    try:
        server_dict = cloudIfObj.create_server(paramDict)
        logger.debug("Cloud server '%s' started", str(cloud_server_object))
    except Exception as e:
        logger.exception("Failed to start cloud server %s", str(cloud_server_object))
        cloud_server_object.state = SCS_CLOUDIF_STATE_ERROR 
        cloud_server_object.last_error = str(e)
        cloud_server_object.save()
        raise ImmediateHttpResponse(HttpBadRequest(str(e)))
        
    cloud_server_object.server_id = server_dict[SCS_CLOUDIF_ID] 
    cloud_server_object.host = server_dict[SCS_CLOUDIF_HOST] 
    cloud_server_object.state = server_dict[SCS_CLOUDIF_STATE] 
    cloud_server_object.save()

 
def scsApiCloudServerUsable(cloud_server):
    '''
    This function is invoked when the cloud state poller notices that
    that a server has transitioned to usable.  This causes us to look
    at the list of tasks to see if anything can now start to run. 
    @return: the resulting state of the cloud server
    '''
    
    # Get a list of tasks waiting for this cloud server to come free
    logger.debug("Cloud server %s is now usable, looking for tasks to run",
                 str(cloud_server))
    task_list = SmModelTask.objects.filter(cloud_server=cloud_server).filter(state="booting")
    if task_list.count() > 0:          
        task = task_list.first()
        logger.debug("Starting task %s on cloud server %s",
                     task, cloud_server)
        sm_controller.api_task.scsApiTaskStartTask(task, None, True)
        return task.cloud_server.state
    return SCS_CLOUDIF_STATE_USABLE
        

def scsCloudServerStop(cloud_server_object):
    logger.debug("Stopping cloud server '%s'", str(cloud_server_object))
    cloud_info, cloud_account_info = scsMakeCloudInfo(cloud_server_object)
    cloudIfObj = scsCloudServerIfFactoryGet().getCloudServerIf(cloud_info, cloud_account_info)
    cloudIfObj.terminate_server(cloud_server_object.server_id)
    logger.debug("Cloud server '%s' stopped", str(cloud_server_object))
    server_info = cloudIfObj.get_server_info(cloud_server_object.server_id)
    if server_info is None:
        cloud_server_object.state = SCS_CLOUDIF_STATE_STOPPED
    else:
        cloud_server_object.state = server_info[SCS_CLOUDIF_STATE]
    cloud_server_object.save()
 
    
