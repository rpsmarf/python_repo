'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Apr 24, 2015

    @author: rpsmarf
'''
import logging
import requests
from smarfcontrolserver.init.envvar import SCS_ENV_VAR_MAILGUN_FROM,\
    SCS_ENV_VAR_MAILGUN_REPLY_TO, SCS_ENV_VAR_MAILGUN_URL,\
    SCS_ENV_VAR_MAILGUN_API_KEY, scsGetEnvVar

logger = logging.getLogger(__name__)

REQUEST_ACCESS_EMAIL_TEMPLATE = \
    "Hello,\n" + \
    "%requesterFirstName% %requesterLastName% (email %requesterEmail%) has " + \
    "requested access to the %objectType% '%objectName%'.\n\n" + \
    "Access requested: %access%\n" + \
    "Explanation for access request:\n" + \
    "  %reason%" + \
    "\n\n" + \
    "To grant access please log in to RP-SMARF and browse to this %objectType% " + \
    "and click on the 'Permission' tab and add this user.\n" + \
    "\n" + \
    "              Regards,\n" + \
    "                       The RP-SMARF Team."
    
REQUEST_ACCESS_EMAIL_SUBJECT = "RP-SMARF Request for Access"
    
ACCESS_GRANTED_EMAIL_TEMPLATE = \
    "Hello,\n" + \
    "%ownerFirstName% %ownerLastName% (email %ownerEmail%) has " + \
    "granted you access to the %objectType% '%objectName%'.\n\n" + \
    "Access granted: %perm0% %perm1% %perm2% \n" + \
    "\n" + \
    "              Regards,\n" + \
    "                       The RP-SMARF Team."
    
ACCESS_GRANTED_EMAIL_SUBJECT = "RP-SMARF Access Granted"
    
ACCESS_REMOVED_EMAIL_TEMPLATE = \
    "Hello,\n" + \
    "%ownerFirstName% %ownerLastName% (email %ownerEmail%) has " + \
    "removed your access to the %objectType% '%objectName%'.\n\n" + \
    "Access removed:  %perm0% %perm1% %perm2%\n" + \
    "\n" + \
    "              Regards,\n" + \
    "                       The RP-SMARF Team."
    
ACCESS_REMOVED_EMAIL_SUBJECT = "RP-SMARF Access Removed"
    
ACCESS_DENIED_EMAIL_TEMPLATE = \
    "Hello,\n" + \
    "%ownerFirstName% %ownerLastName% (email %ownerEmail%) chose " + \
    "not to grant you access to the %objectType% '%objectName%'.\n\n" + \
    "\n" + \
    "              Regards,\n" + \
    "                       The RP-SMARF Team."
    
ACCESS_DENIED_EMAIL_SUBJECT = "RP-SMARF Access Was Not Granted"
    
    
EXTRACTION_SUCCESSFUL_TEMPLATE = \
    "Hello,\n\n" + \
    "Metadata extraction on the repository %repoName% completed successfully.\n" + \
    "\n" + \
    "The extraction took %duration%\n" + \
    "\n" + \
    "              Regards,\n" + \
    "                       The RP-SMARF Team."
    
EXTRACTION_SUCCESSFUL_SUBJECT = "RP-SMARF Metadata Extraction Completed Successfully"
    
    
EXTRACTION_FAILURE_TEMPLATE = \
    "Hello,\n\n" + \
    "Metadata extraction on the repository %repoName% failed.\n" + \
    "\n" + \
    "The extraction took %duration%\n" + \
    "\n" + \
    "Failure reasons include:" + \
    "%failureReasons%\n" + \
    "\n" + \
    "              Regards,\n" + \
    "                       The RP-SMARF Team."
    
EXTRACTION_FAILURE_SUBJECT = "RP-SMARF Metadata Extraction FAILED"
    
       
_scsMailer = None


def scsGetGlobalMailer():
    global _scsMailer
    return _scsMailer

    
def scsSetGlobalMailer(mailer):
    global _scsMailer
    _scsMailer = mailer

    
class ScsMailer(object):
    '''
    This is a global object which implements a object to send mail to users via Mailgun.
    '''

    def __init__(self):
        '''
        Constructor.  All configuration values are passed as environment variables.
        See https://rpsmarf.atlassian.net/wiki/display/RPS/Setup+Mailgun+API+Access+for+Email+Sending
        for details.
        '''
        pass
        
    def merge(self, rpsmarf_template_dir, params):
        body = rpsmarf_template_dir
        for key, value in params.items():
            body = body.replace(key, value)
        return body
        
    def sendEmail(self, destEmail, subject, body):
        apiKey = scsGetEnvVar(SCS_ENV_VAR_MAILGUN_API_KEY)
        if apiKey is None:
            logger.warning("Property SCS_MAILGUN_API_KEY not configured - no email sent to %s", destEmail)
            logger.warning("See https://rpsmarf.atlassian.net/wiki/display/RPS/Setup+Mailgun+API+Access+for+Email+Sending")
            logger.warning("for configuration instructions.")
            return 
        
        resp = requests.post(scsGetEnvVar(SCS_ENV_VAR_MAILGUN_URL),
                             auth=("api", apiKey),
                             data={"from": scsGetEnvVar(SCS_ENV_VAR_MAILGUN_FROM),
                                   "to": [destEmail],
                                   "h:Reply-To": scsGetEnvVar(SCS_ENV_VAR_MAILGUN_REPLY_TO),
                                   "subject": subject,
                                   "text": body})
        if resp.status_code != 200:
            logger.error("Email sending to mailgun returned %d (url = %s)", resp.status_code, 
                         scsGetEnvVar(SCS_ENV_VAR_MAILGUN_URL))
            logger.error(" Content: %s", resp.content.decode("utf-8"))
        else:
            logger.debug("Email send to %s with subject %s", destEmail, subject)
            
            
class ScsMockMailer(ScsMailer):
    '''
    This is used during unit testing to very that emails were sent as expected
    '''
    def __init__(self):
        self.email_sent_list = []
        super().__init__()
        
    def sendEmail(self, destEmail, subject, body):
        self.email_sent_list.append({"email": destEmail,
                                     "subject": subject,
                                     "body": body})

    
                        