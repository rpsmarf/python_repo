angular.module( 'rpsmarf.mkdir', [
	'ui.bootstrap'
])

.controller( 'MkdirCtrl', ['$scope', '$modalInstance', 'action', function($scope, $modalInstance, action){

	/***STANDARD VARIABLEs***/
	var patt = /(\\|\/|:|\*|\?|\"|<|>|\|)/;

	/***SCOPE VARIABLEs***/
	$scope.formError = false;
	$scope.title = '';
	$scope.action = action;
	/***SCOPE FUNCTIONS***/
	$scope.ok = function(){
		if(!$scope.folderName || patt.test($scope.folderName)){
			$scope.formError = true;
			return;
		}
		if($scope.folderName.substring($scope.folderName.length-4) === '.zip'){
			$scope.folderName = $scope.folderName.substring(0,$scope.folderName.length-4);
		}
		$modalInstance.close($scope.folderName);
	};

	$scope.cancel = function(){
		$modalInstance.dismiss('cancel');
	};

	/***EXE***/
	switch(action){
		case 'zip':
			$scope.title = 'Name zip file';
			break;
		default:
			$scope.title = 'Name folder';
	}
}])
;
