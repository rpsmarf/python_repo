import json
import os
import sys
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath, scsGetToJson
from scripts.scs_obj_create import scsCreateObjectPath

try:
    os.mkdir("/tmp/src")
except:
    pass
try:
    os.mkdir("/tmp/dest")
except:
    pass

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

root_path = scsGetObjectPath("user", "username=rpsmarf")
user_a = scsGetObjectPath("user", "username=a")
user_b = scsGetObjectPath("user", "username=b")


carleton_container_path = scsGetObjectPath("container", "name_key=carleton_demos_data") 
dair_ab_container_path = scsGetObjectPath("container", "name_key=testab1_demos_data")
rt_cloud_files_data_path = scsGetObjectPath("resource_type", "name_key=generic_storage")


# Setting up Asteroids demo
TASK_TYPE_ASTEROIDS_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", "name": "Argument", "value": "bash"},
                                     {"type": "string", "name": "Argument", "value": "asteroids.sh"},
                                     ],
                            "computeResourceRole": "compute",
                            "interactive": True,
                            "workingDir": {"type": "string", "value": "/opt/smarf-demos/demos/git_demo/asteroids_git_demo"},
                            "versioning": "git"
                       }

tt_path = scsCreateObjectPath("task_type", {"name": "Asteroids Game",
                                            "name_key": "asteroids",
                                            "code_module": "smcommon.task_runners.task_runner_shell",
                                            "code_classname": "SmTaskRunnerShell",
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_ASTEROIDS_CFG_DICT),
                                            "description": "Allows the user to run a game with different versions"})
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

rt_compute_path = scsCreateObjectPath("resource_type", 
                                      {"name": "Asteroid Processor", 
                                       "name_key": "asteroid_processor",
                                       "nature": "compute",
                                       "description": "This is a resource which can run the video " +
                                       "playing application"
                                       })
scsCreateObjectPath("resource_type_task_type", 
                    {"role": "compute", 
                     "task_type": tt_path, 
                     "resource_type": rt_compute_path,
                     "nature": "compute",
                     "name": "Asteroid Compute RT3",
                     "name_key": "asteroids_compute_rt3",                                         
                     "description": "This is the compute resource for the video capture tool"})

res_path = scsCreateObjectPath("resource", {"name": "Game Runner at DAIR Alberta",
                                            "name_key": "asteroid_compute", 
                                            "resource_type": rt_compute_path,
                                            "owner": root_path,
                                            "container": dair_ab_container_path,
                                            "parametersJson": 
                                            json.dumps({"taskViewBaseUrl": 
                                                        "http://testab1.rpsmarf.ca:8080/guacamole" +
                                                        "/#/client/c/default"}),
                                            "description": "This is the DAIR-Alberta-based computation " + 
                                            "resource to run the Asteroid game"
                                            }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

 
print("Task type 'stream' objects setup.")
