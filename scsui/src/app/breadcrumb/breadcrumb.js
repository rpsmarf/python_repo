angular.module( 'rpsmarf.taskbreadcrumb', [
  'ui.router',
  'placeholders',
  'ui.bootstrap',
  'ngSanitize'
])

.config(function config( $stateProvider ) {
  // $stateProvider.state( 'about', {
  //   url: '/about',
  //   views: {
  //     "main": {
  //       controller: 'AboutCtrl',
  //       templateUrl: 'about/about.tpl.html'
  //     }
  //   },
  //   data:{ pageTitle: 'What is It?' }
  // });
})

.controller( 'BreadcrumbCtrl', ['$scope', function ( $scope ) {
  
  /***VARIABLES***/
  /*Scope*/
  $scope.crumbLevel = 0;
  $scope.crumbs = [
  {name:'Home', stateName:'home'}, 
  {name:'Tools', stateName:'tools'},
  {name:'Task Configuration', stateName:'taskstart'}, 
  {name:'Resource Configuration', stateName:'resourceconfig.taskstart'}];
  /*Standard*/
  
  var communitiescrumbs =  [ {name:'Home', stateName:'home'}, {name:'Comunities', stateName:'communities'}, {name:'Community Information', stateName:'communityinfo'}];
  var toolscrumbs = [  {name:'Home', stateName:'home'}, {name:'Tools', stateName:'tools'}, {name:'Task Configuration', stateName:'taskstart'}, {name:'Resource Configuration', stateName:'resourceconfig.taskstart'}]; 
  $scope.crumbs = toolscrumbs; 
  /***FUNCTIONS***/
  /*Scope*/
  $scope.openIssueCollector = function(){
    angular.element('#'+$scope.issueCollectorId).trigger('SmOpenIssueCollector');
  };

  /***EXE***/
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    switch(toState.name){
      case 'tools':
        $scope.crumbLevel = 1;
        $scope.crumbs = toolscrumbs; 
        break;
      case 'tools.reservation':
        $scope.crumbLevel = 1;
        break;
      case 'taskstart':
        if(fromState.name && fromState.name.indexOf('reservation') >=0){
          $scope.crumbs[1].stateName = 'tools.reservation';
        }
        $scope.crumbLevel = 2;
        break;
      case 'taskstart.reservation':
        $scope.crumbs[2].stateName = 'taskstart.reservation';
        $scope.crumbLevel = 2;
        $scope.crumbs = toolscrumbs; 
        break;
      case 'resourceconfig.taskstart':
        $scope.crumbLevel = 3;
        $scope.crumbs = toolscrumbs; 
        break;
      case 'communities':
        $scope.crumbLevel = 1;
        $scope.crumbs = communitiescrumbs;
        break;     
      case 'communityinfo':
        $scope.crumbLevel = 2;
        $scope.crumbs = communitiescrumbs;
        break;     
      default:
        $scope.crumbLevel =0;
    }
  });

  
}])

;
