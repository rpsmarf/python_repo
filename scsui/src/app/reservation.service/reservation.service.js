angular.module('rpsmarf.reservation.service',[
	'restangular',
	'rpsmarf.user.service',
	'rpsmarf.task.create.service',
	'ui.bootstrap',
	'ui.router',
	'rpsmarf.confirm',
	'rpsmarf.alert'

])
.service('SmReservation',['Restangular', '$q', 'SmUser', 'SmTaskCreate', '$modal','$state','$interval', function(Restangular, $q, SmUser, SmTaskCreate, $modal, $state, $interval){

	var reservation;
	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var reservationParameters;

	var setReservation = function(r){
		if(angular.isObject(r)){
			reservation = r;
		}
		
	};

	var getReservation = function(){
		return reservation;
	};

	var hasReservation = function(){
		return angular.isObject(reservation);
	};

	var reset = function(){
		reservation = undefined;
		reservationParameters = undefined;
	};

	var getTaskTypes = function(){
		if(!angular.isObject(reservation)){
			return null;
		}
		var rid = reservation.resource.id;
		var rtid;
		var promises = [];
		var ttid;
		var uid = SmUser.getProperty('id');
		return Restangular.one('resource',rid+'/').get().then(function(obj){
			rtid = obj.resource_type.split('/')[3];
			return Restangular.all('resource_type_task_type/?resource_type__id='+rtid).get('').then(function(response){
				var rt3s = response.objects;
				for(var i =rt3s.length-1; i>=0; i--){
					ttid = rt3s[i].task_type.split('/')[3];
					promises.push(Restangular.all('task_type/'+ttid+'/?extForUser='+uid+'&sortForUser='+uid).get(''));
				}

				return $q.all(promises);
			});

		});

	};

	var saveReservationParameters = function(params){
		reservationParameters = params;
	};

	var getReservationParameters = function(){
		return reservationParameters;
	};

	var areReservationParamsSaved = function(){
		return angular.isDefined(reservationParameters);
	};

	return {
		setReservation: setReservation,
		getReservation: getReservation,
		hasReservation: hasReservation,
		reset: reset,
		getTaskTypes: getTaskTypes,
		saveReservationParameters: saveReservationParameters,
		getReservationParameters: getReservationParameters,
		areReservationParamsSaved: areReservationParamsSaved
	};

}]);