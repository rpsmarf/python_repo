'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jan 19, 2015

    @author: rpsmarf
'''
import errno
import os
import logging
from smcommon.exceptions.sm_exc import SmException


SM_FS_EXC_NOT_CONN = "NOTCONN"
SM_FS_EXC_ALREADY_CONN = "ALREADYCONN"
SM_FS_EXC_CONN_FAIL = "CONNFAIL"

logger = logging.getLogger(__name__)


class SmFileAccessException(SmException):
    '''
    This exception is raised when there is an error on a file operation.
    '''

    def __init__(self, httpCodeToReturn, reasonCode, path, description):
        '''
        Constructor
        '''
        super(SmFileAccessException, self).__init__(httpCodeToReturn, reasonCode, path, description)        
 

def smFsRaiseFileExcFomEnvError(e, path=None):
    if isinstance(e, SmFileAccessException):
        raise e
    
    if hasattr(e, "errno") and e.errno is not None:
        eno = e.errno
        if path is None:
            path = e.filename
        logger.info("in smFsRaiseFileExcFomEnvError errno is %d", eno)
        raise SmFileAccessException(422, errno.errorcode[eno], path, os.strerror(eno))
        
    logger.info("in smFsRaiseFileExcFomEnvError exc = %s", str(e))
    raise SmFileAccessException(422, "UNKNOWN", "<unknown>", str(e))

    
def smFsRaiseFileExecNotConnected():
    logger.info("Connect required before operation")
    raise SmFileAccessException(422, "ENOTCONNECTED", "", "Connect required before operation")

    
def smFsRaiseFileExecAlreadyConnected():
    logger.info("Already connected")
    raise SmFileAccessException(422, "ECONNECTED", "", "Already connected")
    
        