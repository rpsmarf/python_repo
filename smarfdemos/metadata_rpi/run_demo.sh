#!/bin/bash

if [ "$SCS_URL" == "" ]
then 
    SCS_URL=localhost
fi

if [ "$SCS_RESOURCE" == "" ]
then 
    SCS_RESOURCE=1
fi

if [ "$SCS_SCHEMA" == "" ]
then 
    SCS_SCHEMA=1
fi

URL_BASE=$SCS_URL/scs
SHOW_CODE="-w \\nHTTP_CODE:%{http_code}\\n"

cat <<EOF > /tmp/md
{
  "propType": [
    "wind_speed",
    "temperature"
  ],
  "data": {
    "dir\/f1": [
      1,
      -1
    ],
    "dir\/f2": [
      2,
      -2
    ],
    "dir\/f3": [
      3,
      -3
    ]
   }
}
EOF


echo "This script runs through a number of steps to show off the "
echo "operation of the RP-SMARF Metadata Search RPI"
echo ""
echo "This demo is accessing $URL_BASE"
echo ""
echo "Hit ENTER to continue..."
read text
echo "Creating the new repo..."
REPO=`curl -s -i -H "Content-Type: application/json" -d "{\"resources\": [\"/scs/resource/1/\"], \"name\": \"Bridge Data Repo \`date\`\", \"schema\": \"/scs/md_schema/1/\"}" http://am.rpsmarf.ca/scs/md_repo/ | grep Location | sed -e "s/^.*repo//" | grep -o -e "[0-9]*"`
echo Created REPO $REPO
read text
echo "Uploading metadata for the 3 files f1, f2 and f3"
cat /tmp/md
read text
echo "Posting file to $URL_BASE/bulkupload/?resource=$SCS_RESOURCE&repo=$REPO"
URLPATH="$URL_BASE/bulkupload/?resource=$SCS_RESOURCE&repo=$REPO"
curl -F properties=@/tmp/md "$URLPATH"
read text
echo ""
echo "Listing number of paths via$URL_BASE/md_path/?repo=$REPO"
echo curl "$URL_BASE/md_path/?repo=$REPO&limit=0"
curl -s "$URL_BASE/md_path/?repo=$REPO&limit=0" |grep total_count
read text
echo ""
echo "Listing number of properties via $URL_BASE/md_property/?path__repo=$REPO "
curl -s "$URL_BASE/md_property/?path__repo=$REPO&limit=0"|grep total_count
read text
echo 'Searching for wind_speed > 1 - expect 2 records (f2 and f3)'
echo "via $URL_BASE/md_path/search/?repo=3&wind_speed__gt=1"
curl -s "$URL_BASE/md_path/search/?repo=3&wind_speed__gt=1" | grep relative
read text
echo 'Searching for wind_speed > 1 and NOT temp <= -3 - expect 1 records (f2)'
echo " via $URL_BASE/md_path/search/?repo=3&wind_speed__gt=1&-temperature__lte=-3"
curl -s "$URL_BASE/md_path/search/?repo=3&wind_speed__gt=1&-temperature__lte=-3" | grep relative
read text
echo "Deleting repo..."
curl $SHOW_CODE -X DELETE $URL_BASE/md_repo/$REPO/
read text
