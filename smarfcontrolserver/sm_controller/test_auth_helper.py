'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Mar 5, 2015

    @author: rpsmarf
'''
import logging
from django.contrib.auth.models import User
from guardian.shortcuts import assign_perm, remove_perm
from sm_controller.models import SmModelResourceType, SmModelContainer,\
    SmModelResource, SmModelTaskType, SmModelResourceTypeTaskType
import json
from sm_controller.test_helper import DUMMY_DESCRIPTION
from smarfcontrolserver.init.envvar import scsApiSetAuthOn

logger = logging.getLogger(__name__)


class TestAuthReadOnlyFieldChecker(object):
    '''
    This class tests that the fields specified are readonly in the api.py
    '''

    def __init__(self, 
                 testClass, objectName, 
                 objectCreationDictGenerator,
                 fieldDict):
        '''
        Constructor
        '''
        self.testClass = testClass
        self.objectName = objectName
        self.fieldDict = fieldDict
        self.objectCreationDictGenerator = objectCreationDictGenerator
        
    def runTest(self):
        helper = self.testClass.helper
        uOwner = User(username="owner2", first_name="ow", last_name="ner")
        uOwner.is_superuser = True
        uOwner.save()
        
        helper.setupRestAuth(uOwner.username)
        d1 = self.objectCreationDictGenerator(self.objectName)
        object1 = helper.objCreateViaRest("/scs/" + self.objectName + "/", 
                                          d1)
        # Send in the modify, expect no fields to be set
        helper.objModifyViaRest(object1, self.fieldDict)
        resp = helper.objGetViaRest(object1)
        # Check that no fields were set
        for key in self.fieldDict.keys():
            self.testClass.assertNotEqual(resp[key], self.fieldDict[key])
      

class TestVisibleToAllEditOnlyByOwner(object):
    '''
    classdocs
    '''

    def __init__(self, 
                 testClass, objectName, 
                 alterFieldName, alterFieldValue,
                 objectCreationDictGenerator):
        '''
        Constructor
        '''
        self.testClass = testClass
        self.objectName = objectName
        self.alterFieldName = alterFieldName
        self.alterFieldValue = alterFieldValue
        self.objectCreationDictGenerator = objectCreationDictGenerator
        self.postUserSetupCallback = None
        self.extraUserPerm = None
        
    def runTest(self):
        helper = self.testClass.helper
        objListPath = "/scs/" + self.objectName + "/"
        info = helper.objGetViaRest(objListPath)
        initialNumber = info["meta"]["total_count"]

        uOwner = User(username="owner", first_name="ow", last_name="ner")
        uOwner.save()
        
        if self.extraUserPerm is not None:
            # Add permission to user
            helper.setupRestAuth("root")
            permission_name = "sm_controller." + self.extraUserPerm
            helper.objGetViaRest("/scs/user/" + str(uOwner.id) + 
                                 "/setperm/?action=assign&perm=" +
                                 permission_name)
            
        uOther = User(username="other", first_name="oth", last_name="er")
        uOther.save()
                
        if self.postUserSetupCallback:
            self.postUserSetupCallback(uOwner, uOther)
            
        helper.setupRestAuth(uOwner.username)
        d1 = self.objectCreationDictGenerator(self.objectName)
        d1["owner"] = "/scs/user/99"  # Should be overriden to be the invoking user
        object1 = helper.objCreateViaRest("/scs/" + self.objectName + "/", 
                                          d1)
        info = helper.objGetViaRest(object1)
        self.testClass.assertEqual(info["owner"], "/scs/user/" + str(uOwner.id) + "/")
        object2 = helper.objCreateViaRest("/scs/" + self.objectName + "/", 
                                          self.objectCreationDictGenerator(self.objectName))
            
        # Test access with "other" user's token token
        helper.setupRestAuth(uOther)
        self.testClass.assertRaises(AssertionError, helper.objDeleteViaRest, object1)
        helper.objDeleteViaRest(objListPath)
        objList = helper.objGetViaRest(objListPath)
        self.testClass.assertEqual(initialNumber + 2, objList["meta"]["total_count"])
        helper.objGetViaRest(object1)
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, object1,
                                    {self.alterFieldName: self.alterFieldValue})
        helper.objGetViaRest(object1)
        
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, objListPath,
                                    {"objects": [{"resource_uri": object1, self.alterFieldName: self.alterFieldValue}]})
        helper.objGetViaRest(object1)
        
        # Test access with owner token
        helper.setupRestAuth(uOwner.username)
        helper.objGetViaRest(object1)
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertEqual(initialNumber + 2, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        helper.objModifyViaRest(object1, {self.alterFieldName: self.alterFieldValue})
        helper.objModifyViaRest(objListPath,
                                {"objects": [{"resource_uri": object1, self.alterFieldName: self.alterFieldValue}]})
        helper.objDeleteViaRest(object1)

        # Delete the other one by deleting "all"
        helper.objDeleteViaRest(objListPath)
        self.testClass.assertRaises(AssertionError, helper.objGetViaRest, object2)
              

class TestVisibleToAllCreateWithPermEditByOwner(object):
    '''
    '''
    def __init__(self, 
                 testClass, objectName, 
                 alterFieldName, alterFieldValue,
                 objectCreationDictGenerator,
                 permissionRequired):
        '''
        Constructor
        '''
        self.testClass = testClass
        self.objectName = objectName
        self.objectCreationDictGenerator = objectCreationDictGenerator
        self.permissionRequired = permissionRequired
        self.ownerTest = TestVisibleToAllEditOnlyByOwner(testClass,
                                                         objectName, 
                                                         alterFieldName,
                                                         alterFieldValue,
                                                         objectCreationDictGenerator)
        self.ownerTest.extraUserPerm = permissionRequired
        
    def runTest(self):
        # Run the owner tests
        self.ownerTest.runTest()

        uOther2 = User(username="other2", first_name="oth2", last_name="er2")
        uOther2.save()
        self.testClass.helper.setupRestAuth("other2")
        
        # Test that a user without permission fails
        # Attempt object create - expect failure because uOwner does 
        # not have permission yet
        self.testClass.assertRaises(AssertionError,
                                    self.testClass.helper.objCreateViaRest,
                                    "/scs/" + self.objectName + "/", 
                                    self.objectCreationDictGenerator(self.objectName))
        
        # Test that super user works
        self.testClass.helper.setupRestAuth("root")
        self.testClass.helper.objCreateViaRest("/scs/" + self.objectName + "/", 
                                               self.objectCreationDictGenerator(self.objectName))
      
        
class TestVisibleOnlyToSelf(object):
    '''
    classdocs
    '''

    def __init__(self, 
                 testClass, objectName, 
                 alterFieldName, alterFieldValue,
                 objectCreationDictGenerator):
        '''
        Constructor
        '''
        self.testClass = testClass
        self.objectName = objectName
        self.alterFieldName = alterFieldName
        self.alterFieldValue = alterFieldValue
        self.objectCreationDictGenerator = objectCreationDictGenerator
        
    def runTest(self):
        helper = self.testClass.helper
        objListPath = "/scs/" + self.objectName + "/"
        uOwner = User(username="owner", first_name="ow", last_name="ner")
        uOwner.save()
        
        uOther = User(username="other", first_name="oth", last_name="er")
        uOther.save()
        
        helper.setupRestAuth(uOwner.username)
        info = helper.objGetViaRest(objListPath)
        initialNumber = info["meta"]["total_count"]
        d1 = self.objectCreationDictGenerator(self.objectName)
        d1["owner"] = "/scs/user/99"  # Should be overriden to be the invoking user
        object1 = helper.objCreateViaRest("/scs/" + self.objectName + "/", 
                                          d1)
        info = helper.objGetViaRest(object1)
        self.testClass.assertEqual(info["owner"], "/scs/user/" + str(uOwner.id) + "/")
        object2 = helper.objCreateViaRest("/scs/" + self.objectName + "/", 
                                          self.objectCreationDictGenerator(self.objectName))
        
        # Test access with "other" user's token token
        helper.setupRestAuth(uOther)
        self.testClass.assertRaises(Exception, helper.objDeleteViaRest, object1)
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertEqual(initialNumber, info["meta"]["total_count"])
        helper.objDeleteViaRest(objListPath)  # Does nothing
        self.testClass.assertEqual(initialNumber, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        self.testClass.assertRaises(AssertionError, helper.objGetViaRest, object1)
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, object1,
                                    {self.alterFieldName: self.alterFieldValue})
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, objListPath,
                                    {"objects": [{"resource_uri": object1, self.alterFieldName: self.alterFieldValue}]})
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertEqual(0, helper.objGetViaRest(objListPath)["meta"]["total_count"])

        # Test access with owner token
        helper.setupRestAuth(uOwner.username)
        helper.objGetViaRest(object1)
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertEqual(initialNumber + 2, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        helper.objModifyViaRest(object1, {self.alterFieldName: self.alterFieldValue})
        helper.objModifyViaRest(objListPath,
                                {"objects": [{"resource_uri": object1, self.alterFieldName: self.alterFieldValue}]})
        helper.objDeleteViaRest(object1)
        if initialNumber == 0:
            helper.objDeleteViaRest(objListPath)
        else:
            self.testClass.assertRaises(AssertionError, helper.objDeleteViaRest, objListPath)
            helper.objDeleteViaRest(object2)
      
        
class TestVisibleToAllEditOnlyWithPerm(object):
    '''
    classdocs
    '''

    def __init__(self, 
                 testClass, objectName, 
                 alterFieldName, alterFieldValue,
                 objectCreationDictGenerator,
                 permissionRequired):
        '''
        Constructor
        '''
        self.testClass = testClass
        self.objectName = objectName
        self.alterFieldName = alterFieldName
        self.alterFieldValue = alterFieldValue
        self.objectCreationDictGenerator = objectCreationDictGenerator
        self.permissionRequired = permissionRequired
        
    def runTest(self):
        helper = self.testClass.helper
        objListPath = "/scs/" + self.objectName + "/"
        uOther = User(username="userOutOfGroup", first_name="user", last_name="OutOfGroup")
        uOther.save()
        
        info = helper.objGetViaRest(objListPath)
        initialNumber = info["meta"]["total_count"]
        
        uOwner = User(username="userInGroup", first_name="user", last_name="InGroup")
        uOwner.save()
        helper.setupRestAuth(uOwner.username)
        
        d1 = self.objectCreationDictGenerator(self.objectName)
        d1["owner"] = "/scs/user/99"  # Should be overriden to be the invoking user

        # Attempt object create - expect failure because uOwner does 
        # not have permission yet
        self.testClass.assertRaises(AssertionError,
                                    helper.objCreateViaRest,
                                    "/scs/" + self.objectName + "/", 
                                    d1)
        
        # Add permission to user
        helper.setupRestAuth("root")
        
        permission_name = "sm_controller." + self.permissionRequired
        helper.objGetViaRest("/scs/user/" + str(uOwner.id) + 
                             "/setperm/?action=assign&perm=" +
                             permission_name)
        resp = helper.objGetViaRest("/scs/user/" + str(uOwner.id) + 
                             "/getperm/")
        self.testClass.assertEqual(1, len(resp))
        self.testClass.assertEqual(permission_name, resp[0])        
        helper.setupRestAuth(uOwner.username)

        assign_perm("sm_controller." + self.permissionRequired, uOwner)
        
        # Try create again
        helper.setupRestAuth("userInGroup")
        object1 = helper.objCreateViaRest("/scs/" + self.objectName + "/", 
                                          d1)
        info = helper.objGetViaRest(object1)
        self.testClass.assertEqual(info["owner"], "/scs/user/" + str(uOwner.id) + "/")
        helper.objCreateViaRest("/scs/" + self.objectName + "/",  
                                self.objectCreationDictGenerator(self.objectName))
        
        # Test access with "other" user's token token
        helper.setupRestAuth(uOther)
        self.testClass.assertRaises(AssertionError, helper.objDeleteViaRest, object1)
        helper.objDeleteViaRest(objListPath)
        self.testClass.assertEqual(initialNumber + 2, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        helper.objGetViaRest(object1)
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, object1,
                                    {self.alterFieldName: self.alterFieldValue})
        helper.objGetViaRest(object1)
        
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, objListPath,
                                    {"objects": [{"resource_uri": object1, self.alterFieldName: self.alterFieldValue}]})
        helper.objGetViaRest(object1)
        
        # Test access with owner token
        helper.setupRestAuth(uOwner.username)
        helper.objGetViaRest(object1)
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertEqual(initialNumber + 2, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        helper.objModifyViaRest(object1, {self.alterFieldName: self.alterFieldValue})
        helper.objModifyViaRest(objListPath,
                                {"objects": [{"resource_uri": object1, self.alterFieldName: self.alterFieldValue}]})
        helper.objDeleteViaRest(object1)
        helper.objDeleteViaRest(objListPath) # Deletes the ones it can which is all (even the one created in setup
      
        self.testClass.assertEqual(0, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        
        
class TestAuthHelperResourceTest(object):
    '''
    Test for the authorization tests for a resource - not a reusable test
    like the previous classes
    '''

    def __init__(self, testClass):
        '''
        Constructor
        '''
        self.testClass = testClass
        self.objectName = "resource"
        self.fileCount = 0
        
    def doOperation(self, resource, uOwner, path, expResult):
        fullpath = resource + path
        helper = self.testClass.helper
        helper.setupRestAuth(uOwner.username)
        resp = self.testClass.client.get(fullpath, **helper.auth_header)
        self.testClass.assertEqual(resp.status_code, expResult)
        return resp
    
    def doUpload(self, resource, uOwner, path, expResult):
        fullpath = resource + path
        helper = self.testClass.helper
        helper.setupRestAuth(uOwner.username)
        fileName = "file" + str(self.fileCount)
        self.fileCount += 1
        helper.tempFolder.writeFile(fileName, "1234")
        with open(helper.tempFolder.mkAbsolutePath(fileName), "rb") as fp:
            resp = self.testClass.client.post(fullpath, 
                                    {'name': 'fred', 
                                     'uploadfile': fp}, **helper.auth_header)    
        self.testClass.assertEqual(resp.status_code, expResult)
    
    def doDelete(self, resource, uOwner, path, expResult):
        fullpath = resource + path
        helper = self.testClass.helper
        helper.setupRestAuth(uOwner.username)
        resp = self.testClass.client.delete(fullpath, json.dumps({}),
                                            "application/json", **helper.auth_header)
        self.testClass.assertEqual(resp.status_code, expResult)

    def doRunTask(self, task_type_path, resource_path, rttt_path, uOwner, expResult):
        helper = self.testClass.helper
        helper.setupRestAuth(uOwner.username)
        # Create task object
        task_path = helper.objCreateViaRest("/scs/task/",
                                            {"task_type": task_type_path,
                                             "community": "abc",
                                             "parametersJson": json.dumps({"input_ids": {"1": {"path": "june_2014/"}}})})
        
        # Create task resource objects
        helper.objCreateViaRest("/scs/task_resource/",
                                {"task": task_path,
                                 "resource": resource_path,
                                 "resource_type_task_type": rttt_path,
                                 "parametersJson": "{}"})
        
        # Start task
        resp = self.testClass.client.get(task_path + "?action=start", **helper.auth_header)
        self.testClass.assertEqual(resp.status_code, expResult, 
                                   resp.content.decode("utf-8"))
        
    def runTest(self):
        scsApiSetAuthOn(True)
        helper = self.testClass.helper
        objListPath = "/scs/" + self.objectName + "/"
        
        info = helper.objGetViaRest(objListPath)
        initialNumber = info["meta"]["total_count"]
        
        uOther = User(username="userOutOfGroup", first_name="user", last_name="OutOfGroup")
        uOther.save()
        uOtherPath = "/scs/user/" + str(uOther.id) + "/"

        uOwner = User(username="userInGroup", first_name="user", last_name="InGroup")
        uOwner.save()
        helper.setupRestAuth(uOwner.username)
        
        rt = SmModelResourceType.objects.first()
        resourceTypePath = "/scs/resource_type/" + str(rt.id) + "/"
        container = SmModelContainer.objects.first()
        containerPath = "/scs/container/" + str(container.id) + "/"
        task_type = SmModelTaskType()
        task_type.name = "xxx"
        task_type.name_key = "xxx"
        task_type.owner = uOwner
        task_type.description = DUMMY_DESCRIPTION
        task_type.code_module = "smcommon.task_runners.task_runner_delete"
        task_type.code_classname = "SmTaskRunnerDelete"
        task_type.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"type": "resource",
                                      "name": "File to delete", 
                                      "input_id": 1,
                                      "direction": "input", 
                                      "role": "storage"},
                                     ],
                            "computeResourceRole": "storage"
                       }
                   )
        task_type.save()
        task_type_path = "/scs/task_type/" + str(task_type.id) + "/"
        rtype = SmModelResourceType()
        rtype.name = "Resource Type Y"
        rtype.name_key = "default_resource_y"
        rtype.nature = "data"
        rtype.description = DUMMY_DESCRIPTION
        rtype.save()
        rttt = SmModelResourceTypeTaskType()
        rttt.resource_type = rtype
        rttt.task_type = task_type
        rttt.role = "storage"
        rttt.name = "storage2"
        rttt.description = DUMMY_DESCRIPTION
        rttt.save()    
        rttt_path = "/scs/resource_type_task_type/" + str(rttt.id) + "/"
        
        d1 = {
              "owner": "/scs/user/99",  # Should be overriden to be the invoking user
              "name": "name 1",
              "name_key": "name1",
              "resource_type": resourceTypePath,
              "description": "My description is long enough",
              "container": containerPath,
              "nature": "data",
              "parametersJson": '{ "folder": "r1/" }',
              "status": "up",
              "personalFolder": False,
              "capacityJson": "{}"
        }
            
        # Attempt object create - expect failure because uOwner does 
        # not have permission yet
        self.testClass.assertRaises(AssertionError,
                                    helper.objCreateViaRest,
                                    "/scs/" + self.objectName + "/", 
                                    d1)
        
        # Add permission to user
        assign_perm("sm_controller.add_smmodelresource", uOwner)
        
        # Try create again, expect it to work now
        object1 = helper.objCreateViaRest("/scs/" + self.objectName + "/", d1)
        
        # Check that the user was overriden
        info = helper.objGetViaRest(object1)
        self.testClass.assertEqual(info["owner"], "/scs/user/" + str(uOwner.id) + "/")
        
        # Create second object
        d1["name"] = "name2"
        d1["name_key"] = "namekey2"
        d1["parametersJson"] = '{ "folder": "r2/" }'
        helper.objCreateViaRest("/scs/" + self.objectName + "/", d1)

        resource = SmModelResource.objects.get(id=object1.rsplit("/", 2)[1])
        # Try each operation with the owner - should all work
        self.doOperation(object1, uOwner, "mkrootdir/", 422)
        self.doOperation(object1, uOwner, "x/mkdir/", 200)
        self.doOperation(object1, uOwner, "update/", 200)
        self.doUpload(object1, uOwner, "qq/upload/", 200)
        self.doOperation(object1, uOwner, "qq/download/", 200)
        self.doDelete(object1, uOwner, "qq/file/", 204)
        self.doOperation(object1, uOwner, "x/zip/?zipFilePath=x.zip", 200)
        self.doOperation(object1, uOwner, "y/mkdir/", 200)
        self.doOperation(object1, uOwner, "y/unzip/?zipFilePath=x.zip", 200)
        self.doOperation(object1, uOwner, "x/list/", 200)
        self.doOperation(object1, uOwner, "x/metadata/", 200)
        self.doOperation(object1, uOwner, "x/rename/?newName=y", 200)
        self.doOperation(object1, uOwner, "y/move/?newPath=z", 200)
        self.doOperation(object1, uOwner, "z/deleterecursive/", 200)
        self.doRunTask(task_type_path, object1, rttt_path, uOwner, 200)
                
        # Try each operation without permission
        self.doOperation(object1, uOther, "mkrootdir/", 401)
        self.doOperation(object1, uOther, "x/mkdir/", 401)
        self.doOperation(object1, uOther, "update/", 200)
        self.doUpload(object1, uOther, "qq/upload/", 401)
        self.doOperation(object1, uOther, "qq/download/", 401)
        self.doDelete(object1, uOther, "qq/file/", 401)
        self.doOperation(object1, uOther, "x/zip/?zipFilePath=x2.zip", 401)
        self.doOperation(object1, uOther, "y/mkdir/", 401)
        self.doOperation(object1, uOther, "y/unzip/?zipFilePath=x2.zip", 401)
        self.doOperation(object1, uOther, "x/list/", 401)
        self.doOperation(object1, uOther, "x/metadata/", 401)
        self.doOperation(object1, uOther, "x/rename/?newName=y", 401)
        self.doOperation(object1, uOther, "y/move/?newPath=z", 401)
        self.doOperation(object1, uOther, "z/deleterecursive/", 401)
        self.doRunTask(task_type_path, object1, rttt_path, uOther, 401)
        
        # Try each operation with read permission
        assign_perm("read_resource_data", uOther, resource)
        self.doOperation(object1, uOther, "mkrootdir/", 401)
        self.doOperation(object1, uOther, "x/mkdir/", 401)
        self.doOperation(object1, uOther, "update/", 200)
        self.doUpload(object1, uOther, "qq/upload/", 401)
        self.doOperation(object1, uOther, "qq/download/", 422)  # File not uploaded, so expect failure
        self.doDelete(object1, uOther, "qq/file/", 401)
        self.doOperation(object1, uOther, "x/zip/?zipFilePath=x3.zip", 200)
        self.doOperation(object1, uOther, "y/mkdir/", 401)
        self.doOperation(object1, uOther, "y/unzip/?zipFilePath=x3.zip", 401)
        self.doOperation(object1, uOther, "x/list/", 422) # Mkdir failed, so expect failure
        self.doOperation(object1, uOther, "x/metadata/", 422) # Mkdir failed, so expect failure
        self.doOperation(object1, uOther, "x/rename/?newName=y", 401)
        self.doOperation(object1, uOther, "y/move/?newPath=z", 401)
        self.doOperation(object1, uOther, "z/deleterecursive/", 401)
        self.doRunTask(task_type_path, object1, rttt_path, uOther, 401)
        
        # Try each operation with write permission
        remove_perm("read_resource_data", uOther, resource)
        assign_perm("write_resource_data", uOther, resource)
        self.doOperation(object1, uOther, "mkrootdir/", 422)
        self.doOperation(object1, uOther, "x/mkdir/", 200)
        self.doOperation(object1, uOther, "update/", 200)
        self.doUpload(object1, uOther, "qq/upload/", 200)
        self.doOperation(object1, uOther, "qq/download/", 401)
        self.doDelete(object1, uOther, "qq/file/", 204)
        self.doOperation(object1, uOther, "x/zip/?zipFilePath=x4.zip", 401)
        self.doOperation(object1, uOther, "y/mkdir/", 200)
        self.doOperation(object1, uOther, "y/unzip/?zipFilePath=x4.zip", 200)
        self.doOperation(object1, uOther, "x/list/", 401)
        self.doOperation(object1, uOther, "x/metadata/", 401)
        self.doOperation(object1, uOther, "x/rename/?newName=y", 200)
        self.doOperation(object1, uOther, "y/move/?newPath=z", 200)
        self.doOperation(object1, uOther, "z/deleterecursive/", 200)
        self.doRunTask(task_type_path, object1, rttt_path, uOther, 401)
        
        # Try each operation with read and write permission
        # Setting my own permission on someone else's resource is not OK
        self.doOperation(object1, uOther, "setperm/?action=assign&perm=r&user=" + uOtherPath, 401)
        # Resource owner setting permission is OK
        self.doOperation(object1, uOwner, "setperm/?action=assign&perm=r&user=" + uOtherPath, 200)
        resp = self.doOperation(object1, uOwner, "getperm/", 200)
        respJson = json.loads(resp.content.decode("utf-8"))
        self.testClass.assertEqual(1, len(respJson))
        self.testClass.assertEqual(2, len(respJson[uOtherPath]))
        self.testClass.assertTrue('write_resource_data' in respJson[uOtherPath])
        self.testClass.assertTrue('read_resource_data' in respJson[uOtherPath])

        self.doOperation(object1, uOther, "mkrootdir/", 422)
        self.doOperation(object1, uOther, "x/mkdir/", 200)
        self.doOperation(object1, uOther, "update/", 200)
        self.doUpload(object1, uOther, "qq/upload/", 200)
        self.doOperation(object1, uOther, "qq/download/", 200)
        self.doDelete(object1, uOther, "qq/file/", 204)
        self.doOperation(object1, uOther, "x/zip/?zipFilePath=x5.zip", 200)
        self.doOperation(object1, uOther, "y/mkdir/", 200)
        self.doOperation(object1, uOther, "y/unzip/?zipFilePath=x5.zip", 200)
        self.doOperation(object1, uOther, "x/list/", 200)
        self.doOperation(object1, uOther, "x/rename/?newName=y", 200)
        self.doOperation(object1, uOther, "y/move/?newPath=z", 200)
        self.doOperation(object1, uOther, "z/deleterecursive/", 200)
        self.doRunTask(task_type_path, object1, rttt_path, uOther, 401)
        
        # Add run permission
        self.doOperation(object1, uOwner, "setperm/?action=assign&perm=x&user=" + uOtherPath, 200)

        # Lock the resource
        resource.locked = True
        resource.save()
        # Try each operation with the owner - should all fail
        self.doOperation(object1, uOwner, "mkrootdir/", 410)
        self.doOperation(object1, uOwner, "x/mkdir/", 410)
        self.doOperation(object1, uOwner, "update/", 200)
        self.doUpload(object1, uOwner, "qq/upload/", 410)
        self.doOperation(object1, uOwner, "qq/download/", 410)
        self.doDelete(object1, uOwner, "qq/file/", 410)
        self.doOperation(object1, uOwner, "x/zip/?zipFilePath=x.zip", 410)
        self.doOperation(object1, uOwner, "y/mkdir/", 410)
        self.doOperation(object1, uOwner, "y/unzip/?zipFilePath=x.zip", 410)
        self.doOperation(object1, uOwner, "x/list/", 410)
        self.doOperation(object1, uOwner, "x/metadata/", 410)
        self.doOperation(object1, uOwner, "x/rename/?newName=y", 410)
        self.doOperation(object1, uOwner, "y/move/?newPath=z", 410)
        self.doOperation(object1, uOwner, "z/deleterecursive/", 410)
        self.doRunTask(task_type_path, object1, rttt_path, uOwner, 410)
                
        # Try each operation with other (other has permission, but the resource is locked so will get 401)
        self.doOperation(object1, uOther, "mkrootdir/", 410)
        self.doOperation(object1, uOther, "x/mkdir/", 410)
        self.doOperation(object1, uOther, "update/", 200)
        self.doUpload(object1, uOther, "qq/upload/", 410)
        self.doOperation(object1, uOther, "qq/download/", 410)
        self.doDelete(object1, uOther, "qq/file/", 410)
        self.doOperation(object1, uOther, "x/zip/?zipFilePath=x2.zip", 410)
        self.doOperation(object1, uOther, "y/mkdir/", 410)
        self.doOperation(object1, uOther, "y/unzip/?zipFilePath=x2.zip", 410)
        self.doOperation(object1, uOther, "x/list/", 410)
        self.doOperation(object1, uOther, "x/metadata/", 410)
        self.doOperation(object1, uOther, "x/rename/?newName=y", 410)
        self.doOperation(object1, uOther, "y/move/?newPath=z", 410)
        self.doOperation(object1, uOther, "z/deleterecursive/", 410)
        self.doRunTask(task_type_path, object1, rttt_path, uOther, 410)
        # Unlock the resource
        resource.locked = False
        resource.save()

        # Lock the task type and test that owner is OK, other is rejected
        task_type.lock = True
        task_type.save()
        self.doRunTask(task_type_path, object1, rttt_path, uOwner, 200)
        self.doRunTask(task_type_path, object1, rttt_path, uOther, 401)
        task_type.lock = False
        task_type.save()

        # Try each operation with just run permission
        self.doOperation(object1, uOwner, "setperm/?action=remove&perm=rw&user=" + uOtherPath, 200)
        # Invalid action
        self.doOperation(object1, uOwner, "setperm/?action=add&user=" + uOtherPath, 400)

        # Test getting permissions
        resp = self.doOperation(object1, uOwner, "getperm/", 200)
        respJson = json.loads(resp.content.decode("utf-8"))
        self.testClass.assertEqual(1, len(respJson))
        self.testClass.assertEqual(respJson[uOtherPath], ["execute_on_resource"])
        resp = self.doOperation(object1, uOther, "getperm/", 200)
        self.testClass.assertEqual(1, len(respJson))
        self.testClass.assertEqual(respJson[uOtherPath], ["execute_on_resource"])
        
        self.doOperation(object1, uOther, "mkrootdir/", 401)
        self.doOperation(object1, uOther, "x/mkdir/", 401)
        self.doOperation(object1, uOther, "update/", 200)
        self.doUpload(object1, uOther, "qq/upload/", 401)
        self.doOperation(object1, uOther, "qq/download/", 401)
        self.doDelete(object1, uOther, "qq/file/", 401)
        self.doOperation(object1, uOther, "x/zip/?zipFilePath=x6.zip", 401)
        self.doOperation(object1, uOther, "y/mkdir/", 401)
        self.doOperation(object1, uOther, "y/unzip/?zipFilePath=x6.zip", 401)
        self.doOperation(object1, uOther, "x/list/", 401)
        self.doOperation(object1, uOther, "x/rename/?newName=y", 401)
        self.doOperation(object1, uOther, "y/move/?newPath=z", 401)
        self.doOperation(object1, uOther, "z/deleterecursive/", 401)
        # Run of task type fails because uOther does not have run permission on this task_type
        self.doRunTask(task_type_path, object1, rttt_path, uOther, 401)

        # Add permission to access the task_type
        self.doOperation(task_type_path, uOwner, "setperm/?action=assign&perm=x&user=" + uOtherPath, 200)
        self.doOperation(object1, uOwner, "setperm/?action=assign&perm=r&user=" + uOtherPath, 200)
        # Test getting task type permissions
        resp = self.doOperation(task_type_path, uOwner, "getperm/", 200)
        respJson = json.loads(resp.content.decode("utf-8"))
        self.testClass.assertEqual(1, len(respJson))
        self.testClass.assertEqual(respJson[uOtherPath], ["execute_task_type"])

        # Test getting resource permissions
        resp = self.doOperation(object1, uOwner, "getperm/", 200)
        respJson = json.loads(resp.content.decode("utf-8"))
        self.testClass.assertEqual(1, len(respJson))
        self.testClass.assertTrue('read_resource_data' in respJson[uOtherPath])

        self.doRunTask(task_type_path, object1, rttt_path, uOther, 200)

        # Test access with "other" user's token token
        helper.setupRestAuth(uOther)
        self.testClass.assertRaises(AssertionError, helper.objDeleteViaRest, object1)
        helper.objDeleteViaRest(objListPath)
        self.testClass.assertEqual(initialNumber + 4, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        helper.objGetViaRest(object1)
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, object1,
                                    {"description": "This is another long description"})
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, objListPath,
                                    {"objects": [{"resource_uri": object1, 
                                                  "description": "This is another long description"}]})
        helper.objGetViaRest(object1) 
        
        # Test access with owner token
        helper.setupRestAuth(uOwner.username)
        helper.objGetViaRest(object1)
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertEqual(initialNumber + 4, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        helper.objModifyViaRest(object1, {"description": "This is another long description"})
        helper.objModifyViaRest(objListPath,
                                {"objects": [{"resource_uri": object1, "description": "This is another long description"}]})
        helper.objDeleteViaRest(object1)
        self.testClass.assertEqual(initialNumber + 3, helper.objGetViaRest(objListPath)["meta"]["total_count"])        
        helper.objDeleteViaRest(objListPath) # Deletes the ones it can which is just one more
        self.testClass.assertEqual(initialNumber, helper.objGetViaRest(objListPath)["meta"]["total_count"])

         
class TestVisibleToAllEditOnlySuperuser(object):
    '''
    classdocs
    '''

    def __init__(self, 
                 testClass, objectName, 
                 alterFieldName, alterFieldValue,
                 objectCreationDictGenerator):
        '''
        Constructor
        '''
        self.testClass = testClass
        self.objectName = objectName
        self.alterFieldName = alterFieldName
        self.alterFieldValue = alterFieldValue
        self.objectCreationDictGenerator = objectCreationDictGenerator
        
    def runTest(self):
        helper = self.testClass.helper
        objListPath = "/scs/" + self.objectName + "/"
        uOther = User(username="userOutOfGroup", first_name="user", last_name="OutOfGroup")
        uOther.save()
            
        info = helper.objGetViaRest(objListPath)
        initialNumber = info["meta"]["total_count"]
        
        uOwner = User(username="userInGroup", first_name="user", last_name="InGroup")
        uOwner.save()
        helper.setupRestAuth(uOwner.username)
        
        d1 = self.objectCreationDictGenerator(self.objectName)
        d1["owner"] = "/scs/user/99/"  # Should be overriden to be the invoking user

        # Attempt object create - expect failure because uOwner does 
        # not have permission yet
        self.testClass.assertRaises(AssertionError,
                                    helper.objCreateViaRest,
                                    "/scs/" + self.objectName + "/", 
                                    d1)
        
        # Add permission to user
        uOwner.is_superuser = True
        uOwner.save()
        
        # Try create again
        object1 = helper.objCreateViaRest("/scs/" + self.objectName + "/", 
                                          d1)
        object2 = helper.objCreateViaRest("/scs/" + self.objectName + "/",  
                                          self.objectCreationDictGenerator(self.objectName))
        
        # Test access with "other" user's token token
        helper.setupRestAuth(uOther)
        self.testClass.assertRaises(AssertionError, helper.objDeleteViaRest, object1)
        helper.objDeleteViaRest(objListPath)
        self.testClass.assertEqual(initialNumber + 2, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        helper.objGetViaRest(object1)
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, object1,
                                    {self.alterFieldName: self.alterFieldValue})
        helper.objGetViaRest(object1)
        
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, objListPath,
                                    {"objects": [{"resource_uri": object1, self.alterFieldName: self.alterFieldValue}]})
        helper.objGetViaRest(object1)
        
        # Test access with owner token
        helper.setupRestAuth(uOwner.username)
        helper.objGetViaRest(object1)
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertEqual(initialNumber + 2, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        helper.objModifyViaRest(object1, {self.alterFieldName: self.alterFieldValue})
        helper.objModifyViaRest(objListPath,
                                {"objects": [{"resource_uri": object1, self.alterFieldName: self.alterFieldValue}]})
        helper.objDeleteViaRest(object1)
        helper.objDeleteViaRest(object2)
      
        objList = helper.objGetViaRest(objListPath)
        self.testClass.assertEqual(initialNumber, objList["meta"]["total_count"])
        
        
class TestVisibleToAllCreateDeleteByResOrCommOwner(object):
    '''
    classdocs
    '''

    def __init__(self, 
                 testClass, objectName, 
                 objectCreationDictGenerator):
        '''
        Constructor
        '''
        self.testClass = testClass
        self.objectName = objectName
        self.objectCreationDictGenerator = objectCreationDictGenerator
        
    def runTest(self):
        helper = self.testClass.helper
        objListPath = "/scs/" + self.objectName + "/"
        info = helper.objGetViaRest(objListPath)
        initialNumber = info["meta"]["total_count"]
       
        # Create usera who owns a resource
        usera = User(username="usera", first_name="oth2", last_name="er2")
        usera.save()
        helper.setupRestAuth("root")
        permission_name = "sm_controller.add_smmodelresource"
        helper.objGetViaRest("/scs/user/" + str(usera.id) + 
                             "/setperm/?action=assign&perm=" +
                             permission_name)
        permission_name = "sm_controller.add_smmodeltasktype"
        helper.objGetViaRest("/scs/user/" + str(usera.id) + 
                             "/setperm/?action=assign&perm=" +
                             permission_name)
        helper.setupRestAuth("usera")
        self.resource_path = helper.objCreateViaRest("/scs/resource/",
                                                     {"container": "/scs/container/1/",
                                                      "resource_type": "/scs/resource_type/1/",
                                                      "name": "abcz",
                                                      "description": "sldkfjlsdkjflksdjflksdjfl",
                                                      "parametersJson": '{"folder": "a"}'})
        
        self.task_type_path = helper.objCreateViaRest("/scs/task_type/",
                                                     {"code_module": "a",
                                                      "code_classname": "b",
                                                      "name": "abcz",
                                                      "description": "sldkfjlsdkjflksdjflksdjfl",
                                                      "configurationJson": json.dumps({"args": []})})
        
        # Create userb who owns a community
        userb = User(username="userb", first_name="oth2", last_name="er2")
        userb.save()
        
        # Create a community owner by userb
        helper.setupRestAuth("root")
        permission_name = "sm_controller.add_smmodelcommunity"
        helper.objGetViaRest("/scs/user/" + str(userb.id) + 
                             "/setperm/?action=assign&perm=" +
                             permission_name)
        helper.setupRestAuth("userb")
        self.community_path = helper.objCreateViaRest("/scs/community/",
                                                      self.testClass.communityAuthDictGen("aaa"))        
        comm_obj = helper.objGetViaRest(self.community_path)
        self.tag_path = comm_obj["tag"]
        
        # Create userc who owns nothing
        userc = User(username="userc", first_name="oth2", last_name="er2")
        userc.save()
        
        # Attempt to create link as userc - expect failure
        helper.setupRestAuth("userc")
        self.testClass.assertRaises(AssertionError,
                                    helper.objCreateViaRest,
                                    "/scs/" + self.objectName + "/", 
                                    self.objectCreationDictGenerator(self.objectName))

        # Attempt to create and delete link as usera - expect OK
        # Check updated - expect failure as usera, userb or userb
        helper.setupRestAuth("usera")
        obj_path = helper.objCreateViaRest("/scs/" + self.objectName + "/", 
                                          self.objectCreationDictGenerator(self.objectName))
       
        # Check that userc does nothing
        helper.setupRestAuth("userc")
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, objListPath,
                                    {"objects": [{"resource_uri": obj_path, "tag": "/scs/tag/1/"}]})
        self.testClass.assertRaises(AssertionError, helper.objDeleteViaRest, obj_path)
        helper.objDeleteViaRest(objListPath)
        self.testClass.assertEqual(initialNumber + 1, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        
        # Bulk delete by user a is OK 
        helper.setupRestAuth("usera")
        helper.objDeleteViaRest(objListPath)
        self.testClass.assertEqual(initialNumber, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        obj_path = helper.objCreateViaRest("/scs/" + self.objectName + "/", 
                                          self.objectCreationDictGenerator(self.objectName))
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, objListPath,
                                    {"objects": [{"resource_uri": obj_path, "tag": "/scs/tag/1/"}]})
        helper.objDeleteViaRest(obj_path)
        
        # Attempt to create and delete link as userb - expect OK
        helper.setupRestAuth("userb")
        helper.objCreateViaRest("/scs/" + self.objectName + "/", 
                                self.objectCreationDictGenerator(self.objectName))
        helper.objDeleteViaRest(objListPath)
        self.testClass.assertEqual(initialNumber, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        obj_path = helper.objCreateViaRest("/scs/" + self.objectName + "/", 
                                          self.objectCreationDictGenerator(self.objectName))
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, objListPath,
                                    {"objects": [{"resource_uri": obj_path, "tag": "/scs/tag/1/"}]})
        helper.objDeleteViaRest(obj_path)
      
        objList = helper.objGetViaRest(objListPath)
        self.testClass.assertEqual(initialNumber, objList["meta"]["total_count"])
        
        
class TestAuthHelperUserObject(object):
    '''
    classdocs
    '''

    def __init__(self, 
                 testClass, objectName, 
                 alterFieldName, alterFieldValue):
        '''
        Constructor
        '''
        self.testClass = testClass
        self.objectName = objectName
        self.alterFieldName = alterFieldName
        self.alterFieldValue = alterFieldValue        

    def deleteHomeFolder(self, user):
        username = User.objects.get(id=user.rsplit("/", 2)[1])
        tempFolder = self.testClass.helper.tempFolder
        tempFolder.rmdir("private_" + str(username) + "/.rpsmarf/tmp")
        tempFolder.rmdir("private_" + str(username) + "/.rpsmarf")
        tempFolder.rmdir("private_" + str(username))
        tempFolder.rmdir("public_" + str(username) + "/.rpsmarf/tmp")
        tempFolder.rmdir("public_" + str(username) + "/.rpsmarf")
        tempFolder.rmdir("public_" + str(username))
    
    def runTest(self):
        helper = self.testClass.helper
        objListPath = "/scs/" + self.objectName + "/"
        info = helper.objGetViaRest(objListPath)
        initialNumber = info["meta"]["total_count"]
        
        uOther = User(username="userOutOfGroup", first_name="user", last_name="OutOfGroup")
        uOther.save()
            
        uOwner = User(username="userInGroup", first_name="user", last_name="InGroup")
        uOwner.save()
        helper.setupRestAuth(uOwner.username)
        
        # Attempt object create - expect failure because uOwner does 
        # not have permission yet
        self.testClass.assertRaises(AssertionError,
                                    helper.objCreateViaRest,
                                    "/scs/" + self.objectName + "/", 
                                    {"username": "xxx"})
        
        # Add permission to user
        uOwner.is_superuser = True
        uOwner.save()
        
        # Try create again
        object1 = "/scs/user/" + str(uOwner.id) + "/"
        object2 = "/scs/user/" + str(uOther.id) + "/"
        
        # Test access with "other" user's token token
        helper.setupRestAuth(uOther)
        self.testClass.assertRaises(AssertionError, helper.objDeleteViaRest, object1)
        helper.objDeleteViaRest(objListPath)
        self.testClass.assertEqual(initialNumber + 2, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        helper.objGetViaRest(object1)
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, object1,
                                    {self.alterFieldName: self.alterFieldValue})
        helper.objGetViaRest(object1)
        
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, objListPath,
                                    {"objects": [{"resource_uri": object1, self.alterFieldName: self.alterFieldValue}]})
        helper.objGetViaRest(object1)
        
        # Test access with owner token
        helper.setupRestAuth(uOwner.username)
        helper.objGetViaRest(object1)
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertEqual(initialNumber + 2, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        helper.objModifyViaRest(object1, {self.alterFieldName: self.alterFieldValue})
        helper.objModifyViaRest(objListPath,
                                {"objects": [{"resource_uri": object1, self.alterFieldName: self.alterFieldValue}]})
        
        self.deleteHomeFolder(object2)
        helper.objDeleteViaRest(object2)
        self.deleteHomeFolder(object1)
        helper.objDeleteViaRest(object1)
      
        
class TestAuthHelperUserSettings(object):
    '''
    Test for user settings authorization.  Much like other "visible to self"
    objects, but cannot be created or destroyed (these are done by signal handlers)
    '''

    def __init__(self, 
                 testClass):
        '''
        Constructor
        '''
        self.testClass = testClass
        
    def runTest(self):
        self.objectName = 'user_setting'
        self.alterFieldName = "performSetupOnNextLogin"
        self.alterFieldValue = False
        helper = self.testClass.helper
        objListPath = "/scs/" + self.objectName + "/"

        uOwner = User(username="owner", first_name="ow", last_name="ner")
        uOwner.save()
        
        uOther = User(username="other", first_name="oth", last_name="er")
        uOther.save()
                
        helper.setupRestAuth(uOwner.username)
        object1 = "/scs/user_setting/" + str(uOwner.id) + "/"
        
        # Check that a user can see his own user_settings
        info = helper.objGetViaRest(object1)
        self.testClass.assertEqual(info["owner"], "/scs/user/" + str(uOwner.id) + "/")
            
        # Test access with "other" user's token token
        helper.setupRestAuth(uOther)
        self.testClass.assertRaises(AssertionError, helper.objDeleteViaRest, object1)
        # Check that delete all does nothing
        helper.objDeleteViaRest(objListPath)
        objList = helper.objGetViaRest(objListPath)
        self.testClass.assertEqual(3, objList["meta"]["total_count"])
        helper.objGetViaRest(object1)
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, object1,
                                    {self.alterFieldName: self.alterFieldValue})
        self.testClass.assertRaises(AssertionError, helper.objModifyViaRest, objListPath,
                                    {"objects": [{"resource_uri": object1, self.alterFieldName: self.alterFieldValue}]})
        
        # Test access with owner token
        helper.setupRestAuth(uOwner.username)
        helper.objGetViaRest(object1)
        info = helper.objGetViaRest(objListPath)
        self.testClass.assertEqual(3, helper.objGetViaRest(objListPath)["meta"]["total_count"])
        helper.objModifyViaRest(object1, {self.alterFieldName: self.alterFieldValue})
        helper.objModifyViaRest(objListPath,
                                {"objects": [{"resource_uri": object1, self.alterFieldName: self.alterFieldValue}]})
        self.testClass.assertRaises(AssertionError, helper.objDeleteViaRest, object1)
        helper.objDeleteViaRest(objListPath)
      

        