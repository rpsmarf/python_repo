#!/bin/bash

echo "Updating version list from repo..."
sudo apt-get update -o Dir::Etc::sourcelist="sources.list.d/sw.rpsmarf.list"     -o Dir::Etc::sourceparts="-" -o APT::Get::List-Cleanup="0"
echo "Installing tools..."
sudo apt-get -y --force-yes install smarf-demos