#!/usr/bin/env python 
# encoding: utf-8
'''
scripts.scs_obj_create -- script to create an object on the RPSMARF SCS server

scripts.scs_obj_get is a description

It defines classes_and_methods

@author:     user_name

@copyright:  2014 organization_name. All rights reserved.

@license:    license

@contact:    user_email
@deffield    updated: Updated
'''

import sys
import os
from urllib.parse import urlparse
from scripts.scs_obj_common import scsScriptGetDefaultUrl, scsScriptGetVerify,\
    scsScriptGetHeaders
sys.path.insert(1, "/opt/smarf-scs/scs")

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
import requests
import json

__all__ = []
__version__ = 0.1
__date__ = '2014-09-25'
__updated__ = '2014-09-25'


class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''

    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg

    def __str__(self):
        return self.msg

    def __unicode__(self):
        return self.msg


def makeTypedValue(value):
    if value.startswith("s."):
        return value[2:]
    if value.startswith("i."):
        return int(value[2:])
    if value.startswith("f."):
        return float(value[2:])
    if value.startswith("b."):
        return bool(value[2:])
    
    # No prefix specified, go with defaults - if int return int, otherwise return string    
    try:
        return int(value)
    except ValueError:
        return value    


def scsModifyObject(obj_path, fieldDict, **kwargs):
    
    if ("url" in kwargs):
        url = kwargs["url"]
    else:
        url = scsScriptGetDefaultUrl()

    url = url + obj_path
    put_response = requests.put(url=url, 
                                data=json.dumps(fieldDict), 
                                headers=scsScriptGetHeaders(), 
                                verify=scsScriptGetVerify())
    if put_response.status_code != 204:
        print("Response code " + str(put_response.status_code) + " with message: " + put_response.text)
    put_response.raise_for_status()


def scsCreateObjectPath(obj_type, fieldDict, **kwargs):
    
    if ("url" in kwargs):
        url = kwargs["url"]
    else:
        url = scsScriptGetDefaultUrl()

    url = url + "/scs/" + obj_type + "/"
    put_response = requests.post(url=url, 
                                 data=json.dumps(fieldDict), 
                                 headers=scsScriptGetHeaders(), 
                                 verify=scsScriptGetVerify())
    if put_response.status_code != 201:
        print("Response code " + str(put_response.status_code) + " with message: " + put_response.text)
    put_response.raise_for_status()
    created_url = urlparse(put_response.headers["Location"])
    return created_url.path


def scsCreateObjectId(obj_type, fieldDict, **kwargs):
    path = scsCreateObjectPath(obj_type, fieldDict, **kwargs)
    return path[:-1].rpartition("/")[2]


def main(argv=None): 
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = "Script to create an RPSMARF object via the REST API on the SCS server"
    program_license = '''%s

  Created by Andrew McGregor on %s.
  Copyright 2014 Carleton. All rights reserved.
  
USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-u", "--url", dest="url", default=scsScriptGetDefaultUrl, 
                            help="URL to access REST API [default: %(default)s]")
        parser.add_argument("-t", "--type", dest="obj_type", default="",
                            help="The type of the object to get")
        parser.add_argument("-j", "--json", dest="jsonObjectData",
                            help="Data to post as a prebuild JSON string")
        parser.add_argument("-f", "--fields", dest="fields", nargs='+',
                            help="Set of field/value pairs of the form a=b c=d ...")
        parser.add_argument("-v", "--verbose", action='store_true', dest="verbose",
                            help="Enable verbose mode")
        parser.epilog = ("\nFor example:\m" + 
                     "scs_obj_create.py -t task -f name=hello task_type=7")
        # Process arguments
        args = parser.parse_args()
        
        if args.jsonObjectData is None:
            if args.fields is None:
                raise CLIError("One of --json and --fields must be specified")
            d = {}
            for argPair in args.fields:
                (name, sep, value) = argPair.partition("=")
                if sep != "=":
                    print("ERROR: fields must be of the form name=value, found '" + argPair + "'")
                typed_value = makeTypedValue(value)
                d[name] = typed_value
            jsonObjectData = json.dumps(d)
        else:
            jsonObjectData = args.jsonObjectData
                
        if args.verbose:
            print("Verbose mode on")

        if args.obj_type == "":
            parser.print_help()
            raise CLIError("Type must be specified.")

        url = args.url + "/scs/" + args.obj_type + "/"
        if args.verbose:
            print("URL=" + url)
            print("JSON=" + jsonObjectData)
        put_response = requests.post(url=url, data=jsonObjectData,
                                     headers=scsScriptGetHeaders(), 
                                     verify=scsScriptGetVerify())
        if args.verbose:
            print("RESPONSE=" + put_response.text)
        put_response.raise_for_status()
        
        print(put_response.headers["Location"][:-1].rpartition("/")[2])
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    sys.exit(main())
