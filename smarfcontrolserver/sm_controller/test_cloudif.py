'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 10, 2015

    @author: rpsmarf
'''

import unittest
from django.test.testcases import TransactionTestCase
from sm_controller.test_helper import ScsTestHelper, scsAwsTestKeysDefined,\
    scsTerminataAllAwsCloudServers
from sm_controller.signal_handlers import smTriggerSignalInstall
import logging
from smcommon.globals.sm_copy_controller import SmCopyController
from smcommon.globals.sm_global_context import smGlobalContextInit
from smarfcontrolserver.pollers.cloud_state_poller import ScsCloudStatePoller
from smarfcontrolserver.pollers.cloud_rogue_server_poller import ScsCloudRogueServerPoller
from smarfcontrolserver.cloudif.cloudif_mock import ScsCloudifMock
from smarfcontrolserver.cloudif.cloudif_factory import scsCloudServerIfFactoryGet
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_STATE,\
    SCS_CLOUDIF_STATE_INIT, SCS_CLOUDIF_ID, SCS_CLOUDIF_STATE_RUNNING,\
    SCS_CLOUDIF_STATE_STOPPED, SCS_CLOUDIF_AWS_PARAM_REGION_NAME,\
    SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID,\
    SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY, SCS_CLOUDIF_AWS_PARAM_IMAGE_ID,\
    SCS_CLOUDIF_AWS_PARAM_INSTANCE_TYPE, SCS_CLOUDIF_AWS_PARAM_KEYPAIR,\
    SCS_CLOUDIF_HOST, SCS_CLOUDIF_STATE_USABLE, SCS_CLOUDIF_STATE_INUSE
import time
from sm_controller.models import SmModelCloudServer
import paramiko
from smcommon.utils.net_utils import smLoadKeyFile
from smarfcontrolserver.cloudif.cloudif_aws import ScsCloudifAws
from smarfcontrolserver.init.envvar import scsGetEnvVar,\
    SCS_ENV_VAR_TEST_AWS_KEY_ID, SCS_ENV_VAR_TEST_AWS_SECRET_KEY,\
    SCS_ENV_VAR_CLOUD_SERVER_SHUTDOWN_HOLDOFF_SEC,\
    SCS_ENV_VAR_TEST_DISABLE_SLOW_TESTS, scsGetEnvVarBool
import os

logger = logging.getLogger(__name__) 
smTriggerSignalInstall()


class TestCloudIf(unittest.TestCase):
    '''
    This class implements low level (non-Django) tests for cloud interfaces
    '''

    def setUp(self):
        self.cloudFactory = scsCloudServerIfFactoryGet()
        
    def tearDown(self):
        if scsAwsTestKeysDefined(None):
            scsTerminataAllAwsCloudServers()

    def tryCreateListDelete(self, cloudifObject, serverParamsDict):
        # Create
        serverInfo1 = cloudifObject.create_server(serverParamsDict)
        serverId = serverInfo1[SCS_CLOUDIF_ID]
        serverInfo = cloudifObject.get_server_info(serverId)
        self.assertTrue(SCS_CLOUDIF_HOST in serverInfo)
        self.assertTrue(SCS_CLOUDIF_STATE in serverInfo)
        self.assertEqual(serverId, serverInfo[SCS_CLOUDIF_ID])
        
        # List - expect to see created server
        found = False
        serverList = cloudifObject.get_server_list()
        for server in serverList:
            if server[SCS_CLOUDIF_ID] == serverId:
                found = True
        self.assertTrue(found)
                
        # Terminate - expect to see server gone or not running
        cloudifObject.terminate_server(serverId)
        
        # Get for invalid server id
        self.assertFalse(cloudifObject.is_server_up(serverId))
        serverInfo = cloudifObject.get_server_info(serverId)
        if serverInfo is not None:
            self.assertFalse(cloudifObject.is_server_up(serverId, serverInfo))
        
        # List - expect to see created server not running or deleted
        found = False
        serverList = cloudifObject.get_server_list()
        for server in serverList:
            if cloudifObject.is_server_up(server[SCS_CLOUDIF_ID], server):
                found = True
        self.assertFalse(found)
                
    def testMockServer(self):
        cloudifObject = ScsCloudifMock()
        self.tryCreateListDelete(cloudifObject, {})
    
    def testAwsServer(self):
        if scsGetEnvVarBool(SCS_ENV_VAR_TEST_DISABLE_SLOW_TESTS):
            logger.warning("testAwsServer skipped because environnment variable %s is True",
                           SCS_ENV_VAR_TEST_DISABLE_SLOW_TESTS)
            return
        self.assertTrue(scsAwsTestKeysDefined("testAwsServer skipped"))
        
        cloud_info = {SCS_CLOUDIF_AWS_PARAM_REGION_NAME: "us-east-1"}
        cloud_account_info = {SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID: scsGetEnvVar(SCS_ENV_VAR_TEST_AWS_KEY_ID),
                              SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY: scsGetEnvVar(SCS_ENV_VAR_TEST_AWS_SECRET_KEY)
                              }
        cloudifObject = ScsCloudifAws(cloud_info, cloud_account_info)
        serverCreateParamsDict = {SCS_CLOUDIF_AWS_PARAM_IMAGE_ID: "ami-d05e75b8",
                                  SCS_CLOUDIF_AWS_PARAM_INSTANCE_TYPE: "t2.micro",
                                  SCS_CLOUDIF_AWS_PARAM_KEYPAIR: "cloud_server_test"}
        
        self.tryCreateListDelete(cloudifObject, serverCreateParamsDict)

 
class CloudObjectTest(TransactionTestCase):

    def setUp(self):
        self.helper = ScsTestHelper(self)
        self.helper.setupModel()
        self.helper.setupAwsCloudModel()
        self.helper.setupRestAuth("root")
        self.copyController = SmCopyController(["111", "testagentid"])
        smGlobalContextInit(self.copyController)
        self.factory = scsCloudServerIfFactoryGet()
        self.mockCloudIf = ScsCloudifMock()
        self.factory.setMockCloudIf(self.mockCloudIf) 
        self.callbackCount = 0
        self.callbackCloudServerStateToSet = SCS_CLOUDIF_STATE_USABLE
        
    def tearDown(self):
        if scsAwsTestKeysDefined(None): 
            scsTerminataAllAwsCloudServers()
        self.helper.tearDown()
        TransactionTestCase.tearDown(self)
        
    def waitUntilServerState(self, cloud_server_path, expectedState, maxTime=1.0, sleepTime=0.1):    
        endTime = time.time() + maxTime
        while time.time() < endTime:
            try:
                resp = self.helper.objGetViaRest(self.helper.cloud_server_path)
                if resp[SCS_CLOUDIF_STATE] == expectedState:
                    break
                time.sleep(sleepTime)
            except:
                break    
        self.assertEqual(resp[SCS_CLOUDIF_STATE], expectedState)

    def transitionToUsableFunc(self, cloud_server):
        self.callbackCount += 1
        cloud_server.state = self.callbackCloudServerStateToSet
        cloud_server.save()
        
    def test_statePoller(self):
        # Create poller
        self.callbackCloudServerStateToSet = SCS_CLOUDIF_STATE_INUSE
        cloudStatePoller = ScsCloudStatePoller(0.2, self.transitionToUsableFunc)
        cloudIfObj = self.factory.getCloudServerIf({}, {})
        try:
            cloudStatePoller.start()
        
            # Get the initial state
            resp = self.helper.objGetViaRest(self.helper.cloud_server_path)
            self.assertEqual(SCS_CLOUDIF_STATE_INIT, resp[SCS_CLOUDIF_STATE])
            
            # Create VM and put its id into the "stock" cloud server
            serverInfo = cloudIfObj.create_server({})
            server_id = serverInfo[SCS_CLOUDIF_ID]
            self.helper.objModifyViaRest(self.helper.cloud_server_path,
                                         {"server_id": server_id})
            
            # Change the state in the mock cloud server
            cloudIfObj.testif_setServerState(server_id, SCS_CLOUDIF_STATE_RUNNING)
            
            # Wait until the poller changes the state
            self.waitUntilServerState(self.helper.cloud_server_path, SCS_CLOUDIF_STATE_INUSE)                

            self.assertEqual(1, self.callbackCount)
        # Stop the poller
        finally:
            cloudStatePoller.stop()
        
    def waitUntilServerGone(self, cloudIfObj, server_id):
        count = 10
        while count > 0:
            count -= 1
            try:
                if cloudIfObj.get_server_info(server_id) is None:
                    break
                time.sleep(0.1)
            except:
                break
        
    def test_rogueServerPoller(self):
        # Create poller
        cloudRogueServerPoller = ScsCloudRogueServerPoller(0.2) 
        cloudIfObj = self.factory.getCloudServerIf({}, {})

        try:
            cloudRogueServerPoller.start()
        
            # Create VM and put its id into the "stock" cloud server
            serverInfo = cloudIfObj.create_server({})
            server_id = serverInfo[SCS_CLOUDIF_ID]
            self.helper.objModifyViaRest(self.helper.cloud_server_path,
                                         {"server_id": server_id})
            
            # Wait for a poll cycle
            time.sleep(0.3)
                        
            # Check that the server was not deleted
            serverInfo = cloudIfObj.get_server_info(server_id)
        
            # Change the state in Django (cannot change via REST API because field is read-only
            cloud_server = SmModelCloudServer.objects.get(server_id=server_id)
            cloud_server.state = SCS_CLOUDIF_STATE_STOPPED
            cloud_server.save()
        
            # Wait until the cloud server is terminated
            self.waitUntilServerGone(cloudIfObj, server_id)
            self.assertIsNone(cloudIfObj.get_server_info(server_id))
            
            # Add a rogue server in the mock cloud
            serverInfo = cloudIfObj.create_server({})

            # Wait until the rogue server is terminated
            self.waitUntilServerGone(cloudIfObj, server_id)
            self.assertIsNone(cloudIfObj.get_server_info(server_id))
        finally:
            cloudRogueServerPoller.stop()
    
    def doSshCmd(self, host, user, keyfile, command):
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        mykey = smLoadKeyFile(keyfile)
        logger.debug("Connecting to %s as %s", host, user)
        ssh_client.connect(host, pkey=mykey, username=user, timeout=3)
        logger.debug("Sending command %s", command)
        _, stdout, stderr = ssh_client.exec_command(command)
        stdout_string = stdout.read()
        stderr_string = stderr.read()
        return stdout.channel.recv_exit_status(), stdout_string, stderr_string
    
    def commonStartStopTest(self, runCommand, wait):
    
        # Start server
        self.helper.objGetViaRest(self.helper.cloud_server_path + "start/")
        
        # Wait until server up
        self.waitUntilServerState(self.helper.cloud_server_path,
                                  SCS_CLOUDIF_STATE_USABLE, wait[0], wait[1])
        
        # Run command (if allowed)
        if runCommand:
            resp = self.helper.objGetViaRest(self.helper.cloud_server_path)
            ret_code, stdout, stderr = self.doSshCmd(resp["host"], "ubuntu", "cloud_server_test.pem", "ls -d /tmp")
            self.assertEqual(0, ret_code)
            self.assertEqual(b"/tmp\n", stdout)
            self.assertEqual(b"", stderr)
        
        # Stop server 
        self.helper.objGetViaRest(self.helper.cloud_server_path + "stop/")
        
        # Wait until server stopped
        self.waitUntilServerState(self.helper.cloud_server_path,
                                  SCS_CLOUDIF_STATE_STOPPED, wait[0], wait[1])
        self.assertEqual(1, self.callbackCount)
    
    def test_awsStartStop(self):
        if scsGetEnvVarBool(SCS_ENV_VAR_TEST_DISABLE_SLOW_TESTS):
            logger.warning("test_awsStartStop skipped because environnment variable %s is True",
                           SCS_ENV_VAR_TEST_DISABLE_SLOW_TESTS)
            return
        self.assertTrue(scsAwsTestKeysDefined("testAwsServer skipped"))

        self.factory.setMockCloudIf(None) 
        cloudStatePoller = ScsCloudStatePoller(5, self.transitionToUsableFunc)
        try:
            cloudStatePoller.start()
        
            self.commonStartStopTest(True, (90, 3))
        finally:
            cloudStatePoller.stop()
    
    def test_mockStartStop(self):
        cloudStatePoller = ScsCloudStatePoller(0.2, self.transitionToUsableFunc)
        try:
            cloudStatePoller.start()
        
            self.commonStartStopTest(False, (1, 0.1))
        finally:
            cloudStatePoller.stop()
    
    def test_autoStop(self):
        self.callbackCloudServerStateToSet = SCS_CLOUDIF_STATE_INUSE
        cloudStatePoller = ScsCloudStatePoller(0.2, self.transitionToUsableFunc)
        os.environ[SCS_ENV_VAR_CLOUD_SERVER_SHUTDOWN_HOLDOFF_SEC] = "1"
        
        try:
            cloudStatePoller.start()
        
            self.helper.objGetViaRest(self.helper.cloud_server_path + "start/")
            resp = self.helper.objGetViaRest(self.helper.cloud_server_path)
            server_id = resp["server_id"]
            self.waitUntilServerState(self.helper.cloud_server_path,
                                      SCS_CLOUDIF_STATE_INUSE, 2, 0.5)
            cloudIfObj = self.factory.getCloudServerIf({}, {})
            
            # Check that server does not terminate if in use
            time.sleep(2)
            
            resp = self.helper.objGetViaRest(self.helper.cloud_server_path)
            self.assertEqual(SCS_CLOUDIF_STATE_INUSE, resp["state"])

            # Change state to USABLE
            cloud_server_obj = SmModelCloudServer.objects.get(server_id=server_id)
            start_time = time.time()
            cloud_server_obj.state = SCS_CLOUDIF_STATE_USABLE
            cloud_server_obj.save()
            
            # Wait until server terminates
            self.waitUntilServerState(self.helper.cloud_server_path,
                                      SCS_CLOUDIF_STATE_STOPPED, 4, 0.2)
            done_time = time.time()
            # Should take a little more than 1 second, so fail if it took more that 3 seconds
            self.assertTrue((done_time - start_time) < 3)
            
            self.assertIsNone(cloudIfObj.get_server_info(server_id))
        finally:
            cloudStatePoller.stop()
    
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()