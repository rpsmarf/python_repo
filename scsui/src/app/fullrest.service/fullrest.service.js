angular.module('rpsmarf.fullrest.service', [
	'restangular'
])
.factory('SmFullRest', function(Restangular){
	return Restangular.withConfig(function(RestangularConfigurer){
		RestangularConfigurer.setFullResponse(true);
	});
});