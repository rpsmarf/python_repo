'''
Created on Sep 5, 2014

@author: rpsmarf
'''
import unittest
import Ice
import smmonicemsgs
from smcommon.utils.net_utils import smGetFreePort
from smcommon.file_access.ice_file_ops import SmIceFsOps
from smcommon.globals.sm_global_context import smGlobalContextInit
from smcommon.globals.sm_copy_controller import SmCopyController
from smcommon.tests.dad_test_utils import TempFolder
from smcommon.constants import sm_const
from smcommon.file_access.data_access_desc import SmDataContainer
from smcommon.logging.sm_logger import smLoggingSetupForTest
import smraicemsgs
import json
from smcommon.tests.ssh_server import sm_start_test_sftp_server, \
    sm_test_sftp_server_stop
from smcommon.task_runners.ice_async_ops_server import SmIceAsyncOpsServer
from smraicemsgs import SmRemAgAsyncOpResps  # @UnresolvedImport
from smcommon.tasks.task_desc import SmTaskDesc
import time
from smcommon.exceptions.sm_exc import SmException
import os
from smcommon.globals.envvar import SM_ENV_VAR_COMMON_MIN_TIME_BETWEEN_PROGRESS


class HeartbeatI(smmonicemsgs.SmHeartbeat):  # @UndefinedVariable
    '''
    Implementation of ICE class
    '''
    def sendHeartbeat(self, heartId, beatCount, current=None):
        return 99

     
class TestAsyncResps(SmRemAgAsyncOpResps):

    def __init__(self):
        self.clear()
        
    def clear(self):
        self.lastOpId = None
        self.lastStatusJson = None
        self.lastStatus = None
        self.lastRespJson = None
        self.progressCount = 0
        self.doneCount = 0
        
    def progress(self, opId, statusJson, current=None):
        self.progressCount += 1
        self.lastOpId = opId
        self.lastStatusJson = statusJson

    def done(self, opId, status, respJson, current=None):
        self.doneCount += 1
        self.lastOpId = opId
        self.lastStatus = status
        self.lastRespJson = respJson


class IceTest(unittest.TestCase):
    '''
    This class tests the ICE client interfaces talking to the ICE server interfaces
    '''

    def setUp(self):
        self.tempFolder = TempFolder()
        self.localDataContainer = SmDataContainer(sm_const.SM_MY_AGENT_ID, "",
                                                  sm_const.SM_DATA_CONTAINER_PROTOCOL_LOCAL + 
                                                  "://localhost/" + self.tempFolder.folder)
        self.sftpDataContainer = SmDataContainer(sm_const.SM_MY_AGENT_ID, "",
                                                 sm_const.SM_DATA_CONTAINER_PROTOCOL_SFTP +
                                                 "://127.0.0.1:8222/" + self.tempFolder.folder)
        smLoggingSetupForTest()
        self.copyController = SmCopyController([self.localDataContainer.agentId])

        smGlobalContextInit(self.copyController)
        self.port = smGetFreePort()
        self.ic = Ice.initialize()
        adapter = self.ic.createObjectAdapterWithEndpoints("smarf", "default -p " + str(self.port))
        obj = HeartbeatI()
        adapter.add(obj, self.ic.stringToIdentity("SimplePrinter"))
        obj = SmIceFsOps()
        adapter.add(obj, self.ic.stringToIdentity("FileOps"))
        self.asyncObj = SmIceAsyncOpsServer(self.ic, "SmRemAgAsyncOpResps:default -p " + str(self.port))
        adapter.add(self.asyncObj, self.ic.stringToIdentity("SmRemAgAsyncOps"))
        self.asyncRespObj = TestAsyncResps()
        adapter.add(self.asyncRespObj, self.ic.stringToIdentity("SmRemAgAsyncOpResps"))
        adapter.activate()

    def tearDown(self):
        self.tempFolder.cleanup()
        if self.ic:
            # Clean up 
            self.ic.destroy()

    def testIceBasics(self):
        base = self.ic.stringToProxy("SimplePrinter:default -p " + str(self.port))
        heart = smmonicemsgs.SmHeartbeatPrx.checkedCast(base)  # @UndefinedVariable
        if not heart:
            raise RuntimeError("Invalid proxy")

        result = heart.sendHeartbeat("Hello World!", 8)
        self.assertEqual(result, 99)

    def opsTests(self, fsOps, container):
        # Connect to the remote system
        fasId = fsOps.connect(container.toJson(), 1000.0)
        
        # Write file f1
        f = fsOps.openForWrite(fasId, "f1")
        fsOps.writeData(fasId, f, b"123")
        fsOps.close(fasId, f)
        
        # List folder, see file
        listInfoJson = fsOps.list(fasId, "/", 1, 10, None, True)
        listInfo = json.loads(listInfoJson)
        self.assertEqual(1, len(listInfo))
        self.assertEqual("f1", listInfo[0]["name"])
  
        frenchName = "f2ùûüÿàâæçé€èêëïîôœ"
        # Write file f2
        f = fsOps.openForWrite(fasId, frenchName)
        fsOps.writeData(fasId, f, b"1234")
        fsOps.close(fasId, f)
    
        # Read file back
        f = fsOps.openForRead(fasId, frenchName)
        s = fsOps.readData(fasId, f, 99)
        fsOps.close(fasId, f)
        self.assertEqual(b"1234", s)
        
        # Create folder folder3
        fsOps.mkdir(fasId, "folder3", False)
        
        # Write f4 in folder3
        f = fsOps.openForWrite(fasId, "folder3/f4")
        fsOps.writeData(fasId, f, b"123455")
        fsOps.close(fasId, f)

        # Read file back
        f = fsOps.openForRead(fasId, "folder3/f4")
        s = fsOps.readData(fasId, f, 99)
        fsOps.close(fasId, f)
        self.assertEqual(b"123455", s)
                
        # List the top folder, see all
        listInfoJson = fsOps.list(fasId, "/", 10, 10, None, True)
        listInfo = json.loads(listInfoJson)
        self.assertEqual(4, len(listInfo))
        self.assertEqual("f1", listInfo[0]["name"])
        self.assertEqual(frenchName, listInfo[1]["name"])
        self.assertEqual("folder3/", listInfo[2]["name"])
        self.assertEqual("folder3/f4", listInfo[3]["name"])
        
        # List folder3 , see f4
        listInfoJson = fsOps.list(fasId, "folder3/", 10, 10, None, True)
        listInfo = json.loads(listInfoJson)
        self.assertEqual(1, len(listInfo))
        self.assertEqual("folder3/f4", listInfo[0]["name"])

        # List f4, see only f4
        listInfoJson = fsOps.list(fasId, "folder3/f4", 10, 10, None, True)
        listInfo = json.loads(listInfoJson)
        self.assertEqual(1, len(listInfo))
        self.assertEqual("folder3/f4", listInfo[0]["name"])

        # Delete f4, check it is gone
        paths = []
        for v in listInfo:
            paths.append(v["name"])
        fsOps.delete(fasId, json.dumps(paths))
        
        # Create f5 in folder3
        f = fsOps.openForWrite(fasId, "folder3/f5")
        fsOps.writeData(fasId, f, b"123455")
        fsOps.close(fasId, f)

        # Delete folder3, check it is gone
        listInfoJson = fsOps.list(fasId, "folder3//", 10, 10, None, True)
        listInfo = json.loads(listInfoJson)
        paths = []
        for v in reversed(listInfo):
            paths.append(v["name"])
        fsOps.delete(fasId, json.dumps(paths))

        listInfoJson = fsOps.list(fasId, "/", 1, 10, None, True)
        listInfo = json.loads(listInfoJson)
        self.assertEqual(2, len(listInfo))
        self.assertEqual("f1", listInfo[0]["name"])
        
        # Disconnect
        fsOps.disconnect(fasId)

    def testIceFileOpsLocal(self):
        base = self.ic.stringToProxy("FileOps:default -p " + str(self.port))
        fsOps = smraicemsgs.SmRemAgFileOpsPrx.checkedCast(base)  # @UndefinedVariable
        if not fsOps:
            raise RuntimeError("Invalid proxy")

        self.opsTests(fsOps, self.localDataContainer)
           
    def testIceFileOpsSftp(self):
        base = self.ic.stringToProxy("FileOps:default -p " + str(self.port))
        fsOps = smraicemsgs.SmRemAgFileOpsPrx.checkedCast(base)  # @UndefinedVariable
        if not fsOps:
            raise RuntimeError("Invalid proxy")
        self.sftpDataContainer.agentUri = "ice://default:" + str(self.port)
        sm_start_test_sftp_server()
        try:
            self.opsTests(fsOps, self.sftpDataContainer)
        finally:
            sm_test_sftp_server_stop()
           
    def testDirectFileOpsLocal(self):
        fsOps = SmIceFsOps()
        self.opsTests(fsOps, self.localDataContainer)
           
    def testDirectFileOpsSftp(self):
        sm_start_test_sftp_server()
        try:
            fsOps = SmIceFsOps()
            self.sftpDataContainer.agentUri = "ice://default:" + str(self.port)
            self.opsTests(fsOps, self.sftpDataContainer)
        finally:
            sm_test_sftp_server_stop()
            
    def waitForDoneCount(self, count):
        maxCount = 50
        while (maxCount > 0):
            if self.asyncRespObj.doneCount == count:
                return
            maxCount -= 1
            time.sleep(0.1)
        raise SmException("Expected doneCount to get to " + str(count) + 
                          "  Found count " + str(self.asyncRespObj.doneCount))
    
    def doAsyncOperations(self, raOps):
        # Request copy, 1 byte at a time
        self.asyncRespObj.doneCount = 0
        srcDad = self.tempFolder.makeLocalDad("d1/f1")
        destDad = self.tempFolder.makeLocalDad("d2/f2")
        taskDesc = SmTaskDesc("smcommon.task_runners.task_runner_copy", "SmTaskRunnerCopy")
        taskDesc.addTaskParam("args", [{"type": "dad", "direction": "input", "dad": srcDad.toDict()}, 
                                       {"type": "dad", "direction": "output", "dad": destDad.toDict()}, 
                                       {"type": "string", "value": 1}])
        raOps.startOperation("myid", "task", taskDesc.toJson())
    # Wait for done
        self.waitForDoneCount(1)
    # Check for progress callbacks
        self.assertEqual("myid", self.asyncRespObj.lastOpId)
        self.assertEqual(10, self.asyncRespObj.progressCount)
    # Check that file has the right content
        s = self.tempFolder.readFile("d2/f2", 100)
        self.assertEqual(b"0123456789", s)
    # Clear state
        self.asyncRespObj.clear()
    # Make second request with 1000 byte copy
        taskDesc = SmTaskDesc("smcommon.task_runners.task_runner_copy", "SmTaskRunnerCopy")
        destDad = self.tempFolder.makeLocalDad("d2/f3")
        taskDesc.addTaskParam("args", [{"type": "dad", "direction": "input", "dad": srcDad.toDict()},  
                                       {"type": "dad", "direction": "output", "dad": destDad.toDict()}, 
                                       {"type": "string", "value": 100}])
        raOps.startOperation("myid2", "task", taskDesc.toJson())
    # Wait for done
        self.waitForDoneCount(1)
    # Check no progress reported
        self.assertEqual("myid2", self.asyncRespObj.lastOpId)
        self.assertEqual(1, self.asyncRespObj.progressCount)
    # Check that file has the right content
        s = self.tempFolder.readFile("d2/f3", 1000)
        self.assertEqual(b"0123456789", s)

    def testTaskIfDirect(self):
        # Create 10 byte input file
        self.tempFolder.mkdir("d1")
        self.tempFolder.mkdir("d2")
        self.tempFolder.writeFile("d1/f1", "0123456789")
        
        # Create client object
        os.environ[SM_ENV_VAR_COMMON_MIN_TIME_BETWEEN_PROGRESS] = "0"
        #base = self.ic.stringToProxy("SmRemAgAsyncOps:default -p " + str(self.port) + " -t 5000")
        #raOps = smraicemsgs.SmRemAgAsyncOpsPrx.checkedCast(base)  # @UndefinedVariable
        raOps = self.asyncObj # This is useful for source level debugging which does not work through ICE
        
        self.doAsyncOperations(raOps)

    def testTaskIfIce(self):
        # Create 10 byte input file
        self.tempFolder.mkdir("d1")
        self.tempFolder.mkdir("d2")
        self.tempFolder.writeFile("d1/f1", "0123456789")
        
        # Create client object
        os.environ[SM_ENV_VAR_COMMON_MIN_TIME_BETWEEN_PROGRESS] = "0"
        base = self.ic.stringToProxy("SmRemAgAsyncOps:default -p " + str(self.port) + " -t 5000")
        raOps = smraicemsgs.SmRemAgAsyncOpsPrx.checkedCast(base)  # @UndefinedVariable
        # raOps = self.asyncObj # This is useful for source level debugging which does not work through ICE
        
        self.doAsyncOperations(raOps)

if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testIceBasics']
    unittest.main()
