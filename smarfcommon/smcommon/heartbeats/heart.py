'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import threading
import time
import logging

logger = logging.getLogger(__name__)


class SmHeart(threading.Thread):
    '''
    This class generates a periodic heartbeat message from this process to the 
    SMARF Control Server to indicate that it is still alive. 
    '''

    def __init__(self, heartId, timeBetweenBeats, heartbeatCallback):
        '''
        Constructor
        @param heartId:a unique id for this heart which is sent to the heart monitor
        @param timeBetweenBeats: a floating point number for how much time must elapse
        between heartbeats being sent to the heart monitor
        @param heartbeatCallback: the function invoked to send the heartbeat 
         
        '''
        threading.Thread.__init__(self, name="Heartbeater")
        self.timeBetweenBeats = timeBetweenBeats
        logger.info("timeBetweenBeats = %f", timeBetweenBeats)
        self.heartbeatCallback = heartbeatCallback
        self.heartId = heartId
        self.quit = False
        self.beatCount = 0
        
    def run(self):
        '''
        Thread running method
        '''
        while (not self.quit):        
            try:
                self.beatCount += 1
                if self.beatCount < 10 or (self.beatCount % 100 == 0):
                    logger.debug("heart[" + self.heartId + "] send beat " + str(self.beatCount))
                self.heartbeatCallback(self.heartId, self.beatCount)
            except:
                logger.exception('Exception during heartbeat', exc_info=True)
            time.sleep(self.timeBetweenBeats)

    def stopHeart(self):
        '''
        Stops the heartbeat thread.  May block until up to the heartbeat time.
        This is expected to be used primarily during testing.
        @return: True if the heart stopped successfully, false otherwise
        '''
        logger.debug("Stopping heart " + self.heartId)
        self.quit = True
        self.join(self.timeBetweenBeats * 2)
        logger.debug("Stopped heart " + self.heartId)
        return not self.isAlive()
    