import boto
import logging
from smcommon.utils.date_utils import sm_date_to_unixtime_sec,\
    smMakeDateFromString
from smcommon.exceptions.file_access_exception import SmFileAccessException
logger = logging.getLogger(__name__)

  
class AwsS3Accessor(object):
      
    def __init__(self, accessID, secretAccessID, bucketName, retryCount):
        self.accessID = accessID
        self.secretAccessID = secretAccessID
        self.bucketName = bucketName
        self.retryCount = retryCount
        self.connection = self.getConnection()   
        self.bucketName = bucketName
        try:
            self.mybucket = self.connection.get_bucket(self.bucketName)
        except:
            self.mybucket = None
            
    def getConnection(self):
        retry = self.retryCount
        while (retry > 0):
            try:
                retry = 0
                s3 = boto.connect_s3(self.accessID, self.secretAccessID)
                return s3
            except Exception:
                retry = retry - 1
                if retry <= 0:
                    raise 
                    
    def listBuckets(self):
        '''
        Returns the list of containers 
        '''            
        retryCount = self.retryCount
        result_list = []
        while True:
            try:
                rs = self.connection.get_all_buckets()
                for bucket in rs:
                    result_list.append({"name": bucket.name})
                break
            except Exception as e:
                retryCount = retryCount - 1
                if retryCount == 0:
                    raise SmFileAccessException(422, "EOTHER", "", "Bucket list failed: " + str(e))
              
        return result_list

    def listKeysInsideBucket(self, path, recursive, maxEntries, lastPathReturned):    
        retry = self.retryCount
        while (retry > 0):
            return_list = []
            try:
                retry = 0
                if recursive:
                    if path == "" or path == "/":
                        key_list = self.mybucket.list(marker=lastPathReturned)
                    else:
                        key_list = self.mybucket.list(prefix=path, marker=lastPathReturned)
                else:
                    key_list = self.mybucket.list(path, delimiter="/", marker=lastPathReturned)
                    
                for key in key_list:
                    name = key.name
                    if name == path:
                        continue
                    isDir = name.endswith("/")
                    if isDir:
                        size = 0
                        mtime = 0 
                    else:
                        size = key.size
                        mtime = int(sm_date_to_unixtime_sec(smMakeDateFromString(key.last_modified)))
                        
                    return_list.append({"name": name,
                                        "mode": "rwx",
                                        "isDir": isDir,
                                        "size": size,
                                        "mtime": mtime})
                    if len(return_list) >= maxEntries:
                        return return_list
                    
                return return_list
            except Exception:
                retry = retry - 1
                if retry <= 0:
                        raise Exception
                      
    def getMetadata(self, path):    
        retry = self.retryCount
        while (retry > 0):
            try:
                key = self.mybucket.get_key(path)
                if key is None or not key.exists():
                    raise SmFileAccessException(404, "ENOENT", path, "No such path found")
                name = key.name
                if name == "":
                    name = "/"
                isDir = name.endswith("/")
                if isDir:
                    size = 0
                    mtime = 0 
                else:
                    size = key.size
                    last_modified = key.date
                    mtime = int(sm_date_to_unixtime_sec(smMakeDateFromString(last_modified)))
                    
                return {"name": name,
                        "mode": "rwx",
                        "isDir": isDir,
                        "size": size,
                        "mtime": mtime}
            except Exception:
                retry = retry - 1
                if retry <= 0:
                        raise 
                      
    def createBucket(self):
        retry = self.retryCount
        while (retry > 0):
            try:
                buck = self.connection.create_bucket(self.bucketName)
                self.mybucket = buck
                retry = 0
                return buck
            except Exception:
                retry = retry - 1
                if retry <= 0:
                        raise Exception
                      
    def deleteBucket(self):
        retry = self.retryCount
        while (retry > 0):
            try:
                self.connection.delete_bucket(self.bucketName)
                retry = 0
            except Exception:
                retry = retry - 1
                if retry <= 0:
                        raise Exception
                    
    def removeFileOrFolder(self, pathtokey):
        retry = self.retryCount
        while (retry > 0):         
            try:
                key = self.mybucket.new_key(pathtokey)
                if key.exists():
                    key.delete()
                    retry = 0
                else:
                    raise SmFileAccessException(422, "ENOENT", pathtokey, "Path does not exist")
            except Exception:
                retry = retry - 1
                if retry <= 0:
                        raise              
                  
    def createSubFolder(self, folderName):
        if not folderName.endswith("/"):
            folderName += "/"
            
        retry = self.retryCount
        while (retry > 0):         
            try:
                key = self.mybucket.new_key(folderName)
                key.set_contents_from_string("")
                return
            except Exception:
                retry = retry - 1
                if retry <= 0:
                    logger.exception("Error in create folder")
                    raise
      
    def downloadFile(self, cloud_path, local_path):
        retry = self.retryCount
        while (retry > 0):         
            try:
                key = self.mybucket.lookup(cloud_path)
                if key is not None and key.exists():
                    key.get_contents_to_filename(local_path)
                    return
                else:
                    raise Exception("File at {} not found".format(cloud_path))
            except Exception:
                retry = retry - 1
                if retry <= 0:
                    raise Exception
    
    def downloadFileViaIterator(self, cloud_path):
        retry = self.retryCount
        while (retry > 0):         
            try:
                key = self.mybucket.lookup(cloud_path)
                if key.exists():
                    return key
                else:
                    raise Exception("No path found at " + cloud_path)
            except Exception:
                retry = retry - 1
                if retry <= 0:
                    raise Exception
    
    def uploadFile(self, fp, size, destPath):
        retry = self.retryCount
        while (retry > 0):          
            try:
                key = self.mybucket.new_key(destPath)
                break
            except:
                retry = retry - 1
                if retry <= 0:
                    raise
        # We cannot retry this because it is operating on a stream
        # We set rewind=True to avoid a bunch of seek/tell calls in boto
        # for backwards compatibility in exchange for a single seek(0, ...)
        key.size = size
        key.send_file(fp)
                       
    def uploadFileName(self, filename, destPath):
        retry = self.retryCount
        while (retry > 0):          
            try:
                key = self.mybucket.new_key(destPath)
                key.set_contents_from_filename(filename)
                return
            except Exception:
                retry = retry - 1
                if retry <= 0:
                        raise Exception
       
    def uploadMultipartStart(self, path):
        return self.mybucket.initiate_multipart_upload(path)
    
    def uploadMultipartChunk(self, uploader, part_num, fp, size):
        uploader.upload_part_from_file(fp, part_num, size=size)
        
    def uploadMultipartEnd(self, uploader):
        return uploader.complete_upload()
''' 
s5= AWSS3Accessor('AKIAI3FC2MD6HETFHKJQ', 'KVjWBYavq0naOmiF06XpjH6Df5fX6eCSNCFH+Qyr', 'rpsmarftest', 1)
#f=open('C:\\data\\DB11gR2_x86_64_WIN\\win64_11gR2_database_1of2\\database\\welcome.html','r+')
#s5.downloadFile('testFolder/testFile1.txt','/home/rpsmarf/Public/testdir/testFile2.txt')
#s5.uploadFile(f,'testFolder')
#s5.removeFileOrFolder("a/b/c/welcome.html")
#print(s5.checkIfKeyExists("a/b/c/welcome.html"))
print(s5.listKeysInsideBucket())
'''