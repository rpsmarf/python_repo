import json
import os
import sys
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath, scsGetToJson
from scripts.scs_obj_create import scsCreateObjectPath

try:
    os.mkdir("/tmp/src")
except:
    pass
try:
    os.mkdir("/tmp/dest")
except:
    pass

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

root_path = scsGetObjectPath("user", "username=rpsmarf")
user_a = scsGetObjectPath("user", "username=a")
user_b = scsGetObjectPath("user", "username=b")


carleton_container_path = scsGetObjectPath("container", "name_key=carleton_demos_data") 
rt_cloud_files_data_path = scsGetObjectPath("resource_type", "name_key=generic_storage")


rt_mock_data_path = scsCreateObjectPath("resource_type", {"name": "tool_copy_source", 
                                                     "nature": "data",
                                                     "description": "This is a source of mock data"})
res_path = scsCreateObjectPath("resource", {"name": "Copy Mock Data A", 
                                            "name_key": "copy_carleton_demo_data_a",
                                            "resource_type": rt_mock_data_path,
                                            "owner": root_path,
                                            "container": carleton_container_path,
                                            "parametersJson": json.dumps({"folder": "mock_data_a/"}),
                                            "description": "This is the repository of mock data 'A'"
                                            })
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=rw")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=r")


TASK_TYPE_COPY_CFG_DICT = {
                           "args": [ 
                                    {"type": "resource", 
                                     "name": "Source File", 
                                     "description": "This is the file from which data is copied",
                                     "pathType": "file", 
                                     "input_id": 1, 
                                     "direction": "input", "role": "src"},
                                    {"type": "resource", 
                                     "name": "Destination File",
                                     "description": "This is the file into which the data is copied", 
                                     "pathType": "file", 
                                     "input_id": 2, 
                                     "direction": "output", 
                                     "role": "dest"},
                                    ],
                           "outputProgress": {"outputArg": 2},
                           "computeResourceRole": "dest",
                           "skipRunPermCheck": True,
                           "preExistingOutputFolderOk": True
                       }

tt_path = scsCreateObjectPath("task_type", {"name": "copy", 
                                            "uiVisible": False,
                                            "code_module": "smcommon.task_runners.task_runner_copy", 
                                            "code_classname": "SmTaskRunnerCopy", "description": "Task to copy data",
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_COPY_CFG_DICT)})
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

scsCreateObjectPath("resource_type_task_type", {"role": "src", "task_type": tt_path, "resource_type": rt_mock_data_path,
                                                "name": "Copy source data",
                                                "name_key": "copy_source_data",
                                                "nature": "data",
                                                "description": "This is the source of data for a copy"})
scsCreateObjectPath("resource_type_task_type", {"role": "dest", "task_type": tt_path, "resource_type": rt_cloud_files_data_path,
                                                "name": "Copy destination data",
                                                "name_key": "copy_dest_data",
                                                "nature": "data",
                                                "description": "This is a destination folder"})
 
print("Task type 'copy' objects setup.")
