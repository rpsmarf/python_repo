'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import Ice
import logging

'''
Global variable which contains all global state
'''
global _smGlobalContext_g
_smGlobalContext_g = None
logger = logging.getLogger(__name__)


class _SmGlobalContext(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        pass


def smGlobalContextInit(copyController):
    global _smGlobalContext_g
    _smGlobalContext_g = _SmGlobalContext()
    _smGlobalContext_g.copyController = copyController
    _smGlobalContext_g.ic = Ice.initialize()

    
def smGlobalContextClear():
    global _smGlobalContext_g
    if _smGlobalContext_g is None:
        return
    
    if _smGlobalContext_g.copyController is not None:
        _smGlobalContext_g.copyController.purgeTmpFolder()
    _smGlobalContext_g = None
 
    
def smGlobalContextGet():
    global _smGlobalContext_g
    return _smGlobalContext_g