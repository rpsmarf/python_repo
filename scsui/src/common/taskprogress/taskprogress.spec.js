describe( 'Task progress', function() {
	beforeEach (module('rpsmarf.config'));
	
	beforeEach(module('rpsmarf.taskprogress'));
	beforeEach(module('ui.router'));
	beforeEach(module('restangular'));
	
	beforeEach(module('taskprogress/taskprogress.tpl.html'));
	//$scope, $modalInstance, taskObj, $interval,TASK_STATE_ENUM, modalTitle, interactive, $modal, TASK_STATE_FINISHED, TASK_STATE_RUNNING
	var $scope, $controller, $interval, $modalInstance, $modal, smarfurl, taskObject, $httpBackend, responseObj;
	//init, starting, prep, running, cleanup and finished
	

	beforeEach(  inject(function(_$controller_, $rootScope, _$interval_, Restangular, SMARF_URL, _$modal_){
		smarfurl = SMARF_URL;
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
		$interval = _$interval_;
		taskObject = Restangular.one('scs/task','1/');
		$scope = $rootScope.$new();
		responseObj = {
					"completion": "completedWithoutError",
					"completionDescription": "TBD",
					"completionReason": "success",
					"end_time": "2014-11-11T03:07:43",
					"gid": 103,
					"id": 1,
					"owner": "/scs/user/1/",
					"parametersJson": "{ \"input_ids\":{\"1\": {\"path\": \"2014-09-02.data\"}, \"2\": {\"path\": \"l1/l2/2014-09-02.data\"}}}",
					"progressJson": "{\"runtime\": 0.1342160701751709, \"progress\": 0.0, \"outputSize\": 0, \"state\": \"running\"}",
					"resource_uri": "/scs/task/20/",
					"resultsJson": "{}",
					"runningDisplayJson": "",
					"start_time": "2014-11-10T03:07:43",
					"state": "init",
					"task_type": "/scs/task_type/1/"
				};

		$modal = _$modal_;
		

	})) ;

	it("should show the correct state and numbers based on the returned progressJson for non-interactive tasks", inject(function(_$controller_, _$httpBackend_, TASK_STATE_ENUM) {
		$httpBackend = _$httpBackend_;
		$controller = _$controller_('TaskProgressCtrl',{$scope:$scope,taskObj: taskObject, $interval:$interval, $modalInstance: {result:{then:function(param){ return true;}}}, modalTitle:"", interactive:false} );
		$httpBackend.when('GET',smarfurl+'/scs/task/1/').respond(200,responseObj);
		$interval.flush(1000);
		$httpBackend.flush();
		expect($scope.taskState).toEqual(TASK_STATE_ENUM['init']);
		expect($scope.percentComplete).toEqual(0.0);
		

		responseObj.state = 'starting';
		responseObj.progressJson = "{\"runtime\": 0.1342160701751709, \"progress\": 30.0, \"outputSize\": 0, \"state\": \"running\"}";
		$httpBackend.when('GET',smarfurl+'/scs/task/1/').respond(200,responseObj);
		$interval.flush(1000);
		$httpBackend.flush();
		expect($scope.taskState).toEqual(TASK_STATE_ENUM['starting']);
		expect($scope.percentComplete).toEqual(30.0);
		

		responseObj.state = 'prep';
		responseObj.progressJson = "{\"runtime\": 0.1342160701751709, \"progress\": 80.0, \"outputSize\": 0, \"state\": \"running\"}";
		$httpBackend.when('GET',smarfurl+'/scs/task/1/').respond(200,responseObj);
		$interval.flush(1000);
		$httpBackend.flush();
		expect($scope.taskState).toEqual(TASK_STATE_ENUM['prep']);
		expect($scope.percentComplete).toEqual(80.0);
		

		responseObj.state = 'running';
		responseObj.progressJson = "{\"runtime\": 0.1342160701751709, \"progress\": 20.0, \"outputSize\": 0, \"state\": \"running\"}";
		$httpBackend.when('GET',smarfurl+'/scs/task/1/').respond(200,responseObj);
		$interval.flush(1000);
		$httpBackend.flush();
		expect($scope.taskState).toEqual(TASK_STATE_ENUM['running']);
		expect($scope.percentComplete).toEqual(20.0);


		responseObj.state = 'running';
		responseObj.progressJson = "{\"runtime\": 0.1342160701751709, \"progress\": 100.0, \"outputSize\": 0, \"state\": \"running\"}";
		$httpBackend.when('GET',smarfurl+'/scs/task/1/').respond(200,responseObj);
		$interval.flush(1000);
		$httpBackend.flush();
		expect($scope.taskState).toEqual(TASK_STATE_ENUM['running']);
		expect($scope.percentComplete).toEqual(100.0);
	

		spyOn($interval, 'cancel');
		responseObj.state = 'finished';
		responseObj.progressJson = "{\"runtime\": 0.1342160701751709, \"progress\": 100.0, \"outputSize\": 0, \"state\": \"running\"}";
		$httpBackend.when('GET',smarfurl+'/scs/task/1/').respond(200,responseObj);
		$interval.flush(1000);
		$httpBackend.flush();
		expect($scope.taskState).toEqual(TASK_STATE_ENUM['finished']);
		expect($scope.percentComplete).toEqual(100.0);
		
		//$scope.$digest();
		expect($interval.cancel).toHaveBeenCalled();

		
	}));

	it("should show the correct state and numbers based on the returned progressJson for interactive tasks", inject(function(_$controller_, _$httpBackend_, TASK_STATE_ENUM ) {
		var cb = null;
		$httpBackend = _$httpBackend_;
		var $modalInstance = {result:{then:function(callback){ cb = callback;}}, close: function(){return true;}};
		$controller = _$controller_('TaskProgressCtrl',{$scope:$scope, $modal: $modal, taskObj: taskObject, $interval:$interval, $modalInstance: $modalInstance, modalTitle:"", interactive:true} );
		$httpBackend.when('GET',smarfurl+'/scs/task/1/').respond(200,responseObj);
		$interval.flush(1000);
		$httpBackend.flush();
		expect($scope.taskState).toEqual(TASK_STATE_ENUM['init']);
		expect($scope.percentComplete).toEqual(0.0);
		

		responseObj.state = 'starting';
		responseObj.progressJson = "{\"runtime\": 0.1342160701751709, \"progress\": 30.0, \"outputSize\": 0, \"state\": \"running\"}";
		$httpBackend.when('GET',smarfurl+'/scs/task/1/').respond(200,responseObj);
		$interval.flush(1000);
		$httpBackend.flush();
		expect($scope.taskState).toEqual(TASK_STATE_ENUM['starting']);
		expect($scope.percentComplete).toEqual(30.0);
		

		responseObj.state = 'prep';
		responseObj.progressJson = "{\"runtime\": 0.1342160701751709, \"progress\": 80.0, \"outputSize\": 0, \"state\": \"running\"}";
		$httpBackend.when('GET',smarfurl+'/scs/task/1/').respond(200,responseObj);
		$interval.flush(1000);
		$httpBackend.flush();
		expect($scope.taskState).toEqual(TASK_STATE_ENUM['prep']);
		expect($scope.percentComplete).toEqual(80.0);
		

		spyOn($modalInstance, 'close');
		responseObj.state = 'running';
		responseObj.progressJson = "{\"runtime\": 0.1342160701751709, \"progress\": 20.0, \"outputSize\": 0, \"state\": \"running\"}";
		$httpBackend.when('GET',smarfurl+'/scs/task/1/').respond(200,responseObj);
		$interval.flush(1000);
		$httpBackend.flush();
		expect($scope.taskState).toEqual(TASK_STATE_ENUM['running']);
		expect($scope.percentComplete).toEqual(100.0);
		expect($modalInstance.close).toHaveBeenCalled();

		spyOn($interval,'cancel');
		spyOn($modal, 'open').andCallThrough();
		cb();
		expect($interval.cancel).toHaveBeenCalled();
		expect($modal.open).toHaveBeenCalled();

		




		
	}));

	
	
  
});

