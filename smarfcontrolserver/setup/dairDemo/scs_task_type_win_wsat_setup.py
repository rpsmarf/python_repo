import json
import os
import sys
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath, scsGetToJson
from scripts.scs_obj_create import scsCreateObjectPath

try:
    os.mkdir("/tmp/src")
except:
    pass
try:
    os.mkdir("/tmp/dest")
except:
    pass

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

root_path = scsGetObjectPath("user", "username=rpsmarf")
user_a = scsGetObjectPath("user", "username=a")
user_b = scsGetObjectPath("user", "username=b")

carleton_container_path = scsGetObjectPath("container", "name_key=carleton_demos_data") 
carleton_spplash_runner = scsGetObjectPath("container", "name_key=carleton_spplash_runner") 
rt_cloud_files_data_path = scsGetObjectPath("resource_type", "name_key=generic_storage")

TASK_TYPE_SPPLASH_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", 
                                      "name": "Argument", 
                                      "value": "/cygdrive/c/Users/splaash/Desktop/wsat-rpsmarf.bat"}
                                     ],
                            "computeResourceRole": "w2compute",
                            "interactive": True
                       }
tt_path = scsCreateObjectPath("task_type", {"name": "Run WSAT on Windows",
                                            "name_key": "win_wsat",
                                            "description": " This tool runs the WSAT application on a Windows computer ", 
                                            "code_module": "smcommon.task_runners.task_runner_shell", 
                                            "code_classname": "SmTaskRunnerShell", 
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_SPPLASH_CFG_DICT)})
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

rt_wsat_compute_path = scsCreateObjectPath("resource_type", 
                                           {"name": "WSAT Tool Runner", 
                                            "nature": "compute",
                                            "description": "This is a computing resource which can run the WSAT tool"})

scsCreateObjectPath("resource_type_task_type", {"role": "w2compute", "task_type": tt_path, 
                                                "resource_type": rt_wsat_compute_path,
                                                "description": "This is the compute resource for the WSAT tool",
                                                "nature": "compute",
                                                "name": "WSAT Compute Resource",
                                                "name_key": "wsat_compute"})


res_path = scsCreateObjectPath("resource", {"name": "Windows Task WSAT Running Resource", 
                               "name_key": "maj06_win_wsat_runner",
                               "resource_type": rt_wsat_compute_path,
                               "owner": root_path,
                               "parametersJson": 
                               json.dumps({"taskViewBaseUrl": 
                                           "http://majumdar-10.sce.carleton.ca:9114/guacamole/client.xhtml?id=c%2Fmaj-06",
                                           "keyfile": "dropbear_key",
                                           "user": "splaash",
                                           "tempDataFolder": "/cygdrive/c/temp"
                                           }),
                               "container": carleton_spplash_runner,
                               "description": "This is the Carleton-based WSAT Windows host"
                               }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=x")


print("Task type 'win_wsat' objects setup.")
