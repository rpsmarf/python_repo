angular.module( 'rpsmarf.reservationlist', [
  'rpsmarf.config',
  'restangular',
  'ui.router',
  'ui.bootstrap',
  'ngSanitize',
  'rpsmarf.user.service',
  'rpsmarf.confirm',
  'rpsmarf.task.create.service',
  'rpsmarf.taskoutput',
  'rpsmarf.reservation.service'
  
])
.config(['$stateProvider',function ( $stateProvider, RestangularProvider ) {
  $stateProvider.state( 'reservationlist', {
    url: '/reservations/',
    views: {
      "main": {
        controller: 'ReservationListCtrl',
        templateUrl: 'reservationlist/reservationlist.tpl.html'
      }
    },
    data:{ pageTitle: 'Reservations' }
  })
  ;

}])
/*
  descriptionJson currently has the following structure. Note that descriptionJson has free form format that is specified when the reservation is made. The
  UI creates this structure:
  {resource_name:string, manual: boolean, resource_namekey: string, task_name: string (present only if the reservation is automatic)}
*/
.filter('reservationUpdate', [ 'Restangular', 'TASK_STATE_FINISHED', 'TASK_STATE_RUNNING',
 function(Restangular, TASK_STATE_FINISHED, TASK_STATE_RUNNING){

  var monthNames = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

  var getTaskDuration = function(task){
    var sd = task.start_time;
    var ed = task.end_time;
    var sdm;
    var edm;
    var diff;
    var duration="";
    if(sd[sd.length-1] !== 'Z'){
      sd += 'Z';
    }
    if(ed[ed.length-1] !== 'Z'){
      ed += 'Z';
    }

    sdm = Date.parse(sd);
    edm = Date.parse(ed);

    diff = edm-sdm;
    //Hours
    duration = diff/3600000 > 1 ? Math.floor(diff/3600000) +'h': '';
    //Minutes
    diff = diff%3600000;
    duration += diff/60000 > 1 ? Math.floor(diff/60000) +'m': '';
    //Seconds
    diff = diff%60000;
    duration += diff/1000 > 1 ? Math.floor(diff/1000) :'';
    //milliseconds
    diff = diff%1000;
    duration += diff > 0 ?  diff > 1000 ? '.'+diff.substring(0,3)+'s' : '.'+diff+'s' : 's';

    if(duration === '' || duration === 's'){
      duration = '---';
    }

    return duration;
    
  };

  

  var getReservationStatus = function(reservation){
    var sd = new Date(reservation.start_time);
    var ed = new Date(reservation.end_time);
    var now = new Date();

    if(now.getTime() >= sd.getTime() && now.getTime() <= ed.getTime()){
      if(reservation.manual){
        return 'ready'  ;
      }
      else{
        return 'running';
      }
      
    }
    else if(now.getTime() < sd.getTime()){
      return 'waiting';
    }
    else{
      return 'completed';
    }
  };

  var getTimeNormalized = function(time){
    var date = new Date(time);
    // var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

    // var offset = date.getTimezoneOffset() / 60;
    // var hours = date.getHours();

    // newDate.setHours(hours - offset);

    return monthNames[date.getMonth()] + " " + date.getDate() + (" "+date.getFullYear()).substring(date.getFullYear().length-2) + " " + date.toTimeString().substring(0,5);  
  };

  var getTaskCompletionInfo = function(task){
    var resJson;
    if(task.state === TASK_STATE_FINISHED){
      if(task.completion === 'completedWithError'){
        resJson = JSON.parse(task.resultsJson);
        return 'Task completion failed with error message: '+resJson.result.description;
      }
      else{
        return 'Task completed successfully';
      }
    }
    else{
      return 'Task is currently running';
    }
  };

  return function(reservations){
    var reservation, paramsJson;
    for(var i=reservations.length-1; i>=0; i--){
      reservation = reservations[i];
      
      descriptionJson = JSON.parse(reservation.description);
      reservation.type = descriptionJson.manual ? 'manual' : 'auto';
      reservation.task_name = descriptionJson.task_name ? descriptionJson.task_name : '-----';
      reservation.manual = descriptionJson.manual;
      reservation.resource_name = descriptionJson.resource_name;
      reservation.startTimeNorm = getTimeNormalized(reservation.start_time);
      reservation.endTimeNorm = getTimeNormalized(reservation.end_time);
      reservation.status = getReservationStatus(reservation);
      reservation.descriptionJson = descriptionJson;
      
    }
    
    return reservations;
  };
}])
.controller( 'ReservationListCtrl', ['$scope', 'Restangular', '$modal', 'SmUser','$state', 'TASK_STATE_FINISHED', '$interval', 'SmTaskCreate', 'filterFilter', 'USER_STATE', 'SmReservation',
  function ( $scope, Restangular, $modal, SmUser, $state, TASK_STATE_FINISHED, $interval, SmTaskCreate, filterFilter, USER_STATE, SmReservation) {

  /***VARIABLES***/
  /*Standard*/
  var uid = SmUser.getProperty('id');

  /*Scope*/
  
  $scope.reservations = [];
  $scope.currentPage = 1;
  $scope.totalReservations = null;

  /*Standard*/
  var reservationApi = Restangular.one('reservation/?owner__id='+uid+'&order_by=start_time','');

  /***FUNCTIONS***/
  /*Scope*/
  $scope.taskRowClicked  = function(task){
    var element;
    var rows;
    var i;
    var index;

    var numSelected = $scope.selectedTasks.length;

   
    if(numSelected === 0){
      task.selected = true;
      
      $scope.selectedTasks.push(task);
      $scope.selectedTask = updateSelectedTask(task);
    }
    else if(numSelected ===1 && task.selected){
      task.selected = false;
      
      $scope.selectedTasks.splice(0,1);
      $scope.selectedTask = null;
    }
    else{
      setTasksSelectedAs($scope.selectedTasks, false);
      $scope.selectedTasks.splice(0,$scope.selectedTasks.length);
      task.selected = true;
      $scope.selectedTasks.push(task);
      $scope.selectedTask = updateSelectedTask(task);
    }
    $scope.allTasksSelected = false;
  };

  $scope.taskCheckboxClicked = function(task, $event){
    $event.stopPropagation();
    var index;
    task.selected = !task.selected;
    if(task.selected){
      $scope.selectedTasks.push(task);
      if($scope.selectAllTasks.length === 1){
        $scope.selectedTask = updateSelectedTask(task);
      }
      else{
        $scope.selectedTask = null;
      }
    }
    else{
      index = $scope.selectedTasks.indexOf(task);
      $scope.selectedTasks.splice(index,1);
      $scope.allTasksSelected = false;

      if($scope.selectedTasks.length === 1){
        $scope.selectedTask = updateSelectedTask($scope.selectedTasks[0]);
      }
      else {
        $scope.selectedTask = null;
      }
    }

  };

  $scope.selectAllTasks = function(){

    var val;
    if($scope.allTasksSelected){
      val = false;
      $scope.selectedTasks = [];
      $scope.allTasksSelected = false;
      $scope.selectedTask = null;
    }
    else{
      val = true;
      $scope.selectedTasks = $scope.tasks;
      $scope.allTasksSelected = true;
      $scope.selectedTask = null;
    }
    setTasksSelectedAs($scope.tasks, val);
    //for(i = $scope.tasks.length-1; i>=0; i--){
     //   $scope.tasks[i].selected = val;
    //}
    //$scope.selectedTask = null;
    
  };

  var setTasksSelectedAs = function(tasks,value){

    for(var i=tasks.length-1; i>=0; i--){
      tasks[i].selected = value;
    }
  };
  

  

  $scope.updateTask = function(task){
    var paramsJson = JSON.parse(task.parametersJson);
    task.name = paramsJson.task_type_name;
    task.duration = getTaskDuration(task);
    task.startTimeNorm = getStartTimeNormalized(task.start_time);
    task.status = getTaskStatus(task);
    task.output = getTaskOutput(task);
    task.completionInfo = getTaskCompletionInfo(task);
    task.parametersJson = paramsJson;

  };

  $scope.deleteReservation = function(reservation, index){
    var body = 'Delete reservation? (Cannot be undone)';
    
    var modalInstance = $modal.open({
      controller: 'ConfirmCtrl',
      templateUrl: 'confirm/confirm.tpl.html',
      resolve:{
        title: function(){ return 'Confirm Deletion';},
        body: function(){ return body;}
      }
    });

    modalInstance.result.then(function(){
      Restangular.one('reservation',reservation.id+'/').remove().then(function(){
        $scope.reservations.splice(index,1);
      });
    },
    function(){
      console.log('reservation deletion cancelled');
    });
    
  };

  $scope.cancelReservation = function(reservation, index){
    var body = 'Cancel reservation? (Cannot be undone)';
    
    var modalInstance = $modal.open({
      controller: 'ConfirmCtrl',
      templateUrl: 'confirm/confirm.tpl.html',
      resolve:{
        title: function(){ return 'Confirm Cancellation';},
        body: function(){ return body;}
      }
    });

    modalInstance.result.then(function(){
      var tid = JSON.parse(reservation.description).task_id;
      Restangular.one('reservation',reservation.id+'/').remove().then(function(){
        $scope.reservations.splice(index,1);
      });
      Restangular.one('task',tid+'/').remove().then();
    },
    function(){
      console.log('reservation Cancellation cancelled');
    });
  };

  $scope.rerun = function(){
    var task = $scope.selectedTask;
    if(!task.valParams && !task.resParams){
      task = updateSelectedTask(task);
    }
    var title = 'Rerun task configuration?';
    var body = 'This task was run with the following configuration:\n';
    var modalInstance;
    var valParams = task.valParams;
    var resParams = task.resParams;
    if(valParams && valParams.length >0){
      body += '<table class="table"><thead><tr><th>Parameter Name</th><th>Value</th></tr></thead><tbody>';
      for(i=0;i<valParams.length;i++){
        p = valParams[i];
        body += '<tr><td>'+p.name+'</td><td>'+p.value+'</td></tr>';
      }
      body += '</tbody></table>';
    }
    if(resParams && resParams.length >0){
      body += '<table class="table"><thead><tr><th>Parameter Name</th><th>Resource Name</th><th>Path</th></tr></thead><tbody>';
      for(i=0;i<resParams.length;i++){
        p = resParams[i];
        if(p.nature === 'compute'){
          body += '<tr><td>'+p.name+'</td><td>'+p.resourceName+'</td><td>--</td></tr>';
        }
        else{
          body += '<tr><td>'+p.name+'</td><td>'+p.resourceName+'</td><td>'+p.path+'</td></tr>';
        }
      }
      body += '</tbody></table>';      
    }

    modalInstance = $modal.open({
      controller: 'ConfirmCtrl',
      templateUrl: 'confirm/confirm.tpl.html',
      resolve:{
        title: function(){return title;},
        body: function(){return body;}
      }
    });

    modalInstance.result.then(function(){
      var p = SmTaskCreate.rerunTask(task);
      p.then(function(response){
        $scope.$emit('SmTaskStart',Restangular.one('task',response.taskId+'/'),task.paramsJson.task_type_name, response.interactive);
        //Shouldn't be needed but just in case
        SmTaskCreate.reset();
        taskApi.get().then(function(response){
          $scope.totalTasks = response.meta.total_count;
          $scope.tasks = filterFilter(response.objects,function(val, index){ return val.uiVisible;});

        });
      });
      
    }, function(){
      console.log('rerun was cancelled');
    });

  };

  $scope.reconfigure = function(){
    var task = $scope.selectedTask;
    var modalInstance = $modal.open({
      controller: 'ConfirmCtrl',
      templateUrl: 'confirm/confirm.tpl.html',
      resolve:{
        title: function(){return 'Confirm';},
        body: function(){return 'Do you want to reconfigure this task? This will take you to another page.';}
      }
    });

    modalInstance.result.then(function(){
      var p = SmTaskCreate.reconfigureTask(task);
    

      p.then(function(){
        var tt = SmTaskCreate.getTaskType();
        $state.go('taskstart',{tooltask_namekey:tt.name_key});
      });

    },function(){
      console.log('reconfigured cancelled.');
    });
    
  };

  $scope.pageChanged = function(){
    console.log($scope.currentPage);
    SmInterval.stopAllIntervals();
    Restangular.one('task/?aggregateParam=True&owner__id='+SmUser.getProperty('id')+'&orderByIdDesc=True&limit=20&offset='+(($scope.currentPage-1)*20),'').get().then(function(response){
      $scope.totalTasks = response.meta.total_count;
      $scope.tasks = filterFilter(response.objects,function(val, index){ return val.uiVisible;});

    });
  };

  $scope.viewTaskOutput = function(){
    $modal.open({
      controller: 'TaskOutputCtrl',
      templateUrl: 'taskoutput/taskoutput.tpl.html',
      size: 'lg',
      resolve: {
        task: function(){ return $scope.selectedTask; }
      }
    });
  };

  $scope.selectTaskForReservation = function(reservation){
    SmReservation.setReservation(reservation);
    $state.go('tools.reservation', {compute_resource: reservation.descriptionJson.resource_namekey});
  };

  

  /*STANDARD VARIABLES*/

  var updateSelectedTask = function(task){
    if(!task.paramsJson){
      task.paramsJson = JSON.parse(task.parametersJson);
    }
    var valParams = task.paramsJson.parameters;
    var resParams = task.paramsJson.resources;
    var inputIds = task.paramsJson.input_ids;
    var vp, rp, p, i, resJson, key, val;

    if(valParams && valParams.length >0){
      vp = [];
      for(i=0;i<valParams.length;i++){
        p = valParams[i];
        vp.push(p);
      }

      task.valParams = vp;
    }
    if(resParams && resParams.length >0){
      rp = [];
      for(i=0;i<resParams.length;i++){
        p = resParams[i];
        if(!p.name || p.name === '' && p.nature === 'compute'){
          p.name = 'Compute';
          p.resourcePathEntry = '--';
        }
        else if(p.path){
          p.resourcePathEntry = p.path;
        }
        rp.push(p);
      }

      task.resParams = rp;
    }

    if(task.resultsJson){
      resJson = JSON.parse(task.resultsJson);
      if(resJson.result){
        task.hasErrors = true;
        task.errorMessage = 'An error occurred during task execution: '+ resJson.result.description;
      }
      if(resJson.alteredPaths){
        task.alteredPaths = [];
        for(key in resJson.alteredPaths){
          val = resJson.alteredPaths[key];
          task.alteredPaths.push({original: key, 'new': val});
        }

        if(task.alteredPaths.length > 0){
          task.hasAlteredPaths = true;
        }
        
      }
    }

    return task;

  };

  

  /***EXE***/
  reservationApi.get().then(function(response){
    $scope.totalReservations = response.meta.total_count;
    $scope.reservations = response.objects;

  });
  
}]);



