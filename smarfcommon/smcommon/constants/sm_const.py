'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''


SM_DATA_CONTAINER_PROTOCOL_LOCAL = "local"
SM_DATA_CONTAINER_PROTOCOL_SFTP = "sftp"
SM_DATA_CONTAINER_PROTOCOL_SSH = "ssh"
SM_DATA_CONTAINER_PROTOCOL_ICE = "ice"
SM_DATA_CONTAINER_PROTOCOL_SWIFT = "swift"
SM_DATA_CONTAINER_PROTOCOL_S3 = "s3"

SM_TEST_AGENT_ID = "testagentid"
SM_MY_AGENT_ID = "testagentid"
SM_COPY_FAKE_ICE_PREFIX = None


# Special names within the data model
SM_COMMUNITY_ALL_NAME_KEY = "all_resources"
SM_RESOURCE_TYPE_USER_STORAGE_NAME_KEY = "generic_storage"
SM_CONTAINER_USER_STORAGE_NAME_KEY = "user_storage"
SM_TASK_TYPE_DELETE_RECURSIVE_NAME_KEY = "delete_recusive"


def smConstSetMyAgentId(agentId):
    global SM_MY_AGENT_ID
    SM_MY_AGENT_ID = agentId


def smConstGetMyAgentId():
    global SM_MY_AGENT_ID
    return SM_MY_AGENT_ID