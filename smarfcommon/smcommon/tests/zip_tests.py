'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 15, 2015

    @author: rpsmarf
'''

import unittest
from smcommon.logging.sm_logger import smLoggingSetupForTest
from smcommon.tests.dad_test_utils import TempFolder
import zipstream
from zipfile import ZipFile
from os.path import os
from smcommon.constants import sm_const
from smcommon.file_access.data_access_desc import SmDataContainer
from smcommon.globals.sm_copy_controller import SmCopyController
from smcommon.globals.sm_global_context import smGlobalContextInit
from smcommon.file_access.file_accessor_local import SmFileAccessorLocal
from smcommon.file_access.file_access_iter import SmFileAccessorIterator,\
    SmZipStreamWrapper


class TestZip(unittest.TestCase):
    '''
    We use the zipstream add-on package so we can add data from
    file-like objects, not just files
    '''

    def writeZipFile(self, zip_file_path, zip_file):
        with open(zip_file_path, 'wb') as f:
            for chunk in zip_file:
                f.write(chunk)
        
        zip_file.close()

    def checkZipFile(self, zip_file_path, dest_dir, content_list):
        # Unzip the file into the temp folder
        zfile = ZipFile(zip_file_path)
        zfile.extractall(path=self.tempFolder.makeAbsolutePath(dest_dir, "local"))
        zfile.close()
   
        # Check the contents
        for name, data in content_list:
            self.assertEqual(self.tempFolder.readFile(os.path.join(dest_dir, name), 10000), data)

    def testZipUnZip(self):
        smLoggingSetupForTest()
        self.tempFolder = TempFolder()
        self.tempFolder.mkdir("dir1")
       
        zip_file_path = self.tempFolder.makeAbsolutePath("x.zip", "local")
        test_file_path = self.tempFolder.makeAbsolutePath("f1.txt", "local")
        self.tempFolder.writeFileBinary("f1.txt", b"1234")
        
        # Make a zip file with two files and folder with a file
        zip_file = zipstream.ZipFile(mode='w', compression=zipstream.ZIP_DEFLATED)
        zip_file.write(test_file_path, "f1.txt")
        zip_file.write(test_file_path, "f1b.txt")

        self.writeZipFile(zip_file_path, zip_file)
        
        self.checkZipFile(zip_file_path, "dir1", 
                          [("f1.txt", b"1234"), 
                           ("f1b.txt", b"1234")])
        
    def testZipUnZipViaFileAccessor(self):
        
        smLoggingSetupForTest()
        self.tempFolder = TempFolder()
        self.tempFolder.mkdir("dir1")
       
        localDataContainer = SmDataContainer(sm_const.SM_MY_AGENT_ID, "", 
                                             sm_const.SM_DATA_CONTAINER_PROTOCOL_LOCAL + "://localhost/" + 
                                             self.tempFolder.folder)
        copyController = SmCopyController([localDataContainer.agentId])
        smGlobalContextInit(copyController)

        zip_file_path = self.tempFolder.makeAbsolutePath("x.zip", "local")
        self.tempFolder.writeFileBinary("f1.txt", b"1234")
        self.tempFolder.writeFileBinary("f2.txt", b"abcd")
        
        # Make a zip file with two files and folder with a file
        zip_file = zipstream.ZipFile(mode='w', compression=zipstream.ZIP_DEFLATED)

        fileAccessor = SmFileAccessorLocal(localDataContainer)
        f1_iter = SmFileAccessorIterator("f1.txt", fileAccessor, False)
        zip_file.write_iter("f1.txt", f1_iter)
        
        fileAccessor = SmFileAccessorLocal(localDataContainer)
        f2_iter = SmFileAccessorIterator("f2.txt", fileAccessor, False)
        zip_file.write_iter("f2.txt", f2_iter)

        self.writeZipFile(zip_file_path, zip_file)
        
        self.checkZipFile(zip_file_path, "dir1", 
                          [("f1.txt", b"1234"), 
                           ("f2.txt", b"abcd")])
        
    def testZipStreamWithWrapper(self):
        
        smLoggingSetupForTest()
        self.tempFolder = TempFolder()
        self.tempFolder.mkdir("dir1")
       
        localDataContainer = SmDataContainer(sm_const.SM_MY_AGENT_ID, "", 
                                             sm_const.SM_DATA_CONTAINER_PROTOCOL_LOCAL + "://localhost/" + 
                                             self.tempFolder.folder)
        copyController = SmCopyController([localDataContainer.agentId])
        smGlobalContextInit(copyController)

        zip_file_path = self.tempFolder.makeAbsolutePath("x.zip", "local")
        self.tempFolder.writeFileBinary("f1.txt", b"1234")
        self.tempFolder.writeFileBinary("f2.txt", b"abcd")
        
        # Make a zip file with two files and folder with a file
        fileAccessor = SmFileAccessorLocal(localDataContainer)
        fileAccessor.connectSession()
        zip_file = SmZipStreamWrapper(fileAccessor, mode='w', compression=zipstream.ZIP_DEFLATED)

        f1_iter = SmFileAccessorIterator("f1.txt", fileAccessor, False)
        zip_file.write_iter("f1.txt", f1_iter)
        
        f2_iter = SmFileAccessorIterator("f2.txt", fileAccessor, False)
        zip_file.write_iter("f2.txt", f2_iter)

        self.writeZipFile(zip_file_path, zip_file)
        
        self.checkZipFile(zip_file_path, "dir1", 
                          [("f1.txt", b"1234"), 
                           ("f2.txt", b"abcd")])
        self.assertFalse(fileAccessor.connected)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testZipUnZip']
    unittest.main()