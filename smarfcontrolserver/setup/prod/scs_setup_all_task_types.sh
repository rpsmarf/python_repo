#!/bin/bash

echo "SCS_URL is $SCS_URL (should contain https)"
echo "SMARF_CERT_CHECK is $SMARF_CERT_CHECK (should be OFF)"
echo "SMARF_AUTH_HDR is $SMARF_AUTH_HDR (should be 'ApiKey rpsmarf:<value from tastypie_apikey DB table>)'"

python scs_agent_container_setup.py

for i in scs_task_type*; do
    python $i
done