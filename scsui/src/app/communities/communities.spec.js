describe( 'Community Selection page (/communities)', function() {
	beforeEach (module('rpsmarf.config'));
	
	beforeEach(module('rpsmarf.communities'));
	beforeEach(module('ui.router'));
	beforeEach(module('restangular'));
	beforeEach(module('rpsmarf.task.create.service'));
	beforeEach(module('rpsmarf.user.service'));
	
	beforeEach(module('taskprogress/taskprogress.tpl.html'));
	//$scope, $modalInstance, taskObj, $interval,TASK_STATE_ENUM, modalTitle, interactive, $modal, TASK_STATE_FINISHED, TASK_STATE_RUNNING
	var $scope, $controller, SmTaskCreate, $state, $interval, $modalInstance, $modal, smarfurl, taskObject, $httpBackend, responseObj, prefix = 'src/app/communities/';
	var SmUser, user, $event = {stopPropagation: function(){}};
	var empty = {objects:[]};
	//init, starting, prep, running, cleanup and finished
	

	beforeEach(  inject(function(_$controller_, $rootScope, _$interval_, Restangular, SMARF_URL, _$modal_, _SmTaskCreate_, _$state_, _$httpBackend_, _SmUser_, DEBUG){
		smarfurl = SMARF_URL+'/scs/';
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
		SmUser = _SmUser_;
		user = SmUser.getObject();
		
		user.id = 4;
		user.community = {id:2};
		user.community.name_key = 'bearings';
		
		SmTaskCreate = _SmTaskCreate_;
		$interval = _$interval_;
		$httpBackend = _$httpBackend_;
		$scope = $rootScope.$new();
		$state = _$state_;

		responseObj = JSON.parse(__html__[prefix+'communities.spec.json']);
		$httpBackend.when('GET',smarfurl+"community/").respond(200,responseObj);
		$httpBackend.when('GET',smarfurl+'news_item/?community=1&limit=1&order_by=-id').respond(200,empty);
		$httpBackend.when('GET',smarfurl+'news_item/?community=2&limit=1&order_by=-id').respond(200,responseObj.news_item);
		$httpBackend.when('GET',smarfurl+'news_item/?community=3&limit=1&order_by=-id').respond(200,empty);
		$httpBackend.when('GET',smarfurl+'user_setting/1/').respond(200,responseObj.owner_settings);
		$httpBackend.when('GET',smarfurl+'user/1/').respond(200,responseObj.user);
		$httpBackend.when('GET',smarfurl+'task_type/?communityFilter=all_resources&uiVisible=true&limit=1').respond({meta: {total_count:45}});
		$httpBackend.when('GET',smarfurl+'task_type/?communityFilter=bearings&uiVisible=true&limit=1').respond({meta: {total_count:7}});
		$httpBackend.when('GET',smarfurl+'task_type/?communityFilter=bridge_mon&uiVisible=true&limit=1').respond({meta: {total_count:5}});

		$httpBackend.when('GET',smarfurl+'resource/?communityFilter=all_resources&limit=1').respond({meta: {total_count:200}});
		$httpBackend.when('GET',smarfurl+'resource/?communityFilter=bearings&limit=1').respond({meta: {total_count:14}});
		$httpBackend.when('GET',smarfurl+'resource/?communityFilter=bridge_mon&limit=1').respond({meta: {total_count:6}});

		$httpBackend.when('GET',smarfurl+'user_setting/?community__id=2&limit=1').respond({meta: {total_count:21}});
		$httpBackend.when('GET',smarfurl+'user_setting/?community__id=1&limit=1').respond({meta: {total_count:21}});
		$httpBackend.when('GET',smarfurl+'user_setting/?community__id=3&limit=1').respond({meta: {total_count:21}});
/*
var communityresources = Restangular.one('resource/?communityFilter=' + $obj.name_key +'&limit=1');
var communitytasktypes = Restangular.one('task_type/?communityFilter=' + $obj.name_key + '&uiVisible=true&limit=1');
var communityusers = Restangular.one('user_setting/?community__id=' + $obj.id + '&limit=1');
*/
		$modal = _$modal_;
		$controller = _$controller_;
		$controller('CommunitiesCtrl', {$scope: $scope, Restangular:Restangular,$state:$state , SmTaskCreate:SmTaskCreate, SmUser:SmUser, $modal: $modal });
		$httpBackend.flush();

	})) ;

	it('should create the community objects', inject(function(){
		expect($scope.listcommunities.length).toBe(3);
		expect($scope.listcommunities[0].name_key).toBe('bearings');
		expect($scope.listcommunities[0].ownerfirstname).toBe('Henry');
		expect($scope.listcommunities[0].lastnewsitem).toBe('System Upgrade on June 30, 2015');
		expect($scope.listcommunities[0].toolcount).toBe(7);
		expect($scope.listcommunities[0].resourcecount).toBe(14);
		expect($scope.listcommunities[0].memberscount).toBe(21);
	}));

	describe('create community module', function(){
		var $createCommunityController, $createCommunityScope;
		beforeEach(inject(function($controller, $rootScope){
			$modalInstance = {close: function(obj){}};
			$createCommunityScope = $rootScope.$new();
			$createCommunityController = $controller('CreateCommunityCtrl', {$scope: $createCommunityScope, $modalInstance: $modalInstance});
		}));

		it('should create community', inject(function(){
			var postResponse = {
									"description": "This community contains all resources associated with bridge monitoring.",
									"gid": 17,
									"id": 3,
									"imageUrl": "https://www.rpsmarf.ca/icons-scs/defaultCommunityIcon.png",
									"name": "Bridge Monitoring",
									"name_key": "bridge_mon",
									"owner": "/scs/user/1/",
									"resource_uri": "/scs/community/3/",
									"shortDescription": "",
									"tag": "/scs/tag/2/"
								};
			var formObj = {$valid: false};
			
			spyOn($state, 'go');
			spyOn($modalInstance, 'close');
			$createCommunityScope.submitForm(formObj);
			
			expect(formObj.$submitted).toBe(true);


			formObj.$valid = true;
			formObj.$submitted = false;
			var postObj = {name: 'test community', imageUrl: 'http://www.someurl.com', description: 'this is a test community', shortDescription: 'test'};
			$createCommunityScope.community = angular.copy(postObj);
			postObj.owner = '/scs/user/4/';
			$httpBackend.expect('POST', smarfurl+'community/',postObj).respond(200, postResponse);
			$createCommunityScope.submitForm(formObj);
			$httpBackend.flush();
			expect($createCommunityScope.complete).toBe(true);
			
			$createCommunityScope.close();
			expect($modalInstance.close).toHaveBeenCalledWith({complete: true, object: postResponse , update: true});

			$createCommunityScope.goToCommunity();
			expect($modalInstance.close).toHaveBeenCalledWith({complete: true, update: false});
			expect($state.go).toHaveBeenCalledWith('communityinfo', {communityNameKey: 'bridge_mon'});


		}));
	});

});

