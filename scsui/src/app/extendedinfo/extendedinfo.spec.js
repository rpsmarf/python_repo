describe('extendedinfo module', function(){

	var $scope, Restangular, tagsObject, NoteObject, smarfurl, $httpBackend;
	beforeEach(module('rpsmarf.config'));
	beforeEach(module('rpsmarf.extendedinfo'));
	beforeEach(module('restangular'));

	beforeEach(inject(function($rootScope, $controller, _Restangular_, SMARF_URL, _$httpBackend_){
		$scope = $rootScope.$new();
		$httpBackend = _$httpBackend_;
		smarfurl = SMARF_URL+'/scs/';
		Restangular = _Restangular_;
		Restangular.configuration.baseUrl = smarfurl;
		Restangular.configuration.encodeIds = false;
		tagsObject = {tags: ['tag1','tag2', 'tag3', 'tag4']};
		noteObject1 = {typeName:'task_type', exists: false, typeId: 45, ownerId: 7, typeObject: {}};
		noteObject2 = {noteText:'Hello World!', ownerId: 1, typeName: 'resource', typeId: 4, exists: true, typeObject: {ext_note_id: 67}};


	}));

	it('should set tags and post a note if no note object exists', inject(['$controller', function($controller){
		$controller('ExtendedInfoCtrl', {$scope: $scope, Restangular: Restangular, tagsObject: tagsObject, noteObject: noteObject1, $modalInstance: null});
		expect($scope.hasTags).toBe(true);
		expect($scope.title).toBe('Details');
		expect($scope.noteText).toBe('');
		
		$scope.noteText = 'World Hello!';
		$httpBackend.expect('POST', smarfurl+'task_type_note/',{note:'World Hello!', owner: '/scs/user/7/', task_type:'/scs/task_type/45/'}).respond(200, {headers: function(v){ return '/scs/task_type_note/1/';}},{Location:'/scs/task_type_note/1/'});
		$scope.modifyNote();

		$httpBackend.flush();

		
		expect(noteObject1.exists).toBe(true);
		expect(noteObject1.typeObject.ext_note).toBe('World Hello!');
		expect(noteObject1.typeObject.ext_note_id).toBe(1);

	}]));

	it('should set tags and patch a note if note object exists', inject(['$controller', function($controller){
		$controller('ExtendedInfoCtrl', {$scope: $scope, Restangular: Restangular, tagsObject: tagsObject, noteObject: noteObject2, $modalInstance: null});
		expect($scope.hasTags).toBe(true);
		expect($scope.title).toBe('Details');
		expect($scope.noteText).toBe('Hello World!');
		
		$scope.noteText = 'patch test text';
		$httpBackend.expect('PATCH', smarfurl+'resource_note/67/',{note:'patch test text'}).respond(200);
		$scope.modifyNote();

		$httpBackend.flush();
		expect(noteObject2.typeObject.ext_note).toBe('patch test text');
	}]));

});