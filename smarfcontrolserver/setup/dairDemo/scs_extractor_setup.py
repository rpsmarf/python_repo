# Setting up metadata...
# Add a schema
from scripts.scs_obj_create import scsCreateObjectPath
from scripts.scs_obj_get import scsGetObjectPath, scsGetToJson
import json

print("Setting up extractor...")

root_path = scsGetObjectPath("user", "username=rpsmarf")
user_a = scsGetObjectPath("user", "username=a")
user_b = scsGetObjectPath("user", "username=b")
community_path = scsGetObjectPath("community", "name_key=all_resources")
rt_weather_data_path = scsCreateObjectPath("resource_type", {"name": "Weather data type", 
                                                     "nature": "data",
                                                     "description": "This is a source of weather data"})
rt_weather_compute_path = scsCreateObjectPath("resource_type", 
                                           {"name": "Weather compute type", 
                                            "nature": "compute",
                                            "description": 
                                            "Resource on which to extract weather data"})

ab_container_path = scsGetObjectPath("container", "name_key=testab1_demos_data")
carleton_container_path = scsGetObjectPath("container", "name_key=carleton_demos_data")

res_list = []
# Create the 3 data resources
res_path = scsCreateObjectPath("resource", {"name": "Weather Data Repo 1",
                                            "resource_type": rt_weather_data_path,
                                            "owner": root_path,
                                            "container": ab_container_path,
                                            "parametersJson": json.dumps({"folder": "extractor_dataset1/"}),
                                            "description": "This is a dataset with metadata to extract."
                                            }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=rw")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=r")
res_list.append(res_path)
    
res_path = scsCreateObjectPath("resource", {"name": "Weather Data Repo 2",
                                            "resource_type": rt_weather_data_path,
                                            "owner": root_path,
                                            "container": carleton_container_path,
                                            "parametersJson": json.dumps({"folder": "extractor_dataset2/"}),
                                            "description": "This is a dataset with metadata to extract."
                                            }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=rw")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=r")
res_list.append(res_path)
    
res_path = scsCreateObjectPath("resource", {"name": "Weather Data Repo 3",
                                            "resource_type": rt_weather_data_path,
                                            "owner": root_path,
                                            "container": ab_container_path,
                                            "parametersJson": json.dumps({"folder": "extractor_dataset3/"}),
                                            "description": "This is a dataset with metadata to extract."
                                            }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=rw")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=r")
res_list.append(res_path)
    
# Make the compute resource
res_path = scsCreateObjectPath("resource", {"name": "Weather Data Extract Resource",
                                            "resource_type": rt_weather_compute_path,
                                            "owner": root_path,
                                            "container": ab_container_path,
                                            "parametersJson": json.dumps({}),
                                            "description": "This is a compute resource to extract metadata."
                                            }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=x")
compute_res_path = res_path

schema_path = scsCreateObjectPath("md_schema", {"name": "Temperature and Wind Direction Schema",
                                                "description": "This is a simple schema with name," +
                                                " temperature and wind direction",
                                                "owner": root_path,
                                                "resource_type": rt_weather_data_path,
                                                "community": community_path
                                                })


# Add property types
ptype_wind_path = scsCreateObjectPath("md_property_type", {"name": "Wind Direction",
                                                           "name_key": "winddir",
                                                           "property_type": "STRING",
                                                           "schema": schema_path
                                                           })
ptype_name_path = scsCreateObjectPath("md_property_type", {"name": "File name",
                                                           "name_key": "name",
                                                           "property_type": "STRING",
                                                           "schema": schema_path
                                                           })
ptype_temp_path = scsCreateObjectPath("md_property_type", {"name": "Temperature",
                                                           "property_type": "FLOAT",
                                                           "schema": schema_path
                                                           })

# Add a repo
repo_path = scsCreateObjectPath("md_repo", 
                                {"name": "Weather Data Repo",
                                 "name_key": "weather_repo",
                                 "description": "This is the first example metadata repository",
                                 "owner": root_path,
                                 "resources": res_list,
                                 "community": community_path,
                                 "schema": schema_path
                                 })

# Add task type for extraction
TASK_TYPE_EXTRACTOR_CFG_DICT = {
                                "args": [ 
                                         {"type": "string", 
                                          "name": "Path", 
                                          "value": "/opt/smarf-demos/demos/extractor/tools/sm_extraction_process_files.sh"},
                                         {"type": "string", 
                                          "name": "URL", 
                                          "input_id": 2},
                                         {"type": "string", 
                                          "name": "prefix", 
                                          "input_id": 3},
                                         {"type": "resource", 
                                          "name": "File of control parameters", 
                                          "pathType": "file", 
                                          "input_id": 1, 
                                          "direction": "input", 
                                          "role": "src", 
                                          "name": "Data to analyze",
                                          "description": "This specifies the folder of data to submit for analysis"},
                                     ],
                                "computeResourceRole": "compute",
                                "workingDir": {"type": "string", "value": "/opt/smarf-demos/demos/data/"}
                                }
tt_path = scsCreateObjectPath("task_type", {"name": "Weather Extraction",
                                            "name_key": "weather_extraction",
                                            "uiVisible": False,
                                            "description": "Extraction: Extracts metadata", 
                                            "code_module": "smcommon.task_runners.task_runner_shell", 
                                            "code_classname": "SmTaskRunnerShell", 
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_EXTRACTOR_CFG_DICT)})
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

scsCreateObjectPath("resource_type_task_type", {"role": "w2compute", "task_type": tt_path, 
                                                "resource_type": rt_weather_data_path,
                                                "description": "This is the link to data for this task type",
                                                "nature": "data",
                                                "name": "Weather Data",
                                                "name_key": "weather_data"})

scsCreateObjectPath("resource_type_task_type", {"role": "compute", "task_type": tt_path, 
                                                "resource_type": rt_weather_compute_path,
                                                "description": "This is the link to compute for this task type",
                                                "nature": "compute",
                                                "name": "Weather Compute",
                                                "name_key": "weather_compute"})

# Add extractor type
et_path = scsCreateObjectPath("md_extractor_type", {"name": "Weather Extractor Type",
                                                 "name_key": "weather_extractor",
                                                 "description": "Extractor type for weather data", 
                                                 "schema": schema_path, 
                                                 "resource_type": rt_weather_data_path,
                                                 "task_type": tt_path})

# Add extractor
e_path = scsCreateObjectPath("md_extractor", {"repo": repo_path,
                                              "extractor_type": et_path,
                                              "compute_resource": compute_res_path})
print("Repos setup.")
