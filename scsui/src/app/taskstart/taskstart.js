angular.module( 'rpsmarf.taskstart', [
  'rpsmarf.config',
  'rpsmarf.resourceconfig',
  'restangular',
  'ui.router',
  'rpsmarf.foldernav',
  'rpsmarf.foldernavpane',
  'ui.bootstrap',
  'rpsmarf.task.create.service',
  'rpsmarf.alert',
  'rpsmarf.user.service',
  'rpsmarf.helpers.service',
  'rpsmarf.reservation.service'
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider, RestangularProvider, SMARF_URL ) {
  $stateProvider.state( 'taskstart', {
    url: '/tool/:tooltask_namekey/',
    views: {
      "main": {
        controller: 'TaskStartCtrl',
        templateUrl: 'taskstart/taskstart.tpl.html'
      }
    },
    data:{ pageTitle: 'Task configuration' }
  }).state('taskstart.reservation', {
    url: 'reservation/',
    views: {
      "main": {
        controller: 'TaskStartCtrl',
        templateUrl: 'taskstart/taskstart.tpl.html'
      }
    },
    data:{ pageTitle: 'Reserve a resource for a task' }
  });
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'TaskStartCtrl', ['$scope', 'Restangular', '$modal','$state', 'SmTaskCreate', 'SmUser', '$interval', 'filterFilter', 'SmHelpers', 'SmReservation',
  function ( $scope, Restangular, $modal, $state, SmTaskCreate, SmUser, $interval, filterFilter, SmHelpers, SmReservation) {

    /***VARIABLES***/
    /*Standard*/
    var uid = SmUser.getProperty('id');
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var task_type = SmTaskCreate.getTaskType();
    if(!task_type){
      $state.go('tools');
      return;
    }
    var inputs = SmTaskCreate.getInputs();
    var date = new Date();
    var startdate = new Date().getTime();
    var minDate = new Date();
    var restartInterval;
    date.setHours(date.getHours()+1);
    date.setMinutes(0);

    /*Scope*/
    $scope.$state = $state;
    $scope.tool = task_type;
    $scope.valParams = inputs.valueParameters;
    $scope.resParams = inputs.resourceParameters;
    $scope.requiresComputeResource = SmTaskCreate.getRequiresComputeResource();
    $scope.compParam = $scope.requiresComputeResource ? inputs.computeResource: null;
    $scope.modalInstance = null;
    $scope.taskIsInteractive = SmTaskCreate.getTaskInteractive();
    $scope.reservation = $state.is('taskstart.reservation') && SmReservation.areReservationParamsSaved() ? SmReservation.getReservationParameters() : {manual: $scope.taskIsInteractive ? true : false, starttime:date,startdate: startdate, duration: 1, popOpened: false, minDate:minDate,alert: false, alertType: 'success', validated: false, alertMsg: '', numCompute: 1};
    $scope.computeResources = null;
    $scope.tasknameUser = task_type.name + '_' + months[date.getMonth()].substring(0,3)+'_'+date.getDate()+'_'+date.getFullYear();
    $scope.taskname = task_type.name;
    $scope.versions = null;
    $scope.selectedVersion = {value: null};
    $scope.versionSet = {value:null};

    /***FUNCTIONS***/
    /*Scope*/
    $scope.configureResource = function(resourceParam){
      SmTaskCreate.setActiveResourceParameterId(resourceParam.input_id);
      if($state.is('taskstart.reservation')){
        SmReservation.saveReservationParameters($scope.reservation);
      }
      
      var resourceInputName = resourceParam.name.toLowerCase().replace(" ","_");
      $state.go('resourceconfig.taskstart',{resourceInputName: resourceInputName});
    };

    $scope.cancel = function(){
      $state.go('home');
    };

    $scope.back = function(){
      $state.go('tools');
    };

    $scope.start = function(){
      var paramsValid = false;
      SmTaskCreate.setTaskName($scope.tasknameUser);
      if($state.is('taskstart.reservation')){

        if(!$scope.reservation.manual){
          paramsValid = validateParameters();

          if(!paramsValid){
            return;
          }
        }
        var starthour = $scope.reservation.starttime.getHours();
        var duration = $scope.reservation.duration;

        if(duration < 0){
          $modal.open({
            controller: 'AlertCtrl',
            templateUrl: 'alert/alert.tpl.html',
            resolve: {
              title: function(){return 'Error';},
              message: function(){return 'Duration must be a positive number';}
            }
          });
          return;
        }
        var meridian = 'AM';
        if(starthour === 0){
          starthour = 12;
          meridian = 'AM';
        }
        else if(starthour === 12){
          meridian = 'PM';
        }
        else if(starthour > 12){
          meridian = 'PM';
          starthour -= 12; 
        }
        var startminutes = ($scope.reservation.starttime.getMinutes() > 9) ? $scope.reservation.starttime.getMinutes() : '0'+$scope.reservation.starttime.getMinutes();
        var starttime = starthour +':' +startminutes + ' ' +meridian;
        var startdateObject = angular.isDate($scope.reservation.startdate) ? $scope.reservation.startdate : new Date($scope.reservation.startdate);
        var startdate = months[startdateObject.getMonth()] + ' ' +startdateObject.getDate()+ ' ' + startdateObject.getFullYear();
        var confirmText = 'Create a reservation for '+ $scope.compParam.resource.name + ' starting at ' + starttime + ' on ' + startdate;
       
        confirmText += ' lasting '+duration + ' hr(s)';
        

        confirmText += '?';
        $scope.modalInstance = $modal.open({
          controller: 'ConfirmCtrl',
          templateUrl: 'confirm/confirm.tpl.html',
          resolve:{
            title: function(){return 'Confirm Reservation';},
            body: function(){return confirmText;}
          }
        });

        $scope.modalInstance.result.then(function(){
          var sd = startdateObject;
          var st = $scope.reservation.starttime;
          var startIso = sd.toISOString().substring(0,11)+st.toISOString().substring(11,17)+'00Z';
          var endIso;
          var descriptionJson = {resource_name:$scope.compParam.resource.name, manual: $scope.reservation.manual, resource_namekey: $scope.compParam.resource.name_key};
          if(!$scope.reservation.manual || ($scope.reservation.manual && $scope.taskIsInteractive)){
            descriptionJson['task_name'] = task_type.name;

          }
          descriptionJson = JSON.stringify(descriptionJson);
          var postObj = {owner: '/scs/user/'+uid+'/', resource: '/scs/resource/'+$scope.compParam.resource.id+'/', start_time:startIso, description:descriptionJson};
          sd.setHours(st.getHours());
          sd.setMinutes(st.getMinutes());
          endIso = new Date(sd.getTime()+(duration*3600000)).toISOString();
          endIso = endIso.substring(0,17)+'00Z';
          postObj['end_time'] = endIso;
    
          Restangular.configuration.fullResponse = true;
          Restangular.all('reservation/').post(postObj,null,{"Content-Type":"application/json"}).then(function(response){
            var reservationId,m;
            
            if(!$scope.reservation.manual){
              reservationId = getLocation(response.headers('Location')).pathname.split('/')[3];
              //reservationId = reservationId;
              Restangular.configuration.fullResponse = false;
              SmTaskCreate.createAndStartTask(startIso, endIso).then(function(taskInfo){
                descriptionJson = JSON.parse(descriptionJson);
                descriptionJson['task_id'] = parseInt(taskInfo.taskId,10);
                Restangular.one('reservation',reservationId+'/').patch({description:JSON.stringify(descriptionJson)},null,{"Content-Type":"application/json"});
                m = $modal.open({
                  controller: 'AlertCtrl',
                  templateUrl: 'alert/alert.tpl.html',
                  resolve: {
                    title: function(){return 'Success';},
                    message: function(){return 'Your reservation was successfully created';}
                  }
                });

                m.result.then(function(){
                  $state.go('home');
                });
              },function(error){
                SmHelpers.handleErrorOnTaskStart(error, {toolName:task_type.name, taskName:$scope.tasknameUser,interactive:SmTaskCreate.getTaskInteractive()},$scope);
              });
                
            }
            else{
              Restangular.configuration.fullResponse = false;
              m = $modal.open({
                controller: 'AlertCtrl',
                templateUrl: 'alert/alert.tpl.html',
                resolve: {
                  title: function(){return 'Success';},
                  message: function(){return 'Your reservation was successfully created';}
                }
              });

              m.result.then(function(){
                $state.go('home');
              });
            }
            
            
          },function(response){
            Restangular.configuration.fullResponse = false;
            $modal.open({
              controller: 'AlertCtrl',
              templateUrl: 'alert/alert.tpl.html',
              resolve: {
                title: function(){return 'Reservation Error';},
                message: function(){return 'Could not create reservation due to:<br>'+response.data.message;}
              }
            });
          });
        },function(){
          console.log('reservation cancelled :)');
        });
      }
      else {
        if(validateParameters()){

          $scope.modalInstance = $modal.open({
            controller: 'ConfirmCtrl',
            templateUrl: 'confirm/confirm.tpl.html',
            resolve:{
              title: function(){return 'Confirm';},
              body: function(){return 'Start '+ task_type.name + '?';}
            }
          });

          $scope.modalInstance.result.then(function(closedResult){
            console.log('closing modal!');
            
            var t = SmTaskCreate.createAndStartTask();
            t.then(function(){
              var taskId = SmTaskCreate.getTaskObjectId();
              var interactive = SmTaskCreate.getTaskInteractive();
              
              $scope.$emit('SmTaskStart',Restangular.one('task',taskId+'/'),$scope.tasknameUser, interactive);
              SmTaskCreate.reset();
              $state.go('home');
            },function(error){
              SmHelpers.handleErrorOnTaskStart(error, {toolName:task_type.name, taskName:$scope.tasknameUser,interactive:SmTaskCreate.getTaskInteractive()},$scope);
            });

            
          },function(dismissResult){
            console.log('Task start cancelled.');
          });
        }
      }
      

      

    };

    $scope.startBtnText = function(){
      if($state.is('taskstart.reservation')){
        return 'Create reservation';
      }
      else{
        return 'Run Task';
      }
    };

    $scope.openCalendar = function($event){
      $event.preventDefault();
      $event.stopPropagation();
      $scope.reservation.popOpened = true;
    };

    $scope.closeAlert = function(){
      $scope.reservation.alert = false;
    };

    $scope.setVersion = function(){
      var modalInstance = $modal.open({
        controller: 'SetVersionCtrl',
        templateUrl: 'taskstart/version.modal.tpl.html',
        scope: $scope,
        resolve: {
          task_type: function(){ return task_type;}
        }
      });

      modalInstance.result.then(function(){

      });
    };

    /***STANDARD FUNCTIONS***/
    var doAlert = function(paramName){
      $modal.open({
        controller: 'AlertCtrl',
        templateUrl: 'alert/alert.tpl.html',
        resolve: {
          message: function(){ return 'You have not assigned a value for the '+ paramName+' parameter';},
          title: function(){return null;}
        }
      });
    };

    var validateParameters = function(){
      var params = $scope.valParams;
      var i, param;
      for(i = params.length-1; i>=0; i--){
        param = params[i];
        if(!param.value ||  param.value === ''){
          param.error = true;
          doAlert(param.name);
          return false;
        }
      }
      params = $scope.resParams;
      for(i = params.length-1; i>=0; i--){
        param = params[i];
        if(!param.resource || !param.resource.id || (!param.resource.path && param.resource_type.nature.indexOf('stream') < 0 ) ){
          param.error = true;
          doAlert(param.name);
          return false;
        }
      }

      if($scope.requiresComputeResource && (!$scope.compParam.resource || !$scope.compParam.resource.id) ){
        $scope.compParam.error = true;
        doAlert($scope.compParam.name);
        return false;
      }

      return true;
    };

    var restartTask = function(taskInfo){
      var f =  function (){
          SmTaskCreate.restartTask(taskInfo.taskId,taskInfo.interactive).then(function(response){
          console.log('Restarting task was successful');
          $interval.cancel(restartInterval);
          $scope.$emit('SmTaskStart',Restangular.one('task',taskInfo.taskId+'/'),$scope.tasknameUser, taskInfo.interactive);
          SmTaskCreate.reset();
        }, function(response){
          console.log('Restarting task failed, Trying again in 5 seconds...');
        });
      };
      
      return f;
    };

    var getLocation = function(href) {
      var location = document.createElement("a");
      location.href = href;
      // IE doesn't populate all link properties when setting .href with a relative URL,
      // however .href will return an absolute URL which then can be used on itself
      // to populate these additional fields.
      if (location.host === "") {
        location.href = location.href;
      }
      return location;
    };

    /***EXE***/
    //Get resources that are compute compatible with this task_type
    if($scope.requiresComputeResource){
      Restangular.one('resource/?resource_type__id='+$scope.compParam.resource_type.id+'&extForUser='+uid).get('').then(function(response){
        var accessibleResources = filterFilter(response.objects,function(resource, index){return !resource.locked && resource.ext_perms.indexOf('x') >= 0;});

        if($state.is('taskstart.reservation')){
          $scope.reservation.numCompute = accessibleResources.length;  
        }
        
        $scope.computeResources = accessibleResources;

        if($state.is('taskstart.reservation') && accessibleResources.length > 0){
          $scope.compParam.resource = $scope.computeResources[0];          
        }

        if($scope.computeResources && $scope.computeResources[0]){
          $scope.compParam.resource.ext_user_cloud_acct = $scope.computeResources[0].ext_user_cloud_acct;
        }
          
        
      });  
    }

    var uiVersionSetAs, uiSettings;
    var configJson = JSON.parse(task_type.configurationJson);
    $scope.hasVersions = configJson.versioning;
    if($scope.hasVersions){
      uiSettings = SmUser.getProperty('uiSettings');

      if(!uiSettings.setToolVersions){
        uiSettings.setToolVersions = {};
      }
      
      $scope.versionSet.value = uiSettings.setToolVersions[task_type.id] || 'latest' ;
      $scope.selectedVersion.value = {tag: $scope.versionSet.value};

      if($scope.selectedVersion.value.tag !== 'latest'){
        SmTaskCreate.setVersionForTaskType($scope.selectedVersion.value.tag);
      }
      Restangular.all('task_type/'+task_type.id+'/getversions/').get('').then(function(response){
        if(response.resultcode === 0 && response.tags){
          $scope.versions = response.tags;
          $scope.versions.unshift({author: '', message: 'Latest build of this software', tag: 'latest' });
        }
      });

      
    }

    

}])
.controller('SetVersionCtrl', ['$scope', '$modalInstance','SmUser','task_type','Restangular','SmTaskCreate', function($scope, $modalInstance, SmUser, task_type, Restangular, SmTaskCreate){
  /***STANDARD VARIABLES***/
  var uiSettings = SmUser.getProperty('uiSettings');
  var uid = SmUser.getProperty('id');
  var previousVersion = uiSettings.setToolVersions[task_type.id] || 'latest';

  /***SCOPE FUNCTIONS***/
  $scope.set = function(){
    
    $scope.versionSet.value = $scope.selectedVersion.value.tag;
    uiSettings.setToolVersions[task_type.id] = $scope.selectedVersion.value.tag === 'latest' ? '' : $scope.selectedVersion.value.tag;
    Restangular.all('user_setting/'+uid+'/').patch({uiSettingsJson:JSON.stringify(uiSettings)}).then(function(){
      
      SmTaskCreate.setVersionForTaskType(uiSettings.setToolVersions[task_type.id]);
    }, function(){
      $scope.versionSet.value = previousVersion;
      uiSettings.setToolVersions[task_type.id] = previousVersion === 'latest' ? '' : previousVersion;
    });

  };

  $scope.close = function(){
    $modalInstance.close();
  };

  /***EXE***/
  for(var i=0; i<$scope.versions.length; i++){
    if($scope.versions[i].tag === $scope.selectedVersion.value.tag){
      $scope.selectedVersion.value = $scope.versions[i];
      break;
    }
  }
}]);

