'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
from tastypie.http import HttpUnauthorized
import json


class SmHttpUnauthorized(HttpUnauthorized):
    def __init__(self, message):
        jsonMessage = json.dumps({"description": message,
                                  "code": "EPERM"})
        super().__init__(jsonMessage)


class SmException(Exception):
    '''
    classdocs
    '''

    def __init__(self, httpCodeToReturn, reasonCode, path, description):
        '''
        Constructor
        '''
        self.httpCodeToReturn = httpCodeToReturn
        self.reasonCode = reasonCode
        self.description = description
        self.path = path
        Exception.__init__(self, description)
        
    def toDict(self):
        return {"code": self.reasonCode,
                "description": self.description,
                "osPath": self.path}        
      
    def __str__(self, *args, **kwargs):
        return Exception.__str__(self, *args, **kwargs) + " Path: " + self.path
    
