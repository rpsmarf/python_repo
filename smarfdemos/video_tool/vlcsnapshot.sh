#!/bin/bash
export DISPLAY=:1
VLC_SNAPSHOT_FOLDER=/home/rpsmarf/Pictures
VIDEO_FILE=$1
OUTPUT_DIR=$2
mkdir $OUTPUT_DIR
rm -f $VLC_SNAPSHOT_FOLDER/*
echo `date` >> $VLC_SNAPSHOT_FOLDER/date.txt

cat $1 | vlc - vlc://quit
#cat $1 > $VLC_SNAPSHOT_FOLDER/tempFile    
cp $VLC_SNAPSHOT_FOLDER/* $OUTPUT_DIR
