angular.module("rpsmarf.foldernav", [
	'ngSanitize',
	'ui.bootstrap',
	'rpsmarf.foldernav.dragdrop'


])

.directive('foldernav',[function(){

	return {
		restrict: 'E',
		scope:{
			resource: '=',
			entries: '=',
			ls: '&',
			selectentry: '&',
			selectall: '=',
			selectedentries: '=',
			smondrop: '&',
			entriesSearch: '@',
			infoPanel: '=',
			smmultidrop: '&'

		},
		templateUrl:  'foldernav/foldernav.tpl.html',
		link: function($scope, $element, $attrs){

			/***VARIABLES***/
			/*Scope*/
			$scope.check = '';
			$scope.linearEntries = [];
			/*Standard*/
			var listener = $scope.$watch('entries', function(){
				
				if($scope.entries){
					$scope.linearEntries = new Array($scope.entries.length);//$scope.entries;
				}

				
			});

			var numSelected = 0, pivotIndex=-1, maxSelectedIndex=-1;

			/***FUNCTIONS***/
			/*Scope*/
			$scope.entryType = function(entry){

				if(entry.isRoot){
					return 'root';
				}
				else if(entry.isDir ){
					return "folder";
				}
				else{
					return "file";
				}

			};

			$scope.toggleEntryExpand = function(entry, $event){

				$event.stopPropagation();
				var index = -1;
				var count, entriesWatcher;
				
				
				if(entry.expanded){
					
					index = traverseFindIndex($scope.entries, entry, 0).index;//$scope.linearEntries.indexOf(entry);
					count = traverseCountEntries(entry.entries, 0);
					$scope.linearEntries.splice(index+1,count);

				}
				else{

					if(!entry.entries){
						$scope.ls({resource: $scope.resource, entry:entry});
						entriesWatcher = $scope.$watch(function(){return entry.entries;}, function(){
							if(entry.entries){
								//$scope.totalEntries = new Array($scope.totalEntries.length + entry.entries.length);
								entriesWatcher();	
								index = traverseFindIndex($scope.entries, entry, 0).index;//$scope.linearEntries.indexOf(entry);
								count = traverseCountEntries(entry.entries, 0);
								$scope.linearEntries.splice.apply($scope.linearEntries, [index+1, 0].concat(new Array(entry.entries.length)));
								setAllEntriesAs(entry.entries, entry.selected);
								setLinearEntriesAs(index+1,count, entry.selected);
							}
							
														
						});

					}
					else{
						//$scope.totalEntries = new Array($scope.totalEntries.length + entry.entries.length);
						index = traverseFindIndex($scope.entries, entry, 0).index;//$scope.linearEntries.indexOf(entry);
						count = traverseCountEntries(entry.entries, 0);
						$scope.linearEntries.splice.apply($scope.linearEntries, [index+1, 0].concat(new Array(count)));
						setAllEntriesAs(entry.entries, entry.selected);
						setLinearEntriesAs(index+1,count, entry.selected);
					}

					
					
					
				}

				entry.expanded = !entry.expanded;
			};

			$scope.select = function(entry, $event){
				var index=0;
				var totalSubEntries = 0;
				var ewsad = false;

				var selectSingleEntry = function(){

				};
				//Entry not selected
				//
				if(!entry.selected){
					if(numSelected > 0){
						unsetAllLinearEntries();
						setAllEntriesAs($scope.entries, false);
					}
					entry.selected = true;
					index = traverseFindIndex($scope.entries, entry,0).index;

					//reset selected entries
					$scope.selectedentries = {};	
					$scope.selectedentries[entry.$$hashKey] = entry;

					if(!$scope.linearEntries[index]){
						$scope.linearEntries[index] = {};
					}
					$scope.linearEntries[index].selected = true;

					if($event.shiftKey){
						if(pivotIndex >=0){
							if(pivotIndex < index){
								//function(entries, start, end, currentIndex, value, selectedEntries)
								traverseSetAllEntriesAndAddToSelected($scope.entries,pivotIndex,index-1,0,true,$scope.selectedentries, false);
								//function(index, num, selected)
								numSelected = index-pivotIndex+1;
								setLinearEntriesAs(pivotIndex,numSelected-1,true);
								maxSelectedIndex = index;

							}
							else if(pivotIndex > index){
								traverseSetAllEntriesAndAddToSelected($scope.entries,index+1, pivotIndex,0,true,$scope.selectedentries, false);
								numSelected = pivotIndex-index+1;
								setLinearEntriesAs(index+1,numSelected-1,true);
								maxSelectedIndex = pivotIndex;
							}

							$scope.resource.selectedEntry = null;
						}
						else{
							pivotIndex = index;
							numSelected = 1;
							$scope.resource.selectedEntry = entry;
						}
					}
					//shiftKey not pressed
					else{
			
						if(entry.entries && entry.expanded){
						
							setAllEntriesAs(entry.entries, entry.selected);
							count = traverseCountEntries(entry.entries, 0);
							setLinearEntriesAs(index+1, count, entry.selected);
						}

						numSelected = 1;
						$scope.resource.selectedEntry = entry;
						pivotIndex = index;
						maxSelectedIndex = -1;
					}

					//countNumSelected($scope.linearEntries);
					
					
				}
				//entry is selected
				else {
					ewsad = entryWithinSelectedAndDelete(entry);
					//Entry is selected (based on entry.selected) and is a sub entry witin an entry that is in the list 'selectedentries'.
					//Note that in this case 'entry' will not be within the 'selectedentries' list since only parents are added to this list
					//This scenario is one where there is at least one entry selected (numSelected >=1) and the currently clicked entry is a sub entry
					//within one of the entries within selectedentries. This would mean that all currently selected entries would be unselected and leave only 
					//the entry that was clicked
					if(numSelected >= 1 && ewsad){

						unsetAllLinearEntries();
						setAllEntriesAs($scope.entries, false);
						entry.selected = true;
						index = traverseFindIndex($scope.entries, entry,0).index;
						//reset selected entries
						$scope.selectedentries = {};
						$scope.selectedentries[entry.$$hashKey] = entry;
						if(!$scope.linearEntries[index]){
							$scope.linearEntries[index] = {};
						}
						$scope.linearEntries[index].selected = true;	

						if($event.shiftKey){
							if(pivotIndex >= 0){
								if(pivotIndex < index){
									//function(entries, start, end, currentIndex, value, selectedEntries)
									traverseSetAllEntriesAndAddToSelected($scope.entries,pivotIndex,index-1,0,true,$scope.selectedentries, false);
									numSelected = index-pivotIndex+1;
									setLinearEntriesAs(pivotIndex,numSelected-1,true);

								}
								else if(pivotIndex > index){
									traverseSetAllEntriesAndAddToSelected($scope.entries,index+1, pivotIndex,0,true,$scope.selectedentries,false);
									numSelected = pivotIndex-index+1;
									setLinearEntriesAs(index+1,numSelected-1,true);
								}
								$scope.resource.selectedEntry = null;
							}
							//Should not be reachable since within this block the entry is selected meaning that pivotIndex SHOULD be set
							else{
								pivotIndex = index;
								numSelected = 1;
								$scope.resource.selectedEntry = entry;
							}
						}
						else {
							if(entry.entries && entry.expanded){
							
								setAllEntriesAs(entry.entries, entry.selected);
								count = traverseCountEntries(entry.entries, 0);
								setLinearEntriesAs(index+1, count, entry.selected);
							}
							numSelected =1;
							$scope.resource.selectedEntry = entry;
							pivotIndex = index;	
						}
						
					}
					else if(numSelected >1 && !ewsad){
						unsetAllLinearEntries();
						setAllEntriesAs($scope.entries, false);
						entry.selected = true;
						index = traverseFindIndex($scope.entries, entry,0).index;
						$scope.selectedentries = {};
						$scope.selectedentries[entry.$$hashKey] = entry;
						if(!$scope.linearEntries[index]){
							$scope.linearEntries[index] = {};
						}
						$scope.linearEntries[index].selected = true;	

						if($event.shiftKey){
							if(pivotIndex >= 0){
								if(pivotIndex < index){
									//function(entries, start, end, currentIndex, value, selectedEntries)
									traverseSetAllEntriesAndAddToSelected($scope.entries,pivotIndex,index-1,0,true,$scope.selectedentries,false);
									numSelected = index-pivotIndex+1;
									setLinearEntriesAs(pivotIndex,numSelected-1,true);

								}
								else if(pivotIndex > index){
									traverseSetAllEntriesAndAddToSelected($scope.entries,index+1, pivotIndex,0,true,$scope.selectedentries, false);
									numSelected = pivotIndex-index+1;
									setLinearEntriesAs(index+1,numSelected-1,true);
								}
								$scope.resource.selectedEntry = null;
							}
							//Should not be reachable since within this block the entry is selected meaning that pivotIndex SHOULD be set
							else{
								pivotIndex = index;
								numSelected = 1;
								$scope.resource.selectedEntry = entry;
							}
						}
						else {
							if(entry.entries && entry.expanded){
							
								setAllEntriesAs(entry.entries, entry.selected);
								count = traverseCountEntries(entry.entries, 0);
								setLinearEntriesAs(index+1, count, entry.selected);
							}
							numSelected =1;
							$scope.resource.selectedEntry = entry;
							pivotIndex = index;	
						}
					}					
					else if(numSelected == 1 && !ewsad) {
						if(!$event.shiftKey){
							numSelected =0;
							entry.selected = false;
							unsetAllLinearEntries();
							setAllEntriesAs($scope.entries, entry.selected);
							$scope.selectedentries= {};
							$scope.resource.selectedEntry = null;
							pivotIndex = -1;
							maxSelectedIndex = -1;
						}
							
					}
					

				}

				//if(entry.selected && numSelected == 1){
				//	$scope.selectentry({entry:entry});
				//}
				if($scope.selectentry){
					$scope.selectentry({entry: $scope.resource.selectedEntry});
				}
				

			};
			
			$scope.clickCheckbox = function(linearEntry, index, $event){
				var entry;
				var count =0;
				var retVal;
				maxSelectedIndex = -1;
				retVal = traverseFindEntryAndSelect(index,$scope.entries, 0);
				entry = retVal.entry;
				if(entryWithinSelectedAndDelete(entry)){
					
					$event.preventDefault();
					$event.stopPropagation();
					entry.selected = true;
					if(entry.entries){
						count = traverseCountEntries(entry.entries, 0);
						setLinearEntriesAs(retVal.index+1, count, linearEntry.selected);
						setAllEntriesAs(entry.entries, entry.selected);
					}
					
					$scope.selectedentries[entry.$$hashKey] = entry;
					if(!$scope.linearEntries[index-1]){
						$scope.linearEntries[index-1] = {};
					}
					$scope.linearEntries[index-1].selected = true;
					
					//numSelected = countNumSelected($scope.linearEntries);
				}
				else {
					if(!linearEntry){
						linearEntry = {};
					}
					linearEntry.selected = !linearEntry.selected;

					
					

					retVal = traverseFindEntryAndSelect(index,$scope.entries, 0);
					entry = retVal.entry;
					if(entry.entries && entry.expanded){
						count = traverseCountEntries(entry.entries, 0);
						setLinearEntriesAs(retVal.index+1, count, linearEntry.selected);
						setAllEntriesAs(entry.entries, linearEntry.selected);
					}

					if(linearEntry.selected){
						$scope.selectedentries[entry.$$hashKey] = entry;
						updateSelectedEntries(entry);
						++numSelected;
					}
					else{
						delete $scope.selectedentries[entry.$$hashKey];
						--numSelected;
					}
					
					$scope.linearEntries[index-1]= linearEntry;
					entry.selected = linearEntry.selected;
					//numSelected = countNumSelected($scope.linearEntries);
				
				}

				if(numSelected === 1){
					$scope.resource.selectedEntry = entry;
				}
				else {
					$scope.resource.selectedEntry = null;
				}

				if($scope.selectentry){
					$scope.selectentry({entry: $scope.resource.selectedEntry});
				}

				
			};

			/*Standard*/
			var setSelected = function(entry){
				var i=$scope.linearEntries.length-1;
				for(;i>=0; i--){
					if($scope.linearEntries[i].name === entry.name){
						$scope.linearEntries[i].selected = true;
					}
				}
			};

			//Starting at 'index' set 'num' amount of linearEntries as value 'selected'
			var setLinearEntriesAs = function(index, num, selected){
				var i=0;
				for(;i< num; i++){
					if(!$scope.linearEntries[index + i]){
						$scope.linearEntries[index + i] = {};
					}
					$scope.linearEntries[index + i].selected = selected;
					
				}
			};
			var unsetAllLinearEntries = function(entry){
				var i = $scope.linearEntries.length-1;
				for(;i>=0; i--){
					if(!$scope.linearEntries[i]){
						$scope.linearEntries[i] = {};
					}
					$scope.linearEntries[i].selected = false;
				}
				
			};

			var setAllEntriesAs = function(entries, selected){
				var subEntry;
				
				for(var i=0; i<entries.length; i++){
					subEntry = entries[i];
					subEntry.selected = selected;
					if(subEntry.entries && subEntry.expanded){
						setAllEntriesAs(subEntry.entries, selected);
					}
					
				}
			};

			var traverseSetAllEntriesAndAddToSelected = function(entries, start, end, currentIndex, value, selectedentries, parentSelected){
				var entry, numSubEntries;
				for(var i=0; i < entries.length; i++){
					entry = entries[i];
					
					if(currentIndex < start){
						if(entry.entries && entry.expanded){
							numSubEntries = traverseSetAllEntriesAndAddToSelected(entry.entries,start,end, currentIndex+1, value, selectedentries, false );
							if(numSubEntries.allSet){
								return numSubEntries;
							}
							currentIndex = numSubEntries.currentIndex;
							//increment current index by 1 since the returned current index will be last accessible index value
							
						}
						else{
							currentIndex++;
						}
						
						
					}
					else if(currentIndex >=start && currentIndex <=end){
						entry.selected = value;
						if(value && !parentSelected){
							selectedentries[entry.$$hashKey] = entry;
						}
						if(entry.entries){
							numSubEntries = traverseSetAllEntriesAndAddToSelected(entry.entries, start, end, currentIndex+1, value, selectedentries, true);
							if(numSubEntries.allSet){
								return numSubEntries;
							}
							currentIndex = numSubEntries.currentIndex;
							
						}
						else{
							currentIndex++;
						}
						
					}
					else if(currentIndex > end){
						return {allSet: true};
					}					
				}

				return {currentIndex:currentIndex};
			};

			var traverseFindEntryAndSelect = function(target, entries, currentCount){
				var subEntry;
				var retVal;
				for(var i=0; i<entries.length; i++){
					subEntry = entries[i];
					currentCount++;
					if(currentCount == target){
						subEntry.selected = !subEntry.selected;
						if(subEntry.entries){
							setAllEntriesAs(subEntry.entries, subEntry.selected);
						}
						return {index: currentCount-1, entry:subEntry};
					}
					else if(subEntry.entries && subEntry.expanded){
						retVal = traverseFindEntryAndSelect(target,subEntry.entries, currentCount);
						if(retVal.entry === null){
							currentCount = retVal.index;
						}
						else{
							return retVal;
						}
					}
					
				}

				return {index: currentCount, entry: null};
				
			};

			var traverseFindIndex = function(entries, target, currentCount){
				var subEntry;
				var retVal;
				for(var i=0; i<entries.length; i++){
					subEntry = entries[i];
					
					if(target.name == subEntry.name){
						return {found: true, index: currentCount};
					}
					else if(subEntry.entries && subEntry.expanded){
						retVal = traverseFindIndex(subEntry.entries, target, currentCount+1);
						
						if(retVal.found === true){
							return retVal;
						}
						else{
							currentCount = retVal.index;
						}
					}

					currentCount++;
					
				}

				return {found: false, index: currentCount-1};
				
			};

			var traverseCountEntries = function(entries, currentCount){
				var subEntry;
				
				for(var i=0; i<entries.length; i++){
					subEntry = entries[i];
					currentCount++;
					if(subEntry.entries && subEntry.expanded){
						currentCount = traverseCountEntries(subEntry.entries, currentCount);
					}
					
				}

				return currentCount;

			};

			var updateSelectedEntries = function(entry){
				var level;
				var selectedEntry;
				var res;
				var numOccurrences = occurrences(entry.name, '/', true)-1;
				var key;
				
				for(key in $scope.selectedentries){
					selectedEntry = $scope.selectedentries[key];
					level = occurrences(selectedEntry.name,'/',true );

					if(selectedEntry.isDir){
						--level;
					}

					if(entry.$$hashKey == key || level == numOccurrences){
						continue;
					}
					
					if(level > numOccurrences && entry.entries){
						res = traverseFindEntryAndRemove(entry.entries, selectedEntry);

						if(res){
							delete $scope.selectedentries[selectedEntry.$$hashKey];
							return;
						}
					}

					else if(numOccurrences > level && selectedEntry.entries){
						res = traverseFindEntryAndRemove(selectedEntry.entries, entry);

						if(res){
							delete $scope.selectedentries[entry.$$hashKey];
							return;
						}
					}
					
				}

			
				
			};

			var traverseFindEntryAndRemove = function(entries, entry){
				var subEntry;
				var retVal;
				for(var i=0; i< entries.length; i++){
					subEntry = entries[i];
					if(subEntry.name == entry.name){
						return true;
					}
					else if(subEntry.entries){
						retVal = traverseFindEntryAndRemove(subEntry.entries, entry);
						if(retVal){
							return true;
						}
					}

				}

				return false;
			};

			var occurrences = function(string, subString, allowOverlapping){

				string+=""; subString+="";
				if(subString.length<=0){
					return string.length+1;
				}

				var n=0, pos=0;
				var step=(allowOverlapping)?(1):(subString.length);

				while(true){
					pos=string.indexOf(subString,pos);
					if(pos>=0){ 
						n++; pos+=step; 
					}
					else{
						break;
					}
				}
				return(n);
			};

			var countNumSelected = function(entries){
				var entry;
				var num=0;
				for(var i= entries.length-1; i>=0; i--){
					entry = entries[i];
					if(entry && entry.selected){
						++num;
					}
				}
				return num;
			};

			//Determines whether the entry clicked is a sub entry of one of the selected entries. If this is true then the parent entry is deleted from the
			//list of selected entries
			var entryWithinSelectedAndDelete = function(entry){

				var selectedEntry;
				var res;
				var key;
				var index;
				var numSubEntries;
				var addition=0;
				
				//If the entry is one of the selected entries it is not sub entry within one of the selected entries
				for(key in $scope.selectedentries){
					selectedEntry = $scope.selectedentries[key];
					if(entry.$$hashKey == key ){
						return false;
					}
					
				}

				//Check the sub entries for the selected entries
				for(key in $scope.selectedentries){
					selectedEntry = $scope.selectedentries[key];
					if(selectedEntry.entries){
						res = entryIsChild(entry,selectedEntry.entries);

						if(res){
							setAllEntriesAs(selectedEntry.entries, false);
							selectedEntry.selected = false;
							index = traverseFindIndex($scope.entries,selectedEntry,0).index;
							numSubEntries = traverseCountEntries(selectedEntry.entries,0);
							
							setLinearEntriesAs(index, numSubEntries+1, false);
							delete $scope.selectedentries[key];
							//console.log(childIndex);
							return true;
						}
						
					}
				}
				

				return false;
				
			};

			//Checks if 'entry' is a child of an element within 'entries'
			var entryIsChild = function(entry, entries){
				var en;
				var val;
				for(var i=0;i < entries.length; i++){
					en = entries[i];
					if(entry.$$hashKey === en.$$hashKey){
						return true;
					}
					else if(en.entries){
						val = entryIsChild(entry, en.entries);
						if(val){
							return val;
						}

					}
				}

				return false;
			};

			//returns the smallest index value which has been selected from 'entries'
			var getMinimumSelectedIndexValue = function(entries){
				var i=0;
				for(;i<entries.length; i++){
					if(entries[i] && entries[i].selected){
						return i;
					}
				}

				return -1;
			};

			//returns the largest index value which has been selected from 'entries'
			var getMaximumSelectedIndexValue = function(entries){
				var i=entries.length-1;
				for(;i>=0; i--){
					if(entries[i] && entries[i].selected){
						return i;
					}
				}

				return -1;
			};
			
		}
		
		
	};




}]);