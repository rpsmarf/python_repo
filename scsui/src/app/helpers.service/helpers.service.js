angular.module('rpsmarf.helpers.service',[
	'rpsmarf.config',
	'ui.router',
	'rpsmarf.alert',
	'ui.bootstrap',
	'rpsmarf.task.create.service',
	'restangular',
	'rpsmarf.user.service',
	'ipCookie',
	'rpsmarf.disqus.embed'
])
.service('SmHelpers',[ '$state', '$modal', 'SmTaskCreate','$interval','Restangular', '$window','SmUser','USER_STATE','ipCookie','FORUM_URL', 'SmDisqus',
	function($state, $modal, SmTaskCreate, $interval, Restangular, $window, SmUser, USER_STATE, ipCookie, FORUM_URL, SmDisqus){
	
	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var monthsShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	//task.toolName is currently expected to be the name of the task_type (tool) being run
	//task.taskName is the user specified name of the task
	var handleErrorOnTaskStart = function(error, task, $scope){
		var errMsg, endDate, title='Error';
		var taskInfo = error.data.taskInfo, modalInstance, endTime, endTimeMeridian;

		if(error.status === 410){
			errMsg = 'An error occured while attempting to start the task. Please try again later.';

			modalInstance = $modal.open({
				controller: 'AlertCtrl',
				templateUrl: 'alert/alert.tpl.html',
				resolve:{
					title: function(){ return 'Error';},
					message: function(){ return errMsg;}
				}
			});

			modalInstance.result.then(function(){
				if($state.includes('taskstart')){
					$state.go('home');
					return;
				}
			});
		}
		else if(error.status === 409){

			if(error.data.status && error.data.status === 'reserved'){
				endDate = new Date(error.data.reservation.end_time);

				if(endDate.getHours()< 12){
					endTimeMeridian = 'AM';
					endTime = endDate.getHours();
				}
				else if(endDate.getHours() === 12){
					endTime = '12';
					endTimeMeridian = 'PM';
				}
				else{
					endTime = endDate.getHours() - 12;
					endTimeMeridian = 'PM';
				}
				endTime += ':' + (endDate.getMinutes() < 10 ? '0'+endDate.getMinutes() : endDate.getMinutes()) + ' ' + endTimeMeridian;
				errMsg = 'Error starting '+ task.toolName+':<br>The selected compute resource is currently in use and is reserved until '+ endTime + ' on ' + months[endDate.getMonth()] + ' ' + endDate.getDate() + ' ' + endDate.getFullYear();
				errMsg += '. Would you like to add your task to the queue for this resource? (Task will run once the resource is free)';

				modalInstance = $modal.open({
					controller: 'ConfirmQueueCtrl',
					templateUrl: 'confirm/confirm.tpl.html',
					resolve: {
						title: function(){ return title;},
						body: function(){return errMsg;}
					}
				});

				modalInstance.result.then(function(){
					console.log('restarting with queue');
					var p = SmTaskCreate.restartTaskWithQueueing(taskInfo.taskId,taskInfo.interactive);
					p.then(function(){
						$state.go('home');
					});
				},function(){
					Restangular.one('task',taskInfo.taskId+'/').remove().then(function(response){
						console.log('Queueing cancelled, task deleted');
					});
				});

				return true;
			}
			else if(error.data.status && error.data.status === 'cancelling'){
				errMsg = 'The system is preparing the resource for your task. It will begin once it is ready.';
				title = 'Notice';
				restartInterval = $interval(restartTask(taskInfo, task, $scope),5000);
				$modal.open({
					controller: 'AlertCtrl',
					templateUrl: 'alert/alert.tpl.html',
					resolve: {
						title: function(){ return title;},
						message: function(){return errMsg;}
					}
				});

				return true;
			}
			else if(error.data.status && error.data.status === 'inuse'){
				errMsg = 'Error starting '+ task.toolName+':<br>The selected compute resource is currently in use';
				errMsg += '. Would you like to add your task to the queue for this resource? (Task will run once the resource is free)';
				modalInstance = $modal.open({
					controller: 'ConfirmQueueCtrl',
					templateUrl: 'confirm/confirm.tpl.html',
					resolve: {
						title: function(){ return title;},
						body: function(){return errMsg;}
					}
				});

				modalInstance.result.then(function(){
					console.log('restarting with queue');
					SmTaskCreate.restartTaskWithQueueing(taskInfo.taskId,taskInfo.interactive);
				},function(){
					Restangular.one('task',taskInfo.taskId+'/').remove().then(function(response){
						console.log('Queueing cancelled, task deleted');
					});
				});

				return true;
			}else {
				return false;
			}
		}

	};

	var restartTask = function(taskInfo, task, $scope){
		var f =  function (){
			SmTaskCreate.restartTask(taskInfo.taskId,taskInfo.interactive).then(function(response){
				console.log('Restarting task was successful');
				$interval.cancel(restartInterval);
				$scope.$emit('SmTaskStart',Restangular.one('task',taskInfo.taskId+'/'),task.taskName, taskInfo.interactive);
				SmTaskCreate.reset();
			}, function(response){
				console.log('Restarting task failed, Trying again in 5 seconds...');
			});
		};

		return f;
	};

	var getObjectIdFromPathName = function(href) {
		var location = document.createElement("a");
		location.href = href;
		// IE doesn't populate all link properties when setting .href with a relative URL,
		// however .href will return an absolute URL which then can be used on itself
		// to populate these additional fields.
		if (location.host === "") {
			location.href = location.href;
		}
		return parseInt(location.pathname.split('/')[3],10);
	};

	var completeFolderDownload = function(info){
		var rid = info.resourceId;
		var path = info.zipFilePath;
		var baseUrl = Restangular.configuration.baseUrl;
		baseUrl = baseUrl.substring(baseUrl.length-1) === '/' ? baseUrl.substring(0,baseUrl.length-1) : baseUrl;
		$window.open(baseUrl+'/resource/'+rid+'/'+path+'/download/');
		//Restangular.all('resource/'+rid+'/'+path+'/download/').get('').then(function(response){
			//Restangular.one('resource/'+rid+'/'+path,'file/').remove().then(function(response){
			//	console.log('temp file removed');
			//});
		//});
	};

	var completeUserLogin = function(userData){

		SmUser.setProperty('id', userData.userid);
		SmUser.setProperty('email', userData.email);
		SmUser.setProperty('firstname', userData.firstname);
		SmUser.setProperty('lastname', userData.lastname);
		SmUser.setProperty('community', userData.community);
		SmUser.setProperty('apikey', userData.apikey);
		SmUser.setProperty('state', USER_STATE.loggingIn);
		SmUser.setProperty('csrf', ipCookie('csrftoken'));
		if(userData.imageUrl){
			SmUser.setProperty('imageUrl', userData.imageUrl);
		}
		if(userData.uiSettingsJson){
			SmUser.setProperty('uiSettings', JSON.parse(userData.uiSettingsJson));
		}

		if(userData.disqusInfo.valid){
			SmDisqus.setConfig(userData.disqusInfo);
		}

		FORUM_URL.value = userData.forumUrl; 
	};

	var normalizeTime = function(time){
		var date = new Date(time);


		return monthsShort[date.getMonth()] + " " + date.getDate() + (" "+date.getFullYear()).substring(date.getFullYear().length-2) + " " + date.toTimeString().substring(0,5);  
	};

	var duration = function(start_time, end_time){
		if(!start_time || !end_time){
			return '-----';
		}
		var sd = start_time;
		var ed = end_time;
		var sdm;
		var edm;
		var diff;
		var duration="";
		if(sd[sd.length-1] !== 'Z'){
			sd += 'Z';
		}
		if(ed[ed.length-1] !== 'Z'){
			ed += 'Z';
		}

		sdm = Date.parse(sd);
		edm = Date.parse(ed);

		diff = edm-sdm;
		//Hours
		duration = diff/3600000 > 1 ? Math.floor(diff/3600000) +'h': '';
		//Minutes
		diff = diff%3600000;
		duration += diff/60000 > 1 ? Math.floor(diff/60000) +'m': '';
		//Seconds
		diff = diff%60000;
		duration += diff/1000 > 1 ? Math.floor(diff/1000) :'';
		//milliseconds
		diff = diff%1000;
		duration += diff > 0 ?  diff > 1000 ? '.'+diff.substring(0,3)+'s' : '.'+diff+'s' : 's';

		if(duration === '' || duration === 's'){
			duration = '---';
		}

		return duration;
		
	};

	return {
		handleErrorOnTaskStart: handleErrorOnTaskStart,
		getObjectIdFromPathName: getObjectIdFromPathName,
		completeFolderDownload: completeFolderDownload,
		completeUserLogin: completeUserLogin,
		normalizeTime: normalizeTime,
		duration: duration
	};
}]);