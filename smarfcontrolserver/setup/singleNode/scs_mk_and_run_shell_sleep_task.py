import json
import sys
from scripts.scs_task_start import scsTaskStartByPath
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath
from scripts.scs_obj_create import scsCreateObjectPath

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

argv = sys.argv
if len(argv) == 1:
    print("Must specify a version\n")
    print("Do curl http://localhost/scs/task_type/?name_key=asteroids|grep id to see the asteroids task type\n")
    print("Then curl http://localhost/scs/task_type/<id>/getversions/ to see the versions available\n")
else:
    version = argv[1]
    print("Running tool with version: '{}'".format(version))
      
        
taskParams = {
              "version": version 
              }

tt_path = scsGetObjectPath("task_type", "name_key=asteroids")
root_path = scsGetObjectPath("user", "name=rpsmarf")
task_path = scsCreateObjectPath("task", {"name": "Asteroids", 
                                         "task_type": tt_path,
                                         "owner": root_path,
                                         "parametersJson": json.dumps(taskParams),
                                         }
                                )

rt3_compute_path = scsGetObjectPath("resource_type_task_type", "name_key=asteroids_compute_rt3")


compute_resource_path = scsGetObjectPath("resource", "name=c1_compute")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": compute_resource_path,
                                      "resource_type_task_type": rt3_compute_path
                                      }
                    )

print("Task " + task_path + " to run asteroids is starting...")

scsTaskStartByPath(task_path, verbose=True)

