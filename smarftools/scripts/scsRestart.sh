#!/bin/bash

echo "Deleting logs..."
sudo rm -f /var/log/smarf-scs/*
echo "Restarting SCS..."
sudo service smarf-scs restart
echo "Restarting nginx ..."
sudo service nginx restart
echo "Done"
