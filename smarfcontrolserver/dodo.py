'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import glob
import os
import fnmatch
import datetime

pyFiles = []
pyFilesToCheck = []
for root, dirnames, filenames in os.walk('.'):
    for filename in fnmatch.filter(filenames, '*.py'):
        pyFiles.append(os.path.join(root, filename))
        if (not (filename.endswith("_ice.py") or filename.startswith("__"))):
            pyFilesToCheck.append(os.path.join(root, filename))

zipFiles = pyFiles
for root, dirnames, filenames in os.walk('sm_controller/ui/assets'):
    for filename in filenames:
        zipFiles.append(os.path.join(root, filename))
zipFiles += glob.glob("canarie_service/templates/platform/*.html")
zipFiles += glob.glob("canarie_service/templates/cloud_rpi/*.html")
zipFiles += glob.glob("canarie_service/templates/metadata_rpi/*.html")

COVERAGE_REPORT_OMIT_LIST = "smarfcontrolserver/wsgi.py," + \
    "smarfcontrolserver/init/scs_init.py," + \
    "smarfcontrolserver/init/scs_init_after_django.py"
installFiles = glob.glob("install/*") + glob.glob("scripts/*.py") + glob.glob("error_pages/*.html")

debFiles = installFiles
debFiles.append('../build_output/scs_python.zip')
debFiles.append('../build_output/smarf_common.zip')

SMARF_HOST = "unset"
SMARF_USER = "unset"
if ("SMARF_HOST" in os.environ):
    SMARF_HOST = os.environ["SMARF_HOST"]
if ("SMARF_GITDIR" in os.environ):
    SMARF_GITDIR = os.environ["SMARF_GITDIR"]
if ("SMARF_USER" in os.environ):
    SMARF_USER = os.environ["SMARF_USER"]
t = datetime.datetime.today()
SMARF_VERSION = t.strftime("%Y%m%d%H%M%S") + "-" + SMARF_USER
    
SSH_CMD = ("ssh  -o UserKnownHostsFile=/dev/null " + 
"-o StrictHostKeyChecking=no " + 
"-i ../tools/keys/smarf_dair_keys.pem ubuntu@" + SMARF_HOST)
FLAKE8_OPTIONS = "--ignore=W292,E291,E261,E262,E265,E292,E128,W293,W291,E123,E126,W391 --max-line-length=128"


DOIT_CONFIG = {'default_tasks': ['show']}


def task_show():
    """show all available tasks"""
    return {'actions': ['doit list'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }


def task_keysetup():
    """show all available tasks"""
    return {'actions': ['rm -f ../build_output/smarf_dair_keys.pem',
                        'cp ../tools/keys/smarf_dair_keys.pem ../build_output/smarf_dair_keys.pem',
                        'chmod 400 ../build_output/smarf_dair_keys.pem'],
            'file_dep': ["../tools/keys/smarf_dair_keys.pem"],
            'targets': ["../build_output/smarf_dair_keys.pem"],
            'verbosity': 2,
            }


def task_vlist():
    """show all available versions of smarf-scs on software server"""
    return {'actions': ['sudo apt-get update > /dev/null',
                        'apt-cache show smarf-scs | grep Version'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }


def task_checker():
    """run flake8 on all project files"""
    for module in pyFilesToCheck:
        yield {'actions': ['../tools/flake8_wrapper smarfcontrolserver ' + FLAKE8_OPTIONS + ' %(dependencies)s '
                           ],
               'name': module,
               'file_dep': [module],
               'verbosity': 0,
               }


def task_commonzip():
    """create ZIP file with smcommon SMARF code."""
    return {'actions': [
            'doit --dir ../smarfcommon zip',
            ],
            'targets': ['../build_output/smarf_common.zip'],
            'verbosity': 2,
            'clean': ["rm -f ../build_output/smarf_common.zip"
                      ],
            }


def task_zip():
    """create ZIP file with SCS Python code.  
    Clean deletes all pycache folders"""
    return {'actions': [
            'rm -f ../build_output/scs_python.zip',
            'zip -q -r ../build_output/scs_python.zip . '
            '--exclude \*__pycache__\* '
            '--exclude .\* '
            '--exclude dodo.py '
            '--exclude install/\*'],
            'targets': ['../build_output/scs_python.zip'],
            'file_dep': zipFiles,
            'task_dep': ['checker'],
            'verbosity': 2,
            'clean': ["rm -f ../build_output/scs_python.zip",
                      "find . -name __pycache__ -prune -exec rm -r {} \;"
                      ],
            }


def task_deb():
    """create .deb file for the SCS"""
    return {'actions': [
            'rm -rf ../build_output/smarf-scs',
            'mkdir ../build_output/smarf-scs',
            'mkdir ../build_output/smarf-scs/DEBIAN',
            'sed -e s/@VERSION/' + SMARF_VERSION + 
'/ install/control > ../build_output/smarf-scs/DEBIAN/control',
            'cp install/conffiles ../build_output/smarf-scs/DEBIAN',
            'cp install/postinst ../build_output/smarf-scs/DEBIAN',
            'chmod 755 ../build_output/smarf-scs/DEBIAN/postinst',
            'mkdir -p ../build_output/smarf-scs/etc/init',
            'cp install/etc_init_smarf-scs.conf ../build_output/smarf-scs/etc/init/smarf-scs.conf ',
            'mkdir -p ../build_output/smarf-scs/etc/nginx/sites-enabled',
            'cp install/etc_nginx_sites-enabled_smarf-scs.conf '
'../build_output/smarf-scs/etc/nginx/sites-enabled/smarf-scs.conf',
            'cp install/etc_nginx_sites-enabled_smarf-scs_log.conf '
'../build_output/smarf-scs/etc/nginx/sites-enabled/smarf-scs_log.conf',
            'mkdir ../build_output/smarf-scs/etc/smarf-scs',
            'cp install/scs.conf ../build_output/smarf-scs/etc/smarf-scs',
            'cp install/logging.conf ../build_output/smarf-scs/etc/smarf-scs',
            'mkdir -p ../build_output/smarf-scs/var/run/smarf-scs',
            'sudo chown www-data.www-data ../build_output/smarf-scs/var/run/smarf-scs',
            'mkdir -p ../build_output/smarf-scs/var/log/smarf-scs',
            'sudo chown www-data.www-data ../build_output/smarf-scs/var/log/smarf-scs',
            'mkdir -p ../build_output/smarf-scs/opt/smarf-scs/scs',
            'unzip ../build_output/scs_python.zip -d ../build_output/smarf-scs/opt/smarf-scs/scs',
            'unzip -o ../build_output/smarf_common.zip -d ../build_output/smarf-scs/opt/smarf-scs/scs',
            'mkdir -p ../build_output/smarf-scs/usr/local/bin',
            'cp scripts/*.py ../build_output/smarf-scs/usr/local/bin',
            'mkdir ../build_output/smarf-scs/etc/smarf-scs/keys',
            'cp ../tools/keys/guac_key.private ../build_output/smarf-scs/etc/smarf-scs/keys',
            'chmod 400 ../build_output/smarf-scs/etc/smarf-scs/keys/*',
            'chmod 755 ../build_output/smarf-scs/usr/local/bin/*',
            'chmod 755 ../build_output/smarf-scs/opt/smarf-scs/scs/manage.py',
            'mkdir -p ../build_output/smarf-scs/var/www/error_pages_scs',
            'cp -r error_pages/* ../build_output/smarf-scs/var/www/error_pages_scs',
            'cp -r canarie_service/templates/var_www_rpsmarf_canarie_service_assets ' +
            '../build_output/smarf-scs/var/www/rpsmarf_canarie_service_assets',
            'cp -r install/var_www_favicon.ico ../build_output/smarf-scs/var/www/favicon.ico',
            'cd ../build_output;dpkg -b smarf-scs',
            ],
            'targets': ['../build_output/smarf-scs.deb'],
            'file_dep': debFiles,
            'task_dep': ['commonzip'],
            'clean': True,
            }


def task_install():
    """install SCS DEB package locally"""
    return {'actions': ['sudo dpkg -r smarf-scs',
                        'sudo dpkg -i ../build_output/smarf-scs.deb'],
            'file_dep': ['../build_output/smarf-scs.deb'],
            'targets': ['/etc/init.d/smarf-scs'],
            'verbosity': 2,
            'clean': ["sudo dpkg --purge smarf-scs"],
            }


def task_test():
    """Run the unit tests"""
    return {'actions': ['export PYTHONPATH=../smarfcommon;'
                        + 'coverage run --source=smarfcontrolserver,sm_controller manage.py test',
                        "coverage report --omit " + COVERAGE_REPORT_OMIT_LIST +
                        "|grep TOTAL|sed -e 's/TOTAL                    /CODE COVERAGE/'",
                        ],
            'file_dep': ['../build_output/scs_python.zip'],
            'verbosity': 2,
            'clean': ['rm /tmp/unittestLogging.conf'],
            }


def task_coverage():
    """view code coverage for the SMARF remote agent"""
    return {'actions': ["rm -rf /tmp/htmlcoverage",
                        "coverage html  --omit " + COVERAGE_REPORT_OMIT_LIST +
                        " -d /tmp/htmlcoverage",
                        "firefox /tmp/htmlcoverage/index.html",
                        ],
            'task_dep': ['test'],
            'verbosity': 2,
            }


def task_rinstall():
    """install the currently building SCS DEB package remotely"""
    return {'actions': [
            'scp -q -o UserKnownHostsFile=/dev/null '
'-o StrictHostKeyChecking=no '
'-i ../tools/keys/smarf_dair_keys.pem ../build_output/smarf-scs.deb ubuntu@' + 
SMARF_HOST + ':/tmp',
            SSH_CMD + ' "sudo dpkg -i /tmp/smarf-scs.deb"',
            ],
            'file_dep': ['../build_output/smarf-scs.deb',
                         '../build_output/smarf_dair_keys.pem'],
            'verbosity': 2,
            }


def task_rclearlog():
    """Clear logs on the remote server defined by SMARF_HOST"""
    return {'actions': [
            SSH_CMD + ' "sudo rm -f /var/log/smarf-scs/*"',
            ],
            'uptodate': [False],
            'file_dep': ['../build_output/smarf_dair_keys.pem'],
            'verbosity': 2,
            }


def task_rrestart():
    """restart the SCS on the remote server defined by SMARF_HOST"""
    return {'actions': [
            SSH_CMD + ' "sudo service smarf-scs restart"',
            ],
            'file_dep': ['../build_output/smarf_dair_keys.pem'],
            'uptodate': [False],
            'verbosity': 2,
            }


def task_rstop():
    """stop the SCS on the remote server defined by SMARF_HOST"""
    return {'actions': [
            SSH_CMD + ' "sudo service smarf-scs stop"',
            ],
            'file_dep': ['../build_output/smarf_dair_keys.pem'],
            'uptodate': [False],
            'verbosity': 2,
            }


def task_rstart():
    """start the SCS on the remote server defined by SMARF_HOST"""
    return {'actions': [
            SSH_CMD + ' "sudo service smarf-scs start"',
            ],
            'file_dep': ['../build_output/smarf_dair_keys.pem'],
            'uptodate': [False],
            'verbosity': 2,
            }


def task_rwipedb():
    """Clear logs on the remote server defined by SMARF_HOST"""
    return {'actions': [
            SSH_CMD + ' "source /opt/python3.4/bin/activate && /usr/local/bin/scsWipeAndInit.py"',
            ],
            'uptodate': [False],
            'file_dep': ['../build_output/smarf_dair_keys.pem'],
            'verbosity': 2,
            }


def task_i():
    '''
    Alias of rinstall
    '''
    return task_rinstall()


def task_c():
    '''
    Alias of rclearlog
    '''
    return task_rclearlog()


def task_r():
    '''
    Alias of rrestart
    '''
    return task_rrestart()


def task_debup():
    """create and upload .deb file for the SCS"""
    return {'actions': 
            ['scp -q -o UserKnownHostsFile=/dev/null '
             '-o StrictHostKeyChecking=no '
            '-i ../tools/keys/smarf_dair_keys.pem '
            '../build_output/smarf-scs.deb '
            'ubuntu@sw.rpsmarf.ca:/var/www/debs/all/smarf-scs-' + 
             SMARF_VERSION + '.deb',
           'ssh  -o UserKnownHostsFile=/dev/null '
            '-o StrictHostKeyChecking=no '
            '-i ../tools/keys/smarf_dair_keys.pem ubuntu@sw.rpsmarf.ca '
            '"cd /var/www/debs;dpkg-scanpackages '
            '-m all| gzip -9c > all/Packages.gz"'
             ],
            'file_dep': ['../build_output/smarf-scs.deb',
                         '../build_output/smarf_dair_keys.pem'],
            'targets': [],
            'verbosity': 2,
            }


def task_patch():
    """replace the python SCS files on the host referred to by SMARF_HOST"""
    return {
        'actions': 
        ['scp -q '
         '-o UserKnownHostsFile=/dev/null '
         '-o StrictHostKeyChecking=no '
         '-i ../tools/keys/smarf_dair_keys.pem '
         '../build_output/scs_python.zip ubuntu@' + SMARF_HOST + ':/tmp',
         'ssh  -o UserKnownHostsFile=/dev/null '
         '-o StrictHostKeyChecking=no '
         '-i ../tools/keys/smarf_dair_keys.pem '
         'ubuntu@' + SMARF_HOST + 
         ' sudo unzip -o /tmp/scs_python.zip -d /opt/smarf-scs/scs'
         ],
        'file_dep': ['../build_output/scs_python.zip',
                     '../build_output/smarf_dair_keys.pem'],
        'targets': [],
        'verbosity': 2,
           }


def task_patchui():
    """replace UI code for the SCS - not implemented yet"""
    return {'actions': ['echo TBD'],
            'targets': [],
            'verbosity': 2,
            }


def task_restart():
    """restart the SCS on the local node"""
    return {'actions': [
            'sudo service smarf-scs restart',
            ],
            'uptodate': [False],
            'verbosity': 2,
            }


def task_clearlog():
    """Clears local logs"""
    return {'actions': [
            'sudo rm -f /var/log/smarf-scs/*',
            'sudo touch /var/log/smarf-scs/uwsgi.log',
            'sudo chmod 666 /var/log/smarf-scs/uwsgi.log',
            ],
            'uptodate': [False],
            'verbosity': 2,
            }

