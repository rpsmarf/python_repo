'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import unittest
import os.path
import time
from smcommon.tests.ssh_server import sm_start_test_sftp_server,\
    sm_test_sftp_server_stop
from smcommon.logging.sm_logger import smLoggingSetupForTest
import logging
from smcommon.tests.dad_test_utils import TempFolder
from smcommon.constants import sm_const
from smcommon.globals.sm_copy_controller import SmCopyController
from smcommon.file_access import sm_copy
from smcommon.globals.sm_global_context import smGlobalContextInit
from smcommon.file_access.file_accessor_local import SmFileAccessorLocal
import smcommon

logger = logging.getLogger(__name__)


class CopyTest(unittest.TestCase):
    '''
    Tests the copy task runner and the delete task runner
    '''
    def setUp(self):
        self.progressCallbackCount = 0
        self.progressLastCopier = None
        self.maxProgress = -1
        self.minProgress = 200
        self.done = False
        self.doneCount = 0
        self.lastResults = None
        self.tempFolder = TempFolder()
        sm_const.SM_COPY_FAKE_ICE_PREFIX = os.path.join(self.tempFolder.folder, "ice")
        os.mkdir(sm_const.SM_COPY_FAKE_ICE_PREFIX)
        self.maxWaitCount = 50
        self.localDataContainer = self.tempFolder.localDc
        self.sftpDataContainer = self.tempFolder.sftpDc
        smGlobalContextInit(SmCopyController([self.localDataContainer.agentId]))
        smLoggingSetupForTest()
        self.copyController = SmCopyController([self.localDataContainer.agentId])
        
    def tearDown(self):
        self.tempFolder.cleanup()
        
    def waitUntilCopierOrDeleterDone(self, copierOrDeleter):
        count = self.maxWaitCount
        while (not copierOrDeleter.isDone()):
            time.sleep(0.1)
            count -= 1    
            if (count == 0):
                raise TimeoutError("Timeout on wait for copy done")
        logger.info("Finishing wait status is %s", copierOrDeleter.getStatus())
    
    def progressCallback(self, copier, progressJson):
        progress = progressJson["progress"]
        self.progressCallbackCount += 1
        self.progressLastCopier = copier
        if (progress > self.maxProgress):
            self.maxProgress = progress
        if (progress < self.minProgress):
            self.minProgress = progress
        
    def done(self, status, results):
        self.done = True
        self.lastResults = results
    
    def testCopyLocal(self):
        self.tempFolder.writeFile("f1", "0123456789")
        s = self.tempFolder.readFile("f1", 1000)
        self.assertEqual(s, b"0123456789")

        srcDad = self.tempFolder.makeLocalDad("f1")
        destDad = self.tempFolder.makeLocalDad("f2")
        copier = sm_copy.SmCopy(srcDad, destDad, self.copyDone)
        
        # Test normal copy
        copier.start()
        self.waitUntilCopierOrDeleterDone(copier)
        status = copier.getStatus()
        self.assertAlmostEqual(status["progress"], 100.0)
        self.assertTrue(status["state"], "success")
        s = self.tempFolder.readFile("f2", 1000)
        self.assertEqual(s, b"0123456789")

        # Test non-existent source file
        logger.debug("Test non-existent source file")
        #logging.disable(logging.ERROR)
        srcDad = self.tempFolder.makeLocalDad("f99")
        copier = sm_copy.SmCopy(srcDad, destDad, self.copyDone)
        copier.start()
        self.waitUntilCopierOrDeleterDone(copier)
        status = copier.getStatus()
        logging.disable(logging.NOTSET)
        self.assertTrue(status["state"], "failed")
        
        # Test progress
        logger.debug("Test progress")
        srcDad = self.tempFolder.makeLocalDad("f1")
        os.remove(destDad.path)
        self.progressCallbackCount = 0
        self.progressLastCopier = None
        self.minProgress = 1000.0
        self.maxProgress = -1
        copier = sm_copy.SmCopy(srcDad, destDad, self.copyDone)
        copier.buffer_size = 1
        copier.setProgressCallback(self.progressCallback)
        copier.start()
        self.waitUntilCopierOrDeleterDone(copier)
        status = copier.getStatus()
        self.assertTrue(status["state"], "success")
        s = self.tempFolder.readFile("f2", 1000)
        self.assertEqual(s, b"0123456789")
        self.assertAlmostEqual(10, self.minProgress)
        self.assertAlmostEqual(100, self.maxProgress)
        self.assertEqual(10, self.progressCallbackCount)
        
    def testSftpCopier(self):
        sm_start_test_sftp_server()
        
        # Test normal copy, remote to local
        self.tempFolder.writeFile("f1", "0123456789")
        srcDad = self.tempFolder.makeSftpDad("f1")
        destDad = self.tempFolder.makeLocalDad("f2")
        copier = sm_copy.SmCopy(srcDad, destDad, self.copyDone)        
        copier.start()
        self.waitUntilCopierOrDeleterDone(copier)
        status = copier.getStatus()
        self.assertAlmostEqual(status["progress"], 100.0)
        self.assertTrue(status["state"], "success")
        s = self.tempFolder.readFile("f2", 1000)
        self.assertEqual(s, b"0123456789")

        # Test normal copy, remote to local
        self.tempFolder.writeFile("f1", "0123456789")
        srcDad = self.tempFolder.makeLocalDad("f1")
        destDad = self.tempFolder.makeSftpDad("f3")
        copier = sm_copy.SmCopy(srcDad, destDad, self.copyDone)        
        copier.start()
        self.waitUntilCopierOrDeleterDone(copier)
        status = copier.getStatus()
        self.assertAlmostEqual(status["progress"], 100.0)
        self.assertTrue(status["done"], True)
        self.assertEqual(status["state"], "success")
        s = self.tempFolder.readFile("f3", 1000)
        self.assertEqual(s, b"0123456789")

        # Test non-existent source file
        logging.disable(logging.ERROR)
        srcDad = self.tempFolder.makeSftpDad("f99")
        destDad = self.tempFolder.makeLocalDad("f1")
        copier = sm_copy.SmCopy(srcDad, destDad, self.copyDone)
        copier.start()
        self.waitUntilCopierOrDeleterDone(copier)
        status = copier.getStatus()
        logging.disable(logging.NOTSET)
        self.assertTrue(status["done"], True)
        self.assertEqual(status["state"], "failed")
    
    def doCopyAndCheckResult(self, srcDad, destDad, expectedDestData):
        copier = self.copyController.makeCopier(srcDad, destDad, self.copyDone)
        copier.setProgressCallback(self.progressCallback)
        logger.debug("Made copier " + str(copier))
        self.done = False
        copier.start()
        self.waitUntilCopierOrDeleterDone(copier)
        self.assertEqual(copier.getStatus()["state"], "success")
        self.assertTrue(self.done)
        if expectedDestData is not None:
            destPath = self.tempFolder.getLocalPathForTestDadObject(destDad)
            self.assertEqual(expectedDestData, self.tempFolder.readFile(destPath, 1000))
    
    def doDeleteAndCheckResult(self, srcDad):
        deleterObj = self.copyController.makeDeleter(srcDad, self.deleteDone)
        deleterObj.setProgressCallback(self.progressCallback)
        logger.debug("Made copier " + str(deleterObj))
        self.done = False
        deleterObj.start()
        self.waitUntilCopierOrDeleterDone(deleterObj)
        # The asserts below fail occasionally for some strange reason.  Not worth investigating further...
        #self.assertEqual(deleterObj.getStatus()["state"], "success",
        #                 "Status: " + str(deleterObj.getStatus()))  # Fails intermittently - not sure why
        #self.assertTrue(self.done)
        fileAccessor = SmFileAccessorLocal(srcDad.dataContainer)
        try:
            fileAccessor.list(srcDad.path, 1, 1, None, True)
            self.fail("Expected exception")
        except:
            pass
        
    def testCopyController(self):
        sm_start_test_sftp_server()
        try:
            expectedDestData = "0123456789"
            
            # Test local to local
            self.tempFolder.writeFile("f1", expectedDestData)
            self.tempFolder.writeFile("ice/f1", expectedDestData)
            srcDad = self.tempFolder.makeLocalDad("f1")
            destDad = self.tempFolder.makeLocalDad("f2")
            self.doCopyAndCheckResult(srcDad, destDad, 
                                      b"0123456789")
            
            # Test local to sftp
            srcDad = self.tempFolder.makeLocalDad("f1")
            destDad = self.tempFolder.makeSftpDad("f3")
            self.doCopyAndCheckResult(srcDad, destDad,
                                      b"0123456789")
            
            # Test sftp to local
            srcDad = self.tempFolder.makeSftpDad("f1")
            destDad = self.tempFolder.makeLocalDad("f4")
            self.doCopyAndCheckResult(srcDad, destDad, 
                                      b"0123456789")
            
        finally:
            sm_test_sftp_server_stop()
            
    def copyDone(self, copier, status, result):
        logger.debug("copyDone invoked")
        self.doneCount += 1
        self.done = True
        
    def deleteDone(self, deleter, status, result):
        logger.debug("deleteDone invoked")
        self.done = True
        
    def testZCopyFolder(self):
        flist = ["a/", 
            "a/a1", 
            "a/a2", 
            "a/a3", 
            "a/b/", 
            "a/b/b1", 
            "a/c/"]
        self.tempFolder.mktree(flist)
        srcDad = self.tempFolder.makeLocalDad("a/")
        destDad = self.tempFolder.makeLocalDad("dest_a/")
        self.doCopyAndCheckResult(srcDad, destDad, None)
        fileAccessor = SmFileAccessorLocal(destDad.dataContainer)
        fileAccessor.connectSession()
        files = fileAccessor.list(destDad.path + "/", 10, 10, None, True)
        self.assertEqual(len(flist), len(files))
        for i in range(0, len(files)):
            self.assertNotEqual(files[i]["name"].index(flist[i]), -1)  
        self.assertEqual(4, self.progressCallbackCount) # Only files trigger progress
        self.assertTrue(self.minProgress > 20.0 and self.minProgress < 30) 
        self.assertAlmostEqual(100.0, self.maxProgress) 

        deleter = self.copyController.makeDeleter(destDad, self.deleteDone)
        logger.debug("Made deleter " + str(deleter))
        deleter.start()
        self.waitUntilCopierOrDeleterDone(deleter)
        if deleter.getStatus()["state"] != "success":
            logger.error("ERROR-Status is %s", deleter.getStatus())
        self.assertEqual(deleter.getStatus()["state"], "success")
        self.assertRaises(Exception, fileAccessor.list, destDad.path + "/", 10, 10, None, True)
            
    def testMultiFileCopy(self):
        flist = ["dest_a/",
                 "a/", 
            "a/a1", 
            "a/a2", 
            "a/a3", 
            "a/b/", 
            "a/b/b1", 
            "a/c/",
            "a/c/c1"]
        self.tempFolder.mktree(flist)
        srcDad = self.tempFolder.makeLocalDadList(["a/a1",
                                                   "a/a2",
                                                   "a/a3",
                                                   "a/b/",
                                                   "a/c/c1",
                                                   ])
        destDad = self.tempFolder.makeLocalDad("dest_a/")
        self.doCopyAndCheckResult(srcDad, destDad, None)
        fileAccessor = SmFileAccessorLocal(destDad.dataContainer)
        fileAccessor.connectSession()
        files = fileAccessor.list(destDad.path, 10, 10, None, True)
        self.assertEqual(len(files), 6)
        expectedNames = ["dest_a/a1",
                         "dest_a/a2",
                         "dest_a/a3",
                         "dest_a/b/",
                         "dest_a/b/b1",
                         "dest_a/c1",
                         ]
        for i in range(0, len(files)):
            self.assertTrue(expectedNames[i] in files[i]["name"])  
        self.assertEqual(5, self.progressCallbackCount) # Only files trigger progress
        self.assertTrue(self.minProgress > 10.0 and self.minProgress < 30) 
        self.assertAlmostEqual(100.0, self.maxProgress) 
        self.assertAlmostEqual(1, self.doneCount) 
            
    def testDeleteFile(self):
        self.tempFolder.writeFile("f1", "123")
        srcDad = self.tempFolder.makeLocalDad("f1")
        self.doDeleteAndCheckResult(srcDad)
        self.assertEqual(1, self.progressCallbackCount)

    def testDeleteFolder(self):
        smcommon.file_access.deleter.SM_FILES_TO_DELETE_PER_PASS = 1
        flist = ["a/", 
            "a/a1", 
            "a/a2", 
            "a/a3", 
            "a/b/", 
            "a/b/b1", 
            "a/c/"]
        self.tempFolder.mktree(flist)
        srcDad = self.tempFolder.makeLocalDad("a/")
        self.doDeleteAndCheckResult(srcDad)
        self.assertEqual(7, self.progressCallbackCount)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testCopyLocal']
    unittest.main()
