'''
    Django settings for smarfcontrolserver project.

    For more information on this file, see
    https://docs.djangoproject.com/en/1.6/topics/settings/

    For the full list of settings and their values, see
    https://docs.djangoproject.com/en/1.6/ref/settings/

    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''

import os
import sys
import time
import json
from smcommon.logging.sm_logger import smLoggingGetForTest

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Settings to make the output of Django tests parseable by
# Jenkins
TEST_RUNNER = 'xmlrunner.extra.djangotestrunner.XMLTestRunner'
TEST_OUTPUT_VERBOSE = True
TEST_OUTPUT_DESCRIPTIONS = True
TEST_OUTPUT_DIR = 'test-reports'

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '8-1t+g&e6#y2ca2itu4i_)1^p(hf08p)5sd$yegv-qi*ezvxo)'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Set whether we are testing
TESTING = sys.argv[1:2] == ['test']

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

TEMPLATE_DIRS = (
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(os.path.dirname(__file__), 'templates'),  # creates a absolute path
    os.path.join(os.path.dirname(__file__), '../canarie_service/templates'),  # creates a absolute path
)

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'tastypie',
    'sm_controller',
    'canarie_service',
    'corsheaders',
    'guardian',  # Per object authorization support (e.g. resource & task_type perms)
    'actstream'  # Activity stream for recording key actions - must be last
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend', # this is default
    'guardian.backends.ObjectPermissionBackend',
)

ANONYMOUS_USER_ID = -1

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'logging_middleware.sm_log_requests.RequestLoggerMiddleware'
)

ROOT_URLCONF = 'smarfcontrolserver.urls'

WSGI_APPLICATION = 'smarfcontrolserver.wsgi.application'

TASTYPIE_DEFAULT_FORMATS = ['json']

ANONYMOUS_USER_ID = -1

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'mysql_cymysql',
        'NAME': 'rpsmarf',
        'TEST_NAME': 'rpsmarf_' + str(time.time()).split('.')[0],
        'USER': 'rpsmarf_dev',
        'PASSWORD': 'canarie',
        'HOST': 'localhost',
        'PORT': '3306',
        'CONN_MAX_AGE': 0,
    },
}

# Settings for the activity stream app
#    'MANAGER': 'myapp.managers.MyActionManager',
ACTSTREAM_SETTINGS = {
    'FETCH_RELATIONS': True,
    'USE_JSONFIELD': True,
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/scs/static/'

STATIC_ROOT = os.path.join(os.path.dirname(__file__), "static")
# List of finder classes that know how to find static files in
# various locations.

# STATICFILES_FINDERS = (
#    'django.contrib.staticfiles.finders.FileSystemFinder',
#    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
# )


LOGGING = {
               'version': 1,
               'disable_existing_loggers': False,
               'formatters': 
               {
                    'verbose': 
                    {
                        "format": "[%(asctime)s] %(levelname)-8s [%(name)s:%(lineno)s] %(message)s",
                        'datefmt': "%d/%b/%Y %H:%M:%S"
                    },
                    'simple': 
                    {
                        'format': '%(levelname)s %(message)s'
                    },
               },
               'handlers': 
               {
                    'file': 
                    {
                        'level': 'DEBUG',
                        'class': 'logging.FileHandler',
                        'filename': '/tmp/scs.log',
                        'formatter': 'verbose'
                    },
               },
               'loggers': {
                           '': {
                                'handlers': ['file'],
                                'propagate': True,
                                'level': 'DEBUG',
                                },
                           'django': {
                                      'handlers': ['file'],
                                      'propagate': True,
                                      'level': 'INFO',
                                      },
                           }
              }

if (os.path.realpath(__file__) == "/opt/smarf-scs/scs/smarfcontrolserver/settings.py"):
    LOGGING_DEST_FILE = "/var/log/smarf-scs/scs.log"
    if (os.path.exists(os.path.dirname(LOGGING_DEST_FILE)) and os.access(os.path.dirname(LOGGING_DEST_FILE), os.W_OK) and 
      (not os.path.exists(LOGGING_DEST_FILE) or os.access(LOGGING_DEST_FILE, os.W_OK))):
        LOGGING_FILE = "/etc/smarf-scs/logging.conf"
        print("settings.py: Using logging config at " + LOGGING_FILE)
        if (os.path.isfile(LOGGING_FILE)):
            with open(LOGGING_FILE) as json_file:
                LOGGING = json.load(json_file)
    else:
        LOGGING = smLoggingGetForTest()
else:
    LOGGING = smLoggingGetForTest()
  
#CORS Settings
CORS_ORIGIN_ALLOW_ALL = True
CORS_EXPOSE_HEADERS = ('Location',
                       )                      
