/** 
	Contains interfaces to support the monitoring of SMARF remote agents by
	the SMARF control server.    
	
	See <img src="https://rpsmarf.atlassian.net/wiki/download/attachments/3080207/ICE%20Messaging.png">
 */
module smmonicemsgs
{
	
    /**
    	This interface contains the operations to support the remote
    	agent sending heartbeat messages to the control server.
    	*/
    interface SmHeartbeat {
    	/**
    		Sends a heartbeat to the control server.
    		
    		@param agentId The id of the agent generating the heartbeat
    		
    		@param beatCount An incrementing count of the heartbeats sent.
    		If the beatCount drops, this indicates to the control server
    		that the remote agent has restarted.
    		
    		@return An error code from the control server. 0 means all is OK,
    		1 means that the control server has no record of this remote agent. 
    	*/
        int sendHeartbeat(string agentId, int beatCount);
        };
};

/**
	Contains interfaces to support the initiation by the SMARF Control Server (SCS)
	of operations on the SMARF Remote Agents (SRAs). 
		
	See <img src="https://rpsmarf.atlassian.net/wiki/download/attachments/3080207/ICE%20Messaging.png">
	
 */
module smraicemsgs
{
	sequence<byte> FsData;

    exception FsError {
    	int httpCodeToReturn; 
        string reasonCode;
        string path;
        string description; 
    }; 

	/**
		This interface allows the SCS to perform synchronous operation
		on the SRA
		*/
	interface SmRemAgSyncOps {
		/**
			Allows a synchronous operation to be performed on the remote agent
			
			@param opName The name of the operation to perform.  Supported operations
			include:
			<ul>
			<li>TBD </li>
			</ul>
			
			@param opParams A string of JSON-encoded data which is specific to the operation.
			
			@return A string of JSON-encoded data which is specific to the operation.
			*/
		string doOperation(string opName, string opParamsJson);
		};

		
	/**
		This interface allows the SCS to perform asynchronous operation
		on the SRA
		*/
	interface SmRemAgAsyncOps {
		/**
			Starts an asynchronous operation on the remote agent.
			
			Completion and progress are returned via callbacks from [SmRemAgAsyncOpResps]
			
			@param opId The unique string indicating the id tag to be 
			used to refer to this operation

			@param opType The name of the operation to perform.  Supported operations
			include:
			<ul>
			<li>task</li>
			</ul>
			
			@param opParams A string of JSON-encoded data which is specific to the operation.
			*/
		string startOperation(string opId, string opType, string opParamsJson);
		
		/**
			Cancels an asynchronous operation on the remote agent.
			
			This requests that the operation be cancelled.  Note that the 
			operation will have stopped when the "done" API is invoked from 
			[SmRemAgAsyncOpResps]
			
			@param opId The unique string indicating the id tag of the operation
			to be cancelled

			@throws NoSuchOperation if the operation referred to by opId is no 
			longer present
			*/
		void cancelOperation(string opId);
		
		/**
			Gets status information about an asynchronous operation on 
			the remote agent.
						
			@param opId The unique string indicating the id tag of the operation
			whose status is requested

			@return A JSON-encoded data string with the following elements:
			<ul>
			<li>progress - float with the percentage completion or 
			-1 if job cannot calculate progress</li>
			<li>outputSize - long integer with the size of the output or -1 if not trackable</li>
			<li>runtime - integer with the amount of time in seconds that the job has been running</li>
			</ul>
			
			@throws NoSuchOperation if the operation referred to by opId is no 
			longer present
			
			*/
		string getStatusOfOperation(string opId);
		};

	/**
		This interface allows the SCS to receive information about asynchronous 
		operation which are underway on the SRA
		*/
	interface SmRemAgAsyncOpResps {
		/**
			Provides progress information about an asynchronous operation on 
			the remote agent.
						
			@param opId The unique string indicating the id tag of the operation
			whose status is being provided

			@param statusJson A JSON-encoded data string with the following elements:
			<ul>
			<li>progress - float with the percentage completion or 
			-1 if job cannot calculate progress</li>
			<li>outputSize - long integer with the size of the output</li>
			<li>runtime - float with the amount of time that the job has been running</li>
			</ul>
			*/
		void progress (string opId, string progressJson);
		
		/**
			Provides information about an asynchronous operation on 
			the remote agent and indicates that it has completed.
						
			@param opId The unique string indicating the id tag of the operation
			which has completed

			@param status The result of the operations:
			<ul>
			<li>ok</li>
			<li>timeout</li>
			<li>failure</li>
			<li>cancelled</li></ul>
			</ul>
			
			@param respJson A JSON-encoded data string with the elements
			related to the task which completed.
 		*/
		void done(string opId, string status, string respJson);
 
		/**
			Provides an interface for the remote agent to deliver
			stdout and stderr information from the task.
						
			@param opId The unique string indicating the id tag of the operation
			which has produced the output

			@param outputType Typically stdout or stderr
			
			@param outputData The data output from the tool.
 		*/
		void taskOutput(string opId, string outputType, string outputData);
		};

	/**
		This interface allows a requestor (SCS or SRA) to communicate directly with
		an SRA to perform file operations.  
		delete
		File operations include:
		<ul>
		<li>delete file</li>
		<li>read file</li>
		<li>write file</li>
		<li>list files</li>
		</ul>
		*/
	interface SmRemAgFileOps {
		/**
		 	Connects to the filesystem described by the 
		 	dataContainer.  This method returns a fileAccessSessionId
		 	(fasId for short) which is used in subsequent calls
		 	
		 	@param dataContainerJson  JSON representation of the 
		 	datacontainer (e.g. filesystem) to create a connection
		 	to
		 	
		 	@param timeout Float which is the number of seconds
		 	for this file access session to be active.  After
		 	this time the file access session may be closed to
		 	reclaim resources 
		*/
		string connect(string dataContainerJson, float timeout) throws FsError;
		
		/**
			Releases a file access session.
			
			After this call the fasId is invalid
			
			@param fasId The file access session id of the session
			to release
		*/
		void disconnect(string fasId);
		
		/**
			Deletes the files provided in the argument.
			
			Note that recursive operations are not supported to support
			progress.  That is, the deleter must list all files, then
			fire delete requests in chunks.  This allows accurate progress
			reporting.
						
			@param fasId the file access session id from connect()

			@param pathsToDeleteJson A JSON-encoded list of paths to delete
			
			@param The number of files deleted
			*/
		int delete (string fasId, string pathsToDeleteJson) throws FsError;
		
		/**
			Lists information about a folder.
						
			@param fasId the file access session id from connect()

			@param path The path to start listing at

			@param recursiveDepth A JSON-encoded data string with the following elements:
			
			@param maxEntries The number of entries to return
			
			@param lastPathReturned The last path returned.  Entries before this
			and this one are skipped.
			
			@param showHidden Shows hidden files (default is to hide hidden files).
			The files which start with '.' are "hidden.
			
			@return A JSON struct with the following elements:
			<ul>
			<li>more - true if there are more entries to fetch</li>
			<li>files - an array of JSON structs, each with:
			  <ul>
  			  <li>name - path relative to the dadJson</li>
  			  <li>size - path relative to the dadJson</li>
  			  <li>dateOfCreation - date of creation for the file in sec since 1970</li>
  			  <li>permissions - a string containing rwx to indicate readable, 
  			  writable or executable</li>
    		  </ul>
			</ul>
			
			*/
		string list(string fasId, string path, int recursiveDepth, int maxEntries, string lastPathReturned, bool showHidden) throws FsError;

		/**
			This call creates the folder specified.
			
			@param fasId the file access session id from connect()

			@param path the path of the folder to create
			
			@param makeParentFolder if True then any missing parent folders are created
			
			@return json dict with tokens "result"="ok"|"error" and "reason"=<description> 
		 */
		string mkdir(string fasId, string path, bool makeParentFolder) throws FsError;
		
		/**
			This call computes the number of bytes in the folder specified.
			
			@param fasId the file access session id from connect()

			@param path the path of the folder to total
						
			@return json dict with tokens "result"="ok"|"error" and "sizeInBytes"=<size in bytes> 
		 */
		string du(string fasId, string path, bool showHidden) throws FsError;
		
		/**
			This call moves the file or folder specified to the new path specified.
			
			@param fasId the file access session id from connect()

			@param oldpath the original path of the file or folder to move 
			
			@param newpath the new path of the file or folder after the move 
			
			@return json dict with tokens "result"="ok"|"error" and "reason"=<description> 
		 */
		string move(string fasId, string oldpath, string newpath) throws FsError;
		
		/**
			This call opens the file for reading.
			
			@param fasId the file access session id from connect()

			@param dadJson The data access descriptor of the file to open for reading
			
			@return The file id in string form
		 */
		string openForRead(string fasId, string path) throws FsError;
		
		/**
			This call opens the file for writing.
			
			@param fasId the file access session id from connect()

			@param dadJson The data access descriptor of the file to open for writing
			
			@return The file id in string form
		 */
		string openForWrite(string fasId, string path) throws FsError;
		
		/**
			A call to write data to a file.
			
			The file must have been opened with openForWrite().
			
			@param fasId the file access session id from connect()

			@param fileId The fileId of the file to write as returned by openForWrite()
			
			@param data A byte sequence of data to write
		 */
		void writeData(string fasId, string fileId, FsData data) throws FsError;
		
		/**
			A call to read data to a file.
			
			The file must have been opened with openForRead().
			
			@param fasId the file access session id from connect()

			@param fileId The fileId of the file to read as returned by openForRead()
			
			@param maxSize The maximum number of bytes to read.  Note that if the 
			length of the data returned equals maxSize then readData() must be
			called again to read the next part of the file.
			
			@return A byte sequence of the data which was read 
		 */
		FsData readData(string fasId, string fileId, int maxSize) throws FsError;
		
		/**
			Closes the file.  After this is called fileId is no longer valid.
			
			@param fasId the file access session id from connect()

			@param fileId The fileId of the file to close as returned by openForRead()
			or openForWrite()
			
		 */
		void close(string fasId, string fileId) throws FsError;

		};

};