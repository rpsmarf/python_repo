'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Dec 8, 2014

    @author: rpsmarf
'''
import logging
import threading
import time
from sm_controller.models import SmModelResource
from smcommon.exceptions.file_access_exception import SmFileAccessException
from django import db
from sm_controller.api import SmApiResource

logger = logging.getLogger(__name__)


class ScsTmpPurgerThread(threading.Thread):
    
    def __init__(self, purger):
        threading.Thread.__init__(self, name="TmpPurger")
        self.purger = purger

    def run(self):
        expiryTime = self.purger.expiryTimeInSec
        while not self.purger.quit:
            try:
                db.close_connection()                
                time.sleep(self.purger.pollTimeInSec)
                now = time.time()
                expiryTime = now - self.purger.expiryTimeInSec
                self.purger.doOneCheck(expiryTime)
            except:
                logger.exception("Error during temporary file purging")
                time.sleep(10)
            

class ScsTmpFilePurger(object):
    '''
    This class implements a timer to purge files which have been abandoned
    in any resource's .rpsmarf/tmp folder
    '''

    def __init__(self, pollTimeInSec, expiryTimeInSec):
        '''
        Constructor
        '''
        self.pollTimeInSec = pollTimeInSec
        self.expiryTimeInSec = expiryTimeInSec
        self.thread = None
        self.quit = False
        self.checkCount = 0
        
    def purgeResource(self, resource, expiryTime):
        # Open connection
        logger.debug("Checking for files expiring in resource %s", str(resource))
        fileAcc = SmApiResource.makeFileAccFromResource(resource)
        fileAcc.connectSession()
        
        try:
            # List files in .rpsmarf/tmp
            flist = fileAcc.list(".rpsmarf/tmp/", 1, -1, None, True)
            
            # For each file, check if too old
            for f in flist:
                fileDate = f["mtime"]
                if fileDate < expiryTime:
                    logger.debug("Deleting %s", f["name"])
                    if f["isDir"]:
                        sublist = fileAcc.list(f["name"] + "/", -1, -1, None, True)
                        sublist.reverse()
                        fileAcc.deleteFileInfos(sublist)
                    else:
                        fileAcc.deletePaths([f["name"]])
                    
        except Exception as e:
            if isinstance(e, SmFileAccessException):
                if e.reasonCode == "ENOENT":
                    try:
                        fileAcc.mkdir(".rpsmarf/tmp/", True)
                        return
                    except:
                        logger.info("Error creating .rpsmarf/tmp on resource %d", resource.pk)
                        return
            logger.warning("Exception accessing folder .rpsmarf/tmp", exc_info=True)
        finally:
            fileAcc.disconnectSession()
    
    def doOneCheck(self, expiryTime):
        self.checkCount += 1
        resourceList = SmModelResource.objects.all()
        for resource in resourceList:
            if resource.nature == "data":
                self.purgeResource(resource, expiryTime)

    def start(self):
        logger.info("Starting TmpFilePurger task")
        self.thread = ScsTmpPurgerThread(self)
        self.thread.start()
        
    def stop(self):
        ''' 
        This is only used during unit tests which have a small poll time
        so the "join" is OK
        '''
        logger.info("Stopping TmpFilePurger task")
        self.quit = True
        tmp = self.thread.join(2 * self.pollTimeInSec)
        logger.info("Stopped TmpFilePurger task")
        return tmp
        