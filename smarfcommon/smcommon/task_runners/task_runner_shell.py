'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
from asyncio.subprocess import PIPE
import logging
import subprocess
import threading
import time
import selectors

from smcommon.constants import sm_const
from smcommon.constants.sm_const import SM_DATA_CONTAINER_PROTOCOL_LOCAL
from smcommon.globals.sm_global_context import smGlobalContextGet
import paramiko
import os.path
import socket
from subprocess import TimeoutExpired
from smcommon.file_access.data_access_desc import SmDataAccessDescriptor
from smcommon.utils.net_utils import smLoadKeyFile
from smcommon.task_runners.task_runner import SmTaskRunnerBase,\
    TASK_STATE_SUCCESS, TASK_STATE_FAILED, TASK_STATE_CMD_ERROR
from smcommon.exceptions.exec_exception import SmExecutionException
import json
from smcommon.globals.envvar import smCommonGetEnvVar,\
    SM_ENV_VAR_COMMON_GIT_CHECKOUT_COMMAND, SM_ENV_VAR_COMMON_GIT_FETCH_COMMAND,\
    SM_ENV_VAR_COMMON_GIT_FETCH_TAGS_COMMAND,\
    SM_ENV_VAR_COMMON_GIT_RESET_COMMAND


logger = logging.getLogger(__name__)

THREAD_COUNT = 1 # Used to generate unique names for each thread
TIMEOUT_IN_WAIT_FOR_DONE = 1.0
TASK_RUNNER_SSH_CONNECT_COUNT = 0


class SmShellThread(threading.Thread):
    '''
    Implements a thread to run a task and wait for the task to complete
    '''
    
    def __init__(self, parent):
        global THREAD_COUNT
        threading.Thread.__init__(self)
        self.name = "copyThread-" + str(THREAD_COUNT)
        THREAD_COUNT += 1 
        self.parent = parent
        
    def run(self):
        try:
            self.parent._runProcessThread()
        except Exception as e:
            logger.error("SmShellThread failed because of exception", exc_info=True)
            self.parent.done(TASK_STATE_FAILED, SmExecutionException.makeException(e).toDict())


class SmShellOutputHandler(threading.Thread):
    '''
    Implements a thread to run a task and wait for the task to complete
    '''
    
    def __init__(self, shellTask, fd, name):
        global THREAD_COUNT
        threading.Thread.__init__(self)
        self.name = "outputThread-" + str(THREAD_COUNT)
        THREAD_COUNT += 1 
        self.shellTask = shellTask
        self.fd = fd
        self.streamName = name
        self.finished = False
        
    def run(self):
        try:
            for s in self.fd:
                logger.debug("%s: %s", self.streamName, s)
                self.shellTask.annouceTaskOutput(self.streamName, s.decode("utf-8"))
        except:
            logger.debug("Thread %s stopped reading due to exception", self.name)
        finally:
            self.finished = True

    def waitUntilFinished(self, maxTimeInSec):
        endTime = time.time() + maxTimeInSec
        while not self.finished:
            time.sleep(0.1)
            if time.time() > endTime:
                logger.warning("Command thread did not finish during wait for thread %s",
                            self.streamName)
                return
        logger.debug("Command thread finished during wait for thread %s",
                    self.streamName)
        

class SmTaskRunnerShell(SmTaskRunnerBase):
    '''
    This implements a "shell task" which can be run via the 
    task framework.  Note that this class can also running 
    code via SSH.
    '''

    def __init__(self, taskId, doneCallback, taskDesc):
        '''
        Constructor
        '''
        SmTaskRunnerBase.__init__(self, taskId, doneCallback, taskDesc)
        self.phase = "unstarted"
        self.streamCopierList = []
        self.argList = []
        self.inputCopierList = []
        self.outputCopierList = []
        self.process = None
        self.ssh = None
        self.sshChannel = None
        self.processThread = SmShellThread(self)
        self.failed = False
        self.copyController = smGlobalContextGet().copyController
        self.tempDadList = []
        self.timeout = 1000.0  # Should get this from task descriptor
        self.waitTimeBetweenProgressChecks = TIMEOUT_IN_WAIT_FOR_DONE
        self.outputProgressDad = None
        self.outputProgressDadArg = None
        self.outputProgressFileAcc = None
        self.outputProgressMaxSize = None
        outputProgressDict = taskDesc.getTaskParam("outputProgress")
        if outputProgressDict:
            if "stdoutLines" in outputProgressDict:
                self.outputProgressMaxStdoutLines = int(outputProgressDict["stdoutLines"])
            if "outputArg" in outputProgressDict:
                self.outputProgressMaxSize = outputProgressDict.get("maxSize")
                self.outputProgressDadArg = outputProgressDict.get("outputArg")
            
    def _calcProgress(self):
        outputSize = -1
        progress = -1
        
        if self.outputProgressFileAcc:
            logger.debug("Calculating progress maxSize=%d", self.outputProgressMaxSize)
            outputSize = 0
            try:
                duInfo = self.outputProgressFileAcc.du(self.outputProgressDad.path)
                outputSize = duInfo["sizeInBytes"]            
                logger.debug("Calculating progress on folder %s, found %d bytes", 
                             self.outputProgressDad.path, outputSize)
                if self.outputProgressMaxSize is not None:
                    progress = round(outputSize * 100 / self.outputProgressMaxSize, 1)
                    if progress < 3.0:
                        progress = 3.0
            except:
                pass # Ignore failure - maybe file has not been created yet
            
        prog = {}
        if outputSize != -1:
            prog["outputSize"] = outputSize
        if progress != -1:
            prog["progress"] = progress
        return prog

    def readDataStdout(self, file, event):
        try:
            data = file.recv(1000)
            logger.debug("SSH stdout: %s", data)
            self.annouceTaskOutput("stdout", data.decode("utf-8"))
        except socket.timeout:
            pass
    
    def readDataStderr(self, file, event):
        try:
            data = file.recv(1000)
            logger.debug("SSH stderr: %s", data)
            self.annouceTaskOutput("stderr", data.decode("utf-8"))
        except socket.timeout:
            pass

    def doDataCallbacks(self, sel):
        events = sel.select(self.waitTimeBetweenProgressChecks)
        logger.debug("Got %d events", len(events))
        for key, mask in events:
            callback = key.data
            callback(key.fileobj, mask)

    def consumeCmdOutput(self, stdout, stderr):
        try:
            sel = selectors.DefaultSelector()
            stdout.channel.setblocking(False)
            stderr.channel.setblocking(False)
            try:
                sel.register(stdout.channel, selectors.EVENT_READ, self.readDataStdout)
            except:
                logger.debug("Error in stdout FD register")
            try:
                sel.register(stderr.channel, selectors.EVENT_READ, self.readDataStderr)
            except:
                logger.debug("Error in stderr FD register")
            # Wait for the command to terminate
            while not stdout.channel.exit_status_ready():
                logger.debug("Checking status...")
                self.doDataCallbacks(sel)
                time.sleep(0.1) # When there is no output from the shell this loops too fast
                progDict = self._calcProgress()
                self.announceProgress(progDict)
            self.doDataCallbacks(sel)
        except:
            logger.debug("Consume done because of exception - perhaps fd closed", exc_info=True)
            
    def runSyncCommandViaSsh(self, execInfo, args):
        try:
            host = execInfo["host"]
            user = execInfo.get("user")
            port = execInfo.get("port")
            if port is None:
                port = 22
                 
            password = execInfo.get("password") 
            workingDir = execInfo.get("absWorkingDir")
            
            ssh_client = paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            if "keyfile" in execInfo:
                mykey = smLoadKeyFile(execInfo['keyfile'])
            else:
                mykey = None
 
            logger.debug("Connecting to %s:%s as %s", host, port, user)
            ssh_client.connect(host, port=port, pkey=mykey, username=user, password=password)            
            command = "cd " + workingDir + ";" + " ".join(args)
            logger.debug("Running '%s' in dir '%s'", command, workingDir)                        
            _, stdout, stderr = ssh_client.exec_command(command)
            stdout_string = stdout.read()
            stderr_string = stderr.read()
            return stdout.channel.recv_exit_status(), stdout_string, stderr_string
        except Exception as e:
            return (-1, "", str(e))            
    
    def runCommandViaSsh(self, execInfo, cmdList):
        global TASK_RUNNER_SSH_CONNECT_COUNT
        host = execInfo["host"]
        user = execInfo.get("user")
        port = execInfo.get("port")
        if port is None:
            port = 22
         
        password = execInfo.get("password") 
        workingDir = execInfo.get("absWorkingDir")
        try:
            self.ssh = paramiko.SSHClient()
            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            if "keyfile" in execInfo:
                mykey = smLoadKeyFile(execInfo['keyfile'])
            else:
                mykey = None
 
            logger.debug("Connecting to %s:%s as %s", host, port, user)
            self.ssh.connect(host, port=port, pkey=mykey, username=user, password=password)
            logger.debug("Connected to %s", host)
            TASK_RUNNER_SSH_CONNECT_COUNT += 1
            transport = self.ssh.get_transport()
            # Set keepalive so terminating the local command will (eventually) terminate the remote command            
            transport.set_keepalive(1)   
            self.sshChannel = transport.open_session()
            self.sshChannel.get_pty()
            
        except paramiko.AuthenticationException:
            e = SmExecutionException(422, "ECONNREFUSED", "", 
                                     "Authentication failed when connecting to {}. Giving up".format(host))
            self.done(TASK_STATE_CMD_ERROR, e.toDict())
            logger.debug("Authentication failed when connecting to %s", host)
            return -1
        except:
            e = SmExecutionException(422, "ECONNREFUSED", "", 
                                     "Could not connect to {}. Giving up".format(host))
            self.done(TASK_STATE_CMD_ERROR, e.toDict())
            logger.debug("Could not connect to %s. Giving up", host, exc_info=True)
            return -2

        if workingDir is not None and workingDir != ".":
            logger.debug("cd to %s", workingDir)
            _, stdout, stderr = self.sshChannel.exec_command("cd " + workingDir)
            self.consumeCmdOutput(stdout, stderr)
        
        # Send the command (non-blocking)
        cmd = " ".join(cmdList)
        logger.debug("Sending command: %s", cmd)
        self.sshChannel.exec_command(cmd + "\n")
        stdout = self.sshChannel.makefile()
        stderr = self.sshChannel.makefile_stderr()
        logger.debug("Sent command, stdout = %s, stderr = %s", str(stdout), str(stderr))
        self.consumeCmdOutput(stdout, stderr)
            
        logger.debug("Getting exit status...")
        result = stdout.channel.recv_exit_status()
         
        if result != 0:
            e = SmExecutionException(422, "ECMDFAIL", "", 
                                     "Command {} failed with result {}".format(cmd, result))
            self.done(TASK_STATE_CMD_ERROR, e.toDict())

        # Disconnect from the host
        logger.debug("Command done, closing SSH connection, result %d", result)
        self.ssh.close()
        self.sshChannel.close()
        return result
    
    def runSyncCommandLocally(self, execInfo, args):
        try:
            logger.debug("Running command '%s' locally", " ".join(args))
            pipes = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                     cwd=execInfo["absWorkingDir"])
            std_out, std_err = pipes.communicate()
            
            return (pipes.returncode, std_out, std_err)
        except Exception as e:
            return (-1, "", str(e))            
    
    def runCommandLocally(self, execInfo, argList):
        workingDir = execInfo.get("absWorkingDir")
        logger.debug("Starting subprocess with args %s in dir %s", argList, workingDir)
        startTime = time.time()
        self.process = subprocess.Popen(argList, stdout=PIPE, stderr=PIPE, cwd=workingDir)
        result = None
        try:
            self.stdoutHandler = SmShellOutputHandler(self, self.process.stdout, "stdout")
            self.stdoutHandler.start()
            self.stderrHandler = SmShellOutputHandler(self, self.process.stderr, "stderr")
            self.stderrHandler.start()
        
            # Poll for progress
            while ((time.time() - startTime) < self.timeout) and (result is None):
                result = None
                try:
                    logger.debug("Waiting %f time ", self.waitTimeBetweenProgressChecks)
                    result = self.process.wait(timeout=self.waitTimeBetweenProgressChecks)
                except TimeoutExpired:
                    time.sleep(0.1)
                except:
                    logger.warning("Exception while running task with args %s", str(argList), exc_info=True)
                if result is None:
                    # Timeout, calculate progress
                    progDict = self._calcProgress()
                    self.announceProgress(progDict)
                else:
                    if result != 0:
                        e = SmExecutionException(422, "ECMDFAIL", "", 
                                                 "Command {} failed with result {}".format(" ".join(argList), result))
                        self.done(TASK_STATE_CMD_ERROR, e.toDict())

            logger.debug("Process completed with result %d", result)
        
            # Wait until FD handlers finish up. This ensures that any error message
            # is passed back before we send the "done" and freeze the task object
            # on the SCS.
            self.stdoutHandler.waitUntilFinished(3.0)
            self.stderrHandler.waitUntilFinished(1.0)
        finally:
            logger.debug("Closing stdout")
            self.process.stdout.close()
            logger.debug("Closing stderr")
            self.process.stderr.close()
            logger.debug("Done closing")
            
        return result
    
    def makeAbsWorkingDir(self, execInfo, taskDesc):
        '''
        This method converts the working directory information to an absolute path which
        is returned
        '''
        if "workingDir" not in execInfo:
            return "."
        wDirDict = execInfo["workingDir"]
        pathType = wDirDict["type"]
        if pathType == "string":
            return wDirDict["value"]
        
        # Working dir is related to a resource - this is used for tools like
        # zip which should not use an absolute path so they set modifier=filename
        # for the argument and then use the same DAD with modifier=parent
        # for the working dir 
        dad = SmDataAccessDescriptor.makeFromDict(wDirDict["dad"])
        return dad.getPathWithModifier()
    
    def runSyncCommand(self, args):
        '''
        Run a synchronous command (e.g. block until done) and return
        the resulting code and stdout and strerr strings
        '''
        execInfo = self.taskDesc.getTaskParam("execInfo")
        execInfo["absWorkingDir"] = execInfo["workingDir"]["value"]
        if (execInfo.get("protocol") == "ssh"):
            return self.runSyncCommandViaSsh(execInfo, args)
        else:
            return self.runSyncCommandLocally(execInfo, args)     
    
    def runSyncCommandFromTaskDesc(self):
        args = self.taskDesc._taskSpecificParamDict["args"]
        for arg in args:
            if arg["type"] == "string":
                self.argList.append(arg["value"])
            else:
                raise SmExecutionException("Invalid argument type " + arg["type"])

        return self.runSyncCommand(self.argList)     
    
    def runSyncCommandWithCheck(self, args):
        code, stdout_string, stderr_string = self.runSyncCommand(args)
        if code != 0:
            raise SmExecutionException(422, "ESYNC", "", 
                                       "Command {} failed with code {} and output: '{}'/'{}'".format(" ".join(args), 
                                                                                                     code, 
                                                                                                     stdout_string, 
                                                                                                     stderr_string))
        return code, stdout_string, stderr_string

    def syncToVersion(self, execInfo):
        
        # Fetch latest from git repo origin
        args = smCommonGetEnvVar(SM_ENV_VAR_COMMON_GIT_FETCH_COMMAND).split(" ")
        self.runSyncCommandWithCheck(args)
            
        # Throw away any previous checkout
        args = smCommonGetEnvVar(SM_ENV_VAR_COMMON_GIT_RESET_COMMAND).split(" ")
        self.runSyncCommandWithCheck(args)
            
        # If a version was specified sync to it
        if "version" in execInfo and execInfo["version"] != "latest":
            # Get latest tags in case the tag is new and we don't have it
            args = smCommonGetEnvVar(SM_ENV_VAR_COMMON_GIT_FETCH_TAGS_COMMAND).split(" ")
            self.runSyncCommandWithCheck(args)
            
            # Replace $VERSION$ with version requested in argument list
            gitCmd = smCommonGetEnvVar(SM_ENV_VAR_COMMON_GIT_CHECKOUT_COMMAND)
            args = [execInfo["version"] if item == "$VERSION$" else item for item in gitCmd.split(" ")]
            # Checkout the version specified        
            self.runSyncCommandWithCheck(args)
            
    def _runProcessThread(self):
        logger.debug("In task running thread...")
        self.setState("running")
        if self.outputProgressDad:
            self.outputProgressFileAcc = self.copyController.makeFileAccessor(self.outputProgressDad.dataContainer)
            self.outputProgressFileAcc.connectSession()
            logger.debug("Setting up progress tracking for dad %s with maxSize %s", 
                         self.outputProgressDad, self.outputProgressMaxSize)

        execInfo = self.taskDesc.getTaskParam("execInfo")
        absWorkingDir = self.makeAbsWorkingDir(execInfo, self.taskDesc)
        execInfo["absWorkingDir"] = absWorkingDir
                
        if ("versioning" in execInfo):
            try:
                self.syncToVersion(execInfo)
            except SmExecutionException as e:
                logger.exception("Error during syncing to version")
                self.done(TASK_STATE_CMD_ERROR, e.toDict())
                return
                        
        if (execInfo.get("protocol") == "ssh"):
            result = self.runCommandViaSsh(execInfo, self.argList)
        else:
            result = self.runCommandLocally(execInfo, self.argList)
                        
        if result is None:
            # Timeout!
            logger.error("Process timed out!")
            e = SmExecutionException(422, "ETIMEOUT", "", 
                                     "Command %s timed out".format(" ".join(self.argList)))
            self.done(TASK_STATE_CMD_ERROR, e.toDict())
            return

        if result != 0:
            # self.done() already called from runCommand...
            logger.error("Aborting task due to failed return code %d from command %s",
                         result, self.argList)
            return

        # Start copies if needed
        self.setState("cleanup")
        if len(self.outputCopierList) == 0:
            logger.debug("No output copies, calling callback...")
            self.done(TASK_STATE_SUCCESS, {})
        else:
            logger.debug("%d output copies, starting copies...", len(self.outputCopierList))

            for copier in self.outputCopierList:
                logger.debug("Starting output copy back from %s to %s", 
                             copier.srcDad, copier.destDad)
                copier.start()
        
    def isDataWithExec(self, dadInTaskDesc, execInfo):
        if not dadInTaskDesc.isLocal(): 
            return False

        execProtocol = execInfo.get("protocol")
        if execProtocol is None:
            execProtocol = SM_DATA_CONTAINER_PROTOCOL_LOCAL
        if dadInTaskDesc.getProtocol() != execProtocol:
            return False

        if execProtocol == SM_DATA_CONTAINER_PROTOCOL_LOCAL:
            return True
        
        execHost = execInfo.get("host")
        if execHost is None:
            execHost = "localhost"
        if dadInTaskDesc.getHost() != execHost:
            return False
        
        return True
    
    def copyStringToDad(self, s, dad, **kwargs):
        destFileAcc = self.copyController.makeFileAccessor(dad.dataContainer)
        destFileAcc.connectSession()
        try:
            fout = destFileAcc.open(dad.path, "w")
            try:    
                fout.write(s)
            finally:
                fout.close()
        finally:
            destFileAcc.disconnectSession()
    
    def makeTempFolder(self, dad):
        logger.debug("Creating temp data folder: %s", str(dad))
        fileAcc = self.copyController.makeFileAccessor(dad.dataContainer)
        try:
            fileAcc.connectSession()
            fileAcc.mkdir(dad.path, False)
        except:
            logger.exception("Creating temp data folder: %s", str(dad))
        finally:
            fileAcc.disconnectSession()
        logger.debug("Finished creating temp data folder: %s", str(dad))
            
    def makeControlFile(self, tempDad, dadInTaskDesc, localPathPrefix):
        '''
        This method creates a "control file" to be passed to a task for 
        an argument of type dad_file.  This is a file in CSV format with the fields:
        <original name>,<local name>
        where the names are URL encoded.
        '''
        copyCon = smGlobalContextGet().copyController
        destFileAcc = copyCon.makeFileAccessor(tempDad.dataContainer)
        try:
            destFileAcc.connectSession()
            fout = destFileAcc.open(tempDad.path, "wb")
            paths = dadInTaskDesc.path
            for path in paths:
                local_name = os.path.join(localPathPrefix, path)
                data = path + "," + local_name + "\n"
                fout.write(data.encode("utf-8"))
        finally:
            try:
                fout.close()
            except:
                pass
            destFileAcc.disconnectSession()
    
    def start(self):
        # Start copies if needed
        logger.debug("MY_AGENT_ID is %s", sm_const.SM_MY_AGENT_ID)
        args = self.taskDesc._taskSpecificParamDict["args"]
        execInfo = self.taskDesc.getExecInfo()
        firstInputCopierSetup = False
        firstOutputCopierSetup = False
        argIndex = -1
        for arg in args:
            argIndex += 1
            arg_type = arg["type"]
            if arg_type == "string":
                self.argList.append(arg["value"])
            elif arg_type == "script": # Save script in temp file and append name 
                # Make temp name
                tempDad = self.copyController.makeTempDad(False, execInfo)
                self.tempDadList.append(tempDad)                
                self.copyStringToDad(arg["value"], tempDad)
                self.argList.append(tempDad.getPathWithModifier())
            elif arg_type in ["dad", "dad_file"]:
                dadInTaskDesc = self.taskDesc.getArgDad(arg)
                logger.debug("Got DAD %s isLocal() %s", dadInTaskDesc, dadInTaskDesc.isLocal())
                if arg["direction"] == "input":
                    if self.isDataWithExec(dadInTaskDesc, execInfo):
                        if arg_type == "dad_file":
                            tempDad = self.copyController.makeTempDad(False, execInfo)
                            self.makeControlFile(tempDad, dadInTaskDesc, dadInTaskDesc.dataContainer.getPath())
                            pathForArg = tempDad.getPathWithModifier()
                        else:
                            pathForArg = dadInTaskDesc.getPathWithModifier()
                            logger.debug("Modified path for input file is %s", pathForArg)
                            # If it is a relative path then we don't check for existence
                            if pathForArg.startswith("/"):
                                if not os.path.exists(pathForArg):
                                    e = SmExecutionException(422, "ENOENT", pathForArg, 
                                                             "Input path {} not found".format(pathForArg))
                                    self.done(TASK_STATE_FAILED, e.toDict())
                                    self.failed = True
                                    return
                    else:
                        if arg_type == "dad_file":
                            # This is the control file
                            tempDad = self.copyController.makeTempDad(False, execInfo)
                            # This is the temporary destination folder to hold
                            # the files copied
                            tempDadDest = self.copyController.makeTempDad(True, execInfo)
                            # Create folder
                            self.makeTempFolder(tempDadDest)
                            self.makeControlFile(tempDad, dadInTaskDesc, tempDadDest.getAbsPath())                            
                        else:
                            tempDad = self.copyController.makeTempDad(not dadInTaskDesc.isFile(), execInfo)
                            tempDadDest = tempDad
                        pathForArg = tempDad.getPathWithModifier()
                        self.tempDadList.append(tempDad)
                        if tempDad != tempDadDest:
                            self.tempDadList.append(tempDadDest)

                        copier = self.copyController.makeCopier(dadInTaskDesc,
                                                                tempDadDest,
                                                                self.inputCopyDone)
                        if not firstInputCopierSetup:
                            copier.setProgressCallback(self.progressCallback)
                            firstInputCopierSetup = True
                        logger.debug("Starting copy of DAD from [%s] to local DAD[%s] before task start",
                                     str(dadInTaskDesc),
                                     str(tempDad))

                        copier.start()
                        self.inputCopierList.append(copier)
                elif arg["direction"] == "output":
                    if self.isDataWithExec(dadInTaskDesc, execInfo):
                        pathForArg = dadInTaskDesc.getPathWithModifier()
                        # Refuse to overwrite path
                        logger.debug("Checking if path %s exists", pathForArg)
                        if os.path.exists(pathForArg):
                            logger.warning("Returning error EEXISTS because output path already present")
                            e = SmExecutionException(422, "EEXISTS", pathForArg, 
                                                     "File {} already present at output path".format(pathForArg))
                            self.done(TASK_STATE_FAILED, e.toDict())
                            self.failed = True
                            return
                        if argIndex == self.outputProgressDadArg:
                            self.outputProgressDad = dadInTaskDesc
                            
                    else:
                        tempDad = self.copyController.makeTempDad(not dadInTaskDesc.isFile(), 
                                                                  execInfo)

                        pathForArg = tempDad.getPathWithModifier() 
                        logger.debug("Making temp DAD %s", tempDad)
                        if not dadInTaskDesc.isFile():
                            self.makeTempFolder(tempDad)
                        self.tempDadList.append(tempDad)                        
                        copier = self.copyController.makeCopier(tempDad,
                                                                dadInTaskDesc,
                                                                self.outputCopyDone)
                        if not firstOutputCopierSetup:
                            copier.setProgressCallback(self.progressCallback)
                            firstOutputCopierSetup = True
                        self.outputCopierList.append(copier)
                        if argIndex == self.outputProgressDadArg:
                            self.outputProgressDad = tempDad
                            
                self.argList.append(pathForArg)
            elif arg_type == "stream": 
                dadInTaskDesc = self.taskDesc.getArgDad(arg)
                direction = arg["direction"]
                logger.debug("Got stream DAD isLocal() %s - %s", dadInTaskDesc.isLocal(), dadInTaskDesc)
                if direction == "input":
                    # Make named pipe
                    tempDad = self.copyController.makeNamedPipeDad()
                    self.tempDadList.append(tempDad) # Add to clean up list
                    
                    # Put named pipe name into the argument list
                    pathForArg = tempDad.getAbsPath()
                    
                    # Start copier to copy data from DAD to the named pipe
                    copier = self.copyController.makeCopier(dadInTaskDesc,
                                                            tempDad,
                                                            self.streamCopyDone)
                    logger.debug("Starting stream copy of DAD from [%s] to local DAD[%s]",
                                 str(dadInTaskDesc),
                                 str(tempDad))
                    copier.start()
                    self.streamCopierList.append(copier)
                elif direction == "output":
                    # Make named pipe
                    tempDad = self.copyController.makeNamedPipeDad()
                    self.tempDadList.append(tempDad) # Add to clean up list
                    
                    # Put named pipe name into the argument list
                    pathForArg = tempDad.getAbsPath()
                    
                    # Start copier to copy data from named pipe to the DAD
                    copier = self.copyController.makeCopier(tempDad,
                                                            dadInTaskDesc,
                                                            self.streamCopyDone)
                    copier.setProgressCallback(self.progressCallback)
                    logger.debug("Starting stream copy of DAD from [%s] to DAD[%s]",
                                 str(tempDad),
                                 str(dadInTaskDesc))
                    copier.start()
                    self.streamCopierList.append(copier)
                else:
                    raise Exception("Invalid direction %s", direction)
                self.argList.append(pathForArg)
            
        # Start thread with shell command
        if len(self.inputCopierList) == 0:
            logger.debug("No non-local DADs, so starting task immediately")
            self.processThread.start()
    
    def cancel(self):
        for copier in self.inputCopierList:
            copier.cancel()
        for copier in self.outputCopierList:
            copier.cancel()
        if self.process:
            self.process.kill()
        if self.ssh:
            self.ssh.close()
        if self.sshChannel:
            self.sshChannel.close()

    def progressCallback(self, copier, progress):
        logger.debug("Shell task progress for %s to %s", self.taskId, progress)
        self.announceProgress(progress)
        
    def streamCopyDone(self, copier, state, resultDict):
        logger.debug("Got stream copy done with state %s for %s to %s", 
                     state, copier.srcDad, copier.destDad)
        if state != TASK_STATE_SUCCESS:
            logger.error("Stream file copy failed with result %s!", json.dumps(resultDict))
            e = SmExecutionException(422, "EREMOTEIO",
                                     "", 
                                     "Remote stream copy failed")
            self.done(TASK_STATE_FAILED, e.toDict())
            self.failed = True
            return
            
    def inputCopyDone(self, copier, state, resultDict):
        logger.debug("Got input copy done with state %s for %s to %s", 
                     state, copier.srcDad, copier.destDad)
        if state != TASK_STATE_SUCCESS:
            logger.error("Input file copy failed!")
            self.done(state, resultDict)
            self.failed = True
            return
            
        for copier in self.inputCopierList:
            if not copier.isDone():
                return
            
        if not self.failed:
            logger.debug("Starting process because all input copying done...")
            self.processThread.start()

    def outputCopyDone(self, copier, state, resultDict):
        logger.debug("Got output copy done with state %s for %s to %s", 
                     state, copier.srcDad, copier.destDad)
        if state != TASK_STATE_SUCCESS:
            logger.error("Output file copy failed!")
            self.done(state, resultDict)
            self.failed = True
            return

        for copier in self.outputCopierList:
            if not copier.isDone():
                return
            
        if not self.failed:
            logger.debug("Done shell task because all output copying done.")
            self.done(TASK_STATE_SUCCESS, {})
      
    def cleanup(self):
        fileAcc = None
        try:
            for dad in self.tempDadList:
                if dad.autoDelete:
                    logger.debug("Deleting %s", dad)
                    time.sleep(0.1)  # Wait to allow streams to consume any data in pipes
                    fileAcc = self.copyController.makeFileAccessor(dad.dataContainer)
                    fileAcc.connectSession()
                    if dad.isFile():
                        fileAcc.deletePaths([dad.path])
                    else:
                        flist = fileAcc.list(dad.path + "/", 10, 1000, None, True)
                        fileAcc.deleteFileInfos(reversed(flist))
                    fileAcc.disconnectSession()
                    fileAcc = None
                
            if self.outputProgressFileAcc:
                self.outputProgressFileAcc.disconnectSession()
                
            for copier in self.streamCopierList:
                copier.cancel()
                
        except:
            logger.error("Failure during cleanup of post-copy temp files", exc_info=True)
            if fileAcc:
                fileAcc.disconnectSession()
            