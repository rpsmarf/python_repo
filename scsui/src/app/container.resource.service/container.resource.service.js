angular.module( 'rpsmarf.container.resource.service', [
 'rpsmarf.config',
 'restangular',
 'ui.bootstrap',
 'rpsmarf.alert',
 'rpsmarf.mkdir',
 'rpsmarf.upload',
 'rpsmarf.rename',
 'rpsmarf.confirm',
 'rpsmarf.constants'
])
.service('SmContainerResource', [ 'DEBUG', 'Restangular', '$modal', 'ROOT_FOLDER_ENTRY', '$window', function(DEBUG, Restangular, $modal, ROOT_FOLDER_ENTRY, $window){

	var resourceApi = Restangular.one('resource/');
	var rootFolderEntry = angular.copy(ROOT_FOLDER_ENTRY);

	var download = function(selectedResource){
		var entry = selectedResource.selectedEntry;
		var modalInstance, body,entryPath;
		if(entry.mode.indexOf('r') < 0 ){
			$modal.open({
				templateUrl: 'alert/alert.tpl.html',
				controller: 'AlertCtrl',
				resolve: {
					title: function(){return 'Error';},
					message: function(){return 'Error downloading '+ entry.basename + ' due to:<br>Permission denied';}
				}
			});
		}
		else{
			if(entry.isDir){
				modalInstance = $modal.open({
					templateUrl: 'confirm/confirm.tpl.html',
					controller: 'ConfirmCtrl',
					resolve: {
						title: function(){return "Confirm download";},
						body: function(){return "Do you want to download <strong>"+entry.basename+"</strong>? It will be downloaded as a zip file.";}
					}

				});

				modalInstance.result.then(function(){
					//Remove trailing slash as it will be URI encoded when it does not need to be
					entryPath = entry.name.substring(0, entry.name.length-1);
					var baseUrl = Restangular.configuration.baseUrl;
					baseUrl = baseUrl.substring(baseUrl.length-1) === '/' ? baseUrl.substring(0,baseUrl.length-1) : baseUrl;
					$window.open(baseUrl+'/resource/'+selectedResource.id+'/'+encodeURIComponent(entryPath)+'/download/');
					//Restangular.one('resource',selectedResource.id+'/'+ encodeURIComponent(entryPath) +'/download/').get().then(function(response){
						//var taskInfo = response.taskPath;
						//var taskId = taskInfo.split('/')[3];
						//var zipFilePath = response.zipFilePath;
						//return {taskObj: Restangular.one('task',taskId+'/'), taskName:'Preparing folder download', interactive: false, additionalInfo: {zipFilePath: zipFilePath, downloadFolder:true, resourceId: selectedResource.id }};
					//});
				}, function(){
					console.log('folder not downloaded');
				});
				
			}
			else {
				modalInstance = $modal.open({
					templateUrl: 'confirm/confirm.tpl.html',
					controller: 'ConfirmCtrl',
					resolve: {
						title: function(){return "Confirm download";},
						body: function(){return "Do you want to download <strong>"+entry.basename+"</strong>?";}
					}

				});

				modalInstance.result.then(
				function (status) {    
					var baseUrl = Restangular.configuration.baseUrl;
					baseUrl = baseUrl.substring(baseUrl.length-1) === '/' ? baseUrl.substring(0,baseUrl.length-1) : baseUrl;
					$window.open(baseUrl+'/resource/'+selectedResource.id+'/'+encodeURIComponent(entry.name)+'/download/');
					console.log('Downloaded!');
				},
				function(status){
					console.log('not downloaded');
				});
			}
			

			
		}
		
	};

	var deleteSelection = function(selectedResource){
		var entry = selectedResource.selectedEntry;
		var message;

		if(entry.isDir){
			message = "Are you sure you want to delete <strong>"+entry.basename.substring(0,entry.basename.length-1)+"</strong> and all of its contents? (Cannot be undone)";
		}
		else{
			message = "Are you sure you want to delete <strong>"+entry.basename+"</strong>? (Cannot be undone)";
		}
		console.log(entry);
		var modalInstance = $modal.open({
			templateUrl: 'confirm/confirm.tpl.html',
			controller: 'ConfirmCtrl',
			resolve: {
				title: function(){return "Confirm deletion";},
				body: function(){return message;}
			}

		});

		return modalInstance.result.then(
		function (status) {    
			console.log('deleting!');
			var type;
			if(entry.isDir){

				return Restangular.one('resource/'+selectedResource.id+'/'+encodeURIComponent(entry.name.substring(0,entry.name.length-1)),'deleterecursive/').get().then(
				function(response){
					var taskInfo = response.taskPath;
					var taskId = taskInfo.split('/')[3];
					selectedResource.selectedEntry = null;
					refreshEntries(selectedResource.entries, '/',selectedResource,  selectedResource.id);

					return {taskObj: Restangular.one('task',taskId+'/'), taskName:'Deletion Task', interactive: false};
				},
				function(response){
					$modal.open({
						templateUrl: 'alert/alert.tpl.html',
						controller: 'AlertCtrl',
						resolve: {
							message: function(){return 'Error deleting '+ entry.basename+':<br>'+response.data.description;},
							title: function(){return 'Error';}
						}
					});

					return null;
				});
			}
			else{
				return Restangular.one('resource/'+selectedResource.id+'/'+ encodeURIComponent(entry.name),'file/').remove().then(
				function(){
					selectedResource.selectedEntry = null;
					refreshEntries(selectedResource.entries, '/',selectedResource,  selectedResource.id);
					return null;
				},
				function(response){
					$modal.open({
						templateUrl: 'alert/alert.tpl.html',
						controller: 'AlertCtrl',
						resolve: {
							message: function(){return 'Error deleting '+ entry.basename+':<br>'+response.data.description;},
							title: function(){return 'Error';}
						}
					});
					return null;
				});
			}
			
			

		},
		function(status){
			console.log('delete cancelled');
		});
	};

	

	var mkdir = function(selectedResource){
		var entryPath = selectedResource.selectedEntry ? selectedResource.selectedEntry.name.substring(0,selectedResource.selectedEntry.name.length-1) : '';
		var modalInstance = $modal.open({
			templateUrl: 'mkdir/mkdir.tpl.html',
			controller: 'MkdirCtrl',
			resolve: {
				action: function(){ return 'mkdir';}
			}
		});

		modalInstance.result.then(
		function(folderName){
			Restangular.one('resource/'+selectedResource.id+'/'+entryPath,encodeURIComponent(folderName)+'/mkdir/').get().then(
			function(){
			selectedResource.selectedEntry= null;
			refreshEntries(selectedResource.entries, '/',selectedResource,  selectedResource.id);
			},
			function(response){
				$modal.open({
					templateUrl: 'alert/alert.tpl.html',
					controller: 'AlertCtrl',
					resolve: {
						message: function(){return 'Could not make folder '+ folderName+':<br>'+response.data.description;},
						title: function(){return 'Error';}
					}
				});
			});
		},
		function(status){
			console.log('mkdir cancelled');
		});
	};

	var upload = function(selectedResource){
		var modalInstance,json, percentageUsed;

		if(selectedResource.full){
			$modal.open({
				templateUrl: 'alert/alert.tpl.html',
				controller: 'AlertCtrl',
				resolve: {
					title: function(){ return 'Error'; },
					message: function(){ return selectedResource.name + ' is full. Cannot perform upload.';}
				}
			});
			return;
		}
		else if(selectedResource.warn){
			json = JSON.parse(selectedResource.capacityJson);
			percentageUsed = (json.diskUsed/json.diskUsedFailThreshold*100).toFixed(0);
			modalInstance = $modal.open({
				controller: 'ConfirmCtrl',
				templateUrl: 'confirm/confirm.tpl.html',
				resolve: {
					title: function(){ return 'Warning';},
					body: function(){ return 'Destination folder is at '+ percentageUsed +'% capacity. Do you want to continue?';}
				}
			});

		}

		if(modalInstance){
			modalInstance.result.then(function(){
				completeUpload(selectedResource);
			});
		}
		else{
			completeUpload(selectedResource);
		}
		
	};

	var completeUpload = function(selectedResource){
		var selectedEntry = selectedResource.selectedEntry === null ? '' : selectedResource.selectedEntry.name;
		var baseUrl = Restangular.configuration.baseUrl;
		baseUrl = baseUrl.substring(baseUrl.length-1) === '/' ? baseUrl : baseUrl+'/';
		var url = baseUrl+'resource/'+selectedResource.id+'/'+selectedEntry;
		modalInstance = $modal.open({
			templateUrl: 'upload/upload.tpl.html',
			controller: 'UploadCtrl',
			resolve: {
				url: function(){return url;}
			}
		});
	};

	var refresh = function(selectedResource){
		selectedResource.selectedEntry= null;
		refreshEntries(selectedResource.entries, '/',selectedResource,  selectedResource.id);
	};

	var refreshEntries = function(entries, parentName, parentObj, resourceid){
		var entry, root;
		resourceApi.getList(resourceid+parentName+'list/').then(function(objects){
			root = angular.copy(rootFolderEntry);
			root.selected = false;
			objects = objects.plain();
			objects.unshift(root);
			parentObj.entries = objects;
		});
		

		
	};

	var rename = function(selectedResource){
		var selectedEntry = selectedResource.selectedEntry;
		selectedEntry = selectedEntry.isDir ? selectedEntry.name : selectedEntry.name+'/';
		var modalInstance = $modal.open({
			controller: 'RenameCtrl',
			templateUrl: 'rename/rename.tpl.html',
			resolve: {
				originalName: function(){ return selectedResource.selectedEntry.isDir ? selectedResource.selectedEntry.name.substring(0,selectedResource.selectedEntry.name.length-1) : selectedResource.selectedEntry.name;}
			}
		});

		modalInstance.result.then(function(entry){
			Restangular.one('resource/'+selectedResource.id+'/'+selectedEntry+'rename/?newName='+encodeURIComponent(entry.name)).get().then(function(response){
				console.log('renamed');
				refreshEntries(selectedResource.entries, '/',selectedResource,  selectedResource.id);
			}, function(response){
				$modal.open({
					controller: 'AlertCtrl',
					templateUrl: 'alert/alert.tpl.html',
					resolve: {
						title: function(){ return 'Error';},
						message: function(){ return 'Could not rename ' + selectedResource.selectedEntry.basename + ':<br>'+response.data.description; }
					}
				});
			});
		}, function(){
			console.log('rename cancelled');
		});
	};

	var editFile = function(selectedResource){
		var entry = selectedResource.selectedEntry;
		var fileName = entry.basename;
		var url = 'resource/'+selectedResource.id+'/'+encodeURIComponent(entry.name)+'/'; 
		if(entry.mode.indexOf('r') < 0 ){
			$modal.open({
				templateUrl: 'alert/alert.tpl.html',
				controller: 'AlertCtrl',
				resolve: {
					title: function(){return 'Error';},
					message: function(){return 'Error editing '+ entry.basename + ' due to:<br>Permission denied';}
				}
			});
		}
		else {
			$modal.open({
				controller: 'FileEditorCtrl',
				templateUrl: 'file.editor/file.editor.tpl.html',
				resolve: {
					file: function(){ return {name:fileName, url:url};}
				},
				size: 'lg'
			});
		}
		
	};

	var zip = function(selectedResource){
		var entry = selectedResource.selectedEntry, name, modalInstance;
		var parentFolder = entry.isRoot ? '' : entry.name.substring(0,entry.name.indexOf(entry.basename));
		if(!entry){
			entry = {name: '', mode:'rwx'};
			name = 'the root folder';
		}
		else {
			name = entry.basename;
		}

		if(entry.mode.indexOf('r') < 0){
			modalInstance = $modal.open({
				templateUrl: 'alert/alert.tpl.html',
				controller: 'AlertCtrl',
				resolve: {
					title: function(){return "Error";},
					message: function(){return "Error occured while trying to zip " + name + ":<br>Permission denied";}
				}

			});

			return null;
		}
		modalInstance = $modal.open({
			templateUrl: 'mkdir/mkdir.tpl.html',
			controller: 'MkdirCtrl',
			resolve: {
				action: function(){return "zip";}
			}

		});

		return modalInstance.result.then(function(fileName){
			return Restangular.one('resource',selectedResource.id+'/'+ (entry.isDir ? encodeURIComponent(entry.name.substring(0,entry.name.length-1))+'/' : encodeURIComponent(entry.name) +'/')+'zip/?zipFilePath='+encodeURIComponent(parentFolder+fileName)).get().then(function(response){
				var taskInfo = response.taskPath;
				var taskId = taskInfo.split('/')[3];
				return {taskObj: Restangular.one('task',taskId+'/'), taskName:'Zip', interactive: false};
			});
		});
	};

	var unzip = function(selectedResource){
		var entry = selectedResource.selectedEntry, modalInstance, parentFolder;
		parentFolder = entry.isRoot ? '' : entry.name.substring(0,entry.name.indexOf(entry.basename));

		if(entry.mode.indexOf('r') < 0){
			modalInstance = $modal.open({
				templateUrl: 'alert/alert.tpl.html',
				controller: 'AlertCtrl',
				resolve: {
					title: function(){return "Error";},
					message: function(){return "Error occured while trying to unzip " + entry.basename + ":<br>Permission denied";}
				}

			});

			return null;
		}

		modalInstance = $modal.open({
			templateUrl: 'mkdir/mkdir.tpl.html',
			controller: 'MkdirCtrl',
			resolve: {
				action: function(){return 'unzip';}
			}

		});

		return modalInstance.result.then(function(folderName){
			return Restangular.one('resource',selectedResource.id+'/'+encodeURIComponent(entry.name)+'/unzip/?zipFilePath='+encodeURIComponent(parentFolder+folderName)).get().then(function(response){
				var taskInfo = response.taskPath;
				var taskId = taskInfo.split('/')[3];
				return {taskObj: Restangular.one('task',taskId+'/'), taskName:'Unzip', interactive: false};
			});
		});
	};

	return {
			download: download,
			deleteSelection: deleteSelection,
			mkdir: mkdir,
			upload: upload,
			refresh: refresh,
			rename: rename,
			editFile: editFile,
			zip: zip,
			unzip: unzip
			};
}])
;


