angular.module('rpsmarf.interval.service',[

])
.service('SmInterval', ['$interval',function($interval){

	var intervals = [];

	var addInterval = function(interval){
		intervals.push(interval);
	};

	var stopAllIntervals = function(){
		for(var i = intervals.length-1; i>=0; i--){
			$interval.cancel(intervals[i]);
		}

		intervals = [];
	};

	var getNumberOfIntervals = function(){
		return intervals.length;
	};

	return {addInterval: addInterval, stopAllIntervals: stopAllIntervals, getNumberOfIntervals:getNumberOfIntervals};
}]);