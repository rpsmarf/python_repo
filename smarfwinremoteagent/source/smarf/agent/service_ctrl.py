import logging
import subprocess
import sys
from time import sleep

import win32service as ws

logger = logging.getLogger(__name__)


class Service(object):
    STATUS_UNINSTALLED = 'UNINSTALLED'
    STATUS_STOPPED = 'STOPPED'
    STATUS_STARTING = 'STARTING'
    STATUS_STOPPING = 'STOPPING'
    STATUS_RUNNING = 'RUNNING'
            
    def __init__(self, name, executable, start_args=(), stop_args=()):
        self.name = name
        self.executable = executable
        self.start_args = start_args
        self.stop_args = stop_args
        self.service = ServiceController(name)

    def start(self, *args):
        arguments = [self.executable]
        arguments.extend(self.start_args)
        arguments.append(self.name)
        arguments.extend(args)

        # Remove existing service
        if self.status() != self.STATUS_UNINSTALLED:
            self.stop()

        rc = subprocess.call(arguments)

        try:
            self.service.fetch_status(ServiceController.SERVICE_STOPPED)
            self.service.start(ServiceController.TYPE_MANUAL)
            self.service.fetch_status(ServiceController.SERVICE_RUNNING)
        except Exception as e:
            logger.info('%s service failed to start' % self.name)
            logger.exception(e)

            
    def stop(self, *args):
        if self.service.status() != 'STOPPED':
            try:
                self.service.stop()
                self.service.fetch_status(ServiceController.SERVICE_STOPPED)        
            except Exception as e:
                logger.exception(e)

        # Remove service process
        arguments = [self.executable]
        arguments.extend(self.stop_args)
        arguments.append(self.name)
        arguments.extend(args)
        rc = subprocess.call(arguments)


    def status(self):
        return self.service.status()
        

class ServiceContext(object):
    def __init__(self, name):
        self.name = name
        self.scmhandle = None 
        self.handle = None
        
    def __enter__(self):
        try:
            self.scmhandle = ws.OpenSCManager(None, None, ws.SC_MANAGER_ALL_ACCESS)
            self.handle = ws.OpenService(self.scmhandle, self.name, ws.SERVICE_ALL_ACCESS)
        except:
            if self.__exit__(*sys.exc_info()):
                pass
            else:
                raise
            
        return self.handle
    
    def __exit__(self, exc_type, exc_value, traceback):
        if self.handle: ws.CloseServiceHandle(self.handle)
        if self.scmhandle: ws.CloseServiceHandle(self.scmhandle)


class ServiceController(object):
    TYPE_BOOT = 0
    TYPE_SYSTEM = 1
    TYPE_AUTOMATIC = 2
    TYPE_MANUAL = 3
    TYPE_DISABLED = 4
        
    SERVICE_STOPPED = ws.SERVICE_STOPPED
    SERVICE_STARTING = ws.SERVICE_START_PENDING
    SERVICE_STOPPING = ws.SERVICE_STOP_PENDING
    SERVICE_RUNNING = ws.SERVICE_RUNNING
    SERVICE_UNINSTALLED = Service.STATUS_UNINSTALLED 
 
    STATUS_STRINGS = {
        SERVICE_STOPPED: Service.STATUS_STOPPED,
        SERVICE_STARTING: Service.STATUS_STARTING,
        SERVICE_STOPPING: Service.STATUS_STOPPING,
        SERVICE_RUNNING: Service.STATUS_RUNNING,
    }    
     
        
    def __init__(self, name):
        self.name = name
 
    def start(self, type):
        with ServiceContext(self.name) as handle:
            ws.ChangeServiceConfig(handle, ws.SERVICE_NO_CHANGE, type, ws.SERVICE_NO_CHANGE, None, None, 0, None, None, None, self.name)
            ws.StartService(handle, None)
 
    def stop(self):
        with ServiceContext(self.name) as handle:
            ws.ControlService(handle, ws.SERVICE_CONTROL_STOP)
 
    def status(self):
        status_string = ServiceController.SERVICE_UNINSTALLED
        try:
            with ServiceContext(self.name) as handle:
                stat = ws.QueryServiceStatus(handle)
                if stat[1] in self.STATUS_STRINGS:
                    status_string = self.STATUS_STRINGS[stat[1]]
        except:
            pass
 
        return status_string
 
    def status_string(self, status):
        return self.STATUS_STRINGS.get(status, '')
 
    def fetch_status(self, status):
        with ServiceContext(self.name) as handle:
            while True:
                stat = ws.QueryServiceStatus(handle)
                if stat[1] == status:
                    break
 
                sleep(1)
 
    def get_name(self):
        return self.name    
