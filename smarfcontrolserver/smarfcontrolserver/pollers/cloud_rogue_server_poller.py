'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Dec 8, 2014

    @author: rpsmarf
'''
import logging
import threading
import time
from sm_controller.models import SmModelCloudServer, SmModelCloudAccount,\
    SmModelContainer
from django import db
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_STATE_STOPPED,\
    SCS_CLOUDIF_STATE, SCS_CLOUDIF_TYPE, SCS_CLOUDIF_AWS_PARAM_REGION_NAME,\
    SCS_CLOUDIF_ID
from smarfcontrolserver.cloudif.cloudif_factory import scsCloudServerIfFactoryGet
import json

logger = logging.getLogger(__name__)


class ScsCloudRogueServerPollerThread(threading.Thread):
    
    def __init__(self, ageOutObject):
        threading.Thread.__init__(self, name="CloudRogueServerPoller")
        self.ageOutObject = ageOutObject

    def run(self):
        while not self.ageOutObject.quit:
            try:
                db.close_connection()
                time.sleep(self.ageOutObject.pollTimeInSec)
                self.ageOutObject.doOneCheck()
            except:
                logger.exception("Error during cloud state poller thread")
                time.sleep(10)
            

class ScsCloudRogueServerPoller(object):
    '''
    This class implements a poller to find running cloud servers check if they
    exist in the Django model and are supposed to be running.  If not they are terminated
    (with extreme prejudice!).
    '''

    def __init__(self, pollTimeInSec):
        '''
        Constructor
        '''
        self.pollTimeInSec = pollTimeInSec
        self.thread = None
        self.quit = False
        self.checkCount = 0

    def doOneCheck(self):
        self.checkCount += 1
        # Make region list
        regionList = []
        for container in SmModelContainer.objects.all():
            try:
                parametersJsonDict = json.loads(container.parametersJson)
                if SCS_CLOUDIF_AWS_PARAM_REGION_NAME in parametersJsonDict:
                    regionList.append(parametersJsonDict[SCS_CLOUDIF_AWS_PARAM_REGION_NAME])
            except:
                pass  # Some parametersJson fields are not JSON
              
        # For each account, list the servers in each region   
        terminate_list = []
        for account in SmModelCloudAccount.objects.all():
            cloud_account_info = json.loads(account.accountInfoJson)
            cloud_info = {SCS_CLOUDIF_TYPE: account.cloud.cloud_type}
            for region in regionList:
                cloud_info[SCS_CLOUDIF_AWS_PARAM_REGION_NAME] = region
                cloudIfObj = scsCloudServerIfFactoryGet().getCloudServerIf(cloud_info, cloud_account_info)
                # Some cloud objects may only be used for storage so they have no cloudServerIf 
                # and we need to skip them in checking for rogue servers
                if cloudIfObj is None:
                    continue
                server_list = cloudIfObj.get_server_list()
                for server in server_list:
                    if server[SCS_CLOUDIF_STATE] != SCS_CLOUDIF_STATE_STOPPED:
                        # Check the Django objects
                        server_id = server[SCS_CLOUDIF_ID]
                        try:
                            cloud_server = SmModelCloudServer.objects.get(server_id=server_id)
                            if cloud_server.state == SCS_CLOUDIF_STATE_STOPPED:
                                logger.warning("Server %s is in state %s in DB.  " +
                                               "Cloud management says it is in state %s, terminating", 
                                               server_id, cloud_server.state, server[SCS_CLOUDIF_STATE])
                                terminate_list.append(server_id)
                        except:
                            logger.warning("Server %s not found in DB, terminating", server_id)
                            # Must terminate
                            terminate_list.append(server_id)
                    
                for server_id in terminate_list:
                    cloudIfObj.terminate_server(server_id)
                    
    def start(self):
        logger.info("Starting CloudRogueServerPoller task")
        self.thread = ScsCloudRogueServerPollerThread(self)
        self.thread.start()
        
    def stop(self):
        ''' 
        This is only used during unit tests which have a small poll time
        so the "join" is OK
        '''
        logger.info("Stopping CloudRogueServerPoller task")
        self.quit = True
        tmp = self.thread.join(2 * self.pollTimeInSec)
        logger.info("Stopped CloudRogueServerPoller task")
        return tmp
        