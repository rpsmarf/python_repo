import json
import os
import sys
from scripts.scs_obj_common import scsScriptGetDefaultUrl
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_AWS_PARAM_IMAGE_ID,\
    SCS_CLOUDIF_AWS_PARAM_REGION_NAME, SCS_CLOUDIF_AWS_PARAM_INSTANCE_TYPE,\
    SCS_CLOUDIF_AWS_PARAM_KEYPAIR, SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID,\
    SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY, SCS_CLOUDIF_DYNAMIC_SERVER
from smarfcontrolserver.init.envvar import SCS_ENV_VAR_TEST_AWS_KEY_ID,\
    scsGetEnvVar, SCS_ENV_VAR_TEST_AWS_SECRET_KEY, scsLoadAwsKeys
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath, scsGetToJson
from scripts.scs_obj_create import scsCreateObjectPath

try:
    os.mkdir("/tmp/src")
except:
    pass
try:
    os.mkdir("/tmp/dest")
except:
    pass

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

root_path = scsGetObjectPath("user", "username=rpsmarf")
user_a = scsGetObjectPath("user", "username=a")
user_b = scsGetObjectPath("user", "username=b")

cloud_agent_data_path = scsGetObjectPath("agent", "name_key=cloud_file_agent")
rt_storage_path = scsGetObjectPath("resource_type", "name_key=generic_storage")

cloud_container_path = scsCreateObjectPath("container", 
                                           {"name": "Cloud Data Container", 
                                            "name_key": "cloud_data_container",
                                            "containerUrl": "local://localhost/opt/smarf-demos/demos/data/",
                                            "description": 
                                            "This container provides access to data for demonstrating the platform",
                                            "agent": cloud_agent_data_path,
                                            "status": "up"})

rt_photo_repo_data_path = scsCreateObjectPath("resource_type",
                                              {"name": "Photo Repository Resource Type",
                                               "name_key": "photo_repo", 
                                               "nature": "data",
                                               "description": 
                                               "This is the type of resources which run code on compute servers"})


res_path = scsCreateObjectPath("resource", {"name": "JPEG Photo Repository", 
                                            "name_key": "jpeg_photo_repo",
                                            "resource_type": rt_photo_repo_data_path,
                                            "owner": root_path,
                                            "container": cloud_container_path,
                                            "parametersJson": json.dumps({"folder": "photos/"}),
                                            "description": "This is a photo repository of images from the web'"
                                            })
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=rw")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=r")


rt_cloud_compute_path = scsCreateObjectPath("resource_type", 
                                            {"name": "Cloud Compute Resource Type", 
                                             "nature": "compute",
                                             "description":    
                                             "This is the type of resources which run code on compute servers"})
CLOUD_ACCOUNT_SCHEMA = {
                        "$schema": "http://json-schema.org/draft-04/schema#",
                        "title": "Cloud Account parametersJson Schema",
                        "additionalProperties": False,
                        "type": "object",
                        "properties": {
                                       SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID: 
                                       {"type": "string",
                                        "title": "AWS Key Id",
                                        "description": 
                                        "This is the key id from the 'Security Credentials' page in AWS"},
                                       SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY: 
                                       {"type": "string",
                                        "title": "AWS Secret Key",
                                        "description": "This is the secret key from the 'Security Credentials' page in AWS"},
                                       }
                        }
CLOUD_CONTAINER_SCHEMA = {
                        "$schema": "http://json-schema.org/draft-04/schema#",
                        "title": "Cloud Container parametersJson Schema",
                        "additionalProperties": False,
                        "type": "object",
                        "properties": {
                                       SCS_CLOUDIF_AWS_PARAM_IMAGE_ID: 
                                       {"type": "string",
                                        "title": "AWS Image Id",
                                        "description": "This is the iamge id of the AWS image to run (e.g. ami-de657fb6)"},
                                       SCS_CLOUDIF_AWS_PARAM_REGION_NAME:
                                       {"enum": 
                                        ["us-east-1",
                                         "us-west-2",
                                         "us-west-1",
                                         "eu-west-1",
                                         "eu-central-1",
                                         "ap-southeast-1",
                                         "ap-southeast-2",
                                         "ap-northeast-1",
                                         "sa-east-1",
                                         ],
                                        "title": "AWS Region",
                                        "description": 
                                        "This is the region that VMs associated with this container will run in"},
                                       
                                       SCS_CLOUDIF_AWS_PARAM_INSTANCE_TYPE: {"enum": 
                                                                             ["t2.micro",
                                                                              "t2.medium",
                                                                              "t2.large",
                                                                              "m4.large",
                                                                              "m4.xlarge",
                                                                              "m4.2xlarge",
                                                                              "m4.4xlarge",
                                                                              "m4.10xlarge",
                                                                              "m3.medium",
                                                                              "m3.large",
                                                                              "m3.xlarge",
                                                                              "m3.2xlarge",
                                                                              "c4.large",
                                                                              "c4.xlarge",
                                                                              "c4.2xlarge",
                                                                              "c4.4xlarge",
                                                                              "c4.8xlarge",
                                                                              "c3.large",
                                                                              "c3.xlarge",
                                                                              "c3.2xlarge",
                                                                              "c3.4xlarge",
                                                                              "c3.8xlarge",
                                                                              "g2.2xlarge",
                                                                              "g2.8xlarge",
                                                                              "r3.large",
                                                                              "r3.xlarge",
                                                                              "r3.2xlarge",                                                                 
                                                                              "r3.4xlarge",
                                                                              "r3.8xlarge",
                                                                              "i2.xlarge",
                                                                              "i2.2xlarge",
                                                                              "i2.4xlarge",
                                                                              "i2.8xlarge",
                                                                              "d2.xlarge",
                                                                              "d2.2xlarge",
                                                                              "d2.4xlarge",
                                                                              "d2.8xlarge"
                                                                              ],
                                                                        "title": "Instance Type",
                                                                        "description": "The size of the VM to start"},
                                       SCS_CLOUDIF_AWS_PARAM_KEYPAIR: {"type": "string",
                                                                        "title": "",
                                                                        "description": ""},
                                       }
                          }

cloud_path = scsCreateObjectPath("cloud", 
                                 {"name": "Amazon Web Services", 
                                  "name_key": "aws",
                                  "description": "Object representing the Amazon EC2 web service", 
                                  "cloud_type": "aws",
                                  "cloud_account_param_json_schema": json.dumps(CLOUD_ACCOUNT_SCHEMA),
                                  "cloud_container_param_json_schema": json.dumps(CLOUD_CONTAINER_SCHEMA)})
        
# Create cloud account
scsLoadAwsKeys()
cloud_account_path = scsCreateObjectPath("cloud_account",
                                         {"name": "RP-SMARF AWS Account", 
                                          "name_key": "aws_acct",
                                          "cloud": cloud_path,
                                          "accountInfoJson": json.dumps(
                                                                        {SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID: 
                                                                         scsGetEnvVar(SCS_ENV_VAR_TEST_AWS_KEY_ID),
                                                                         SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY: 
                                                                         scsGetEnvVar(SCS_ENV_VAR_TEST_AWS_SECRET_KEY)}
                                                                        )
                                          })

cloud_container = scsCreateObjectPath("container", 
                                      {"name": "AWS Cloud Container", 
                                       "name_key": "aws_cloud", 
                                       "cloud": cloud_path,
                                       "description": 
                                       "This container provides resources to run programs on AWS servers",
                                       "containerUrl": "ssh://localhost:22/",
                                       "agent": cloud_agent_data_path,
                                       "status": "up",
                                       "cloud_account": cloud_account_path,
                                       "parametersJson": json.dumps({SCS_CLOUDIF_DYNAMIC_SERVER: True,
                                                                     SCS_CLOUDIF_AWS_PARAM_IMAGE_ID: "ami-d05e75b8",
                                                                     SCS_CLOUDIF_AWS_PARAM_REGION_NAME: "us-east-1",
                                                                     SCS_CLOUDIF_AWS_PARAM_INSTANCE_TYPE: "t2.micro",
                                                                     SCS_CLOUDIF_AWS_PARAM_KEYPAIR: "cloud_server_test"})})
                                       
res_path = scsCreateObjectPath("resource", {"name": "Dynamic Amazon Cloud Server", 
                                            "name_key": "dyn_aws_server",
                                            "resource_type": rt_cloud_compute_path,
                                            "owner": root_path,
                                            "container": cloud_container,
                                            "parametersJson": json.dumps({"keyfile": "cloud_server_test.pem",
                                                                          "user": "ubuntu",
                                                                          "tempDataFolder": "/tmp"}),
                                            "description": "This is a dynamic cloud server which is "
                                            "started and stopped automatically"
                                            })
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=x")
scsGetToJson(res_path + "setperm/?action=assign&user=" + root_path + "&perm=x")

tt_path = scsCreateObjectPath("script", {"key": "jpeg_invert", 
                                         "value": 
                                         "sudo apt-get update\n"
                                         "sudo apt-get -y -qq install imagemagick\n"
                                         "convert -negate $1 $2/negated.jpg\n"
                                         "cp $1 $2/input.jpg"})

TASK_TYPE_COPY_CFG_DICT = {
                           "args": [ 
                                    {"name": "Script to run",
                                     "type": "string", 
                                     "value": "bash"},
                                    {"name": "Script to run",
                                     "type": "script", 
                                     "value": "jpeg_invert"},
                                    {"type": "resource", 
                                     "name": "Source File", 
                                     "description": "This is the file from which data is copied",
                                     "pathType": "file", 
                                     "input_id": 1, 
                                     "direction": "input", 
                                     "role": "src"},
                                    {"type": "resource", 
                                     "name": "Destination File",
                                     "description": "This is the folder into which the data is copied", 
                                     "pathType": "folder", 
                                     "input_id": 2, 
                                     "direction": "output", 
                                     "role": "dest"},
                                    ],
                           "computeResourceRole": "compute",
                       }

tt_path = scsCreateObjectPath("task_type", {"name": "Photo Negation Converter", 
                                            "name_key": "jpeg_to_ascii", 
                                            "uiVisible": True,
                                            "code_module": "smcommon.task_runners.task_runner_shell", 
                                            "code_classname": "SmTaskRunnerShell", 
                                            "description": "Task to negate a JPEG image",
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_COPY_CFG_DICT)})
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

scsCreateObjectPath("resource_type_task_type", {"role": "src", "task_type": tt_path, "resource_type": rt_photo_repo_data_path,
                                                "name": "Source of images",
                                                "name_key": "image_src",
                                                "nature": "data",
                                                "description": "This is the source of data for image processing"})
scsCreateObjectPath("resource_type_task_type", {"role": "dest", "task_type": tt_path, "resource_type": rt_storage_path,
                                                "name": "Output folder",
                                                "name_key": "output_folder",
                                                "nature": "data",
                                                "description": "This is the output folder for the output of image processing"})
scsCreateObjectPath("resource_type_task_type", {"role": "compute", "task_type": tt_path, "resource_type": rt_cloud_compute_path,
                                                "name": "Amazon EC2 Photo Processor",
                                                "name_key": "ec2_server",
                                                "nature": "compute",
                                                "description": "This is a destination folder"})
 
print("Task type 'cloud' objects setup.")
