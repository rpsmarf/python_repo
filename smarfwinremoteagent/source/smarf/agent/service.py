import os
import sys
import logging
import argparse
import win32api
import win32serviceutil
import win32service
import win32event
import win32timezone


class RPSmarfRAService(win32serviceutil.ServiceFramework):
    _svc_name_ = "RPSmarfRAService"
    _svc_display_name_ = "RP-SMARF Remote Agent"
    _svc_description_ = "RP-SMARF Remote Agent Description"
   
    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)           

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)                    
         
    def SvcDoRun(self):
        import servicemanager
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,servicemanager.PYS_SERVICE_STARTED,('RPSmarfRAService', ''))
        self.ReportServiceStatus(win32service.SERVICE_RUNNING)
        
        
        #################################################################################
        #
        # Start service functionality here.
        # 
        #################################################################################        
        
        win32event.WaitForSingleObject(self.hWaitStop, win32event.INFINITE)


def HandleCommandLine():
    parser = argparse.ArgumentParser(description='RP-SMARF Remote Agent')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--install', nargs='?', dest='install_name', const='RPSmarfRAService', metavar="service_name", help='Installs the named service')
    group.add_argument('--remove', nargs='?', dest='remove_name', const='RPSmarfRAService', metavar="service_name", help='Removes the named service')
    args = parser.parse_args()
    
    # Get the class string for the frozen executable (probably won't work unless frozen)
    if args.install_name:
        fname = win32api.GetFullPathName(sys.executable)
        path = os.path.split(fname)[0]
        fname = os.path.join(path, win32api.FindFiles(fname)[0][8])
        class_string = os.path.splitext(fname)[0] + '.' + RPSmarfRAService.__name__

        win32serviceutil.InstallService(
            class_string,
            args.install_name,
            RPSmarfRAService._svc_display_name_,
            serviceDeps=None,
            startType=win32service.SERVICE_DEMAND_START,
            bRunInteractive=None,
            userName=None,
            password=None,
            exeName=None,
            perfMonIni=None,
            perfMonDll=None,
            exeArgs=None,
            description=RPSmarfRAService._svc_description_,
            delayedstart=False)
         
         
    if args.remove_name:
        win32serviceutil.RemoveService(args.remove_name)


    