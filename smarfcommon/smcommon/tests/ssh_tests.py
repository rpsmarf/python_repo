'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import unittest
import paramiko
from smcommon.constants.sm_const import SM_TEST_AGENT_ID
import tempfile
import shutil
import os.path
from smcommon.tests.ssh_server import sm_start_test_sftp_server,\
    sm_test_sftp_server_stop 
from smcommon.logging.sm_logger import smLoggingSetupForTest
from smcommon.globals.envvar import SM_ENV_VAR_TEST_PASSWORD, smCommonGetEnvVar


class Ssh_Test(unittest.TestCase):

    def setUp(self):
        global SM_MY_AGENT_ID
        smLoggingSetupForTest()
        self.maxWaitCount = 100
        SM_MY_AGENT_ID = SM_TEST_AGENT_ID
        self.folder = tempfile.mkdtemp(prefix="copy_test")
        # Set up server
        sm_start_test_sftp_server() 
        
    def tearDown(self):
        # Stop server
        shutil.rmtree(self.folder)
        sm_test_sftp_server_stop()
        
    def writeFile(self, fname, content): 
        if (not fname.startswith("/")):
            fname = os.path.join(self.folder, fname)
        with open(fname, 'w') as the_file:
            the_file.write(content)
     
    def readFile(self, fname, maxSize): 
        if (not fname.startswith("/")):
            fname = os.path.join(self.folder, fname)
        with open(fname, 'r') as the_file:
            return the_file.read(maxSize)
     
    def testParamikoSFtpClient(self):
        # Show that connect/disconnect is OK
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        #rsa_key = paramiko.RSAKey.from_private_key_file(SM_SSH_TEST_PRIVATE_KEY_FILE)
        ssh.connect('127.0.0.1', username='rpsmarf', port=8222,
                    password=smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD))

        # Do local to SFTP copy
        ftp = ssh.open_sftp()
        self.writeFile("fremote", "9876543210")
        localfile = os.path.join(self.folder, "flocal")
        remotefile = os.path.join(self.folder, "fremote")
        ftp.get(remotefile, localfile)
        ftp.close()
        self.assertEqual(self.readFile("flocal", 1000), "9876543210")

        # Do SFTP to local copy
        ftp = ssh.open_sftp()
        self.writeFile("flocal2", "abcdefgh")
        localfile = os.path.join(self.folder, "flocal2")
        remotefile = os.path.join(self.folder, "fremote2")
        ftp.put(localfile, remotefile)
        ftp.close()
        self.assertEqual(self.readFile("fremote2", 1000), "abcdefgh")

        # Show progress working
           
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testSFtp']
    unittest.main()