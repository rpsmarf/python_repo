/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'rpsmarf.communities', [
  'restangular',
  'ui.router',
  'ui.bootstrap',
  'rpsmarf.resourceconfig',
  'rpsmarf.taskstart',
  'rpsmarf.task.create.service',
  'rpsmarf.user.service',
  'rpsmarf.extendedinfo',
  'rpsmarf.constants'
  
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider, RestangularProvider ) {
  $stateProvider.state( 'communities', {
    url: '/communities/',
    views: {
      "main": {
        controller: 'CommunitiesCtrl',
        templateUrl: 'communities/communities.tpl.html'
      }
      // ,
      // "breadcrumb": {
      //   controller: 'BreadcrumbCtrl',
      //   templateUrl: 'breadcrumb/breadcrumb.tpl.html'
      // }
    },
    data:{ pageTitle: 'Communities' }
  });
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'CommunitiesCtrl', ['$scope', 'Restangular', '$state', 'filterFilter', 'SmUser', '$modal', 'DEFAULT_COMMUNITY_ICON_URL',
  function ( $scope, Restangular, $state, filterFilter, SmUser, $modal, DEFAULT_COMMUNITY_ICON_URL) {

  /***VARIABLES***/
  /*Scope*/
  $scope.listcommunities = [];
  $scope.itemcommunity = null;

  /***STANDARD VARIABLES***/
  var user_community = SmUser.getProperty('community');
  var user_community_name_key = user_community.name_key;
  var uid = SmUser.getProperty('id');
  var communitiesall = Restangular.one('community/');
  var communityowner = Restangular.all('user/');
  var communitynewsitem = Restangular.all('news_item/');
  var communitymember = Restangular.all('user_setting/');
  var allCommunities = [];

  /***FUNCTIONS***/
  /*Scope*/
  $scope.clickCommunity = function(community){
    $scope.itemcommunity = user_community;
    if(community.selected){
      community.selected = false;
      $scope.itemcommunity = null;
    }
    else{
      setCommunitySelectedAs($scope.listcommunities, false);
      community.selected = true;
      //$scope.$parent.selectedcommunity = community;
      $state.go('communityinfo', {communityNameKey: community.name_key});
    }
  };

  $scope.filterCommunities = function(text){
    text = text.toLowerCase();
    $scope.listcommunities = filterFilter(allCommunities, function(community, index){
      return community.name.toLowerCase().indexOf(text) >=0 || community.description.toLowerCase().indexOf(text) >=0;
    });
  };

  $scope.createCommunity = function(){
    var modalInstance = $modal.open({
      size: 'lg',
      controller: 'CreateCommunityCtrl',
      templateUrl: 'communityinfo/communityinfo.modal.tpl.html'

    });

    modalInstance.result.then(function(response){
      if(response.complete && response.update){
        getCommunityBriefData(response.object);
        $scope.listcommunities.push(response.object);
      }
    }, function(){
      console.log('community not created');
    });
  };

  /*Standard*/
  var setCommunitySelectedAs = function(communities, val){
    for(var i = communities.length - 1; i >= 0; i-- ){
      communities[i].selected = val;
    }
  };
 
  var getCommunityMemberCount = function($obj){
    var communityusers = Restangular.one('user_setting/?community__id=' + $obj.id + '&limit=1');
    communityusers.get().then(function(response){
      $obj.memberscount = response.meta.total_count ? response.meta.total_count : '0';
    });
  };
  var getCommunityResourceCount = function($obj){
    var communityresources = Restangular.one('resource/?communityFilter=' + $obj.name_key +'&limit=1');
    communityresources.get().then(function(response){
      $obj.resourcecount = response.meta.total_count ? response.meta.total_count : '0';
    });
  };
  var getCommunityToolCount = function($obj){
    var communitytasktypes = Restangular.one('task_type/?communityFilter=' + $obj.name_key + '&uiVisible=true&limit=1');
    communitytasktypes.get().then(function(response){
      $obj.toolcount = response.meta.total_count ? response.meta.total_count : '0';
    });
  };
  var getLastNewsItemHeadline = function($obj){
    communitynewsitem.get('?community=' + $obj.id + '&limit=1&order_by=-id').then(function(response){
      if(response.objects.length > 0 ){
        $obj.lastnewsitem = response.objects[0].headline;
      }
    });
  };
  var getCommunityOwner = function ($obj){
    $obj.ownerid=String($obj.owner).split('/')[3];
    communityowner.get($obj.ownerid+'/').then(function(response){ 
      $obj.ownerlastname = response.last_name;
      $obj.ownerfirstname = response.first_name;

      if(response.email.trim() === ''){
        $obj.emailvisible = 0;
      }else{
        $obj.emailvisible = 1;
        $obj.owneremail = response.email;
      }
    });
  };
  var getMemberInformation = function ($obj) {
    communitymember.get($obj.ownerid+'/').then(function(response){ 
      $obj.ownertitle = response.title;
      $obj.owneravatar = response.imageUrl;
      $obj.ownerinfo = response.info;
    });
  };

  var getCommunityBriefData = function($obj){
    $obj.fieldofinterest = 'Science';
    getCommunityOwner($obj);
    getMemberInformation($obj);
    getLastNewsItemHeadline($obj);
    getCommunityToolCount($obj);
    getCommunityResourceCount($obj);
    getCommunityMemberCount($obj);
  };

  var initiateCommunities = function(){
    allCommunities = [];
    allCommunities.push(user_community);
    communitiesall.get().then(function(response){
      for(var i in response.objects){
        if(user_community_name_key == response.objects[i].name_key){ 
          n = 0;
          //$scope.listcommunities[0] = response.objects[i];
          //$scope.listcommunities[0].yourcommunity = true;
          allCommunities[0] = response.objects[i];
          allCommunities[0].yourcommunity = true;
        }
        else{
          //n = $scope.listcommunities.push(response.objects[i]) - 1;
          n = allCommunities.push(response.objects[i]) - 1;
        }
        //getCommunityBriefData($scope.listcommunities[n]);
        allCommunities[n].hide_community = (allCommunities[n].tag === null);
        if(allCommunities[n].imageUrl === ''){
          allCommunities[n].imageUrl = DEFAULT_COMMUNITY_ICON_URL;
        }
        getCommunityBriefData(allCommunities[n]);
        
      }
      $scope.listcommunities = allCommunities;
    });
  };

  /***EXE***/
  initiateCommunities();
  

    
}])
.controller('CreateCommunityCtrl', ['$scope', '$modalInstance', 'Restangular', '$modal','$state','SmUser', function($scope, $modalInstance, Restangular, $modal, $state, SmUser){

  /***STANDARD VARIABLES***/
  var createdCommunity;
  var namekey;
  var uid = SmUser.getProperty('id');

  /***SCOPE VARIABLES***/
  $scope.current = {};
  $scope.community = {};
  $scope.complete = false;

  /***SCOPE FUNCTIONS***/
  $scope.submitForm = function(form){
    var community = angular.copy($scope.community);
    community.owner = '/scs/user/'+uid+'/';
    if(form.$valid){
      Restangular.all('community/').post(community).then(function(response){
        createdCommunity = response.plain();
        $scope.complete = true;
        namekey = response.name_key;
      }, function(response){
        $modal.open({
          controller: 'AlertCtrl',
          templateUrl: 'alert/alert.tpl.html',
          resolve: {
            title: function(){ return 'Error creating community';},
            message: function(){ return 'An error occurred while attempting to create your community: ' + response.data.error;}
          }
        });
      });
    }
    else{
      form.$submitted = true;
    }
  };

  $scope.close = function(){
    $modalInstance.close({complete: $scope.complete, object:createdCommunity, update: true});
  };

  $scope.goToCommunity = function(){
    $state.go('communityinfo',{communityNameKey: namekey});
    $modalInstance.close({complete: $scope.complete, update: false});
  };
}]);