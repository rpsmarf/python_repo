'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 23, 2015

    @author: rpsmarf
'''
import logging
from smarfcontrolserver.cloudif.cloudif_aws import ScsCloudifAws
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_TYPE,\
    SCS_CLOUDIF_TYPE_AWS

logger = logging.getLogger(__name__)


class ScsCloudServerIfFactory(object):
    '''
    This is a factory class to make the appropriate cloud interface
    object given the circumstances.
    '''

    def __init__(self):
        self.mockCloudIf = None
        
    def getCloudServerIf(self, cloud_info, cloud_account_info):
        '''
        Method to return the appropriate cloud interface object 
        '''
        if self.mockCloudIf is not None:
            self.mockCloudIf.setLastCloudInfo(cloud_info, cloud_account_info)
            return self.mockCloudIf
        
        cloud_type = cloud_info[SCS_CLOUDIF_TYPE]
        if cloud_type == SCS_CLOUDIF_TYPE_AWS:
            return ScsCloudifAws(cloud_info, cloud_account_info)
        
        raise Exception("Unknown cloud type '{}'".format(cloud_type)) 

    def setMockCloudIf(self, mockCloudIf):
        self.mockCloudIf = mockCloudIf
         
         
_scsCloudFactory = ScsCloudServerIfFactory()


def scsCloudServerIfFactoryGet():
    '''
    Method to get the global cloud interface factory
    '''
    return _scsCloudFactory 

        