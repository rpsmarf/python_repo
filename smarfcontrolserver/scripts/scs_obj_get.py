#!/usr/bin/env python 
# encoding: utf-8
'''
scripts.scs_obj_get -- script to get an object

scripts.scs_obj_get is a description

It defines classes_and_methods

@author:     user_name

@copyright:  2014 organization_name. All rights reserved.

@license:    license

@contact:    user_email
@deffield    updated: Updated
'''

import sys
import os
from scripts.scs_obj_common import scsScriptGetDefaultUrl, scsScriptGetVerify,\
    scsScriptGetHeaders
from requests.packages import urllib3
sys.path.insert(1, "/opt/smarf-scs/scs")

from argparse import ArgumentParser 
from argparse import RawDescriptionHelpFormatter
import requests
import json

__all__ = []
__version__ = 0.1
__date__ = '2014-09-25'
__updated__ = '2014-09-25'

urllib3.disable_warnings()


class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''

    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
        
    def __str__(self):
        return self.msg
    
    def __unicode__(self):
        return self.msg


def scsGetToJson(pathAndQueryString, **kwargs):
    
    if ("url" in kwargs):
        url = kwargs["url"]
    else:
        url = scsScriptGetDefaultUrl()
    url = url + pathAndQueryString
    get_response = requests.get(url=url,
                                headers=scsScriptGetHeaders(), 
                                verify=scsScriptGetVerify())
    get_response.raise_for_status()
    return get_response.json()
    

def scsGetToFile(fileName, pathAndQueryString, **kwargs):
    
    if ("url" in kwargs):
        url = kwargs["url"]
    else:
        url = scsScriptGetDefaultUrl()
    url = url + pathAndQueryString
    with open(fileName, 'wb') as handle:
        response = requests.get(url=url,
                                headers=scsScriptGetHeaders(), 
                                verify=scsScriptGetVerify(), 
                                stream=True)
        response.raise_for_status()
        
        for block in response.iter_content(1024):
            if not block:
                break
        handle.write(block)
    
    
def scsPutFile(fileName, pathAndQueryString, **kwargs):
    
    if ("url" in kwargs):
        url = kwargs["url"]
    else:
        url = scsScriptGetDefaultUrl()
    url = url + pathAndQueryString
    hdrs = scsScriptGetHeaders()
    del(hdrs['Content-Type'])
    with open(fileName, "rb") as fp:
        resp = requests.post(files={'uploadfile': ('name', fp)},
                             url=url,
                             headers=hdrs, 
                             verify=scsScriptGetVerify())  
        resp.raise_for_status()
    

def scsGetObjectId(obj_type, queryString, **kwargs):
    
    if ("url" in kwargs):
        url = kwargs["url"]
    else:
        url = scsScriptGetDefaultUrl()
    url = url + "/scs/" + obj_type + "/?" + queryString
    get_response = requests.get(url=url, 
                                headers=scsScriptGetHeaders(), 
                                verify=scsScriptGetVerify())
    get_response.raise_for_status()
    return get_response.json()["objects"][0]["id"]
    

def scsMakePathFromId(obj_type, objId):
    return "/scs/" + obj_type + "/" + str(objId) + "/"


def scsGetObjectPath(obj_type, queryString, **kwargs):
    objId = scsGetObjectId(obj_type, queryString, **kwargs)
    return scsMakePathFromId(obj_type, objId)
    

def main(argv=None): 
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = "Script to get an RPSMARF object via the REST API on the SCS server"
    program_license = '''%s

  Created by Andrew McGregor on %s.
  Copyright 2014 Carleton. All rights reserved.
  
USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-u", "--url", dest="url", default=scsScriptGetDefaultUrl(),
                            help="URL to access REST API [default: %(default)s]")
        parser.add_argument("-t", "--type", dest="obj_type", default="",
                            help="The type of the object to get")
        parser.add_argument("-p", "--pk", dest="primaryKey", 
                            help="The primary key of the object to get")
        parser.add_argument("-i", "--id", dest="getId", action='store_true',
                            help="Return only the the id of the object which matches the query string")
        parser.add_argument("-q", "--query", dest="queryString",
                            help="Query string e.g. name=resource1")
        parser.add_argument("-v", "--verbose", action='store_true', dest="verbose",
                            help="Enable verbose mode")

        # Process arguments
        args = parser.parse_args()

        if args.verbose:
            print("Verbose mode on")

        if args.obj_type == "":
            parser.print_help()
            raise CLIError("Type must be specified.")

        if args.primaryKey is not None:        
            url = args.url + "/scs/" + args.obj_type + "/" + args.primaryKey + "/"
        else:
            url = args.url + "/scs/" + args.obj_type + "?" + args.queryString
        if args.verbose:
            print("URL=" + url)
        get_response = requests.get(url=url, 
                                    headers=scsScriptGetHeaders(), 
                                    verify=scsScriptGetVerify())
        if args.verbose:
            print("RESPONSE=" + get_response.text)
        get_response.raise_for_status()
        
        if args.getId:
            if args.primaryKey is not None:        
                print(str(get_response.json()["id"]))
            else:                
                print(str(get_response.json()["objects"][0]["id"]))
        else:
            print(json.dumps(get_response.json(), sort_keys=True, indent=2, separators=(',', ': ')))
        
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    sys.exit(main())
