'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Mar 4, 2015

    @author: rpsmarf
'''
import logging
from tastypie.authorization import DjangoAuthorization
from tastypie.exceptions import ImmediateHttpResponse, Unauthorized
from guardian.core import ObjectPermissionChecker
from tastypie.http import HttpGone
from smarfcontrolserver.init.envvar import scsApiGetAuthOn
import json
from django.http.response import HttpResponse
from smcommon.exceptions.sm_exc import SmHttpUnauthorized
from smarfcontrolserver.init.metric_names import SCS_OPS_REJ_DUE_TO_AGENT_DOWN,\
    SCS_OPS_REJ_DUE_TO_AGENT_LOCKED
from statsd.defaults.env import statsd
from sm_controller.models import SmModelCommunity, SmModelResource,\
    SmModelTaskType, SmModelTag

logger = logging.getLogger(__name__)


class Http422(HttpResponse):
        
    def __init__(self, s, *args, **kwargs):
        self.status_code = 422
        super().__init__(json.dumps({"description": s}))


class SmDjangoAuthorization(DjangoAuthorization):

    def generic_base_check(self, object_list, bundle):
        """
            Returns False if either:
                a) if the `object_list.model` doesn't have a `_meta` attribute
                b) the `bundle.request` object doesn't have a `user` attribute
        """
        if hasattr(object_list, "model"):
            model = object_list.model
        else:  # Sometime we are looking at lists which were made from other authZ checks so they are simple lists
            if len(object_list) == 0:
                return
            model = type(object_list[0])
        klass = self.base_checks(bundle.request, model)
        if klass is False and scsApiGetAuthOn():
            logger.debug('Failing generic_base_check, owner = %s, user = %s', bundle.obj.owner, bundle.request.user)
            raise Unauthorized("You are not allowed to access that resource.")
 

class ScsGdnAuthVisibleOnlyToSelf(SmDjangoAuthorization):
    """
    This is the authorization class for an object which can be created 
    by any user and only modified or viewed by that user.
    
    It is used as followed:

        class Something(models.Model):
            name = models.CharField()

        class SomethingResource(ModelResource):
            class Meta:
                queryset = Something.objects.all()
                authorization = ScsGdnAuthVisibleToAllEditOnlyByOwner()

    """
 
    def __init__(self, *args, **kwargs):
        super(ScsGdnAuthVisibleOnlyToSelf, self).__init__(**kwargs)
 
    def generic_item_check(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
         
        if (bundle.obj.owner != bundle.request.user and
                not bundle.request.user.is_superuser and scsApiGetAuthOn()):
            logger.debug('Failing item check, owner = %s, user = %s', bundle.obj.owner, bundle.request.user)
            raise Unauthorized("You are not allowed to access that resource.")
        return True
 
    def generic_list_check(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
        user = bundle.request.user
        return [i for i in object_list if user == i.owner]
        
    # List Checks
    def create_list(self, object_list, bundle):
        return object_list
 
    def read_list(self, object_list, bundle):
        return self.generic_list_check(object_list, bundle)
 
    def update_list(self, object_list, bundle):
        return self.generic_list_check(object_list, bundle)
 
    def delete_list(self, object_list, bundle):
        return self.generic_list_check(object_list, bundle)
 
    # Item Checks
    def create_detail(self, object_list, bundle):
        return True
 
    def read_detail(self, object_list, bundle):
        return self.generic_item_check(object_list, bundle)
 
    def update_detail(self, object_list, bundle):
        return self.generic_item_check(object_list, bundle)
 
    def delete_detail(self, object_list, bundle):
        return self.generic_item_check(object_list, bundle)
        
        
class ScsGdnAuthVisibleToAllEditOnlyByOwner(SmDjangoAuthorization):
    """
    This is the authorization class for an object which can be created 
    by any user and only modified by that user, however anyone can view the 
    object.
    
    It is used as followed:

        class Something(models.Model):
            name = models.CharField()

        class SomethingResource(ModelResource):
            class Meta:
                queryset = Something.objects.all()
                authorization = ScsGdnAuthVisibleToAllEditOnlyByOwner()

    """
 
    def __init__(self, *args, **kwargs):
        self.deletesAllowed = True
        super().__init__(**kwargs)
 
    def generic_item_check_alter(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
         
        if (bundle.obj.owner != bundle.request.user and 
                not bundle.request.user.is_superuser and scsApiGetAuthOn()):
            logger.debug('Failing alter check, owner = %s, user = %s', bundle.obj.owner, bundle.request.user)
            raise Unauthorized("You are not allowed to access that resource.")
        return True
 
    def generic_list_check_alter(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
        user = bundle.request.user
        return [i for i in object_list if user == i.owner or user.is_superuser]
        
    # List Checks
    def create_list(self, object_list, bundle):
        return object_list
 
    def read_list(self, object_list, bundle):
        return object_list
 
    def update_list(self, object_list, bundle):
        return self.generic_list_check_alter(object_list, bundle)
 
    def delete_list(self, object_list, bundle):
        if not self.deletesAllowed:
            return []        
        return self.generic_list_check_alter(object_list, bundle)
 
    # Item Checks
    def create_detail(self, object_list, bundle):
        return True
 
    def read_detail(self, object_list, bundle):
        return True
 
    def update_detail(self, object_list, bundle):
        logger.debug('In  update_detail, owner = %s, user = %s', bundle.obj.owner, bundle.request.user)
        return self.generic_item_check_alter(object_list, bundle)
 
    def delete_detail(self, object_list, bundle):
        if not self.deletesAllowed:
            return False
        return self.generic_item_check_alter(object_list, bundle)
    
        
class ScsGdnAuthVisibleToAllCreateWithPermEditByOwner(ScsGdnAuthVisibleToAllEditOnlyByOwner):
    """
    :permission_code:
        the permission code that signifies the user can create
        one of these objects

    This is a specialization of ScsGdnAuthVisibleToAllEditOnlyByOwner with the 
    refinement that only users with the permission provided can do the 
    create operation so we just override create and add the check
    """
 
    def __init__(self, *args, **kwargs):
        permission_code = kwargs.pop("permission_code", 'alter_object')
        self.create_permission_code = permission_code
        super().__init__(**kwargs)
 
    def generic_list_check(self, object_list, bundle, permission):
        self.generic_base_check(object_list, bundle)
        user = bundle.request.user
        return [i for i in object_list if user.has_perm(permission)]
 
    def generic_post_check(self, object_list, bundle, permission):
        self.generic_base_check(object_list, bundle)
        user = bundle.request.user
        if not user.has_perm(permission) and scsApiGetAuthOn():
            raise Unauthorized("You are not allowed to access that resource.")
        return True
    
    # List Checks
    def create_list(self, object_list, bundle):
        return self.generic_list_check(object_list, bundle,
                                       self.create_permission_code)
 
    # Item Checks
    def create_detail(self, object_list, bundle):
        return self.generic_post_check(object_list, bundle,
                                       self.create_permission_code)
         
        
class ScsGdnAuthVisibleToAllAlterWithPerm(SmDjangoAuthorization):
    """
    :permission_code:
        the permission code that signifies the user can create, modify or delete
        one of these objects

    :kwargs:
        other permission codes


        class Something(models.Model):
            name = models.CharField()

        class SomethingResource(ModelResource):
            class Meta:
                queryset = Something.objects.all()
                authorization = GuardianAuthorization(
                    permission_code = 'alter_task_type',
                    )

    """
 
    def __init__(self, *args, **kwargs):
        permission_code = kwargs.pop("permission_code", 'alter_object')
        self.create_permission_code = permission_code
        self.update_permission_code = permission_code
        self.delete_permission_code = permission_code
        super().__init__(**kwargs)
 
    def generic_item_check(self, object_list, bundle, permission):
        self.generic_base_check(object_list, bundle)
        
        if not bundle.request.user.has_perm(permission) and scsApiGetAuthOn():
            raise Unauthorized("You are not allowed to access that resource.")
 
        return True
 
    def generic_list_check(self, object_list, bundle, permission):
        self.generic_base_check(object_list, bundle)
        user = bundle.request.user
        return [i for i in object_list if user.has_perm(permission)]
 
    def generic_post_check(self, object_list, bundle, permission):
        self.generic_base_check(object_list, bundle)
        user = bundle.request.user
        if not user.has_perm(permission) and scsApiGetAuthOn():
            raise Unauthorized("You are not allowed to access that resource.")
 
        return True
 
    # List Checks
    def create_list(self, object_list, bundle):
        return self.generic_list_check(object_list, bundle,
            self.create_permission_code)
 
    def read_list(self, object_list, bundle):
        return object_list
 
    def update_list(self, object_list, bundle):
        return self.generic_list_check(object_list, bundle,
            self.update_permission_code)
 
    def delete_list(self, object_list, bundle):
        return self.generic_list_check(object_list, bundle,
            self.delete_permission_code)
 
    # Item Checks
    def create_detail(self, object_list, bundle):
        return self.generic_post_check(object_list, bundle,
            self.create_permission_code)
 
    def read_detail(self, object_list, bundle):
        return True
 
    def update_detail(self, object_list, bundle):
        return self.generic_item_check(object_list, bundle,
            self.update_permission_code)
 
    def delete_detail(self, object_list, bundle):
        return self.generic_item_check(object_list, bundle,
            self.delete_permission_code)
        
        
class ResourceAuthorization(SmDjangoAuthorization):
    """
    :create_permission_code:
        the permission code that signifies the user can create one of these objects
    :view_permission_code:
        the permission code that signifies the user can view the detail
    :update_permission_code:
        the permission code that signifies the user can update one of these objects
    :remove_permission_code:
        the permission code that signifies the user can remove one of these objects

    :kwargs:
        other permission codes


        class Something(models.Model):
            name = models.CharField()

        class SomethingResource(ModelResource):
            class Meta:
                queryset = Something.objects.all()
                authorization = GuardianAuthorization(
                    view_permission_code = 'can_view',
                    read_data_permission_code = 'can_create',
                    write_data_permission_code = 'can_update',
                    run_permission_code = 'can_delete'
                    )

    """
 
    def __init__(self, *args, **kwargs):
        self.alter_permission_code = kwargs.pop("alter_permission_code", 'can_create')
        self.read_data_permission_code = kwargs.pop("read_data_permission_code", 'can_read')
        self.write_data_permission_code = kwargs.pop("write_data_permission_code", 'can_write')
        self.run_permission_code = kwargs.pop("run_permission_code", 'can_run')
        super(ResourceAuthorization, self).__init__(**kwargs)
 
    def generic_list_check_alter(self, object_list, bundle):
        # Check that object is owned by the user making the request
        self.generic_base_check(object_list, bundle)
        user = bundle.request.user
        return [i for i in object_list if user == i.owner]
        
    def generic_item_check_alter(self, object_list, bundle):
        if not scsApiGetAuthOn():
            return True
        self.generic_base_check(object_list, bundle)
         
        if bundle.obj.owner != bundle.request.user:
            raise Unauthorized("You are not allowed to access that resource.")
        return True

    def generic_post_check(self, object_list, bundle, permission):
        if not scsApiGetAuthOn():
            return True
        self.generic_base_check(object_list, bundle)
        user = bundle.request.user
        if not user.has_perm(permission):
            raise Unauthorized("You are not allowed to access that resource.")
        return True
  
    def generic_item_check(self, object_list, bundle, permission):
        if not scsApiGetAuthOn():
            return True
        self.generic_base_check(object_list, bundle)
 
        checker = ObjectPermissionChecker(bundle.request.user)
        if not checker.has_perm(permission, bundle.obj):
            raise Unauthorized("You are not allowed to access that resource.")
        return True
 
    def rejectIfLocked(self, modelObj, request):
        '''
        This method will reject a request if the resource is locked.
        '''
        if modelObj.locked:
            statsd.incr(SCS_OPS_REJ_DUE_TO_AGENT_LOCKED)
            raise ImmediateHttpResponse(HttpGone(json.dumps({"description": 
                                                             "Object {} is locked".format(str(modelObj))})))
    
        #logger.debug("Checking modelObj for status... (obj %s)", str(modelObj))

        if hasattr(modelObj, 'status'):
            if modelObj.status == "down":
                statsd.incr(SCS_OPS_REJ_DUE_TO_AGENT_DOWN)
                logger.info("Resource %s is down status (is %s)", str(modelObj), modelObj.status)
                raise ImmediateHttpResponse(HttpGone(json.dumps({"description": 
                                                                "Object {} is down".format(str(modelObj))})))
            
    def rejectIfFull(self, resource):
        '''
        This method will reject a request if the resource is full.
        '''
        if resource.capacityJson != "":
            capDict = json.loads(resource.capacityJson)
            if capDict.get("diskUsedState") == "full":
                logger.warning("Resource is full, returning error")
                raise ImmediateHttpResponse(Http422("Resource {} is full".format(str(resource))))
                
    # List Checks
    def create_list(self, object_list, bundle):
        return self.generic_list_check(object_list, bundle,
            self.alter_permission_code)
 
    def read_list(self, object_list, bundle):
        return object_list
 
    def update_list(self, object_list, bundle):
        return self.generic_list_check_alter(object_list, bundle)
 
    def delete_list(self, object_list, bundle):
        return self.generic_list_check_alter(object_list, bundle)
 
    # Item Checks
    def create_detail(self, object_list, bundle):
        return self.generic_post_check(object_list, bundle,
            self.alter_permission_code)
 
    def read_detail(self, object_list, bundle):
        return True
 
    def update_detail(self, object_list, bundle):
        return self.generic_item_check_alter(object_list, bundle)
 
    def delete_detail(self, object_list, bundle):
        return self.generic_item_check_alter(object_list, bundle)

    # Special checks to control access to data in a resource or 
    # running commands on a resource
    def check_read_data(self, apiObj, modelObj, request):
        self.rejectIfLocked(modelObj, request)
        if not scsApiGetAuthOn():
            return
        apiObj.is_authenticated(request)        
        checker = ObjectPermissionChecker(request.user) 
        if (modelObj.owner != request.user and 
                not checker.has_perm(self.read_data_permission_code, modelObj)):
            logger.warning("You are not allowed 'read' access to {}".format(str(modelObj)))
            raise ImmediateHttpResponse(SmHttpUnauthorized("You are not allowed read access to {}".format(str(modelObj))))

    def check_read_data_or_run(self, apiObj, modelObj, request):
        self.rejectIfLocked(modelObj, request)
        if not scsApiGetAuthOn():
            return
        apiObj.is_authenticated(request)        
        checker = ObjectPermissionChecker(request.user) 
        if (modelObj.owner != request.user and 
                not (checker.has_perm(self.read_data_permission_code, modelObj) or
                     checker.has_perm(self.run_permission_code, modelObj))):
            logger.warning("You are not allowed 'read' or 'run' access to {}".format(str(modelObj)))
            raise ImmediateHttpResponse(SmHttpUnauthorized
                                        ("You are not allowed read or run access to {}".format(str(modelObj))))

    def check_write_data(self, apiObj, modelObj, request, rejectIfFull=True):
        self.rejectIfLocked(modelObj, request)
        if rejectIfFull:
            self.rejectIfFull(modelObj)
        if not scsApiGetAuthOn():
            return
        apiObj.is_authenticated(request)        
        checker = ObjectPermissionChecker(request.user)
        if (modelObj.owner != request.user and 
                not checker.has_perm(self.write_data_permission_code, modelObj)):
            logger.warning("Rejecting write due to user and permission check")
            raise ImmediateHttpResponse(SmHttpUnauthorized("You are not allowed write access to {}".format(str(modelObj))))
                
    def check_write_data_wo_tastypie(self, modelObj, user):
        self.rejectIfLocked(modelObj, None)
        self.rejectIfFull(modelObj)
        if not scsApiGetAuthOn():
            return
        checker = ObjectPermissionChecker(user)
        if (modelObj.owner != user and 
                not checker.has_perm(self.write_data_permission_code, modelObj)):
            logger.warning("Rejecting write due to user and permission check")
            raise ImmediateHttpResponse(SmHttpUnauthorized("You are not allowed write access to {}".format(str(modelObj))))
                
    def check_run(self, apiObj, modelObj, request):
        self.rejectIfLocked(modelObj, request)
        if not scsApiGetAuthOn():
            return
        apiObj.is_authenticated(request)        
        checker = ObjectPermissionChecker(request.user)
        if (modelObj.owner != request.user and 
                not checker.has_perm(self.run_permission_code, modelObj)):
            logger.warning("You are not allowed 'run' access to {}".format(str(modelObj)))
            raise ImmediateHttpResponse(SmHttpUnauthorized("You are not allowed 'run' access to {}".format(str(modelObj))))
            

class ScsGdnAuthVisibleToAllEditOnlySuperuser(SmDjangoAuthorization): 
    """
    This is the authorization class for an object which can be created 
    by any user and only modified by that user, however anyone can view the 
    object.
    
    It is used as followed:

        class Something(models.Model):
            name = models.CharField()

        class SomethingResource(ModelResource):
            class Meta:
                queryset = Something.objects.all()
                authorization = ScsGdnAuthVisibleToAllEditOnlyByOwner()

    """
 
    def __init__(self, *args, **kwargs):
        super(ScsGdnAuthVisibleToAllEditOnlySuperuser, self).__init__(**kwargs)
 
    def generic_item_check_alter(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
         
        if not bundle.request.user.is_superuser and scsApiGetAuthOn():
            raise Unauthorized("You are not allowed to access that resource.")
        return True
 
    def generic_list_check_alter(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
        user = bundle.request.user
        return [i for i in object_list if user.is_superuser]
        
    # List Checks
    def create_list(self, object_list, bundle):
        return self.generic_list_check_alter(object_list, bundle)
 
    def read_list(self, object_list, bundle):
        return object_list
 
    def update_list(self, object_list, bundle):
        return self.generic_list_check_alter(object_list, bundle)
 
    def delete_list(self, object_list, bundle):
        return self.generic_list_check_alter(object_list, bundle)
 
    # Item Checks
    def create_detail(self, object_list, bundle):
        return self.generic_item_check_alter(object_list, bundle)
 
    def read_detail(self, object_list, bundle):
        return True
 
    def update_detail(self, object_list, bundle):
        return self.generic_item_check_alter(object_list, bundle)
 
    def delete_detail(self, object_list, bundle):
        return self.generic_item_check_alter(object_list, bundle)
    
        
class ScsGdnAuthTagLink(SmDjangoAuthorization):
    """
    This is the authorization class for an object which links a tag to a resource
    or task_type.  It cannot be changed.  It can be created by the community or 
    resource/task_type owner.
    """
 
    def __init__(self, link_type, **kwargs):
        self.link_type = link_type
        super().__init__(**kwargs)
 
    def generic_item_check_alter(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
         
        if (bundle.obj.owner != bundle.request.user and 
                not bundle.request.user.is_superuser and scsApiGetAuthOn()):
            logger.debug('Failing alter check, owner = %s, user = %s', bundle.obj.owner, bundle.request.user)
            raise Unauthorized("You are not allowed to access that resource.")
        return True
 
    def isUserResourceOrCommunityOwner(self, bundle, obj):
        if not scsApiGetAuthOn():
            return True
        if bundle.request.user.is_superuser:
            return True
        if getattr(obj, self.link_type).owner == bundle.request.user:
            return True
        communityList = SmModelCommunity.objects.filter(tag=obj.tag)
        if communityList.count() > 0:
            # Should only be one match
            if communityList.first().owner == bundle.request.user:
                return True
        return False
        
    # List Checks
    def create_list(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
        return [i for i in object_list 
                if self.isUserResourceOrCommunityOwner(bundle, i)]
 
    def read_list(self, object_list, bundle):
        return object_list
 
    def update_list(self, object_list, bundle):
        return []
 
    def delete_list(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
        return [i for i in object_list 
                if self.isUserResourceOrCommunityOwner(bundle, i)]
 
    # Item Checks
    def create_detail(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
        if not scsApiGetAuthOn():
            return True
        if bundle.request.user.is_superuser:
            return True
        link_id = int(bundle.data[self.link_type].split("/")[3])
        if self.link_type == "resource":
            link_obj = SmModelResource.objects.get(id=link_id)
        elif self.link_type == "task_type":
            link_obj = SmModelTaskType.objects.get(id=link_id)
            
        if link_obj.owner == bundle.request.user:
            return True
        tag_id = int(bundle.data["tag"].split("/")[3])
        tag_obj = SmModelTag.objects.get(id=tag_id)
        communityList = SmModelCommunity.objects.filter(tag=tag_obj)
        if communityList.count() > 0:
            # Should only be one match
            community = communityList.first()
            if community.owner == bundle.request.user:
                return True
        return False
 
    def read_detail(self, object_list, bundle):
        return True
 
    def update_detail(self, object_list, bundle):
        return False
 
    def delete_detail(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
        return self.isUserResourceOrCommunityOwner(bundle, bundle.obj)


class ScsGdnAuthVisibleToAllEditOnlyByOwnerOrLinkedObjectOwner(SmDjangoAuthorization):
    """
    This is the authorization class for an object which can be created 
    by any user and be deleted/modified by that user or the owner of a linked object
    (e.g. a community owner can delete/edit news items linked to his community), 
    however anyone can view the object.
    """
 
    def __init__(self, link_name, **kwargs):
        self.link_name = link_name
        super().__init__(**kwargs)
 
    def generic_item_check_alter(self, bundle, obj):         
        user = bundle.request.user
        if obj.owner == user:
            return True
        if getattr(obj, self.link_name).owner == user:
            return True
        if user.is_superuser:
            return True
        if not scsApiGetAuthOn():
            return True
        
        logger.debug('Failing alter check, owner = %s, user = %s', obj.owner, user)
        return False
                
    # List Checks
    def create_list(self, object_list, bundle):
        return object_list
 
    def read_list(self, object_list, bundle):
        return object_list
 
    def update_list(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
        return [i for i in object_list if self.generic_item_check_alter(bundle, i)]
 
    def delete_list(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
        return [i for i in object_list if self.generic_item_check_alter(bundle, i)]
 
    # Item Checks
    def create_detail(self, object_list, bundle):
        return True
 
    def read_detail(self, object_list, bundle):
        return True
 
    def update_detail(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
        if not self.generic_item_check_alter(bundle, bundle.obj):
            logger.warning("You are not allowed to access that resource.")
            raise Unauthorized("You are not allowed to access that resource.")
        return True
 
    def delete_detail(self, object_list, bundle):
        self.generic_base_check(object_list, bundle)
        if not self.generic_item_check_alter(bundle, bundle.obj):
            logger.warning("You are not allowed to access that resource.")
            raise Unauthorized("You are not allowed to access that resource.")
        return True

    
        
