'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 22, 2014

    @author: rpsmarf
'''
import logging
from smraicemsgs import SmRemAgAsyncOps  # @UnresolvedImport
from smcommon.tasks.task_desc import SmTaskDesc
from smcommon.task_runners.task_runner import SmTaskRunnerFactory
from smcommon.exceptions.sm_exc import SmException
import smraicemsgs
import json

logger = logging.getLogger(__name__)


class SmIceAsyncOpsServer(SmRemAgAsyncOps):
    '''
    classdocs
    '''

    def __init__(self, iceObject, responseIceConnectString):
        '''
        Constructor
        '''
        self.iceObject = iceObject
        self.taskRunnerDict = {}
        self.responseIceConnectString = responseIceConnectString
        
    def startOperation(self, opId, opType, opParamsJson, current=None):
        try:
            if opType == "task":
                taskDesc = SmTaskDesc.makeFromJson(opParamsJson)
                taskRunner = SmTaskRunnerFactory.makeTaskRunner(opId, self._taskDone, taskDesc)
                taskRunner.setProgressCallback(self._taskProgress)
                taskRunner.setTaskOutputCallback(self._taskOutput)
                taskRunner.start()
                self.taskRunnerDict[opId] = taskRunner
            else:
                raise SmException("Unknown opType '" + opType + "' in startOperation")
        except Exception as e:
            logger.error("Error during operation start", exc_info=True)
            raise e
        
    def cancelOperation(self, opId, current=None):
        self.taskRunnerDict[opId].cancel()
    
    def getStatusOfOperation(self, opId, current=None):
        return self.taskRunnerDict[opId].getProgress()
    
    def _taskProgress(self, opId, progress):
        logger.debug("Got progress on %s to %s", opId, progress)
        base = self.iceObject.stringToProxy(self.responseIceConnectString)
        asyncResp = smraicemsgs.SmRemAgAsyncOpRespsPrx.checkedCast(base)  # @UndefinedVariable
        if not asyncResp:
            logger.warning("Invalid proxy via '" + self.responseIceConnectString + "'")
        try:
            asyncResp.progress(opId, json.dumps(progress))
        except:
            logger.exception("Exception on progress sending to %s.",                        
                         self.responseIceConnectString, exc_info=True)
            
    def _taskDone(self, opId, status, resultDict):
        resultString = json.dumps(resultDict)
        logger.debug("Task %s completed with status %s, results = %s", opId, status, resultString)
        logger.debug("Sending response to %s", self.responseIceConnectString)
        base = self.iceObject.stringToProxy(self.responseIceConnectString)
        asyncResp = smraicemsgs.SmRemAgAsyncOpRespsPrx.checkedCast(base)  # @UndefinedVariable
        asyncResp.done(opId, status, resultString)

    def _taskOutput(self, opId, outputType, outputData):
        logger.debug("Sending %s to %s", outputType, self.responseIceConnectString)
        base = self.iceObject.stringToProxy(self.responseIceConnectString)
        asyncResp = smraicemsgs.SmRemAgAsyncOpRespsPrx.checkedCast(base)  # @UndefinedVariable
        asyncResp.taskOutput(opId, outputType, outputData)
