describe( 'breadcrumb section', function() {
	beforeEach( module( 'rpsmarf.taskbreadcrumb' ) );
	beforeEach( module('breadcrumb/breadcrumb.tpl.html'));

	describe('breadcrumb functionality based on state', function(){

		var scope, controller,httpBackend, createController, tRestangular;
		

		beforeEach(inject(function($rootScope,$controller){

			scope = $rootScope.$new();
			controller = $controller('BreadcrumbCtrl', {$scope: scope});
		}));

		it('should show the right amount and state for the breadcrumbs', function(){
			scope.$broadcast('$stateChangeSuccess',{name: 'tools'}, {}, {}, {});
			expect(scope.crumbLevel).toBe(1);
			scope.$broadcast('$stateChangeSuccess',{name: 'taskstart'}, {}, {}, {});
			expect(scope.crumbLevel).toBe(2);
			scope.$broadcast('$stateChangeSuccess',{name: 'resourceconfig.taskstart'}, {}, {}, {});
			expect(scope.crumbLevel).toBe(3);
		});
	});
	
});

