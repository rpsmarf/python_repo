describe( 'User resources page (user.resources)', function() {
	beforeEach (module('rpsmarf.config'));
	
	beforeEach(module('rpsmarf.user.resources'));
	beforeEach(module('ui.router'));
	beforeEach(module('ui.bootstrap'));
	beforeEach(module('restangular'));
	
	beforeEach(module('rpsmarf.user.service'));
	beforeEach(module('rpsmarf.alert'));
	beforeEach(module('alert/alert.tpl.html'));
	beforeEach(module('confirm/confirm.tpl.html'));
	beforeEach(module('user.resources/user.resources.modal.tpl.html'));
	beforeEach(module('user.resources/user.resources.form.tpl.html'));
	
	//$scope, $modalInstance, taskObj, $interval,TASK_STATE_ENUM, modalTitle, interactive, $modal, TASK_STATE_FINISHED, TASK_STATE_RUNNING
	var $scope, $controller, $state, $interval, $modalInstance, smarfurl, taskObject, $httpBackend, responseObj, jsonPath = 'src/app/user.resources/user.resources.spec.json';
	var SmUser, user, $rootScope, Restangular, json;
	//init, starting, prep, running, cleanup and finished
	

	beforeEach(  inject(function(_$controller_, _$rootScope_, _Restangular_, SMARF_URL, _$state_, _$httpBackend_, _SmUser_, DEBUG){
		$rootScope = _$rootScope_;
		Restangular = _Restangular_;
		smarfurl = SMARF_URL+'/scs/';
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
		SmUser = _SmUser_;
		user = SmUser.getObject();
	
		user.id = 2;
		user.community = {};
		user.community.name_key = 'rads';
		
		$httpBackend = _$httpBackend_;
		
		$scope = $rootScope.$new();//console.log(__html__);
		responseObj = JSON.parse(__html__[jsonPath]);
		
		$httpBackend.expect('GET',smarfurl+'resource/?owner__id='+user.id+'&extForUser='+user.id).respond(200,responseObj);
		$httpBackend.expect('GET',smarfurl+'task_type/?owner__id='+user.id+'&extForUser='+user.id).respond(200,responseObj.task_types);
		
		$controller = _$controller_;
		$controller('UserResourcesCtrl', {$scope: $scope, Restangular:Restangular,$state:$state, SmUser:SmUser });
		$httpBackend.flush();

	})) ;

	it('should update the locked variable for the resource', inject([function(){
		var r = $scope.resources[0];
		$httpBackend.expect('PATCH', smarfurl+'resource/'+r.id+'/',{locked:!$scope.resources[0].locked}).respond(200);
		expect($scope.resources.length).toEqual(responseObj.meta.total_count);
		expect($scope.resources[0].attached).toBe(true);
		
		$scope.resourceAttachToggle($scope.resources[0], 'resource');

		$httpBackend.flush();

		$httpBackend.expect('PATCH', smarfurl+'resource/'+r.id+'/',{locked:!$scope.resources[0].locked}).respond(200);
		$scope.resourceAttachToggle($scope.resources[0], 'resource');

		$httpBackend.flush();

		r = $scope.tools[0];
		$httpBackend.expect('PATCH', smarfurl+'task_type/'+r.id+'/',{locked:!$scope.tools[0].locked}).respond(200);
		expect($scope.tools[0].attached).toBe(true);
		
		$scope.resourceAttachToggle($scope.tools[0], 'task_type');

		$httpBackend.flush();

		$httpBackend.expect('PATCH', smarfurl+'task_type/'+r.id+'/',{locked:!$scope.tools[0].locked}).respond(200);
		$scope.resourceAttachToggle($scope.tools[0], 'task_type');

		$httpBackend.flush();

		//expect($scope.tooltask).toEqual($scope.tooltasks[1]);

	}]));

	it('should allow the user to add a new resource', inject(function(){
		$httpBackend.expect('GET',smarfurl+'resource_type/?nature=data').respond(200,responseObj.resource_types_data);
		$httpBackend.expect('GET',smarfurl+'container/').respond(200,responseObj.containers);
		$httpBackend.expect('GET',smarfurl+'community/').respond(200,responseObj.communities);

		$scope.getResourceRelatedTypes();

		$httpBackend.flush();
		$scope.resource = {name:'New Resource', resource_type:responseObj.resource_types_data.objects[0],owner:'/scs/user/2/',container:responseObj.containers.objects[0],
							nature:'data', parametersJson: 'my/folder',description:'This is a description of the resource!!!!!'
						};
		$scope.selectedCommunity.obj = responseObj.communities.objects[0];

		$httpBackend.expect('POST',smarfurl+'resource/',{name:'New Resource', resource_type:'/scs/resource_type/1/',
															owner:'/scs/user/2/',container:'/scs/container/1/',nature:'data', 
															parametersJson: '{"folder":"my/folder/"}',description:'This is a description of the resource!!!!!'
														},{"Accept":"application/json, text/plain, */*","Content-Type":"application/json;charset=utf-8"}).respond(200,{headers:function(val){ return '/scs/resource/47/'; }},{Location:'/scs/resource/47/'});

		$httpBackend.expect('POST',smarfurl+'resource_tag/',{resource:'/scs/resource/47/', tag:'/scs/tag/1/'},{"Accept":"application/json, text/plain, */*","Content-Type":"application/json;charset=utf-8"}).respond(200);
		$scope.submitResourceForm({$valid: true});

		$httpBackend.flush();

		expect($scope.resource).toEqual({name:null, resource_type:null,owner:null,container:null,nature:'data', parametersJson: null,description:null});
	}));

	it('should refresh resources', inject(function(){
		$httpBackend.expect('GET',smarfurl+'resource/?owner__id='+user.id+'&extForUser='+user.id).respond(200,responseObj);
		$scope.refreshResources();
		$httpBackend.flush();
	}));

	it('should delete a resource', inject(function(){
		angular.element('body').children().remove();
		$httpBackend.expect('DELETE',smarfurl+'resource/2/').respond(200);
		var numResources = $scope.resources.length;
		$scope.deleteResource($scope.resources[1],1);
		$rootScope.$apply();
		angular.element('.modal-footer .btn-primary').click();
		$rootScope.$apply();
		$httpBackend.flush();

		expect($scope.resources.length).toBe(numResources-1);
	}));

	//$scope, Restangular, $modalInstance,resource
	it('should allow the user to edit a resource', inject(function($modal){
		//$scope, Restangular, $modalInstance,resource
		var resourceObj = responseObj.objects[0];
		$scope = $rootScope.$new();
		$controller('UserResourcesEditCtrl', {$scope:$scope, Restangular: Restangular, $modalInstance: null, resource:resourceObj, $modal:$modal});
		$httpBackend.expect('GET', smarfurl+'resource_type/'+resourceObj.id+'/').respond(200,responseObj.resource_types_data.objects[0]);
		$httpBackend.expect('GET', smarfurl+'container/'+resourceObj.id+'/').respond(200,responseObj.containers.objects[0]);
		$httpBackend.expect('GET', smarfurl+'resource_tag/?resource__id='+resourceObj.id).respond(200,{objects:[{id:1,resource:'/scs/resource/'+resourceObj.id+'/',tag:'/scs/tag/4/'}]});
		$httpBackend.expect('GET',smarfurl+'resource_type/?nature=data').respond(200,responseObj.resource_types_data);
		$httpBackend.expect('GET',smarfurl+'container/').respond(200,responseObj.containers);
		$httpBackend.expect('GET',smarfurl+'community/').respond(200,responseObj.communities);
		$httpBackend.expect('GET', smarfurl+'tag/4/').respond(200,{tag_name:'community:test'});

		$httpBackend.expect('GET', smarfurl+'community/?name_key=test').respond(200,{objects:[responseObj.communities.objects[1]]});
		
		
		$httpBackend.flush();

		expect($scope.resource.parametersJson).toBe('files/');
		$scope.resource.parametersJson = 'files/test/';
		$scope.resource.description += 'test';
		$httpBackend.expect('PATCH', smarfurl+'resource/'+resourceObj.id+'/',{description: $scope.resource.description+'test',parametersJson: JSON.stringify({folder:'files/test/'}) });
		$scope.submitResourceForm({$valid: true});




	}));
	
  
});

