import json
import sys
from scripts.scs_task_start import scsTaskStartByPath
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath
from scripts.scs_obj_create import scsCreateObjectPath
import time

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

taskParams = {
              "input_ids": {
                            "1": {"path": "a"},
                            "2": {"path": "dir1-" + str(time.time())},
                            "3": {"value": "8"},
                            "4": {"value": "prefix1-"},
                            "5": {"path": "a"},
                            "6": {"path": "dir2-" + str(time.time())},
                            "7": {"value": "4"},
                            "8": {"value": "prefix2-"},
                            }
              }

tt_path = scsGetObjectPath("task_type", "name_key=cp_many")
root_path = scsGetObjectPath("user", "name=rpsmarf")
task_path = scsCreateObjectPath("task", {"name": "shell_cp", 
                                         "task_type": tt_path,
                                         "owner": root_path,
                                         "parametersJson": json.dumps(taskParams),
                                         }
                                )

rt3_src1_path = scsGetObjectPath("resource_type_task_type", "name_key=many_cp_src1")
rt3_dest1_path = scsGetObjectPath("resource_type_task_type", "name_key=many_cp_dest1")
rt3_src2_path = scsGetObjectPath("resource_type_task_type", "name_key=many_cp_src2")
rt3_dest2_path = scsGetObjectPath("resource_type_task_type", "name_key=many_cp_dest2")
rt3_compute_path = scsGetObjectPath("resource_type_task_type", "name_key=many_cp_compute")


src_resource_path = scsGetObjectPath("resource", "name=copy_src")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": src_resource_path,
                                      "resource_type_task_type": rt3_src1_path
                                      }
                    )
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": src_resource_path,
                                      "resource_type_task_type": rt3_src2_path
                                      }
                    )

dest_resource_path = scsGetObjectPath("resource", "name=copy_dest")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": dest_resource_path,
                                      "resource_type_task_type": rt3_dest1_path
                                      }
                    )
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": dest_resource_path,
                                      "resource_type_task_type": rt3_dest2_path
                                      }
                    )
compute_resource_path = scsGetObjectPath("resource", "name=c1_compute")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": compute_resource_path,
                                      "resource_type_task_type": rt3_compute_path
                                      }
                    )

print("Task " + task_path + " to copy /tmp/src/a to /tmp/dest/b is setup, starting task...")

scsTaskStartByPath(task_path, verbose=True)

