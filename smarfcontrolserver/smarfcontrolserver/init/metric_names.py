'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jun 24, 2015

    @author: rpsmarf
'''

# Gauges
SCS_NUM_USERS_IN_DB = "scs.num_users"
SCS_NUM_COMMUNITIES = "scs.num_communities"
SCS_NUM_TASK_TYPES = "scs.num_task_types"
SCS_NUM_AGENTS = "scs.num_agents"
SCS_NUM_AGENTS_DOWN = "scs.num_agents_down"
SCS_NUM_RESOURCES = "scs.num_resources"
SCS_NUM_RESOURCES_DOWN = "scs.num_resources_down"
SCS_NUM_TASKS_RUNNING = "scs.num_tasks_running"
SCS_NUM_TASKS_QUEUED = "scs.num_tasks_queued"
SCS_NUM_TASKS_LONG_RUNNING = "scs.num_tasks_long_running"

# Counters
SCS_METRIC_NUM_USERS_LOGGED_IN = "scs.log_ins"
SCS_METRIC_NUM_USERS_LOGGED_OUT = "scs.log_outs"

SCS_NUM_TASKS_SUCCEEDED_ON_REMOTE_AGENT = "scs.tasks_succeeded_on_remote_agent"
SCS_NUM_TASKS_FAILED_ON_REMOTE_AGENT = "scs.tasks_failed_on_remote_agent"
SCS_NUM_TASKS_CANCELLED = "scs.tasks_cancelled"
SCS_NUM_TASKS_STARTED = "scs.tasks_started" #
SCS_NUM_TASKS_FAILED = "scs.tasks_failed" #

SCS_NUM_TASKS_FAILED_ON_REMOTE_AGENT = "scs.tasks_failed_on_remote_agent"
SCS_METRIC_NUM_UPLOADS = "scs.file_uploads"
SCS_METRIC_NUM_DOWNLOADS = "scs.file_downloads"
SCS_METRIC_NUM_DELETES = "scs.file_deletes"
SCS_METRIC_NUM_DELETE_RECUR = "scs.file_delete_recur"
SCS_METRIC_NUM_LISTS = "scs.file_lists"
SCS_METRIC_NUM_MOVES = "scs.file_moves"
SCS_METRIC_NUM_RENAMES = "scs.file_renames"
SCS_METRIC_NUM_MKDIRS = "scs.file_mkdir"
SCS_METRIC_NUM_ZIP = "scs.file_zip"
SCS_METRIC_NUM_UNZIP = "scs.file_unzip"

# Not implemented
SCS_NUM_REST_CALLS = "scs.num_rest_calls"
SCS_NUM_REST_BYTES_DOWNLOAD = "scs.num_rest_bytes_download"
SCS_NUM_REST_BYTES_UPLOAD = "scs.num_rest_bytes_upload"
SCS_OPS_REJ_DUE_TO_AGENT_DOWN = "scs.ops_rejected_due_to_agent_down" #
SCS_OPS_REJ_DUE_TO_AGENT_LOCKED = "scs.ops_rejected_due_to_agent_locked" #

