angular.module( 'rpsmarf.resourceconfig', [
  'rpsmarf.constants',
  'rpsmarf.config',
  'rpsmarf.user.service',
  'restangular',
  'ui.router',
  'rpsmarf.foldernav',
  'ui.bootstrap',
  'rpsmarf.container.resource.service',
  'rpsmarf.task.create.service',
  'rpsmarf.extendedinfo',
  'rpsmarf.file.editor',
  'rpsmarf.accessrequest',
  'rpsmarf.resourceconfig.service'
  
])

.config(function config( $stateProvider, RestangularProvider, SMARF_URL ) {
  
  $stateProvider.state('resourceconfig', {
    url: '/resources/',
    views: {
      'main': {
        controller: 'ResourceConfigCtrl',
        templateUrl: 'resourceconfig/resourceconfig.tpl.html'
      },
      'footer': {
        templateUrl: 'resourceconfig/resourceconfig.infopanel.tpl.html',
        controller: 'ResourceConfigInfoPanelCtrl'
      }
    },
    abstract: true,
    data: {pageTitle: 'Resources'}

  })
  .state('resourceconfig.taskstart', {
    url: '^/resource/:resourceInputName/'
    
  })
  .state('resourceconfig.browse',{
    url: ''
  })
  .state('resourceconfig.browse.upload',{
    url: 'upload/'
  });
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'ResourceConfigCtrl', ['$scope', 'Restangular', '$modal', 'SmUser', '$state', 'SmTaskCreate', 'USER_STATE', 'filterFilter', 'SmContainerResource', 'SmResourceConfig','ROOT_FOLDER_ENTRY', 'SmDisqus',
  function ( $scope, Restangular, $modal, SmUser, $state, SmTaskCreate, USER_STATE, filterFilter, SmContainerResource, SmResourceConfig, ROOT_FOLDER_ENTRY, SmDisqus) {

  /***PRE-EXE***/
  var usn = null;
  var userState = SmUser.getProperty('state');
  for(var k in USER_STATE){
    if(USER_STATE[k] === userState){
      usn = k;
      break;
    }
  }
  if($state.is('resourceconfig.taskstart') && userState === USER_STATE.browse){
    $state.go('home');
    return;
  }
  /***VARIABLES***/
  /*Standard*/
  var uid = SmUser.getProperty('id');
  var community = SmUser.getProperty('community').name_key;
  var resourceParameter = SmTaskCreate.getActiveResourceParameter();
  var taskType = SmTaskCreate.getTaskType();
  var selectedResourceIndex = null;
  var resourceApi;
  var allResourceApi = Restangular.one('resource/?sortForUser='+uid+'&communityFilter='+community+'&extForUser='+uid+'&limit=0');
  var allRootEntries = [];
  var allResources = [];
  var userPermMap = null;
  var userMap = {};
  var rootFolderEntry = angular.copy(ROOT_FOLDER_ENTRY);
  var publicCloudFolders = null;
  

  //Map resource api to 1 since the returned results for /resource/ is an object that contains
  //the list of resources
  if(userState === USER_STATE.task_start){
    resourceApi = Restangular.one('resource/?resource_type__id='+resourceParameter.resource_type.id+'&sortForUser='+uid+'&communityFilter='+community+'&extForUser='+uid+'&limit=0');  
  }
  
  
  var num = 0;

  /*SCOPE VARIABLES*/
  $scope.USER_STATE = USER_STATE;
  $scope.userState = userState;
  $scope.userStateName = usn;
  $scope.resourceSelected = 'false';
  $scope.resources = [];
  $scope.selectedResource = null;
  $scope.userSelected = false;
  $scope.allEntriesSelected = false;
  $scope.resourceParameter = resourceParameter;
  $scope.rootFolderSelected = false;
  $scope.entriesSearch = 'false';
  $scope.numUsersSelected =0;
  $scope.resourceSearchText = '';
  $scope.resourcesToShow = 'default';
  
  //$scope.select = {first: false};
 
  if(userState === USER_STATE.task_start){
    $scope.resource_nature = resourceParameter.resource_type.nature.indexOf('stream') >=0 ? 'stream' : resourceParameter.resource_type.nature;
    $scope.resourceParameterName = resourceParameter.name;
  }
  
  //$scope.resourceInputName = $stateParams.resourceInputName;


  /***FUNCTIONS***/
  /*Scope*/
  $scope.selectResource = function(resource, index){

    var userResourceAccess, now;    
    if(resource.selected){
      return false;
    }
    else{
      if(selectedResourceIndex !== null){
        $scope.resources[selectedResourceIndex].selected = false;
      }
      
      selectedResourceIndex =index;
      resource.selected = true;
      $scope.resourceSelected = "true";
      $scope.selectedResource = resource;
      userResourceAccess = $scope.userHasAccess(resource);
      rootFolderEntry = angular.copy(ROOT_FOLDER_ENTRY);
      if((userState === USER_STATE.task_start && $scope.resource_nature === 'data') || (userState === USER_STATE.browse && resource.nature === "data")){
        if(userResourceAccess === 'true'){
          Restangular.all('resource/'+resource.id+'/list/').get('').then(function(objects){
            objects.unshift(rootFolderEntry);
            allRootEntries = objects;
            $scope.selectedResource.entries = objects;
            $scope.selectedResource.selectedEntry = null;
          }, function(response){
            if(response.status == 410){
              $scope.refreshResources();
            }
            else{
              //attempt to get non cached resource list response
              if(userResourceAccess === 'true' && !resource.locked && resource.status === 'up'){
                now = new Date();
                now = now.getTime();
                Restangular.all('resource/'+resource.id+'/list/?nc='+now).get('').then(function(objects){
                  console.log('got non-cached resource list');
                  objects.unshift(rootFolderEntry);
                  allRootEntries = objects;
                  $scope.selectedResource.entries = objects;
                  $scope.selectedResource.selectedEntry = null;
                });
              }
            }
            
          });
        }
        

        Restangular.one('user',resource.owner.split('/')[3]+'/').get().then(function(response){
          var name = response.first_name + ' ' + response.last_name;
          $scope.selectedResource['owner_info'] = {name: name, email: response.email};
        });
      }
      else if(userState === USER_STATE.browse && resource.nature === 'compute'){
        resource.runnableTools = [];
        Restangular.all('resource_type_task_type/?resource_type__id='+resource.resource_type.split('/')[3]).get('').then(function(response){
          var objects = response.objects, o;

          for(var i = objects.length-1; i>=0; i--){
            o = objects[i];
            getToolObject(resource,o.task_type.split('/')[3]);
          }
        });
      }

      $scope.$emit('SmObjectSelectInfoPanel', $scope.selectedResource);
      
    }
      
  };

  $scope.refreshResources = function(){
   
    initiateResources();
    
    if(selectedResourceIndex !== null){
      selectedResourceIndex = null;
      allRootEntries = null;
      $scope.selectedResource = null;
      $scope.resourceSelected = 'false';
      $scope.allEntriesSelected = false;
    }
      
    
  };

  $scope.userAccess = function(user, action){
    if(user[action] && user[action] === "true"){
      return 'fa-check';
    }
    else{
      return 'fa-ban';
    }
  };

  $scope.selectUser = function(user){

    if(user.selected){
      numUsersSelected++;
    }
    else{
      numUsersSelected--;
    }
  };

  $scope.isDisabled = function(user, action){

    switch(action){
      case 'all':
        return user.read === "true" && user.write === "true";
      case 'readonly':
        return user.read === "true";
      case 'revoke':
        return user.read === "false" && user.write === "false";
      default:
        break;
    }
  };

  $scope.getId = function(){

    return num++;
  };

  $scope.userHasAccess = function(resource){

    if(resource.locked){
      return "locked";
    }
    else if((!resource.ext_perms || resource.ext_perms === "") && (resource.owner.split('/')[3] != uid)){
      return "false";
    }
    else{
      return "true";
    }
  };

  $scope.entryLs = function(resource, entry){

    Restangular.all('resource/'+$scope.selectedResource.id+'/'+entry.name +'list/').get('').then(function(objects){
      entry.entries = objects;
    }, function(response){
      entry.expanded = false;
      $modal.open({
        controller: 'AlertCtrl',
        templateUrl: 'alert/alert.tpl.html',
        resolve: {
          title: function(){ return 'Error';},
          message: function(){ return 'Could not list the contents of '+entry.basename+' due to error:<br>' + response.data.description;}
        }
      });
      
    });
    
    
  };

  $scope.selectEntry = function(entry){
    //console.log('in select!');
    if(entry){
      var parent = entry.name.substring(0,entry.name.indexOf(entry.basename));
      var dateModified = new Date(Math.ceil(entry.mtime*1000));
      var size;
      //console.log(dateModified);
      if(!entry.isDir){
        size = entry.size/1024;
        if(size > 1000){
          size = (size/1024).toFixed(2) + " MB";
        }
        else{
          size = size.toFixed(2) + " KB";
        }
      }
      else{
        size = '---';
      }
      
      var entryObj = entry; //{name:entry.basename,parent: parent,  size:size, dateModified: dateModified.toDateString(), path: entry.name};
      entryObj.parent = parent, entryObj.sizeText = size, entryObj.dateModified = entry.isRoot ? '' : dateModified.toDateString();
      $scope.selectedResource.selectedEntry = entryObj;
    }
    else{
      $scope.selectedResource.selectedEntry = null; 
    }

    //console.log($scope.selectedResource.selectedEntries);
    
  };

  $scope.seeSelected = function(){
    console.log($scope.selectedResource.selectedEntries);
  };
  $scope.done = function(){

    var entry, warnModal, percentageUsed,json;
    
    if($scope.rootFolderSelected){
      entry = {basename: '',
          isDir: true,
          mtime: 0.0,
          name: '',
          size: 0.0,
          isRoot: true
      };
    }
    else if($scope.resource_nature !== 'stream'){
      entry = $scope.selectedResource.selectedEntry;
    }
    
    if(resourceParameter.role == 'dest' && $scope.selectedResource.full)
    {
      $modal.open({
        templateUrl: 'alert/alert.tpl.html',
        controller: 'AlertCtrl',
        resolve: {
          message: function(){return 'Destination folder is full. Please select another output location';},
          title: function(){return 'Error';}
        }
      });

      return false;
    }
    else if(resourceParameter.role == 'dest' && $scope.selectedResource.warn){
      json = JSON.parse($scope.selectedResource.capacityJson);
      percentageUsed = (json.diskUsed/json.diskUsedFailThreshold*100).toFixed(0);
      warnModal = $modal.open({
        controller: 'ConfirmCtrl',
        templateUrl: 'confirm/confirm.tpl.html',
        resolve: {
          title: function(){ return 'Warning';},
          body: function(){ return 'Destination folder is at '+ percentageUsed +'% capacity. Do you want to continue?';}
        }
      });
    }

    if(warnModal){
      warnModal.result.then(function(){
        verifySelectedEntry(entry);
      });
    }
    else{
      verifySelectedEntry(entry);
    }

    
    
    
  };

  $scope.refresh = function(selectedResource){
    SmContainerResource.refresh(selectedResource);  
  };

  $scope.download = function(selectedResource, $event){
    var p;
    if(!selectedResource.selectedEntry || selectedResource.ext_perms == 'x'){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    
    p = SmContainerResource.download(selectedResource);

    if(p){
      p.then(function(taskInfo){
        $scope.$emit('SmTaskStart',taskInfo.taskObj, taskInfo.taskName, taskInfo.interactive, taskInfo.additionalInfo);
      }, function(){
        
      });
    }
  };

  $scope.deleteSelection = function(selectedResource, $event){
    if(!selectedResource.selectedEntry || selectedResource.selectedEntry.isRoot){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    var promise = SmContainerResource.deleteSelection(selectedResource);
    if(selectedResource.selectedEntry.isDir){
      promise.then(function(taskInfo){

        if(taskInfo){
          $scope.$emit('SmTaskStart',taskInfo.taskObj, taskInfo.taskName, taskInfo.interactive);
        }
        else{
          console.log('Error when receiving task info from deleteSelection');
        }
        
      });
    }

  };

  $scope.mkdir = function(selectedResource, $event){
    if(selectedResource.selectedEntry && !selectedResource.selectedEntry.isDir){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    SmContainerResource.mkdir(selectedResource);
  };

  $scope.upload = function(selectedResource, $event){
    if(selectedResource.selectedEntry && !selectedResource.selectedEntry.isDir){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    SmContainerResource.upload(selectedResource);
  };

  $scope.rename = function(selectedResource, $event){
    if(!selectedResource.selectedEntry || selectedResource.selectedEntry.isRoot){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    SmContainerResource.rename(selectedResource);
  };

  $scope.cancel = function(){
    if(SmTaskCreate.isCurrentTaskReservation()){
      $state.go('taskstart.reservation',{tooltask_namekey:taskType.name_key});
    }else{
      $state.go('taskstart',{tooltask_namekey:taskType.name_key});
    }
    
  };

  $scope.filterRootEntries = function(searchText){
    var text = searchText.toLowerCase();

    if(!text || text === ''){
      $scope.entriesSearch = 'false';
    }
    else{
      $scope.entriesSearch = 'true';
    }
    $scope.selectedResource.entries = filterFilter(allRootEntries, {name:searchText}, false);
    $scope.selectedResource.selectedEntry = null;
  };

  $scope.toggleRootFolderSelected = function(){
    //var entry;
    $scope.rootFolderSelected = !$scope.rootFolderSelected;

    if($scope.rootFolderSelected){
      $scope.selectedResource.selectedEntry = null;
    }
  };

  $scope.filterResources = function(searchText){
    var text = searchText.toLowerCase();
    var resourcesToFilter = $scope.resourcesToShow === 'default' ? allResources : publicCloudFolders;
    $scope.resources = filterFilter(resourcesToFilter, {name:searchText}, false);
  };

  $scope.favouriteToggle = function(resource){
    if(resource.ext_favourite){
      Restangular.all('favourite_resource/').remove({owner__id:uid, resource__id: resource.id}).then(function(){
        resource.ext_favourite = false;
      });
    }
    else{
      Restangular.all('favourite_resource/').post({owner:'/scs/user/'+uid+'/', resource: '/scs/resource/'+resource.id+'/'}).then(function(){
        resource.ext_favourite = true;
      });

    }
  };

  $scope.openNotesEditor = function(resource){
    var exists = angular.isDefined(resource.ext_note) ? true : false;
    $modal.open({
      controller: 'ExtendedInfoCtrl',
      templateUrl: 'extendedinfo/extendedinfo.tpl.html',
      resolve: {
        tagsObject: function(){ return null;},
        noteObject: function(){ return {typeName: 'resource',typeId: resource.id, ownerId: uid, exists: exists, noteText: resource.ext_note, typeObject: resource };}
      }
    });
  };

  $scope.editFile = function(selectedResource, $event){
    if(!selectedResource.selectedEntry || selectedResource.selectedEntry.isDir){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    SmContainerResource.editFile(selectedResource);
  };

  $scope.zip = function(selectedResource, $event){
    if(selectedResource.selectedEntry && !selectedResource.selectedEntry.isDir){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    var p = SmContainerResource.zip(selectedResource);

    if(p){
      p.then(function(taskInfo){
        $scope.$emit('SmTaskStart',taskInfo.taskObj, taskInfo.taskName, taskInfo.interactive);
      }, function(response){
        $modal.open({
          controller: 'AlertCtrl',
          templateUrl: 'alert/alert.tpl.html',
          resolve: {
            title: function(){ return 'Error'; },
            message: function(){ return response.data.description; }
          }
        });
        console.log('promise rejected in zip');
      });
    }
    
  };

  $scope.unzip = function(selectedResource, $event){
    if(!selectedResource.selectedEntry || selectedResource.selectedEntry.isDir || selectedResource.selectedEntry.basename.indexOf('.zip') < 0){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    var p = SmContainerResource.unzip(selectedResource);

    if(p){
      p.then(function(taskInfo){
        $scope.$emit('SmTaskStart',taskInfo.taskObj, taskInfo.taskName, taskInfo.interactive);
      },function(){
        console.log('promise rejected in unzip');
      });
    }
  };

  $scope.requestAccess = function(resource){
    var type='resource', val, res = angular.copy(resource);
    

    if(!userPermMap){
      Restangular.one('resource',resource.id+'/getperm/').get().then(function(map){
        map = map.plain();
        userPermMap = map;
        openRequestAccessForm(resource);
      });
    }
    else{
      openRequestAccessForm(resource);
    }
  };

  $scope.showPublicCloudFolders = function(){
    var resources;
    if($scope.resourcesToShow === 'publicCloudFolders'){
      return;
    }

    $scope.resources = publicCloudFolders;
    
    $scope.resourceSearchText = '';
    $scope.resourcesToShow = 'publicCloudFolders';
  };

  $scope.showDefaultResources = function(){
    if($scope.resourcesToShow === 'default'){
      return;
    }

    $scope.resources = allResources;
    $scope.resourcesToShow = 'default';
    
  };

    /*Standard Fns*/
  var copyResource = function(resource){
    var r = {};
    var v = null;
    for(var key in resource){
      if(key.indexOf('$') >=0 || key === 'selectedEntry' || key === 'entries'){
        continue;
      }
      r[key] = resource[key];
    }

    return r;
  };

  var initiateResources = function(){
    var i = 0;
    if(userState == USER_STATE.task_start){
      resourceApi.get().then(function(response){
        allResources = response.objects;
        processCapacityForResources(allResources);
        $scope.resources = allResources;
      });
    }
    else{
      allResourceApi.get().then(function(response){
        if($state.is('resourceconfig.browse.upload')){
          allResources = filterFilter(response.objects,function(value,index){return value.nature === 'data';},false);
        }
        else{
          allResources = filterFilter(response.objects,function(value,index){return value.nature === 'data' || value.nature === 'compute';},false);  
        }
        
        processCapacityForResources(allResources);
        $scope.resources = allResources;

        if(SmResourceConfig.isResourceSelected()){
          var resourceId = SmResourceConfig.getResourceId();
          var r, foundResource;
          for(i= 0; i< $scope.resources.length; i++){
            r = $scope.resources[i];
            if(r.id == resourceId){
              foundResource = true;
              break;
            }
          }

          if(foundResource){
            r.selected = true;
            $scope.resourceSelected = 'true';
            $scope.selectedResource = r;

            Restangular.all('resource/'+resourceId+'/list/').get('').then(function(objects){
              objects.unshift(rootFolderEntry);
              allRootEntries = objects;
              $scope.selectedResource.entries = objects;
              $scope.selectedResource.selectedEntry = null;
            });
          }
        }
        
      });
    }

    Restangular.all('resource/?limit=0&personalFolder=True&name_key__icontains=public&extForUser='+uid).get('').then(function(response){
      var resources = response.objects;
      processCapacityForResources(resources);
      publicCloudFolders = resources;
    });

    $scope.resourcesToShow = 'default';
  };

  var processCapacityForResources = function(resources){
    var obj,i,json;
    for(i= 0; i< resources.length; i++){
      obj = resources[i];
      if(obj.personalFolder && obj.capacityJson){
        json = JSON.parse(obj.capacityJson);
        resources[i].full = (json && json.diskUsedState && json.diskUsedState === 'full');
        resources[i].warn = (json && json.diskUsedState && json.diskUsedState === 'warn' );
      }else{
        resources[i].full = false;            
      }
    }
  };

  var openRequestAccessForm = function( res){
    var userPerm = [{name:'read', hasPerm: false},{name:'write', hasPerm: false},{name:'execute', hasPerm: false}];
    if(userPermMap['/scs/user/'+uid+'/']){
      for( var i=userPermMap['/scs/user/'+uid+'/'].length-1;i>=0;i--){
        val = userPermMap['/scs/user/'+uid+'/'][i];
        if(val.indexOf('read') >= 0){
          userPerm[0].hasPerm = true;
        }
        else if(val.indexOf('write') >= 0){
          userPerm[1].hasPerm = true;
        }
        else if(val.indexOf('execute') >= 0){
          userPerm[2].hasPerm = true;
        }
      }
    }

    res.userPerm = userPerm;

    $modal.open({
      controller: 'AccessRequestCtrl',
      templateUrl: 'accessrequest/accessrequest.tpl.html',
      resolve:{
        type: function(){ return 'resource';},
        object: function(){ return res;}
      }
    });

  };

  var getToolInfo = function(resource, rtid){
    Restangular.all('resource_type_task_type/?resource_type__id='+rtid).get('').then(function(response){
      var objects = response.objects, o;
      for(var i = objects.length-1; i>=0; i--){
        o = objects[i];
        getToolObject(resource, o.task_type.split('/')[3]);
      }
    });
  };

  var getToolObject = function(resource, ttid){
    Restangular.one('task_type/'+ttid+'/').get().then(function(tool){
      tool = tool.plain();
      resource.runnableTools.push(tool);
      getOwnerInfo(tool, tool.owner.split('/')[3]);

    });
  };

  var getOwnerInfo = function(tool, uid){
    var o;
    if(userMap[uid]){
      o = userMap[uid];
      tool.ownerName = o.first_name + ' ' + o.last_name;
      tool.ownerEmail = o.email;
    }
    else{
      Restangular.one('user/'+uid+'/').get().then(function(owner){
        owner = owner.plain();
        userMap[owner.id] = owner;
        tool.ownerName = owner.first_name + ' ' + owner.last_name;
        tool.ownerEmail = owner.email;
      });
    }
    
  };

  var verifySelectedEntry = function(entry){
    var message, selectionTypeError=false;
    if($scope.resource_nature !== 'stream' && !resourceParameter.isCompute && !entry){
      $modal.open({
        controller: 'AlertCtrl',
        templateUrl: 'alert/alert.tpl.html',
        resolve: {
          message: function(){ return 'This parameter requires you to select a file or folder';},
          title: function(){return null;}
        }
      });

      return;
    }

    if(entry && entry.isDir && resourceParameter.pathType === 'file' && resourceParameter.direction === 'input'){
      selectionTypeError = true;
      message = 'This parameter requires you to select a file. You have selected a folder.';
    }else if(entry && !entry.isDir && resourceParameter.pathType === 'folder'){
      selectionTypeError = true;
      message = 'This parameter requires you to select a folder. You have selected a file.';
    }

    if(selectionTypeError){
      $modal.open({
        controller: 'AlertCtrl',
        templateUrl: 'alert/alert.tpl.html',
        resolve: {
          message: function(){ return message;},
          title: function(){return null;}
        }
      });
      return;
    }
    
    var resourceCopy = copyResource($scope.selectedResource);
    if($scope.resource_nature !== 'stream' && !resourceParameter.isCompute){
      resourceCopy.path = entry.name;
    }

    SmTaskCreate.setValueForActiveResourceParameter(resourceCopy);
    if(SmTaskCreate.isCurrentTaskReservation()){
      $state.go('taskstart.reservation', {tooltask_namekey: SmTaskCreate.getTaskType().name_key});
    }else{
      $state.go('taskstart', {tooltask_namekey: SmTaskCreate.getTaskType().name_key});
    }
  };
  
  /***EXE***/
  initiateResources();

  $scope.$on('SmCommunityChange', function(event){
    community = SmUser.getProperty('community').name_key;
    allResourceApi = Restangular.one('resource/?sortForUser='+uid+'&communityFilter='+community+'&extForUser='+uid+'&limit=0');
    initiateResources();
  });

  //if($scope.disqusEnabled && !SmDisqus.getEmbedInitialized()){
    //SmDisqus.initDisqusEmbed();
  //}
  
}])
.controller('ResourceConfigInfoPanelCtrl', ['$scope', 'Restangular', '$modal', 'SmUser', '$state', 'SmTaskCreate', 'USER_STATE', 'filterFilter', 'SmContainerResource', 'SmResourceConfig', 'SmDisqus',
  function ( $scope, Restangular, $modal, SmUser, $state, SmTaskCreate, USER_STATE, filterFilter, SmContainerResource, SmResourceConfig, SmDisqus) {

    /***STANDARD FUNCTIONS***/
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var uid = SmUser.getProperty('id');

    /***SCOPE VARIABLES***/
    //$scope.infoPanel = {collapsed: false};
    $scope.selectedResource = null;
    $scope.select = {first: false};
    $scope.userAccessRequests = null;

    /***SCOPE FUNCTIONS***/
    $scope.infoPanelCollapseToggle = function(){
      $scope.infoPanel.collapsed = !$scope.infoPanel.collapsed;
      $scope.$emit('SmInfoPanelToggle', $scope.infoPanel.collapsed);
    };

    $scope.getPermissions = function(selectedResource){
      var perms, isOwner = false, i, user,permObj, userFullPerm = false;
      selectedResource.users_perm = [];
      if(selectedResource.owner.split('/')[3] == uid){
        isOwner = true;
      }
      selectedResource.userIsOwner = isOwner;
      Restangular.one('resource',selectedResource.id+'/getperm/').get().then(function(map){
        map = map.plain();
        userPermMap = map;
        for( var key in map){
          user = {isCurrentUser : key.split('/')[3] == uid, editing: false};
          permObj = {};
          perms = map[key];
          getUserInfo(key,user);
          user.read = false, user.write = false, user.execute= false;
          permObj['read'] = false, permObj['write'] = false, permObj['execute'] = false;
          for(i=perms.length-1; i>=0; i--){
            if(perms[i] === 'read_resource_data'){
              user.read = true;
              permObj['read'] = true;
            }
            if(perms[i] === 'write_resource_data'){
              user.write = true;
              permObj['write'] = true;
            }
            if(perms[i] === 'execute_on_resource'){
              user.execute = true;
              permObj['execute'] = true;
            }
          }
          user['perms'] = permObj;
          if(user.isCurrentUser){
            selectedResource.userFullPerm = selectedResource.nature === 'compute'? user.execute : user.execute && user.write && user.read;
          }
          
          selectedResource.users_perm.push(user);
        }
      });

      Restangular.all('request/?target='+selectedResource.resource_uri).get('').then(function(response){
        if(response.objects.length < 1){
          return;
        }
        else{
          selectedResource.hasAccReq = true;
        }
        var req, userObj;
        for(var i=response.objects.length-1; i>=0; i--){
          req = response.objects[i];
          req.requester_info = {name:null, id:null};
          req.access = '';
          getUserInfo(req.requester, req.requester_info);
          if(req.access_requested.indexOf('r')>=0){
            req.access += 'read';
          }
          if(req.access_requested.indexOf('w')>=0){
            req.access += req.access === '' ? 'write': ', write';
          }
          if(req.access_requested.indexOf('x')>=0){
            req.access += req.access === '' ? selectedResource.nature === 'data' ? 'available for tool input' : 'execute': selectedResource.nature === 'data' ? ', available for tool input' : ', execute';
          }

        }

        $scope.userAccessRequests = response.objects;
      });
    };

    $scope.toggleEditPermissions = function(user, selectedResource, index){
      var assignOperations = '', removeOperations = '';

      if(user.editing){
        if(user.read !== user.perms.read){
          if(user.read){
            assignOperations += 'r';
          }
          else{
            removeOperations += 'r';
          }
        }
        if(user.write !== user.perms.write){
          if(user.write){
            assignOperations += 'w';
          }
          else{
            removeOperations += 'w';
          }
        }
        if(user.execute !== user.perms.execute){
          if(user.execute){
            assignOperations += 'x';
          }
          else{
            removeOperations += 'x';
          }
        }
        user.perms.read = user.read; user.perms.write = user.write; user.perms.execute = user.execute;
        if(assignOperations !== ''){
          Restangular.one('resource',selectedResource.id+'/setperm/').get({user:'/scs/user/'+user.id+'/',perm:assignOperations, action:'assign', notify: true}).then(function(){
            console.log('assign operation complete');
          });
        }

        if(removeOperations !== ''){
          Restangular.one('resource',selectedResource.id+'/setperm/').get({user:'/scs/user/'+user.id+'/',perm:removeOperations, action:'remove', notify: true}).then(function(){
            console.log('remove operation complete');
          });

          if(!user.perms.read && !user.perms.write && !user.perms.execute){
            selectedResource.users_perm.splice(index, 1);
          }
        }
      }
      user.editing = !user.editing;


    };

    $scope.getUsageInfo = function(resource){
      var usages = [], users = [];
      var user, u, d;
      Restangular.one('resource', resource.id+'/getusage/').get({user:uid}).then(function(response){
        r = response.plain();
        if(!r.total){
          response['total'] = {w:0,m:0,y:0};
        }
        if(!r.user){
          response['user'] = {w:0,m:0,y:0};
        }
        r.total.user_name = 'All users';
        r.user.user_name = 'You';
        usages.push(response.user);
        usages.push(response.total);
        resource.usage_summary = usages;
      });
      Restangular.one('resource', resource.id+'/getusers/').get().then(function(response){
        response = response.plain();
        for(var key in response){
          u = response[key];
          u.title = u.title ? u.title: '';
          d = new Date(u.last_use);
          u.last_use = months[d.getMonth()] + ' ' + d.getDate() + ' ' + d.getFullYear();
          user = {user_name: u.title + ' ' + u.first_name + ' ' + u.last_name, email: u.email, use_count: u.use_count, last_use: u.last_use};
          users.push(user);
        }
        resource.usage_users = users;
      });
    };

    $scope.requestAccess = function(resource){
      var type='resource', val, res = angular.copy(resource);
      

      if(!userPermMap){
        Restangular.one('resource',resource.id+'/getperm/').get().then(function(map){
          map = map.plain();
          userPermMap = map;
          openRequestAccessForm(resource);
        });
      }
      else{
        openRequestAccessForm(resource);
      }
    };

    $scope.grantAccess = function(request, index){
      console.log(request);
      Restangular.one('resource',$scope.selectedResource.id+'/setperm/').get({user:request.requester,perm:request.access_requested, action:'assign', notify: true}).then(function(){
        console.log('access granted');
        var user = {isCurrentUser : false, editing: false};
        var permObj = {}, userInUserPerms=false,i, u;
        console.log($scope.selectedResource.user_perms);
        if($scope.selectedResource.users_perm)
        {
          for(i = $scope.selectedResource.users_perm.length-1; i>=0; i--){
            if($scope.selectedResource.users_perm[i].id == request.requester.split('/')[3]){
              u = $scope.selectedResource.users_perm[i];
              userInUserPerms = true;
              if(request.access_requested.indexOf('r') >=0){
                u.read = true;
                u.perms['read'] = true;
              }
              if(request.access_requested.indexOf('w') >=0){
                u.write = true;
                u.perms['write'] = true;
              }
              if(request.access_requested.indexOf('x') >=0){
                u.execute = true;
                u.perms['execute'] = true;
              }
            }  
          }
        }
        
        if(!userInUserPerms){
          getUserInfo(request.requester,user);
          user.read = false, user.write = false, user.execute= false;
          permObj['read'] = false, permObj['write'] = false, permObj['execute'] = false;
          
          if(request.access_requested.indexOf('r') >=0){
            user.read = true;
            permObj['read'] = true;
          }
          if(request.access_requested.indexOf('w') >=0){
            user.write = true;
            permObj['write'] = true;
          }
          if(request.access_requested.indexOf('x') >=0){
            user.execute = true;
            permObj['execute'] = true;
          }
          
          user['perms'] = permObj;

          $scope.selectedResource.users_perm.push(user);
        }
        

        Restangular.one('request',request.id+'/').remove().then(function(){
          $scope.userAccessRequests.splice(index,1);
        });
      });
      
    };

    $scope.denyAccess = function(request, index){
      Restangular.one('request',request.id+'/').remove({deny:true}).then(function(){
        console.log('access denied');
        $scope.userAccessRequests.splice(index,1);
      });
    };

    $scope.calculateFolderSize = function(resource, entry){
      entry.sizeText = 'calculating...';
      var path = entry.name.substring(0,entry.name.length-1), size;
      Restangular.all('resource/'+resource.id+'/'+encodeURIComponent(path)+'/getfoldersize/').get('').then(function(response){
        if(response.result === 'ok'){
          size = response.sizeInBytes/1024;
          if(size > 1000){
            size = (size/1024).toFixed(2) + " MB";
          }
          else{
            size = size.toFixed(2) + " KB";
          }
          entry.sizeText = size + ' ('+response.fileCount + ' total files/folders)';
        }
        else{
          entry.sizeText = 'Error';
        }
      },function(response){
        entry.sizeText = 'Error';
      });
    };

    $scope.loadComments = function(resource){
      if(!SmDisqus.getEmbedInitialized()){
        SmDisqus.initDisqusEmbed('resource/'+resource.name_key,location.origin+'/#!'+resource.name_key,resource.name);
      }
      else{
        DISQUS.reset({
          reload: true,
          config: function () {
              this.page.identifier = 'resource/'+resource.name_key;
              this.page.url = location.origin+'/#!'+resource.name_key;
              this.page.title = resource.name;
              //this.language = 'en';
          }
        });
      }
      
    };

    /***STANDARD FUNCTIONS***/
    var getUserInfo = function(idPath, userObj){
      var id = idPath.split('/')[3], extra='';
      Restangular.one('user',id+'/').get().then(function(response){
        if(id == uid){
          extra = ' (You)';
        }
        userObj.id = id;
        userObj.name = response.first_name + ' ' + response.last_name + extra;
      });
    };

    var openRequestAccessForm = function( res){
      var userPerm = [{name:'read', hasPerm: false},{name:'write', hasPerm: false},{name:'execute', hasPerm: false}];
      if(userPermMap['/scs/user/'+uid+'/']){
        for( var i=userPermMap['/scs/user/'+uid+'/'].length-1;i>=0;i--){
          val = userPermMap['/scs/user/'+uid+'/'][i];
          if(val.indexOf('read') >= 0){
            userPerm[0].hasPerm = true;
          }
          else if(val.indexOf('write') >= 0){
            userPerm[1].hasPerm = true;
          }
          else if(val.indexOf('execute') >= 0){
            userPerm[2].hasPerm = true;
          }
        }
      }

      res.userPerm = userPerm;

      $modal.open({
        controller: 'AccessRequestCtrl',
        templateUrl: 'accessrequest/accessrequest.tpl.html',
        resolve:{
          type: function(){ return 'resource';},
          object: function(){ return res;}
        }
      });

    };

    /***EXE***/
    $scope.$on('SmInfoPanelUpdate', function(event, selectedResource){
      $scope.selectedResource = selectedResource;
      $scope.select.first = true;
    });

  }]);

