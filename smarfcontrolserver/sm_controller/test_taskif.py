'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 10, 2015

    @author: rpsmarf
'''

import unittest
from sm_controller.models import SmModelResource, \
    SmModelTaskType, SmModelTask, SmModelResourceTypeTaskType, SmModelAgent,\
    SmModelResourceType, SmModelContainer,\
    SmModelScript, SmModelReservation
from django.test.testcases import TransactionTestCase
from sm_controller.test_helper import ScsTestHelper, DUMMY_DESCRIPTION
from smcommon.utils.date_utils import sm_date_now, smContainsDateString
from django.contrib.auth.models import User
import json
from datetime import timedelta
from smarfcontrolserver.init.envvar import scsClearEnvVar
from smcommon.globals.envvar import smCommonClearEnvVar,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_PREP_COUNT,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_SLEEP_TIME,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RUNNING_COUNT,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_CLEANUP_COUNT,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RESULT, SM_ENV_VAR_COMMON_GIT_CHECKOUT_COMMAND,\
    SM_ENV_VAR_COMMON_GIT_FETCH_COMMAND,\
    SM_ENV_VAR_COMMON_GIT_FETCH_TAGS_COMMAND,\
    SM_ENV_VAR_COMMON_GIT_RESET_COMMAND
from sm_controller import test_helper
import time
from smcommon.task_runners.mock_async_ops_server import SmMockAsyncOpsServer,\
    SmMockOpHandlerResponder
from smcommon.globals.remote_agent_factory import smRemoteAgentAsyncOperationFactory
import os
from smarfcontrolserver.pollers.TmpFilePurger import ScsTmpFilePurger
from smarfcontrolserver.pollers.TaskAgeOut import ScsTaskAgeOut
from smcommon.utils import date_utils
import logging
from sm_controller.signal_handlers import smTriggerSignalInstall
from smcommon.constants.sm_const import SM_TASK_TYPE_DELETE_RECURSIVE_NAME_KEY
from sm_controller.api import HttpError422
from smcommon.task_runners.mock_sync_ops_server import SmMockSyncOpsServer

logger = logging.getLogger(__name__) 
smTriggerSignalInstall()


class TaskTest(TransactionTestCase):

    def setUp(self):
        scsClearEnvVar()
        smCommonClearEnvVar()
        self.helper = ScsTestHelper(self)
        self.helper.setupTempFolder()
        self.helper.setupModel()
        self.helper.setupRestAuth("root")
        self.helper.setupRemoteAgentViaIce() 
                
        self.container = SmModelContainer.objects.get(name="test_container")
        self.container.containerUrl = "local://default:" + str(self.helper.icePort) + self.helper.tempFolder.folder + "/"
        self.container.save()
        self.resource = SmModelResource.objects.get(name="resourcexxx")
        self.resource.parametersJson = json.dumps({"folder": "r_folder/"})
        self.resource.save()
        self.rttt = SmModelResourceTypeTaskType.objects.get(role="compute")
        self.rttt_src = SmModelResourceTypeTaskType.objects.get(role="src")
        self.rttt_dest = SmModelResourceTypeTaskType.objects.get(role="dest")
            
    def tearDown(self):
        self.helper.tearDown()
        TransactionTestCase.tearDown(self)
        smCommonClearEnvVar()
        
    def test_personalCloudFeature(self):
        self.helper.setupTempFolder()
        
        # This method tests the side effects of adding a user which are related
        # to cloud folders
        self.helper.setupModel()
        self.helper.setupRemoteAgentViaIce()
        
        # Create user "z"
        user_path = self.helper.objCreateViaRest("/scs/user/",
                                          {'username': 'z',
                                           'first_name': 'first_z',
                                           'last_name': 'last_z',
                                           })
        userDict = self.helper.objGetViaRest(user_path)
        
        # Check that 2 resources created
        resourceDict = self.helper.objGetViaRest("/scs/resource/?personalFolder=true&owner=" + 
                                          str(userDict["id"]))
        self.assertEqual(2, resourceDict["meta"]["total_count"])
        
        # Check that directory can be created in each one
        for rDict in resourceDict["objects"]:
            ruri = rDict["resource_uri"]
            self.helper.objGetViaRest(ruri + "folder1/mkdir/")
        
        # Try to delete the user - expect success
        self.helper.objDeleteViaRest(user_path)
        
    def test_taskStart(self):
        # Set up objects in the data model  
        u2a = User.objects.get(username="root")
        self.helper.setupRestAuth("root")
        
        self.taskType = SmModelTaskType()
        self.taskType.name = "sleeper"
        self.taskType.code_module = "smcommon.tests.task_runner_tester"
        self.taskType.code_classname = "SmTaskRunningTester"
        self.taskType.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"type": "float",
                                      "name": "Argument",
                                      "input_id": 1},
                                     {"type": "boolean",
                                      "name": "Argument",
                                      "value": True},
                                     ],
                            "computeResourceRole": "compute"
                       }
                   )
        self.taskType.description = test_helper.DUMMY_DESCRIPTION
        self.taskType.owner = self.helper.user
        self.taskType.save()
        
        self.task = SmModelTask()
        self.task.owner = u2a
        self.task.task_type = self.taskType
        self.task.status = "init"
        self.task.start_time = str(sm_date_now())
        self.task.end_time = str(sm_date_now())
        self.task.parametersJson = json.dumps({
                                               "input_ids": {
                                                             "1": {"value": 0.5}
                                                             }
                                               })
        self.task.save()

        self.helper.makeTaskResource(self.task, self.resource, self.rttt, u2a, "compute")
        
        self.assertEqual("init", self.helper.getTask(self.task.id)["state"])
        
        # Start a test task running
        resp = self.client.get("/scs/task/" + str(self.task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content)
        self.assertNotEqual(str(resp.content).find("starting"), -1, "Resp invalid.  It is : " + str(resp.content))

        # Test that second start is rejected
        startTime = time.time()
        resp = self.client.get("/scs/task/" + str(self.task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 412)

        self.assertNotEqual("init", self.helper.getTask(self.task.id)["state"])

        # Poll for done
        self.helper.waitForTaskDone(self.task.id)
        
        # Check min time elapsed
        elapsedTime = time.time() - startTime
        self.assertGreaterEqual(elapsedTime, 0.5)
        
        # Check response correct
        task = self.helper.getTask(self.task.id)
        self.assertEqual(task["completion"], "completedWithoutError")
        self.assertEqual(task["completionReason"], "success")
    
    def test_taskStartWithResourceFull(self):
        # Set up objects in the data model  
        u2 = User.objects.get(username="root")

        self.resource.capacityJson = json.dumps({"diskUsedState": "full"})
        self.resource.save()
        
        self.taskType = SmModelTaskType()
        self.taskType.name = "sleeper"
        self.taskType.code_module = "smcommon.tests.task_runner_tester"
        self.taskType.code_classname = "SmTaskRunningTester"
        self.taskType.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"type": "float",
                                      "name": "Argument",
                                      "input_id": 1},
                                     {"type": "resource",
                                      "name": "Argument",
                                      "pathType": "file",
                                      "direction": "ouptut",
                                      "input_id": 2,
                                      "role": "storage"},
                                     ],
                            "computeResourceRole": "compute"
                       }
                   )
        self.taskType.description = test_helper.DUMMY_DESCRIPTION
        self.taskType.owner = self.helper.user
        self.taskType.save()
        
        self.task = SmModelTask()
        self.task.owner = u2
        self.task.task_type = self.taskType
        self.task.status = "init"
        self.task.start_time = str(sm_date_now())
        self.task.end_time = str(sm_date_now())
        self.task.parametersJson = json.dumps({
                                               "input_ids": {
                                                             "1": {"value": 0.5},
                                                             "2": {"path": "a"}
                                                             }
                                               })
        self.task.save()

        self.helper.makeTaskResource(self.task, self.resource, self.rttt, u2, "compute")
        
        rttt_storage = SmModelResourceTypeTaskType()
        rttt_storage.resource_type = self.resource.resource_type
        rttt_storage.task_type = self.taskType
        rttt_storage.role = "storage"
        rttt_storage.name = "storage_name"
        rttt_storage.description = test_helper.DUMMY_DESCRIPTION
        rttt_storage.save()
        
        self.helper.makeTaskResource(self.task, self.resource, rttt_storage, u2)
        
        self.assertEqual("init", self.helper.getTask(self.task.id)["state"])
        
        # Start a test task running
        resp = self.client.get("/scs/task/" + str(self.task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 422)
        self.assertTrue("too full" in str(resp.content), str(resp.content))

        # Check that mkdir fails
        res_path = "/scs/resource/" + str(self.resource.pk) + "/"
        resp = self.client.get(res_path + "dir1/mkdir/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 422)

        # Check that delete works even though it is a write operation
        self.helper.tempFolder.writeFile("r_folder/f1", '1234')
        resp = self.client.delete(res_path + "f1/file/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 204, resp.content.decode('utf-8'))
        
        # Check that delete recursive works.
        delTaskType = SmModelTaskType.objects.get(name_key=SM_TASK_TYPE_DELETE_RECURSIVE_NAME_KEY)
        rttt_storage = SmModelResourceTypeTaskType()
        rttt_storage.resource_type = self.resource.resource_type
        rttt_storage.task_type = delTaskType
        rttt_storage.role = "storage"
        rttt_storage.name = "del_name"
        rttt_storage.description = test_helper.DUMMY_DESCRIPTION
        rttt_storage.save()
        
        self.helper.tempFolder.mkdir("r_folder/dir1/")
        self.helper.tempFolder.writeFile("r_folder/dir1/f2", '1234')
        self.task = SmModelTask()
        self.task.owner = u2
        self.task.task_type = delTaskType
        self.task.status = "init"
        self.task.start_time = str(sm_date_now())
        self.task.end_time = str(sm_date_now())
        self.task.parametersJson = json.dumps({
                                               "input_ids": {
                                                             "1": {"path": "dir1/"},
                                                             }
                                               })
        self.task.save()

        self.helper.makeTaskResource(self.task, self.resource, rttt_storage, u2, "storage")
        
        # Start a test task running
        self.assertTrue(os.path.exists(self.helper.tempFolder.makeAbsolutePath("r_folder/dir1", "local")))
        resp = self.client.get("/scs/task/" + str(self.task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.helper.waitForTaskDone(self.task.id)
        self.assertFalse(os.path.exists(self.helper.tempFolder.makeAbsolutePath("r_folder/dir1", "local")))

    def test_taskStartWithReservation(self):
        # Set up objects in the data model  
        root = User.objects.get(username="root")
        userxxx = User.objects.get(username="userxxx")

        self.helper.makeTaskTypeSleep()
        
        resource_path = "/scs/resource/" + str(self.resource.id) + "/"
        
        # Start a test task running
        task = self.helper.makeSleepTask(root, 0)
        self.assertEqual("init", self.helper.getTask(task.id)["state"])
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)

        # Make a reservation as userxxx for "now"
        self.helper.setupRestAuth("userxxx")
        res = self.helper.objCreateViaRest("/scs/reservation/",
                                           {"resource": resource_path,
                                            "start_time": str(sm_date_now()),
                                            "end_time": str(sm_date_now() + timedelta(seconds=10))})
        
        # Try to start task as root - expect failure
        self.helper.setupRestAuth("root")
        task = self.helper.makeSleepTask(root, 0)
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 409)
        
        # Try to start task as userxxx - expect success
        self.helper.setupRestAuth("userxxx")
        task = self.helper.makeSleepTask(userxxx, 0)
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        
        self.helper.waitForTaskDone(task.id)
        
        # Attempt to start task as root - will fail due to reservation by userxxx
        task = self.helper.makeSleepTask(root, 0)
        self.helper.setupRestAuth("root")
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 409)        
        
        # Delete reservation
        self.helper.setupRestAuth("userxxx")
        self.helper.objDeleteViaRest(res)
        
        # Try starting task again - should be OK
        self.helper.setupRestAuth("root")
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content)        
        self.helper.waitForTaskDone(task.id)
        
        # Start a task as userxxx which waits for 3 seconds
        task = self.helper.makeSleepTask(userxxx, 3)
        self.helper.setupRestAuth("userxxx")
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content)
        
        # Create a reservation for user root
        self.helper.setupRestAuth("root")
        res = self.helper.objCreateViaRest("/scs/reservation/",
                                           {"resource": resource_path,
                                            "start_time": str(sm_date_now()),
                                            "end_time": str(sm_date_now() + timedelta(seconds=10))})
        
        # Attempt to start task with 3 second wait - expect refusal and cancel of prev task
        task = self.helper.makeSleepTask(root, 3)
        prevTaskId = task.id
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 409)
        respJson = json.loads(resp.content.decode("utf-8"))
        self.assertEqual("canceling", respJson["status"])
        
        # Poll until start works
        count = 10
        while count > 0:
            task = self.helper.makeSleepTask(root, 3)
            resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=start", **self.helper.auth_header)
            if resp.status_code == 200:
                break
            respJson = json.loads(resp.content.decode("utf-8"))
            self.assertEqual("canceling", respJson["status"])
            self.assertEqual(prevTaskId, respJson["current_task"]["id"])
            self.assertEqual("userxxx", respJson["current_task"]["owner"])
            count -= 1
            time.sleep(0.1)
            
        # Attempt to start task with 3 second wait - expect refusal wo cancel
        # because it is my own task
        prevTaskId = task.id
        task = self.helper.makeSleepTask(root, 3)
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 409)
        respJson = json.loads(resp.content.decode("utf-8"))
        self.assertEqual("inuse", respJson["status"])
        self.assertEqual(prevTaskId, respJson["current_task"]["id"])
        self.assertEqual("root", respJson["current_task"]["owner"])
        
    def test_mockRecordingHandler(self):
        '''
        This test configures the task handling system to use a recording handler
        and then issues a task start checks that the 
        commands are passed to the handler and that responses are processed
        '''
        mockRaServer = SmMockAsyncOpsServer(self.helper.asyncRespObj)
        smRemoteAgentAsyncOperationFactory.setMockAsyncOpsServer(mockRaServer)
        os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE] = "True"
        
        self.helper.setupRestAuth(self.helper.user.username)
        task = list(SmModelTask.objects.all())[0]
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.assertNotEqual(str(resp.content).find("starting"), -1, "Resp invalid.  It is : " + str(resp.content))
        mockHandler = mockRaServer.getHandler(str(task.pk))
        self.assertEqual(1, len(mockHandler.eventList))
        event = mockHandler.eventList.pop()
        self.assertEqual("start", event)
        mockHandler.setProgress(progressPct=62, progressOutputSize=1234)
        mockHandler.announceProgress()
        task = list(SmModelTask.objects.all())[0]
        progressDict = json.loads(task.progressJson)
        self.assertEqual(62, progressDict["progress"])
        self.assertEqual(1234, progressDict["outputSize"])

        mockHandler.setProgress(progressPct=62, progressOutputSize=1234)
        task = list(SmModelTask.objects.all())[0]
        progressDict = json.loads(task.progressJson)
        self.assertEqual(62, progressDict["progress"])
        self.assertEqual(1234, progressDict["outputSize"])
        self.assertEqual("prep", progressDict["state"])
    
        mockHandler.setProgress(progressPct=22, progressOutputSize=12345, state="running")
        task = list(SmModelTask.objects.all())[0]
        progressDict = json.loads(task.progressJson)
        self.assertEqual(22, progressDict["progress"])
        self.assertEqual(12345, progressDict["outputSize"])
        self.assertEqual("running", progressDict["state"])
    
        mockHandler.announceDone("xxx", {"code": 7})
        task = list(SmModelTask.objects.all())[0]
        resultDict = json.loads(task.resultsJson)
        self.assertEqual(7, resultDict["code"])
        self.assertEqual("xxx", task.completionReason)
        self.assertEqual("finished", task.state)
        self.assertEqual("completedWithError", task.completion)
    
    def test_mockAutoRespondingHandler(self):
        '''
        This test configures the task handling system to use an auto responding handler
        and then issues a task start and checks that the 
        commands are passed to the handler and that responses are processed
        '''
        self.helper.setupRestAuth(self.helper.user.username)
        mockRaServer = SmMockAsyncOpsServer(self.helper.asyncRespObj)
        mockRaServer.handlerClass = SmMockOpHandlerResponder
        smRemoteAgentAsyncOperationFactory.setMockAsyncOpsServer(mockRaServer)
        os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE] = "True"
        os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_SLEEP_TIME] = "0.05"
        os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_PREP_COUNT] = "1"
        os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RUNNING_COUNT] = "1"
        os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_CLEANUP_COUNT] = "1"
        os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RESULT] = "success"
                
        task = list(SmModelTask.objects.all())[0]
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode('utf-8'))
        self.assertNotEqual(str(resp.content).find("starting"), -1, "Resp invalid.  It is : " + str(resp.content))
        for _ in range(0, 9):
            time.sleep(0.5)
            task = list(SmModelTask.objects.all())[0]
            if task.state == "finished":
                break
        self.assertEqual("finished", task.state)
        self.assertEqual("completedWithoutError", task.completion)

    def test_taskCancellation(self):
        '''
        This test configures the task handling system to use a recording handler
        and then issues a task start checks that the 
        cancel is correctly generated
        '''
        self.helper.setupRestAuth(self.helper.user.username)
        mockRaServer = SmMockAsyncOpsServer(self.helper.asyncRespObj)
        smRemoteAgentAsyncOperationFactory.setMockAsyncOpsServer(mockRaServer)
        os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE] = "True"
        task = list(SmModelTask.objects.all())[0]
        
        t = SmModelTask()
        t.owner = task.owner
        t.task_type = task.task_type
        t.status = "init"
        t.parametersJson = json.dumps({})
        t.start_time = str(sm_date_now())
        t.end_time = str(sm_date_now())
        t.save()
        
        resp = self.client.get("/scs/task/" + str(t.pk) + "/?action=cancel", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/task/" + str(t.pk) + "/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsonResp = json.loads(resp.content.decode("utf-8"))  
        self.assertEqual("cancelled", jsonResp["completionReason"])
        self.assertEqual("finished", jsonResp["state"])
        self.assertEqual("completedWithError", jsonResp["completion"])
        t.delete()
        
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.assertNotEqual(str(resp.content).find("starting"), -1, "Resp invalid.  It is : " + str(resp.content))
        mockHandler = mockRaServer.getHandler(str(task.pk))
        self.assertEqual(1, len(mockHandler.eventList))
        event = mockHandler.eventList.pop()
        self.assertEqual("start", event)
        
        mockHandler.setProgress(progressPct=62, progressOutputSize=1234)
        mockHandler.announceProgress()
        
        task = list(SmModelTask.objects.all())[0]
        progressDict = json.loads(task.progressJson)
        self.assertEqual(62, progressDict["progress"])
        self.assertEqual(1234, progressDict["outputSize"])

        mockHandler.setProgress(progressPct=62, progressOutputSize=1234)
        task = list(SmModelTask.objects.all())[0]
        progressDict = json.loads(task.progressJson)
        self.assertEqual(62, progressDict["progress"])
        self.assertEqual(1234, progressDict["outputSize"])
        self.assertEqual("prep", progressDict["state"])
    
        mockHandler.setProgress(progressPct=22, progressOutputSize=12345, state="running")
        task = list(SmModelTask.objects.all())[0]
        progressDict = json.loads(task.progressJson)
        self.assertEqual(22, progressDict["progress"])
        self.assertEqual(12345, progressDict["outputSize"])
        self.assertEqual("running", progressDict["state"])
    
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=cancel", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        resp = self.client.get("/scs/task/" + str(task.pk) + "/", **self.helper.auth_header)
        jsonResp = json.loads(resp.content.decode("utf-8"))  
        self.assertEqual("finished", jsonResp["state"])

        event = mockHandler.eventList.pop()
        self.assertEqual("cancel", event)

        mockHandler.announceDone("xxx", {"code": 7})
        task = list(SmModelTask.objects.all())[0]
        self.assertEqual("cancelled", task.completionReason)
        self.assertEqual("finished", task.state)
        self.assertEqual("completedWithError", task.completion)

        # Cancelling an already canceled task is OK
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=cancel", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)

    def test_copyTask(self):
        # Set up input file
        self.helper.tempFolder.writeFile("r_folder/f1", "1234")
        
        # Set up objects in the data model  
        u2 = User.objects.get(username="root")

        self.taskType = SmModelTaskType()
        self.taskType.name = "copy"
        self.taskType.code_module = "smcommon.task_runners.task_runner_copy"
        self.taskType.code_classname = "SmTaskRunnerCopy"
        self.taskType.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"type": "resource",
                                      "name": "Input",
                                      "input_id": 1,
                                      "direction": "input",
                                      "role": "src"},
                                     {"type": "resource",
                                      "name": "Output",
                                      "input_id": 2,
                                      "direction": "output",
                                      "role": "dest"},
                                     ],
                            "computeResourceRole": "dest"
                       }
                   )
        self.taskType.description = test_helper.DUMMY_DESCRIPTION
        self.taskType.owner = self.helper.user
        self.taskType.save()
        
        self.task = SmModelTask()
        self.task.owner = u2
        self.task.task_type = self.taskType
        self.task.parametersJson = json.dumps({
                                               "input_ids": {
                                                             "1": {"path": "f1"},
                                                             "2": {"path": "f2"}
                                                             }
                                               })
        self.task.save()

        self.helper.makeTaskResource(self.task, self.resource, self.rttt_src, u2)
        self.helper.makeTaskResource(self.task, self.resource, self.rttt_dest, u2)
                
        # Start a test task running
        resp = self.client.get("/scs/task/" + str(self.task.pk) + "/?action=start", **self.helper.auth_header)
        
        self.assertEqual(resp.status_code, 200)
        self.assertNotEqual(str(resp.content).find("starting"), -1, "Resp invalid.  It is : " + str(resp.content))
        
        # Poll for done
        self.helper.waitForTaskDone(self.task.id)
        
        # Check that the dest file was created
        s = self.helper.tempFolder.readFile("r_folder/f2", 1000)
        self.assertEqual(b"1234", s)

        # Test copying 2 files at a time
        self.helper.tempFolder.mkdir("r_folder/d1")
        self.task = SmModelTask()
        self.task.owner = u2
        self.task.task_type = self.taskType
        self.task.parametersJson = json.dumps({
                                               "input_ids": {
                                                             "1": {"path": ["f1", "f2"]},
                                                             "2": {"path": "d1/"}
                                                             }
                                               })
        self.task.save()

        self.helper.makeTaskResource(self.task, self.resource, self.rttt_src, u2)
        self.helper.makeTaskResource(self.task, self.resource, self.rttt_dest, u2)
        resp = self.client.get("/scs/task/" + str(self.task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.assertNotEqual(str(resp.content).find("starting"), -1, "Resp invalid.  It is : " + str(resp.content))
        # Poll for done
        self.helper.waitForTaskDone(self.task.id)
                
        s = self.helper.tempFolder.readFile("r_folder/d1/f1", 1000)
        self.assertEqual(b"1234", s)
        s = self.helper.tempFolder.readFile("r_folder/d1/f2", 1000)
        self.assertEqual(b"1234", s)
                
    def test_copyCommand(self):
        x = HttpError422("hello")
        self.assertEquals(422, x.status_code)
        
        # Set up input file
        self.helper.tempFolder.writeFile("r_folder/f1", "1234")
        
        # Set up objects in the data model  
        self.taskType = SmModelTaskType()
        self.taskType.name = "copy"
        self.taskType.name_key = "copy"
        self.taskType.code_module = "smcommon.task_runners.task_runner_copy"
        self.taskType.code_classname = "SmTaskRunnerCopy"
        self.taskType.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"type": "resource",
                                      "name": "Input",
                                      "input_id": 1,
                                      "direction": "input",
                                      "role": "src"},
                                     {"type": "resource",
                                      "name": "Output",
                                      "input_id": 2,
                                      "direction": "output",
                                      "role": "dest"},
                                     ],
                            "computeResourceRole": "dest",
                            "preExistingOutputFolderOk": True
                       }
                   )
        self.taskType.description = test_helper.DUMMY_DESCRIPTION
        self.taskType.owner = self.helper.user
        self.taskType.save()
                        
        rttt = SmModelResourceTypeTaskType()
        rttt.resource_type = SmModelResourceType.objects.get(name_key="generic_storage")
        rttt.task_type = self.taskType
        rttt.role = "src"
        rttt.name = "copy_src"
        rttt.description = test_helper.DUMMY_DESCRIPTION
        rttt.save()    
            
        rttt = SmModelResourceTypeTaskType()
        rttt.resource_type = SmModelResourceType.objects.get(name_key="generic_storage")
        rttt.task_type = self.taskType
        rttt.role = "dest"
        rttt.name = "copy_dest"
        rttt.description = test_helper.DUMMY_DESCRIPTION
        rttt.save()    
            
        # Start a copy task running
        resp = self.client.get("/scs/resource/" + str(self.resource.pk) + 
                               "/f2/copy/?srcResource=" + str(self.resource.pk) + 
                               "&srcPath=f1", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode('utf-8'))
        
        # Poll for done
        respDict = json.loads(resp.content.decode("utf-8"))
        taskPath = respDict["taskPath"]
        self.helper.waitForTaskDone(self.helper.getIdFromPath(taskPath))
        
        # Check that the dest file was created
        s = self.helper.tempFolder.readFile("r_folder/f2", 1000)
        self.assertEqual(b"1234", s)

        # Test copying 2 files at a time
        self.helper.tempFolder.mkdir("r_folder/d1")
        resp = self.client.get("/scs/resource/" + str(self.resource.pk) + 
                               "/d1/copy/?srcResource=" + str(self.resource.pk) + 
                               "&srcPath=f1&srcPath=f2", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        respDict = json.loads(resp.content.decode("utf-8"))
        taskPath = respDict["taskPath"]
        self.helper.waitForTaskDone(self.helper.getIdFromPath(taskPath))
        
        s = self.helper.tempFolder.readFile("r_folder/d1/f1", 1000)
        self.assertEqual(b"1234", s)
        s = self.helper.tempFolder.readFile("r_folder/d1/f2", 1000)
        self.assertEqual(b"1234", s)
                
    def test_shellTask(self):
        # Set up input file
        # self.helper.tempFolder.mkdir("r_folder") - created by resource
        self.helper.tempFolder.writeFile("r_folder/f1", "1234")
        
        # Set up objects in the data model  
        u2 = User.objects.get(username="root")
        agent = SmModelAgent.objects.get(name="test_agent")
        agent.guid = "testagentid"
        agent.save()

        self.taskType = SmModelTaskType()
        self.taskType.name = "sleeper"
        self.taskType.code_module = "smcommon.task_runners.task_runner_shell"
        self.taskType.code_classname = "SmTaskRunnerShell"
        self.taskType.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"type": "string",
                                      "name": "Argument",
                                      "value": "cp"},
                                     {"type": "resource",
                                      "name": "Argument",
                                      "input_id": 1,
                                      "direction": "input",
                                      "role": "src"},
                                     {"type": "resource",
                                      "name": "Argument",
                                      "input_id": 2,
                                      "direction": "output",
                                      "role": "dest"},
                                     ],
                            "outputProgress": {"outputArg": 2, "maxSize": 1234},
                            "computeResourceRole": "compute"
                       }
                   )
        self.taskType.description = test_helper.DUMMY_DESCRIPTION
        self.taskType.owner = self.helper.user
        self.taskType.save()
        
        self.task = SmModelTask()
        self.task.owner = u2
        self.task.task_type = self.taskType
        self.task.status = "init"
        self.task.start_time = str(sm_date_now())
        self.task.end_time = str(sm_date_now())
        self.task.parametersJson = json.dumps({
                                               "input_ids": {
                                                             "1": {"path": "f1"},
                                                             "2": {"path": "f2"}
                                                             }
                                               })
        self.task.save()

        self.helper.makeTaskResource(self.task, self.resource, self.rttt, u2, "compute")
        self.helper.makeTaskResource(self.task, self.resource, self.rttt_src, u2)
        self.helper.makeTaskResource(self.task, self.resource, self.rttt_dest, u2)
        
        # Start a test task running
        resp = self.client.get("/scs/task/" + str(self.task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.assertNotEqual(str(resp.content).find("starting"), -1, "Resp invalid.  It is : " + str(resp.content))
        
        # Poll for done
        self.helper.waitForTaskDone(self.task.id)
        
        # Check that the dest file was created
        s = self.helper.tempFolder.readFile("r_folder/f2", 1000)
        self.assertEqual(b"1234", s)

        # Test that if the task is run again, the output path is changed
        # to add a timestamp
        self.task = SmModelTask()
        self.task.owner = u2
        self.task.task_type = self.taskType
        self.task.status = "init"
        self.task.parametersJson = json.dumps({
                                               "input_ids": {
                                                             "1": {"path": "f1"},
                                                             "2": {"path": "f2"}
                                                             }
                                               })
        self.task.save()
        
        self.helper.makeTaskResource(self.task, self.resource, self.rttt, u2, "compute")
        self.helper.makeTaskResource(self.task, self.resource, self.rttt_src, u2)
        self.helper.makeTaskResource(self.task, self.resource, self.rttt_dest, u2)
        
        # Start a test task running
        resp = self.client.get("/scs/task/" + str(self.task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.assertNotEqual(str(resp.content).find("starting"), -1, "Resp invalid.  It is : " + str(resp.content))
        
        # Poll for done
        self.helper.waitForTaskDone(self.task.id)
        
        # Get the task and check that output name is changed
        task = SmModelTask.objects.get(id=self.task.id)
        paramJsonDict = json.loads(task.parametersJson)
        newPath = paramJsonDict["input_ids"]['2']['path']
        self.assertTrue(smContainsDateString(newPath) != -1)
        
        # Check that the dest file was created
        s = self.helper.tempFolder.readFile("r_folder/" + newPath, 1000)
        self.assertEqual(b"1234", s)
   
    def test_shellScriptTask(self):
        # Set up input file
        # self.helper.tempFolder.mkdir("r_folder") - created by resource
        self.helper.tempFolder.writeFile("r_folder/f1", "1234")
        
        # Set up objects in the data model  
        u2 = User.objects.get(username="root")
        agent = SmModelAgent.objects.get(name="test_agent")
        agent.guid = "testagentid"
        agent.save()

        script = SmModelScript()
        script.key = "cp_script"
        script.value = "cp $1 $2\n"
        script.owner = self.helper.user
        script.save()
        
        self.taskType = SmModelTaskType()
        self.taskType.name = "sleeper"
        self.taskType.code_module = "smcommon.task_runners.task_runner_shell"
        self.taskType.code_classname = "SmTaskRunnerShell"
        self.taskType.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"type": "string",
                                      "name": "Argument",
                                      "value": "bash"},
                                     {"type": "script",
                                      "name": "Argument",
                                      "value": "cp_script"},
                                     {"type": "resource",
                                      "name": "Input",
                                      "input_id": 1,
                                      "direction": "input",
                                      "role": "src"},
                                     {"type": "resource",
                                      "name": "Output",
                                      "input_id": 2,
                                      "direction": "output",
                                      "role": "dest"},
                                     ],
                            "outputProgress": {"arg": 3,
                                               "maxSize": 1234},
                            "computeResourceRole": "compute"
                       }
                   )
        self.taskType.description = test_helper.DUMMY_DESCRIPTION
        self.taskType.owner = self.helper.user
        self.taskType.save()
        
        self.task = SmModelTask()
        self.task.owner = u2
        self.task.task_type = self.taskType
        self.task.status = "init"
        self.task.start_time = str(sm_date_now())
        self.task.end_time = str(sm_date_now())
        self.task.parametersJson = json.dumps({
                                               "input_ids": {
                                                             "1": {"path": "f1"},
                                                             "2": {"path": "f2"}
                                                             }
                                               })
        self.task.save()

        self.helper.makeTaskResource(self.task, self.resource, self.rttt, u2, "compute")
        self.helper.makeTaskResource(self.task, self.resource, self.rttt_src, u2)
        self.helper.makeTaskResource(self.task, self.resource, self.rttt_dest, u2)
        
        # Start a test task running
        resp = self.client.get("/scs/task/" + str(self.task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.assertNotEqual(str(resp.content).find("starting"), -1, "Resp invalid.  It is : " + str(resp.content))
        
        # Poll for done
        self.helper.waitForTaskDone(self.task.id)
        
        # Check that the dest file was created
        s = self.helper.tempFolder.readFile("r_folder/f2", 1000)
        self.assertEqual(b"1234", s)

    def test_shellTaskWithStreams(self):
        '''
        Checkout the wiki page at https://rpsmarf.atlassian.net/wiki/display/RPS/Streaming+Design
        in the "Task Management" section to see the configuration created.
        ''' 
        # Set up input file
        # self.helper.tempFolder.mkdir("r_folder") - created by resource
        self.helper.tempFolder.mkdir("w_folder")
        self.helper.tempFolder.writeFile("r_folder/f1", "1234")
        srcFile = self.helper.tempFolder.makeLocalDad("r_folder/f1").getAbsPath()
        destFile = self.helper.tempFolder.makeLocalDad("w_folder/f2").getAbsPath()
        
        # Set up objects in the data model  
        u2 = User.objects.get(username="root")
        agent = SmModelAgent.objects.get(name="test_agent")
        agent.guid = "testagentid"
        agent.save()

        container = SmModelContainer.objects.get(name="test_container")

        taskType = SmModelTaskType()
        taskType.name = "cp"
        taskType.code_module = "smcommon.task_runners.task_runner_shell"
        taskType.code_classname = "SmTaskRunnerShell"
        taskType.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"type": "string",
                                      "name": "Argument",
                                      "value": "cp"},
                                     {"type": "resource",
                                      "name": "Argument",
                                      "input_id": 1,
                                      "direction": "input",
                                      "role": "stream_src"},
                                     {"type": "resource",
                                      "name": "Argument",
                                      "input_id": 2,
                                      "direction": "output",
                                      "role": "stream_dest"},
                                     ],
                            "computeResourceRole": "compute"
                       }
                   )
        taskType.description = test_helper.DUMMY_DESCRIPTION
        taskType.owner = self.helper.user
        taskType.save()
        
        # Create a source resource type
        rtypesrc = SmModelResourceType()
        rtypesrc.name = "sourcetype"
        rtypesrc.nature = "streamout"
        rtypesrc.configuration = json.dumps({"pipeFolder": self.helper.tempFolder.folder})
        rtypesrc.code_classname = "SmTaskRunnerStreamSource"
        rtypesrc.code_module = "smcommon.task_runners.task_runner_stream"
        rtypesrc.description = test_helper.DUMMY_DESCRIPTION
        rtypesrc.save()
        
        rtttsrc = SmModelResourceTypeTaskType()
        rtttsrc.resource_type = rtypesrc
        rtttsrc.task_type = taskType
        rtttsrc.role = "stream_src"
        rtttsrc.name = "stream_src"
        rtttsrc.description = test_helper.DUMMY_DESCRIPTION
        rtttsrc.save()
            
        # Create a source resource 
        rsrc = SmModelResource()
        rsrc.resource_type = rtypesrc
        rsrc.name = "stream_source"
        rsrc.parametersJson = json.dumps({"args": [{"type": "resource", "direction": "output", "role": "stream"},
                                                   {"type": "string", "value": "-interval"},
                                                   {"type": "float", "value": 0.02},
                                                   {"type": "string", "value": "-objectsToSendPerInterval"},
                                                   {"type": "int", "value": 1},
                                                   {"type": "string", "value": "-durationInIntervals"},
                                                   {"type": "int", "value": 3},
                                                   {"type": "string", "value": "-pattern"},
                                                   {"type": "string", "value": "file_repeating"},
                                                   {"type": "string", "value": "-patternFile"},
                                                   {"type": "string", "value": srcFile},
                                                   ]})                                          
        rsrc.owner = u2
        rsrc.container = container
        rsrc.description = test_helper.DUMMY_DESCRIPTION
        rsrc.status = "up"
        rsrc.save()
    
        # Create a sink resource type        
        rtypesink = SmModelResourceType()
        rtypesink.name = "sinktype"
        rtypesink.nature = "streamin"
        rtypesink.code_classname = "SmTaskRunnerStreamSink"
        rtypesink.code_module = "smcommon.task_runners.task_runner_stream"
        rtypesink.configuration = json.dumps({"pipeFolder": self.helper.tempFolder.folder})
        rtypesink.description = test_helper.DUMMY_DESCRIPTION
        rtypesink.save()
        
        rtttsink = SmModelResourceTypeTaskType()
        rtttsink.resource_type = rtypesink
        rtttsink.task_type = taskType
        rtttsink.role = "stream_dest"
        rtttsink.name = "stream_dest"
        rtttsink.description = test_helper.DUMMY_DESCRIPTION
        rtttsink.save()
            
        # Create a sink resource 
        rsink = SmModelResource()
        rsink.resource_type = rtypesink
        rsink.name = "stream_sink"
        rsink.parametersJson = json.dumps({"args": [{"type": "resource", "direction": "input", "role": "stream"},
                                                    {"type": "string", "value": "-objectsExpected"},
                                                    {"type": "int", "value": 3},
                                                    {"type": "string", "value": "-pattern"},
                                                    {"type": "string", "value": "file_repeating"},
                                                    {"type": "string", "value": "-patternFile"},
                                                    {"type": "string", "value": destFile},
                                                    ]})                                          
        rsink.owner = u2
        rsink.container = container
        rsink.description = test_helper.DUMMY_DESCRIPTION
        rsink.status = "up"
        rsink.save()
        
        task = SmModelTask()
        task.owner = u2
        task.task_type = taskType
        task.status = "init"
        task.start_time = str(sm_date_now())
        task.end_time = str(sm_date_now())
        task.parametersJson = json.dumps({
                                          "input_ids": {
                                                        "1": {},
                                                        "2": {}
                                                        }
                                          })
        task.save()

        self.helper.makeTaskResource(task, rsrc, rtttsrc, u2)
        self.helper.makeTaskResource(task, rsink, rtttsink, u2)
        self.helper.makeTaskResource(task, self.resource, self.rttt, u2)
        
        self.resource.parametersJson = json.dumps({"taskViewBaseUrl": "http://localhost:1234/default"})
        self.resource.save()
    
        # Start a test task running
        resp = self.client.get("/scs/task/" + str(task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.assertNotEqual(str(resp.content).find("starting"), -1, "Resp invalid.  It is : " + str(resp.content))
        
        # Poll for done
        self.helper.waitForTaskDone(task.id)
        
        taskObj = self.helper.getTask(task.pk)
        runningDisplayDict = json.loads(taskObj["runningDisplayJson"])
        self.assertEqual(runningDisplayDict["taskView"]["url"], "http://localhost:1234/default")
        # Check that the dest file was created
        s = self.helper.tempFolder.readFile("w_folder/f2", 1000)
        self.assertEqual(b"123", s)

    def test_tmpFilePurging(self):
        time.sleep(0.5)  # Allow folders to be created
        # Create folders
        # self.helper.tempFolder.mktree(["r_folder/.rpsmarf/"])
        # self.helper.tempFolder.mktree(["r_folder/.rpsmarf/tmp/"])

        # Create purger and start
        purger = ScsTmpFilePurger(0.1, 1.0)
        # Create file "a" in tmp folder
        self.helper.tempFolder.writeFile("r_folder/.rpsmarf/tmp/a", "12345")

        # Create tree "b" in tmp folder
        self.helper.tempFolder.mktree(["r_folder/.rpsmarf/tmp/b/",
                                "r_folder/.rpsmarf/tmp/b/f1",
                                "r_folder/.rpsmarf/tmp/b/f2",
                                "r_folder/.rpsmarf/tmp/b/c"])
        createTimeA = time.time()

        purger.doOneCheck(createTimeA - 2)
        
        # Check files still there
        fileList = os.listdir(self.helper.tempFolder.mkAbsolutePath("r_folder/.rpsmarf/tmp/"))
        self.assertEqual(2, len(fileList))
        
        time.sleep(2)
        # Create file "c"
        self.helper.tempFolder.writeFile("r_folder/.rpsmarf/tmp/c", "12345")
        createTimeB = time.time()

        # Wait again
        purger.doOneCheck(createTimeA + 2)

        # Check that a and b gone
        fileList = os.listdir(self.helper.tempFolder.mkAbsolutePath("r_folder/.rpsmarf/tmp/"))
        self.assertEqual(1, len(fileList))

        # Wait again - check "c" is gone
        purger.doOneCheck(createTimeB + 2)
        fileList = os.listdir(self.helper.tempFolder.mkAbsolutePath("r_folder/.rpsmarf/tmp/"))
        self.assertEqual(0, len(fileList))

        try:
            purger.checkCount = 0
            purger.start()
            for _ in range(0, 20):
                if purger.checkCount > 0:
                    break
                else:
                    time.sleep(0.1)
            self.assertTrue(purger.checkCount > 0)
        finally:
            purger.stop()
                   
    def makeTask(self, user, endTime):
        t = SmModelTask()
        t.owner = user
        t.task_type = self.helper.ttype
        t.status = "init"
        t.parametersJson = json.dumps({})
        t.start_time = endTime
        t.end_time = endTime
        t.save()

    def makeRes(self, user, endTime):
        res = SmModelReservation()
        res.owner = user
        res.resource = self.helper.resourceSleep
        res.start_time = endTime
        res.end_time = endTime
        res.save()

    def test_taskAgeOut(self):

        testStartTime = sm_date_now()
        other_tasks = len(SmModelTask.objects.all())
        
        # Create 6 tasks a,b,c,d,e,g
        # a, b and c are owned by user 1
        # a ended 100 seconds ago, b 50 seconds ago and c 5 seconds ago
        user1 = User.objects.create_user("u1", "u1@b.com", "hello")
        self.makeTask(user1, testStartTime - timedelta(seconds=100))
        self.makeTask(user1, testStartTime - timedelta(seconds=50))
        self.makeTask(user1, testStartTime - timedelta(seconds=5))
        # d and e are owned by user 2
        # d ended 30 seconds ago, e 5 seconds ago
        user2 = User.objects.create_user("u2", "u2@b.com", "hello")
        self.makeTask(user2, testStartTime - timedelta(seconds=30))
        self.makeTask(user2, testStartTime - timedelta(seconds=5))
        # f is owned by user 3 and ended ended 100 seconds ago
        user3 = User.objects.create_user("u3", "u3@b.com", "hello")
        self.makeTask(user3, testStartTime - timedelta(seconds=100))

        # Create purger object
        purger = ScsTaskAgeOut(0.1)

        # Kill all older than 200 seconds, expect none deleted
        purger.doOneCheck(testStartTime - timedelta(seconds=200), 2)
        
        # Check all still there
        self.assertEqual(other_tasks + 6, len(SmModelTask.objects.all()))

        # Kill all older than 1 second, but allow 3 per user, expect none deleted
        purger.doOneCheck(testStartTime - timedelta(seconds=1), 3)
        
        # Check all still there
        self.assertEqual(other_tasks + 6, len(SmModelTask.objects.all()))

        # Kill all older than 60 seconds, expect "a" deleted
        purger.doOneCheck(testStartTime - timedelta(seconds=60), 2)

        # Check 5 still there
        self.assertEqual(other_tasks + 5, len(SmModelTask.objects.all()))
        
        # Kill all older than 60 seconds, only one per user allowed, expect none deleted
        purger.doOneCheck(testStartTime - timedelta(seconds=60), 1)

        # Check 5 still there
        self.assertEqual(other_tasks + 5, len(SmModelTask.objects.all()))

        # Kill all older than 10 seconds, only one per user allowed, expect 2 deleted
        purger.doOneCheck(testStartTime - timedelta(seconds=10), 1)

        # Check 3 still there
        self.assertEqual(other_tasks + 3, len(SmModelTask.objects.all()))

        # Test start/ stop
        try:
            purger.checkCount = 0
            purger.start()
            for _ in range(0, 20):
                if purger.checkCount > 0:
                    break
                else:
                    time.sleep(0.1)
            self.assertTrue(purger.checkCount > 0)
        finally:
            purger.stop()
                   
    def test_resAgeOut(self):

        testStartTime = sm_date_now()
        other_res = len(SmModelReservation.objects.all())
        
        # Create 6 tasks a,b,c,d,e,g
        # a, b and c are owned by user 1
        # a ended 100 seconds ago, b 50 seconds ago and c 5 seconds ago
        user1 = User.objects.create_user("u1", "u1@b.com", "hello")
        self.makeRes(user1, testStartTime - timedelta(seconds=100))
        self.makeRes(user1, testStartTime - timedelta(seconds=50))
        self.makeRes(user1, testStartTime - timedelta(seconds=5))
        # d and e are owned by user 2
        # d ended 30 seconds ago, e 5 seconds ago
        user2 = User.objects.create_user("u2", "u2@b.com", "hello")
        self.makeRes(user2, testStartTime - timedelta(seconds=30))
        self.makeRes(user2, testStartTime - timedelta(seconds=5))
        # f is owned by user 3 and ended ended 100 seconds ago
        user3 = User.objects.create_user("u3", "u3@b.com", "hello")
        self.makeRes(user3, testStartTime - timedelta(seconds=100))

        # Create purger object
        purger = ScsTaskAgeOut(0.1)

        # Kill all older than 200 seconds, expect none deleted
        purger.doOneCheck(testStartTime - timedelta(seconds=200), 2)
        
        # Check all still there
        self.assertEqual(other_res + 6, len(SmModelReservation.objects.all()))

        # Kill all older than 60 seconds, expect 2 deleted
        purger.doOneCheck(testStartTime - timedelta(seconds=60), 2)

        # Check 5 still there
        self.assertEqual(other_res + 4, len(SmModelReservation.objects.all()))
        
        # Kill all older than 10 seconds, only one per user allowed, expect 2 left
        purger.doOneCheck(testStartTime - timedelta(seconds=10), 2)

        # Check 2 still there
        self.assertEqual(other_res + 2, len(SmModelReservation.objects.all()))

    def test_resourceCapUpdater(self):
        self.helper.setupRemoteAgentViaIce()
        self.helper.setupRestAuth("root")
        
        robjs = SmModelResource.objects
        data12 = '012345678901'
        
        # Set capacity record for resource
        resource = robjs.get(name_key="resourcexxx")
        resource.capacityJson = json.dumps({"pollDiskUsed": True,
                                            "diskUsedWarnThreshold": 10,
                                            "diskUsedFailThreshold": 20
                                            })
        resource.save()
        
        # Force update via REST API
        self.helper.objGetViaRest("/scs/resource/" + str(resource.id) + "/update/")
        
        # Check fields present
        capDict = json.loads(robjs.get(name_key="resourcexxx").capacityJson)
        self.assertEqual(0, capDict["diskUsed"])
        self.assertEqual("normal", capDict["diskUsedState"])
        
        # Add data to get to warn state
        self.helper.tempFolder.writeFile("r_folder/f1", data12)
        
        # Force update via REST API
        self.helper.objGetViaRest("/scs/resource/" + str(resource.id) + "/update/")
        
        # Check state is warn and data total is correct
        capDict = json.loads(robjs.get(name_key="resourcexxx").capacityJson)
        self.assertEqual(12, capDict["diskUsed"])
        self.assertEqual("warn", capDict["diskUsedState"])
        
        # Add data to get to fail state
        self.helper.tempFolder.writeFile("r_folder/f2", data12)
        
        # Force update via REST API
        self.helper.objGetViaRest("/scs/resource/" + str(resource.id) + "/update/")
        
        # Check state is fail  and data total is correct
        capDict = json.loads(robjs.get(name_key="resourcexxx").capacityJson)
        self.assertEqual(24, capDict["diskUsed"])
        self.assertEqual("full", capDict["diskUsedState"])

        # Remove all files
        self.helper.tempFolder.rm("r_folder/f1")
        self.helper.tempFolder.rm("r_folder/f2")

        # Force update via REST API
        self.helper.objGetViaRest("/scs/resource/" + str(resource.id) + "/update/")
        
        # Check state is "normal" and data total is correct
        capDict = json.loads(robjs.get(name_key="resourcexxx").capacityJson)
        self.assertEqual(0, capDict["diskUsed"])
        self.assertEqual("normal", capDict["diskUsedState"])
                           
    def test_zipUnzip(self):
        # Set up input files
        self.helper.tempFolder.mktree(["r_folder/a/", "r_folder/a/f1", "r_folder/a/b/", "r_folder/a/b/f2"])
            
        # Set up objects in the data model  
        agent = SmModelAgent.objects.get(name="test_agent")
        agent.guid = "testagentid"
        agent.state = "up"
        agent.save()

        self.helper.setupZipUnzipTaskTypes()
    
        resource_id = self.resource.pk
        
        # Make zip file to tmp folder from server folder "a"
        resp = self.client.get("/scs/resource/" + str(resource_id) + "/a/zip/?makeTempPath=True",
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode("utf-8"))
        respDict = json.loads(resp.content.decode("utf-8"))
        zipFilePath = respDict["zipFilePath"]
        taskPath = respDict["taskPath"]
        r = self.client.get(taskPath, **self.helper.auth_header)
        respDict = json.loads(r.content.decode("utf-8"))
        self.assertFalse(respDict["uiVisible"])
        self.helper.waitForTaskDone(self.helper.getIdFromPath(taskPath))
        
        # Download zip file
        resp = self.client.get("/scs/resource/" + str(resource_id) + "/" + zipFilePath + "/download/",
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        data = b''
        for s in resp.streaming_content:
            data += s
        self.helper.tempFolder.writeFileBinary("temp.zip", data)

        # Upload zip file to tmp folder
        with open(self.helper.tempFolder.mkAbsolutePath("temp.zip"), "rb") as fp:
            resp = self.client.post("/scs/resource/" + str(resource_id) + "/upload/?makeTempPath=True",
                                    {'uploadfile': fp}, **self.helper.auth_header)    
        self.assertEqual(resp.status_code, 200)
        respDict = json.loads(resp.content.decode("utf-8"))
        tempPath2 = respDict["path"]
        
        # Unzip file to server folder "new_a"
        resp = self.client.get("/scs/resource/" + str(resource_id) + "/new_a/unzip/?zipFilePath=" + tempPath2,
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        respDict = json.loads(resp.content.decode("utf-8"))
        taskPath = respDict["taskPath"]
        self.helper.waitForTaskDone(self.helper.getIdFromPath(taskPath))
        
        # List contents of "new_a"
        resp = self.client.get("/scs/resource/" + str(resource_id) + "/new_a/list/?recursiveDepth=100",
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode('utf-8'))
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(4, len(jsoncon))        

        # Unzip file to server folder "a" - expect new folder a_<date> to be created
        resp = self.client.get("/scs/resource/" + str(resource_id) + "/a/unzip/?zipFilePath=" + tempPath2,
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        respDict = json.loads(resp.content.decode("utf-8"))
        taskPath = respDict["taskPath"]
        self.helper.waitForTaskDone(self.helper.getIdFromPath(taskPath))
        
        taskInfo = self.helper.objGetViaRest(taskPath)
        logger.debug("taskinfo = %s", json.dumps(taskInfo))
        fileName = json.loads(taskInfo["parametersJson"])["input_ids"]["1"]["path"]
        self.assertTrue(date_utils.smContainsDateString(fileName))
        
    def test_recusiveDelete(self):
        logger.warning("STARTED DELETE RECURSIVE TASK")
        # Set up input files
        self.helper.tempFolder.mktree(["r_folder/a/", "r_folder/a/f1", "r_folder/a/b/", "r_folder/a/b/f2"])
        
        # Set up objects in the data model  
        agent = SmModelAgent.objects.get(name="test_agent")
        agent.guid = "testagentid"
        agent.save()

        resource_id = self.resource.pk
        
        # Fire recursive delete on server folder "a"
        resp = self.client.get("/scs/resource/" + str(resource_id) + "/a/deleterecursive/",
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        respDict = json.loads(resp.content.decode("utf-8"))
        taskPath = respDict["taskPath"]
        r = self.client.get(taskPath, **self.helper.auth_header)
        respDict = json.loads(r.content.decode("utf-8"))
        self.assertFalse(respDict["uiVisible"])
        self.helper.waitForTaskDone(self.helper.getIdFromPath(taskPath))
                
        # List contents of "new_a"
        resp = self.client.get("/scs/resource/" + str(resource_id) + "/new_a/list/", **self.helper.auth_header)
        logger.warning("DONE DELETE RECURSIVE TASK")
        self.assertEqual(resp.status_code, 422, resp.content.decode('utf-8'))
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual("ENOENT", jsoncon["code"])       
        
    def test_getVersions(self):
        responses = [# Git fetch response
                     json.dumps({"resultcode": 0, "stdout": "", "stderr": ""}),
                     # Git tag response
                     json.dumps({"resultcode": 0, "stdout": "b_rpsmarf\nc_rpsmarf\n", "stderr": ""}),
                     # git cat-file tag <tagName> response
                     json.dumps({"resultcode": 0, "stdout": "object d7d0b575b3d77ce518dae12d9ca18eb2cfa0d91f\n" +
                                 "type commit\n" +
                                 "tag v1_rpsmarf\n" +
                                 "tagger Andrew McGregor <andrewmcgregor@sce.carleton.ca> 1437572154 +0000\n" +
                                 "\n" +
                                 "First version"}),
                     # git cat-file tag <tagName> response
                     json.dumps({"resultcode": 0, "stdout": "object d7d0b575b3d77ce518dae12d9fa18eb2cfa0d91f\n" +
                                 "type commit\n" +
                                 "tag v2_rpsmarf\n" +
                                 "tagger Andrew McGregor <andrewmcgregor@sce.carleton.ca> 1437592154 +0000\n" +
                                 "\n" +
                                 "Second commit\nwith 2 lines"})]
                                 
        mockRaServer = SmMockSyncOpsServer(responses)
        smRemoteAgentAsyncOperationFactory.setMockSyncOpsServer(mockRaServer)
        os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE] = "True"
        # Set up objects in the data model  
        task_type = SmModelTaskType()
        task_type.name = "sleeper"
        task_type.code_module = "smcommon.task_runners.task_runner_shell"
        task_type.code_classname = "SmTaskRunnerShell"
        task_type.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"type": "string",
                                      "name": "Argument",
                                      "value": "ls"},
                                     ],
                            "outputProgress": {"outputArg": 2, "maxSize": 1234},
                            "workingDir": {"type": "string", "value": "/tmp"},
                            "computeResourceRole": "compute",
                            "versioning": "git"
                       }
                   )
        task_type.description = test_helper.DUMMY_DESCRIPTION
        task_type.owner = self.helper.user
        task_type.save()
        
        # Make rt3 and link to existing resource's resource type
        rttt = SmModelResourceTypeTaskType()
        rttt.resource_type = SmModelResourceType.objects.get(name_key="default_resource")
        rttt.task_type = task_type
        rttt.role = "compute"
        rttt.name = "compute_name_2"
        rttt.description = DUMMY_DESCRIPTION
        rttt.save()
        
        # Get versions test task running
        resp = self.client.get("/scs/task_type/" + str(task_type.pk) + "/getversions/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode("utf-8"))
        respDict = json.loads(resp.content.decode("utf-8"))
        self.assertEqual(0, respDict["resultcode"])
        self.assertEqual(2, len(respDict["tags"]))
        tagDict = respDict["tags"][0]
        self.assertEqual("b_rpsmarf", tagDict["tag"])
        self.assertEqual("Andrew McGregor <andrewmcgregor@sce.carleton.ca>", tagDict["author"])
        self.assertEqual("First version", tagDict["message"])
        self.assertEqual(1437572154, tagDict["timestamp"])
        tagDict = respDict["tags"][1]
        self.assertEqual("c_rpsmarf", tagDict["tag"])
        self.assertEqual("Andrew McGregor <andrewmcgregor@sce.carleton.ca>", tagDict["author"])
        self.assertEqual("Second commit\nwith 2 lines", tagDict["message"])
        self.assertEqual(1437592154, tagDict["timestamp"])
   
    def test_versionedlTask(self):
        '''
        In this test we run a task_type which supports git versioning with a version specified,
        but we changed the 'git pull <version>' command to 'touch <version>' to allow us to 
        easily check if command was executed (and to not need git repos for the unit tests).  
        '''
        os.environ[SM_ENV_VAR_COMMON_GIT_FETCH_COMMAND] = "touch fetch"
        os.environ[SM_ENV_VAR_COMMON_GIT_FETCH_TAGS_COMMAND] = "touch fetch-tags"
        os.environ[SM_ENV_VAR_COMMON_GIT_RESET_COMMAND] = "touch reset"
        os.environ[SM_ENV_VAR_COMMON_GIT_CHECKOUT_COMMAND] = "touch $VERSION$"
        # Set up objects in the data model  
        u2 = User.objects.get(username="root")
        agent = SmModelAgent.objects.get(name="test_agent")
        agent.guid = "testagentid"
        agent.save()

        task_type = SmModelTaskType()
        task_type.name = "sleeper"
        task_type.code_module = "smcommon.task_runners.task_runner_shell"
        task_type.code_classname = "SmTaskRunnerShell"
        task_type.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"type": "string",
                                      "name": "Argument",
                                      "value": "ls"},
                                     ],
                            "outputProgress": {"outputArg": 2, "maxSize": 1234},
                            "workingDir": {"type": "string", "value": self.helper.tempFolder.folder},
                            "computeResourceRole": "compute",
                            "versioning": "git"
                       }
                   )
        task_type.description = test_helper.DUMMY_DESCRIPTION
        task_type.owner = self.helper.user
        task_type.save()
        
        self.task = SmModelTask(owner=u2, task_type=task_type)
        self.task.parametersJson = json.dumps({
                                               "version": "1234"
                                               })
        self.task.save()

        self.helper.makeTaskResource(self.task, self.resource, self.rttt, u2, "compute")
        
        # Start a test task running
        resp = self.client.get("/scs/task/" + str(self.task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.assertNotEqual(str(resp.content).find("starting"), -1, "Resp invalid.  It is : " + str(resp.content))
        
        # Poll for done
        self.helper.waitForTaskDone(self.task.id)
        
        # Check that the dest file was created by the "git pull process"
        self.assertEqual(b"", self.helper.tempFolder.readFile("1234", 1000))
        self.assertEqual(b"", self.helper.tempFolder.readFile("fetch", 1000))
        self.assertEqual(b"", self.helper.tempFolder.readFile("fetch-tags", 1000))
        self.assertEqual(b"", self.helper.tempFolder.readFile("reset", 1000))

        self.helper.tempFolder.rm("1234")
        self.helper.tempFolder.rm("fetch")
        self.helper.tempFolder.rm("fetch-tags")
        self.helper.tempFolder.rm("reset")
        self.assertRaises(Exception, self.helper.tempFolder.readFile, "1234", 1000)
        
        # Make a new task with verion = "latest"
        parametersJson = json.dumps({
                                     "version": "latest"
                                    })
        self.task = SmModelTask(owner=u2, task_type=task_type, parametersJson=parametersJson)
        self.task.save()
        self.helper.makeTaskResource(self.task, self.resource, self.rttt, u2, "compute")
        
        # Start a test task running
        resp = self.client.get("/scs/task/" + str(self.task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        # Poll for done
        self.helper.waitForTaskDone(self.task.id)
        self.assertRaises(Exception, self.helper.tempFolder.readFile, "1234", 1000)
        self.assertEqual(b"", self.helper.tempFolder.readFile("fetch", 1000))
        self.assertRaises(Exception, self.helper.tempFolder.readFile, "fetch-tags", 1000)
        self.assertEqual(b"", self.helper.tempFolder.readFile("reset", 1000))
        
        # Make a new task with no version
        parametersJson = json.dumps({
                                    })
        self.task = SmModelTask(owner=u2, task_type=task_type, parametersJson=parametersJson)
        self.task.save()
        self.helper.makeTaskResource(self.task, self.resource, self.rttt, u2, "compute")
        
        # Start a test task running
        resp = self.client.get("/scs/task/" + str(self.task.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        # Poll for done
        self.helper.waitForTaskDone(self.task.id)
        self.assertRaises(Exception, self.helper.tempFolder.readFile, "1234", 1000)
        self.assertEqual(b"", self.helper.tempFolder.readFile("fetch", 1000))
        self.assertRaises(Exception, self.helper.tempFolder.readFile, "fetch-tags", 1000)
        self.assertEqual(b"", self.helper.tempFolder.readFile("reset", 1000))
                
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()