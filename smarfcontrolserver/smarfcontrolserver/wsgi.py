'''
    WSGI config for smarfcontrolserver project.

    It exposes the WSGI callable as a module-level variable named ``application``.

    For more information on this file, see
    https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/

    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
from smarfcontrolserver.init.scs_init import scsInitNonDjangoCode
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "smarfcontrolserver.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

FIRST_TIME_LOADED = True
if (FIRST_TIME_LOADED):
    FIRST_TIME_LOADED = False
    scsInitNonDjangoCode()
