'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import logging
from smcommon.task_runners.task_runner import SmTaskRunnerBase
from smcommon.tests.stream_tester import SmStreamTestSource, SmStreamTestSink,\
    PATTERN_FIXED_STRING, PATTERN_INCREMENTING, PATTERN_FILE_ONCE,\
    PATTERN_FILE_REPEATING
import os

logger = logging.getLogger(__name__)


class SmTaskRunnerStreamSource(SmTaskRunnerBase):
    '''
    classdocs
    '''

    def __init__(self, taskId, doneCallback, taskDesc):
        '''
        Constructor
        '''
        SmTaskRunnerBase.__init__(self, taskId, doneCallback, taskDesc)

        logger.debug("Starting SmTaskRunnerStreamSource task with taskDesc %s", str(taskDesc))
        streamDad = taskDesc.getArgDad(taskDesc.getArgDict(0))
        self.pipeName = streamDad.getAbsPath()
        self.createPipe = taskDesc.getArgDict(0)["create"]
        if self.createPipe:
            os.mkfifo(self.pipeName)
        
        index = 1
        pattern = PATTERN_INCREMENTING
        interval = 0.0
        durationInIntervals = -1
        objectsToSendPerInterval = 1
        while index < len(taskDesc._taskSpecificParamDict["args"]):
            argValue = taskDesc.getArgDict(index)["value"]
            if argValue[0] != "-":
                raise Exception("Arguments must start with '-', found %s", argValue)
            paramName = argValue[1:]
            logger.debug("Examining logger param %s", paramName)
            index += 1
            if paramName == "interval":
                interval = taskDesc.getArgDict(index)["value"]
            elif paramName == "objectsToSendPerInterval":
                objectsToSendPerInterval = taskDesc.getArgDict(index)["value"]
            elif paramName == "durationInIntervals":
                durationInIntervals = taskDesc.getArgDict(index)["value"]
            elif paramName == "pattern":
                pattern = taskDesc.getArgDict(index)["value"]
            elif paramName == "patternString":
                patternString = taskDesc.getArgDict(index)["value"]
            elif paramName == "patternFile":
                patternFile = taskDesc.getArgDict(index)["value"]
            index += 1
                
        logger.debug("Starting SmStreamTestSource on pipe %s, sending %d objects %d times with pattern %s", 
                     self.pipeName, objectsToSendPerInterval, durationInIntervals, pattern)
        self.source = SmStreamTestSource(self.pipeName, objectsToSendPerInterval, interval, durationInIntervals)
        if pattern == PATTERN_FIXED_STRING:
            self.source.setPatternString(patternString)
        elif pattern == PATTERN_FILE_ONCE:
            self.source.setPatternFileOnce(patternFile)
        elif pattern == PATTERN_FILE_REPEATING:
            self.source.setPatternFileRepeating(patternFile)
        self.source.setCallbacks(self.progressCallback, self.genDone)
        
    def start(self):
        self.source.startGenerator()
    
    def cancel(self):
        self.source.stopGenerator()

    def progressCallback(self, streamSourceObject, progress):
        logger.debug("Generator progress for %s to %s", self.taskId, progress)
        self.announceProgress(progress)
        
    def genDone(self, streamSourceObject, status):
        logger.debug("Generator done with status %s, signalling taskDone", status)
        self.done(status, {})
        if self.createPipe:
            os.remove(self.pipeName)
        
        
class SmTaskRunnerStreamSink(SmTaskRunnerBase):
    '''
    classdocs
    '''

    def __init__(self, taskId, doneCallback, taskDesc):
        '''
        Constructor
        '''
        SmTaskRunnerBase.__init__(self, taskId, doneCallback, taskDesc)
        logger.debug("Starting SmTaskRunnerStreamSink task with taskDesc %s", str(taskDesc))
        
        streamDad = taskDesc.getArgDad(taskDesc.getArgDict(0))
        self.pipeName = streamDad.getAbsPath()
        self.createPipe = taskDesc.getArgDict(0)["create"]
        if self.createPipe:
            os.mkfifo(self.pipeName)
        
        pattern = PATTERN_INCREMENTING
        index = 1
        while index < len(taskDesc._taskSpecificParamDict["args"]):
            argValue = taskDesc.getArgDict(index)["value"]
            if argValue[0] != "-":
                raise Exception("Arguments must start with '-', found %s", argValue)
            paramName = argValue[1:]
            index += 1
            if paramName == "objectsExpected":
                objectsExpected = taskDesc.getArgDict(index)["value"]
            elif paramName == "pattern":
                pattern = taskDesc.getArgDict(index)["value"]
            elif paramName == "patternString":
                patternString = taskDesc.getArgDict(index)["value"]
            elif paramName == "patternFile":
                patternFile = taskDesc.getArgDict(index)["value"]
            index += 1
                
        logger.debug("Starting SmStreamTestSink expecting %d items on pipe %s", objectsExpected, self.pipeName)
        self.sink = SmStreamTestSink(self.pipeName, objectsExpected)
        if pattern == PATTERN_FIXED_STRING:
            self.sink.setExpectedPatternString(patternString)
        elif pattern == PATTERN_FILE_REPEATING:
            self.sink.setDestFile(patternFile)
        self.sink.setCallbacks(self.progressCallback, self.genDone)
        
    def start(self):
        self.sink.startConsumer()
    
    def cancel(self):
        self.sink.stopConsumer(1.0)

    def progressCallback(self, streamSourceObject, progress):
        logger.debug("Consumer progress for %s to %s", self.taskId, progress)
        self.announceProgress(progress)
        
    def genDone(self, streamSourceObject, status):
        logger.debug("Consumer done with status %s, signalling taskDone", status)
        self.done(status, {})
        if self.createPipe:
            os.remove(self.pipeName)
        