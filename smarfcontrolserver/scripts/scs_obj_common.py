'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 22, 2014

    @author: rpsmarf
'''
import os


def scsScriptGetDefaultUrl():
    url = os.environ.get("SCS_URL")
    if url is None:
        smarfHost = os.environ.get("SMARF_HOST")
        if smarfHost is None:
            url = "http://localhost:8000"
        else:
            url = "http://" + smarfHost
    return url


def scsScriptGetVerify():
    '''
    This script returns False if the environment variable SMARF_CERT_CHECK is OFF 
    '''
    check = os.environ.get("SMARF_CERT_CHECK")
    if check is None:
        return True
    elif check == "OFF":
        return False
    return True
        
        
def scsScriptGetHeaders():
    d = {"Content-Type": "application/json"}
    authz_hdr = os.environ.get("SMARF_AUTH_HDR")
    if authz_hdr is not None:
        d["Authorization"] = authz_hdr
    return d


