'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Nov 10, 2014

    @author: rpsmarf
'''

import logging

logger = logging.getLogger()

    
class SmMockOpHandlerRecorder(object):
    
    def __init__(self, opResponses):
        self.opResponses = opResponses
        self.eventList = []
        
    def doOp(self, opName, opParamsJson):
        self.eventList.append((opName, opParamsJson))
        try:
            resp = self.opResponses.pop(0)
            logger.debug("Returning response '%s'", resp)
            return resp
        except:
            logger.warning("No response found for operation %s", opName) 
            return "no response defined"
    
    
class SmMockSyncOpsServer(object):
    '''
    This is the mock sync operations server.  This allows an SCS
    to perform task operations without one or more remote agents.
    '''

    def __init__(self, responses):
        '''
        Constructor
        '''
        self.handler = SmMockOpHandlerRecorder(responses)
        
    def doOperation(self, opName, opParamsJson, current=None):
        return self.handler.doOp(opName, opParamsJson)
     
    def getHandler(self, opId):
        return self.handler
