'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import logging
import os
import stat
from time import sleep
import time
import unittest

from smcommon.exceptions.sm_exc import SmException
from smcommon.globals.sm_global_context import smGlobalContextInit
from smcommon.logging.sm_logger import smLoggingSetupForTest
from smcommon.task_runners.task_runner import SmTaskRunnerFactory
from smcommon.task_runners.task_runner_copy import SmTaskRunnerCopy
from smcommon.task_runners.task_runner_delete import SmTaskRunnerDelete
from smcommon.task_runners.task_runner_shell import SmTaskRunnerShell
from smcommon.task_runners.task_runner_stream import SmTaskRunnerStreamSource, SmTaskRunnerStreamSink
from smcommon.tasks.task_desc import SmTaskDesc
from smcommon.tests.dad_test_utils import TempFolder
from smcommon.tests.ssh_server import sm_start_test_sftp_server, \
    sm_test_sftp_server_stop
from smcommon.tests.stream_tester import SmStreamTestSource, SmStreamTestSink
from smcommon.task_runners import task_runner_shell
from smcommon.constants.sm_const import SM_DATA_CONTAINER_PROTOCOL_LOCAL,\
    SM_DATA_CONTAINER_PROTOCOL_SSH
from subprocess import PIPE, Popen
import json
from smcommon.task_runners.task_runner_zip import SmTaskRunnerZip
from smcommon.task_runners.ice_sync_ops_server import SmIceSyncOpsServer
from smcommon.globals.envvar import SM_ENV_VAR_TEST_PASSWORD, smCommonGetEnvVar


logger = logging.getLogger(__name__)


class DynInvokeTestClass():
    
    def __init__(self, taskId, callback, taskDesc):
        self.x = 1
        self.taskId = taskId
        
    def setX(self, x):
        self.x = x
        
        
class SmTestTask(unittest.TestCase):
    def setUp(self):
        smLoggingSetupForTest()
        self.done = False
        self.tempFolder = TempFolder()
        smGlobalContextInit(self.tempFolder.makeCopyController())
        self.output = {}
        self.output["stdout"] = ""
        self.output["stderr"] = ""
        
    def tearDown(self):
        self.tempFolder.cleanup()

    def testDynamicClassCreation(self):
        taskDesc = SmTaskDesc("smcommon.tests.task_tests", "DynInvokeTestClass")
        
        obj = SmTaskRunnerFactory.makeTaskRunner("abc", self._done, taskDesc)
        self.assertEqual(1, obj.x)
        obj.setX(2)
        self.assertEqual(2, obj.x)

    def _done(self, taskId, status, results):
        logger.debug("done method invoked")
        self.done = True
        self.lastTaskId = taskId
        self.lastStatus = status
        self.lastResults = results
    
    def taskDone(self, obj, results):
        logger.debug("done method invoked")
        self.done = True
    
    def progressCallback(self, task, progress):
        logger.debug("progressCallback method invoked with progress=" + str(progress))
    
    def testTestRunner(self):
        self.done = False
        taskDesc = SmTaskDesc("smcommon.tests.task_runner_tester", "SmTaskRunningTester")
        taskDesc.addTaskParam("args", [{"type": "float", "value": 0.5}, {"type": "boolean", "value": False}])
        t = SmTaskRunnerFactory.makeTaskRunner("abc", self._done, taskDesc)
        t.minTimeBetweenProgressCallbacks = 0
        self.assertAlmostEqual(0.0, t.getProgress())
        t.start()
        sleep(0.2)
        self.assertNotAlmostEqual(0.0, t.getProgress())
        sleep(0.4)
        self.assertAlmostEqual(100.0, t.getProgress())
        self.assertTrue(self.done)
        self.assertTrue(t.progressCount >= 4 and t.progressCount <= 6)
        
    def testTestRunnerProgressThrottle(self):
        self.done = False
        taskDesc = SmTaskDesc("smcommon.tests.task_runner_tester", "SmTaskRunningTester")
        taskDesc.addTaskParam("args", [{"type": "float", "value": 0.5}, {"type": "boolean", "value": False}])
        t = SmTaskRunnerFactory.makeTaskRunner("abc", self._done, taskDesc)
        t.minTimeBetweenProgressCallbacks = 0.4
        self.assertAlmostEqual(0.0, t.getProgress())
        t.start()
        sleep(0.2)
        self.assertNotAlmostEqual(0.0, t.getProgress())
        sleep(0.4)
        self.assertAlmostEqual(100.0, t.getProgress())
        self.assertTrue(self.done)
        self.assertTrue(t.progressCount >= 1 and t.progressCount <= 2)

    def testCopyTask(self):
        self.tempFolder.mkdir("d1")
        self.tempFolder.mkdir("d2")
        srcDad = self.tempFolder.makeLocalDad("d1/f1")
        self.tempFolder.writeFile("d1/f1", "hello")
        destDad = self.tempFolder.makeLocalDad("d2/f2")
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_copy", "SmTaskRunnerCopy")
        taskDesc.addTaskParam("args", [{"type": "dad", "direction": "input", "dad": srcDad.toDict()},
                                       {"type": "dad", "direction": "output", "dad": destDad.toDict()}
                                       ])
        t = SmTaskRunnerCopy("id1", self._done, taskDesc)
        
        self.assertAlmostEqual(0.0, t.getProgress()["progress"])
        t.start()
        self.waitUntilDone()
        self.assertAlmostEqual(100.0, t.getProgress()["progress"])
        outputData = self.tempFolder.readFile("d2/f2", 10000)
        self.assertEqual(b"hello", outputData)

    def testCopyTaskWithMultipleInputFiles(self):
        self.tempFolder.mktree(["d1/",
                                "d1/f1",
                                "d1/f2",
                                "d2/",
                                ])
        srcDad = self.tempFolder.makeLocalDadList(["d1/f1", "d1/f2"])
        destDad = self.tempFolder.makeLocalDad("d2/")
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_copy", "SmTaskRunnerCopy")
        taskDesc.addTaskParam("args", [{"type": "dad", "direction": "input", "dad": srcDad.toDict()},
                                       {"type": "dad", "direction": "output", "dad": destDad.toDict()}
                                       ])
        t = SmTaskRunnerCopy("id1", self._done, taskDesc)
        
        self.assertAlmostEqual(0.0, t.getProgress()["progress"])
        t.start()
        self.waitUntilDone()
        self.assertAlmostEqual(100.0, t.getProgress()["progress"])
        outputData = self.tempFolder.readFile("d2/f1", 10000)
        self.assertEqual(b"data-d1/f1", outputData)
        outputData = self.tempFolder.readFile("d2/f2", 10000)
        self.assertEqual(b"data-d1/f2", outputData)

    def testShellTaskWithMultipleInputFiles(self):
        self.tempFolder.mktree(["d1/",
                                "d1/f1",
                                "d1/f2",
                                "d2/",
                                ])
        srcDad = self.tempFolder.makeLocalDadList(["d1/f1", "d1/f2"])
        args = [{"type": "string", "value": "cat"},
                {"type": "dad_file", "direction": "input", "dad": srcDad.toDict()},
                ]
        self.startTask(args, {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL})
        self.assertTrue("d1/f1" in self.output["stdout"])
        self.assertTrue("d1/f2" in self.output["stdout"])

    def testSshShellTaskWithMultipleInputFiles(self):
        self.tempFolder.mktree(["d1/",
                                "d1/f1",
                                "d1/f2",
                                "d2/",
                                ])
        srcDad = self.tempFolder.makeLocalDadList(["d1/f1", "d1/f2"])
        args = [{"type": "string", "value": "cat"},
                {"type": "dad_file", "direction": "input", "dad": srcDad.toDict()},
                ]
        self.startTask(args, {"protocol": "ssh",
                              "host": "localhost",
                              "user": "rpsmarf",
                              "password": smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD),
                              "tempDataFolder": self.tempFolder.folder,
                              "port": 22})
        self.assertTrue("d1/f1" in self.output["stdout"])
        self.assertTrue("d1/f2" in self.output["stdout"])

    def testDeleteTask(self):
        self.tempFolder.mktree(["a/", "a/f1", "a/f2", "b/"])
        srcDad = self.tempFolder.makeLocalDad("")
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_delete", "SmTaskRunnerDelete")
        taskDesc.addTaskParam("args", [{"type": "dad", "direction": "input", "dad": srcDad.toDict()},
                                       ])
        t = SmTaskRunnerDelete("id1", self._done, taskDesc)
        
        self.assertAlmostEqual(0.0, t.getProgress()["progress"])
        t.start()
        self.waitUntilDone()
        self.assertAlmostEqual(100.0, t.getProgress()["progress"])
        self.assertRaises(Exception, self.tempFolder.readFile, "a/f2", 10000)

    def waitUntilDone(self):
        count = 100
        while count > 0:
            count -= 1
            if self.done:
                if self.lastStatus != "success":
                    raise SmException(422, "XXX", "", "Error - lastStatus " + str(self.lastStatus)
                                      + "  lastResults " + str(self.lastResults), )
                return
            time.sleep(0.1)
        raise SmException(422, 'EOTHER', "",
                          "Timeout waiting for task to complete")
    
    def testShellTaskProgress(self):        
        # Make shell task which outputs one byte every 200 ms 4 times (total of 4 bytes)
        scriptPath = self.tempFolder.makeAbsolutePath("progress_script.sh", "local")
        with open(scriptPath, 'w') as the_file:
            the_file.write("#!/bin/bash\n")
            the_file.write("for i in 1 2 3 4; do sleep 0.5; echo -n $i >> " +
                           self.tempFolder.makeAbsolutePath("f1", "local") +
                           ";done\n")
        os.chmod(scriptPath, stat.S_IXUSR | stat.S_IRUSR)
        srcDad = self.tempFolder.makeLocalDad("f1")

        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_shell", "SmTaskRunnerShell")
        taskDesc.addTaskParam("args", [{"type": "string", "value": scriptPath},
                                       {"type": "dad", "direction": "output", "dad": srcDad.toDict()}
                                       ])
        taskDesc.addTaskParam("outputProgress", {"outputArg": 1, "maxSize": 4})
        taskDesc.addTaskParam("execInfo", {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL, 
                                           })
        t = SmTaskRunnerShell("id1", self._done, taskDesc)
        t.minTimeBetweenProgressCallbacks = 0
        t.waitTimeBetweenProgressChecks = 0.05
        self.assertAlmostEqual(0.0, t.getProgress()["progress"])
        t.start()
        
        # Poll task to collect set of progress and outputSize values
        progressValueSet = set()
        sizeSet = set()
        maxCount = 100
        while not self.done:
            time.sleep(0.05)
            progressValueSet.add(t.getProgress()["progress"])
            sizeSet.add(t.getProgress().get("outputSize"))
            maxCount -= 1
            if maxCount == 0:
                raise Exception("Timeout waiting for task to complete")
            
        # Check output file created
        outputData = self.tempFolder.readFile("f1", 10000)
        self.assertEqual(b"1234", outputData)

        # Check that progress callbacks happened
        self.assertTrue(25.0 in progressValueSet)
        self.assertTrue(50.0 in progressValueSet)
        self.assertTrue(75.0 in progressValueSet)
        self.assertTrue(0 in sizeSet)
        self.assertTrue(1 in sizeSet)
        self.assertTrue(2 in sizeSet)
        self.assertTrue(3 in sizeSet)
        
    def stdoutProgressCallback(self, taskId, progressDict):
        self.progressSet.add(progressDict["progress"])

    def testShellStdoutTaskProgress(self): 
        self.progressSet = set()       
        # Make shell task which outputs one line every 200 ms 4 times (total of 4 lines)
        scriptPath = self.tempFolder.makeAbsolutePath("progress_script2.sh", "local")
        with open(scriptPath, 'w') as the_file:
            the_file.write("#!/bin/bash\n")
            the_file.write("for i in 1 2 3 4; do sleep 0.2; echo Hello" +
                           ";done\n")
        os.chmod(scriptPath, stat.S_IXUSR | stat.S_IRUSR)
        srcDad = self.tempFolder.makeLocalDad("f1")

        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_shell", "SmTaskRunnerShell")
        taskDesc.addTaskParam("args", [{"type": "string", "value": scriptPath},
                                       {"type": "dad", "direction": "output", "dad": srcDad.toDict()}
                                       ])
        taskDesc.addTaskParam("outputProgress", {"stdoutLines": "4"})
        taskDesc.addTaskParam("execInfo", {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL, 
                                           })
        t = SmTaskRunnerShell("id1", self._done, taskDesc)
        t.minTimeBetweenProgressCallbacks = 0
        t.setProgressCallback(self.stdoutProgressCallback)
        t.waitTimeBetweenProgressChecks = 0.001
        self.assertAlmostEqual(0.0, t.getProgress()["progress"])
        self.done = False
        self.progressValueSet = set()
        t.start()
        
        # Poll task to collect set of progress and outputSize values
        maxCount = 100
        while not self.done:
            time.sleep(0.05)
            maxCount -= 1
            if maxCount == 0:
                raise Exception("Timeout waiting for task to complete")
            
        # Check that progress callbacks happened
        self.assertTrue(25.0 in self.progressSet)
        self.assertTrue(50.0 in self.progressSet)
        self.assertTrue(75.0 in self.progressSet)
        
    def testShellTaskWithCopy(self):
        '''
        Run a job with the original data and the resulting data on SFTP.
        This triggers copying of the data into the task and copying
        data out of the task.
        '''
        self.tempFolder.writeFile("f1", "123456")
        srcDad = self.tempFolder.makeSftpDad("f1")
        destDad = self.tempFolder.makeSftpDad("f2")
        
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_shell", "SmTaskRunnerShell")
        taskDesc.addTaskParam("args", [{"type": "string", "value": "cp"},
                                       {"type": "dad", "direction": "input", "dad": srcDad.toDict()},
                                       {"type": "dad", "direction": "output", "dad": destDad.toDict()}
                                       ])
        taskDesc.addTaskParam("execInfo", {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL, 
                                           })
        t = SmTaskRunnerShell("id1", self._done, taskDesc)
        # Start SFTP server
        sm_start_test_sftp_server() 

        try:
            t.start()
            self.waitUntilDone()
            
        finally:
            # Stop SFTP server
            sm_test_sftp_server_stop()
        
        # Check output file created
        outputData = self.tempFolder.readFile("f2", 10000)
        self.assertEqual(b"123456", outputData)

    def testShellTaskWithSshCopy(self):
        '''
        Run a job with the original data and the resulting data on SFTP.
        This triggers copying of the data into the task and copying
        data out of the task.
        '''
        task_runner_shell.TASK_RUNNER_SSH_CONNECT_COUNT = 0
        self.tempFolder.writeFile("f1", "123456")
        srcDad = self.tempFolder.makeLocalDad("f1")
        destDad = self.tempFolder.makeLocalDad("f2")
        
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_shell", "SmTaskRunnerShell")
        taskDesc.addTaskParam("args", [{"type": "string", "value": "cp"},
                                       {"type": "dad", "direction": "input", "dad": srcDad.toDict()},
                                       {"type": "dad", "direction": "output", "dad": destDad.toDict()}
                                       ])
        taskDesc.addTaskParam("execInfo", {"protocol": "ssh",
                                           "host": "localhost",
                                           "user": "rpsmarf",
                                           "password": smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD),
                                           "tempDataFolder": self.tempFolder.folder,
                                           "port": 22
                                           })
        t = SmTaskRunnerShell("id1", self._done, taskDesc)
        # Start SFTP server
        sm_start_test_sftp_server() 

        try:
            t.start()
            self.waitUntilDone()
            
        finally:
            # Stop SFTP server
            sm_test_sftp_server_stop()
        
        # Check output file created
        outputData = self.tempFolder.readFile("f2", 10000)
        self.assertEqual(b"123456", outputData)
        self.assertEqual(task_runner_shell.TASK_RUNNER_SSH_CONNECT_COUNT, 1)

    def testStreamTasks(self):
        dad = self.tempFolder.makeFifoDad()

        # Create FIFO Object
        taskDescSrc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_stream", "SmTaskRunnerStreamOutput")
        taskDescSrc.addTaskParam("args", [{"type": "resource", "direction": "output", "dad": dad.toDict(), "create": False},
                                          {"type": "string", "value": "-interval"},
                                          {"type": "float", "value": 0.03},
                                          {"type": "string", "value": "-objectsToSendPerInterval"},
                                          {"type": "int", "value": 5},
                                          {"type": "string", "value": "-durationInIntervals"},
                                          {"type": "int", "value": 4},
                                          ])
        taskDescSrc.addTaskParam("execInfo", {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL, 
                                              })
        tsrc = SmTaskRunnerStreamSource("id1", self._done, taskDescSrc)            
        self.assertAlmostEqual(0.0, tsrc.getProgress()["outputSize"])
        tsrc.start()
        
        taskDescSink = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_stream", "SmTaskRunnerStreamOutput")
        taskDescSink.addTaskParam("args", [{"type": "resource", "direction": "input", "create": False, "dad": dad.toDict()},
                                          {"type": "string", "value": "-objectsExpected"},
                                          {"type": "int", "value": 20}])
        taskDescSink.addTaskParam("execInfo", {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL, 
                                               })
        tsink = SmTaskRunnerStreamSink("id1", self._done, taskDescSink)
        
        self.assertAlmostEqual(0, tsink.getProgress()["outputSize"])
        tsink.start()
        
        self.waitUntilDone()
        self.assertAlmostEqual(20, tsrc.getProgress()["outputSize"])
        self.assertAlmostEqual(20, tsink.getProgress()["outputSize"])

    def testShellTaskWithCpStreams(self):
        '''
        Run a job with the the "cp" shell command copying an input stream
        to an output stream.
        '''
        srcDad = self.tempFolder.makeFifoDad()
        sinkDad = self.tempFolder.makeFifoDad()
        
        # Make a source FIFO, start a generator on that FIFO
        source = SmStreamTestSource(srcDad.getAbsPath(), 2, 0.01, 10)
        source.startGenerator()
        
        # Make a sink FIFO, start a consumer on that FIFO
        sink = SmStreamTestSink(sinkDad.getAbsPath(), 20)
        sink.startConsumer()
        
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_shell", "SmTaskRunnerShell")
        taskDesc.addTaskParam("args", [{"type": "string", "value": "cp"},
                                       {"type": "stream", "direction": "input", "dad": srcDad.toDict()},
                                       {"type": "stream", "direction": "output", "dad": sinkDad.toDict()}
                                       ])
        taskDesc.addTaskParam("execInfo", {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL, 
                                           })
        t = SmTaskRunnerShell("id1", self._done, taskDesc)

        try:
            t.start()
            self.waitUntilDone()
            
        finally:
            pass
        
        # Check that the sink got the data expected
        self.assertFalse(source.errorSeen())
        self.assertFalse(sink.errorSeen())
        self.assertEqual(20, source.getBytesTxed())
        self.assertEqual(20, sink.getBytesRxed())

    def getProcessIdList(self, pattern):
        pidList = []
        process = Popen(['ps', '-eo', 'pid,args'], stdout=PIPE, stderr=PIPE)
        stdout, _ = process.communicate()
        for line in stdout.splitlines():
            pid, cmdline = line.decode('utf-8').lstrip().split(' ', 1)
            if pattern in cmdline:
                logger.debug("pid = '%s' cmd = '%s'", pid, cmdline)
                pidList.append(pid)
        return pidList
    
    def testCancelShellTask(self):
        '''
        Test canceling a shell task
        '''
        
        # Start a sleep task
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_shell", "SmTaskRunnerShell")
        taskDesc.addTaskParam("args", [{"type": "string", "value": "sleep"},
                                       {"type": "string", "value": "4"}
                                       ])
        taskDesc.addTaskParam("execInfo", {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL, 
                                           })
        
        t = SmTaskRunnerShell("id1", self._done, taskDesc)
        t.start()
        time.sleep(0.1)
        
        # Check sleep in process list
        pidList = self.getProcessIdList("sleep 4")
        self.assertEqual(1, len(pidList))
        
        # Send a cancel request
        t.cancel()

        # Check sleep not in process list
        try:
            self.waitUntilDone()
        except SmException:
            pass
            
        pidList = self.getProcessIdList("sleep 4")
        self.assertEqual(0, len(pidList))
        
    def testCancelSshShellTask(self):
        '''
        Test canceling an SSH shell task
        '''
        
        # Start a sleep task
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_shell", "SmTaskRunnerShell")
        taskDesc.addTaskParam("args", [{"type": "string", "value": "sleep"},
                                       {"type": "string", "value": "331"}
                                       ])
        taskDesc.addTaskParam("execInfo", {"protocol": SM_DATA_CONTAINER_PROTOCOL_SSH,
                                           "host": "localhost",
                                           "user": os.environ["USER"],
                                           "password": smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD) 
                                           })
        
        t = SmTaskRunnerShell("id1", self._done, taskDesc)
        t.start()

        # Check sleep in process list
        for _ in range(0, 20):
            pidList = self.getProcessIdList("sleep 331")
            if len(pidList) > 0:
                break
            time.sleep(0.5)
        self.assertEqual(1, len(pidList))
        
        # Send a cancel request
        t.cancel()

        # Check sleep not in process list
        try:
            self.waitUntilDone()
        except SmException:
            pass
            
        for _ in range(0, 20):
            pidList = self.getProcessIdList("sleep 331")
            if len(pidList) == 0:
                break
            time.sleep(0.5)
        self.assertEqual(0, len(pidList))
    
    def testCancelCopyTask(self):
        # Create small directory hierarchy
        self.tempFolder.mktree(["a/", "a/f1", "a/f2", "b/", "b/c/", "b/1", "b/2", "b/3"])
        
        # Issue delete
        srcDad = self.tempFolder.makeLocalDad("a/")
        destDad = self.tempFolder.makeLocalDad("adest/")
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_copy", "SmTaskRunnerCopy")
        taskDesc.addTaskParam("args", [{"type": "dad", "direction": "input", "dad": srcDad.toDict()},
                                       {"type": "dad", "direction": "output", "dad": destDad.toDict()},
                                       ])
        t = SmTaskRunnerDelete("id1", self._done, taskDesc)
        
        self.assertAlmostEqual(0.0, t.getProgress()["progress"])
        t.start()
        
        t.cancel()

        self.waitUntilDone()
        
        # Check that cancel completed 
        for _ in range(0, 10):
            if t.isDone:
                break
            time.sleep(0.2)
        self.assertTrue(t.isDone)
    
    def testCancelDeleteTask(self):
        '''
        This is not a great test because the delete may complete before the copy.  
        This is not a complicated implementation so I'm not too worried about this
        '''
        # Create small directory hierarchy
        self.tempFolder.mktree(["a/", "a/f1", "a/f2", "b/", "b/c/", "b/1", "b/2", "b/3"])
        
        # Issue delete
        srcDad = self.tempFolder.makeLocalDad("a/")
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_delete", "SmTaskRunnerDelete")
        taskDesc.addTaskParam("args", [{"type": "dad", "direction": "input", "dad": srcDad.toDict()},
                                       ])
        t = SmTaskRunnerDelete("id1", self._done, taskDesc)
        
        self.assertAlmostEqual(0.0, t.getProgress()["progress"])
        t.start()
        
        t.cancel()

        self.waitUntilDone()
        
        # Check that cancel completed 
        for _ in range(0, 10):
            if t.isDone:
                break
            time.sleep(0.2)
        self.assertTrue(t.isDone)
    
    def taskOutput(self, taskId, outputType, outputData):
        logger.debug("output[%s] was %s", outputType, self.output[outputType])
        self.output[outputType] += outputData
        logger.debug("output[%s] is now %s", outputType, self.output[outputType])
        
    def startTask(self, argList, execDict):
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_shell", "SmTaskRunnerShell")
        taskDesc.addTaskParam("args", argList)
        taskDesc.addTaskParam("execInfo", execDict)
        t = SmTaskRunnerShell("id1", self._done, taskDesc)
        t.setTaskOutputCallback(self.taskOutput)
        t.start()

        try:
            self.waitUntilDone()
        except SmException:
            pass
        time.sleep(0.1)
            
    def testStdxxxShellTask(self):
        '''
        Test a shell task which outputs to stdout and stdee
        '''
        # Start an echo task
        self.startTask([{"type": "string", "value": "bash"}, 
                        {"type": "string", "value": "-c"},
                        {"type": "string", "value": "echo Hello"}],
                       {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL})
        self.assertEqual(self.output["stdout"], "Hello\n")
            
        self.startTask([{"type": "string", "value": "bash"}, 
                        {"type": "string", "value": "-c"},
                        {"type": "string", "value": "echo Hello > /dev/stderr"}],
                       {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL})
        
        logger.debug("Checking result")
        self.assertEqual(self.output["stderr"], "Hello\n")
            
    def testStdxxxSshShellTask(self):
        '''
        Test stdout/stderr for an SSH shell task
        '''
        self.startTask([{"type": "string", "value": "echo Hello"}],
                       {"protocol": SM_DATA_CONTAINER_PROTOCOL_SSH,
                        "host": "localhost",
                        "user": os.environ["USER"],
                        "password": smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD)})
        
        time.sleep(0.5)    
        self.assertEqual(self.output["stdout"], "Hello\r\n")
        self.output["stdout"] = ""
            
        self.startTask([{"type": "string", "value": "wc"},
                        {"type": "string", "value": "/garbage_xxx"}],
                       {"protocol": SM_DATA_CONTAINER_PROTOCOL_SSH,
                        "host": "localhost",
                        "user": os.environ["USER"],
                        "password": smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD)})
        time.sleep(1.5)    
        logger.debug("Checking result")
        # The data from the failed wc command above should show up in stderr, but 
        # for some reason it shows up in stdout.  Not worth investigating further.
        self.assertTrue("garbage" in self.output["stdout"], self.output["stdout"])
            
    def testScriptWithShellTask(self):
        '''
        Test a shell task which outputs to stdout and stdee
        '''
        # Start an echo task
        self.startTask([{"type": "string", "value": "bash"}, 
                        {"type": "script", "value": "echo Hello"}],
                       {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL})
        self.assertEqual(self.output["stdout"], "Hello\n")
            
        self.startTask([{"type": "string", "value": "bash"}, 
                        {"type": "string", "value": "-c"},
                        {"type": "string", "value": "echo Hello > /dev/stderr"}],
                       {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL})
        
        logger.debug("Checking result")
        self.assertEqual(self.output["stderr"], "Hello\n")
            
    def testScriptWithSshShellTask(self):
        '''
        Test stdout/stderr for an SSH shell task
        '''
        self.startTask([{"type": "string", "value": "bash"}, 
                        {"type": "script", "value": "echo Hello"}],
                       {"protocol": SM_DATA_CONTAINER_PROTOCOL_SSH,
                        "host": "localhost",
                        "user": os.environ["USER"],
                        "password": smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD),
                        "port": 22,
                        "tempDataFolder": "/tmp"})
        
        time.sleep(0.5)    
        self.assertEqual(self.output["stdout"], "Hello\r\n")
            
    def zipProgressCallback(self, taskId, progressDict):
        # Deep copy
        self.progList.append(json.loads(json.dumps(progressDict)))

    def testZipTaskProgress(self):
        self.progList = []
        self.tempFolder.mktree(["a/", "a/b/", "a/b/f1", "a/c/", "a/c/f3"])
        srcDad = self.tempFolder.makeLocalDad("a/")
        destDad = self.tempFolder.makeLocalDad("a.zip")
        
        # Make zip task which zips up the folder above)
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_zip", "SmTaskRunnerZip")
        taskDesc.addTaskParam("args", [{"type": "string", "value": "zip"},
                                       {"type": "string", "value": "-rdc"},
                                       {"type": "dad", "direction": "output", "dad": destDad.toDict()},
                                       {"type": "dad", "direction": "input", "dad": srcDad.toDict()}
                                       ])
        taskDesc.addTaskParam("execInfo", {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL, 
                                           })
        
        t = SmTaskRunnerZip("id1", self._done, taskDesc)
        t.setProgressCallback(self.zipProgressCallback)
        t.minTimeBetweenProgressCallbacks = 0
        t.waitTimeBetweenProgressChecks = 0
        self.assertAlmostEqual(0.0, t.getProgress()["progress"])
        t.start()
        
        maxCount = 100
        while not self.done:
            time.sleep(0.05)
            maxCount -= 1
            if maxCount == 0:
                raise Exception("Timeout waiting for task to complete")
            
        # Check output file created
        outputData = self.tempFolder.readFile("a.zip", 10000)
        self.assertTrue(len(outputData) > 100)

    def commandTest(self, execInfo):
        # Make a task runner shell
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_shell", "SmTaskRunnerShell")
        taskDesc.addTaskParam("execInfo", execInfo)
        t = SmTaskRunnerShell("id1", self._done, taskDesc)
        
        # Send in touch 12345
        code, stdout_string, stderr_string = t.runSyncCommand(["touch", "12345"])
        self.assertEqual(0, code)
        self.assertEqual(b"", stdout_string)
        self.assertEqual(b"", stderr_string)
        # Check that a file in the working directory (d1) called 12345 was created
        s = self.tempFolder.readFile("d1/12345", 100)
        self.assertEqual(0, len(s))
        
        # Do wc on 12345, expect to see data on stdout
        code, stdout_string, stderr_string = t.runSyncCommand(["wc", "12345"])
        self.assertEqual(0, code)
        self.assertEqual(b"0 0 0 12345\n", stdout_string)
   
        # Do wc on 12345, expect to see data on stdout
        code, stdout_string, stderr_string = t.runSyncCommand(["wc", "12345_bad"])
        self.assertEqual(1, code)
        self.assertEqual(b"", stdout_string)
        self.assertEqual(b"wc: 12345_bad: No such file or directory\n", stderr_string)
   
    def testSyncCmdLocal(self):
        self.tempFolder.mkdir("d1")
        execInfo = {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL, 
                    "workingDir": {"type": "string", "value": self.tempFolder.makeAbsolutePath("d1")}}
        self.commandTest(execInfo)
           
    def testSyncCmdSsh(self):
        self.tempFolder.mkdir("d1")
        execInfo = {"protocol": SM_DATA_CONTAINER_PROTOCOL_SSH,
                    "host": "localhost",
                    "user": os.environ["USER"],
                    "password": smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD),
                    "port": 22,
                    "workingDir": {"type": "string", "value": self.tempFolder.makeAbsolutePath("d1")}}
        self.commandTest(execInfo)
        
    def testSyncCmdHandling(self):
        self.tempFolder.mkdir("d1")
        self.tempFolder.mkdir("d1/d2")
        taskDesc = SmTaskDesc("smarf.smcommon.task_runner.sm_task_runner_shell", "SmTaskRunnerShell")
        taskDesc.addTaskParam("args", [{"type": "string", "value": "wc"},
                                       {"type": "string", "value": "d2"},
                                       ])
        taskDesc.addTaskParam("execInfo", {"protocol": SM_DATA_CONTAINER_PROTOCOL_LOCAL, 
                                           "workingDir": {"type": "string", "value": self.tempFolder.makeAbsolutePath("d1")}
                                           })
        server = SmIceSyncOpsServer()
        resp = server.doOperation("shellcommand", taskDesc.toJson())
        respDict = json.loads(resp)
        self.assertEqual(1, respDict["resultcode"])
        self.assertEqual('      0       0       0 d2\n', respDict["stdout"])
        self.assertEqual('wc: d2: Is a directory\n', respDict["stderr"])
        
if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
