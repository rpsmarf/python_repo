#!/bin/bash

echo "Setting up Guacamole..."

cd /tmp

echo "1) Downloading"
sudo apt-get update
sudo apt-get -y install make libcairo2-dev libpng12-dev freerdp-x11 libssh2-1 libfreerdp-dev libvorbis-dev libssl0.9.8 gcc libssh-dev libpulse-dev tomcat7 tomcat7-admin tomcat7-docs libvncserver-dev libossp-uuid-dev
sudo apt-get -y install libpango1.0-dev libssh2-1-dev
wget -O guacamole-server-0.9.6.tar.gz http://sourceforge.net/projects/guacamole/files/current/source/guacamole-server-0.9.6.tar.gz/download
wget -O guacamole-0.9.6.war http://sourceforge.net/projects/guacamole/files/current/binary/guacamole-0.9.6.war/download

echo "2) Building"
sudo rm -rf guacamole-server-0.9.6
tar -xvzf guacamole-server-0.9.6.tar.gz
cd guacamole-server-0.9.6
sudo chown -R $USER.$USER .
./configure --with-init-dir=/etc/init.d
make

echo "3) Installing"
sudo make install
#This sets the autostart for the default runlevels. 
sudo update-rc.d guacd defaults

echo "4) Configuring"
# This essentially loads your config changes
sudo ldconfig
sudo mkdir -p /etc/guacamole
cat > guacamole.properties << EOF
 # Hostname and port of guacamole proxy
 guacd-hostname: localhost
 guacd-port:     4822
 
 # Auth provider class (authenticates user/pass combination, needed if using the provided login screen)
 #auth-provider: net.sourceforge.guacamole.net.basic.BasicFileAuthenticationProvider
 auth-provider: net.sourceforge.guacamole.net.auth.noauth.NoAuthenticationProvider
 noauth-config: /etc/guacamole/noauth-config.xml
 #basic-user-mapping: /etc/guacamole/user-mapping.xml
 lib-directory: /var/lib/guacamole/classpath
EOF
sudo cp guacamole.properties /etc/guacamole/guacamole.properties

cat > user-mapping.xml << EOF
<user-mapping>
    <authorize username="test" password="pa55w0rd">
    <connection name="Ubuntu 14.04">
            <protocol>rdp</protocol>
            <param name="hostname">localhost</param>
            <param name="port">3389</param>
    </connection>
    <connection name="Guac Canarie">
        <protocol>rdp</protocol>
        <param name="hostname">guac.rpsmarf.ca</param>
        <param name="port">3389</param>
    </connection>
    <connection name="Home PC">
        <protocol>rdp</protocol>
        <param name="hostname">pc.test.com</param>
        <param name="port">3389</param>
    </connection>
    </authorize>
</user-mapping>
EOF
sudo cp user-mapping.xml /etc/guacamole/user-mapping.xml

sudo mkdir -p /usr/share/tomcat7/.guacamole
sudo ln -s /etc/guacamole/guacamole.properties /usr/share/tomcat7/.guacamole
sudo mkdir -p /var/lib/tomcat7/webapps
sudo cp ../guacamole-0.9.6.war /var/lib/tomcat7/webapps/guacamole.war
sudo service guacd start
echo "#ADD JAVA_HOME=/usr/lib/jvm/java-8-oracle/" >> tomcat7
sudo cp tomcat7 /etc/default/tomcat7
sudo service tomcat7 restart
sudo chmod 755 /etc/guacamole/user-mapping.xml