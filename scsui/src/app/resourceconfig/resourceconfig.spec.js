describe( 'Resourceconfig module', function() {
	
	beforeEach(module('rpsmarf.config'));
	beforeEach(module('rpsmarf.taskstart'));
	beforeEach(module('rpsmarf.resourceconfig'));
	beforeEach(module('ui.router'));
	beforeEach(module('ui.bootstrap'));
	beforeEach(module('restangular'));
	beforeEach(module('rpsmarf.task.create.service'));
	beforeEach(module('rpsmarf.user.service'));
	beforeEach(module('taskprogress/taskprogress.tpl.html'));

	var $scope, $controller, SmTaskCreate, $state, smarfurl, taskObject, $httpBackend, responseObj, jsonPrefix='src/app/resourceconfig/';
	var rt3s, resource_types, task_type, dataObj, resources, SmUser, USER_STATE, Restangular, user, $modal, $scopeInfoPanel;
	var rootFolderEntry;

	var setHttpBackendExpectations = function(){
		$httpBackend.expect('GET',smarfurl+'resource/?limit=0&personalFolder=True&name_key__icontains=public&extForUser='+user.id).respond(200,dataObj.publicCloudFolders);
	};

	beforeEach(  inject(function(_$controller_, $rootScope, _$interval_, _Restangular_, SMARF_URL, _$modal_, _SmTaskCreate_, _$state_, _$httpBackend_, _SmUser_, _USER_STATE_, DEBUG, ROOT_FOLDER_ENTRY){
		smarfurl = SMARF_URL+'/scs/';
		Restangular = _Restangular_;
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
		SmTaskCreate = _SmTaskCreate_;
		SmUser = _SmUser_;
		user = SmUser.getObject();
		
		rootFolderEntry = ROOT_FOLDER_ENTRY;

		user.id=1;
		user.community = {name_key:'rads'};
		
		$httpBackend = _$httpBackend_;
		$scope = $rootScope.$new();
		$state = _$state_;
		
		dataObj = JSON.parse(__html__[jsonPrefix+'resourceconfig.spec.json']);
		
		$controller = _$controller_;
		USER_STATE = _USER_STATE_;
		
		$modal = _$modal_;



	})) ;

	
	describe('general resource operations (tested in browse user state)', function(){
		beforeEach(inject(function(){
			SmTaskCreate.reset();
			user.id = 2;
			$httpBackend.expect('GET',smarfurl+'resource/?sortForUser='+SmUser.getProperty('id')+'&communityFilter='+SmUser.getProperty('community').name_key+'&extForUser='+SmUser.getProperty('id')+'&limit=0').respond(200,dataObj);
			setHttpBackendExpectations();
			$controller('ResourceConfigCtrl', {$scope:$scope, Restangular:Restangular, SmUser: SmUser, $state: $state, SmTaskCreate: SmTaskCreate, USER_STATE:USER_STATE});
			$httpBackend.flush();
			expect($scope.userState).toEqual(USER_STATE.browse);
			$httpBackend.when('GET',smarfurl+'resource/1/list/').respond(200,dataObj.r1_entries);
			$httpBackend.when('GET',smarfurl+'resource/1/l1/list/').respond(200,dataObj.r1_l1_entries);
		}));

		it('should display the resources when going to the resourceconfig.browse state ', inject([function(){

			spyOn($scope,'$emit');
			
			expect($scope.resources.length).toEqual(12); //12 resources with nature = =data || nature ==compute, filter should be at play here
			expect($scope.resourceParameter).toBeFalsy();

			$httpBackend.expect('GET',smarfurl+'user/1/').respond(200,dataObj.owner);
			$scope.selectResource(dataObj.objects[0], 0);
			
			expect($scope.resourceSelected).toEqual('true');
			expect($scope.selectedResource).toEqual(dataObj.objects[0]);
			$scope.entryLs({},dataObj.r1_entries[4]);
			$httpBackend.flush();
			expect($scope.$emit).toHaveBeenCalledWith('SmObjectSelectInfoPanel',$scope.selectedResource);
			$scope.selectEntry(dataObj.r1_l1_entries[2]);
			
			//
			entry = dataObj.r1_l1_entries[2];
			parent = entry.name.substring(0,entry.name.indexOf(entry.basename));
			expect($scope.selectedResource.selectedEntry).toEqual({ basename : 'tesla/', isDir : true, mtime : 1420648327.5450668, name : 'l1/tesla/', size : 0, sizeText : '---', parent : 'l1/', dateModified : 'Wed Jan 07 2015' });

			
			


		}]));

		it('should get the tools for a compute resource when it is selected in user state browse',inject(function(){
			$httpBackend.expect('GET', smarfurl+'resource_type_task_type/?resource_type__id=23').respond(200,{objects:[{task_type:'/scs/task_type/73/'}]});
			$httpBackend.expect('GET', smarfurl+'task_type/73/').respond(200,{name:'test tool', id:73, owner:'/scs/user/103/'});
			$httpBackend.expect('GET', smarfurl+'user/103/').respond(200,{name:'Owner', id:103});
			$scope.selectResource({nature: 'compute', resource_type: '/scs/resource_type/23/', owner:'/scs/user/1/'},0);
			$httpBackend.flush();

			
		}));

		it('should send the proper values to the accessrequest controller', inject(function(){
			
			spyOn($modal,'open');
			$httpBackend.expect('GET',smarfurl+'resource/1/getperm/').respond(200, dataObj.perms);
			$scope.requestAccess(dataObj.objects[0]);
			$httpBackend.flush();

			expect($modal.open.mostRecentCall.args[0].resolve.object().userPerm).toEqual([{name:'read', hasPerm: true},{name:'write', hasPerm: false},{name:'execute', hasPerm: false}]);

		}));

		it('should open the notes editor with the right parameters', inject([function(){
			spyOn($modal, 'open');
			$scope.resources[0].ext_note = "my note";
			
			$scope.openNotesEditor($scope.resources[0]);

			var args = $modal.open.mostRecentCall.args;

			expect(args[0].controller).toBe('ExtendedInfoCtrl');
			expect(args[0].templateUrl).toBe('extendedinfo/extendedinfo.tpl.html');
			var tagsObj = args[0].resolve.tagsObject();
			var noteObj = args[0].resolve.noteObject();

			expect(tagsObj).toBe(null);
			expect(noteObj).toEqual({typeName: 'resource',typeId: $scope.resources[0].id, ownerId: user.id, exists: true, noteText: $scope.resources[0].ext_note, typeObject:$scope.resources[0]});
			
		}]));

		it('should perform the correct calls when a resource is favourited/unfavourited', inject([function(){
			$httpBackend.expect('POST', smarfurl+'favourite_resource/',{owner: '/scs/user/'+user.id+'/', resource: '/scs/resource/'+$scope.resources[0].id+'/'}).respond(200);
			$scope.favouriteToggle($scope.resources[0]);

			$httpBackend.flush();

			$httpBackend.expect('DELETE', smarfurl+'favourite_resource/?owner__id='+user.id+'&resource__id='+$scope.resources[0].id).respond(200);
			$scope.favouriteToggle($scope.resources[0]);

			$httpBackend.flush();
			
		}]));

		it('should update resources when an SmCommunityChange event is fired in the resourceconfig.browse state', inject(function(){
			user.community.name_key='bridge';
			$httpBackend.expect('GET',smarfurl+'resource/?sortForUser='+SmUser.getProperty('id')+'&communityFilter='+SmUser.getProperty('community').name_key+'&extForUser='+SmUser.getProperty('id')+'&limit=0').respond(200,dataObj);
			setHttpBackendExpectations();
			//$httpBackend.expect('GET',smarfurl+'resource/?limit=0&personalFolder=True&name_key__icontains=public').respond(200,dataObj.publicCloudFolders);
			$scope.$broadcast('SmCommunityChange');
			$httpBackend.flush();


		}));

		it('should refresh resources', inject([function(){
			var objToReturn = {objects:[dataObj.objects[0],dataObj.objects[1]]};
						
			expect($scope.resourceSelected).toEqual('false');
			expect($scope.selectedResource).toBe(null);
			expect($scope.resources.length).toBe(12);
			$httpBackend.expect('GET',smarfurl+'user/1/').respond(200,dataObj.owner);
			$scope.selectResource($scope.resources[0]);
			$httpBackend.flush();
			expect($scope.resourceSelected).toEqual('true');
			expect($scope.selectedResource).toBe($scope.resources[0]);

			$httpBackend.resetExpectations();
			$httpBackend.expect('GET',smarfurl+'resource/?sortForUser='+SmUser.getProperty('id')+'&communityFilter='+SmUser.getProperty('community').name_key+'&extForUser='+SmUser.getProperty('id')+'&limit=0').respond(200,objToReturn);
			setHttpBackendExpectations();
			//$httpBackend.expect('GET',smarfurl+'resource/?limit=0&personalFolder=True&name_key__icontains=public').respond(200,dataObj.publicCloudFolders);
			$scope.refreshResources();
			$httpBackend.flush();
			expect($scope.resources.length).toEqual(2);
			expect($scope.resourceSelected).toEqual('false');
			expect($scope.selectedResource).toBe(null);

		}]));

		it('should filter root entry results based on the user input', inject([function(){
			$httpBackend.expect('GET',smarfurl+'user/1/').respond(200,dataObj.owner);
			$scope.selectResource(dataObj.objects[0], 0);
			$httpBackend.flush();
			expect($scope.resourceSelected).toEqual('true');
			expect($scope.selectedResource).toEqual(dataObj.objects[0]);
			dataObj.r1_entries.unshift(rootFolderEntry);
			expect(angular.copy($scope.selectedResource.entries)).toEqual(dataObj.r1_entries);

			$scope.filterRootEntries('navdeep');

			expect($scope.selectedResource.entries.length).toEqual(2);

			expect($scope.selectedResource.entries).toEqual([dataObj.r1_entries[6],dataObj.r1_entries[7]]);
			


		}]));

		it('should show either the default or public cloud folders based on what is selected', inject(function(filterFilter){
			var processCapacityForResources = function(resources){
				var obj,i,json;
				for(i= 0; i< resources.length; i++){
					obj = resources[i];
					if(obj.personalFolder && obj.capacityJson){
						json = JSON.parse(obj.capacityJson);
						resources[i].full = (json && json.diskUsedState && json.diskUsedState === 'full');
						resources[i].warn = (json && json.diskUsedState && json.diskUsedState === 'warn' );
					}else{
						resources[i].full = false;            
					}
				}
			};
			var defaultResources = angular.copy(dataObj.objects);
			defaultResources = filterFilter(defaultResources,function(value,index){return value.nature === 'data' || value.nature === 'compute';},false); 
			processCapacityForResources(defaultResources);

			expect($scope.resources).toEqual(defaultResources);
			expect($scope.resourcesToShow).toBe('default');
			$scope.showPublicCloudFolders();
			var pcf = angular.copy(dataObj.publicCloudFolders.objects);

			processCapacityForResources(pcf);
			expect($scope.resources).toEqual(pcf);
			expect($scope.resourcesToShow).toBe('publicCloudFolders');
		}));


	});

	describe('task start operations', function(){

		beforeEach(inject(function(){

			task_type = dataObj.task_type;
			rt3s = dataObj.rt3s;
			resource_types = dataObj.resource_types;

			$httpBackend.when('GET',smarfurl+'resource_type_task_type/?task_type__id='+task_type.id).respond(200,{objects:rt3s});
			$httpBackend.when('GET',smarfurl+'resource_type/1/').respond(200,resource_types[0]);
			$httpBackend.when('GET',smarfurl+'resource_type/3/').respond(200,resource_types[2]);
			$httpBackend.when('GET',smarfurl+'resource_type/4/').respond(200,resource_types[3]);

			$httpBackend.when('GET',smarfurl+'resource/?resource_type__id=4').respond(200,{meta:{total_count:1 },objects:[dataObj.objects[5]]}) ;
			$httpBackend.when('GET',smarfurl+'resource/?resource_type__id=1').respond(200,{meta:{total_count:1 },objects:[dataObj.objects[0]]}) ;
			$httpBackend.when('GET',smarfurl+'resource/?resource_type__id=1&sortForUser='+SmUser.getProperty('id')+'&communityFilter='+SmUser.getProperty('community').name_key+'&extForUser='+SmUser.getProperty('id')+'&limit=0').respond(200,{meta:{total_count:1 },objects:[dataObj.objects[0]]}) ;
			$httpBackend.when('GET',smarfurl+'resource/?resource_type__id=3&sortForUser='+SmUser.getProperty('id')+'&communityFilter='+SmUser.getProperty('community').name_key+'&extForUser='+SmUser.getProperty('id')+'&limit=0').respond(200,dataObj);
			$httpBackend.when('GET',smarfurl+'resource/1/list/').respond(200,dataObj.r1_entries);
			$httpBackend.when('GET',smarfurl+'resource/1/l1/list/').respond(200,dataObj.r1_l1_entries);
			$httpBackend.when('GET',smarfurl+'user/'+user.id+'/').respond(200,dataObj.owner);
			
			SmTaskCreate.setTaskType(task_type);
			$httpBackend.flush();
			SmUser.setProperty('state',USER_STATE.task_start);
			SmTaskCreate.setActiveResourceParameterId(1);
			setHttpBackendExpectations();
			//$httpBackend.expect('GET',smarfurl+'resource/?limit=0&personalFolder=True&name_key__icontains=public').respond(200,dataObj.publicCloudFolders);
			//$httpBackend.expect('GET',smarfurl+'resource/?sortForUser='+SmUser.getProperty('id')+'&communityFilter='+SmUser.getProperty('community').name_key+'&extForUser='+SmUser.getProperty('id')+'&limit=0').respond(200,dataObj);
			
			$controller('ResourceConfigCtrl', {$scope:$scope, Restangular:Restangular, SmUser: SmUser, $state: $state, SmTaskCreate: SmTaskCreate, USER_STATE:USER_STATE});
			$httpBackend.flush();

		}));

		it('should allow the root folder to be selected as the resource parameter', inject([function(){
			
			var obj;
			
		
			
			SmTaskCreate.setActiveResourceParameterId(2);

			
			$httpBackend.expect('GET',smarfurl+'user/1/').respond(200,dataObj.owner);
			$scope.selectResource(dataObj.objects[0], 0);
			$httpBackend.flush();
			expect($scope.resourceSelected).toEqual('true');
			expect($scope.selectedResource).toEqual(dataObj.objects[0]);

			spyOn(SmTaskCreate, 'setValueForActiveResourceParameter');
			spyOn($state,'go');
			$scope.rootFolderSelected = true;
			$scope.done();
			$scope.$digest();
			obj = angular.copy(dataObj.objects[0]);
			obj.path = '';
			expect(SmTaskCreate.setValueForActiveResourceParameter.mostRecentCall.args[0].path).toBe('');

		}]));

		it('should set the \'value\' for the given resource parameter (assumes task creation is under way) when in the resourceconfig.taskstart state', inject([function(){
			var activeResource;
			var entry, parent;
			
			$httpBackend.expect('GET',smarfurl+'user/1/').respond(200,dataObj.owner);
			$scope.selectResource(dataObj.objects[0], 0);
			$httpBackend.flush();
			expect($scope.resourceSelected).toEqual('true');
			expect($scope.selectedResource).toEqual(dataObj.objects[0]);
			$scope.entryLs({},dataObj.r1_entries[4]);
			$httpBackend.flush();
			
			$scope.selectEntry(dataObj.r1_l1_entries[2]);
			
			//
			entry = dataObj.r1_l1_entries[2];
			parent = entry.name.substring(0,entry.name.indexOf(entry.basename));
			expect($scope.selectedResource.selectedEntry).toEqual({ basename : 'tesla/', isDir : true, mtime : 1420648327.5450668, name : 'l1/tesla/', size : 0, sizeText : '---', parent : 'l1/', dateModified : 'Wed Jan 07 2015' });

			$scope.done();

			activeResource = SmTaskCreate.getActiveResourceParameter();
			
			expect(activeResource.resource).toEqual({
				"container": "/scs/container/2/",
				"description": "This is your cloud files storage.",
				"gid": 12,
				"icon": "",
				"id": 1,
				"name": "Personal Cloud Files",
				"name_key": "demo_cloud_files",
				"owner": "/scs/user/1/",
				"parametersJson": "{\"folder\": \"files/\"}",
				"resource_type": "/scs/resource_type/1/",
				"resource_uri": "/scs/resource/1/",
				"status": "",
				"nature": "data",
				"ext_perms": "rwx",
				"locked": false,
				selected: true,
				owner_info: { name : 'Henry Tory', email : 'rpsmarf@sce.carleton.ca' },
				path: "l1/tesla/"

			});

		}]));


	});

	describe('Info panel controller', function(){

		beforeEach(inject(function($rootScope){
			$scopeInfoPanel = $rootScope.$new();
			user.id=1;
			user.community = {name_key:'rads'};
			user.first_name = 'Henry';
			user.last_name = 'Tory';
			SmTaskCreate.reset();
			$httpBackend.expect('GET',smarfurl+'resource/?sortForUser='+SmUser.getProperty('id')+'&communityFilter='+SmUser.getProperty('community').name_key+'&extForUser='+SmUser.getProperty('id')+'&limit=0').respond(200,dataObj);
			setHttpBackendExpectations();
			//$httpBackend.expect('GET',smarfurl+'resource/?limit=0&personalFolder=True&name_key__icontains=public').respond(200,dataObj.publicCloudFolders);
			$controller('ResourceConfigCtrl', {$scope:$scope, Restangular:Restangular, SmUser: SmUser, $state: $state, SmTaskCreate: SmTaskCreate, USER_STATE:USER_STATE, $modal:$modal});
			$controller('ResourceConfigInfoPanelCtrl', {$scope:$scopeInfoPanel, Restangular:Restangular, SmUser: SmUser, $state: $state, SmTaskCreate: SmTaskCreate, USER_STATE:USER_STATE, $modal:$modal});
			$httpBackend.flush();

			//expect($scope.userState).toEqual(USER_STATE.browse);
			$scopeInfoPanel.$broadcast('SmInfoPanelUpdate',$scope.resources[0] );
		}));
		it('should get the proper permissions and access requests for the selected resource', inject(function(){

			var accReqObj = angular.copy(dataObj.access_requests.objects[0]);
			accReqObj.requester_info = {name:'Albert Allen', id:'2'};
			accReqObj.access = 'available for tool input';
			

			//$httpBackend.expect('GET',smarfurl+'user/1/').respond(200,dataObj.owner);
			$httpBackend.expect('GET',smarfurl+'resource/1/getperm/').respond(200, dataObj.perms);
			$httpBackend.expect('GET',smarfurl+'request/?target=/scs/resource/1/').respond(200, dataObj.access_requests);
			$httpBackend.expect('GET',smarfurl+'user/1/').respond(200,dataObj.users_info[0]);
			$httpBackend.expect('GET',smarfurl+'user/2/').respond(200,dataObj.users_info[1]);
			$httpBackend.expect('GET',smarfurl+'user/2/').respond(200,dataObj.users_info[1]);

			//$scope.selectResource($scope.resources[0],0);

			$scopeInfoPanel.getPermissions($scopeInfoPanel.selectedResource);

			$httpBackend.flush();

			var users_perm = $scopeInfoPanel.selectedResource.users_perm;
			expect($scopeInfoPanel.selectedResource.hasAccReq).toBe(true);
			var uar = $scopeInfoPanel.userAccessRequests;
			
			expect(uar[0]).toEqual(accReqObj);

			expect(users_perm).toEqual([{isCurrentUser: true, editing: false, read: true, write: true, execute:true, perms: {read:true,write:true, execute:true}, id: '1', name: 'Henry Tory (You)'},
				{isCurrentUser: false, editing: false, read: true, write: false, execute:false, perms: {read:true,write:false, execute:false}, id: '2', name: 'Albert Allen'}]);

			$httpBackend.expect('GET', smarfurl+'resource/1/setperm/?action=assign&notify=true&perm=w&user='+encodeURIComponent('/scs/user/2/')).respond(200);
			$httpBackend.expect('GET', smarfurl+'resource/1/setperm/?action=remove&notify=true&perm=r&user='+encodeURIComponent('/scs/user/2/')).respond(200);
			users_perm[1].read = false, users_perm[1].write = true;

			users_perm[1].editing = true;
			$scopeInfoPanel.toggleEditPermissions(users_perm[1],$scopeInfoPanel.selectedResource,1);

			$httpBackend.flush();
			expect($scopeInfoPanel.selectedResource.users_perm[1].perms.read).toBeFalsy();
			expect($scopeInfoPanel.selectedResource.users_perm[1].perms.write).toBeTruthy();

			//grant access to user
			$httpBackend.expect('GET', smarfurl+'resource/1/setperm/?action=assign&notify=true&perm=x&user='+encodeURIComponent('/scs/user/2/')).respond(200);
			$httpBackend.expect('DELETE', smarfurl+'request/1/').respond(200);
			$scopeInfoPanel.grantAccess(uar[0],0);
			$httpBackend.flush();
			expect($scopeInfoPanel.userAccessRequests.length).toBe(0);
			expect($scopeInfoPanel.selectedResource.users_perm[1].execute).toBeTruthy();

			//deny access
			$httpBackend.expect('DELETE', smarfurl+'request/21/?deny=true').respond(200);
			$scopeInfoPanel.userAccessRequests[0] = {};
			$scopeInfoPanel.denyAccess({id:'21'},0);
			$httpBackend.flush();

		}));

		it('should get the usage info for the selected resource', inject(function(){

			//$httpBackend.expect('GET',smarfurl+'user/1/').respond(200,dataObj.owner);
			//$scope.selectResource($scope.resources[0],0);
			//$httpBackend.flush();

			$httpBackend.expect('GET', smarfurl+'resource/1/getusage/?user='+user.id).respond(200,dataObj.usage_summary);
			$httpBackend.expect('GET', smarfurl+'resource/1/getusers/').respond(200,dataObj.usage_users);

			$scopeInfoPanel.getUsageInfo($scopeInfoPanel.selectedResource);
			$httpBackend.flush();

			expect($scopeInfoPanel.selectedResource.usage_users).toEqual([{user_name: 'Dr Henry Tory', email: 'rpsmarf@sce.carleton.ca', use_count: 30, last_use: jasmine.any(String)}]);
		}));

		it('should get the folder size for a folder', inject(function(){
			var entry = {sizeText:'---',name:'path/to a/folder/'};
			$httpBackend.expect('GET', smarfurl+'resource/1/'+encodeURIComponent('path/to a/folder')+'/getfoldersize/').respond({"result": "ok", "fileCount": 11, "sizeInBytes": 7354852});
			$scopeInfoPanel.calculateFolderSize($scopeInfoPanel.selectedResource,entry);
			expect(entry.sizeText).toBe('calculating...');
			$httpBackend.flush();
			expect(entry.sizeText).toBe('7.01 MB (11 total files/folders)');
		}));

	});
  
});

