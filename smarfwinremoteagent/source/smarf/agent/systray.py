from PyQt4 import QtCore, QtGui
import webbrowser
from smarf.agent.service_ctrl import Service
import logging

logger = logging.getLogger(__name__)


class LocalServiceStatusWidget(QtGui.QDialog):
    status_styles = {
        Service.STATUS_STOPPED: 'color: red',
        Service.STATUS_STARTING: 'color: yellow',
        Service.STATUS_STOPPING: 'color: yellow',
        Service.STATUS_RUNNING: 'color: green',
        Service.STATUS_UNINSTALLED: 'color: red',        
    }
    
    def __init__(self, parent=None):
        super(LocalServiceStatusWidget, self).__init__(parent)
        self.service = Service('RPSmarfRAService', 'rpsra_svc.exe', ('--install',), ('--remove',))
        self.service_status = self.service.status()

        self.init_ui()
        
        self.status_timer = QtCore.QTimer(self)
        self.status_timer.timeout.connect(self.update_status)

    def showEvent(self, event):
        self.status_timer.start(1000)

    def hideEvent(self, event):
        self.status_timer.stop()

    def init_ui(self):
        self.setWindowTitle('RP-SMARF Remote Agent Service')
        self.setWindowFlags(QtCore.Qt.WindowSystemMenuHint)
        
        self.service_status_label = self.create_status_label(self.service_status) 
        layout = QtGui.QGridLayout()
        layout.addWidget(QtGui.QLabel('RP-SMARF Remote Agent Service'), 0, 0)
        layout.addWidget(self.service_status_label, 0, 1)
        
        self.service_button = QtGui.QPushButton("Start")
        self.service_button.clicked.connect(self.toggle_service)
        layout.addWidget(self.service_button, 0, 2)
        
        layout.addWidget(QtGui.QLabel('RP-SMARF Cloud Services'), 1, 0)
        layout.addWidget(self.create_status_label('<b style="color: green">OK</b>'), 1, 1)
        layout.addWidget(QtGui.QLabel('RP-SMARF Website'), 2, 0)
        layout.addWidget(self.create_status_label('<b style="color: green">OK</b>'), 2, 1)

        buttons = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok)
        layout.addWidget(buttons, 3, 0, 1, 3)
        self.setLayout(layout)

        self.connect(buttons, QtCore.SIGNAL("accepted()"), self.close)
        
    def create_status_label(self, status):
        label = QtGui.QLabel(self.style_status_label(status))
        label.setAlignment(QtCore.Qt.AlignCenter)
        return label
    
    def style_status_label(self, status):
        return '<B STYLE="%s">%s</B>' % (self.status_styles.get(status), status)
    
    def update_status(self):
        self.service_status = self.service.status()
        self.service_status_label.setText(self.style_status_label(self.service_status))

        if self.service_status == Service.STATUS_STARTING:
            self.service_button.setEnabled(False)
            self.service_button.setText('Starting')
        elif self.service_status == Service.STATUS_STOPPING:
            self.service_button.setEnabled(False)
            self.service_button.setText('Stopping')
        elif self.service_status in (Service.STATUS_STOPPED, Service.STATUS_UNINSTALLED):
            self.service_button.setEnabled(True)
            self.service_button.setText('Start')
        elif self.service_status == Service.STATUS_RUNNING:
            self.service_button.setEnabled(True)
            self.service_button.setText('Stop')
        
    def toggle_service(self):
        self.service_button.setEnabled(False)
        if self.service_status in (Service.STATUS_STOPPED, Service.STATUS_UNINSTALLED):
            self.service.start()
        elif self.service_status == Service.STATUS_RUNNING:
            self.service.stop()
            
        self.update_status()
        

class SystemTrayWidget(QtGui.QSystemTrayIcon):
    def __init__(self, icon, parent=None):
        QtGui.QSystemTrayIcon.__init__(self, icon, parent)
        self.init_ui()

    def init_ui(self):
        self.localServiceDlg = LocalServiceStatusWidget()
        
        self.menu = QtGui.QMenu(self.parent())
        self.menu.addAction("Agent Configuration", self.show_local_service_dialog)
        self.menu.addAction("RP-SMARF Web Application", self.show_browser)
        self.menu.addSeparator()
        self.menu.addAction("Exit", self.exit)
        self.messageClicked.connect(self.show_local_service_dialog)
        self.setToolTip('RP-SMARF Remote Agent')
        self.setContextMenu(self.menu)
        
    def show_local_service_dialog(self):
        self.localServiceDlg.show()
    
    def show_browser(self):
        webbrowser.open('http://sparrowiq.com/')
    
    def exit(self):
        if QtGui.QMessageBox.question(None, 'RP-SMARF Remote Agent', 'Are you sure you wish to exit?', QtGui.QMessageBox.Yes, QtGui.QMessageBox.No) == QtGui.QMessageBox.Yes:
            QtGui.QApplication.instance().quit()        
