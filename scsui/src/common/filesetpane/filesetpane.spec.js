/**
 * Tests sit right alongside the file they are testing, which is more intuitive
 * and portable than separating `src` and `test` directories. Additionally, the
 * build process will exclude all `.spec.js` files from the build
 * automatically.
 
describe( 'Folder and File  Navigation', function() {
	beforeEach( module( 'rpsmarf.foldernavpane') );

	beforeEach(module('foldernavpane/foldernavpane.tpl.html'));
	var scope, element, entries,timeout,homeSubEntries,documentsSubEntries,dataSubEntries;

		
	describe('Display folder/file entries', function(){

		
		beforeEach(inject(function($rootScope, $compile, $timeout){
			homeSubEntries = [{basename:'documents/', path:'/home/documents/', isDir:true},{basename:'pictures/', path:'/home/pictures/',isDir:true}, {basename:'myfile', path:'/home/documents/myfile',isDir:false}];
			documentsSubEntries = [{basename:'report1.doc', path:'/home/documents/report1.doc',isDir:false},{basename:'paper.pdf', path:'/home/documents/paper.pdf',isDir:false}];
			dataSubEntries = [{basename:'bridge.dat', path:'/home/data/bridge.dat',isDir:false},{basename:'machinery.dat', path:'/home/data/machinery.dat',isDir:false}, {basename:'data.dat', path:'/home/data/data.dat',isDir:false}];

			entries = [{basename:'home/', path:'/home/',isDir:true},{basename:'documents/', path:'/home/documents/',isDir:true},{basename:'data/', path:'/data/',isDir:true},{basename:'file.txt', path:'/file.txt',isDir:false}];
			scope = $rootScope.$new();
			element = angular.element('<foldernavpane entries="entries" ls="entryLs($event, entry)" levelup="levelUp()" smondrop="drop(srcEntries, dstEntries, entry)"></foldernavpane>');
			
			
			timeout = $timeout;
			scope.entries = entries;
			scope.levels = [entries];
			scope.entryLs = function($event, entry){
				console.log(entry);
				switch(entry.path){
					case '/home/':
						scope.entries = homeSubEntries;
						scope.levels.push(homeSubEntries);
						break;
					case '/home/documents/':
						scope.entries = documentsSubEntries;
						scope.levels.push(documentsSubEntries);
						break;
					case '/data/':
						scope.entries = dataSubEntries;
						scope.levels.push(dataSubEntries);
						break;
					case '/home/pictures/':
						scope.entries = [];
						scope.levels.push([]);
						break;
					default:
						scope.entries = [];
						return;
				}
				
				scope.$digest();
				
			};

			scope.levelUp = function(){
				scope.levels.pop();

				scope.entries = scope.levels[scope.levels.length-1];
				scope.$digest();
			};

			element = $compile(element)(scope);

			scope.$digest();

			
			//scope = element.isolateScope();

			
			


		}));

		it( 'should return the correct type and amount of entries for the selected entry', function() {
			
			var isolated = element.isolateScope();
			expect(scope.entries.length).toEqual(entries.length);
			isolated.ls({entry:scope.entries[0]});
			expect(scope.entries.length).toEqual(homeSubEntries.length);
			expect(homeSubEntries).toEqual(scope.entries);

			scope.entries = entries;
			isolated.ls({entry:scope.entries[2]});
			expect(scope.entries.length).toEqual(dataSubEntries.length);
			expect(dataSubEntries).toEqual(scope.entries);
			scope.entries = entries;
			expect(isolated.entryType(scope.entries[3])).toEqual(["fa"," fa-file-text", "entry-icon-file"]);
			expect(isolated.entryType(scope.entries[0])).toEqual(["fa"," fa-folder-open","entry-icon-folder"]);

		});

		it('should display the correct number and types of elements within the navpane', function(){

			var isolated = element.isolateScope();
			expect(element.find('div.entry-container').length).toEqual(scope.entries.length);

			expect(element.find('div.entry-container:eq(0)').parent().attr('id')).toEqual(scope.entries[0].$$hashKey);
			//dblclick home
			isolated.ls({$event: null, entry:scope.entries[0]});

			console.log(isolated.entries);
			//give the DOM time to update. Ugly :(
			//console.log(element);
			expect(element.find('div.entry-container').length).toEqual(scope.entries.length);
			//dblclick pictures
			isolated.ls({entry:scope.entries[1]});
			expect(element.find('div.entry-container').length).toEqual(0);
			//expect(element.find('div.panel-body').html().trim()).toEqual('empty folder');

			//Go back to home level
			isolated.levelup();
			expect(scope.entries).toEqual(homeSubEntries);
			expect(element.find('div.entry-container').length).toEqual(scope.entries.length);

			// console.log(element);

			//timeout.flush();
			

		});



		

		

	});
  
});

*/