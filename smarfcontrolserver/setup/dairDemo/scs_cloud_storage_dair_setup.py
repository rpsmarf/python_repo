import json
import os
import sys
from scripts.scs_obj_common import scsScriptGetDefaultUrl
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID,\
    SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY, SCS_CLOUDIF_SWIFT_ACC_TENANT,\
    SCS_CLOUDIF_SWIFT_ACC_TOKEN_URL, SCS_CLOUDIF_SWIFT_ACC_USERNAME,\
    SCS_CLOUDIF_SWIFT_ACC_PASSWORD
from smarfcontrolserver.init.envvar import scsGetEnvVar,\
    scsLoadDairKeys, SCS_ENV_VAR_TEST_DAIR_PASSWORD
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath, scsGetToJson
from scripts.scs_obj_create import scsCreateObjectPath

try:
    os.mkdir("/tmp/src")
except:
    pass
try:
    os.mkdir("/tmp/dest")
except:
    pass

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

root_path = scsGetObjectPath("user", "username=rpsmarf")
user_a = scsGetObjectPath("user", "username=a")
user_b = scsGetObjectPath("user", "username=b")

rt_storage_path = scsGetObjectPath("resource_type", "name_key=generic_storage")
local_agent_path = scsGetObjectPath("agent", "guid=LOCAL_AGENT")

CLOUD_ACCOUNT_SCHEMA = {
                        "$schema": "http://json-schema.org/draft-04/schema#",
                        "title": "Cloud Account parametersJson Schema",
                        "additionalProperties": False,
                        "type": "object",
                        "properties": {
                                       SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID: 
                                       {"type": "string",
                                        "title": "AWS Key Id",
                                        "description": 
                                        "This is the key id from the 'Security Credentials' page in AWS"},
                                       SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY: 
                                       {"type": "string",
                                        "title": "AWS Secret Key",
                                        "description": "This is the secret key from the 'Security Credentials' page in AWS"},
                                       }
                        }
CLOUD_CONTAINER_SCHEMA = {
                        "$schema": "http://json-schema.org/draft-04/schema#",
                        "title": "Cloud Container parametersJson Schema",
                        "additionalProperties": False,
                        "type": "object",
                        "properties": {
                                       }
                          }

cloud_path = scsCreateObjectPath("cloud", 
                                 {"name": "CANARIE DAIR Storage", 
                                  "name_key": "dair_storage",
                                  "description": "Object representing the DAIR cloud", 
                                  "cloud_type": "openstack",
                                  "cloud_account_param_json_schema": json.dumps(CLOUD_ACCOUNT_SCHEMA),
                                  "cloud_container_param_json_schema": json.dumps(CLOUD_CONTAINER_SCHEMA)})
        
# Create cloud account
scsLoadDairKeys()
cloud_account_path = scsCreateObjectPath("cloud_account",
                                         {"name": "RP-SMARF DAIR Storage Account", 
                                          "name_key": "dair_storage_acct",
                                          "cloud": cloud_path,
                                          "accountInfoJson": json.dumps(
                                                                        {SCS_CLOUDIF_SWIFT_ACC_TENANT: 
                                                                         'NEP-Carleton University & Solana Networks',
                                                                         SCS_CLOUDIF_SWIFT_ACC_TOKEN_URL: 
                                                                         'https://nova-ab.dair-atir.canarie.ca:35357/v2.0/',
                                                                         SCS_CLOUDIF_SWIFT_ACC_USERNAME: 'OMelendez',
                                                                         SCS_CLOUDIF_SWIFT_ACC_PASSWORD: 
                                                                         scsGetEnvVar(SCS_ENV_VAR_TEST_DAIR_PASSWORD)}
                                                                        )
                                          })

cloud_container = scsCreateObjectPath("container", 
                                      {"name": "Dair Cloud Storage Container", 
                                       "name_key": "dair_storage", 
                                       "cloud": cloud_path,
                                       "description": 
                                       "This container supports resources based on the DAIR cloud",
                                       "containerUrl": "swift://swift-ab.dair-atir"
                                       ".canarie.ca:8080/v1/AUTH_4c8f4711207b4b6bb74a1c200e8ac18b",
                                       "status": "up",
                                       "agent": local_agent_path,
                                       "cloud_account": cloud_account_path,
                                       "parametersJson": json.dumps({})})
                                       
res_path = scsCreateObjectPath("resource", {"name": "DAIR Storage", 
                                            "name_key": "dair_storage",
                                            "resource_type": rt_storage_path,
                                            "status": "up",
                                            "owner": root_path,
                                            "container": cloud_container,
                                            "parametersJson": json.dumps({"container": "demo_rpsmarf_ca"}),
                                            "description": "This is a Swift/DAIR storage resource"
                                            })
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=x")
scsGetToJson(res_path + "setperm/?action=assign&user=" + root_path + "&perm=x")

print("AWS Cloud Storage setup.")
