import json
from random import randint
count = 10
propType = ['wind_speed', 'temperature']
pathPrefix = 'dir/f'
data = {}
for x in range(3, count):
    key = pathPrefix + str(x)
    data[key] = [randint(0, 100), randint(-40, 40)]
jsonData = {'propType': propType, 'data': data}
with open('/home/rpsmarf/properties.json', 'w') as outfile:
    json.dump(jsonData, outfile)