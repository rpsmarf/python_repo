describe('service responsible for handling common resource operations (container.resource.service)', function(){

	beforeEach(module('rpsmarf.container.resource.service'));
	beforeEach(module('rpsmarf.config'));
	beforeEach(module('confirm/confirm.tpl.html'));
	beforeEach(module('mkdir/mkdir.tpl.html'));
	beforeEach(module('ui.bootstrap'));
	beforeEach(module('rename/rename.tpl.html'));
	beforeEach(module('alert/alert.tpl.html'));
	beforeEach(module('rpsmarf.constants'));

	var SmContainerResource, $rootScope, Restangular, smarfurl, $httpBackend, $modal;
	var rootFolderEntry;

	beforeEach(inject(function(_SmContainerResource_, _$rootScope_, _Restangular_,SMARF_URL, _$httpBackend_, _$modal_, ROOT_FOLDER_ENTRY){

		$rootScope = _$rootScope_;
		SmContainerResource =_SmContainerResource_;
		Restangular = _Restangular_;
		rootFolderEntry = angular.copy(ROOT_FOLDER_ENTRY);
		rootFolderEntry.selected = false;
		smarfurl = SMARF_URL+'/scs/';
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
		angular.element('body').children().remove();
		$httpBackend = _$httpBackend_;
		$modal = _$modal_;

	}));

	//download: download,deleteSelection: deleteSelection,mkdir: mkdir,upload: upload,refresh: refresh, rename: rename

	it('should make the proper calls for download action',inject([function(){

		var promise;
		//Downloading a file
		var obj = {selectedEntry:{name:'myFile.txt', mode:'rwx'}, id:1};
		spyOn(window, 'open');

		promise = SmContainerResource.download(obj);

		$rootScope.$apply();
		angular.element('body .modal-footer .btn-primary').click();
		$rootScope.$apply();

		//Restangular.configuration.baseUrl+'/resource/'+selectedResource.id+'/'+entry.name+'/download/'
		expect(promise).toBeFalsy();
		expect(window.open).toHaveBeenCalledWith(smarfurl+'resource/'+obj.id+'/'+obj.selectedEntry.name+'/download/');

		//Downloading a folder
		spyOn($modal,'open').andCallThrough();
		obj = {selectedEntry:{name:'someFolder/myFolder/', mode:'rwx',basename:'myFolder/', isDir: true }, id:33};
		//{zipFilePath: ".rpsmarf/tmp/8329c3ca-1f03-4351-8984-2fddc8a18514.zip", taskPath: '/scs/task/234/'}
		//$httpBackend.expect('GET',smarfurl+'resource/33/' + encodeURIComponent('someFolder/myFolder') +'/download/').respond(200);
		promise = SmContainerResource.download(obj);
		expect($modal.open.mostRecentCall.args[0].resolve.body()).toBe("Do you want to download <strong>myFolder/</strong>? It will be downloaded as a zip file.");
		$rootScope.$apply();
		angular.element('body .modal-footer .btn-primary').click();
		$rootScope.$apply();

		//$httpBackend.flush();
		expect(promise).toBeFalsy();
		expect(window.open).toHaveBeenCalledWith(smarfurl+'resource/33/' + encodeURIComponent('someFolder/myFolder') +'/download/');
		//{taskObj: Restangular.one('task',taskId+'/'), taskName:'Preparing folder for download', interactive: false, additionalInfo: {zipFilePath: zipFilePath, folderDownload:true, resourceId: selectedResource.id }}
		//promise.then(function(response){
		//	expect(response.taskObj).toEqual(jasmine.any(Object));
		//	expect(response.taskName).toEqual('Preparing folder for download');
		//	expect(response.interactive).toEqual(false);
		//	expect(response.additionalInfo).toEqual({zipFilePath:".rpsmarf/tmp/8329c3ca-1f03-4351-8984-2fddc8a18514.zip", downloadFolder:true, resourceId: 33});
		//});

	}]));

	it('should make the proper calls for delete action',inject([function(){

		//Delete file
		var obj = {selectedEntry:{name:'myFile.txt'}, id:1, entries:[{name:'myFile.txt'},{name:'myFile2.txt'}]};
		//entry.name,type
		$httpBackend.expect('DELETE', smarfurl+'resource/'+obj.id+'/'+obj.selectedEntry.name+'/file/' ).respond(200);
		//resourceApi.getList(resourceid+parentName+'list/').then(function(objects){
		$httpBackend.expect('GET', smarfurl+'resource/'+obj.id+'/list/' ).respond(200, [{name:'myFile2.txt'}]);


		spyOn(Restangular, 'one').andCallThrough();

		var promise = SmContainerResource.deleteSelection(obj);

		$rootScope.$apply();
		angular.element('body .modal-footer .btn-primary').click();
		$rootScope.$apply();
		$httpBackend.flush();

		expect(Restangular.one).toHaveBeenCalled();
		expect(obj.entries).toEqual([rootFolderEntry,{name:'myFile2.txt'}]);

		//Delete folder
		obj = {selectedEntry:{name:'myFolder/', isDir:true, basename:'myFolder/'}, id:1, entries:[{name:'myFolder/', isDir:true, basename:'myFolder/'},{name:'myFile2.txt'}]};
		angular.element('body').children().remove();

		//'resource/'+selectedResource.id+'/'+entry.name.substring(0,entry.name.length-1),'deleterecursive/'
		$httpBackend.expect('GET', smarfurl+'resource/'+obj.id+'/'+'myFolder'+'/deleterecursive/' ).respond(200, {taskPath:'/scs/task/20/'});
		$httpBackend.expect('GET', smarfurl+'resource/'+obj.id+'/list/' ).respond(200, [{name:'myFile2.txt'}]);

		promise = SmContainerResource.deleteSelection(obj);

		$rootScope.$apply();
		angular.element('body .modal-footer .btn-primary').click();
		
		promise.then(function(taskInfo){
			console.log('in promise');
			expect(taskInfo).toBeTruthy();
			expect(taskInfo.taskObj).toBeTruthy();
			expect(taskInfo.taskName).toEqual('Deletion Task');
			expect(taskInfo.interactive).toBe(false);
		});
		$rootScope.$apply();
		$httpBackend.flush();
		$rootScope.$apply();

		
	}]));

	it('should make the proper calls for mkdir action',inject([function(){
		var obj = {selectedEntry:{name:'folder/'}, id:1, entries:[{name:'myFile.txt'},{name:'myFile2.txt'}]};
		var newFolder = "newFolder";
		//'resource/'+selectedResource.id+'/'+entryPath,folderName+'/mkdir/'
		$httpBackend.expect('GET', smarfurl+'resource/'+obj.id+'/'+obj.selectedEntry.name+newFolder+'/mkdir/').respond(200);
		$httpBackend.expect('GET', smarfurl+'resource/'+obj.id+'/list/' ).respond(200, [{name:'myFile2.txt'}]);

		SmContainerResource.mkdir(obj);
		$rootScope.$apply();
		angular.element('body .modal-body input').val(newFolder);
		angular.element('body .modal-body input').trigger('input');
		angular.element('body .modal-footer .btn-primary').click();
		$rootScope.$apply();
		$httpBackend.flush();

	}]));

	it('should make the proper calls for upload action',inject(['$modal',function($modal){
		var obj = {selectedEntry:{name:'folder/'}, id:1, entries:[{name:'myFile.txt'},{name:'myFile2.txt'}], name:'test resource'};
		spyOn($modal, 'open');

		SmContainerResource.upload(obj);

		expect($modal.open).toHaveBeenCalledWith({
			templateUrl: 'upload/upload.tpl.html',
			controller: 'UploadCtrl',
			resolve: {
				url: jasmine.any(Function)
			}
		});

		expect($modal.open.mostRecentCall.args[0].resolve.url()).toEqual(Restangular.configuration.baseUrl+'resource/'+obj.id+'/'+obj.selectedEntry.name);

		obj.full = true;
		SmContainerResource.upload(obj);
		expect($modal.open).toHaveBeenCalledWith({
			templateUrl: 'alert/alert.tpl.html',
			controller: 'AlertCtrl',
			resolve: jasmine.any(Object)
		});

		expect($modal.open.mostRecentCall.args[0].resolve.message()).toEqual('test resource is full. Cannot perform upload.');
		
	}]));

	it('should make the proper calls for rename action',inject(['$modal',function($modal){
		var obj = {selectedEntry:{name:'file.txt', isDir:false}, id:1};
		var newName = 'new Name';
		//Restangular.one('resource/'+selectedResource.id+'/'+selectedEntry+'rename/?newName='+encodeURIComponent(entry.name)).get().then(function(response){

		$httpBackend.expect('GET',smarfurl+'resource/'+obj.id+'/'+obj.selectedEntry.name+'/rename/?newName='+encodeURIComponent(newName+'.txt')).respond(200);
		$httpBackend.expect('GET', smarfurl+'resource/'+obj.id+'/list/' ).respond(200, [{name:'myFile2.txt'}]);
		SmContainerResource.rename(obj);
		$rootScope.$apply();
		angular.element('body .modal-body input[type="text"]').val(newName);
		angular.element('body .modal-body input[type="text"]').trigger('input');
		angular.element('body .modal-footer .btn-primary').click();
		$rootScope.$apply();
		$rootScope.$apply();
		$rootScope.$apply();
		$httpBackend.flush();


	}]));

	it('should make the proper calls for refresh action',inject([function(){
		var obj = {selectedEntry:{name:'file.txt'}, id:1, entries:[{name:'file.txt'}]};
		$httpBackend.expect('GET', smarfurl+'resource/'+obj.id+'/list/' ).respond(200, [{name:'file.txt'},{name:'myFile2.txt'},{name:'folder/'}]);
		SmContainerResource.refresh(obj);

		$httpBackend.flush();
		expect(obj.selectedEntry).toBeFalsy();
		expect(obj.entries).toEqual([rootFolderEntry,{name:'file.txt'},{name:'myFile2.txt'},{name:'folder/'}]);
		
	}]));

	it('should perform proper actions when performing a editFile action',inject([function(){
		var obj = {selectedEntry:{name:'file.txt', mode:'wx', basename:'file.txt'}, id:1, entries:[{name:'file.txt'}]};
		
		spyOn($modal, 'open');
		SmContainerResource.editFile(obj);

		expect($modal.open).toHaveBeenCalled();
		expect($modal.open.mostRecentCall.args[0].controller).toBe('AlertCtrl');

		obj.selectedEntry.mode = 'rwx';

		SmContainerResource.editFile(obj);
		expect($modal.open).toHaveBeenCalled();
		expect($modal.open.mostRecentCall.args[0].resolve.file()).toEqual({name: 'file.txt', url: 'resource/1/'+encodeURIComponent(obj.selectedEntry.name)+'/'});
		
	}]));

	it('should perform proper actions when performing a zip operation',inject([function(){
		$httpBackend.resetExpectations();
		var obj = {selectedEntry:{name:'folder/', mode:'wx', basename:'folder/', isDir: true}, id:123, entries:[{name:'folder'}]};
		
		spyOn($modal, 'open').andCallThrough();
		SmContainerResource.zip(obj);
		$rootScope.$apply();
		expect($modal.open).toHaveBeenCalled();
		expect($modal.open.mostRecentCall.args[0].controller).toBe('AlertCtrl');

		angular.element('.modal-footer .btn-primary').click();
		$rootScope.$apply();
		

		obj.selectedEntry.mode = 'rwx';

		$httpBackend.expect('GET',smarfurl+'resource/123/folder/zip/?zipFilePath='+encodeURIComponent('testName')).respond(200,{zipFilePath: 'testName.zip', taskPath: '/scs/task/77/'});
		SmContainerResource.zip(obj).then(function(response){
			expect(response.taskName).toBe('Zip');
			expect(response.interactive).toBe(false);
		});
		$rootScope.$apply();
		angular.element('.modal-body input').val('testName.zip');
		angular.element('.modal-body input').trigger('input');
		angular.element('.modal-footer .btn-primary').click();

		expect($modal.open).toHaveBeenCalled();
		expect($modal.open.mostRecentCall.args[0].controller).toBe('MkdirCtrl');
		$httpBackend.flush();
		


	}]));

	it('should perform proper actions when performing an unzip operation',inject([function(){
		$httpBackend.resetExpectations();
		var obj = {selectedEntry:{name:'folder/file.zip', mode:'wx', basename:'file.zip', isDir: false}, id:123, entries:[{name:'folder'}]};
		
		spyOn($modal, 'open').andCallThrough();
		SmContainerResource.unzip(obj);
		$rootScope.$apply();
		expect($modal.open).toHaveBeenCalled();
		expect($modal.open.mostRecentCall.args[0].controller).toBe('AlertCtrl');
		angular.element('.modal-footer .btn-primary').click();
		obj.selectedEntry.mode = 'rwx';

		$httpBackend.expect('GET',smarfurl+'resource/123/'+encodeURIComponent('folder/file.zip')+'/unzip/?zipFilePath='+encodeURIComponent('folder/test unzip Folder')).respond(200,{taskPath: '/scs/task/77/'});
		SmContainerResource.unzip(obj).then(function(response){
			expect(response.taskName).toBe('Unzip');
			expect(response.interactive).toBe(false);
		});
		$rootScope.$apply();
		angular.element('.modal-body input').val('test unzip Folder');
		angular.element('.modal-body input').trigger('input');
		angular.element('.modal-footer .btn-primary').click();
		$rootScope.$apply();

		expect($modal.open).toHaveBeenCalled();
		expect($modal.open.mostRecentCall.args[0].controller).toBe('MkdirCtrl');
		$httpBackend.flush();
		


	}]));

});