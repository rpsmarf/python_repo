'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Feb 23, 2015

    @author: rpsmarf
'''

import unittest
from smcommon.utils.date_utils import smContainsDateString, sm_date_now,\
    smMakeDateString, smMakeNewPath


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testDateContains(self):
        self.assertEqual(-1, smContainsDateString(None))
        self.assertEqual(-1, smContainsDateString("12"))
        self.assertEqual(-1, smContainsDateString("abcdef"))
        self.assertEqual(0, smContainsDateString("20150220T135911Z"))
        self.assertEqual(4, smContainsDateString("test20150220T135911Z"))
        self.assertEqual(4, smContainsDateString("test20150220T135911Ztest"))
        self.assertEqual(-1, smContainsDateString("20150220T135911"))
        self.assertEqual(-1, smContainsDateString("20150220T135911z"))
        self.assertEqual(-1, smContainsDateString("20150220135911Z"))
        self.assertEqual(-1, smContainsDateString("0150220T135911Z"))
        now = smMakeDateString(sm_date_now())
        self.assertEqual(0, smContainsDateString(now))

    def testDateReplace(self):
        new_date_string = "2001010T123456Z"
        self.assertEqual(new_date_string, smMakeNewPath("20150220T135911Z",
                                                        new_date_string))
        self.assertEqual(4, smContainsDateString("test20150220T135911Z"))
        self.assertEqual("test" + new_date_string + "test",
                         smMakeNewPath("test20150220T135911Ztest",
                                       new_date_string))
        self.assertEqual("test_" + new_date_string,
                         smMakeNewPath("test",
                                       new_date_string))

            
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testDateUtils']
    unittest.main()