/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'rpsmarf.tools', [
  'restangular',
  'ui.router',
  'ngSanitize',
  'ui.bootstrap',
  'rpsmarf.resourceconfig',
  'rpsmarf.taskstart',
  'rpsmarf.task.create.service',
  'rpsmarf.user.service',
  'rpsmarf.extendedinfo',
  'rpsmarf.reservation.service',
  'rpsmarf.disqus.embed'
  
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider, RestangularProvider ) {
  $stateProvider.state( 'tools', {
    url: '/tools/',
    views: {
      "main": {
        controller: 'ToolsCtrl',
        templateUrl: 'tools/tools.tpl.html'
      },
      "footer": {
        controller: 'ToolsInfoPanelCtrl',
        templateUrl: 'tools/tools.infopanel.tpl.html'
      }
    },
    data:{ pageTitle: 'Tools' }
  }).state( 'tools.reservation', {
    url: ':compute_resource/',
    views: {
      "main": {
        controller: 'ToolsCtrl',
        templateUrl: 'tools/tools.tpl.html'
      },
      "breadcrumb": {
        controller: 'BreadcrumbCtrl',
        templateUrl: 'breadcrumb/breadcrumb.tpl.html'
      }
    },
    data:{ pageTitle: 'Tools' }
  });
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'ToolsCtrl', ['$scope', 'Restangular', '$state', 'SmTaskCreate', 'filterFilter', 'SmUser', '$modal', 'SmReservation', 'SmDisqus',
  function ( $scope, Restangular, $state, SmTaskCreate, filterFilter, SmUser, $modal, SmReservation, SmDisqus) {

  /***VARIABLES***/
  /*Scope*/
  $scope.tooltasks = [];//[{name:"SPPLASH", summary:"Bridge Vibration Analysis Tool"},{name:"WSAT", summary:"Bridge behaviour modeling"},{name:"WSAT", summary:"Bridge behaviour modeling"},{name:"WSAT", summary:"Bridge behaviour modeling"},{name:"WSAT", summary:"Bridge behaviour modeling"},{name:"WSAT", summary:"Bridge behaviour modeling"},{name:"WSAT", summary:"Bridge behaviour modeling"},{name:"WSAT", summary:"Bridge behaviour modeling"}];
  $scope.tooltask = null;
  $scope.entries = [];//[{name:'home/', path:'/home/'},{name:'documents/', path:'/home/documents/'},{name:'data/', path:'/data/'}];
  $scope.levels = [];
  $scope.srcEntries = [];
  $scope.dstEntries = [];
  $scope.stateName = $state.current.name;

  /***STANDARD***/
  var community = SmUser.getProperty('community').name_key;
  var uid = SmUser.getProperty('id');
  var allToolTasks = null;
  var tasks = Restangular.all('task/');
  var taskResource = Restangular.all('task_resource/');
  var tasktypes = Restangular.one('task_type/?communityFilter='+community+'&extForUser='+uid+'&sortForUser='+uid);
  var modalInstance;
  var modalTransferProgInst;
  
  var reservation = null;
  //map selected users by id
  //var selectedUsers = {};


  /***FUNCTIONS***/
  /*Scope*/
  $scope.clickToolTask = function(tooltask){
    $scope.tooltask = tooltask;
    if(tooltask.selected){
      tooltask.selected = false;
      $scope.tooltask = null;
      $scope.$emit('SmObjectSelectInfoPanel', null);
    }
    else{

      setToolTasksSelectedAs($scope.tooltasks, false);
      tooltask.selected = true;
      Restangular.one('user',tooltask.owner.split('/')[3]+'/').get().then(function(response){
        var name = response.first_name + ' ' + response.last_name;
        $scope.tooltask['owner_info'] = {name: name, email: response.email};
      });

      $scope.$emit('SmObjectSelectInfoPanel', $scope.tooltask);
    }
    

  };

  $scope.startToolTask = function(tooltask, $event, reservation){
    $event.stopPropagation();
    var res = reservation ? '.reservation' : '';
    //Should return promise
    SmTaskCreate.setTaskType(tooltask, reservation).then(function(){
      $state.go('taskstart'+res,{tooltask_namekey:tooltask.name_key});
    });
    

  };

  $scope.filterToolTasks = function(searchText){
    var text = searchText.toLowerCase();
    $scope.tooltasks = filterFilter(allToolTasks, function(value, index){return value.name.toLowerCase().indexOf(text) >=0 || value.description.toLowerCase().indexOf(text) >=0;});
  };

  $scope.favouriteToggle =function(tooltask, $event){
    $event.stopPropagation();
    if(tooltask.ext_favourite){
      Restangular.all('favourite_task_type/').remove({owner__id:uid, task_type__id: tooltask.id}).then(function(){
        tooltask.ext_favourite = false;
      });
    }
    else{
      Restangular.all('favourite_task_type/').post({owner:'/scs/user/'+uid+'/', task_type: '/scs/task_type/'+tooltask.id+'/'}).then(function(){
        tooltask.ext_favourite = true;
      });

    }
  };

  $scope.openNotesEditor = function(tooltask, $event){
    $event.stopPropagation();
    var exists = angular.isDefined(tooltask.ext_note) ? true : false;
    $modal.open({
      controller: 'ExtendedInfoCtrl',
      templateUrl: 'extendedinfo/extendedinfo.tpl.html',
      resolve: {
        tagsObject: function(){ return null;},
        noteObject: function(){ return {typeName: 'task_type',typeId: tooltask.id, ownerId: uid, exists: exists, noteText: tooltask.ext_note, typeObject: tooltask };}
      }
    });
  };

  $scope.headerText = function(){
    if($state.is('tools')){
      return 'Tools Available';
    }
    else{
      return 'Tools to run on '+ reservation.resource_name;
    }
  };

  $scope.requestAccess = function(tooltask,$event){
    $event.stopPropagation();
    var type='task_type', res = angular.copy(tooltask);

    $modal.open({
      controller: 'AccessRequestCtrl',
      templateUrl: 'accessrequest/accessrequest.tpl.html',
      resolve:{
        type: function(){ return type;},
        object: function(){ return res;}
      }
    });
  };

  
  /***STANDARD FUNCTIONS***/
  var setToolTasksSelectedAs = function(tooltasks, val){
    for(var i =tooltasks.length-1; i>=0; i-- ){
      tooltasks[i].selected = val;
    }
  };

  var getLocation = function(href) {
    var location = document.createElement("a");
    location.href = href;
    // IE doesn't populate all link properties when setting .href with a relative URL,
    // however .href will return an absolute URL which then can be used on itself
    // to populate these additional fields.
    if (location.host === "") {
      location.href = location.href;
    }
    return location;
  };
  
  /***EXE***/
  if($state.is('tools')){
    tasktypes.get().then(function(response){
      allToolTasks = filterFilter(response.objects,function(val, index){ return val.uiVisible;});
      $scope.tooltasks = allToolTasks;
    });
  }
  else if($state.is('tools.reservation')){
    reservation = SmReservation.getReservation();
    SmReservation.getTaskTypes().then(function(response){
      allToolTasks = filterFilter(response,function(val, index){ return val.uiVisible;});
      $scope.tooltasks = allToolTasks;
    });
    
   
  }

  //if($scope.disqusEnabled && !SmDisqus.getEmbedInitialized()){
    //SmDisqus.initDisqusEmbed();
  //}
  

}])
.controller( 'ToolsInfoPanelCtrl', ['$scope', 'Restangular', '$state', 'SmTaskCreate', 'filterFilter', 'SmUser', '$modal', 'SmReservation','SmDisqus',
  function ( $scope, Restangular, $state, SmTaskCreate, filterFilter, SmUser, $modal, SmReservation, SmDisqus) {
     /***STANDARD FUNCTIONS***/
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var uid = SmUser.getProperty('id');

    /***SCOPE VARIABLES***/
    //$scope.infoPanel = {collapsed: true};
    $scope.tooltask = null;
    $scope.select = {first: false};
    $scope.userAccessRequests = null;
    //$scope.numSelectedUsers = 0;

    /***SCOPE FUNCTIONS***/
    $scope.infoPanelCollapseToggle = function(){
      $scope.infoPanel.collapsed = !$scope.infoPanel.collapsed;
      $scope.$emit('SmInfoPanelToggle', $scope.infoPanel.collapsed);
    };

    $scope.getPermissions = function(tooltask){
      var perms, isOwner = false, i, user,permObj ;
      tooltask.users_perm = [];
      //selectedUsers = {};
      //$scope.numSelectedUsers = 0;
      if(tooltask.owner.split('/')[3] == uid){
        isOwner = true;
      }
      tooltask.userIsOwner = isOwner;

      Restangular.one('task_type', tooltask.id+'/getperm/').get().then(function(response){
        var map = response.plain();
        for( var key in map){
          user = {isOwner : key.split('/')[3] == uid, selected: false};
          permObj = {};
          perms = map[key];
          getUserInfo(key,user);
          user.execute= false;
          permObj['execute'] = false;
          for(i=perms.length-1; i>=0; i--){
            if(perms[i] === 'execute_task_type'){
              user.execute = true;
              permObj['execute'] = true;
            }
          }
          user['perms'] = permObj;
          tooltask.users_perm.push(user);
        }

      });

      Restangular.all('request/?target='+tooltask.resource_uri).get('').then(function(response){
        if(response.objects.length < 1){
          return;
        }
        else{
          tooltask.hasAccReq = true;
        }
        var req, userObj;
        for(var i=response.objects.length-1; i>=0; i--){
          req = response.objects[i];
          req.requester_info = {name:null, id:null};
          
          getUserInfo(req.requester, req.requester_info);

        }

        $scope.userAccessRequests = response.objects;
      });
    };

    $scope.selectUserToggle = function(user, tooltask){
      if(user.selected){
        selectedUsers[user.id] = user;
        ++$scope.numSelectedUsers;
      }
      else{
        delete selectedUsers[user.id];
        --$scope.numSelectedUsers;
      }
    };

    $scope.setUsersPermission = function(tooltask, action, user, index){
    
      if(action === 'remove'){
        Restangular.one('task_type',tooltask.id+'/setperm/').get({action:action,perm:'x','user':'/scs/user/'+user.id+'/', notify: true}).then(function(){
          console.log('Permission ' + action);
          user.execute = user.perms.execute;
          //--$scope.numSelectedUsers;
          tooltask.users_perm.splice(index,1);
          
        });
      }
      
       

    };

    $scope.getUsageInfo = function(tooltask){
      var usages = [], users = [];
      var user, u, d;
      Restangular.one('task_type', tooltask.id+'/getusage/').get({user:uid}).then(function(response){
        var r = response.plain();
        if(!r.total){
          r['total'] = {w:0,m:0,y:0};
        }
        if(!r.user){
          r['user'] = {w:0,m:0,y:0};
        }
        r.total.user_name = 'All users';
        r.user.user_name = 'You';
        usages.push(r.user);
        usages.push(r.total);
        tooltask.usage_summary = usages;
        //tooltask.usage_summary[1].m=46545;
      });
      Restangular.one('task_type', tooltask.id+'/getusers/').get().then(function(response){
        response = response.plain();
        for(var key in response){
          u = response[key];
          u.title = u.title ? u.title: '';
          d = new Date(u.last_use);
          u.last_use = months[d.getMonth()] + ' ' + d.getDate() + ' ' + d.getFullYear();
          user = {user_name: u.title + ' ' + u.first_name + ' ' + u.last_name, email: u.email, use_count: u.use_count, last_use: u.last_use};
          users.push(user);
        }
        tooltask.usage_users = users;
      });
    };

    $scope.grantAccess = function(request, index){
      console.log(request);
      Restangular.one('task_type',$scope.tooltask.id+'/setperm/').get({user:request.requester,perm:request.access_requested, action:'assign', notify: true}).then(function(){
        console.log('access granted');
        Restangular.one('request',request.id+'/').remove().then(function(){
          var user = {isOwner : false, selected: false}, userInUsersPerm=false, i, u;
          for(i = $scope.tooltask.users_perm.length-1;i>=0; i--){
            u = $scope.tooltask.users_perm[i];
            if(u.id == request.requester.split('/')[3]){
              userInUsersPerm = true;
              u.execute = true;
              u.perms = {};
              u.perms['execute'] = true;
            }
          }

          if(!userInUsersPerm){
            getUserInfo(request.requester,user);
            user.execute = true;
            user.perms = {};
            user.perms['execute'] = true;
            $scope.tooltask.users_perm.push(user);
          }
          

          $scope.userAccessRequests.splice(index,1);
        });
      });
      
    };

    $scope.denyAccess = function(request, index){
      Restangular.one('request',request.id+'/').remove({deny:true}).then(function(){
        console.log('access denied');
        $scope.userAccessRequests.splice(index,1);
      });
    };

    $scope.loadComments = function(tool){
      //var disqusConfig = SmDisqus.getConfig();
      //console.log(disqusConfig);
      if(!SmDisqus.getEmbedInitialized()){
        SmDisqus.initDisqusEmbed('tool/'+tool.name_key,location.origin+'/#!'+tool.name_key,tool.name);
      }
      else{
        DISQUS.reset({
          reload: true,
          config: function () {
              this.page.identifier = 'tool/'+tool.name_key;
              this.page.url = location.origin+'/#!'+tool.name_key;
              this.page.title = tool.name;
              //this.language = 'en';
              //this.page.remote_auth_s3 = disqusConfig.remote_auth_s3;
              //this.page.api_key = disqusConfig.api_key;
          }
        });
      }
        
      
    };

    /*STANDARD FUNCTIONS*/
    // var setUserPermission = function(perm, action, user,tooltask_id){

    // };

    var getUserInfo = function(idPath, userObj){
      var id = idPath.split('/')[3], extra='';
      Restangular.one('user',id+'/').get().then(function(response){
        if(id == uid){
          extra = ' (You)';
        }
        userObj.id = id;
        userObj.name = response.first_name + ' ' + response.last_name + extra;
      });
    };

    /***EXE***/
    $scope.$on('SmInfoPanelUpdate', function(event, tooltask){
      $scope.tooltask = tooltask;
      $scope.select.first = true;

      if(!$scope.tooltask){
        $scope.infoPanel.collapsed=true;
        $scope.$emit('SmInfoPanelToggle', $scope.infoPanel.collapsed);
      }
    });

  }]);

