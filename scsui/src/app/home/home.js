/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'rpsmarf.home', [
  'restangular',
  'ui.router',
  'ui.bootstrap',
  'rpsmarf.user.service',
  'rpsmarf.constants',
  'rpsmarf.resourceconfig.service',
  'rpsmarf.task.create.service',
  'rpsmarf.confirm',
  'rpsmarf.helpers.service',
  'rpsmarf.simpleinput',
  'rpsmarf.reservation.service',
  'rpsmarf.metadata',
  'rpsmarf.postnews'
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider, RestangularProvider ) {
  $stateProvider.state( 'home', {
    url: '/home/',
    views: {
      "main": {
        controller: 'HomeCtrl',
        templateUrl: 'home/home.tpl.html'
      }
    },
    data:{ pageTitle: 'Dashboard' }
  });
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'HomeCtrl', ['$scope', 'Restangular', '$modal','SmUser','SmResourceConfig', '$state', 'SmTaskCreate', 'SmHelpers',
  function HomeController( $scope, Restangular, $modal, SmUser, SmResourceConfig, $state, SmTaskCreate, SmHelpers) {

  /***STANDARD VARIABLES***/
  var uid = SmUser.getProperty('id');
  var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  var uiSettings = SmUser.getProperty('uiSettings');
  var lastAlertIdAcked = uiSettings.lastAlertIdAcked;
  var extraUrlText = lastAlertIdAcked ? '&lastIdAcked='+lastAlertIdAcked : '';

  //community id
  var cid;
  /***SCOPE VARIABLES***/
  $scope.alert = null;
  $scope.showSystemInfo = true;
  $scope.recentTasks = [];
  $scope.userCommunity = SmUser.getProperty('community');
  $scope.favouriteTools = [];
  $scope.favouriteResources = [];
  $scope.communityNews = [];
  $scope.settings = {'sysIntro': {show:true, hide: false}};
  $scope.videoUrls = {'startInteractiveTool':'','startBatchTool':'', 'accessData':''};
  $scope.uid = uid;
  $scope.userOwnsHisCurrentCommunity = uid == $scope.userCommunity.owner.split('/')[3];

  /***SCOPE FUNCTIONS***/

  $scope.browseResource = function(resource){
    SmResourceConfig.setResourceSelected(resource.id);
    $state.go('resourceconfig.browse');
  };

  $scope.startTool = function(tool){
    SmTaskCreate.setTaskType(tool, false).then(function(){
      $state.go('taskstart',{tooltask_namekey:tool.name_key});
    });
  };

  $scope.unfavourite = function(obj, type, index){
    var removeParams = {owner__id:uid};
    removeParams[type+'__id'] = obj.id;
    Restangular.all('favourite_'+type+'/').remove(removeParams).then(function(){
      switch(type){
        case 'resource':
          $scope.favouriteResources.splice(index,1);
          break;
        case 'task_type':
          $scope.favouriteTools.splice(index,1);
          break;
      }
    });
  };

  $scope.rerunTask = function(task){
    var now = new Date();
    var nameModal = $modal.open({
      controller: 'SimpleInputCtrl',
      templateUrl: 'simpleinput/simpleinput.tpl.html',
      resolve: {
        name: function(){return task.task_type_name + '_' + months[now.getMonth()].substring(0,3) + '_' + now.getDate() + '_' + now.getFullYear();}
      }
    });

    var endDate, errMsg, promise;
    
    nameModal.result.then(function(name){
      promise = SmTaskCreate.decomposeAndRerunTask(task, name);
      promise.then(function(response){
        $scope.$emit('SmTaskStart',Restangular.one('task',response.taskId+'/'),name, response.interactive);
        //Shouldn't be needed but just in case
        SmTaskCreate.reset();
      }, function(error){
        var paramsJson = JSON.parse(task.parametersJson);
        SmHelpers.handleErrorOnTaskStart(error, {toolName: task.task_type_name, taskName:name, interactive: paramsJson.interactive},$scope);
      });
    });
      
  };

  $scope.reconfigureTask = function(task){
    var p = SmTaskCreate.reconfigureTask(task);
    

    p.then(function(){
      var tt = SmTaskCreate.getTaskType();
      $state.go('taskstart',{tooltask_namekey:tt.name_key});
    });
  };

  $scope.hideSystemIntro = function(){

    if(!uiSettings['sysIntro']){
      uiSettings['sysIntro'] = {};
    }
    uiSettings.sysIntro.show = false;
    $scope.settings.sysIntro.show = false;
    Restangular.one('user_setting',uid+'/').patch({uiSettingsJson:JSON.stringify(uiSettings)});
  };

  $scope.ackAlert = function(){
    Restangular.all('news_item/getsystemalert/?communityId='+cid+'&lastIdAcked='+$scope.alert.id).get('').then(function(response){
      uiSettings.lastAlertIdAcked = $scope.alert.id;
      if(response.length > 0){
        $scope.alert = response[0];
      }
      else{
        $scope.alert = null;
      }
      Restangular.one('user_setting/',uid+'/').patch({uiSettingsJson: JSON.stringify(uiSettings)});
    });
  };

  $scope.playVideo = function(videoContainerId, vidName){
    var vid = angular.element('#'+videoContainerId);
    var src;

    vid.attr('src', $scope.smProperties[vidName].value+'?autoplay=1');
  };

  $scope.refreshTasks = function(){
    Restangular.all('task/?aggregateParam=True&limit=4&owner='+uid+'&orderByStartTimeDesc=True').get('').then(function(response){
      var pj, dt, objects = response.objects;
      for (var i = objects.length - 1; i >= 0; i--) {
        pj = JSON.parse(objects[i].parametersJson);
        objects[i].task_type_name = pj.task_type_name;
        dt = new Date(objects[i].start_time);
        objects[i].start_time_norm = months[dt.getMonth()] + ' ' + dt.getDate() + ', ' + dt.getFullYear();
        objects[i].pj = pj;
      }

      $scope.recentTasks = objects;
    });
  };

  $scope.postNewsItem = function(){
    var info = {posted: false, communityUri:'/scs/community/'+$scope.userCommunity.id+'/', edit:false };
    var modalInstance = $modal.open({
      controller: 'PostNewsCtrl',
      templateUrl: 'postnews/postnews.modal.tpl.html',
      resolve: {
        info: function(){ return info;}
      }
    });

    modalInstance.result.then(function(){
      if(info.posted){
        getNewsAndAlerts();  
      }
    });
  };

  $scope.removeNewsItem = function(news, index){
    var modalInstance = $modal.open({
      controller: 'ConfirmCtrl',
      templateUrl: 'confirm/confirm.tpl.html',
      resolve: {
        title: function(){ return 'Confirm Deletion';},
        body: function(){ return 'Delete this news item (cannot be undone)?';}
      }
    });
    modalInstance.result.then(function(){
      Restangular.one('news_item',news.id+'/').remove().then(function(response){
        $scope.communityNews.splice(index, 1);
      });
    });
    
  };

  $scope.editNewsItem = function(news){
    var info = {posted: false, edit: true, newsId: news.id, headline:news.headline, body:news.body };
    var modalInstance = $modal.open({
      controller: 'PostNewsCtrl',
      templateUrl: 'postnews/postnews.modal.tpl.html',
      resolve: {
        info: function(){ return info;}
      }
    });
    modalInstance.result.then(function(){
      if(info.posted){
        getNewsAndAlerts();
      }
    });
    
  };
  /***STANDARD FUNCTIONS***/
  //Each favourite type contains an object corresponding to the actual object as a property
  //favourite_task_type = {task_type:{id: 4 ....}, id:2 ....}_
  var getObjectsAsArray = function(objectName, objects){
    var i, newObjects=[];

    for(i = 0; i <  objects.length; i++){
      newObjects.push(objects[i][objectName]);
    }

    return newObjects;
  };

  var getNewsAndAlerts = function(){
    Restangular.all('news_item/getmynews/?communityId='+cid+'&limit=7').get('').then(function(response){
      $scope.communityNews = response;

    });

    Restangular.all('news_item/getsystemalert/?communityId='+cid+extraUrlText).get('').then(function(response){
      if(response.length > 0){
        $scope.alert = response[0];
      }
    });
  };

  /***EXE***/
  cid = $scope.userCommunity.id;
  if(uiSettings['sysIntro']){
    $scope.settings['sysIntro'].show = uiSettings['sysIntro'].show;
    $scope.settings['sysIntro'].hide = !uiSettings['sysIntro'].show;
  }

  Restangular.all('task/?aggregateParam=True&limit=4&owner='+uid+'&orderByStartTimeDesc=True').get('').then(function(response){
    var pj, dt, objects = response.objects;
    for (var i = objects.length - 1; i >= 0; i--) {
      pj = JSON.parse(objects[i].parametersJson);
      objects[i].task_type_name = pj.task_type_name;
      objects[i].version = pj.version || 'latest';
      dt = new Date(objects[i].start_time);
      objects[i].start_time_norm = months[dt.getMonth()] + ' ' + dt.getDate() + ', ' + dt.getFullYear();
      objects[i].pj = pj;
    }

    $scope.recentTasks = objects;
  });

  

  Restangular.all('favourite_task_type/?owner='+uid).get('').then(function(response){
    if(response.objects.length > 0){
      $scope.favouriteTools = getObjectsAsArray('task_type',response.objects);
    }
    

  });

  Restangular.all('favourite_resource/?owner='+uid).get('').then(function(response){
    
    if(response.objects.length > 0){
      $scope.favouriteResources = getObjectsAsArray('resource',response.objects);
    }

  });

  getNewsAndAlerts();

  $scope.$on('SmCommunityChange', function(event){
    $scope.userCommunity = SmUser.getProperty('community');
    cid = $scope.userCommunity.id;
    getNewsAndAlerts();
  });
  
  
}]);

