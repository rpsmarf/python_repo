import json
import sys
from scripts.scs_task_start import scsTaskStartByPath
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath
from scripts.scs_obj_create import scsCreateObjectPath

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

tt_path = scsGetObjectPath("task_type", "name_key=asteroids")

argv = sys.argv
if len(argv) != 2:
    print("Must specify version as first argument!\n")
    print("Do curl " + scsScriptGetDefaultUrl() + tt_path + "/getversions/ to\n")
    print("see the available versions\n")
    sys.exit(1)
else:
    version = argv[1]
    print("Using version " + version)

taskParams = {
    "version": version
    }

root_path = scsGetObjectPath("user", "name=rpsmarf")
task_path = scsCreateObjectPath("task", {"name": "Aseroids", 
                                         "task_type": tt_path,
                                         "owner": root_path,
                                         "parametersJson": json.dumps(taskParams),
                                         }
                                )

rt3_compute_path = scsGetObjectPath("resource_type_task_type", "name_key=asteroids_compute_rt3")


compute_resource_path = scsGetObjectPath("resource", "name=c1_compute")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": compute_resource_path,
                                      "resource_type_task_type": rt3_compute_path
                                      }
                    )

print("Task " + task_path + " to sleep 10 seconds is setup, starting task...")

scsTaskStartByPath(task_path, verbose=True)

