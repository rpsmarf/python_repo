describe( 'User service (SmUser)', function() {
	beforeEach(module('rpsmarf.config'));
	beforeEach(module('rpsmarf.user.service'));
	beforeEach(module('rpsmarf.constants'));
	
	var SmUser;
	beforeEach(inject(function(_SmUser_){
		SmUser = _SmUser_;
	}));

	it('should populate the user with preset values, set values, and erase (invalidate) values', inject(['DEBUG', 'DEFAULT_COMMUNITY_ICON_URL', 'DEFAULT_USER_ICON_URL',function(DEBUG,DEFAULT_COMMUNITY_ICON_URL, DEFAULT_USER_ICON_URL){
		var user = SmUser.getObject();

		if(DEBUG){
			expect(user.id).toBeTruthy();
			expect(user.email).toBeTruthy();
			//user.firstname='Amelia',user.lastname='Brand', user.community={id:1,name:'RADS', name_key: 'rads', description:'The radest group. Located at Carleton University.'};
			expect(user.firstname).toBeTruthy();
			expect(user.lastname).toBeTruthy();
			expect(user.community).toBeTruthy();
			expect(user.community.id).toEqual(1);
			expect(user.community.name).toEqual('Everything');
			expect(user.community.name_key).toEqual('all_resources');
			expect(user.community.description).toEqual('This community represents all tools');
			expect(user.imageUrl).toEqual(DEFAULT_USER_ICON_URL);
			expect(SmUser.getProperty('id')).toBeTruthy();
			
			expect(SmUser.getProperty('community')).toEqual({
				"owner": "/scs/user/1/",
				"description": "This community represents all tools",
				"gid": 2,
				"id": 1,
				"leader": "/scs/user/1/",
				"name": "Everything",
				"name_key": "all_resources",
				"resource_uri": "/scs/community/1/",
				"imageUrl": DEFAULT_COMMUNITY_ICON_URL
			});

			
		}
		else{
			expect(user.id).toBeFalsy();
			expect(user.email).toBeFalsy();
			//user.firstname='Amelia',user.lastname='Brand', user.community={id:1,name:'RADS', name_key: 'rads', description:'The radest group. Located at Carleton University.'};
			expect(user.firstname).toBeFalsy();
			expect(user.lastname).toBeFalsy();
			expect(user.community).toBeFalsy();
			

			expect(SmUser.getProperty('id')).toBeFalsy();
			
			expect(SmUser.getProperty('community')).toBeFalsy();
		}
		
		SmUser.setProperty('id', 2);
		expect(SmUser.getProperty('id')).toEqual(2);

		SmUser.invalidate();

		expect(user.id).toBeFalsy();
		expect(user.firstname).toBeFalsy();
		expect(user.community).toBeFalsy();
		expect(SmUser.getProperty('lastname')).toBeFalsy();

	}]));

	
  
});

