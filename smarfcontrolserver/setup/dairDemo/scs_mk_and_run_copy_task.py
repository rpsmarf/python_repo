import json
import sys
import time
from scripts.scs_task_start import scsTaskStartByPath
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath
from scripts.scs_obj_create import scsCreateObjectPath

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

taskParams = {
              "input_ids": {
                            "1": {"path": "2014-09-01.data"},
                            "2": {"path": "copydata" + time.strftime("%H%M%S")}
                            }
              }

tt_path = scsGetObjectPath("task_type", "name=copy")
root_path = scsGetObjectPath("user", "name=rpsmarf")
task_path = scsCreateObjectPath("task", {"name": "copy", 
                                         "task_type": tt_path,
                                         "owner": root_path,
                                         "parametersJson": json.dumps(taskParams),
                                         }
                                )

resource_type_task_type_data_dest_path = scsGetObjectPath("resource_type_task_type", "name_key=copy_dest_data")
resource_type_task_type_data_src_path = scsGetObjectPath("resource_type_task_type", "name_key=copy_source_data")


src_resource_path = scsGetObjectPath("resource", "name_key=copy_carleton_demo_data_a")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": src_resource_path,
                                      "resource_type_task_type": resource_type_task_type_data_src_path,
                                      }
                    )

dest_resource_path = scsGetObjectPath("resource", "name_key=demo_cloud_files")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": dest_resource_path,
                                      "resource_type_task_type": resource_type_task_type_data_dest_path,
                                      }
                    )

print("Task " + task_path + " to copy /tmp/src/a to /tmp/dest/b is setup, starting task...")

scsTaskStartByPath(task_path, verbose=True)

