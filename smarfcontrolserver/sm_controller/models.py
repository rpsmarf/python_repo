
'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
from django.db import models
from django.contrib.auth.models import User
from smcommon.utils.date_utils import sm_date_now, sm_date_far_future
import re
from django.core.exceptions import ValidationError
import logging
from actstream import registry
from smarfcontrolserver.init.envvar import scsGetEnvVar,\
    SCS_ENV_VAR_DEFAULT_USER_AVATAR_URL, SCS_ENV_VAR_DEFAULT_RESOURCE_AVATAR_URL,\
    SCS_ENV_VAR_DEFAULT_TASK_TYPE_AVATAR_URL,\
    SCS_ENV_VAR_DEFAULT_COMMUNITY_AVATAR_URL,\
    SCS_ENV_VAR_DEFAULT_RESOURCE_COMPUTE_AVATAR_URL
from django.db.models.base import Model
from rdflib.namespace import XSD
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_STATE_INIT
from smcommon.constants.sm_const import SM_COMMUNITY_ALL_NAME_KEY


logger = logging.getLogger()


def smValidateName(value):
    if value == "":
        raise ValidationError(u'Field "name" may not be blank')
    if value is None:
        raise ValidationError(u'Field "name" may not be None')


def smValidateNameKey(value):
    if value == "":
        raise ValidationError(u'Field "name_key" may not be blank')
    if value is None:
        raise ValidationError(u'Field "name_key" may not be None')
    if ' ' in value:
        raise ValidationError(u'Value "name_key" may not contain spaces was "%s"' % value)
    if not value.islower():
        raise ValidationError(u'Value "name_key" must be lower-case only was "%s"' % value)


class SmarfModel(models.Model):
    gid = models.AutoField(primary_key=True)
    
    class Meta:
        db_table = "scs_smarf"
        
    def __str__(self):
        if hasattr(self, 'name'):
            name = str(self.pk) + " - " + self.name
        else:
            name = str(self.pk)
        return (self.__class__.__name__ + ": " + str(name))
        
    def save(self, force_insert=False, force_update=False, using=None, 
             update_fields=None):
        if hasattr(self, 'name'):
            smValidateName(self.name) 
            if hasattr(self, 'name_key'):
                if self.name_key == "":
                    self.name_key = re.sub('[^0-9a-zA-Z]+', '_', self.name.lower())
            smValidateNameKey(self.name_key)
             
        return models.Model.save(self, 
                                 force_insert=force_insert, 
                                 force_update=force_update, 
                                 using=using, 
                                 update_fields=update_fields)
        

class SmModelCloud(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    name = models.CharField(max_length=255, unique=True, validators=[smValidateName])
    name.help_text = "This is the human-readable name of the object"
    name_key = models.CharField(max_length=255, unique=True, validators=[smValidateNameKey])
    name_key.help_text = "This is the programmer-friendly name of this object"
    description = models.TextField()
    description.help_text = "This the human-readable description of this object"
    cloud_type = models.CharField(max_length=64)
    cloud_type.help_text = "This is the type of the cloud.  Values supported are 'aws' and 'open_stack'."
    cloud_account_param_json_schema = models.TextField()
    cloud_account_param_json_schema.help_text = "This is the JSON schema for the parametersJson in the cloud_account " +\
        "objects associated with this type of cloud.  Essentially this field defines what " +\
        "fields are required in the cloud_account object to authenticate on this type of cloud"
    cloud_container_param_json_schema = models.TextField()
    cloud_container_param_json_schema.help_text = "This is the JSON schema for the parametersJson in the container " +\
        "objects associated with this type of cloud.  Essentially this field defines what " +\
        "fields are required in the container object to start a server on this type of cloud"
    
    class Meta:
        db_table = "scs_cloud"


class SmModelCloudAccount(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    cloud = models.ForeignKey(SmModelCloud)
    cloud.help_text = "This is the type of cloud that this account applies to"
    accountInfoJson = models.TextField()
    accountInfoJson.help_text = "This is settings for the cloud account"
    
    class Meta:
        db_table = "scs_cloud_acct"


# Create your models here.
# SmModelAgent related models        
class SmModelAgent(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    guid = models.CharField(max_length=64, unique=True)
    guid.help_text = "This is the globally unique id (GUID) of the remote agent.  This " \
        "value must be configured into the AGENT_GUID environment variable "\
        "specified in the remote agent file /etc/smarf-sra/sra.conf"
    name = models.CharField(max_length=255, unique=True, validators=[smValidateName])
    name.help_text = "This is the human-readable name of the object"
    name_key = models.CharField(max_length=255, unique=True, validators=[smValidateNameKey])
    name_key.help_text = "This is the programmer-friendly name of this object"
    description = models.TextField()
    description.help_text = "This the human-readable description of this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    last_change = models.DateTimeField(null=True)
    last_change.help_text = "This is the date and time of the last state change (i.e. to up or down state)"
    status = models.CharField(max_length=64, default="down")
    status.help_text = "up is the remote agent is reporting heartbeats, down if the " \
        "remote agent has not reported a heartbeat in the last minute or so"
    agentUrl = models.CharField(max_length=255, unique=True)
    agentUrl.help_text = "The protocol, host and port of the remote agent process for this agent. "\
        "A typical value is ice://demo.rpsmarf.ca:9001"
    
    class Meta:
        db_table = "scs_agent"

    
class SmModelContainer(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    name = models.CharField(max_length=255, unique=True, validators=[smValidateName])
    name.help_text = "This is the human-readable name of the object"
    name_key = models.CharField(max_length=255, unique=True, validators=[smValidateNameKey])
    name_key.help_text = "This is the programmer-friendly name of this object"
    
    description = models.TextField()
    description.help_text = "This the human-readable description of this object"
    agent = models.ForeignKey('SmModelAgent')
    agent.help_text = "This is the link to the agent which supports access to this container"
    containerUrl = models.CharField(max_length=255)
    containerUrl.help_text = "This is protocol, host and port for this container.  The path of "\
        "this URL is base path for storage or the working directory for a compute container"
    status = models.CharField(max_length=255, default="down")
    status.help_text = "This is the current state (up or down) for this container"
    parametersJson = models.TextField()
    parametersJson.help_text = ("A JSON dict.\n" +
                                "For a cloud container, this specifies the various parameters required\n" +
                                "to launch a cloud server.  These include:\n" + 
                                "dynamic: true - to indicate that the server is to be created on demand\n" +
                                "imageId: <string> - the name of the image\n" +
                                "instanceType: <string> - the type of the image (e.g. t2.micro)\n" +
                                "region_name: <string> - the name of the region (e.g. us-east-1)\n"
                                )
    cloud = models.ForeignKey(SmModelCloud, blank=True, null=True)
    cloud.help_text = "This is the cloud on which compute servers can run or storage can be resident"
    cloud_account = models.ForeignKey(SmModelCloudAccount, blank=True, null=True)
    cloud_account.help_text = "This is the cloud account which will be used for " +\
        "cloud servers created by tasks running in this container.  If this is " +\
        "empty then the user must have a cloud account of his or her own to " +\
        "start a cloud server"
    
    def save(self, force_insert=False, force_update=False, using=None, 
             update_fields=None):
        if self.agent is not None:
            self.status = self.agent.status
        return super(SmModelContainer, self).save(force_insert=force_insert, 
                                            force_update=force_update, 
                                            using=using, 
                                            update_fields=update_fields)

    class Meta:
        db_table = "scs_container"


class SmModelCloudServer(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    server_id = models.CharField(max_length=64)
    server_id.help_text = "This is the server id of the user"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    cloud_account = models.ForeignKey(SmModelCloudAccount)
    cloud_account.help_text = "This is the account used to run this server"
    container = models.ForeignKey(SmModelContainer)
    container.help_text = "This is the container for this server"
    host = models.CharField(max_length=64)
    host.help_text = "This is the IP address of the server"
    state = models.CharField(max_length=64, default=SCS_CLOUDIF_STATE_INIT)
    state.help_text = "This is the state of the server.  Values include:" +\
        "init - object created, no cloud server start requested yet\n" +\
        "booting - server is started\n" +\
        "running - server is available for use\n" +\
        "stopping - server is shutting down\n" +\
        "stopped - server has stopped\n"
    last_error = models.CharField(max_length=64, default=SCS_CLOUDIF_STATE_INIT)
    last_error.help_text = "This is the error message from the last operations"
    last_state_change_time = models.DateTimeField(default=sm_date_now, null=True)
    last_state_change_time.help_text = "Date and time that the last transition occured"    

    class Meta:
        db_table = "scs_cloud_server"


# SmModelResource related models
NATURE_CHOICES = (("data", "data"),
                  ("compute", "compute"),
                  ("streamout", "streamout"),
                  ("streamin", "streamin"),
                  )


RESOURCE_PARAMETERS_JSON_SCHEMA = {
                          "$schema": "http://json-schema.org/draft-04/schema#",
                          "title": "Resource parametersJson Schema",
                          "additionalProperties": False,
                          "type": "object",
                          "properties": 
                          {
                              "type": {"type": "string"},
                              "args": {
                                    "title": "Arguments for resource as stream",
                                    "type": "array",
                                    "items": {
                                              "title": "Argument used when running a cmd for a stream resource",
                                              "type": "object",
                                              "properties": 
                                              {
                                                  "type": {"type": "string"},
                                                  "value": {},
                                                  "direction": {"type": "string"},
                                                  "role": {"type": "string"},
                                              },
                                              "required": ["type"]
                                              }
                                    },
                              "taskViewBaseUrl": {"type": "string"},
                              "keyfile": {"type": "string"},
                              "user": {"type": "string"},
                              "tempDataFolder": {"type": "string"},
                              "folder": {"type": "string"},
                              "container": {"type": "string"},
                              "bucket": {"type": "string"},
                          }
                          }


class SmModelResource(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    name = models.CharField(max_length=255, unique=True, validators=[smValidateName])
    name.help_text = "This is the human-readable name of the object"
    name_key = models.CharField(max_length=255, unique=True, validators=[smValidateNameKey])
    name_key.help_text = "This is the programmer-friendly name of this object"
    resource_type = models.ForeignKey('SmModelResourceType') 
    resource_type.help_text = "This is the resource type object associated with this resource. "\
        "This describes the type of data (or capabilities) of this resource and defines which "\
        "task types can use this type of resource."
    description = models.TextField()
    description.help_text = "This the human-readable description of this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    container = models.ForeignKey(SmModelContainer)
    container.help_text = "This field links to the container object which defines how to access "
    "this resource."
    # The nature field is cloned at save time from the nature field of the 
    # resource_type object
    nature = models.CharField(max_length=255, choices=NATURE_CHOICES)
    nature.help_text = "Values supported:\n" +\
        "- data - for resources which hold data\n" +\
        "- compute - for resource which compute results" +\
        "- streamout - for resources which stream data out" +\
        "- streamin - for resources which stream data into themselves"
    parametersJson = models.TextField()
    parametersJson.help_text = ("A JSON dict.\n" +
                                "For a resource where resource type 'nature' is 'data' the dict is: \n" +
                                'writeable: True/False\n' +
                                'path: <The path within the resource (e.g. dir/file)\n' +
                                "For a resource where resource type 'nature' is 'streamin' or 'streamout'" +
                                "the dict looks like:\n" +
                                ' "args": [ ' +
                                '{ "type": "string", "value" : "cp" },' + 
                                '{ "type": "string", "value" : "/dev/zero" },' + 
                                '{ "type": "resource", "direction": "output", "role" : "stream_output" },' +
                                '],' +
                                "For a resource where resource type 'nature' is 'compute'" +
                                "the dict can optionally contain a base URL for interactive tools:\n" +
                                ' "taskViewBaseUrl": "http://<target system host>/<path>"\n' +
                                'and settings to control the execution of the task as follows:\n'
                                '  "user": "rpsmarf" - the user account under which to run the task \n'
                                '  "tempDataFolder": "/tmp" - the folder to use for SSH tasks to store data locally \n'
                                '  or to hold files which have been zipped up for downloading\n'
                                '  "user": "rpsmarf" - the user account under which to run the task \n'
                                '  "password": "xxx" - the password for the user account under which to run the task \n'
                                '  "keyfile": "dropbear_key" - the name of the key file for the SSH connection\n' +
                                ' (if any).  Note that the file name is relative to the key folder of the remote agent\n' +
                                ' which defaults to /etc/smarf-sra/keys\n'
                                '\n' +
                                ' Note that the input_id fields are resolved by substitutions from the parametersJson' +
                                'field from a task object when the task is started'
                                )
    status = models.CharField(max_length=255)
    status.help_text = "This is the current state (up or down) for this resource"
    locked = models.BooleanField(default=False)
    locked.help_text = ("If this is true then this resource is not usable by anyone except the resource owner." +
        " It has been disabled by the resource owner.")
    personalFolder = models.BooleanField(default=False)
    personalFolder.help_text = "If this is true then this resource is writable only by the owner of the resource"
    capacityJson = models.TextField(default="{}")
    capacityJson.help_text = ("This is a JSON string which can have the following optional elements:\n" +
        " pollDiskUsed - true to poll to calculate capacity \n" +
        " diskUsed - Approximate number of bytes used in files for this resource\n" +
        " diskUsedWarnThreshold - Number of bytes to declare resource in 'warn' state " +
        "(user is warned to delete data upon use) \n" +
        " diskUsedFailThreshold - Number of bytes to declare resource in 'full' state " +
        "(output to this resource is not allowed) \n" +
        " diskUsedState - One of 'normal', 'warn', 'full'.  " +
        "This indicates how the system should react to usage of this resource. \n" +
        "")
    imageUrl = models.CharField(max_length=255, default=scsGetEnvVar(SCS_ENV_VAR_DEFAULT_RESOURCE_AVATAR_URL))
    imageUrl.help_text = "This is the icon associated with this resource"
        
    def save(self, force_insert=False, force_update=False, using=None, 
             update_fields=None):
        self.nature = self.resource_type.nature
        if self.nature == "compute":
            if self.imageUrl == scsGetEnvVar(SCS_ENV_VAR_DEFAULT_RESOURCE_AVATAR_URL):
                self.imageUrl = scsGetEnvVar(SCS_ENV_VAR_DEFAULT_RESOURCE_COMPUTE_AVATAR_URL)
        self.status = self.container.status
        return super(SmModelResource, self).save(force_insert=force_insert, 
                                                 force_update=force_update, 
                                                 using=using, 
                                                 update_fields=update_fields)
        
    class Meta:
        db_table = "scs_resource"
        permissions = (
                       ('read_resource_data', 'Read resource data'),
                       ('write_resource_data', 'Write resource data'),
                       ('execute_on_resource', 'Run software on resource'),
                      )


class SmModelResourceType(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    name = models.CharField(max_length=255, unique=True, validators=[smValidateName])
    name.help_text = "This is the human-readable name of the object"
    name_key = models.CharField(max_length=255, unique=True, validators=[smValidateNameKey])
    name_key.help_text = "This is the programmer-friendly name of this object"
    description = models.TextField()
    description.help_text = "This the human-readable description of this object"
    # The fields starting with code_ are only used if the 
    # resource type nature field is streamin or streamout
    code_module = models.CharField(max_length=255)
    code_module.help_text = "This is the name of the Python module in the SMARF Remote Agent " \
        "in which the class described by the code_classname field is present"
    code_classname = models.CharField(max_length=255)
    code_classname.help_text = "This is the name of the class which will be invoked to run " \
        "software associated with streams for this resource type.  This field is used only " \
        "if nature is streamin or streamout"
    nature = models.CharField(max_length=255)
    nature.help_text = "Values supported:\n" +\
        "- data - for resources which hold data\n" +\
        "- compute - for resource which compute results" +\
        "- streamout - for resources which stream data out" +\
        "- streamin - for resources which stream data into themselves"
    configuration = models.TextField()
    configuration.help_text = "Dict with this following fields:\n" +\
        "pipeFolder - used if nature is stream*, specifies folder for named " +\
        "pipes to be created in"

    class Meta:
        db_table = "scs_resource_type"


class SmModelResourceTag(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    tag = models.ForeignKey('SmModelTag')
    tag.help_text = "This is the tag object that this object is associating "\
        "to the specified resource"
    resource = models.ForeignKey('SmModelResource')
    resource.help_text = "This is the resource that is tagged by this object"
    
    class Meta:
        db_table = "scs_resource_tag"


class SmModelTaskTypeTag(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    tag = models.ForeignKey('SmModelTag')
    tag.help_text = "This is the tag object that this object is associating "\
        "to the specified resource"
    task_type = models.ForeignKey('SmModelTaskType')
    task_type.help_text = "This is the task type that is tagged by this object"
    
    class Meta:
        db_table = "scs_task_type_tag"


class SmModelTag(SmarfModel): 
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    tag_name = models.CharField(max_length=255, unique=True)
    tag_name.help_text = "This is the tag string for the tag that this object represents"
    
    class Meta:
        db_table = "scs_tag"


class SmModelResourceTypeTaskType(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    task_type = models.ForeignKey('SmModelTaskType')
    task_type.help_text = "This is the task type that this record associates the specified resource type to"
    resource_type = models.ForeignKey('SmModelResourceType')
    resource_type.help_text = "This is the resource type that this record associates the specified task type to"
    role = models.CharField(max_length=255)
    role.help_text = "This is the name of the relationship that this record described.  This string"\
        "typically matches one of the roles described in the task_type configurationJson field."
    name = models.CharField(max_length=255, unique=True, validators=[smValidateName])
    name.help_text = "This is the human-readable name of the object"
    name_key = models.CharField(max_length=255, unique=True, validators=[smValidateNameKey])
    name_key.help_text = "This is the programmer-friendly name of this object"
    description = models.TextField()
    description.help_text = "This the human-readable description of this object"
    metadataJson = models.TextField()
    metadataJson.help_text = (
                              "Settings include (for resources where resource_type.nature='data'):\n" +
                              "type: 'file' or 'folder' to specify whether the data resource" +
                              " is a file or folder\n"
                              "access: 'r', 'rw'\n"
                              )
    nature = models.CharField(max_length=255)
    nature.help_text = "Values supported:\n" +\
        "- data - for resources which hold data\n" +\
        "- compute - for resource which compute results" +\
        "- streamout - for resources which stream data out" +\
        "- streamin - for resources which stream data into themselves"

    class Meta:
        db_table = "scs_rtype_ttype"


class SmModelFavouriteResource(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the user with whom this favorite is associated"
    resource = models.ForeignKey('SmModelResource')
    resource.help_text = "This is the resource which has is marked as a favorite by this object"

    class Meta:
        db_table = "scs_fav_resource"


class SmModelFavouriteTaskType(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the user with whom this favorite is associated"
    task_type = models.ForeignKey('SmModelTaskType')
    task_type.help_text = "This is the task type which has is marked as a favorite by this object"

    class Meta:
        db_table = "scs_fav_task_type"


TASK_PARAMETERS_JSON_SCHEMA = {
                          "$schema": "http://json-schema.org/draft-04/schema#",
                          "title": "Task Type parametersJson Schema",
                          "type": "object",
                          "additionalProperties": False,
                          "properties": 
                          {
                              "input_ids": 
                              {
                                    "title": "Substitutions for running this task",
                                    "type": "object",
                                    "properties":
                                    {
                                     "path": {"type": "string"},
                                     "value": {"type": "string"},
                                    },
                              },
                              "version": {"type": "string"},
                          }
                               }


# SmModelTask related models
class SmModelTask(Model):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    name = models.CharField(max_length=255, blank=True)
    name.help_text = "This is human-readable information about the task"
    name_key = models.CharField(max_length=255, blank=True)
    name_key.help_text = "This is the programmer-friendly name of this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    computeResource = models.ForeignKey(SmModelResource, blank=True, null=True)
    computeResource.help_text = "This is the resource on which this task runs.  It is set automatically at task start time."
    start_time = models.DateTimeField(default=sm_date_now, null=True)
    start_time.help_text = "Date and time that the task was created or started"
    end_time = models.DateTimeField(default=sm_date_now, null=True)
    end_time.help_text = "Date and time that the task was last updated (while running) or ended."
    task_type = models.ForeignKey('SmModelTaskType')
    task_type.help_text = "This is the task type record describing the 'type' of this task"
    
    runningDisplayJson = models.TextField()
    runningDisplayJson.blank = True
    runningDisplayJson.help_text = ("This string contains information about what to display\n" +
                                    "while a task is running.  If this is not empty when the state\n" + 
                                    "transitions to 'running' then the GUI should change the user's display\n" +
                                    "to a screen based on the information in this field." +
                                    "It is a JSON dict with the following fields (for example): \n" +
                                    '{ "taskView":{' +
                                    '"url": "http://ra.com/display?authToken=abc123",' +
                                    '"layout": "newWindow"}' +
                                    '}')
    
    parametersJson = models.TextField()
    parametersJson.blank = True
    parametersJson.help_text = ("This string contains task-specific settings which are merged with the 'configuration'" + 
                                " field of the associated task_type object. " + 
                                "It is a JSON dict with the following fields (for example): \n" +
                                '{ "input_ids":{\n' +
                                '     "2": {"path": "june_2014/"},\n' +
                                '     "3": {"path": "my_output/"},\n' +
                                '     "1": {"value": "-r" }\n' +
                                '     },\n' +
                                '  "version": "1.3"\n' +
                                "}\n" +
                                " Note that if the parameter aggregateParam is set to True on a get through the REST" +
                                "API then the parameters are aggregated with information from other objects to support" +
                                "displaying the parameters with fewer REST calls in the UI.  If aggregateParam is True " +
                                "then the parametersJson string will be of the form:\n" +
                                '{ ' +
                                '  "task_type_name": "SPPLASH Tool",'
                                '  "parameters":' +
                                ' [' +
                                '   { "name": "Damping Factory", "input_id": 2, "type": "int", "value": "22"}.' + 
                                '   { "name": "Output Precision", "input_id": 3, "type": "string", "value": "low"}.' +
                                ' ],' +
                                '{ "resources":' +
                                ' [' +
                                '   { "name": "Bridge Data Folder",  "input_id": 1, "nature": "data",' + 
                                '     "resourceName": "PEI Bridge Data", "resourcePath": "/scs/resource/2",' +
                                '     "direction": "input", "path": "/2014-01/12-23-00.xml"}' +
                                '   { "name": "Results Folder", "nature": "data", ' + 
                                '     "resourceName": "Personal Cloud Folder", "resourcePath": "/scs/resource/8",' +
                                '     "direction": "input", "path": "/2014-01/"}' +
                                '   { "name": "Video Stream", "nature": "streamin", ' + 
                                '     "resourceName": "Video Stream From NRC Rm 213", "resourcePath": "/scs/resource/17",' +
                                '     "direction": "input"}'
                                '   { "nature": "compute", ' + 
                                '     "resourceName": "Video Processor", "resourcePath": "/scs/resource/22"}' +
                                ' ],' +
                                ' "input_ids":{' +
                                '    { "2": {"path": "june_2014/"  },' +
                                '    { "3": {"path": "my_output/" },' +
                                '    { "1": {"value": "-r" }' +
                                '  }' +
                                '}'
                                )
    
    resultsJson = models.TextField()
    resultsJson.blank = True 
    resultsJson.help_text = ("The following fields are used:\n" +
                             "alteredPaths: A list of pairs of paths with the original " +
                             "path and a modified output path to make it unique\n" +
                             "code: A code which indicates the result if there was a failure. Codes include: " +
                             "  ECONNREFUSED - failed to connect to remote system via SSH\n" +
                             "  ECMDFAIL - the shell command running the task returned a non-zero result\n" +
                             "  ETIMEOUT - the command took longer than allowed and was aborted\n" +
                             "  ENOENT - the command failed because an input file or folder does not exist\n" +
                             "  EEXISTS - the command failed because an output file or folder already exists\n" +
                             "  EREMOTEIO - the command failed because a remote stream failed\n" +
                             "  EOTHER - other unspecified type of exception\n" +
                             "description: A human readable description of the error\n" +
                             "osPath: The path to the file (present if the problem was due to a specific file)\n")
    progressJson = models.TextField()
    progressJson.help_text = ("A JSON dict with the following fields \n" +
        " - progress - float with the percentage completion or -1 if job cannot calculate progress\n" +
        " - outputSize - long integer with the size of the output or -1 if unknown\n" +
        " - runtime - integer with the amount of time in seconds that the job has been running\n")
    
    community = models.CharField(max_length=255)
    community.help_text = "This is the name_key of the community the user was set to when he configured this task"
    
    state = models.CharField(max_length=255)
    # See the task state machine at
    #    https://rpsmarf.atlassian.net/wiki/display/RPS/SMARF+Control+Server+%28SCS%29+Notes
    state.default = "init"
    state.help_text = "States are init, queued, booting, starting, prep, running, cleanup and finished"
    completion = models.CharField(max_length=255)
    completion.default = ""
    completion.help_text = "One of: notCompleted, completedWithoutError, completedWithError"
    completionReason = models.CharField(max_length=255)
    completionReason.default = ""
    completionReason.help_text = ("One of ok, timeout, cancelled, etc... This allows a user " + 
        "interface to react specially to a particular failure")
    completionDescription = models.TextField()
    completionDescription.default = ""
    completionDescription.help_text = ("This is a human-readable description of the reason " +
        "for the completion reason with suggestions on how to resolve errors.")
    stdout = models.TextField()
    stdout.help_text = "This is the last N bytes of the standard output generated by the tool while running"
    stderr = models.TextField()
    stderr.help_text = "This is the last N bytes of the standard error generated by the tool while running"
    uiVisible = models.BooleanField()
    uiVisible.default = True
    uiVisible.help_text = "True to show in the UI.  False not to show in the UI - these are internally generated tasks"
    note = models.TextField()
    note.help_text = "This is the note associated with this task"
    cloud_server = models.ForeignKey(SmModelCloudServer, null=True, blank=True)
    cloud_server.help_text = "This is the link to the cloud_server on which this" +\
        "task has already or will execute"
    
    class Meta:
        db_table = "scs_task"

    def __str__(self):
        name = str(self.pk) + " - " + self.name
        return (self.__class__.__name__ + ": " + str(name))
        

TASK_TYPE_CONFIGURATION_JSON_SCHEMA = {
                          "$schema": "http://json-schema.org/draft-04/schema#",
                          "title": "Task Type parametersJson Schema",
                          "type": "object",
                          "additionalProperties": False,
                          "properties": 
                          {
                              "args": {
                                    "title": "Arguments for resource as stream",
                                    "type": "array",
                                    "items": {
                                              "title": "Argument used when running a cmd",
                                              "type": "object",
                                              "additionalProperties": False,
                                              "properties": 
                                              {
                                                  "type": {"type": "string"},
                                                  "value": {},
                                                  "modifier": {"type": "string"},
                                                  "pathType": {"type": "string"},
                                                  "direction": {"type": "string"},
                                                  "role": {"type": "string"},
                                                  "name": {"type": "string"},
                                                  "input_id": {"type": "number"},
                                                  "description": {"type": "string"},
                                              },
                                              "required": ["type", "name"]
                                              }
                                    },
                              "outputProgress": 
                              { 
                                  "title": "Output progress settings",
                                  "type": "object",
                                  "properties":
                                  {
                                      "outputArg": {"type": "number"},
                                      "maxSize": {"type": "number"},
                                      "stdoutLines": {"type": "number"},
                                  },
                              },
                              "workingDir": 
                              { 
                                  "title": "Directory to execute the tool in",
                                  "type": "object",
                                  "properties":
                                  {
                                      "type": {"type": "string"},
                                      "input_id": {"type": "number"},
                                      "role": {"type": "string"},
                                      "modifier": {"type": "string"},
                                  },
                              },
                              "computeResourceRole": {"type": "string"},
                              "skipRunPermCheck": {"type": "boolean"},
                              "preExistingOutputFolderOk": {"type": "boolean"},
                              "interactive": {"type": "boolean"},
                              "versioning": {"type": "string"},
                              "executePermOnDataOk": {"type": "boolean"},
                          },
                          "required": ["args"]
                                       }
                                       
                                       
class SmModelTaskType(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    name = models.CharField(max_length=255, unique=True, validators=[smValidateName])
    name.help_text = "This is the human-readable name of the object"
    name_key = models.CharField(max_length=255, unique=True, validators=[smValidateNameKey])
    name_key.help_text = "This is the programmer-friendly name of this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    locked = models.BooleanField(default=False)
    locked.help_text = ("If this is true then this task_type is not usable by anyone except the task_type owner." +
        " It has been disabled by the owner.")
    description = models.TextField()
    description.help_text = "This the human-readable description of this object"
    code_module = models.CharField(max_length=255)
    code_module.help_text = "This is the name of the Python module in the SMARF Remote Agent " \
        "in which the class described by the code_classname field is present"
    code_classname = models.CharField(max_length=255)
    code_classname.help_text = "This is the name of the class which will be invoked to run " \
        "tasks of this type"
    configurationJson = models.TextField()
    configurationJson.help_text = ("A JSON dict with the following fields (for example): \n" +
                                   ' "args": [ ' +
                                   '{ "type": "string", "value" : "cp" },' + 
                                   '{ "type": "string", "input_id" : 1, "name": "flags", ' +
                                   '"description": "Flags to add to the cp command" },' +
                                   '{ "type": "resource", "pathType": "file", "input_id" : 2, ' +
                                   '"direction": "input", "role" : "src_resource" },' +
                                   '{ "type": "resource", "pathType": "folder", "input_id" : 3, ' +
                                   '"direction": "output", "role" : "dest_resource" }' +
                                   '],\n' +
                                   '"outputProgress" : {\n' +  
                                   '   "outputArg": 3, \n' + # Optional - arg to use for output progress
                                   '   "maxSize" : 1234, #optional\n' +
                                   '   "stdoutLines: 27 #optional\n' + 
                                   '}\n' +
                                   '"computeResourceRole": "src_resource",\n' +
                                   # Optional - may be type string or resource or be omitted
                                   '"workingDir": { "type": "resource", "input_id": 2, "direction": "input", ' +
                                   '"role": "zip", "modifier": "parent"}\n' + 
                                   '"interactive":true,\n' +  # True or false for Guacamole access
                                   '}\n' + 
                                   ' Note that the input_id fields are resolved by substitutions from the parametersJson' +
                                   'field from a task object when the task is started\n' +
                                   ' Arg type values:\n' +
                                   '  string - argument is the string specified in the value attribute\n' +
                                   '  resource - argument is the path created from the from the other arguments\n' +
                                   '  script - argument is a temporary file into which the the script \n' +
                                   '    contained in the scs_script table with the name specified in the value \n' +
                                   '    attribute' +
                                   '"skipRunPermCheck": True, # If true then the user does not need "run" permission\n' +
                                   '    on a resource to run this task type.  This would be "True" for internal commands\n' +
                                   '    such as copy, deleterecursive, zip, unzip' +
                                   '"versioning": "git" - true if tool has git versioning. Other versioning\n' +
                                   '    types may be added later' +
                                   '"executePermOnDataOk": True - true if, for all input resources, read permission is not\n' +
                                   '    required, execute permisson on the data resource is sufficient.  This allows\n' +
                                   '    users to have permission to use data in a tool, but not to take a copy of the data\n' +
                                   '    for themselves.' 
                                   )
    uiVisible = models.BooleanField()
    uiVisible.default = True
    uiVisible.help_text = "True to show the task type in the UI.  False not to show in the UI " \
                          "- if true they are used only for internally generated tasks"
    imageUrl = models.CharField(max_length=255, default=scsGetEnvVar(SCS_ENV_VAR_DEFAULT_TASK_TYPE_AVATAR_URL))
    imageUrl.help_text = "This is the icon associated with this task type"
    forum_id = models.IntegerField(null=True, blank=True)
    
    class Meta:
        db_table = "scs_task_type"
        permissions = (
                       ('execute_task_type', 'Run this type of task'),
                      )


class SmModelTaskResource(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    task = models.ForeignKey('SmModelTask')
    task.help_text = "This is the task associated with this task_resource object"
    resource = models.ForeignKey('SmModelResource')
    resource.help_text = "This is the resource associated with this task_resource object"
    resource_type_task_type = models.ForeignKey('SmModelResourceTypeTaskType')
    resource_type_task_type.help_text = "This is the rsource_type_task_type associated with this task_resource object"
    parametersJson = models.TextField()
    parametersJson.blank = True
    parametersJson.help_text = "These are parameters set when this task_resource is used for a streaming input or output"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    
    class Meta:
        db_table = "scs_task_resource"


class SmModelTaskTypeNote(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    task_type = models.ForeignKey('SmModelTaskType')
    task_type.help_text = "This is the task type to which this note applies"
    note = models.TextField()
    note.help_text = "This is the note associated with the specified task type"
    
    class Meta:
        db_table = "scs_task_type_note"


class SmModelResourceNote(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    resource = models.ForeignKey('SmModelResource')
    resource.help_text = "This is the resource to which this note applies"
    note = models.TextField()
    note.help_text = "This is the note associated with the specified resource"
    
    class Meta:
        db_table = "scs_resource_note"


class SmModelCommunity(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User, related_name='owner')
    owner.help_text = "This is the formal leader of this community (e.g. the decision maker)"
    name = models.CharField(max_length=255, unique=True, validators=[smValidateName])
    name.help_text = "This is the human-readable name of the community"
    name_key = models.CharField(max_length=255, unique=True, validators=[smValidateNameKey])
    name_key.help_text = "This is the programmer-friendly name of this community"
    description = models.TextField()
    description.help_text = "This the human-readable description of this community"
    shortDescription = models.CharField(max_length=80)
    shortDescription.help_text = "This is the short description for summary views in the UI"
    imageUrl = models.CharField(max_length=255, default=scsGetEnvVar(SCS_ENV_VAR_DEFAULT_COMMUNITY_AVATAR_URL))
    imageUrl.help_text = "This is the icon associated with this community"
    tag = models.ForeignKey(SmModelTag, related_name='tag', null=True, blank=True)
    tag.help = "This is the tag object that resources are associated with to " + \
        "mark them as part of this community.  The 'Everything' community " + \
        "has an empty tag field."

    class Meta:
        db_table = "scs_community"


class SmModelNewsItems(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    release_date_time = models.DateTimeField(default=sm_date_now)
    release_date_time.help_text = "This is the release date and time of the news item"
    expiry_date_time = models.DateTimeField(default=sm_date_far_future)
    expiry_date_time.help_text = "This is the expiry time for this news item. " +\
        "This is especially useful for system alerts (e.g. the system will be down tomorrow)." 
    headline = models.CharField(max_length=255, unique=True)
    headline.help_text = "This is the headline for the new item.  It is 50 characters or less."
    external_link = models.TextField()
    external_link.help_text = "This is the URL of another page which contains further details. " \
        "This may be empty if there is no external link."
    body = models.TextField()
    body.help_text = "This the text associated with the new item."
    community = models.ForeignKey(SmModelCommunity)
    community.help_text = "This is the community associated with this news item"
    alert = models.BooleanField(default=False)
    alert.help_text = "If this is True this it will be shown as an alert " +\
        "in the top of the screen"

    class Meta:
        db_table = "scs_news_item"


def scsModelGetEverythingCommunity():
    return SmModelCommunity.objects.get(name_key=SM_COMMUNITY_ALL_NAME_KEY)


class SmModelUserSetting(SmarfModel):
    owner = models.OneToOneField(User, primary_key=True)
    owner.help_text = "The user record associated with these settings"
    owner.full = True
    title = models.CharField(max_length=32, blank=True)
    title.help_text = "This is the user's title (e.g. Professor, Ms., etc...)."
    imageUrl = models.CharField(max_length=255, default=scsGetEnvVar(SCS_ENV_VAR_DEFAULT_USER_AVATAR_URL))
    imageUrl.help_text = "This is the user's avatar."
    community = models.ForeignKey(SmModelCommunity,
                                  on_delete=models.SET(scsModelGetEverythingCommunity))
    community.help_text = "This is the currently assigned community for this user"
    community.full = True
    performSetupOnNextLogin = models.BooleanField(default=True)
    performSetupOnNextLogin.help_text = "If this is True then on the next login " +\
        "the user is prompted to select a default community and other initial " +\
        "setup settings"
    info = models.TextField(blank=True)
    info.help_text = "This is additional free-form information about the user."
    guestAccount = models.BooleanField(default=False)
    guestAccount.help_text = "If this is True then the account is a guest account " +\
        "and some features are disabled."
    uiSettingsJson = models.TextField(default="{}")
    uiSettingsJson.help_text = "This is a JSON-formatted field which is used by the " +\
        "UI to hold various UI-defined settings for this user (e.g. hideTheGettingStartedPanel)"

    class Meta:
        db_table = "scs_user_setting"


class SmModelScript(SmarfModel):
    '''
    This table contains scripts which are referenced by the task_type
    configurationJson field
    '''
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    key = models.CharField(max_length=255, blank=False)
    key.help_text = "This is the name of the script"
    value = models.CharField(max_length=10240)
    value.help_text = "This is the text of the script."
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"

    class Meta:
        db_table = "scs_script"


class SmModelProperty(SmarfModel):
    '''
    These properties define various externally queriable settings. 
    '''
    key = models.CharField(max_length=255, primary_key=True, blank=False)
    key.help_text = "This is the name of the property"
    value = models.CharField(max_length=255)
    value.help_text = "This is the value of the property."

    class Meta:
        db_table = "scs_property"


class SmModelReservation(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    resource = models.ForeignKey(SmModelResource)
    resource.help_text = "This is the resource which is reserved by this object"
    start_time = models.DateTimeField()
    start_time.help_text = "This is the start time of the reservation"
    end_time = models.DateTimeField()
    end_time.help_text = "This is the start time of the reservation"
    description = models.TextField()
    description.help_text = "This is some free-form information about this " +\
                            "reservation which is publicly visible."
    
    class Meta:
        db_table = "scs_reservation"


class SmModelFaq(SmarfModel):
    '''
    This table contains FAQ questions and answers for any objects which 
    have them. 
    '''
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    target = models.CharField(max_length=255, blank=False)
    target.help_text = "This is the object to which the question and answer apply. "\
        "It is in the format /scs/<object type>/ for entries which apply to "\
        "an object type or /scs/<object type>/<object id>/ for entries which apply "\
        "to a specific object.  For example /scs/resource/8/"
    question = models.CharField(max_length=255, blank=False)
    question.help_text = "This is the question for the FAQ list"
    answer = models.TextField()
    answer.help_text = "This is the answer to the question."

    class Meta:
        db_table = "scs_faq"


class SmModelRequest(SmarfModel):
    '''
    This table request for access from users to resource/tool owners. 
    '''
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User, related_name='+')
    owner.help_text = "This is the id of the user who owns this object"
    requester = models.ForeignKey(User, related_name='+')
    requester.help_text = "This is the id of the user who is requesting access this object"
    target = models.CharField(max_length=255, blank=False)
    target.help_text = "This is the object to which the request for access applies. "\
        "For example /scs/resource/8/"
    reason = models.CharField(max_length=255, blank=False)
    reason.help_text = "This is a string describing the reason for the request"
    access_requested = models.CharField(max_length=8, blank=False)
    access_requested.help_text = "This is the access requested with one or more of 'r', 'w' and 'x'."

    class Meta:
        db_table = "scs_request"


class SmForumSession(SmarfModel):
    '''
    This table is used by the phpBB software. 
    '''
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='+')
    session_key = models.CharField(max_length=255, blank=False)
    ip = models.CharField(max_length=255, blank=False)

    class Meta:
        db_table = "scs_forum_sessions"
    

class SmModelSchema(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    name = models.CharField(max_length=255, unique=True, validators=[smValidateName])
    name.help_text = "This is the human-readable name of the object"
    name_key = models.CharField(max_length=255, unique=True, validators=[smValidateNameKey])
    name_key.help_text = "This is the programmer-friendly name of this object"
    namespace = models.URLField()
    namespace.help_text = "This is the programmer-friendly name of this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    community = models.ForeignKey(SmModelCommunity)
    community.help_text = "This is the list of resources that this metadata " +\
                          "was built from (and refer to)"
    community = models.ForeignKey(SmModelCommunity)
    description = models.TextField()
    description.help_text = "This is some free-form information about this " +\
                            "metadata repository which is publicly visible."
    resource_type = models.ForeignKey('SmModelResourceType') 
    resource_type.help_text = "This is the resource type object associated with this resource. "\
        "This describes the type of data (or capabilities) of this resource and defines which "\
        "task types can use this type of resource."
                            
    class Meta:
        db_table = "scs_md_schema"

    
class SmModelMdRepo(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    name = models.CharField(max_length=255, unique=True, validators=[smValidateName])
    name.help_text = "This is the human-readable name of the object"
    name_key = models.CharField(max_length=255, unique=True, validators=[smValidateNameKey])
    name_key.help_text = "This is the programmer-friendly name of this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    description = models.TextField()
    description.help_text = "This is some free-form information about this " +\
                            "metadata repository which is publicly visible."
    resources = models.ManyToManyField(SmModelResource)
    resources.help_text = "This is the list of resources that this metadata " +\
                          "was built from (and refer to)"
    schema = models.ForeignKey(SmModelSchema)
    schema.help_text = "This is the id of the schema who owns this object"
                            
    class Meta:
        db_table = "scs_md_repo"
    
    
class SmModelPropertyType(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    name = models.CharField(max_length=255, validators=[smValidateName])
    name.help_text = "This is the human-readable name of the object"
    name_key = models.CharField(max_length=255, validators=[smValidateNameKey])
    name_key.help_text = "This is the programmer-friendly name of this object"
    # SmModelPropertyType related properties
    BOOL = 'BOOL'
    FLOAT = 'FLOAT'
    INT = 'INT'
    REF = 'REF'
    STRING = 'STRING'
    DATE = 'DATETM'
    VALUE_TYPES = {
        BOOL: ('Boolean', XSD.boolean, 'xsd:boolean'),
        FLOAT: ('Number', XSD.float, 'xsd:float'),
        INT: ('Integer', XSD.integer, 'xsd:integer'),
        REF: ('Reference', None, None),
        STRING: ('String', XSD.string, 'xsd:string'),
        DATE: ('DateTime', XSD.dateTime, 'xsd:dateTime'),
    }
    property_type = models.CharField(max_length=6, choices=[(k, v[0]) for k, v in VALUE_TYPES.items()])
    property_type.help_text = "Values supported:\n" +\
        "- BOOL - for property types which contain boolean data\n" +\
        "- FLOAT - for property types which contain numbers\\n" +\
        "- INT - for property types which contain integers\\n" +\
        "- REF - for property types which contain references\\n" +\
        "- STRING - for property types which contain strings\\n" +\
        "- DateTime - for property types which contain a datetime\\n"
    schema = models.ForeignKey(SmModelSchema)
    schema.help_text = "This is the id of the schema who owns this object"
                            
    class Meta:
        db_table = "scs_md_property_type"
        
    @property
    def qname(self):
        return '%s:%s' % (self.schema.name_key, self.name_key)

    @property
    def fqdn(self):
        return '%s%s' % (self.schema.namespace, self.name_key)
    
    @property
    def datatype(self):
        "Returns the XSD datatype for the property type.  Assumes argument to exist."
        return SmModelPropertyType.VALUE_TYPES[self.property_type][1]

    @property
    def datatype_str(self):
        "Returns the XSD datatype string for the property type.  Assumes argument to exist."
        return SmModelPropertyType.VALUE_TYPES[self.property_type][2]
        

# Derived from Model directly to allow bulk insert
class SmModelPathList(models.Model):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    relative_path = models.CharField(max_length=255, validators=[smValidateName])
    relative_path.help_text = "This is the suffix path of the object"
    repo = models.ForeignKey(SmModelMdRepo)
    repo.help_text = "This is the id of the MD Repo who owns this object"
    resource = models.ForeignKey(SmModelResource)
    resource.help_text = "This is the id of the resource who owns this object"
    state = models.CharField(max_length=16, default="created")
    state.help_text = "State of record\n" +\
        "- created - record was created but no data has been loaded" +\
        "- loaded - metadata loaded for this file"
                
    class Meta:
        db_table = "scs_md_path_list"
        
        
class SmModelMdProperty(Model):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    path = models.ForeignKey(SmModelPathList, related_name="properties")
    path.help_text = "This is the id of the path record which describes the file associated with this property"
    property_type = models.ForeignKey(SmModelPropertyType)
    property_type.help_text = "This is the id of the property type record"
    value = models.CharField(max_length=80)
    value.help_text = "This is the value of the property"
    
    class Meta:
        db_table = "scs_md_property"
        
             
class SmModelMdExtractorType(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    name = models.CharField(max_length=255, unique=True, validators=[smValidateName])
    name.help_text = "This is the human-readable name of the object"
    name_key = models.CharField(max_length=255, unique=True, validators=[smValidateNameKey])
    name_key.help_text = "This is the programmer-friendly name of this object"
    description = models.TextField()
    description.help_text = "This is some free-form information about this " +\
                            "type of extractor which is publicly visible."
    schema = models.ForeignKey(SmModelSchema)
    schema.help_text = "This is the id of the schema who owns this object"
    task_type = models.ForeignKey(SmModelTaskType)
    task_type.help_text = "This is the link to the task_type to extract metadata"
    resource_type = models.ForeignKey(SmModelResourceType)
    resource_type.help_text = "This is the link to the resource_type"\
        " of the compute resource which can run the extraction"
    
    class Meta:
        db_table = "scs_md_extractor_type"
        

MDEXTRACTOR_STATE_CHOICES = (("init", "init"),
                             ("loading", "loading"),
                             ("loaded", "loaded"),
                             ("updating", "updating"),
                             ("failed", "failed"),
                             )

        
class SmModelMdExtractor(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    repo = models.ForeignKey(SmModelMdRepo)
    repo.help_text = "This is the link to the "\
        " metadata repository which is associated with the extractor"
    extractor_type = models.ForeignKey(SmModelMdExtractorType)
    extractor_type.help_text = "This is the link to the extractor_type to extract metadata"
    compute_resource = models.ForeignKey(SmModelResource)
    compute_resource.help_text = "This is the link to the "\
        " compute resource which will run an extraction"
    state = models.CharField(max_length=255, choices=MDEXTRACTOR_STATE_CHOICES, default="init")
    state.help_text = "Values supported:\n" +\
        "- init - no metadata loaded yet\n" +\
        "- purging - removing records from the previous extraction\n" +\
        "- scanning - scanning resources to build list of files to extract metadata from\n" +\
        "- extracting - extracting metadata from files\n" +\
        "- done - metadata loaded\n" +\
        "- updating - metadata updating (incremental loading)\n"
    progressJson = models.TextField(default="{}")
    progressJson.help_text = ("A JSON dict with the following fields \n" +
        " - files_processed - number of files processed so far\n" +
        " - files_to_process - nce in 'extracting' state, this tag is the number of files "
        "from which metadata will be extracted\n" +
        " - duration - running time at last extraction complete in seconds\n" +
        " - files_with_no_metadata_at_done - once in finished state, this is the number of "
        " files for which no metadata was extracted."
                              )
    emailWhenDone = models.BooleanField(default=False)
    emailWhenDone.help_text = "If this is true then an email will be sent when the " +\
        "extraction process is complete"
    last_extract_start = models.DateTimeField(null=True)
    last_extract_start.help_text = "This is the date and time of the start of the last extraction"
    last_extract_end = models.DateTimeField(null=True)
    last_extract_end.help_text = "This is the date and time of the end of the last extraction"
    last_extract_result = models.CharField(max_length=64)
    last_extract_result.help_text = "This is one of 'ok', 'failed', 'running'"
    last_extract_errors = models.TextField()
    last_extract_errors.help_text = "Contains a list of errors associated with the last extraction operation"
    stdout = models.TextField()
    stdout.help_text = "This is the last N bytes of the standard output generated by the extraction"
    stderr = models.TextField()
    stderr.help_text = "This is the last N bytes of the standard error generated by the extraction"

    class Meta:
        db_table = "scs_md_extractor"
        

# Models for file sets (e.g. search results)
class SmModelFileSet(SmarfModel):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    name = models.CharField(max_length=255, validators=[smValidateName])
    name.help_text = "This is the human-readable name of the object"
    name_key = models.CharField(max_length=255, validators=[smValidateNameKey])
    name_key.help_text = "This is the programmer-friendly name of this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    create_date_time = models.DateTimeField(default=sm_date_now, null=True)
    create_date_time.help_text = "This is the date and time of the creation of the file_set"
    source = models.CharField(max_length=64)
    source.help_text = "This is one of 'search', but other values may be supported later"
    source_info = models.TextField()
    source_info.help_text = "Contains source-specific information " +\
        "(e.g the parameters to the search if source=search)"
    auto_delete = models.BooleanField(default=True)
    auto_delete.help_text = "If this is true then the next search operation " +\
        "will delete this file_set.  This is set to False to save the search results"

    class Meta:
        db_table = "scs_file_set"
        

class SmModelFileSetItem(Model):
    id = models.AutoField(primary_key=True)
    id.help_text = "The auto-generated id for this object"
    owner = models.ForeignKey(User)
    owner.help_text = "This is the id of the user who owns this object"
    resource = models.ForeignKey(SmModelResource)
    resource.help_text = "The resource of the file represented by this object"
    file_set = models.ForeignKey(SmModelFileSet)
    file_set.help_text = "The file set that this item is associated with"
    relative_path = models.CharField(max_length=255, validators=[smValidateName])
    relative_path.help_text = "This is the suffix path of the file within the resource"

    class Meta:
        db_table = "scs_file_set_item"
        
             
# Register model with activity stream to generate usage event records
registry.register(User)
registry.register(SmModelResource)
registry.register(SmModelTaskType)
