'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import unittest
import logging
from smcommon.logging.sm_logger import smLoggingSetupForTest

logger = logging.getLogger(__name__)


class LoggingTest(unittest.TestCase):

    def testLogging(self):
        smLoggingSetupForTest()
        
        logger.debug("Hello at debug level - expected")
        logger.info("Hello at info level - expected")

        rootLogger = logging.getLogger("")
        rootLogger.setLevel(logging.INFO)
        logger.debug("Hello at debug leve - UNEXPECTED")
        logger.info("Hello at info level - expected")

        loggern1 = logging.getLogger("n1")
        loggern1.setLevel(logging.INFO)
        loggern1n2 = logging.getLogger("n1.n2")
        loggern1n2.debug("Hello at debug leve - UNEXPECTED")
        loggern1n2.info("Hello at info level - expected")
        loggern1n2.setLevel(logging.DEBUG)
        loggern1n2.debug("Hello at debug level - expected")
        loggern1n2.info("Hello at info level - expected")
        loggern1.setLevel(logging.INFO)
        loggern1n2.debug("Hello at debug level - UNEXPECTED")
        loggern1n2.info("Hello at info level - expected")
        self.assertTrue(loggern1n2.propagate)
        self.assertEqual(loggern1n2.parent, loggern1)
        
if __name__ == "__main__":
    #import syssys.argv = ['', 'Test.testName']
    unittest.main()