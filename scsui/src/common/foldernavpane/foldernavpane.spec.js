/**
 * Tests sit right alongside the file they are testing, which is more intuitive
 * and portable than separating `src` and `test` directories. Additionally, the
 * build process will exclude all `.spec.js` files from the build
 * automatically.
 */
describe( 'Folder and File  Navigation', function() {
	beforeEach( module( 'rpsmarf.foldernavpane') );
	beforeEach( module( 'ui.router') );
	beforeEach( module( 'rpsmarf.config') );
	beforeEach( module( 'restangular') );

	beforeEach(module('foldernavpane/foldernavpane.tpl.html'));
	var scope, element, entries,timeout,homeSubEntries,documentsSubEntries,dataSubEntries;
	var resource;

		
	describe('Display folder/file entries', function(){

		
		beforeEach(inject(function($rootScope, $compile, $timeout, Restangular, SMARF_URL){
			Restangular.configuration.baseUrl=SMARF_URL;
			Restangular.configuration.encodeIds = false;
			var restresource = Restangular.one('/scs/resource','1/');
			homeSubEntries = [{basename:'documents/', path:'/home/documents/', isDir:true},{basename:'pictures/', path:'/home/pictures/',isDir:true}, {basename:'myfile', path:'/home/documents/myfile',isDir:false}];
			documentsSubEntries = [{basename:'report1.doc', path:'/home/documents/report1.doc',isDir:false},{basename:'paper.pdf', path:'/home/documents/paper.pdf',isDir:false}];
			dataSubEntries = [{basename:'bridge.dat', path:'/home/data/bridge.dat',isDir:false},{basename:'machinery.dat', path:'/home/data/machinery.dat',isDir:false}, {basename:'data.dat', path:'/home/data/data.dat',isDir:false}];

			entries = [{basename:'home/', path:'/home/',isDir:true},{basename:'documents/', path:'/home/documents/',isDir:true},{basename:'data/', path:'/data/',isDir:true},{basename:'file.txt', path:'/file.txt',isDir:false}];
			scope = $rootScope.$new();
			element = angular.element('<foldernavpane entries="resource.entries" download="download(entry)" ls="entryLs($event, entry, resource)" levelup="levelUp(resource)" smondrop="drop(srcEntries, dstEntries, entry)" refresh="refresh(resource)"></foldernavpane>');
			
			
			timeout = $timeout;
			//scope.entries = entries;
			//scope.levels = [entries];
			resource = {id:1, entries: entries, levels:[],path:'/'};
			scope.resource = resource;
			scope.entryLs = function($event, entry, resource){
				//console.log(entry);
				switch(entry.path){
					case '/home/':
						resource.entries = homeSubEntries;
						resource.levels.push(homeSubEntries);
						break;
					case '/home/documents/':
						resource.entries = documentsSubEntries;
						resource.levels.push(documentsSubEntries);
						break;
					case '/data/':
						resource.entries = dataSubEntries;
						resource.levels.push(dataSubEntries);
						break;
					case '/home/pictures/':
						resource.entries = [];
						resource.levels.push([]);
						break;
					default:
						resource.entries = [];
						return;
				}
				
				scope.$digest();
				
			};

			scope.refresh = function(resource){
				restresource.get().then(function(entries){
					resource.entries = entries;
					scope.$digest();
				});
				

			};
			scope.levelUp = function(resource){
				resource.levels.pop();

				resource.entries = resource.levels[resource.levels.length-1];
				scope.$digest();
			};

			scope.download = function(entry){
				var resource = this.resource;
				window.open(SMARF_URL+'/scs/resource/'+resource.id+'/'+entry.name+'/download/');
			};

			element = $compile(element)(scope);

			scope.$digest();

			
			//scope = element.isolateScope();

			
			


		}));

		it( 'should return the correct type and amount of entries for the selected entry', function() {
			
			var isolated = element.isolateScope();
			expect(scope.resource.entries.length).toEqual(entries.length);
			isolated.ls({entry:scope.resource.entries[0], resource: scope.resource});
			expect(scope.resource.entries.length).toEqual(homeSubEntries.length);
			expect(homeSubEntries).toEqual(scope.resource.entries);

			scope.resource.entries = entries;
			isolated.ls({entry:scope.resource.entries[2], resource:scope.resource});
			expect(scope.resource.entries.length).toEqual(dataSubEntries.length);
			expect(dataSubEntries).toEqual(scope.resource.entries);
			scope.resource.entries = entries;
			expect(isolated.entryType(scope.resource.entries[3])).toEqual(["fa"," fa-file-text", "entry-icon-file"]);
			expect(isolated.entryType(scope.resource.entries[0])).toEqual(["fa"," fa-folder-open","entry-icon-folder"]);



		});

		it('should display the correct number and types of elements within the navpane', function(){

			var isolated = element.isolateScope();
			expect(element.find('div.entry-container').length).toEqual(scope.resource.entries.length);

			expect(element.find('div.entry-container:eq(0)').parent().attr('id')).toEqual(scope.resource.entries[0].$$hashKey);
			//dblclick home
			isolated.ls({$event: null, entry:scope.resource.entries[0], resource:scope.resource});

			//console.log(isolated.entries);
			//give the DOM time to update. Ugly :(
			//console.log(element);
			expect(element.find('div.entry-container').length).toEqual(scope.resource.entries.length);
			//dblclick pictures
			isolated.ls({entry:scope.resource.entries[1], resource:scope.resource});
			expect(element.find('div.entry-container').length).toEqual(0);
			//expect(element.find('div.panel-body').html().trim()).toEqual('empty folder');

			//Go back to home level
			isolated.levelup(scope.resource);
			expect(scope.resource.entries).toEqual(homeSubEntries);
			expect(element.find('div.entry-container').length).toEqual(scope.resource.entries.length);

			expect(element.find('.entry-icon-file').parent().find('i.fa-cloud-download').length).toEqual(1);

			// console.log(element);

			//timeout.flush();
			

		});

		it('should refresh the entries', inject(function($httpBackend, SMARF_URL){
			var oldEntries = entries;
			$httpBackend.expect('GET', SMARF_URL+'/scs/resource/1/').respond([{basename:'home/', path:'/home/',isDir:true},{basename:'documents/', path:'/home/documents/',isDir:true},{basename:'data/', path:'/data/',isDir:true},{basename:'file.txt', path:'/file.txt',isDir:false},{basename:'documents2/', path:'/home/documents2/', isDir:true}]);
			scope.refresh(resource);
			oldEntries.push({basename:'documents2/', path:'/home/documents2/', isDir:true});
			
			expect(scope.resource.entries).toEqual(oldEntries);

		}));

		it('should allow files to be downloaded', function(){
			var resource = {};
			scope.resource = resource;
			resource.entries = entries;
			spyOn(window,'open');
			scope.download(resource.entries[3]);
			expect(window.open).toHaveBeenCalled();

		});



		

		

	});
  
});

