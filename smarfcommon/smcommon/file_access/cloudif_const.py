'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 23, 2015

    @author: rpsmarf
'''

import logging

logger = logging.getLogger(__name__)


SCS_CLOUDIF_TYPE = "type"
SCS_CLOUDIF_TYPE_AWS = "aws"
SCS_CLOUDIF_TYPE_OPENSTACK = "openstack"

SCS_CLOUDIF_DYNAMIC_SERVER = "dynamic"
 
SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID = "key_id"
SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY = "secret_access_key"

SCS_CLOUDIF_AWS_PARAM_REGION_NAME = "region_name"
SCS_CLOUDIF_AWS_PARAM_IMAGE_ID = "imageId"
SCS_CLOUDIF_AWS_PARAM_KEYPAIR = "keypair"
SCS_CLOUDIF_AWS_PARAM_INSTANCE_TYPE = "instance_type"

SCS_CLOUDIF_SWIFT_ACC_USERNAME = "username"
SCS_CLOUDIF_SWIFT_ACC_PASSWORD = "password"
SCS_CLOUDIF_SWIFT_ACC_TENANT = "tenant"
SCS_CLOUDIF_SWIFT_ACC_TOKEN_URL = "tokenUrl"

SCS_CLOUDIF_ID = "id"
SCS_CLOUDIF_HOST = "host"
SCS_CLOUDIF_STATE = "state"
SCS_CLOUDIF_STATE_INIT = "init"
SCS_CLOUDIF_STATE_USABLE = "usable"
SCS_CLOUDIF_STATE_INUSE = "inuse"
SCS_CLOUDIF_STATE_RUNNING = "running"
SCS_CLOUDIF_STATE_BOOTING = "booting"
SCS_CLOUDIF_STATE_STOPPING = "stopping"
SCS_CLOUDIF_STATE_STOPPED = "stopped"
SCS_CLOUDIF_STATE_ERROR = "error"
