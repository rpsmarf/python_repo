'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jun 29, 2015

    @author: rpsmarf
'''

import logging
from sm_controller.models import SmModelAgent, SmModelResource, SmModelTaskType,\
    SmModelTask
from django.contrib.auth.models import User
from smarfcontrolserver.init.envvar import SCS_ENV_VAR_METRIC_MINUTE,\
    scsGetEnvVarInt, SCS_ENV_VAR_METRIC_DAY, SCS_ENV_VAR_METRIC_LONG_TASK_SECS,\
    SCS_ENV_VAR_METRIC_HOUR
from smcommon.metrics.metrics_sender import METRIC_TYPE_GUAGE
from smarfcontrolserver.init.metric_names import SCS_NUM_AGENTS_DOWN,\
    SCS_NUM_RESOURCES_DOWN, SCS_NUM_AGENTS, SCS_NUM_RESOURCES,\
    SCS_NUM_TASK_TYPES, SCS_NUM_USERS_IN_DB, SCS_NUM_COMMUNITIES,\
    SCS_NUM_TASKS_RUNNING, SCS_NUM_TASKS_QUEUED, SCS_NUM_TASKS_LONG_RUNNING
import datetime
from datetime import timedelta

logger = logging.getLogger(__name__)


LONG_RUNNING_SECS = scsGetEnvVarInt(SCS_ENV_VAR_METRIC_LONG_TASK_SECS)


def scsGetNumDownRemoteAgents():
    return SmModelAgent.objects.filter(status="down").count()


def scsGetNumRemoteAgents():
    return SmModelAgent.objects.all().count()


def scsGetNumDownResources():
    return SmModelResource.objects.filter(status="down").count()


def scsGetNumResources():
    return SmModelResource.objects.all().count()


def scsGetNumTaskType():
    return SmModelTaskType.objects.all().count()


def scsGetNumUsers():
    return User.objects.all().count()


def scsGetNumCommunity():
    return User.objects.all().count()


def scsGetNumTasksRunning():
    return SmModelTask.objects.exclude(state="init").\
        exclude(state="finished").exclude(state="queued").count()


def scsGetNumTasksLongRunning():
    return SmModelTask.objects.exclude(state="init").\
        exclude(state="finished").exclude(state="queued").\
        filter(start_time__gt=(datetime.date.today() + timedelta(seconds=LONG_RUNNING_SECS))).count()


def scsGetNumTasksQueued():
    return SmModelTask.objects.filter(state="queued").count()


def scsMetricCallbacksRegister(metricsSender):
    # Intervals may be overriden to speed up testing
    minuteInterval = scsGetEnvVarInt(SCS_ENV_VAR_METRIC_MINUTE)
    hourInterval = scsGetEnvVarInt(SCS_ENV_VAR_METRIC_HOUR)
    dayInterval = scsGetEnvVarInt(SCS_ENV_VAR_METRIC_DAY)
    metricsSender.registerCallback(SCS_NUM_AGENTS_DOWN, METRIC_TYPE_GUAGE, minuteInterval, scsGetNumDownRemoteAgents)
    metricsSender.registerCallback(SCS_NUM_RESOURCES_DOWN, METRIC_TYPE_GUAGE, minuteInterval, scsGetNumDownResources)
    metricsSender.registerCallback(SCS_NUM_TASKS_RUNNING, METRIC_TYPE_GUAGE, minuteInterval, scsGetNumTasksRunning)
    metricsSender.registerCallback(SCS_NUM_TASKS_QUEUED, METRIC_TYPE_GUAGE, minuteInterval, scsGetNumTasksQueued)
    
    metricsSender.registerCallback(SCS_NUM_TASKS_LONG_RUNNING, METRIC_TYPE_GUAGE, hourInterval, scsGetNumTasksLongRunning)

    # Get quanitites
    metricsSender.registerCallback(SCS_NUM_AGENTS, METRIC_TYPE_GUAGE, dayInterval, scsGetNumRemoteAgents)
    metricsSender.registerCallback(SCS_NUM_RESOURCES, METRIC_TYPE_GUAGE, dayInterval, scsGetNumResources)
    metricsSender.registerCallback(SCS_NUM_TASK_TYPES, METRIC_TYPE_GUAGE, dayInterval, scsGetNumTaskType)
    metricsSender.registerCallback(SCS_NUM_USERS_IN_DB, METRIC_TYPE_GUAGE, dayInterval, scsGetNumUsers)
    metricsSender.registerCallback(SCS_NUM_COMMUNITIES, METRIC_TYPE_GUAGE, dayInterval, scsGetNumCommunity)


