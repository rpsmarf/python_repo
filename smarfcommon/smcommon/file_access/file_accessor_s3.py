'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public Ls3nse as published by
    the Free Software Foundation, either version 3 of the Ls3nse, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public Ls3nse for more details.

    You should have received a copy of the GNU General Public Ls3nse
    along with RP-SMARF.  If not, see <http://www.gnu.org/ls3nses/>.
   
    Created on Sep 12, 2014

    @author: rpsmarf
'''
import logging
from smcommon.exceptions.file_access_exception import SmFileAccessException,\
    SM_FS_EXC_NOT_CONN
from smcommon.file_access.aws_s3_accessor import AwsS3Accessor
from io import BytesIO
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID,\
    SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY

logger = logging.getLogger(__name__)

# This is the minimum chunk size for chunked uploads
S3_MIN_CHUNKSIZE = 5200000


class SmFileWriterWrapperS3(object):
    '''
    This is an object which is returned when a write to an S3 bucket is 
    started.  It processes write operations by buffering data until there
    is enough for an s3 chunk (currently > 5 MB) or a close happens 
    '''
    def __init__(self, s3If, path):
        self.s3If = s3If
        self.uploader = None
        self.part_num = 0
        self.buffer = bytearray()
        self.path = path
        
    def write(self, data):
        self.buffer.extend(data)
        if len(self.buffer) < S3_MIN_CHUNKSIZE:
            return
        
        if self.uploader is None:
            self.uploader = self.s3If.uploadMultipartStart(self.path)
                
        with BytesIO(self.buffer) as fp:
            self.s3If.uploadMultipartChunk(self.uploader, 
                                           self.part_num, 
                                           fp, len(self.buffer))
        self.part_num += 1
        self.buffer = bytearray()
        
    def close(self):
        if len(self.buffer) > 0:
            with BytesIO(self.buffer) as fp:
                if self.uploader is not None:
                    self.s3If.uploadMultipartChunk(self.uploader, self.part_num, 
                                                   fp, len(self.buffer))
                else:
                    self.s3If.uploadFile(fp, len(self.buffer), self.path)   
        if self.uploader is not None:
            self.s3If.uploadMultipartEnd(self.uploader)
        return 0


class SmFileReadWrapperS3(object):
    '''
    '''
    def __init__(self, data_iter):
        self.data_iter = data_iter

    def read(self, maxBytes):
        try:
            return next(self.data_iter)
        except StopIteration:
            return b''

    def close(self):
        return 0


class SmFileAccessorS3(object):
    '''
    This class implements the standard file access class for the Amazon
    S3 protocol.
    '''

    def __init__(self, dataContainer):
        '''
        Constructor
        '''
        self.dataContainer = dataContainer
        self.connected = False
        bucketName = self.dataContainer.getParam("bucket")
        logger.debug("s3 bucket is %s", bucketName)
        self.s3If = AwsS3Accessor(self.dataContainer.getParam(SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID),
                                  self.dataContainer.getParam(SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY),
                                  bucketName, 
                                  3)
        
    def connectSession(self):
        if self.connected:
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, "", 
                                        "File accessor is already connected")
        self.connected = True
        
    def disconnectSession(self):
        self.connected = False
        
    def deleteFileInfos(self, fileInfosToDelete):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, "", 
                                        "Delete of paths failed because connect operation was not done")
        
        try:
            for f in fileInfosToDelete:
                self.s3If.removeFileOrFolder(f["name"])
        except Exception as e: 
            raise SmFileAccessException(422, "EOTHER", "<multiple>", e.description)

    def deletePaths(self, pathsToDelete):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, "<multiple>", 
                                        "Delete of paths failed because connect operation was not done")
        
        try:
            for path in pathsToDelete:
                self.s3If.removeFileOrFolder(path)
        except Exception as e:
            raise SmFileAccessException(422, "EOTHER", e.path, e.description)
        
    def list(self, path, recursiveDepth, maxEntries, lastPathReturned, showHidden):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, path, "List failed because connect operation was not done")
        
        if path == "/":
            path = ""
            
        # If the path ends with // then we want the path of the folder as well as the children so 
        # in  open stack we take off the / at the end to get that
        if path.endswith("//"):
            path = path[:-2]
                
        slashesInPath = path.count("/")
        returnList = []
        entriesToGet = maxEntries
        try:
            while True:
                if maxEntries == 1:
                    return [self.s3If.getMetadata(path)]
                
                s3List = self.s3If.listKeysInsideBucket(path, (recursiveDepth > 1), 
                                                        entriesToGet, lastPathReturned)
                
                if len(s3List) == 1 and path == s3List[0]["name"]:
                    raise SmFileAccessException(422, "ENOTDIR", path, "Path is not a directory")
                
                if recursiveDepth == 1 and showHidden:
                    return s3List
                else:
                    fullListReturned = (len(s3List) >= entriesToGet)
                    # Operate on copy of string ([:]) to avoid problems of iterating 
                    # on changing list
                    discards = 0
                    for item in s3List:
                        name = item["name"]
                        if not showHidden and (name[0] == '.' or "/." in name):
                            discards += 1
                            continue
                        if recursiveDepth > 1:
                            if (name.count("/") - slashesInPath) > recursiveDepth:
                                discards += 1
                                continue
                        returnList.append(item)
                    if fullListReturned and discards > 0:
                        # Need to go back and get more because we threw away some entries and now we are 
                        # short some entries
                        entriesToGet = entriesToGet - len(returnList)
                        lastPathReturned = s3List[-1]["name"]
                    else:
                        return returnList                        
                
        except Exception as e:
            raise SmFileAccessException(422, "EOTHER", path, e.description)
        return returnList
    
    def mkdir(self, path, makeParentFolders=False):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, path, "Mkdir failed because connect operation was not done")

        try:
            self.s3If.createSubFolder(path)
        except Exception as e:
            raise SmFileAccessException(422, "EOTHER", path, e.description)
        
    def du(self, path, includeHidden):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, path, "du failed because connect operation was not done")

        try:
            size = 0
            if not path.endswith("/"):
                path += "/"
            if path == "/" or path == "./":
                path = ""
            s3_list_info = self.s3If.listKeysInsideBucket(path, True, 10000, None)
            count = 0
            for file in s3_list_info:
                name = file["name"]
                if not includeHidden and (name.startswith(".") or "/." in name):
                    continue
                size += file["size"]
                count += 1
                
            return {"sizeInBytes": size, "fileCount": count}
        except Exception as e:
            raise SmFileAccessException(422, "EOTHER", path, e.description)
        
    def move(self, oldpath, newpath):
        raise Exception("Move not supported")
    
    def open(self, path, access_mode):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, 
                                        "Open of path " + path + " failed because connect operation was not done")

        if "r" in access_mode:
            return SmFileReadWrapperS3(self.s3If.downloadFileViaIterator(path))
        else:
            return SmFileWriterWrapperS3(self.s3If, path)
    
