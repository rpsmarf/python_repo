'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Oct 27, 2014

    @author: rpsmarf
'''
import threading
import logging
import time
from smcommon.utils.file_utils import sm_safe_close

logger = logging.getLogger(__name__)

PATTERN_INCREMENTING = "incr"
PATTERN_FIXED_STRING = "fixed"
PATTERN_FILE_ONCE = "file_once"
PATTERN_FILE_REPEATING = "file_repeating"
PATTERN_NONE = "none"
THREAD_COUNT = 0


def voidCallbackTwoArgs(arg1, arg2):
    pass


class SmStreamTestSourceThread(threading.Thread):
    
    def __init__(self, streamTestSource):
        global THREAD_COUNT
        threading.Thread.__init__(self)
        self.name = "sourceThread-" + str(THREAD_COUNT)
        THREAD_COUNT += 1 
        self.streamTestSource = streamTestSource
        
    def run(self):
        self.streamTestSource._generateData()


class SmStreamTestSource(object):
    '''
    This class is used to generate a stream of data for use in streaming
    testing.
    '''

    def __init__(self, outputFileName, objectsToSendPerInterval, interval, durationInIntervals=-1):
        '''
        Constructor
        objectsToSendPerInterval - number of bytes to generate per interval
        interval - floating point delay (in seconds) between data generation
        Note that the pattern of data generated defaults to "incrementing bytes"
        see @setPattern to change the pattern generated.
        '''
        self.objectsToSendPerInterval = objectsToSendPerInterval
        self.interval = interval
        self.pattern = PATTERN_INCREMENTING
        self.patternString = None
        self.outputFileName = outputFileName
        self.durationInIntervals = durationInIntervals
        self.state = "init"
        self.thread = None
        self.quit = False
        self._bytesTxed = 0
        self.doneCallback = voidCallbackTwoArgs
        self.progressCallback = voidCallbackTwoArgs
        self.errorString = None
        
    def setCallbacks(self, progressCallback, doneCallback):
        self.doneCallback = doneCallback
        self.progressCallback = progressCallback
        
    def _announceProgress(self, byteCount):
        self.progressCallback(self, {"outputSize": byteCount})
        
    def setPatternString(self, s):
        self.patternString = s
        self.pattern = PATTERN_FIXED_STRING
        
    def setPatternFileOnce(self, filePath):
        self.patternString = filePath
        self.pattern = PATTERN_FILE_ONCE
        
    def setPatternFileRepeating(self, filePath):
        self.patternString = filePath
        self.pattern = PATTERN_FILE_REPEATING
        
    def startGenerator(self):
        logger.debug("Starting generator")
        if self.state != "init":
            raise Exception("Expected state init, found {}", self.state)
        self.thread = SmStreamTestSourceThread(self) 
        self.thread.start()
        self.state = "running"
        
    def stopGenerator(self, timeout):
        logger.debug("Stopping generator")
        
        if self.state == "init":
            return        
        if self.state == "stopped":
            return        
        
        logger.debug("Thread still running, requesting stop...")
        self.quit = True
        self.thread.join(timeout)
        if self.thread.isAlive():
            logger.error("Generator did not stop within {}", timeout)
            raise Exception("Generator did not stop")
        else:
            logger.debug("Thread stopped")
   
    def isDone(self):
        if self.state == "stopped":
            return True
        else:
            return False
        
    def errorSeen(self):
        return self.errorString is not None
    
    def getErrorString(self):
        return self.errorString
    
    def _generateData(self):
        outputFd = None
        patternFile = None
        try:
            logger.debug("Opening %s to generate stream", self.outputFileName)
            outputFd = open(self.outputFileName, "wb")
            if self.pattern == PATTERN_FILE_ONCE or self.pattern == PATTERN_FILE_REPEATING:
                patternFile = open(self.patternString, "rb")

            count = 0
            b = bytearray()
            b.extend([0])
            while not self.quit:
                if self.durationInIntervals == count:
                    logger.debug("Source sent enough data, exiting, count = %d", count)
                    return
                count += 1
                if self.pattern == PATTERN_INCREMENTING:
                    for _ in range(0, self.objectsToSendPerInterval):
                        #logger.debug("Sending %d", b[0])
                        outputFd.write(b)
                        self._bytesTxed += 1
                        if b[0] == 255:
                            b[0] = 0
                        else:
                            b[0] += 1
                elif self.pattern == PATTERN_FIXED_STRING:
                    for _ in range(0, self.objectsToSendPerInterval):
                        #logger.debug("Sending %s", self.patternString)
                        outputFd.write(self.patternString)
                        self._bytesTxed += len(self.patternString)
                elif self.pattern == PATTERN_FILE_ONCE or self.pattern == PATTERN_FILE_REPEATING:
                    buf = patternFile.read(self.objectsToSendPerInterval)
                    if (len(buf) < self.objectsToSendPerInterval):
                        if self.pattern == PATTERN_FILE_ONCE:
                            self.quit = True
                        else:
                            patternFile.seek(0)
                            b2 = bytearray()
                            b2 += buf
                            b2 += patternFile.read(self.objectsToSendPerInterval - len(buf))
                            buf = b2
                    logger.debug("Sending %d bytes", len(buf))
                    outputFd.write(buf)
                    self._bytesTxed += len(buf)
                else:
                    logger.error("Unknown pattern " + self.pattern)
                    raise Exception("Unknown pattern " + self.pattern)
                self._announceProgress(self._bytesTxed)
                time.sleep(self.interval)
        except:
            logger.error("Exception!", exc_info=True)
        finally:
            self.state = "stopped"
            sm_safe_close(patternFile)
            sm_safe_close(outputFd)
            if self.errorSeen():
                self.doneCallback(self, "error")
            else:
                self.doneCallback(self, "success")
            logger.debug("Invoked doneCallback")
    
    def getBytesTxed(self):
        return self._bytesTxed
    
           
class SmStreamTestSinkThread(threading.Thread):
    
    def __init__(self, streamTestSink):
        global THREAD_COUNT
        threading.Thread.__init__(self)
        self.name = "sinkThread-" + str(THREAD_COUNT)
        THREAD_COUNT += 1 
        self.streamTestSource = streamTestSink
        
    def run(self):
        self.streamTestSource._consumeData()


class SmStreamTestSink(object):
    '''
    This class is used to generate a stream of data for use in streaming
    testing.
    '''

    def __init__(self, inputFileName, exitAfterNBytes=-1):
        '''
        Constructor
        objectsToSendPerInterval - number of bytes to generate per interval
        interval - floating point delay (in seconds) between data generation
        Note that the pattern of data generated defaults to "incrementing bytes"
        see @setPattern to change the pattern generated.
        '''
        self.inputFileName = inputFileName
        self.state = "init"
        self.thread = None
        self.exitAfterNBytes = exitAfterNBytes
        self.errorString = None
        self.quit = False
        self._bytesRxed = 0
        self.pattern = PATTERN_INCREMENTING
        self.doneCallback = voidCallbackTwoArgs
        self.progressCallback = voidCallbackTwoArgs
        
    def setCallbacks(self, progressCallback, doneCallback):
        self.doneCallback = doneCallback
        self.progressCallback = progressCallback
        
    def _announceProgress(self, byteCount):
        self.progressCallback(self, {"outputSize": byteCount})
        
    def setExpectedPatternString(self, s):
        self.patternString = s
        self.pattern = PATTERN_FIXED_STRING
        
    def setNoPattern(self):
        self.pattern = PATTERN_NONE
        
    def setDestFile(self, filePath):
        self.patternString = filePath
        self.pattern = PATTERN_FILE_REPEATING
        
    def startConsumer(self):
        if self.state != "init":
            raise Exception("Expected state init, found {}", self.state)
        self.thread = SmStreamTestSinkThread(self) 
        self.thread.start()
        self.state = "running"
        
    def stopConsumer(self, timeout):
        logger.debug("Stopping consumer")
        
        if self.state == "init":
            return        
        if self.state == "stopped":
            return        
        
        logger.debug("Consumer thread still running, requesting stop...")
        self.quit = True
        self.thread.join(timeout)
        if self.thread.isAlive():
            logger.error("Consumer did not stop within %f", timeout)
            raise Exception("Consumer did not stop")
        else:
            logger.debug("Thread stopped")

    def isDone(self):
        if self.state == "stopped":
            return True
        else:
            return False
                
    def errorSeen(self):
        return self.errorString is not None
    
    def getErrorString(self):
        return self.errorString
    
    def _calcFirstExpectedValue(self):
        if self.pattern == PATTERN_INCREMENTING:
            self.nextIncrValue = 0
            return 0
        elif self.pattern == PATTERN_FIXED_STRING:
            self.nextOffset = 1
            return self.patternString[0]
        else:
            return None
        
    def _calcNextExpectedValue(self):
        if self.pattern == PATTERN_INCREMENTING:
            self.nextIncrValue += 1
            return self.nextIncrValue
        elif self.pattern == PATTERN_FIXED_STRING:
            v = self.patternString[self.nextOffset]
            self.nextOffset += 1
            if self.nextOffset >= len(self.patternString):
                self.nextOffset = 0
            return v
        else:
            return None
    
    def _consumeData(self):
        inputFd = None
        try:    
            logger.debug("Opening %s to consume stream", self.inputFileName)
            inputFd = open(self.inputFileName, "rb")
            self._bytesRxed = 0
            nextExpectedValue = self._calcFirstExpectedValue()
            if self.pattern == PATTERN_FILE_REPEATING:
                patternFile = open(self.patternString, "wb")
            while not self.quit:
                if self._bytesRxed >= self.exitAfterNBytes:
                    logger.debug("Got enough data, exiting")
                    break
                chunk = inputFd.read(100)                
                if len(chunk) == 0:
                    logger.debug("Got no data from the input file descriptor, delaying and retrying...")
                    time.sleep(0.1)
                    #break
                self._bytesRxed += len(chunk)
                if self.pattern == PATTERN_FILE_REPEATING:
                    patternFile.write(chunk)
                else:
                    #logger.debug("Got %d byte, bytesRead=%d", len(chunk), self._bytesRxed)
                    for byte in chunk:
                        #logger.debug("Got value %d", byte)
                        if nextExpectedValue is not None and byte != nextExpectedValue: 
                            self.errorString = "Expected {0} found {1}".format(nextExpectedValue, byte)
                            raise Exception("Expected {0} found {1}".format(nextExpectedValue, byte))
                        nextExpectedValue = self._calcNextExpectedValue()
                self._announceProgress(self._bytesRxed)

        finally:
            self.state = "stopped"   
            if inputFd:
                inputFd.close()     
                logger.debug("closing consumer FD")
            if self.errorSeen():
                self.doneCallback(self, "error")
            else:
                self.doneCallback(self, "success")
    
    def getBytesRxed(self):
        return self._bytesRxed
    
    
