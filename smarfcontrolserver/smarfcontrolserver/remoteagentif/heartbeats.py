'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 10, 2014

    @author: rpsmarf
'''
from smcommon.heartbeats.heart_monitor import SmHeartMonitor
from django.db.models.signals import post_save, pre_delete
from sm_controller.models import SmModelAgent
import logging
import datetime
import pytz
import smmonicemsgs
from django import db

logger = logging.getLogger(__name__)


class ScsHeartbeatIf(smmonicemsgs.SmHeartbeat):  # @UndefinedVariable
    '''
    This class implements logic to monitor the heartbeats coming from the 
    remote agents to determine if the remote agents are up or not.  It 
    includes several callbacks from:
    - ICE
    - the heart monitor object which determines is a heart has stopped
    - Django to say that remote agents are being added or deleted
    '''

    def __init__(self, minInterval, minTimeAfterAddBeforeAlarm, pollInterval):
        '''
        Constructor
        '''
        
        self.heartMonitor = SmHeartMonitor(minInterval, self, minTimeAfterAddBeforeAlarm, pollInterval)
        
        # Register to track new agents coming and going
        post_save.connect(self.postSaveAgentSignalCallback, sender=SmModelAgent)
        pre_delete.connect(self.preDeleteAgentSignalCallback, sender=SmModelAgent) 
        
        # Add the existing agents in DB
        agents = SmModelAgent.objects.all()
        for agent in agents:
            self.heartMonitor.addHeart(agent.guid)
            
    def start(self):
        self.heartMonitor.start()
        
    def sendHeartbeat(self, heartId, beatCount, current=None):
        '''
        Method invoked from the ICE server to indicate that a remote
        server has sent a heartbeat 
        '''
        self.heartMonitor.heartbeat(heartId, beatCount)
        return 0

    def heartStopped(self, heartId):
        logger.warning("Got heart stopped event for agent[" + heartId + "]")
        agent = SmModelAgent.objects.get(guid=heartId)
        agent.status = "down"
        agent.last_change = datetime.datetime.now(pytz.utc)
        agent.save()
        db.close_connection()                
        
    def heartStarted(self, heartId):
        logger.warning("Got heart started event for agent[" + heartId + "]")
        agent = SmModelAgent.objects.get(guid=heartId)
        agent.status = "up"
        agent.last_change = datetime.datetime.now(pytz.utc)
        agent.save()
        db.close_connection()                
        logger.info("Agent status set to up")
           
    def unknownHeart(self, heartId):
        logger.error("Unknown remote agent with guid=" + heartId + " reporting")

    def postSaveAgentSignalCallback(self, sender, **kwargs):
        '''
        Note that this could be about a new agent or a pre-existing agent
        being saved
        '''
        heartId = kwargs["instance"].guid
        if (heartId not in self.heartMonitor.heartList):
            logger.debug("Got post save signal callback for new id " + heartId)
            self.heartMonitor.addHeart(heartId)

    def preDeleteAgentSignalCallback(self, sender, **kwargs):
        heartId = kwargs["instance"].guid
        logger.debug("Got pre delete signal callback for id " + heartId)
        self.heartMonitor.deleteHeart(heartId)

