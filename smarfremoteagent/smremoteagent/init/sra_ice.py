'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 11, 2014

    @author: rpsmarf
'''
import logging
import Ice
import smmonicemsgs
from smcommon.globals.sm_copy_controller import SmCopyController
from smcommon.globals.sm_global_context import smGlobalContextInit,\
    smGlobalContextGet
from smcommon.file_access.ice_file_ops import SmIceFsOps
from smcommon.task_runners.ice_async_ops_server import SmIceAsyncOpsServer
from smcommon.task_runners.ice_sync_ops_server import SmIceSyncOpsServer

logger = logging.getLogger(__name__)


class SraIceIf(object):
    '''
    classdocs
    '''


    def __init__(self, scsHostName, scsPort, icePort, asyncObjCallbackString, asyncRespObj, agentGuid):
        '''
        Constructor
        '''
        logger.info("Agent port %d, SCS host %s, SCS port %d", icePort, scsHostName, scsPort)
        self.scsHostName = scsHostName
        self.scsPort = scsPort
        self.icePort = icePort
        copyController = SmCopyController([agentGuid])
        smGlobalContextInit(copyController)
        self.ice = smGlobalContextGet().ic
        adapter = self.ice.createObjectAdapterWithEndpoints("smarf", "default -p " + str(self.icePort))
        self.asyncObj = SmIceAsyncOpsServer(self.ice, asyncObjCallbackString)
        adapter.add(self.asyncObj, self.ice.stringToIdentity("SmRemAgAsyncOps"))
        self.syncObj = SmIceSyncOpsServer()
        adapter.add(self.syncObj, self.ice.stringToIdentity("SmRemAgSyncOps"))
        
        # Used only for testing
        if asyncRespObj is not None:
            adapter.add(asyncRespObj, self.ice.stringToIdentity("SmRemAgAsyncOpResps"))
            
        obj = SmIceFsOps()
        adapter.add(obj, self.ice.stringToIdentity("FileOps"))
        adapter.activate()

        
    def getIceCommunicator(self):
        return self.ice
    
    def makeProxy(self, objectName):
        return self.ice.stringToProxy(objectName + ":tcp -h " + self.scsHostName  + " -p " + str(self.scsPort))
    
    def sendHeartbeat(self, agentGuid, beatCount):
        try:
            #logger.debug("Sending heartbeat (" + agentGuid + ", " + str(beatCount) + ") via ICE")
            heartBeatProxy = smmonicemsgs.SmHeartbeatPrx.checkedCast(self.makeProxy("SmHeartbeat"))  # @UndefinedVariable
            heartBeatProxy.sendHeartbeat(agentGuid, beatCount)
        except Ice.ConnectionRefusedException:  # @UndefinedVariable
            logger.warning("Connection refused when attempting to connect to SCS at " 
                           + self.scsHostName + ":" + str(self.scsPort))
            
    