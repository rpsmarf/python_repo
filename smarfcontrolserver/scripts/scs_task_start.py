#!/usr/bin/env python 
# encoding: utf-8
'''
scripts.scs_task_start -- script to start a task via the REST API

scripts.scs_task_start is a script to start a task and monitor the results via the REST API.

@author:     user_name

@copyright:  2014 organization_name. All rights reserved.

@license:    license

@contact:    user_email
@deffield    updated: Updated
'''

import sys
import os
import time
from scripts.scs_obj_get import scsMakePathFromId
from scripts.scs_obj_common import scsScriptGetDefaultUrl, scsScriptGetVerify,\
    scsScriptGetHeaders
sys.path.insert(1, "/opt/smarf-scs/scs")

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
import requests
import json

__all__ = []
__version__ = 0.1
__date__ = '2014-09-25'
__updated__ = '2014-09-25'


class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''

    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
        
    def __str__(self):
        return self.msg
    
    def __unicode__(self):
        return self.msg


def _waitForCompletion(url, timeout, verbose, quiet):
        endTime = time.time() + timeout
        while True:
            get_response = requests.get(url=url, 
                                        headers=scsScriptGetHeaders(), 
                                        verify=scsScriptGetVerify())
            if verbose:
                print("RESPONSE=" + get_response.text)
            get_response.raise_for_status()
            state = get_response.json()["state"]
            if state == "finished":
                break
            print("progress: " + get_response.json()["progressJson"])
            time.sleep(1.0)
            if (time.time() > endTime):
                print("ERROR: Timeout waiting for completion")
                return 1
            
        if (state == "init"):
            print("ERROR: Task never started, state is still 'init'")
            return 1
            
        completion = get_response.json()["completion"]
        if completion != "completedWithoutError":
            print("ERROR: Task did not complete without error.")
            print(json.dumps(get_response.json(), sort_keys=True, indent=2, separators=(',', ': ')))
            return 1
        
        if not quiet:
            print("Task completed without error:")
            print(json.dumps(get_response.json(), sort_keys=True, indent=2, separators=(',', ': ')))


def scsTaskStartByPath(task_path, **kwargs):    
    timeout = kwargs.get("timeout")
    if timeout is None:
        timeout = 1000
    verbose = kwargs.get("verbose")
    if verbose is None:
        verbose = False
    quiet = kwargs.get("quiet")
    if quiet is None:
        quiet = False
    if ("url" in kwargs):
        base_url = kwargs["url"]
    else:
        base_url = scsScriptGetDefaultUrl()
    url = base_url + task_path
    if verbose:
        print("Doing get on URL " + url)
    paramList = "?action=start"
    if "allowQueueing" in kwargs:
        paramList += "&allowQueueing=" + str(kwargs.get("allowQueueing"))
    get_response = requests.get(url=url + paramList, 
                                headers=scsScriptGetHeaders(), 
                                verify=scsScriptGetVerify())
    if verbose:
        print("RESPONSE=" + get_response.text.replace("\n", ""))
    get_response.raise_for_status()
    _waitForCompletion(url, timeout, verbose, quiet)
    

def scsTaskStartById(task_id, **kwargs):
    task_path = scsMakePathFromId("task", task_id)    
    return scsTaskStartByPath(task_path, **kwargs)
    

def main(argv=None): 
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = "Script to start a task and wait (or not) for the task to complete"
    program_license = '''%s

  Created by Andrew McGregor on %s.
  Copyright 2014 Carleton. All rights reserved.
  
USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-u", "--url", dest="url", default=scsScriptGetDefaultUrl,
                            help="URL to access REST API [default: %(default)s]")
        parser.add_argument("-t", "--timeout", dest="timeout", default="5",
                            help="The maximum time to wait for the task to complete")
        parser.add_argument("-k", "--key", dest="primaryKey", 
                            help="The primary key of the task to start (e.g. 7).  Mutually exclusive with -p")
        parser.add_argument("-p", "--path", dest="path", 
                            help="The path of the task to start (e.g. /scs/task/1/.  Mutually exclusive with -k")
        parser.add_argument("-n", "--nowait", dest="noWait", action='store_true',
                            help="Return immediately, do not wait for the task to complete")
        parser.add_argument("-v", "--verbose", action='store_true', dest="verbose",
                            help="Enable verbose mode")
        parser.add_argument("-q", "--quiet", action='store_true', dest="quiet",
                            help="Suppress normal output.  Output only a message on failure.")

        # Process arguments
        args = parser.parse_args()

        if args.verbose:
            print("Verbose mode on")

        if args.path is None and args.primaryKey is None:
            parser.print_help()
            raise CLIError("Type must be specified.")
        if args.path is not None:
            url = args.url + args.path
        else:
            url = args.url + "/scs/task/" + args.primaryKey + "/"
            
        start_task_url = url + "?action=start"
        if args.verbose:
            print("Start task URL=" + start_task_url)
        get_response = requests.get(url=start_task_url, 
                                    headers=scsScriptGetHeaders(), 
                                    verify=scsScriptGetVerify())
        if args.verbose:
            print("RESPONSE=" + get_response.text)
        get_response.raise_for_status()

        if args.noWait:
            return 0
        
        _waitForCompletion(url, args.timeout, args.verbose, args.quiet)
                
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help\n")
        return 2

if __name__ == "__main__":
    sys.exit(main())
