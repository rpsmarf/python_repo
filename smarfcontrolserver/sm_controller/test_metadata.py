'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 10, 2015

    @author: rpsmarf
'''

import unittest
from sm_controller.models import SmModelResource, SmModelCommunity,\
    SmModelTaskType, SmModelResourceTypeTaskType
from django.test.testcases import TransactionTestCase
from sm_controller.test_helper import ScsTestHelper, DUMMY_DESCRIPTION
import json
from sm_controller.signal_handlers import smTriggerSignalInstall
import os.path
from zipfile import ZipFile
import logging
from smcommon.globals.sm_copy_controller import SmCopyController
from smcommon.globals.sm_global_context import smGlobalContextInit
import io

logger = logging.getLogger(__name__) 
smTriggerSignalInstall()


class TestBytesIO(io.BytesIO):
    
    def __init__(self, s, name):
        self.name = name
        super().__init__(s.encode('utf-8'))


class MetadataTest(TransactionTestCase):

    def setUp(self):
        self.helper = ScsTestHelper(self)
        self.helper.setupModel()
        self.helper.setupRestAuth("root")
        self.copyController = SmCopyController(["111", "testagentid"])
        smGlobalContextInit(self.copyController)
        
    def tearDown(self):
        self.helper.tearDown()
        TransactionTestCase.tearDown(self)
        
    def makeSchema(self, res_type_path, user_path, community_path):
        schema_path = self.helper.objCreateViaRest("/scs/md_schema/",
                                                   {"name": "Sample Schema",
                                                    "description": "This is the example schema",
                                                    "owner": user_path,
                                                    "resource_type": res_type_path,
                                                    "community": community_path
                                                    })
        
        self.ptype1 = self.helper.objCreateViaRest("/scs/md_property_type/",
                                                   {"name": "Wind Speed",
                                                    "name_key": "wind_speed",
                                                    "property_type": "INT",
                                                    "schema": schema_path
                                                    })
        self.ptype2 = self.helper.objCreateViaRest("/scs/md_property_type/",
                                                   {"name": "Wind Dir",
                                                    "name_key": "wind_dir",
                                                    "property_type": "STRING",
                                                    "schema": schema_path
                                                    })
        self.ptype3 = self.helper.objCreateViaRest("/scs/md_property_type/",
                                           {"name": "Temperature",
                                            "name_key": "temperature",
                                            "property_type": "FLOAT",
                                            "schema": schema_path
                                            })
        
        return schema_path
    
    def genBulkUploadData(self, dataFilePath, filePathPrefix, count):
        dirList = ['N', 'E', 'S', 'W']
        pathPrefix = filePathPrefix
        data = {}
        for x in range(0, count):
            key = pathPrefix + str(x)
            data[key] = {'wind_speed': x, 
                         'wind_dir': dirList[x % 4], 
                         'temperature': (x * 0.5)}
        jsonData = {'data': data}
        with open(dataFilePath, 'w') as outfile:
            json.dump(jsonData, outfile)
            
    def test_model(self):
        
        # Get a resource and resource type
        res = SmModelResource.objects.first()
        res_path = "/scs/resource/" + str(res.id) + "/"
        res_type = res.resource_type
        community = SmModelCommunity.objects.first()
        community_path = "/scs/community/" + str(community.id) + "/"
        user_path = "/scs/user/" + str(self.helper.user.id) + "/"
        
        # Create schema with 3 properties
        schema_path = self.makeSchema("/scs/resource_type/" + str(res_type.id) + "/",
                                     user_path,
                                     community_path)
        
        # Create repo
        repo_path = self.helper.objCreateViaRest("/scs/md_repo/",
                                                {"name": "Metadata repo 1",
                                                 "description": "This is the first example metadata repository",
                                                 "owner": user_path,
                                                 "resources": [res_path],
                                                 "community": community_path,
                                                 "schema": schema_path
                                                 })
        repo_id_str = repo_path.split("/")[3]
        
        # Add 3 files with 2 properties each
        winddirs = ["N", "E", "S"]
        for i in range(0, 6):
            path_path = self.helper.objCreateViaRest("/scs/md_path/", 
                                                     {"relative_path": "dir/f" + str(i),
                                                      "repo": repo_path,
                                                      "resource": res_path,
                                                      })
            self.helper.objCreateViaRest("/scs/md_property/", 
                                        {"path": path_path,
                                         "property_type": self.ptype1,
                                         "value": str(10 * i)}) 
            self.helper.objCreateViaRest("/scs/md_property/", 
                                        {"path": path_path,
                                         "property_type": self.ptype2,
                                         "value": winddirs[i % 3]})
            self.helper.objCreateViaRest("/scs/md_property/", 
                                        {"path": path_path,
                                         "property_type": self.ptype3,
                                         "value": str(7.5 * i)})  

        search_path = "/scs/md_path/search/?repo=" + repo_id_str

        # Do search for record with wind speed greater than or equal to 20 
        resp = self.helper.objGetViaRest(search_path + "&wind_speed__gte=20")
        obj_list = resp["objects"]
        self.assertEqual(4, len(obj_list))
        
        # Do search for record with temperature greater than or equal to 15 
        resp = self.helper.objGetViaRest(search_path + "&temperature__gte=15")
        obj_list = resp["objects"]
        self.assertEqual(4, len(obj_list))
        
        # Do search for all records
        resp = self.helper.objGetViaRest(search_path)
        obj_list = resp["objects"]
        self.assertEqual(6, len(obj_list))
        
        # Do search for record with wind speed greater than 20 
        resp = self.helper.objGetViaRest(search_path + "&wind_speed__gt=20")
        obj_list = resp["objects"]
        self.assertEqual(3, len(obj_list))
        
        # Do search for record with wind speed less than or equal to 20 
        resp = self.helper.objGetViaRest(search_path + "&wind_speed__lte=20")
        obj_list = resp["objects"]
        self.assertEqual(3, len(obj_list))
        
        # Do search for record with wind speed less than 20 
        resp = self.helper.objGetViaRest(search_path + "&wind_speed__lt=20")
        obj_list = resp["objects"]
        self.assertEqual(2, len(obj_list))
        
        # Do search for record with wind dir = N
        resp = self.helper.objGetViaRest(search_path + "&wind_dir=N")
        obj_list = resp["objects"]
        self.assertEqual(2, len(obj_list))
        
        # Do search for record with wind dir = NW
        resp = self.helper.objGetViaRest(search_path + "&wind_dir=NW")
        obj_list = resp["objects"]
        self.assertEqual(0, len(obj_list))
        
        # Do search for record with wind dir = N & 
        resp = self.helper.objGetViaRest(search_path + "&wind_dir=N&wind_speed=0")
        obj_list = resp["objects"]
        self.assertEqual(1, len(obj_list))
        
        
#         # Purge data from repo
#         self.helper.objGetViaRest("/scs/md_repo/" + repo_id_str + "/purge/")
#         
#         # Search for all records - expect 0
#         resp = self.helper.objGetViaRest(search_path)
#         obj_list = resp["objects"]
#         self.assertEqual(0, len(obj_list))
        
    def test_bulkUpload(self):
        
        # Get a resource and resource type
        res = SmModelResource.objects.first()
        res_path = "/scs/resource/" + str(res.id) + "/"
        res_type = res.resource_type
        community = SmModelCommunity.objects.first()
        community_path = "/scs/community/" + str(community.id) + "/"
        user_path = "/scs/user/" + str(self.helper.user.id) + "/"
        
        # Create schema with 3 properties
        schema_path = self.makeSchema("/scs/resource_type/" + str(res_type.id) + "/",
                                     user_path,
                                     community_path)
        
        # Create repo
        repo_path = self.helper.objCreateViaRest("/scs/md_repo/",
                                                {"name": "Metadata repo 1",
                                                 "description": "This is the first example metadata repository",
                                                 "owner": user_path,
                                                 "resources": [res_path],
                                                 "community": community_path,
                                                 "schema": schema_path
                                                 })
        repo_id_str = repo_path.split("/")[3]
        
        search_path = "/scs/md_path/search/?repo=" + repo_id_str

        # Create 1 path record for dif/x0 to test with preexisting records
        path0_record = self.helper.objCreateViaRest("/scs/md_path/",
                                                    {"repo": repo_path,
                                                     "relative_path": "dir/x0",
                                                     "resource": res_path})
        
        # Check that state is "created"
        resp = self.helper.objGetViaRest(path0_record)
        self.assertEqual(resp["state"], "created")
         
        # Test bulk insert
        record_count = 100
        data_file = self.helper.tempFolder.mkAbsolutePath("f1")
        self.genBulkUploadData(data_file, "dir/x", record_count)
        with open(data_file, "rb") as fp:
            resp = self.client.post("/scs/bulkupload/?repo=" + repo_id_str + "&resource=" + str(res.id),
                                    {'properties': fp})    
        self.assertEqual(resp.status_code, 200, resp.content)
        
        # Do search for all records
        resp = self.helper.objGetViaRest(search_path)
        obj_list = resp["objects"]
        self.assertEqual(record_count, len(obj_list))
        
        # Do search for record with wind dir = north 
        resp = self.helper.objGetViaRest(search_path + "&wind_dir=N")
        obj_list = resp["objects"]
        self.assertEqual(record_count / 4, len(obj_list))
        
        # Do search for record with wind speed greater than or equal to 20 
        resp = self.helper.objGetViaRest(search_path + "&wind_speed__gte=20")
        obj_list = resp["objects"]
        self.assertEqual(record_count - 20, len(obj_list))
        
        # Do search for record with wind speed greater than or equal to 20 
        resp = self.helper.objGetViaRest(search_path + "&temperature__gte=20")
        obj_list = resp["objects"]
        self.assertEqual((record_count * 0.5) + 10, len(obj_list))

        # Check state of records
        resp = self.helper.objGetViaRest(path0_record)
        self.assertEqual(resp["state"], "loaded")
        resp = self.helper.objGetViaRest("/scs/md_path/?relative_path=dir/x1")
        self.assertEqual(len(resp["objects"]), 1)
        self.assertEqual(resp["objects"][0]["state"], "loaded", json.dumps(resp))
   
    def testDownloadSearchResults(self):
        ''' 
        Tests CSV download and JSON download
        '''
        self.helper.setupMetadataModel()
        repo_id_str = self.helper.repo_path.split("/")[3]
        search_path = "/scs/md_path/search/?repo=" + repo_id_str
        resp = self.helper.objGetViaRest(search_path)
        file_list = resp["objects"]
        self.assertEqual(2, len(file_list))
        
        fp = TestBytesIO(json.dumps(file_list), "file.json")
        resp = self.client.post("/scs/resource/downloadfilelist/",
                               {"data": fp, "format": "json"},
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        resp_json = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(file_list, resp_json)

        fp = TestBytesIO(json.dumps(file_list), "file.json")
        resp = self.client.post("/scs/resource/downloadfilelist/", 
                              {"data": fp, "format": "csv"},    
                              **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode("utf-8"))
        csv_list = resp.content.decode("utf-8")
        lines = csv_list.split("\n")
        self.assertEqual(2, len(lines))
   
        resp = self.client.post("/scs/resource/downloadfilelist/", 
                              {"data": json.dumps(file_list), "format": "csv"},    
                              **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode("utf-8"))
        csv_list = resp.content.decode("utf-8")
        lines = csv_list.split("\n")
        self.assertEqual(2, len(lines))
   
    def testZipSearchResults(self):
        ''' 
        Tests ZIP download
        '''
        self.helper.setupMetadataModel()
        
        # Add files to resource folder to match search paths
        self.helper.tempFolder.mkdir("xxx")
        self.helper.tempFolder.mkdir("xxx/dir")
        self.helper.tempFolder.writeFile("xxx/dir/f0", "123")
        self.helper.tempFolder.writeFile("xxx/dir/f1", "456")
        
        repo_id_str = self.helper.repo_path.split("/")[3]
        search_path = "/scs/md_path/search/?repo=" + repo_id_str
        resp = self.helper.objGetViaRest(search_path)
        file_list = resp["objects"]
        self.assertEqual(2, len(file_list))
        
        fp = TestBytesIO(json.dumps(file_list), "file.json")
        resp = self.client.post("/scs/resource/downloadzip/",
                                {"data": fp, "name": "file.zip"},
                                **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)

        zip_file_path = self.helper.tempFolder.makeAbsolutePath("file.zip", "local")
        logger.debug("Writing to %s", zip_file_path)
        with open(zip_file_path, "wb") as f:
            for s in resp.streaming_content:
                logger.debug("Wrote %d bytes", len(s))
                f.write(s)

        # Check folder contents
        zfile = ZipFile(zip_file_path)
        zfile.testzip()
        unzip_dir = self.helper.tempFolder.makeAbsolutePath("unzip_dir", "local")
        zfile.extractall(path=unzip_dir)
        zfile.close()
        self.assertEqual(self.helper.tempFolder.readFile(os.path.join(unzip_dir, "dir/f0"), 10000), b"123")
        self.assertEqual(self.helper.tempFolder.readFile(os.path.join(unzip_dir, "dir/f1"), 10000), b"456")

        # Try a download where one of the files cannot be accessed
        file_list.append({"relative_path": "bad", 
                          "resource": self.helper.resource_path})
        fp = TestBytesIO(json.dumps(file_list), "file.json")
        resp = self.client.post("/scs/resource/downloadzip/",
                                {"data": fp, "name": "file2.zip"},
                                **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        zip_file2_path = self.helper.tempFolder.makeAbsolutePath("file2.zip", "local")
        logger.debug("Writing to %s", zip_file2_path)
        with open(zip_file2_path, "wb") as f:
            for s in resp.streaming_content:
                logger.debug("Wrote %d bytes", len(s))
                f.write(s)

        zfile = ZipFile(zip_file2_path)
        zfile.testzip()
        unzip_dir2 = self.helper.tempFolder.makeAbsolutePath("unzip_dir2", "local")
        zfile.extractall(path=unzip_dir2)
        zfile.close()
        self.assertEqual(self.helper.tempFolder.readFile(os.path.join(unzip_dir2, "dir/f0"), 10000), b"123")
        self.assertEqual(self.helper.tempFolder.readFile(os.path.join(unzip_dir2, "dir/f1"), 10000), b"456")

    def testFsDownloadSearchResults(self):
        ''' 
        Tests CSV download and JSON download via file_set interface
        '''
        self.helper.setupMetadataModel()
        repo_id_str = self.helper.repo_path.split("/")[3]
        search_path = "/scs/md_repo/" + repo_id_str + "/search/" 
        resp = self.helper.objGetViaRest(search_path)
        file_set = resp["file_set"]
        self.assertEqual(2, resp["records"])
        
        resp = self.helper.objGetViaRest("/scs/file_set_item/?file_set=" + str(file_set["id"]))
        self.assertEqual(2, len(resp["objects"]))
        for file_set_item in resp["objects"]:
            self.assertEqual("/scs/resource/1/", file_set_item["resource"])
            self.assertTrue(file_set_item["relative_path"].startswith("dir/f"))
            
        resp = self.client.get("/scs/file_set_item/?file_set=" + str(file_set["id"]) + "&downloadAsFile=true", 
                              **self.helper.auth_header)
        resp_dict = json.loads(resp.content.decode("utf-8"))
        self.assertEqual(2, len(resp_dict))
        self.assertTrue("attachment" in resp["content-disposition"])
        resp = self.client.get("/scs/file_set_item/?file_set=" + str(file_set["id"]) + "&format=csv", 
                              **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode("utf-8"))
        csv_list = resp.content.decode("utf-8")
        lines = csv_list.split("\n")
        self.assertEqual(3, len(lines), csv_list) # Trailing empty line
        for line in lines[:-1]:
            tokens = line.split(",")
            self.assertEqual("/scs/resource/1/", tokens[0])
            self.assertTrue(tokens[1].startswith("dir/f"))
        
        # Do a second search to trigger delete of the previous file_set
        search_path = "/scs/md_repo/" + repo_id_str + "/search/" 
        resp = self.helper.objGetViaRest(search_path)
        file_set_2 = resp["file_set"]
        resp = self.client.get(file_set["resource_uri"], 
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 404, resp.content.decode("utf-8"))
        self.helper.objGetViaRest(file_set_2["resource_uri"])

        # Change auto_delete=False
        self.helper.objModifyViaRest(file_set_2["resource_uri"], {"auto_delete": False})
        
        # Do a third search which will not trigger delete of the previous file_set
        search_path = "/scs/md_repo/" + repo_id_str + "/search/" 
        resp = self.helper.objGetViaRest(search_path)
        file_set_3 = resp["file_set"]
        self.helper.objGetViaRest(file_set_2["resource_uri"])
        self.helper.objGetViaRest(file_set_3["resource_uri"])
                
        # Try download with showProps=True to get the metadata properties for the file_set_items   
        resp = self.helper.objGetViaRest("/scs/file_set_item/?file_set=" + str(file_set_2["id"]) + "&showProps=True")
        self.assertEqual(2, len(resp["objects"]))
        count = 0
        for item in resp["objects"]:
            props = item["props"]
            self.assertEqual(2, len(props))
            self.assertEqual(str(count), props["intProp"])
            self.assertEqual("AAA" + str(count), props["stringProp"])
            count += 1
            
    def testFsZipSearchResults(self):
        ''' 
        Tests ZIP download via file_set based interface
        '''
        self.helper.setupMetadataModel()
        
        # Add files to resource folder to match search paths
        self.helper.tempFolder.mkdir("xxx")
        self.helper.tempFolder.mkdir("xxx/dir")
        self.helper.tempFolder.writeFile("xxx/dir/f0", "123")
        self.helper.tempFolder.writeFile("xxx/dir/f1", "456")
        
        repo_id_str = self.helper.repo_path.split("/")[3]
        search_path = "/scs/md_repo/" + repo_id_str + "/search/" 
        resp = self.helper.objGetViaRest(search_path)
        file_set_path = resp["file_set"]
        self.assertEqual(2, resp["records"])
        
        resp = self.client.get(file_set_path["resource_uri"] + "downloadzip/",
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        zip_file_path = self.helper.tempFolder.makeAbsolutePath("file.zip", "local")
        logger.debug("Writing to %s", zip_file_path)
        with open(zip_file_path, "wb") as f:
            for s in resp.streaming_content:
                logger.debug("Wrote %d bytes", len(s))
                f.write(s)

        # Check folder contents
        zfile = ZipFile(zip_file_path)
        zfile.testzip()
        unzip_dir = self.helper.tempFolder.makeAbsolutePath("unzip_dir", "local")
        zfile.extractall(path=unzip_dir)
        zfile.close()
        self.assertEqual(self.helper.tempFolder.readFile(os.path.join(unzip_dir, "dir/f0"), 10000), b"123")
        self.assertEqual(self.helper.tempFolder.readFile(os.path.join(unzip_dir, "dir/f1"), 10000), b"456")

        # Try a download where one of the files cannot be accessed
        f1_path = self.helper.tempFolder.makeAbsolutePath("xxx/dir/f1", "local")
        os.remove(f1_path)
        
        resp = self.client.get(file_set_path["resource_uri"] + "downloadzip/",
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        zip_file_path2 = self.helper.tempFolder.makeAbsolutePath("file2.zip", "local")
        logger.debug("Writing to %s", zip_file_path)
        with open(zip_file_path2, "wb") as f:
            for s in resp.streaming_content:
                logger.debug("Wrote %d bytes", len(s))
                f.write(s)

        zfile = ZipFile(zip_file_path2)
        zfile.testzip()
        unzip_dir2 = self.helper.tempFolder.makeAbsolutePath("unzip_dir2", "local")
        zfile.extractall(path=unzip_dir2)
        zfile.close()
        self.assertEqual(self.helper.tempFolder.readFile(os.path.join(unzip_dir2, "dir/f0"), 10000), b"123")

    def testFsCopy(self):
        ''' 
        Tests copy of file set
        '''
        self.helper.setupRemoteAgentViaIce()
        
        # Add files to resource folder to match search paths
        self.helper.tempFolder.mkdir("xxx/dest")
        self.helper.tempFolder.mkdir("xxx/dir")
        self.helper.tempFolder.writeFile("xxx/dir/f0", "123")
        self.helper.tempFolder.writeFile("xxx/dir/f1", "456")
        
        self.helper.tempFolder.mkdir("yyy")
        self.helper.tempFolder.mkdir("yyy/dir")
        self.helper.tempFolder.writeFile("yyy/dir/f2", "321")
        self.helper.tempFolder.writeFile("yyy/dir/f3", "654")

        self.taskType = SmModelTaskType()
        self.taskType.name = "copy"
        self.taskType.code_module = "smcommon.task_runners.task_runner_copy"
        self.taskType.code_classname = "SmTaskRunnerCopy"
        self.taskType.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"type": "resource",
                                      "name": "Input",
                                      "input_id": 1,
                                      "direction": "input",
                                      "role": "src"},
                                     {"type": "resource",
                                      "name": "Output",
                                      "input_id": 2,
                                      "direction": "output",
                                      "role": "dest"},
                                     ],
                            "computeResourceRole": "dest"
                       }
                   )
        self.taskType.description = DUMMY_DESCRIPTION
        self.taskType.owner = self.helper.user
        self.taskType.save()
        
        rt3_src = SmModelResourceTypeTaskType(task_type=self.taskType,
                                              role="src",
                                              name="copy_src",
                                              resource_type_id=1,
                                              description=DUMMY_DESCRIPTION)
        rt3_src.save()
        rt3_dest = SmModelResourceTypeTaskType(task_type=self.taskType,
                                               role="dest",
                                               name="copy_dest",
                                               resource_type_id=1,
                                               description=DUMMY_DESCRIPTION)
        rt3_dest.save()
        
        # Create file set and file set items records
        file_set_path = self.helper.objCreateViaRest("/scs/file_set/", {"name": "x", "name_key": "x", "source": "user"})
        res1 = SmModelResource.objects.get(id=1)
        res2 = SmModelResource(resource_type=res1.resource_type, name="yyy", 
                               parametersJson=json.dumps({"folder": "yyy/"}),
                               capacityJson=json.dumps({}), owner=res1.owner,
                               container=res1.container, description=DUMMY_DESCRIPTION,
                               status="up")
        res2.save()
        
        file_set_id = int(file_set_path.split("/")[3])
        for rel_path, res_obj in [("dir/f0", res1), ("dir/f1", res1), ("dir/f2", res2), ("dir/f3", res2)]:
            self.helper.objCreateViaRest("/scs/file_set_item/", 
                                         {"file_set": "/scs/file_set/" + 
                                          str(file_set_id) + "/", 
                                          "relative_path": rel_path, 
                                          "resource": "/scs/resource/" + 
                                          str(res_obj.id) + "/"})
            
        # Copy files to res1
        resp = self.helper.objGetViaRest("/scs/resource/" + str(res1.id) +
                                         "/dest/copy_from_file_set/?file_set=" 
                                         + file_set_path.split("/")[3])
        self.assertEqual(2, len(resp["tasks"]))
        self.helper.waitForTaskDone(int(resp["tasks"][0].split("/")[3]), 2.0, 0.3)
        self.helper.waitForTaskDone(int(resp["tasks"][1].split("/")[3]), 2.0, 0.3)
        destFolder = resp["dest_path"]
        
        # Check files present in destination folder
        self.assertEqual(self.helper.tempFolder.readFile("xxx/" + destFolder + "/resource_resourcexxx/f0", 100), b"123")
        self.assertEqual(self.helper.tempFolder.readFile("xxx/" + destFolder + "/resource_resourcexxx/f1", 100), b"456")
        self.assertEqual(self.helper.tempFolder.readFile("xxx/" + destFolder + "/resource_yyy/f2", 100), b"321")
        self.assertEqual(self.helper.tempFolder.readFile("xxx/" + destFolder + "/resource_yyy/f3", 100), b"654")
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()