angular.module('rpsmarf.simpleinput', [
	'ui.bootstrap'
])
.controller('SimpleInputCtrl', ['$scope', '$modalInstance', 'name', function($scope, $modalInstance, name){

	/***STANDARD VARIABLES***/

	/***SCOPE VARIABLES***/
	$scope.name = {val:name};

	/***SCOPE FUNCTIONS***/
	$scope.ok = function(){
		$modalInstance.close($scope.name.val);
	};

	$scope.cancel = function(){
		$modalInstance.dismiss();
	};



}]);