'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
from smcommon.constants.sm_const import SM_DATA_CONTAINER_PROTOCOL_LOCAL,\
    SM_DATA_CONTAINER_PROTOCOL_SFTP, SM_DATA_CONTAINER_PROTOCOL_SSH,\
    SM_DATA_CONTAINER_PROTOCOL_SWIFT, SM_DATA_CONTAINER_PROTOCOL_S3
import logging
from os.path import os
import uuid
from smcommon.file_access.data_access_desc import SmDataAccessDescriptor,\
    SmDataContainer
from smcommon.file_access.file_accessor_local import SmFileAccessorLocal
from smcommon.file_access.file_accessor_sftp import SmFileAccessorSftp
from smcommon.file_access.file_accessor_ice import SmFileAccessorIce
from smcommon.file_access.sm_copy import SmCopy
from smcommon.exceptions.sm_exc import SmException
from smcommon.file_access.deleter import SmDeleter
from smcommon.globals.envvar import SM_ENV_VAR_COMMON_TEMP_DIR,\
    smCommonGetEnvVar
import shutil
from smcommon.constants import sm_const
from smcommon.file_access.file_accessor_swift import SmFileAccessorSwift
from smcommon.file_access.file_accessor_s3 import SmFileAccessorS3

logger = logging.getLogger(__name__) 


class SmCopyController(object): 
    '''
    '''

    def __init__(self, listOfAgentIds):
        self.listOfAgentIds = listOfAgentIds
        self.tempDir = smCommonGetEnvVar(SM_ENV_VAR_COMMON_TEMP_DIR)
        self.tempDataContainer = SmDataContainer(sm_const.smConstGetMyAgentId(),
                                                 "",
                                                 SM_DATA_CONTAINER_PROTOCOL_LOCAL + "://localhost/" + self.tempDir)
        self.purgeTmpFolder()
        os.mkdir(self.tempDir)
                    
    def makeTempDad(self, folder, execInfo):
        guid = uuid.uuid4()
        if execInfo["protocol"] == SM_DATA_CONTAINER_PROTOCOL_LOCAL:
            tempDataContainer = self.tempDataContainer
            name = os.path.join(self.tempDir, str(guid))
        elif execInfo["protocol"] == SM_DATA_CONTAINER_PROTOCOL_SSH:
            paramDict = {}
            if "password" in execInfo and execInfo["password"]:
                paramDict["password"] = execInfo["password"]
            if "keyfile" in execInfo and execInfo["keyfile"]:
                paramDict["keyfile"] = execInfo["keyfile"]
            paramString = ""
            first = True
            for key in paramDict.keys():
                logger.debug("Processing key %s with value %s", key, paramDict[key])
                if first:
                    paramString += "?"
                    first = False
                else:
                    paramString += "&"
                paramString += key + "=" + paramDict[key]
            if "tempDataFolder" in execInfo:
                tempDataFolder = execInfo["tempDataFolder"]
            else:
                tempDataFolder = "/tmp"
            tempDataContainer = SmDataContainer(self.listOfAgentIds[0],
                                                "",
                                                SM_DATA_CONTAINER_PROTOCOL_SFTP + "://" +
                                                execInfo["user"] + "@" + execInfo["host"] + ":" + str(execInfo["port"]) +
                                                tempDataFolder + paramString)
            name = str(guid)
            
        if folder:
            name = name + "/"
    
        return SmDataAccessDescriptor(tempDataContainer,
                                      name, 
                                      True)

    def makeNamedPipeDad(self):
        guid = uuid.uuid4()
        name = os.path.join(self.tempDir, str(guid))
        os.mkfifo(name)
        return SmDataAccessDescriptor(self.tempDataContainer,
                                      name, 
                                      True)

    def makeCopier(self, srcDad, destDad, callback):
        return SmCopy(srcDad, destDad, callback)
    
    def makeFileAccessor(self, dataContainer):
        protocol = dataContainer.getProtocol()
        if (protocol == SM_DATA_CONTAINER_PROTOCOL_LOCAL):
            if (dataContainer.agentId not in self.listOfAgentIds):
                #logger.debug("Returning SmFileAccessorIce")
                return SmFileAccessorIce(dataContainer)
            else:
                #logger.debug("Returning SmFileAccessorLocal")
                return SmFileAccessorLocal(dataContainer)
        elif (protocol == SM_DATA_CONTAINER_PROTOCOL_SFTP) or (protocol == SM_DATA_CONTAINER_PROTOCOL_SSH):
            if (dataContainer.agentId not in self.listOfAgentIds):
                #logger.debug("Returning SmFileAccessorIce")
                return SmFileAccessorIce(dataContainer)
            else:
                #logger.debug("Returning SmFileAccessorStfp")
                return SmFileAccessorSftp(dataContainer)
        elif (protocol == SM_DATA_CONTAINER_PROTOCOL_SWIFT):
            #logger.debug("Returning SmFileAccessorSwift")
            return SmFileAccessorSwift(dataContainer)
        elif (protocol == SM_DATA_CONTAINER_PROTOCOL_S3):
            #logger.debug("Returning SmFileAccessorStfp")
            return SmFileAccessorS3(dataContainer)
        else:
            raise SmException(422, "EOTHER", "", "Unknown protocol in URL: " + protocol)

    def makeDeleter(self, srcDad, callback):
        return SmDeleter(srcDad, callback)
                
    def purgeTmpFolder(self):
        return shutil.rmtree(self.tempDir, True)

