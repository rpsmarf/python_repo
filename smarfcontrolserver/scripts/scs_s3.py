#!/usr/bin/env python 
# encoding: utf-8
'''
scripts.scs_s3

This is a script to list buckets and bulk delete them.

@author:     Andrew McGregor

@copyright:  2014 organization_name. All rights reserved.

@license:    license

@contact:    andrewmcgregor@sce.carleton.ca
@deffield    updated: Updated
'''

import sys
import os
from requests.packages import urllib3
from smcommon.utils.json_utils import dumpsPretty
from smcommon.file_access.aws_s3_accessor import AwsS3Accessor
sys.path.insert(1, "/opt/smarf-scs/scs")

from argparse import ArgumentParser 
from argparse import RawDescriptionHelpFormatter

__all__ = []
__version__ = 0.1
__date__ = '2014-09-25'
__updated__ = '2014-09-25'

urllib3.disable_warnings()


class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''

    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
        
    def __str__(self):
        return self.msg
    
    def __unicode__(self):
        return self.msg


def main(argv=None): 
    '''Command line options.
    '''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = "Script to operate on S3 objects"
    program_license = '''%s

  Created by Andrew McGregor on %s.
  Copyright 2015 Carleton. All rights reserved.
  
USAGE:
   -a <action> - actions are list or delete
   -d <bucket> - Bucket to delete or list.  If no bucket is specified on list, all buckets are listed 
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-b", "--bucket", dest="bucket", default="",
                            help="Bucket to operate on")
        parser.add_argument("-a", "--action", dest="action", default="",
                            help="The action to perform (list or delete)")
        parser.add_argument("-f", "--format", dest="format", default="plain",
                            help="The format - 'plain' or 'json')")
        parser.add_argument("-v", "--verbose", action='store_true', dest="verbose",
                            help="Enable verbose mode")

        # Process arguments
        args = parser.parse_args()

        if args.verbose:
            print("Verbose mode on\n")
            print(("SCS_TEST_AWS_KEY_ID: {}\n" +
                  "SCS_TEST_AWS_SECRET_KEY: {}\n" +
                  "bucket: {}\n").format(os.environ["SCS_TEST_AWS_KEY_ID"],
                                         os.environ["SCS_TEST_AWS_SECRET_KEY"],
                                         args.bucket)
                  )

        if args.action == "":
            parser.print_help()
            raise CLIError("Action must be specified.\n")

        s3If = AwsS3Accessor(os.environ["SCS_TEST_AWS_KEY_ID"],
                             os.environ["SCS_TEST_AWS_SECRET_KEY"],
                             args.bucket, 3)
        if args.action == "list":
            if args.bucket == "":
                if args.verbose:
                    print("Listing buckets...\n")
                list_info = s3If.listBuckets()
                if args.format == 'json':
                    print(dumpsPretty(list_info))
                    print("\n")
                else:
                    for rec in list_info:
                        print("{}".format(rec["name"]))
            else:
                if args.verbose:
                    print("Listing buckets {}...\n".format(args.bucket))
                list_info = s3If.listKeysInsideBucket("", True, 1000, None)
                if args.format == 'json':
                    print(dumpsPretty(list_info))
                    print("\n")
                else:
                    for rec in list_info:
                        print("{}".format(rec["name"]))
        elif args.action == "delete":
            if args.bucket == "":
                raise CLIError("bucket must be specified for delete.\n")
                
            if args.verbose:
                print("Listing objects in bucket " + args.bucket)
            list_info = s3If.listKeysInsideBucket("", True, 1000, None)
            if args.verbose:
                print("Found {} objects in bucket {}".format(len(list_info), args.bucket))
            for rec in list_info:
                if args.verbose:
                    print("Deleting " + rec["name"])
                s3If.removeFileOrFolder(rec["name"])
            print("Deleting bucket " + args.bucket)
            s3If.deleteBucket()
        else:
            raise CLIError("Invalid action " + args.action + ". Must be 'list' or 'delete'")
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + str(e) + "\n")
        sys.stderr.write(indent + "  for help use --help\n")
        return 2

if __name__ == "__main__":
    sys.exit(main())
