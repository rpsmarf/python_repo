#!/bin/bash


function copy_files {
echo "Copying $1 $3 times into $2 as $4..."
input_file=$1
output_folder=$2
number=$3
base_path=$4

mkdir $output_folder
for i in `seq 1 $3`;
do
    echo cp $input_file $output_folder/$base_path$i
    cp $input_file $output_folder/$base_path$i
done
}

copy_files $1 $2 $3 $4

echo "Copying files into $6..."
copy_files $5 $6 $7 $8

echo "Done."
