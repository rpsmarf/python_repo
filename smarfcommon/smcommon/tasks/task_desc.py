'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import logging
import json
from smcommon.file_access.data_access_desc import SmDataAccessDescriptor
from smcommon.utils.json_utils import dumpsPretty

logger = logging.getLogger(__name__)


class SmTaskDesc(object):
    '''
    This structure is passed from the SCS to the SRA to tell the SRA
    to run a task.
    
    The _dadDict contains zero or more "data access descriptors" which 
    describe the data source or sinks associate with a task (@see SmDataAccessDescriptor)
    Each data descriptor has a tag which is typically accessed by the task.
    
    Argument types include:
    - string - a string value
    - stream - a data stream which appears as a named pipe to the running application
    - dad - a file or folder
    - dad_file - a file containing a list of lines of comma-separated values of the
    form:
    <original name>,<local name>
    - script - where value is a string.  This is copied to the local system system,
    the name is passed as this argument
    For the _taskSpecificParamDict there is structure as follows (for example
    for the command "cp -r src_dir dest_dir"):
    { "args": [ { "type": "string", "value" : "cp" },
                { "type": "string", "value" : "-r" },
                { "type": "stream", "direction": "input", 
                  "dad_file" : {"agentId": "AGENT1", agentUri": "ice://agent.rpsmarf.ca:9001",
                           "containerUri": "local://localhost/tmp/folder2",
                           "path": ["src/a", "src/b"], "port": "-1", "modifier": "" },
                }
                { "type": "dad", "direction": "output",  
                  "dad" : {"agentId": "AGENT1", agentUri": "ice://agent.rpsmarf.ca:9001",
                           "containerUri": "sftp://fred@a.b.c/tmp/folder?keyfile=mykey",
                           "path": "dest/b", "port": "-1", "modifier": "" },
                }
              ],
      "outputProgress" : {"outputArg": 3, #optional - which of the args to use for a file
                          "maxSize" : 1234, # optional
                          "stdoutLines": 50, # optional - max lines on stdout
                         },
      "execInfo" : {"host": "win1.rpsmarf.ca", # from containerUrl
                    "port" : 22, # from containerUrl
                    "protocol": "ssh",   # from containerUrl
                    "workingDir": "/user/rpsmarf", # from containerUrl
                    "user": "rpsmarf", # from resource.parametersJson
                    "password": "xxx", # from resource.parametersJson
                    "keyfile": "dropbear_key" # resource.parametersJson
                    "tempDataFolder": "/tmp", # from resource.parametersJson
                    "versioning": "git", # Optional if git sync
                    "version": "<git tag to fetch before tool is run>" # Must be set
                    } # resource.parametersJson
    }
    '''
    
    def __init__(self, module, cls):
        '''
        Constructor
        '''
        self.module = module
        self.cls = cls
        self._commonParamDict = {}    
        self._taskSpecificParamDict = {}       
        
    def addTaskParam(self, paramName, paramValue):
        self._taskSpecificParamDict[paramName] = paramValue
    
    def getTaskParam(self, name):
        return self._taskSpecificParamDict.get(name)
    
    def getArgDict(self, index):
        args = self._taskSpecificParamDict["args"]
        if len(args) > index:
            return args[index]
    
    def getArgDad(self, arg):
        return SmDataAccessDescriptor.makeFromDict(arg.get("dad"))
    
    def getExecInfo(self,):
        execInfo = self._taskSpecificParamDict.get("execInfo")
        if execInfo is None:
            return {}
        return execInfo
    
    @classmethod
    def makeFromJson(cls, jsonString):
        d = json.loads(jsonString)
        desc = SmTaskDesc(d["module"], d["cls"])
        desc._taskSpecificParamDict = d["taskSpecificParamDict"]
        desc._commonParamDict = d["commonParamDict"]
        return desc
    
    def toJson(self):
        d = {"module": self.module, "cls": self.cls, 
             "commonParamDict": self._commonParamDict, 
             "taskSpecificParamDict": self._taskSpecificParamDict}
        return dumpsPretty(d)
        
        