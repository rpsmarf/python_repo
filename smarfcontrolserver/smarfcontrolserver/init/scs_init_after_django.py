
'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 11, 2014

    @author: rpsmarf
'''

import logging
#import time
from smarfcontrolserver.init import ice_server
from smarfcontrolserver.init.envvar import SCS_ENV_VAR_ICE_PORT,\
    scsGetEnvVarInt, SCS_ENV_VAR_MIN_HEARTBEAT_INTERVAL, scsGetEnvVarFloat,\
    SCS_ENV_VAR_MIN_TIME_AFTER_REMOTE_AGENT_ADD_BEFORE_ALARM,\
    SCS_ENV_VAR_RESOURCE_CAP_POLL_TIME, SCS_ENV_VAR_TMP_EXPIRY_EXPIRY_TIME,\
    SCS_ENV_VAR_TMP_EXPIRY_POLL_TIME, SCS_ENV_VAR_TASK_DELETE_POLL_TIME,\
    SCS_ENV_VAR_QUEUED_TASK_POLL_TIME, \
    SCS_ENV_VAR_METRIC_SEND_TIME, SCS_ENV_VAR_CLOUD_STATUS_POLL_TIME,\
    SCS_ENV_VAR_CLOUD_ROGUE_SERVER_POLL_TIME
from smarfcontrolserver.remoteagentif.heartbeats import ScsHeartbeatIf
import os
from smcommon.globals.sm_global_context import smGlobalContextInit
from sm_controller.api_task import SmTaskUtilsAsyncResps
from smcommon.globals.sm_copy_controller import SmCopyController
from smcommon.globals.remote_agent_factory import smRemoteAgentAsyncOperationFactory
from smcommon.task_runners.mock_async_ops_server import SmMockAsyncOpsServer,\
    SmMockOpHandlerResponder
from sm_controller.signal_handlers import smTriggerSignalInstall
from smarfcontrolserver.pollers.TmpFilePurger import ScsTmpFilePurger
from smarfcontrolserver.pollers.TaskAgeOut import ScsTaskAgeOut
from smarfcontrolserver.pollers.ResourceUsageUpdater import ScsResourceCapUpdater
from smcommon.crypto.sm_crypto import SmCryptoSigner, SmTokenMaker  
from smcommon.globals.envvar import SM_ENV_VAR_COMMON_KEY_DIR, smCommonGetEnvVar,\
    SM_ENV_VAR_COMMON_METRIC_DEST_HOST
from sm_controller import api
from smarfcontrolserver.utils.mailer import ScsMailer, scsSetGlobalMailer
from smarfcontrolserver.pollers.QueuedTaskStarter import ScsQueuedTaskStarter
from smarfcontrolserver.pollers.scs_metric_callbacks import scsMetricCallbacksRegister
from smcommon.metrics.metrics_sender import SmMetricSender
from smarfcontrolserver.pollers.cloud_state_poller import ScsCloudStatePoller
from smarfcontrolserver.pollers.cloud_rogue_server_poller import ScsCloudRogueServerPoller
from sm_controller.api_cloud import scsApiCloudServerUsable
from sm_controller.api_extract import scsApiExtractRestartAll
 
logger = logging.getLogger(__name__)
smTriggerSignalInstall()


def scsInitAfterDjango():
    try:
        logger.info("Environment variables:") 
        
        for k, v in sorted(os.environ.items()):
            logger.info(k + " = " + v)
        logger.info("End of environment variables")
    
        # Create and start metrics sender if configured
        logger.info("Setting up metricsSender to %s", smCommonGetEnvVar(SM_ENV_VAR_COMMON_METRIC_DEST_HOST))  
        metricsSender = SmMetricSender(scsGetEnvVarInt(SCS_ENV_VAR_METRIC_SEND_TIME))
        scsMetricCallbacksRegister(metricsSender)

        metricsSender.start()  
    
        copyController = SmCopyController([])
        smGlobalContextInit(copyController)
    
        logger.info("Global context init done (ICE created)")
            
        scsIceInterface = ice_server.ScsIceInterface(scsGetEnvVarInt(SCS_ENV_VAR_ICE_PORT))
        
        heartbeatIf = ScsHeartbeatIf(scsGetEnvVarFloat(SCS_ENV_VAR_MIN_HEARTBEAT_INTERVAL),
                                     scsGetEnvVarFloat(SCS_ENV_VAR_MIN_TIME_AFTER_REMOTE_AGENT_ADD_BEFORE_ALARM),
                                     scsGetEnvVarFloat(SCS_ENV_VAR_MIN_HEARTBEAT_INTERVAL) / 3)
        logger.info("Starting ICE server...!")
        asyncRespHandler = SmTaskUtilsAsyncResps()
        scsIceInterface.startIceServer(heartbeatIf, asyncRespHandler)
        
        # Setup mock responder, in case it is used 
        mockRaServer = SmMockAsyncOpsServer(asyncRespHandler)
        mockRaServer.handlerClass = SmMockOpHandlerResponder
        smRemoteAgentAsyncOperationFactory.setMockAsyncOpsServer(mockRaServer)

        logger.info("Starting heartbeat server...!")
        heartbeatIf.start()
        
        # Starting temp file purger
        purger = ScsTmpFilePurger(scsGetEnvVarFloat(SCS_ENV_VAR_TMP_EXPIRY_POLL_TIME),
                                  scsGetEnvVarFloat(SCS_ENV_VAR_TMP_EXPIRY_EXPIRY_TIME))
        purger.start()
        
        # Starting resource usage monitor 
        resourceUpdater = ScsResourceCapUpdater(scsGetEnvVarFloat(SCS_ENV_VAR_RESOURCE_CAP_POLL_TIME))
        resourceUpdater.start()
        
        # Starting task age out poller
        taskAgeOut = ScsTaskAgeOut(scsGetEnvVarFloat(SCS_ENV_VAR_TASK_DELETE_POLL_TIME))
        taskAgeOut.start()
        
        privateKeyPath = os.path.join(smCommonGetEnvVar(SM_ENV_VAR_COMMON_KEY_DIR), "guac_key.private")
        signer = SmCryptoSigner(privateKeyPath, None)
        api.scsGuacamoleTokenMaker = SmTokenMaker(signer)

        scsSetGlobalMailer(ScsMailer())

        queuedTaskStarter = ScsQueuedTaskStarter(scsGetEnvVarFloat(SCS_ENV_VAR_QUEUED_TASK_POLL_TIME))
        queuedTaskStarter.start()
        
        cloudStatePoller = ScsCloudStatePoller(scsGetEnvVarFloat(SCS_ENV_VAR_CLOUD_STATUS_POLL_TIME),
                                               scsApiCloudServerUsable)
        cloudStatePoller.start()
        
        cloudRogueServerPoller = ScsCloudRogueServerPoller(scsGetEnvVarFloat(SCS_ENV_VAR_CLOUD_ROGUE_SERVER_POLL_TIME))
        cloudRogueServerPoller.start()
        
        # Restart any extraction which were running when the server went down
        scsApiExtractRestartAll()
    except:
        logger.error("Initialization error!", exc_info=True)
