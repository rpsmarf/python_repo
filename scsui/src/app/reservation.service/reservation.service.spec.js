describe('Reservation service functionality (reservation.service)', function(){

	var smarfurl, Restangular, $httpBackend, SmReservation, reservations, jsonObj, json = 'src/app/reservation.service/reservation.service.spec.json';
	var SmUser, $rootScope, SmTaskCreate, $state, $interval, $scope;
	beforeEach(module('rpsmarf.config'));
	beforeEach(module('rpsmarf.user.service'));
	beforeEach(module('rpsmarf.reservation.service'));
	beforeEach(module('rpsmarf.task.create.service'));
	beforeEach(module('restangular'));
	beforeEach(module('confirm/confirm.tpl.html'));
	beforeEach(module('alert/alert.tpl.html'));
	beforeEach(module('tools/tools.tpl.html'));
	beforeEach(module('tools/tools.infopanel.tpl.html'));
	beforeEach(module('taskstart/taskstart.tpl.html'));
	beforeEach(module('ui.router'));
	beforeEach(module('rpsmarf.tools'));

	beforeEach(inject(function(SMARF_URL,_Restangular_,$controller, _$httpBackend_, _SmReservation_,_SmUser_ , DEBUG, _$rootScope_, _SmTaskCreate_, _$state_, _$interval_){
		smarfurl = SMARF_URL+'/scs/';
		Restangular = _Restangular_;
		Restangular.configuration.baseUrl = smarfurl;
		Restangular.configuration.encodeIds = false;
		$httpBackend = _$httpBackend_;
		SmUser = _SmUser_;
		SmReservation = _SmReservation_;

		jsonObj = JSON.parse(__html__[json]);
		SmUser = _SmUser_;
		SmUser.setProperty('id',2);
		$rootScope = _$rootScope_;
		$scope = $rootScope.$new();
		SmTaskCreate = _SmTaskCreate_;
		$state = _$state_;
		$interval = _$interval_;

	}));

	it('should get task types for the reservation', inject(function(){

		$httpBackend.expect('GET', smarfurl+'resource/6/').respond(200, jsonObj.resource);
		$httpBackend.expect('GET', smarfurl+'resource_type_task_type/?resource_type__id=3').respond(200,jsonObj.rt3s);
		$httpBackend.expect('GET', smarfurl+'task_type/5/?extForUser=2&sortForUser=2').respond(200,jsonObj.task_types);

		SmReservation.setReservation(jsonObj.reservations[0]);

		SmReservation.getTaskTypes();

		$httpBackend.flush();

	}));

	it('should save the reservation parameters', inject(function(){
		expect(SmReservation.areReservationParamsSaved()).toBe(false);
		expect(SmReservation.getReservationParameters()).toBeFalsy();
		SmReservation.saveReservationParameters({test:true});
		expect(SmReservation.areReservationParamsSaved()).toBe(true);
		expect(SmReservation.getReservationParameters()).toBeTruthy();
		SmReservation.reset();
		
		expect(SmReservation.areReservationParamsSaved()).toBe(false);
		expect(SmReservation.getReservationParameters()).toBeFalsy();

	}));


});