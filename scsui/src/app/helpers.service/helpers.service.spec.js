describe('Helper functions (helpers.service)', function(){

	var smarfurl, Restangular, $httpBackend, SmReservation, reservations, SmHelpers;
	var SmUser, $rootScope, SmTaskCreate, $state, $interval, $scope;
	beforeEach(module('rpsmarf.config'));
	beforeEach(module('rpsmarf.user.service'));
	beforeEach(module('rpsmarf.reservation.service'));
	beforeEach(module('rpsmarf.helpers.service'));
	beforeEach(module('rpsmarf.task.create.service'));
	beforeEach(module('restangular'));
	beforeEach(module('confirm/confirm.tpl.html'));
	beforeEach(module('alert/alert.tpl.html'));
	beforeEach(module('ui.router'));

	beforeEach(inject(function(SMARF_URL,_Restangular_,$controller, _$httpBackend_, _SmReservation_,_SmUser_ , DEBUG, _$rootScope_, _SmTaskCreate_, _$state_, _$interval_, _SmHelpers_, _$window_){
		smarfurl = SMARF_URL+'/scs/';
		Restangular = _Restangular_;
		Restangular.configuration.baseUrl = smarfurl;
		Restangular.configuration.encodeIds = false;
		$httpBackend = _$httpBackend_;
		SmUser = _SmUser_;
		SmReservation = _SmReservation_;
		SmHelpers = _SmHelpers_;
		SmUser = _SmUser_;
		SmUser.setProperty('id',2);
		$rootScope = _$rootScope_;
		$scope = $rootScope.$new();
		SmTaskCreate = _SmTaskCreate_;
		$state = _$state_;
		$interval = _$interval_;
		$window = _$window_;

	}));

	
	it('should perform the right actions when a start task fails (handleErrorOnTaskStart)', inject(function(){
		//Perform 409 status code tests
		angular.element('body').children().remove();
		var task = {taskName:'Test task', toolName: '3-Dimensional Line Generation Tool'}, error = {status: 409, data: {taskInfo: {taskId: 45, interactive: false}, status:'reserved', reservation: {end_time: '2015-06-08T17:51:49Z'}}};
		spyOn(SmTaskCreate, 'restartTaskWithQueueing').andCallFake(function(){
			return {then:function(cb){ cb();}};
		});
		spyOn($state, 'go');
		SmHelpers.handleErrorOnTaskStart(error, task);
		$rootScope.$apply();
		angular.element('.modal-footer .btn-primary').click();
		$rootScope.$apply();

		expect(SmTaskCreate.restartTaskWithQueueing).toHaveBeenCalledWith(45, false);
		expect($state.go).toHaveBeenCalledWith('home');

		error.data.status = 'cancelling';
		

		SmHelpers.handleErrorOnTaskStart(error, task,$scope);
		$httpBackend.expect('GET', smarfurl+'task/45/?action=start').respond(409);
		$interval.flush(5000);
		$httpBackend.flush();
		$httpBackend.expect('GET', smarfurl+'task/45/?action=start').respond(409);
		$interval.flush(5000);
		$httpBackend.flush();

		$httpBackend.expect('GET', smarfurl+'task/45/?action=start').respond(200);
		$interval.flush(5000);

		spyOn($interval,'cancel');
		spyOn($scope,'$emit');
		$httpBackend.flush();
		expect($interval.cancel).toHaveBeenCalled();
		expect($scope.$emit).toHaveBeenCalled();

		error.data.status = 'inuse';

		ret = SmHelpers.handleErrorOnTaskStart(error, task,$scope);
		$rootScope.$apply();
		angular.element('.modal-footer .btn-primary').click();
		$rootScope.$apply();

		expect(SmTaskCreate.restartTaskWithQueueing).toHaveBeenCalledWith(45,false);

		$httpBackend.expect('DELETE', smarfurl+'task/45/').respond(200);
		ret = SmHelpers.handleErrorOnTaskStart(error, task,$scope);
		$rootScope.$apply();
		angular.element('.modal-footer .btn-default').click();
		$rootScope.$apply();

		$httpBackend.flush();

	}));

	it('should perform the right calls when completing folder download', inject(function(){
		var info = {resourceId: 120, zipFilePath: ".rpsmarf/tmp/8329c3ca-1f03-4351-8984-2fddc8a18514.zip"};
		spyOn($window, 'open');
		//$httpBackend.expect('GET',smarfurl+'resource/120/.rpsmarf/tmp/8329c3ca-1f03-4351-8984-2fddc8a18514.zip/download/').respond(200);
		//$httpBackend.expect('DELETE', smarfurl+'resource/120/.rpsmarf/tmp/8329c3ca-1f03-4351-8984-2fddc8a18514.zip/file/').respond(200);
		SmHelpers.completeFolderDownload(info);
		//$httpBackend.flush();
		expect($window.open).toHaveBeenCalledWith(smarfurl+'resource/120/.rpsmarf/tmp/8329c3ca-1f03-4351-8984-2fddc8a18514.zip/download/');
	}));


});