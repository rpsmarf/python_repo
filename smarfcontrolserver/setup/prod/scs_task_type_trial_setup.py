import json
import sys
from scripts.scs_obj_common import scsScriptGetDefaultUrl
import os
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath, scsGetToJson
from scripts.scs_obj_create import scsCreateObjectPath, scsModifyObject

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

# This allows us to run local tools on a non-www node
myHost = "www.rpsmarf.ca"
if 'SMARF_MY_HOST' in os.environ:
    myHost = os.environ['SMARF_MY_HOST']
    
root_path = scsGetObjectPath("user", "username=rpsmarf")
all_users_group_path = scsGetObjectPath("group", "name=all_users")
userList = [scsGetObjectPath("user", "username=rpsmarf"),
            scsGetObjectPath("user", "username=andrewmcgregor"),
            scsGetObjectPath("user", "username=navdeepk"),
            scsGetObjectPath("user", "username=bnandy"),
            scsGetObjectPath("user", "username=sikharesh"),
            scsGetObjectPath("user", "username=marc_sthilaire"),
            ]


eval_community_tag_path = scsCreateObjectPath("tag", 
                                              {"tag_name": "community:eval"})
eval_community_path = scsCreateObjectPath("community", 
                                     {"name": "RP-SMARF Evaluation",
                                      "name_key": "eval",
                                      "description": "This community contains a number of " +
                                      "tools and data resources to allow users to try out the " +
                                      "capabilities of the RP-SMARF platform",
                                      "owner": root_path,
                                      "tag": eval_community_tag_path,
                                      "imageUrl": "https://www.rpsmarf.ca/icons-scs/evalCommunityIcon.png",
                                      })
for userName in ["bnandy", "sikharesh", "marc_sthilaire"]:
    user = scsGetObjectPath("user", "username=" + userName)
    scsModifyObject("/scs/user_setting/" + str(user) + "/", 
                    {"performSetupOnNextLogin": False, 
                     "community": eval_community_path})

scsCreateObjectPath("news_item", 
                    {"owner": root_path,
                     "headline": "Welcome to the RP-SMARF Evaluation Community",
                     "body": "You are a member of the RP-SMARF Evaluation community " +
                     "which exists to allow new users to explore the capabilities of the " +
                     "RP-SMARF platform.",
                     "community": eval_community_path,
                     })

mock_data_container_path = scsGetObjectPath("container", "name_key=mock_data_container")
rt_cloud_files_data_path = scsGetObjectPath("resource_type", "name_key=generic_storage")

TASK_TYPE_SHELL_LINES_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", 
                                      "name": "Argument", 
                                      "value": "/opt/smarf-demos/demos/lines/lines.sh"},
                                     {"type": "resource", 
                                      "name": "File of control parameters", 
                                      "pathType": "file", 
                                      "input_id": 1, 
                                      "direction": "input", 
                                      "role": "src", 
                                      "name": "Data to analyze",
                                      "description": "This specifies the folder of data to submit for analysis"},
                                     {"type": "resource", 
                                      "name": "Output folder containing the generated movie", 
                                      "pathType": "folder", 
                                      "input_id": 2, 
                                      "direction": "output", 
                                      "role": "dest", 
                                      "name": "Output folder",
                                      "description": "This specified the location to which the output data will be saved"},
                                     ],
                            "outputProgress": {"outputArg": 2, "maxSize": 296186},
                            "computeResourceRole": "compute"
                       }
tt_path = scsCreateObjectPath("task_type", {"name": "3-Dimensional Line Generation Tool",
                                            "name_key": "mock_tool",
                                            "description": "Mock Tool: " +
                                            " This tool performs a 3 dimensional random line " + 
                                            "walk to generate attractively mesmerizing movies.",
                                            "code_module": "smcommon.task_runners.task_runner_shell", 
                                            "code_classname": "SmTaskRunnerShell", 
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_SHELL_LINES_CFG_DICT)})
scsCreateObjectPath("task_type_tag", {"tag": eval_community_tag_path, "task_type": tt_path})
for userPath in userList:
    scsGetToJson(tt_path + "setperm/?action=assign&user=" + userPath + "&perm=x")

rt_mock_data_path = scsCreateObjectPath("resource_type", {"name": "tool_data_source", 
                                                     "nature": "data",
                                                     "description": "This is a source of mock data"})
rt_mock_compute_path = scsCreateObjectPath("resource_type", 
                                           {"name": "Computational Resource", 
                                            "nature": "compute",
                                            "description": 
                                            "This is a computing resource which can run "
                                            "the 3-Dimensional Line Generation Tool"})

scsCreateObjectPath("resource_type_task_type", {"role": "src", "task_type": tt_path, "resource_type": rt_mock_data_path,
                                                "description": "This is the source of data for the mock tool",
                                                "nature": "data",
                                                "name": "Mock data source",
                                                "name_key": "mock_data_source"})
scsCreateObjectPath("resource_type_task_type", {"role": "dest", "task_type": tt_path, "resource_type": rt_cloud_files_data_path,
                                                "description": "This is the output file " + 
                                                "for the movie generated by the mock tool",
                                                "nature": "data",
                                                "name": "Analysis output folder",
                                                "name_key": "mock_data_output"})
scsCreateObjectPath("resource_type_task_type", {"role": "compute", "task_type": tt_path, "resource_type": rt_mock_compute_path,
                                                "description": "This is the compute resource for the mock tool",
                                                "nature": "compute",
                                                "name": "Mock compute resource",
                                                "name_key": "mock_compute_resource"})


res_path = scsCreateObjectPath("resource", {"name": "Line Generation Data Set", 
                                            "name_key": "line_gen_data_set",
                                            "resource_type": rt_mock_data_path,
                                            "owner": root_path,
                                            "container": mock_data_container_path,
                                            "parametersJson": json.dumps({"folder": "mock_data_d/"}),
                                            "description": "This is the repository of mock data 'C'"
                                            })
scsCreateObjectPath("resource_tag", {"tag": eval_community_tag_path, "resource": res_path})
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=r")

res_path = scsCreateObjectPath("resource", {"name": "Line Generator Computational Resource", 
                                            "name_key": "testab1_compute",
                                            "resource_type": rt_mock_compute_path,
                                            "owner": root_path,
                                            "parametersJson": json.dumps({}),
                                            "container": mock_data_container_path,
                                            "description": "This is the DAIR-based computation "
                                            "resource to run the 3-Dimensional Line Generation Tool"
                                            }
                               )
scsCreateObjectPath("resource_tag", {"tag": eval_community_tag_path, "resource": res_path})
scsGetToJson(res_path + "setperm/?action=assign&group=" + all_users_group_path + "&perm=x")

print("Task type 'lines' objects setup.")

carleton_container_path = scsGetObjectPath("container", "name_key=mock_data_container") 
rt_cloud_files_data_path = scsGetObjectPath("resource_type", "name_key=generic_storage")


TASK_TYPE_VIDEO_STREAM_CFG_DICT = {
                            "args": [ 
                                     {"type": "string",
                                      "name": "Argument", 
                                      "value": "/opt/smarf-demos/demos/video_tool/vlcsnapshot.sh"},
                                     {"type": "resource", 
                                      "name": "Input stream of video data", 
                                      "pathType": "folder", 
                                      "input_id": 1, 
                                      "direction": "input", 
                                      "role": "vsrc", 
                                      "name": "Data to analyze",
                                      "description": "This specifies the folder of data to submit for analysis"},
                                     {"type": "resource", 
                                      "name": "Folder containing snapshot from the video", 
                                      "pathType": "folder", 
                                      "input_id": 2, 
                                      "direction": "output", 
                                      "role": "vdest", 
                                      "name": "Output folder",
                                      "description": "This specified the location to which the output data will be saved"},
                                     ],
                            "computeResourceRole": "vcompute",
                            "interactive": True
                       }
tt_video_path = scsCreateObjectPath("task_type", {"name": "Video Snapshot Tool",
                                                  "name_key": "video_snapshot_tool",
                                                  "description": "" +
                                                  " This tool streams a video to an interactive video viewing application." +
                                                  "The user can take snapshots of the video which are then copied back to " +
                                                  "the folder he defines",
                                                  "code_module": "smcommon.task_runners.task_runner_shell", 
                                                  "code_classname": "SmTaskRunnerShell", 
                                                  "owner": root_path,
                                                  "configurationJson": json.dumps(TASK_TYPE_VIDEO_STREAM_CFG_DICT)})
scsCreateObjectPath("task_type_tag", {"tag": eval_community_tag_path, "task_type": tt_video_path})
for userPath in userList:
    scsGetToJson(tt_video_path + "setperm/?action=assign&user=" + userPath + "&perm=x")

rt_video_stream_path = scsCreateObjectPath("resource_type", {"name": "Video Data Source", 
                                                             "name_key": "video_data_source",
                                                             "nature": "streamin",
                                                             "configuration": json.dumps({"pipeFolder": "/tmp"}),
                                                             "description": "This is a source of video data " +
                                                             "for the mock streaming application",
                                                             "code_module": "smcommon.task_runners.task_runner_stream",
                                                             "code_classname": "SmTaskRunnerStreamSource"})

scsCreateObjectPath("resource_type_task_type", {"role": "vsrc",
                                                "task_type": tt_video_path, 
                                                "resource_type": rt_video_stream_path,
                                                "nature": "streamin",       
                                                "name": "Video source stream",
                                                "name_key": "video_source_stream",                                         
                                                "description": "This is the source of data for a video " +
                                                "tool"})

VIDEO_STREAM1_RSRC_CFG = {"type": "stream",
                          "args": [{"type": "resource", "direction": "output", "role": "stream"},
                                   {"type": "string", "value": "-interval"},
                                   {"type": "float", "value": 0.00},
                                   {"type": "string", "value": "-objectsToSendPerInterval"},
                                   {"type": "int", "value": 10000},
                                   {"type": "string", "value": "-pattern"},
                                   {"type": "string", "value": "file_once"},
                                   {"type": "string", "value": "-patternFile"},
                                   {"type": "string", "value": "/opt/smarf-demos/demos/video1/Wildlife_512kb.mp4"},
                                   ]
                          }

res_path = scsCreateObjectPath("resource", {"name": "Video Stream 1", 
                                            "name_key": "demos_data_stream_1",
                                            "resource_type": rt_video_stream_path,
                                            "owner": root_path,
                                            "container": carleton_container_path,
                                            "parametersJson": json.dumps(VIDEO_STREAM1_RSRC_CFG),
                                            "description": "This is the video stream '1'"
                                            })
scsCreateObjectPath("resource_tag", {"tag": eval_community_tag_path, "resource": res_path})
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=r")

VIDEO_STREAM2_RSRC_CFG = {"type": "stream",
                          "args": [{"type": "resource", "direction": "output", "role": "stream"},
                                   {"type": "string", "value": "-interval"},
                                   {"type": "float", "value": 0.00},
                                   {"type": "string", "value": "-objectsToSendPerInterval"},
                                   {"type": "int", "value": 10000},
                                   {"type": "string", "value": "-pattern"},
                                   {"type": "string", "value": "file_once"},
                                   {"type": "string", "value": "-patternFile"},
                                   {"type": "string", "value": "/opt/smarf-demos/demos/video2/Bandit.mp4"},
                                   ]}

res_path = scsCreateObjectPath("resource", {"name": "Video Stream 2", 
                                            "name_key": "maj10_demos_data_stream",
                                            "resource_type": rt_video_stream_path,
                                            "owner": root_path,
                                            "container": carleton_container_path,
                                            "parametersJson": json.dumps(VIDEO_STREAM2_RSRC_CFG),
                                            "description": "This is the video stream '2'"
                                            }
                               )
scsCreateObjectPath("resource_tag", {"tag": eval_community_tag_path, "resource": res_path})
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=r")

rt_video_compute_path = scsCreateObjectPath("resource_type", 
                                            {"name": "Video Processor", 
                                             "name_key": "video_processor",
                                             "nature": "compute",
                                             "description": "This is a resource which can run the video " +
                                             "playing application"
                                             })
scsCreateObjectPath("resource_type_task_type", 
                    {"role": "vcompute", 
                     "task_type": tt_video_path, 
                     "resource_type": rt_video_compute_path,
                     "nature": "compute",
                     "name": "Video compute",
                     "name_key": "video_compute",                                         
                     "description": "This is the compute resource for the video capture tool"})

res_path = scsCreateObjectPath("resource", {"name": "Video Runner at DAIR Alberta",
                                            "name_key": "stream_testab1_compute", 
                                            "resource_type": rt_video_compute_path,
                                            "owner": root_path,
                                            "container": carleton_container_path,
                                            "parametersJson": 
                                            json.dumps({"taskViewBaseUrl": 
                                                        "http://" + myHost + ":8080/guacamole" +
                                                        "/client.xhtml?id=c%2Fdefault"}),
                                            "description": "This is the DAIR-Alberta-based computation " + 
                                            "resource to run the video processing tool"
                                            }
                               )
scsCreateObjectPath("resource_tag", {"tag": eval_community_tag_path, "resource": res_path})
scsGetToJson(res_path + "setperm/?action=assign&group=" + all_users_group_path + "&perm=x")

# Destination Cloud Folders
scsCreateObjectPath("resource_type_task_type", {"role": "vdest", 
                                                "task_type": tt_video_path, 
                                                "nature": "data",
                                                "name": "Video Destination Folder",
                                                "name_key": "video_dest",                                         
                                                "resource_type": rt_cloud_files_data_path,
                                                "description": "This is the compute resource for the video capture tool"})

 
print("Task type 'stream' objects setup.")
