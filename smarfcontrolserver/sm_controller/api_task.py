'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 28, 2015

    @author: rpsmarf
'''

import logging
from sm_controller.models import SmModelTask, SmModelTaskResource,\
    SmModelReservation, SmModelScript, SmModelCloudServer, SmModelCloudAccount
from urllib.parse import urlparse, parse_qsl, urlunparse, urlencode
from smcommon.globals.remote_agent_factory import smRemoteAgentAsyncOperationFactory
from smarfcontrolserver.init.envvar import SCS_ENV_VAR_STDOUT_LIMIT_IN_DB,\
    SCS_ENV_VAR_STDERR_LIMIT_IN_DB, scsGetEnvVarInt, scsApiGetAuthOn
from smraicemsgs import SmRemAgAsyncOpResps  # @UnresolvedImport
import json
from smarfcontrolserver.init.metric_names import SCS_NUM_TASKS_FAILED_ON_REMOTE_AGENT,\
    SCS_NUM_TASKS_SUCCEEDED_ON_REMOTE_AGENT, SCS_NUM_TASKS_CANCELLED
from smcommon.task_runners.task_runner import TASK_STATE_SUCCESS
from smcommon.utils.date_utils import sm_date_now, smMakeDateStringForJson,\
    smMakeNewPath, smMakeDateString
from actstream.signals import action
from statsd.defaults.env import statsd
from sm_controller.api_base import HttpConflict409, scsCheckOkToRun,\
    HttpOk, HttpError422, HttpPreconditionFailed412,\
    scsMakeExecInfo, scsApiExtractorCallbackGet
from tastypie.exceptions import ImmediateHttpResponse
from smcommon.tasks.task_desc import SmTaskDesc
from smcommon.utils import json_utils
import sm_controller.api
from smcommon.globals.sm_global_context import smGlobalContextGet
from smcommon.file_access.data_access_desc import SmDataAccessDescriptor
import uuid
from sm_controller.api_base import scsGuacamoleTokenMaker
from datetime import timedelta
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_STATE_USABLE,\
    SCS_CLOUDIF_STATE_STOPPED, SCS_CLOUDIF_STATE_INUSE,\
    SCS_CLOUDIF_DYNAMIC_SERVER
from sm_controller.api_cloud import scsCloudServerStart

logger = logging.getLogger(__name__)


class SmTaskUtilsAsyncResps(SmRemAgAsyncOpResps):
    '''
    This class receives the callbacks (via the ICE server) for responses to 
    asynchronous commands issued to remote agents.  The callback is received
    and saved to the database for the UI to pickup on the next poll.
    '''
    def __init__(self):
        pass
    
    def progress(self, taskId, progressJson, current=None):
        logger.debug("For task %s, got progress %s", taskId, progressJson)
        if ("_" in taskId):
            logger.debug("Discarding progress because it is from a stream process")
            return
        elif taskId.startswith("extractor"):
            extract_callback = scsApiExtractorCallbackGet()
            if extract_callback is not None:
                extract_callback.progress(taskId, progressJson)
            else:
                logger.warning("Extractor response discarded because _smApiExtractorCallback is None")
            return
        try:
            task = SmModelTask.objects.get(pk=int(taskId))
            if task.state == "finished":
                logger.debug("Ignoring progress message for taskId %s because state==finished")
                return
        except:
            logger.debug("Task %d not found.  Ignoring progress", int(taskId))
            return
            
        task.progressJson = progressJson
        progDict = json.loads(progressJson)
        if "state" in progDict:
            task.state = progDict["state"]
        task.save()

    def done(self, taskId, status, respJson, current=None):
        logger.debug("For taskObject %s, got done with status=%s, respJson=%s", taskId, status, respJson)
        if ("_" in taskId):
            logger.debug("Discarding done because it is from a stream process")
            return
        elif taskId.startswith("extractor"):
            extract_callback = scsApiExtractorCallbackGet()
            if extract_callback is not None:
                extract_callback.done(taskId, status, respJson)
            else:
                logger.warning("Extractor response discarded because _smApiExtractorCallback is None")
            return
                   
        taskObject = SmModelTask.objects.get(pk=int(taskId))
        if taskObject.state == "finished":
            logger.debug("Ignoring done message for taskId %s because state==finished")
            return
        else:
            if taskObject.cloud_server is not None:
                logger.info("Marking cloud server %s as not in use (e.g. %s)",
                            str(taskObject.cloud_server), SCS_CLOUDIF_STATE_USABLE)
                taskObject.cloud_server.state = SCS_CLOUDIF_STATE_USABLE
                taskObject.cloud_server.save()
                
            action.send(taskObject.owner, verb="run_end", 
                        target=taskObject.computeResource, 
                        task_id=taskObject.id, status=status)

            taskObject.state = "finished"
            taskObject.end_time = sm_date_now()
            if status == TASK_STATE_SUCCESS:
                statsd.incr(SCS_NUM_TASKS_SUCCEEDED_ON_REMOTE_AGENT)
                taskObject.completion = "completedWithoutError"
            else:
                statsd.incr(SCS_NUM_TASKS_FAILED_ON_REMOTE_AGENT)
                taskObject.completion = "completedWithError"
            taskObject.completionReason = status
            taskObject.completionDescription = ""
            if taskObject.resultsJson == "":
                taskObject.resultsJson = respJson
            else:
                # Merge new fields into object
                newRespJsonDict = json.loads(respJson)
                oldRespJsonDict = json.loads(taskObject.resultsJson)
                oldRespJsonDict.update(newRespJsonDict)
                taskObject.resultsJson = json.dumps(oldRespJsonDict)
            
            # Update progress
            if taskObject.progressJson == "":
                progDict = {}
            else:
                progDict = json.loads(taskObject.progressJson)
            progDict["state"] = "finished"
            progDict["progress"] = 100
            taskObject.progressJson = json.dumps(progDict)
            taskObject.save()
            logger.debug("Saved task with new state %s", taskObject.state)
            logger.debug("Saved state is %s", SmModelTask.objects.get(id=taskObject.id).state)

        # Send cancel operations to any resource stream tasks started for this taskObject
        taskResourcesQuerySet = SmModelTaskResource.objects.filter(task=taskObject)
        for taskResource in taskResourcesQuerySet:
            resource = taskResource.resource
            resource_type = resource.resource_type
            nature = resource_type.nature
            
            if nature == "streamin" or nature == "streamout":
                role = taskResource.resource_type_task_type.role
                # Connect to agent
                agentUrlString = resource.container.agent.agentUrl
                agentUrl = urlparse(agentUrlString)                    
                logger.debug("Connecting to agent via %s (host %s, port %s)",
                             agentUrlString, agentUrl.hostname, agentUrl.port)
                try:
                    raOps = smRemoteAgentAsyncOperationFactory.makeAsyncOperationObject(agentUrl)
                
                    # Cancel stream task on agent
                    logger.debug("Sending cancel message to agent for resource stream...")
                    raOps.cancelOperation(str(taskId) + "_" + role)
                    logger.debug("Completed sending cancel message to agent for resource stream")
                except:
                    logger.debug("Exception when sending cancel to stream resource tasks")

    def taskOutput(self, taskId, outputType, outputData, current=None):
        logger.debug("For taskObject %s, got taskOutput with outputType=%s, outputData=%s", taskId, outputType, outputData)
        if ("_" in taskId):
            logger.debug("Discarding taskOutput because it is from a stream process")
            return
        elif taskId.startswith("extractor"):
            extract_callback = scsApiExtractorCallbackGet()
            if extract_callback is not None:
                extract_callback.taskOutput(taskId, outputType, outputData)
            else:
                logger.warning("Extractor response discarded because _smApiExtractorCallback is None")
            return
        taskObject = SmModelTask.objects.get(pk=int(taskId))
        if taskObject.state == "finished":
            logger.debug("Ignoring done message for taskId %s because state==finished")
            return
        if outputType == "stdout":
            maxLen = scsGetEnvVarInt(SCS_ENV_VAR_STDOUT_LIMIT_IN_DB)
        else:
            maxLen = scsGetEnvVarInt(SCS_ENV_VAR_STDERR_LIMIT_IN_DB)
        s = getattr(taskObject, outputType) + outputData
        if len(s) > maxLen:
            s = s[-maxLen:]
        
        setattr(taskObject, outputType, s)
        taskObject.save()
        

def scsApiTaskCheckForOtherTaskAlreadyRunning(computeResource):
        '''
        This is to check if there is another task running on this compute resource.  
        If so, the owner of this task is returned, otherwise None is returned
        '''
        tasksRunningQset = SmModelTask.objects.filter(computeResource=computeResource).\
            exclude(state="finished").exclude(state="init").exclude(state="queued")
        if tasksRunningQset.count() == 0:
            return None
        return tasksRunningQset[0]


def scsApiTaskCancelTask(taskObject):
    '''
    See the task state machine at 
    https://rpsmarf.atlassian.net/wiki/display/RPS/SMARF+Control+Server+%28SCS%29+Notes
    '''
    statsd.incr(SCS_NUM_TASKS_CANCELLED)
    myId = taskObject.id
    if taskObject.state == "finished":
        logger.debug("Skipping cancel at remote agent as the task is already finished")
        raise ImmediateHttpResponse(HttpOk("Task cancellation ignored because task is already finished",
                                    content_type="application/json"))
    
    ttConfigDict = json.loads(taskObject.task_type.configurationJson)
    computeRole = ttConfigDict["computeResourceRole"]
    
    if taskObject.state != "queued" and taskObject.state != "booting":
        for taskResource in SmModelTaskResource.objects.filter(task=taskObject):
            if taskResource.resource_type_task_type.role == computeRole:
                try:
                    computeContainerUrl = urlparse(taskResource.resource.container.agent.agentUrl)
                    # Connect to agent
                    raOps = smRemoteAgentAsyncOperationFactory.makeAsyncOperationObject(computeContainerUrl)
                    # Send cancel to agent
                    raOps.cancelOperation(str(myId))
                except:
                    logger.exception("Failure in cancel operation on remote agent.  Still marking task done.")
    taskObject.endTime = sm_date_now()
    taskObject.state = "finished"
    taskObject.completion = "completedWithError"
    taskObject.completionReason = "cancelled"
    taskObject.resultsJson = json.dumps({"code": "EOTHER",
                                         "description": "Task cancelled"})
    if taskObject.cloud_server is not None:
        taskObject.cloud_server
    taskObject.save()
           

def scsApiTaskCheckForReservations(computeResource, owner, runningTask, allowQueueing):
    '''
    This method is called from start task to check for reservations.
    There are several scenarios to check for:
    Assuming there is at least one reservation for "now" for this resource:
    - If the reservation is not for us, reject the request
    - If the reservation is for us and there is no task currently running, return OK
    - If the reservation is for us and a task which belongs to us is running ignore
            because this is caught by an earlier check
    - If the reservation is for us and a task which belongs to someone else is running
            cancel the existing task and fail with an indication that the current 
            status is canceling

    @return true if the task should be queued
    '''
    
    # Get reservations
    now = sm_date_now()
    resQset = SmModelReservation.objects.filter(resource=computeResource).\
        filter(start_time__lte=now).filter(end_time__gt=now)
    if resQset.count() == 0:
        return False
    
    res = resQset[0]
    if res.owner != owner:
        if allowQueueing:
            return True
        else:
            respDict2 = {
                "status": "reserved", 
                "reservation": {
                    "id": res.id, 
                    "owner": res.owner.username, 
                    "description": res.description, 
                    "start_time": smMakeDateStringForJson(res.start_time), 
                    "end_time": smMakeDateStringForJson(res.end_time)}}
            raise ImmediateHttpResponse(HttpConflict409(json.dumps(respDict2)))
        
    # If there is no running task, we are good to go
    if runningTask is None:
        return False
    
    # Reservation is ours, but there is another task running
    # If it is not ours, we cancel it, otherwise fall through and 
    # the conflict checker will report it
    if runningTask.owner != owner:
        scsApiTaskCancelTask(runningTask)
        if allowQueueing:
            return True
        else:
            respDict = {"status": "canceling",
                        "current_task": {
                                         "id": runningTask.id,
                                         "owner": runningTask.owner.username,
                                         }}
            raise ImmediateHttpResponse(HttpConflict409(json.dumps(respDict)))

    return False


def scsApiTaskCheckForAuth(computeResource, taskTypeObject, ttConfigDict, request):
    '''
    This routine checks if the requesting user has permission to run this task
    on this resource. 
    '''
    if not ttConfigDict.get("skipRunPermCheck") and request is not None:
        apiResource = sm_controller.api.SmApiResource()
        scsCheckOkToRun(apiResource, computeResource, request, 'x')
        apiTaskType = sm_controller.api.SmApiTaskType() 
        scsCheckOkToRun(apiTaskType, taskTypeObject, request, 'x')
            
                        
def scsApiTaskCheckForQueueing(taskObject, computeResource, taskTypeObject, ttConfigDict, allowQueueing):
    '''
    This routine is used for tasks which are not running on dynamic cloud servers.
    If checks if the compute resource has a reservation or is currently in use.
    @return: a tuple of (queueTask, http_response).  The queueTask is true if the task
    should go into queued state because it cannot run now.  If the http_response is not 
    "None" then the response should be returned immediately.  This is used for example
    when the resource is not available and allowQueuing is False.
    '''
    queueTask = False
    if not ttConfigDict.get("skipRunPermCheck"):
        # Check for a task already running on this computeResource
        runningTask = scsApiTaskCheckForOtherTaskAlreadyRunning(computeResource)
    
        # Check for reservations which might allow/disallow this
        try:
            queueTask = scsApiTaskCheckForReservations(computeResource, taskObject.owner, runningTask, allowQueueing)
        except ImmediateHttpResponse as e:
            return queueTask, e.response
        
        if not queueTask:
            # If there is a running task and it belongs to this user, reject this task
            if runningTask is not None:
                if allowQueueing:
                    queueTask = True
                else:
                    d = json.dumps({
                            "status": "inuse", 
                            "current_task": {
                                "id": runningTask.id, 
                                "owner": runningTask.owner.username}})
                    logger.warning("Rejecting task because compute resource in use")
                    return queueTask, HttpConflict409(d)
    return queueTask, None

           
def scsApiTaskMakeDad(resourceObj, pathFromTaskParams, addFolderFromTaskParams=True):
    params = json.loads(resourceObj.parametersJson)
    if "folder" in params:
        folder = params["folder"]
    else:
        folder = ""
        
    if addFolderFromTaskParams:
        if isinstance(pathFromTaskParams, str):
            pathOrPathList = folder + pathFromTaskParams
        else:
            new_list = []
            for p in pathFromTaskParams:
                new_list.append(folder + p)
            pathOrPathList = new_list
    else:
        pathOrPathList = pathFromTaskParams
        
    dataContainer = sm_controller.api.SmApiResource.makeDataContainerFromResource(resourceObj, False)
    return SmDataAccessDescriptor(dataContainer, pathOrPathList)


def scsApiTaskRunTaskOnRemoteAgent(taskIdStr, taskDesc, computeResource):
    '''
    Run the task specified by the task descriptor on the compute resource specified.
    '''
    # Connect to agent
    agentUrl = urlparse(computeResource.container.agent.agentUrl)
    logger.debug("Connecting to agent via %s (host %s, port %s)", str(agentUrl), agentUrl.hostname, agentUrl.port)
    try:
        raOps = smRemoteAgentAsyncOperationFactory.makeAsyncOperationObject(agentUrl)
    
        # Start task on agent
        logger.debug("Sending start message to agent..")
        raOps.startOperation(taskIdStr, "task", taskDesc.toJson())
    except:
        raise ImmediateHttpResponse(HttpError422("Task start failed because agent at {} is down".format(
                                           computeResource.container.agent.agentUrl)))


def scsApiTaskStartStreamTask(taskObject, resource, role):
    '''
    Start a stream source/stream sink process on a resource.
    '''
    rtype_cfg_d = json.loads(resource.resource_type.configuration)  
    # Generate a name for the named pipe to use for this task.  We
    # will pass this named pipe to the main task for it to connect to
    pipePath = rtype_cfg_d["pipeFolder"] + "/" + str(uuid.uuid4())
    resourceParamDict = json.loads(resource.parametersJson)
    streamResourceTaskDesc = SmTaskDesc(resource.resource_type.code_module, resource.resource_type.code_classname)
    logger.debug("Adding DAD for role %s to pipe path %s", "stream", pipePath)
    streamDad = scsApiTaskMakeDad(resource, pipePath, False)
    streamResourceArgList = []
    for argInfoFromTt in resourceParamDict["args"]:
        if (argInfoFromTt["type"] == "resource"):
            streamResourceArgList.append({"type": "dad", "create": True, 
                                          "dad": streamDad.toDict(), 
                                          "direction": argInfoFromTt["direction"]})
        else:
            streamResourceArgList.append({"type": argInfoFromTt["type"], 
                                          "value": argInfoFromTt["value"]})
    
    streamResourceTaskDesc.addTaskParam("args", streamResourceArgList)

    scsApiTaskRunTaskOnRemoteAgent(str(taskObject.id) + "_" + role, streamResourceTaskDesc, resource)
    return streamDad


def scsApiTaskAddTaskViewInfoToTaskObject(taskObject, computeResource):
    '''
    For interactive tasks, this adds the URL to allow the browser to view
    the interactive output.
    '''
    if computeResource.parametersJson != "":
        resourceParamsDict = json.loads(computeResource.parametersJson)
        if "taskViewBaseUrl" in resourceParamsDict:
            baseUrl = urlparse(resourceParamsDict["taskViewBaseUrl"])
            paramList = parse_qsl(baseUrl.query)
            if scsGuacamoleTokenMaker is not None:
                expiryTime = sm_date_now() + timedelta(seconds=10)
                scsGuacamoleTokenMaker.addTokenToParamList("", paramList, expiryTime)
                
            fullUrl = urlunparse((baseUrl[0], baseUrl[1], baseUrl[2], baseUrl[3], urlencode(paramList), baseUrl[5]))
            taskObject.runningDisplayJson = json.dumps({"taskView": {"url": fullUrl, "layout": "newWindow"}})


def scsApiTaskPathExists(dad):
    copyCon = smGlobalContextGet().copyController
    fileAcc = copyCon.makeFileAccessor(dad.dataContainer)
    try:
        fileAcc.connectSession()
        try:
            flist = fileAcc.list(dad.path, 1, 1, None, True)
            if len(flist) == 1:
                return True
        except:
            pass
        return False
    finally:
        fileAcc.disconnectSession()
    

def scsApiTaskUsesCloudServer(computeResource):
    containerParamJson = computeResource.container.parametersJson
    if containerParamJson is not None and containerParamJson != "":
        param_dict = json.loads(containerParamJson)
        if SCS_CLOUDIF_DYNAMIC_SERVER not in param_dict:
            return False
        
        return param_dict[SCS_CLOUDIF_DYNAMIC_SERVER]
    
    return False


def scsApiTaskFindOrStartCloudServer(taskObject, computeResource):
    '''
    This method finds or creates a cloud server and starts it running if it is not
    currently running.
    @return: tuple of (queueTask, cloud_server) where queueTask is true if the task
    cannot run immediately and cloud_server is the server assigned for this task.
    '''
    logger.debug("In scsApiTaskFindOrStartCloudServer for task %s (compute resource %s)",
                 str(taskObject), str(computeResource))
    # If we already have a cloud server assigned, then we are using that
    # no matter what state it is in (or we will create many cloud servers 
    # because it takes time to boot a cloud server)
    currentCloudServer = taskObject.cloud_server
    if currentCloudServer is not None:
        if currentCloudServer.state == SCS_CLOUDIF_STATE_USABLE:
            return False, currentCloudServer
        else:
            return True, currentCloudServer
    
    # List cloud_servers associated with this computeResource which are "usable"
    cloud_servers = SmModelCloudServer.objects.filter(container=computeResource.container).\
        filter(state=SCS_CLOUDIF_STATE_USABLE).filter(owner=taskObject.owner)
    
    # If there is one or more, great use that
    if cloud_servers.count() > 0:
        cloud_server = cloud_servers.first()
        logger.debug("Using pre-existing 'usable' cloud server %s", str(cloud_server))
    else:
        # If there are none, then look for a stopped cloud_server and start it
        cloud_servers = SmModelCloudServer.objects.filter(container=computeResource.container).\
            filter(state=SCS_CLOUDIF_STATE_STOPPED)
        # If auth is on, use only "our" cloud servers
        if scsApiGetAuthOn():
            cloud_servers = cloud_servers.filter(owner=taskObject.owner)
    
        # If there is one or more, great use that
        if cloud_servers.count() > 0:
            cloud_server = cloud_servers.first()
            logger.debug("Using pre-existing 'stopped' cloud server %s", str(cloud_server))
        else:
            # If there are none, then create a cloud_server and start it
            cloud_account = computeResource.container.cloud_account
            if cloud_account is None:
                if scsApiGetAuthOn():
                    cloud_account_list = SmModelCloudAccount.objects.filter(owner=taskObject.owner)
                else:
                    cloud_account_list = SmModelCloudAccount.objects.all()
                if cloud_account_list.count() == 0:
                    logger.warning("No cloud account in container and none linked to the user")
                    raise Exception("No cloud account defined for user for cloud %s", 
                                    str(computeResource.container.cloud))
            cloud_server = SmModelCloudServer(owner=taskObject.owner,
                                              container=computeResource.container,
                                              cloud_account=cloud_account)
            cloud_server.save()
            logger.debug("Created new cloud server %s", str(cloud_server))
    
    taskObject.cloud_server = cloud_server
    if cloud_server.state == SCS_CLOUDIF_STATE_USABLE:
        return False, cloud_server

    logger.debug("Starting cloud server %s", str(cloud_server))
    scsCloudServerStart(cloud_server)
    return True, cloud_server


def scsApiTaskStartTask(taskObject, request, allowQueueing):
    taskTypeObject = taskObject.task_type
    if taskObject.state not in ["init", "queued", "booting"]:
        logger.error("Task %d already running or run, current state is %s ", taskObject.id, taskObject.state)
        return HttpPreconditionFailed412("Task already running or run, current state is " +
                                         taskObject.state)
        
    try:
        alteredPathDict = {}
        
        # Get related objects
        ttConfigDict = json.loads(taskTypeObject.configurationJson)
        logger.debug("Task type configuration dict is %s", json_utils.dumpsPretty(ttConfigDict))
        computeRole = ttConfigDict["computeResourceRole"]

        logger.debug("Task parameters: %s", taskObject.parametersJson)
        taskParamDict = json.loads(taskObject.parametersJson)
        taskInputIdDict = taskParamDict.get("input_ids")
        
        # Build task descriptor
        taskDesc = SmTaskDesc(taskTypeObject.code_module, taskTypeObject.code_classname)
        argList = []  # Eventual list of arguments
        roleToPath = {}
        
        # Go through each resource linked to this task.  Remember them in
        # roleToResourceDict by role.   Also start any stream processes required
        taskResourcesQuerySet = SmModelTaskResource.objects.filter(task=taskObject)
        roleToResourceDict = {}
        rolesToDadDict = {}
        for taskResource in taskResourcesQuerySet:
            resource = taskResource.resource
            role = taskResource.resource_type_task_type.role
            roleToResourceDict[role] = resource
            nature = resource.nature
            if nature == "streamin" or nature == "streamout":
                # Resource is a stream so we need to start the task to create this
                streamDad = scsApiTaskStartStreamTask(taskObject, resource, role)
                rolesToDadDict[role] = streamDad
        
        # Iterate through the arguments defined in the task type configuration string
        # and check if any resources are too full to allow the operation to proceed
        # or if permissions denied
        for argInfoFromTt in ttConfigDict.get("args"):
            if argInfoFromTt["type"] == "resource":
                resource = roleToResourceDict[argInfoFromTt["role"]]
                if argInfoFromTt["direction"] == "input":
                    access = 'r'
                else:
                    access = 'w'
                    if resource.capacityJson != "":
                        capDict = json.loads(resource.capacityJson)
                        if capDict.get("diskUsedState") == "full":
                            logger.warning("Task start rejected because output resource too full")
                            return HttpError422("Task start rejected because resource named " +
                                                resource.name + " is too full to proceed")
                if not ttConfigDict.get("skipRunPermCheck") and request is not None:
                    try:
                        logger.info("Checking for access " + access + " on resource " + str(resource))
                        scsCheckOkToRun(sm_controller.api.SmApiResource(), resource, request, access)
                        logger.info("Checking OK")
                    except:
                        # If we don't have read permission check if execute permission is enough and we have it
                        logger.info("Exception in check....")
                        if access == 'r' and ttConfigDict.get("executePermOnDataOk"):                            
                            logger.info("Rechecking for execute perm...")
                            scsCheckOkToRun(sm_controller.api.SmApiResource(), resource, request, "x")
                            logger.info("Execute perm check OK.")
                        else:
                            raise
                        
        # Iterate through the arguments defined in the task type configuration string
        # to build the task descriptor arguments
        for argInfoFromTt in ttConfigDict.get("args"):
            input_id = argInfoFromTt.get("input_id")
            # If this argument has an "input_id" then it is specified by a value
            # from the task object (e.g. it is specific to this task instance)
            if input_id is None:
                # Plug in constant value from tasktype.configurationJson field                    
                argType = argInfoFromTt["type"]
                if argType == 'script':
                    # Get script from script table
                    argValue = SmModelScript.objects.get(key=argInfoFromTt["value"]).value
                else:
                    argValue = argInfoFromTt["value"]
                argList.append({"type": argType, "value": argValue})
            else:
                # If the argument type is resource then we write a record
                # about this resource into the argList.  It will be resolved
                # to a file/folder path at the remote agent to allow input
                # or output copying to happen
                if argInfoFromTt["type"] == "resource":
                    # Fix the path to append a / if the asrg is a folder and no trailing / is present
                    if argInfoFromTt.get("pathType") == "folder":
                        p = taskInputIdDict[str(input_id)].get("path")
                        if p is not None and not p.endswith("/"):
                            taskInputIdDict[str(input_id)]["path"] += "/"
                
                    roleFromArgInfo = argInfoFromTt["role"]
                    # add reference to resource
                    if roleFromArgInfo in rolesToDadDict:
                        argList.append({"type": "stream", 
                                        "direction": argInfoFromTt["direction"],
                                        "dad": rolesToDadDict[roleFromArgInfo].toDict()})
                        # DAD already added when stream created
                    else:
                        requestedPath = taskInputIdDict[str(input_id)]["path"]
                        roleToPath[roleFromArgInfo] = requestedPath
                        dad = scsApiTaskMakeDad(roleToResourceDict[roleFromArgInfo], 
                                           requestedPath)
                        if ((argInfoFromTt["direction"] == "output") and
                                scsApiTaskPathExists(dad) and 
                                not ttConfigDict.get("preExistingOutputFolderOk")):
                            # Make a new path with a date tag to avoid overwriting old data
                            newPath = smMakeNewPath(requestedPath,
                                                    smMakeDateString(sm_date_now()))
                            dad = scsApiTaskMakeDad(roleToResourceDict[roleFromArgInfo], 
                                               newPath)
                            alteredPathDict[requestedPath] = newPath
                            
                            # Save the new path in the parametersJson to show where the 
                            # output went
                            taskInputIdDict[str(input_id)]["path"] = newPath
                            taskObject.parametersJson = json.dumps(taskParamDict)
                            taskObject.save()
                            
                        if "modifier" in argInfoFromTt:
                            dad.modifier = argInfoFromTt["modifier"]
                        arg = {"type": "dad", 
                               "direction": argInfoFromTt["direction"], 
                               "dad": dad.toDict()}
                        argList.append(arg)                            
                    logger.debug("Associating role %s with input id %d", 
                                 roleFromArgInfo, input_id)           
                else: # Substitute value from task.parametersJson
                    if "value" not in taskInputIdDict[str(input_id)]:
                        raise Exception("No item with name 'value' found for input id " + str(input_id))
                    argList.append({"type": argInfoFromTt["type"], 
                                    "value": taskInputIdDict[str(input_id)]["value"]})
                
        taskDesc.addTaskParam("args", argList)
        # Set up outputProgress to be calculated based on the taskType setting
        if "outputProgress" in ttConfigDict:
            taskDesc.addTaskParam("outputProgress", ttConfigDict.get("outputProgress"))
            
        # Build the execution information
        computeResource = roleToResourceDict[computeRole]
        
        # Check for authorization to run on this resource
        scsApiTaskCheckForAuth(computeResource, 
                               taskTypeObject, ttConfigDict, 
                               request)
        
        cloud_server = None
        if scsApiTaskUsesCloudServer(computeResource):
            queueTask, cloud_server = scsApiTaskFindOrStartCloudServer(taskObject, computeResource)
        else:    
            queueTask, response = scsApiTaskCheckForQueueing(taskObject, computeResource, 
                                                             taskTypeObject, ttConfigDict, allowQueueing)
            if response is not None:
                return response
        
        taskObject.end_time = sm_date_now()
        taskObject.computeResource = computeResource
        
        # Mark the task running or queued
        if taskObject.start_time > sm_date_now() or queueTask:
            if cloud_server is not None:
                taskObject.state = "booting"
            else:
                taskObject.state = "queued"
        else:
            taskObject.state = "starting"

            # Make the execution information dict
            execInfo = scsMakeExecInfo(computeResource, cloud_server)
            if "versioning" in ttConfigDict:
                execInfo["versioning"] = ttConfigDict["versioning"]
                if "version" in taskParamDict:
                    execInfo["version"] = taskParamDict.get("version")
            if "workingDir" in ttConfigDict:
                wDirDict = ttConfigDict["workingDir"]
                if wDirDict["type"] == "string":
                    execInfo["workingDir"] = {"type": "string", "value": wDirDict["value"]}
                else:
                    role = wDirDict["role"]
                    dad = scsApiTaskMakeDad(roleToResourceDict[role], 
                                       roleToPath[role])
                    dad.modifier = wDirDict["modifier"]
                    execInfo["workingDir"] = {"type": "resource", 
                                              "dad": dad.toDict()}
            taskDesc.addTaskParam("execInfo", execInfo)           
                         
            scsApiTaskAddTaskViewInfoToTaskObject(taskObject, computeResource)
                    
            resultsJsonDict = {"alteredPaths": alteredPathDict}
            taskObject.resultsJson = json.dumps(resultsJsonDict)

            action.send(taskObject.owner, verb="run", target=taskTypeObject)
            for resource in roleToResourceDict.values():
                if resource == computeResource:
                    action.send(taskObject.owner, verb="run_start", target=resource, task_id=taskObject.id)
                else:
                    action.send(taskObject.owner, verb="use", target=resource, task_id=taskObject.id)
                    
        taskObject.save()
        logger.debug("Set task state to %s with task desc %s", 
                     taskObject.state, taskDesc.toJson())
        if taskObject.state == "starting":
            if taskObject.cloud_server is not None:
                logger.info("Marking cloud server %s as 'inuse'", str(taskObject.cloud_server))
                taskObject.cloud_server.state = SCS_CLOUDIF_STATE_INUSE
                taskObject.cloud_server.save()
                
            logger.info("Starting task %d on remote agent", taskObject.id)
            scsApiTaskRunTaskOnRemoteAgent(str(taskObject.id), taskDesc, computeResource)       

        return HttpOk(json.dumps({"message": "Task state is now " + taskObject.state,
                                  "state": taskObject.state}))
    
    except Exception as e:
        taskObject.state = "finished"
        taskObject.end_time = sm_date_now()
        taskObject.completion = "completedWithError"
        taskObject.completionReason = "failedOnStart"
        taskObject.resultsJson = json.dumps({"code": "EOTHER",
                                            "description": "Task failed to start, exception was '{}'".format(str(e))})
        taskObject.save()
        logger.error("Error processing request - set state to finished!", exc_info=True)
        raise e 
    
