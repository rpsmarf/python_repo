#!/bin/bash
sudo apt-add-repository -y ppa:mc3man/trusty-media
sudo apt-get update
sudo apt-get -y install ffmpeg gstreamer0.10-ffmpeg
sudo apt-get -y install python-numpy python-scipy python-matplotlib mencoder
