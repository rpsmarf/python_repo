angular.module('rpsmarf.filesetpane', [

])

.directive('filesetpane',['$rootScope', function($rootScope){

	return {
		restrict: 'E',
		scope:{
			input: '='
		},
		templateUrl:  'filesetpane/filesetpane.tpl.html',
		link: function($scope, $element, $attrs){
			var wellElement = $element.find('div.well');
			$scope.entry = null;
			$scope.$on('sm-drag-start',function(){
				console.log($element.parent());
				console.log(angular.element($element).siblings('foldernavpane'));
				if(!$element.closest('.arg-content').find('foldernavpane').hasClass('sm-drag-src')){
					$element.data('droparea', false);
					return;
				}
				$element.data('droparea', true);
				console.log('drag-start scope filesetpane');
				wellElement.addClass('sm-drop-area');
				console.log($element.data('droparea'));
				
			});

			$scope.$on('sm-drag-end',function(){
				if(!$element.data('droparea')){
					return;
				}

				wellElement.removeClass('sm-drop-area');

			});

			wellElement.on('dragover', function(event){
				console.log($element.data('droparea'));
				if(!$element.data('droparea')){
					return;
				}
				var srcPane = angular.element('foldernavpane.sm-drag-src');
				var paneBody = srcPane.find('div.panel-body');
				var entry = paneBody.data('drag-object');

				if(entry.isDir){
					wellElement.removeClass('sm-drop-area');
					wellElement.addClass('sm-drop-forbid');
					if(event.originalEvent){
						event.originalEvent.preventDefault();
						event.originalEvent.dataTransfer.dropEffect = 'move';
					}
					return;
				}

				wellElement.removeClass('sm-drop-area');
				wellElement.addClass('sm-drop-allow');
				if(event.originalEvent){
					event.originalEvent.preventDefault();
					event.originalEvent.dataTransfer.dropEffect = 'move';
				}

			});

			wellElement.on('dragleave', function(event){
				if(!$element.data('droparea')){
					return;
				}
				
				if(wellElement.hasClass('sm-drop-allow')){
					wellElement.removeClass('sm-drop-allow');
				}
				if(wellElement.hasClass('sm-drop-forbid')){
					wellElement.removeClass('sm-drop-forbid');
				}
				wellElement.addClass('sm-drop-area');
				
			});

			wellElement.on('drop', function(event){
				if(!$element.data('droparea')){
					return;
				}

				console.log('droppped!');
				var e;
				if(event.originalEvent){
					e = event.originalEvent;

					e.preventDefault();
					e.stopPropagation();
				}

				//var data = e.dataTransfer.getData('text/html');
				var srcPane = angular.element('foldernavpane.sm-drag-src');

				var srcEntries = srcPane.isolateScope().entries;
				var srcResource = srcPane.isolateScope().resource;
				var dstResource = $scope.resource;

				var dstEntries = $scope.entries;
				var paneBody = srcPane.find('div.panel-body');

				
				var entry = paneBody.data('drag-object');
				if(entry.isDir){
					console.log('broadcasting drop reject');
					$rootScope.$broadcast('sm-drop-reject');
					return;
				}


				
				$rootScope.$broadcast('sm-drop-success');
				$scope.$apply(function(){
					$scope.entry = entry;
					$scope.input.inputValue = entry.name;
				});
				//$scope.smondrop({srce: srcEntries, dste: dstEntries, entryObj: entry, srcResource: srcResource, dstResource: dstResource});

			});

			$scope.$on('sm-drop-success', function(){
				if(!$element.data('droparea')){
					return;
				}
				console.log('drop-success filesetpane');
				wellElement.removeClass('sm-drop-area');
				wellElement.removeClass('sm-drop-allow');
				$element.data('droparea', false);
			});

			$scope.$on('sm-drop-reject', function(){
				if(!$element.data('droparea')){
					return;
				}
				console.log('drop-reject filesetpane');
				wellElement.removeClass('sm-drop-area');
				wellElement.removeClass('sm-drop-forbid');
				$element.data('droparea', false);
				alert('You can only place a file here.');
			});

			
			
		}
		
		
	};




}]);