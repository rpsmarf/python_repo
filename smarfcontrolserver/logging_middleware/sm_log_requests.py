'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 14, 2015

    @author: rpsmarf
'''
import logging
from tastypie.exceptions import ImmediateHttpResponse
import json

logger = logging.getLogger(__name__)


class RequestLoggerMiddleware(object):
    
    def process_request(self, request):
        if 'USER' in request.META:
            log_user = request.META['USER']
        else:
            log_user = "<unknown>"
        params = request.GET
        logger.debug('REQUEST: Path: %s, User %s, Params: %s',
                     request.path,
                     log_user,
                     json.dumps(params))
        return None 

    def process_exception(self, request, exception): 
        '''
        Only called if the view raised an exception
        '''
        if isinstance(exception, ImmediateHttpResponse):
            logger.debug('REQUEST DONE: Response code: %d, Path %s',
                         exception.response.status_code, request.path)
        else:
            logger.debug('REQUEST EXCEPTION: Path %s, Message: %s',
                         request.path,
                         exception)
        return None
    
    def process_response(self, request, response):
        if isinstance(response, dict):
            logger.debug('REQUEST DONE: Response code: %d, Path %s',
                         response["HTTPStatusCode"])
        else:
            logger.debug('REQUEST DONE: Response code: %d, Path %s',
                     response.status_code, request.path)
        return response