package net.sourceforge.guacamole.net.auth.smarf;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import org.glyptodon.guacamole.GuacamoleException;
import org.glyptodon.guacamole.GuacamoleServerException;
import org.glyptodon.guacamole.net.auth.simple.SimpleAuthenticationProvider;
import org.glyptodon.guacamole.net.auth.Credentials;
import org.glyptodon.guacamole.properties.FileGuacamoleProperty;
import org.glyptodon.guacamole.properties.GuacamoleProperties;
import org.glyptodon.guacamole.protocol.GuacamoleConfiguration;


public class SmarfAuthenticationProvider extends SimpleAuthenticationProvider {
       
	private Logger logger = LoggerFactory.getLogger(SmarfAuthenticationProvider.class);
	
	/**
     * Map of all known configurations, indexed by identifier.
     */
    private Map<String, GuacamoleConfiguration> configs;

    /**
     * The last time the configuration XML was modified, as milliseconds since
     * UNIX epoch.
     */
    private long configTime;

    /**
     * The filename of the XML file to read the user mapping from.
     */
    public static final FileGuacamoleProperty SMARFAUTH_CONFIG = new FileGuacamoleProperty() {

        @Override
        public String getName() {
            return "smarfauth-config";
        }

    };
    
    public static final FileGuacamoleProperty SMARFAUTH_PUBKEY = new FileGuacamoleProperty(){
    	
    	@Override
    	public String getName() {
    		return "smarfauth-pubkey";
    	}
    };

    //Note parameter "a" is not present in the actual parameter string
    //non encoded token: IkVEvju5+/btlRsgMqXmAU/TNVX5yc8DpUFX42oWZh3cWmCWyCYXmdrV4nYpvXbvjoigXw4x9F6UkhRuFCUD6TNQ2kwfsPqFyZ0ILsM/grgMOOBgLn5TgymJqVZ84JD0uW5tT0Mn7iGhXVK9S8wtRj2HsJuMAxPngICFsRcphJenhFuFUqSWgMiWT991DrHw++GxFNGF0ZzG91YKXIXSe5psNaMh88zWkmi8Igt/2MZ+7ilcMKyynQS2dwIMeBgtRB82qjaZ4YM3iR7scDr3+xDx9cjBptNYjyjGZaSoZKXkOPzrp3W7Z+XSTcl3cmQPNytssKRLNW58zCXYzN7LMA==
	static String testParamString = "a=b&config=majumdar-11&expiryTime=20200101T010101Z&smarfToken=IkVEvju5%2B%2FbtlRsgMqXmAU%2FTNVX5yc8DpUFX42oWZh3cWmCWyCYXmdrV4nYpvXbvjoigXw4x9F6UkhRuFCUD6TNQ2kwfsPqFyZ0ILsM%2FgrgMOOBgLn5TgymJqVZ84JD0uW5tT0Mn7iGhXVK9S8wtRj2HsJuMAxPngICFsRcphJenhFuFUqSWgMiWT991DrHw%2B%2BGxFNGF0ZzG91YKXIXSe5psNaMh88zWkmi8Igt%2F2MZ%2B7ilcMKyynQS2dwIMeBgtRB82qjaZ4YM3iR7scDr3%2BxDx9cjBptNYjyjGZaSoZKXkOPzrp3W7Z%2BXSTcl3cmQPNytssKRLNW58zCXYzN7LMA%3D%3D";
	
	
	
	/**
	 * Name and order of the parameters to expect from the scs server
	 */
	private static final String [] paramNames = new String[]{"config", "expiryTime", "smarfToken"};
	
	public Date parseDate(String input) throws java.text.ParseException {

		// NOTE: SimpleDateFormat uses GMT[-+]hh:mm for the TZ which breaks
		// things a bit. Before we go on we have to repair this.
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmssz");

		// this is zero time so we need to add that TZ indicator for
		if (input.endsWith("Z")) {
			input = input.substring(0, input.length() - 1) + "GMT-00:00";
		} else {
			int inset = 6;

			String s0 = input.substring(0, input.length() - inset);
			String s1 = input.substring(input.length() - inset, input.length());

			input = s0 + "GMT" + s1;
		}

		return df.parse(input);

	}
	
	/**
	 * @param expTime
	 * @param args
	 * @throws Exception
	 */
	public void checkParams(String param, Date expTime) throws Exception {
		// Find the "token=" string and split the string there
		int tokenOffset = param.indexOf("smarfToken=");
		String paramWoToken = param.substring(0, tokenOffset-1);
		String token = param.substring(tokenOffset + "smarfToken=".length());
		String expiryTimeString = paramWoToken.substring(param
				.indexOf("expiryTime=") + "expiryTime=".length(),paramWoToken.length());
		Date expiryDateInParam = parseDate(expiryTimeString);
		if (expTime.compareTo(expiryDateInParam) > 0)
			throw new Exception("Expiry time invalid");
		validateToken(paramWoToken, token);
	}
	
	private void validateToken(String paramWoToken, String token)
			throws Exception {
		byte[] decodedToken = DatatypeConverter.parseBase64Binary(token);
		PublicKey pubKey = getPublicKey(this.getPublicKeyFile());
		Signature signature = Signature.getInstance("SHA256withRSA");
		signature.initVerify(pubKey);
		byte[] data = paramWoToken.getBytes("utf-8");
		signature.update(data);
		if (!signature.verify(decodedToken))
			throw new Exception("Token did not verify");

	}
	
	private PublicKey getPublicKey(File f) throws Exception {

		FileInputStream fis = new FileInputStream(f);
		DataInputStream dis = new DataInputStream(fis);
		byte[] keyBytes = new byte[(int) f.length()];
		dis.readFully(keyBytes);
		dis.close();

		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}
	
	/**
     * Retrieves the configuration file, as defined within guacamole.properties.
     *
     * @return The configuration file, as defined within guacamole.properties.
     * @throws GuacamoleException If an error occurs while reading the
     *                            property.
     */
    @SuppressWarnings("deprecation")
	private File getConfigurationFile() throws GuacamoleException {
        return GuacamoleProperties.getRequiredProperty(SMARFAUTH_CONFIG);
    }
    
    @SuppressWarnings("deprecation")
	private File getPublicKeyFile() throws GuacamoleException {
        return GuacamoleProperties.getRequiredProperty(SMARFAUTH_PUBKEY);
    }
	
	public synchronized void init() throws GuacamoleException {

        // Get configuration file
        File configFile = getConfigurationFile();
        logger.debug("Reading configuration file: \"{}\"", configFile);

        // Parse document
        try {

            // Set up parser
            SmarfAuthConfigContentHandler contentHandler = new SmarfAuthConfigContentHandler();

            XMLReader parser = XMLReaderFactory.createXMLReader();
            parser.setContentHandler(contentHandler);

            // Read and parse file
            Reader reader = new BufferedReader(new FileReader(configFile));
            parser.parse(new InputSource(reader));
            reader.close();

            // Init configs
            configTime = configFile.lastModified();
            configs = contentHandler.getConfigs();

        }
        catch (IOException e) {
            throw new GuacamoleServerException("Error reading configuration file.", e);
        }
        catch (SAXException e) {
            throw new GuacamoleServerException("Error parsing XML file.", e);
        }

    }
	
    @Override
    public Map<String, GuacamoleConfiguration>
        getAuthorizedConfigurations(Credentials credentials)
        throws GuacamoleException {
    	
    	HttpServletRequest request = credentials.getRequest();
    	
    	if(request == null){
    		logger.info("No parameters present in request");
    		return null;
    	}
    	String paramString = "";
    	String configName = request.getParameterValues("config")[0];
    	
    	if(configName == null || configName.equals("")){
    		logger.info("Config parameter is required");
    		return null;
    	}
    	for(String param : paramNames){
    		if(param.equals("smarfToken")){
    			paramString += param+"="+request.getParameterValues(param)[0];
    		}
    		else{
    			paramString += param+"="+request.getParameterValues(param)[0]+"&";
    		}
    		
    	}
    	
    	logger.info("reconstructed paramString: " + paramString);
    	try{
    		this.checkParams(paramString, new Date());
    	}catch(Exception ex){
    		logger.info("Smarf authentication failed");
    		return null;
    	}
    	// Check mapping file mod time
        File configFile = getConfigurationFile();
        if (configFile.exists() && configTime < configFile.lastModified()) {

            // If modified recently, gain exclusive access and recheck
            synchronized (this) {
                if (configFile.exists() && configTime < configFile.lastModified()) {
                    logger.debug("Configuration file \"{}\" has been modified.", configFile);
                    init(); // If still not up to date, re-init
                }
            }

        }

        // If no mapping available, report as such
        if (configs == null)
            throw new GuacamoleServerException("Configuration could not be read.");

        Map<String, GuacamoleConfiguration> allowedConfig = new HashMap<String, GuacamoleConfiguration>();
        allowedConfig.put(configName, configs.get(configName));
        return allowedConfig;
    	    	     

    }
    
    public static void main(String[] args) throws ParseException {
    	SmarfAuthenticationProvider auth = new SmarfAuthenticationProvider();
		String pString;
		Date date;
		if (args.length == 0) {
			System.err
					.println("This tool checks a parameter string which is the first parameter on the command line\n"
							+ " If the string is not specified a test string is run");
			pString = testParamString;
			date = auth.parseDate("20140101T000000Z");
		} else
			pString = args[1];
		if (args.length >= 1)
			date = auth.parseDate(args[2]);
		else
			date = auth.parseDate("20140101T000000Z");
		try {
			auth.checkParams(pString, date);
			System.out.println("OK: Param string " + pString);
		} catch (Exception e) {
			System.err.println(e.toString());
			System.err.println("ERROR: Param string " + pString + "is not OK");

		}
	}

}