'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 15, 2014

    @author: rpsmarf
'''

import unittest
from smcommon.tests.dad_test_utils import TempFolder, TempFolderViaFileAcc
from smcommon.constants import sm_const
from smcommon.file_access.data_access_desc import SmDataContainer
from smcommon.logging.sm_logger import smLoggingSetupForTest
from smcommon.file_access.file_accessor_local import SmFileAccessorLocal
from smcommon.tests.ssh_server import sm_test_sftp_server_stop,\
    sm_start_test_sftp_server
from smcommon.file_access.file_accessor_sftp import SmFileAccessorSftp
from smcommon.file_access.file_accessor_ice import SmFileAccessorIce
from smcommon.globals.sm_copy_controller import SmCopyController
from smcommon.globals.sm_global_context import smGlobalContextInit,\
    smGlobalContextGet
from smcommon.utils.net_utils import smGetFreePort
from smcommon.file_access.ice_file_ops import SmIceFsOps
import logging
from smcommon.exceptions.file_access_exception import SmFileAccessException
import os
from smcommon.globals.envvar import SM_ENV_VAR_COMMON_KEY_DIR,\
    SM_ENV_VAR_TEST_AWS_SECRET_KEY, SM_ENV_VAR_TEST_AWS_KEY_ID, smLoadAwsKeys,\
    smCommonGetEnvVar, SM_ENV_VAR_TEST_PASSWORD
from smcommon.file_access.file_accessor_swift import SmFileAccessorSwift
import uuid
import urllib.parse
import json
from smcommon.file_access.file_accessor_s3 import SmFileAccessorS3
import time
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID,\
    SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY

logger = logging.getLogger(__name__)


class TestFileOps(unittest.TestCase):
    '''
    This class tests the file accessor classes.  These currently come in three flavours:
    - local for locally accessible fiels
    - SFTP for SFTP accessible fiels
    - ICE for files available on another remote agent
    '''

    def iceServerSetup(self):
        self.port = smGetFreePort()
        self.ic = smGlobalContextGet().ic
        adapter = self.ic.createObjectAdapterWithEndpoints("smarf", "default -p " + str(self.port))
        logger.debug("Start test ICE server on port %d", self.port)
        obj = SmIceFsOps()
        adapter.add(obj, self.ic.stringToIdentity("FileOps"))
        adapter.activate()
        
    def iceServerTeardown(self):
        if self.ic:
            # Clean up
            self.ic.destroy()
        
    def runTestWithFileAccessor(self, fileAccessor):
        # Test that exceptions raised if not connected
        self.assertRaises(SmFileAccessException, fileAccessor.deleteFileInfos, None)
        self.assertRaises(SmFileAccessException, fileAccessor.deletePaths, None)
        self.assertRaises(SmFileAccessException, fileAccessor.mkdir, None, False)
        self.assertRaises(SmFileAccessException, fileAccessor.list, None, None, None, None, True)
        
        fileAccessor.connectSession()
        self.assertRaises(SmFileAccessException, fileAccessor.connectSession)

        # Create f1, d2, d2/f3 and d2/f4
        self.tempFolder.writeFile("f1", "0123456789")
        self.tempFolder.mkdir("d2")
        self.tempFolder.writeFile("d2/f3", "0123456789")
        self.tempFolder.writeFile("d2/f4", "0123456789")
        self.tempFolder.mkdir(".hidden")
        self.tempFolder.writeFile(".hidden/f5", "111")
        self.tempFolder.writeFile("d2/.hidden_file", "222")
        
        # List folder with one level, expect to see f1 and d2
        files = fileAccessor.list("/", 1, 10, None, False)
        self.assertEqual(2, len(files))
        self.assertEqual("d2/", files[0]["name"])
        self.assertEqual("f1", files[1]["name"])
        self.assertEqual(10, files[1]["size"])
        
        files = fileAccessor.list("/", 1, 10, None, True)
        self.assertEqual(3, len(files))
        self.assertEqual(".hidden/", files[0]["name"])
        self.assertEqual("d2/", files[1]["name"])
        self.assertEqual("f1", files[2]["name"])
        self.assertEqual(10, files[2]["size"])
        
        # List folder 2 items at a time, expect to see all items
        files = fileAccessor.list("/", 10, 2, None, False)
        self.assertEqual(2, len(files))
        self.assertEqual("d2/", files[0]["name"])
        self.assertEqual("d2/f3", files[1]["name"])
        files = fileAccessor.list("/", 10, 2, files[1]["name"], False)
        self.assertEqual(2, len(files))
        self.assertEqual("d2/f4", files[0]["name"])
        self.assertEqual("f1", files[1]["name"])
        
        os.chmod(self.tempFolder.mkAbsolutePath("d2/f3"), 0o777)
        os.chmod(self.tempFolder.mkAbsolutePath("d2/f4"), 0o404)
        os.chmod(self.tempFolder.mkAbsolutePath("f1"), 0o440)
        
        # List folder with 10 levels
        files = fileAccessor.list("/", 10, 10, None, False)
        self.assertEqual(4, len(files))
        self.assertEqual("d2/", files[0]["name"])
        self.assertEqual("d2/f3", files[1]["name"])
        self.assertEqual("rwx", files[1]["mode"])
        self.assertEqual("d2/f4", files[2]["name"])
        self.assertEqual("r", files[2]["mode"])
        self.assertEqual("f1", files[3]["name"])
        self.assertEqual("r", files[3]["mode"])

        # List folder with 10 levels with showHidden
        files = fileAccessor.list("/", 10, 10, None, True)
        self.assertEqual(7, len(files))

        # Do du on folder
        duDict = fileAccessor.du("/")
        self.assertEqual(36, duDict["sizeInBytes"])
        
        # Delete d2/f4
        fInfos = fileAccessor.list("d2/f4", 1, 10, None, True)
        fileAccessor.deleteFileInfos(fInfos)
        # Check it is gone by listing d2/f4
        self.assertRaises(SmFileAccessException, fileAccessor.list, "d2/f4", 10, 10, None, True)
        
        # Delete d2, expect error because it is not empty
        paths = ["d2/"]
        self.assertRaises(SmFileAccessException, fileAccessor.deletePaths, paths)
        
        # List all and pass to delete
        files = fileAccessor.list("d2//", 10, 10, None, True)
        fileAccessor.deleteFileInfos(reversed(files))
        paths = ["f1"]
        fileAccessor.deletePaths(paths)
        
        # List, expect to see nothing
        files = fileAccessor.list("/", 10, 10, None, False)
        self.assertEqual(0, len(files))
        
        # Write a file
        self.writeFile(fileAccessor, "f9", b"0123456789")
        
        # Read the file back 
        data = self.readFile(fileAccessor, "f9")
        self.assertEqual(b"0123456789", data)
        
        fileAccessor.disconnectSession()

    def writeFile(self, fileAccessor, path, data):
        try:
            f = fileAccessor.open(path, "wb")
            f.write(data)
        finally:
            f.close()
        
    def readFile(self, fileAccessor, path, maxBytes=1000):
        f = None
        try:
            f = fileAccessor.open(path, "rb")
            return f.read(maxBytes)
        finally:
            if f:
                f.close()
        
    def make_file_dict(self, files):
        file_dict = {}
        for file in files:
            file_dict[file["name"]] = file
        
        return file_dict

    def runCloudTestWithFileAccessor(self, fileAccessor):
        # Test that exceptions raised if not connected
        self.assertRaises(SmFileAccessException, fileAccessor.deleteFileInfos, None)
        self.assertRaises(SmFileAccessException, fileAccessor.deletePaths, None)
        self.assertRaises(SmFileAccessException, fileAccessor.mkdir, None, False)
        self.assertRaises(SmFileAccessException, fileAccessor.list, None, None, None, None, True)
        
        fileAccessor.connectSession()
        self.assertRaises(SmFileAccessException, fileAccessor.connectSession)

        # Upload and create f1, d2, d2/f3 and d2/f4
        self.writeFile(fileAccessor, "f1", b"0123456789")
        fileAccessor.mkdir("d2/")
        self.writeFile(fileAccessor, "d2/f3", b"0123456789")
        self.writeFile(fileAccessor, "d2/f4", b"0123456789")
        fileAccessor.mkdir(".hidden/")
        self.writeFile(fileAccessor, ".hidden/f5", b"111")
        self.writeFile(fileAccessor, "d2/.hidden_file", b"222")
        
        # List folder with one level, expect to see f1 and d2
        files = fileAccessor.list("/", 1, 10, None, False)
        self.assertEqual(2, len(files), json.dumps(files))
        file_dict = self.make_file_dict(files)
        self.assertTrue("d2/" in file_dict)
        self.assertTrue("f1" in file_dict)
        self.assertEqual(10, file_dict["f1"]["size"])
        
        files = fileAccessor.list("/", 1, 10, None, True)
        file_dict = self.make_file_dict(files)
        self.assertEqual(3, len(files))
        self.assertTrue(".hidden/" in file_dict)
        self.assertTrue("d2/" in file_dict)
        self.assertTrue("f1" in file_dict)
        self.assertEqual(10, file_dict["f1"]["size"])
        
        # List folder 2 items at a time, expect to see all items
        files = fileAccessor.list("/", 10, 2, None, False)
        file_dict = self.make_file_dict(files)
        self.assertEqual(2, len(files), json.dumps(files))
        self.assertTrue("d2/" in file_dict)
        self.assertTrue("d2/f3" in file_dict)
        files = fileAccessor.list("/", 10, 2, files[1]["name"], False)
        file_dict = self.make_file_dict(files)
        self.assertEqual(2, len(files))
        self.assertTrue("d2/f4" in file_dict)
        self.assertTrue("f1" in file_dict)

        # List folder with 10 levels
        files = fileAccessor.list("/", 10, 10, None, False)
        self.assertEqual(4, len(files))
        file_dict = self.make_file_dict(files)
        self.assertTrue("d2/" in file_dict)
        self.assertTrue("d2/f3" in file_dict)
        self.assertEqual("rwx", file_dict["d2/f3"]["mode"])
        self.assertTrue("d2/f4" in file_dict)
        self.assertEqual("rwx", file_dict["d2/f4"]["mode"])
        self.assertTrue("f1" in file_dict)
        self.assertEqual("rwx", file_dict["f1"]["mode"])

        # List folder with 10 levels with showHidden
        files = fileAccessor.list("/", 10, 10, None, True)
        self.assertEqual(7, len(files))

        # Do du on folder
        duDict = fileAccessor.du("/", True)
        self.assertEqual(36, duDict["sizeInBytes"])
        
        # Delete d2/f4
        fInfos = fileAccessor.list("d2/f4", 10, 1, None, True)
        fileAccessor.deleteFileInfos(fInfos)
        # Check it is gone by listing d2/f4
        self.assertRaises(SmFileAccessException, fileAccessor.list, "d2/f4", 10, 1, None, True)
        
        # List all and pass to delete
        files = fileAccessor.list("d2//", 10, 10, None, True)
        fileAccessor.deleteFileInfos(reversed(files))
        paths = ["f1"]
        fileAccessor.deletePaths(paths)
        
        # List, expect to see nothing
        files = fileAccessor.list("/", 10, 10, None, False)
        self.assertEqual(0, len(files))
        
        # Write a file
        self.writeFile(fileAccessor, "f9", b"0123456789")
        
        # Read the file back 
        data = self.readFile(fileAccessor, "f9")
        self.assertEqual(b"0123456789", data)
        
        fileAccessor.disconnectSession()

    def setUp(self):
        smLoggingSetupForTest()
        self.tempFolder = TempFolder()
        self.localDataContainer = SmDataContainer(sm_const.SM_MY_AGENT_ID, "", 
                                                  sm_const.SM_DATA_CONTAINER_PROTOCOL_LOCAL + "://localhost/" + 
                                                  self.tempFolder.folder)
        self.sftpDataContainer = SmDataContainer(sm_const.SM_MY_AGENT_ID, "",
                                                 sm_const.SM_DATA_CONTAINER_PROTOCOL_SFTP + 
                                                 "://127.0.0.1:8222/" + self.tempFolder.folder
                                                 + "?uid=" + str(os.geteuid()))
        
        self.copyController = SmCopyController([self.localDataContainer.agentId])
        localDataContainer2 = SmDataContainer(sm_const.SM_MY_AGENT_ID, "",
                                              sm_const.SM_DATA_CONTAINER_PROTOCOL_LOCAL + "://localhost/" + 
                                              self.tempFolder.folder)
        self.assertEqual(self.localDataContainer, localDataContainer2)
        self.assertEqual(hash(self.localDataContainer), hash(localDataContainer2))
        
        smGlobalContextInit(self.copyController)
        self.maxWaitCount = 200

    def tearDown(self):
        self.tempFolder.cleanup()

    def testLocalOps(self):
        fileAccessor = SmFileAccessorLocal(self.localDataContainer)
        self.runTestWithFileAccessor(fileAccessor)

    def testSwiftOps(self):
        container = "unittest-" + str(uuid.uuid4())
        try:
            tenantString = urllib.parse.quote("NEP-Carleton University & Solana Networks")
            authTokenUrl = urllib.parse.quote("https://nova-ab.dair-atir.canarie.ca:35357/v2.0/")
            swiftDataContainer = SmDataContainer(sm_const.SM_MY_AGENT_ID, "",
                                                 sm_const.SM_DATA_CONTAINER_PROTOCOL_SWIFT + 
                                                 "://swift-ab.dair-atir.canarie.ca:8080/v1" +
                                                 "/AUTH_4c8f4711207b4b6bb74a1c200e8ac18b" +
                                                 "?username=OMelendez" +
                                                 "&password=" + smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD) +
                                                 "&tokenUrl=" + authTokenUrl +
                                                 "&container=" + container +
                                                 "&tenant=" + tenantString)
            fileAccessor = SmFileAccessorSwift(swiftDataContainer)
            tempFolderCloud = TempFolderViaFileAcc(fileAccessor)
            self.failIf(fileAccessor.swiftIf.createContainerSubFolder("") > 299)
    
            self.runCloudTestWithFileAccessor(fileAccessor)
        finally:
            # Delete container
            fileAccessor.connectSession()
            tempFolderCloud.cleanup()
            fileAccessor.disconnectSession()

    def testS3Ops(self):
        smLoadAwsKeys()
        bucket = "ca.rpsmarf.unittest-" + str(int(time.time()))
        try:
            s3DataContainer = SmDataContainer(sm_const.SM_MY_AGENT_ID, "",
                                              sm_const.SM_DATA_CONTAINER_PROTOCOL_S3 + 
                                              "://s3.com" +
                                              "?" + SCS_CLOUDIF_CLOUD_ACCT_AWS_KEY_ID + 
                                              "=" + os.environ[SM_ENV_VAR_TEST_AWS_KEY_ID] +
                                              "&" + SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY +
                                              "=" + urllib.parse.quote(os.environ[SM_ENV_VAR_TEST_AWS_SECRET_KEY]) +
                                              "&bucket=" + bucket)
            fileAccessor = SmFileAccessorS3(s3DataContainer)
            tempFolderCloud = TempFolderViaFileAcc(fileAccessor)
            fileAccessor.s3If.createBucket()
    
            self.runCloudTestWithFileAccessor(fileAccessor)
        finally:
            # Delete container
            if not fileAccessor.connected:
                fileAccessor.connectSession()
            tempFolderCloud.cleanup()
            fileAccessor.s3If.deleteBucket()
            fileAccessor.disconnectSession()

    def testSftpOps(self):
        # Start server
        sm_start_test_sftp_server() 

        try:
            fileAccessor = SmFileAccessorSftp(self.sftpDataContainer)
            self.runTestWithFileAccessor(fileAccessor)
        finally:
            # Stop server
            sm_test_sftp_server_stop()

    def testSftpOpsWithKey(self):
        user = os.environ["USER"]
        
        # Set envvar to key path
        cwd = os.getcwd()
        logger.warning("Working dir is %s", cwd)
        os.environ[SM_ENV_VAR_COMMON_KEY_DIR] = "/home/rpsmarf/git/python_repo/tools/keys"
        
        # Make sftpDataContainer
        sftpDataContainerViaLocalhostSsh = SmDataContainer(sm_const.SM_MY_AGENT_ID, "",
                                                 sm_const.SM_DATA_CONTAINER_PROTOCOL_SFTP + 
                                                 "://" + user + "@127.0.0.1:22/" + self.tempFolder.folder +
                                                 "?keyfile=dropbear_key&uid=" + str(os.geteuid()))
        
        try:
            fileAccessor = SmFileAccessorSftp(sftpDataContainerViaLocalhostSsh)
            self.runTestWithFileAccessor(fileAccessor)
        finally:
            pass
        
    def testLocalViaIceOps(self):
        try:
            self.iceServerSetup()
            self.localDataContainer.agentUri = "ice://localhost:" + str(self.port)
            fileAccessor = SmFileAccessorIce(self.localDataContainer)
            self.runTestWithFileAccessor(fileAccessor)
        finally:
            self.iceServerTeardown()
            
    def testSftpViaIceOps(self):
        # Start server
        sm_start_test_sftp_server() 

        try:
            self.iceServerSetup()
            self.sftpDataContainer.agentUri = "ice://localhost:" + str(self.port)
            self.localDataContainer.agentUri = "ice://localhost:" + str(self.port)
            fileAccessor = SmFileAccessorIce(self.sftpDataContainer)
            #fileAccessor = SmFileAccessorIce(self.localDataContainer)
            self.runTestWithFileAccessor(fileAccessor)
        finally:
            # Stop server
            sm_test_sftp_server_stop()
            self.iceServerTeardown()


# See ice_tests for the ICE file operation tests

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()