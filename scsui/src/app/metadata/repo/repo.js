angular.module('rpsmarf.metadata.repo',[
	'rpsmarf.user.service',
	'rpsmarf.config',
	'ui.bootstrap',
	'restangular',
	'rpsmarf.alert'
])
.controller('MetadataRepoCtrl', ['$scope', 'Restangular','$modal','repo','action','$modalInstance', function($scope, Restangular, $modal, repo, action, $modalInstance){

	/***STANDARD VARIABLES***/
	var originalResourcesChosen;
	var originalSchema;
	var originalResourcesAvailable;
	/***SCOPE VARIABLES***/
	$scope.current = {form: null};
	$scope.selected = {};
	$scope.resourcesAvailable = [];
	$scope.resourcesChosen = [];
	$scope.schemas = null;
	$scope.hide = {schema: true};
	$scope.op = repo ? 'edit' : 'add';
	$scope.repo = repo ? repo : {};
	$scope.response = null;

	/***SCOPE FUNCTIONS***/
	$scope.addRepo = function(){
		var obj,res = [], $form = $scope.current.form;
		if($form.$valid && $scope.resourcesChosen.length > 0){
			obj = {name: $scope.repo.name, description:$scope.repo.description, schema: $scope.selected.schema.resource_uri};
			for(var i= 0; i< $scope.resourcesChosen.length; i++){
				res.push($scope.resourcesChosen[i].resource_uri);
			}
			obj.resources = res;
			Restangular.all('md_repo/').post(obj).then(function(){
				action.value = 'add';
				formSubmitted('success.added');
			}, function(){
				formSubmitted('failure.added');
			});
			
		}else{
			$form.$submitted = true;
		}
	};

	$scope.updateRepo = function(){
		var obj,res = [], $form = $scope.current.form, repoId = $scope.repo.id;
		if($form.$valid && $scope.resourcesChosen.length > 0){
			obj = {name: $scope.repo.name, description:$scope.repo.description, schema: $scope.selected.schema.resource_uri};
			for(var i= 0; i< $scope.resourcesChosen.length; i++){
				res.push($scope.resourcesChosen[i].resource_uri);
			}
			obj.resources = res;
			Restangular.one('md_repo', repoId+'/').patch(obj).then(function(){
				repo.name = obj.name;
				repo.description = obj.description;
				repo.schema = obj.schema;
				repo.resources = obj.resources;
				action.value = 'update';
				formSubmitted('success.updated');
			}, function(){
				formSubmitted('failure.updated');
			});
			
		}else{
			$form.$submitted = true;
		}
	};

	$scope.schemaChanged = function(){
		$scope.hide.schema = true;
		var rtid,sid = $scope.selected.schema.id;
		Restangular.all('md_property_type/?schema__id='+sid+'&limit=0').get('').then(function(response){
			$scope.selected.schema.props = response.objects;
		});

		if($scope.op == 'add' || $scope.selected.schema.id != originalSchema){
			rtid = $scope.selected.schema.resource_type.split('/')[3];
			Restangular.all('resource/?resource_type__id='+rtid+'&limit=0').get('').then(function(response){
				$scope.resourcesAvailable = response.objects;
			});

			$scope.resourcesChosen = [];
		}
		else if($scope.op == 'edit' && $scope.selected.schema.id == originalSchema){
			$scope.resourcesAvailable = angular.copy(originalResourcesAvailable);
			$scope.resourcesChosen = angular.copy(originalResourcesChosen);
		}
		

	};

	$scope.addResource = function(resource){
		for(var i= 0; i< $scope.resourcesAvailable.length; i++){
			if(resource.id == $scope.resourcesAvailable[i].id){
				break;
			}
		}

		var removed = $scope.resourcesAvailable.splice(i,1);
		$scope.resourcesChosen.push(removed[0]);
		$scope.selected.resourceAvailable = null;
	};

	$scope.removeResource = function(resource){
		for(var i= 0; i< $scope.resourcesChosen.length; i++){
			if(resource.id == $scope.resourcesChosen[i].id){
				break;
			}
		}

		var removed = $scope.resourcesChosen.splice(i,1);
		$scope.resourcesAvailable.push(removed[0]);
		$scope.selected.resourceChosen = null;
	};

	$scope.deleteRepo = function(){
		var modalInstance = $modal.open({
			controller: 'ConfirmCtrl',
			templateUrl: 'confirm/confirm.tpl.html',
			resolve: {
				title: function(){ return 'Confirm Deletion';},
				body: function(){ return 'Delete '+$scope.repo.name+' (cannot be undone)?';}
			}
		});

		modalInstance.result.then(function(){
			Restangular.one('md_repo',$scope.repo.id+'/').remove().then(function(){
				action.value = 'delete';
				formSubmitted('success.deleted');
				resetForm();
				$modalInstance.close();
			},function(){
				formSubmitted('failure.deleted');
			});
		});
	};

	$scope.close = function(){
		$modalInstance.close();
	};

	/***STANDARD FUNCTIONS***/
	var resetForm = function(){
		$scope.repo = {};
		$scope.selected = {};
		$scope.resourcesAvailable = [];
		$scope.resourcesChosen = [];
		$scope.hide = {schema: true};
		$scope.op = repo ? 'edit' : 'add';
	
	};

	var formSubmitted = function(result){
		var actions = result.split('.');
		switch(actions[0]){
			case 'success':
				$modal.open({
					controller: 'AlertCtrl',
					templateUrl: 'alert/alert.tpl.html',
					resolve: {
						title: function(){ return 'Notice';},
						message: function(){ return 'Repo has been successfully '+actions[1]+'.';}
					}
				});
				if(actions[1] === 'added' || actions[1] === 'deleted'){
					resetForm();	
				}
				
				break;
			case 'failure':
				$modal.open({
					controller: 'AlertCtrl',
					templateUrl: 'alert/alert.tpl.html',
					resolve: {
						title: function(){ return 'Error';},
						message: function(){ return 'An error occurred. The repo could not be '+actions[1]+' at this time.';}
					}
				});
				break;
			default:

		}
	};

	/***EXE***/
	if(repo){
		var sid, rtid;
		sid = originalSchema = repo.schema.split('/')[3];
		
		Restangular.one('md_schema',sid+'/').get({limit:0}).then(function(response){
			$scope.selected.schema = response.plain();
			rtid = $scope.selected.schema.resource_type.split('/')[3];

			var set = '';
			for(var i=0; i<repo.resources.length;i++){
				if(i < repo.resources.length-1){
					set += repo.resources[i].split('/')[3]+';';
				}
				else{
					set += repo.resources[i].split('/')[3];
				}
			}
			Restangular.all('resource/set/'+set+'/').get('').then(function(response){
				$scope.resourcesChosen = response.objects;
				originalResourcesChosen = angular.copy(response.objects);
				var resourcesIds = set.split(';');
				Restangular.all('resource/?resource_type__id='+rtid+'&limit=0').get('').then(function(response){
					response = response.objects;
					//$scope.resourcesAvailable = response.objects;
					for(var j=response.length-1; j>=0; j--){
						for(var k=resourcesIds.length-1; k>=0; k--){
							if(response[j].id == resourcesIds[k]){
								response.splice(j,1);
								break;
							}
						}
					}

					$scope.resourcesAvailable = response;
					originalResourcesAvailable = angular.copy(response);
				});
			});

			

		});

			

		
		Restangular.all('md_property_type/?schema__id='+sid+'&limit=0').get('').then(function(response){
			$scope.selected.schema.props = response.objects;
		});
		
		
	}
	Restangular.all('md_schema/?limit=0').get('').then(function(response){
		$scope.schemas = response.objects;
	});
}]);