#!/usr/bin/env python 
'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import os
import sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "smarfcontrolserver.settings")
sys.path.insert(1, "/opt/smarf-scs/scs")

from django.contrib.auth.models import User
from sm_controller.models import SmModelResource
from sm_controller.models import SmModelTaskType
from sm_controller.models import SmModelTask
import random
from datetime import timezone, timedelta, datetime


def create_random_tasks(num_create):
    
    time_delta = timedelta(0)
    time_zone = timezone(time_delta) 
    num_tasks = SmModelTask.objects.count()
    num_resources = SmModelResource.objects.count()
    num_users = User.objects.count()
    
    if num_users == 0 or num_resources == 0:
        print('ERROR: You need users and resources before creating tasks! You can run the scsWipeAndInit script')
        return False
    elif num_create < 1:
        print('ERROR: Cannot create 0 tasks.')
        return False
        
    num_tasks_start = num_tasks
    
    for num_tasks in range(num_tasks, num_tasks + num_create):
        
        user_choose = random.randint(1, num_users)
        traversed = 1 
        
        users = User.objects.all()
        
        for user in users:
            if traversed == user_choose:
                user_id = user.id
                break
            else:
                traversed += 1
        
        user_obj = User.objects.get(id=user_id)            
        srctasktype_obj = SmModelTaskType.objects.all().iterator().__next__()
        n = str(num_tasks + 1)
        tstart_time = datetime.now(time_zone)
        service_time = random.randint(600, 3600)   
        tend_time = tstart_time + timedelta(days=0, seconds=service_time)
        print('Creating Task with parameters: owner=' + str(user_id) + 
              ', task_type_id=' + str(srctasktype_obj.id))
        SmModelTask.objects.create(owner=user_obj, start_time=tstart_time,
                                   end_time=tend_time, task_type=srctasktype_obj, state='init') 
                                
    print('Created ' + str(int(n) - num_tasks_start) + ' tasks.')
    print('There are now ' + n + ' total tasks in the database')


if __name__ == "__main__":
    argv = sys.argv
    if len(argv) == 2:
        if not argv[1].isdigit():
            print('ERROR: Please enter a positive integer for number to create.')
            sys.exit()   
        num_create = argv[1]

    else:
        print('Usage: scsCreateTask num_to_create')
        print('NOTE: this script will create random tasks (random users and random proper resources)')
        sys.exit()
        
    create_random_tasks(int(num_create))
