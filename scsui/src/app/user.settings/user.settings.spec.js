describe('user settings module', function(){

	beforeEach(module('rpsmarf.config'));
	beforeEach(module('rpsmarf.user.settings'));
	beforeEach(module('rpsmarf.user.service'));
	beforeEach(module('restangular'));
	beforeEach(module('ui.router'));
	beforeEach(module('confirm/confirm.tpl.html'));
	//beforeEach(module('user.settings/user.settings.tpl.html'));
	//beforeEach(module('user.settings/user.settings.account.tpl.html'));

	var $scope,$state,$rootScope, SmUser, Restangular, $controller, $httpBackend, smarfurl, responseObj, json = 'src/app/user.settings/user.settings.spec.json';
	var user;

	beforeEach(inject(function(_$rootScope_, _$state_, _SmUser_, _Restangular_, _$controller_, _$httpBackend_, _SMARF_URL_){
		$rootScope = _$rootScope_;
		Restangular = _Restangular_;
		smarfurl = _SMARF_URL_+'/scs/';
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;

		$scope = $rootScope.$new();
		$state = _$state_;
		SmUser = _SmUser_;
		responseObj = JSON.parse(__html__[json]);
		$controller = _$controller_;
		$httpBackend = _$httpBackend_;
		$state.$current.name = 'user.settings.account';

		angular.element('body').children().remove();
		user = SmUser.getObject();

		user.id= 1, user.email='smarfer@rpsmarf.ca', user.firstname='Amelia',user.lastname='Brand', user.apikey = '8f1c52ad5b4d0eece928b3a46622cf21e83f86c4',
		user.community= {

			"admin": "/scs/user/2/",
			"description": "The RADS group at carleton university.",
			"gid": 97,
			"id": 2,
			"leader": "/scs/user/2/",
			"name": "RADS",
			"name_key": "rads",
			"resource_uri": "/scs/community/2/"

		};

		$httpBackend.expect('GET',smarfurl+'community/').respond(200, responseObj);
		$httpBackend.expect('GET',smarfurl+'resource/?owner__id='+user.id+'&personalFolder=true').respond(200, responseObj.personal_folders);
		
		
		$controller('UserSettingsCtrl', {$scope:$scope,$state:$state,SmUser:SmUser,Restangular: Restangular});
		$httpBackend.flush();

		

	}));

	it('should set up the user account settings page', inject([function(){

		console.log(SmUser.getProperty('id'));
		expect(SmUser.getProperty('community').id).toEqual(2);

		$httpBackend.expect('PATCH',smarfurl+'user_setting/'+SmUser.getProperty('id')+'/',{community:'/scs/community/1/'}, {"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);
		expect($scope.data.communities.length).toEqual(2);
		expect($scope.userSettings.value.length).toBe(8);
		expect($scope.userSettings.value.length).toBeGreaterThan(2);
		expect($scope.userSettings.value[0].editable).toBe(false);
		expect($scope.userSettings.value[1].editable).toBe(false);
		expect($scope.userSettings.value[4].editable).toBe(true);
		expect($scope.userSettings.value[4].select).toBe(true);
		expect($scope.userSettings.value[4].editing).toBe(false);
		expect($scope.userSettings.value[4].value).toEqual(user.community);
		expect($scope.userSettings.value[5].value).toEqual('8f1c52ad5b4d0eece928b3a46622cf21e83f86c4');
		expect(angular.copy($scope.userSettings.value[6])).toEqual({name:'Private Cloud Folder Usage', value: ((4011976/10000000000)*100).toFixed(2) +'% ' + '('+(4011976/1073741824).toFixed(3) +' GB of ' +(10000000000/1073741824).toFixed(2) + ' GB used'+')', editable:false, editing:false, see_more: true });

		$scope.settingAction($scope.userSettings.value[4]);
		expect($scope.userSettings.value[4].editing).toBe(true);
		$scope.userSettings.value[4].value = responseObj.objects[0];
		$scope.settingAction($scope.userSettings.value[4]);
		expect($scope.userSettings.value[4].editing).toBe(false);

		$httpBackend.flush();

		expect(SmUser.getProperty('community').id).toEqual(1);




	}]));

	describe('cloud account setting', function(){
		var $cloudController, postObj;

		

		beforeEach(inject(function($controller){
			$httpBackend.expect('GET', smarfurl+'cloud/').respond(200,responseObj.clouds);
			$httpBackend.expect('GET', smarfurl+'cloud_account/?user__id=1').respond(200, responseObj.cloud_accounts);
			$httpBackend.expect('GET', smarfurl+'container/?cloud_account__id=1').respond(200, responseObj.container);
			$cloudController = $controller('UserCloudAccountsCtrl', {$scope: $scope});
			$httpBackend.flush();
		}));

		it('should perform the proper actions for cloud accounts', inject(function(){
			expect($scope.clouds[0].userSet).toBe(true);
			expect($scope.clouds[0].cloudAccountId).toBe(1);
			expect($scope.containerPropertyValues[1]['imageId']).toBe('ami-d05e75b8');
			//expect($scope.containerPropertyValues[1]['keypair']).toBe('cloud_server_test');
			expect($scope.containerPropertyValues[1]['region_name']).toBe('us-east-1');
			expect($scope.containerPropertyValues[1]['instance_type']).toBe('t2.micro');

			//Delete account
			$httpBackend.expect('DELETE', smarfurl+'cloud_account/1/').respond(200);

			$scope.deleteCloudAccount($scope.clouds[0]);
			$rootScope.$apply();
			angular.element('.modal-footer .btn-primary').click();
			$rootScope.$apply();

			$httpBackend.flush();
			expect($scope.clouds[0].userSet).toBe(false);
			expect($scope.clouds[0].cloudAccountId).toBe(null);

			//Save new account info
			postObj = {owner:'/scs/user/1/',cloud:'/scs/cloud/1/', accountInfoJson:JSON.stringify({key_id:'r336zzcfgf', secret_access_key: '7lk2ymtii76uvw88k8he'})};
			$httpBackend.expect('POST', smarfurl+'cloud_account/', postObj).respond(200,{headers:function(a){ return '/scs/cloud_account/24/';}}, {Location:'/scs/cloud_account/24/'});
			$scope.propertyValues[1]['key_id'] = 'r336zzcfgf';
			$scope.propertyValues[1]['secret_access_key'] = '7lk2ymtii76uvw88k8he';

			$scope.saveCloudAccount($scope.clouds[0]);
			$httpBackend.flush();
			expect($scope.clouds[0].userSet).toBe(true);
			expect($scope.clouds[0].cloudAccountId).toBe(24);

			//Save existing account info
			postObj = {accountInfoJson:JSON.stringify({key_id:'patchTest', secret_access_key:'accessKeyTest'})};
			$httpBackend.expect('PATCH', smarfurl+'cloud_account/24/', postObj).respond(200);
			$scope.propertyValues[1]['key_id'] = 'patchTest';
			$scope.propertyValues[1]['secret_access_key'] = 'accessKeyTest';
			$scope.saveCloudAccount($scope.clouds[0]);
			$httpBackend.flush();

			//Update/Save cloud container settings
			postObj = {imageId:'testImage', instance_type:'m4.large'};
			$httpBackend.expect('PATCH', smarfurl+'container/7/',{parametersJson:JSON.stringify(postObj)}).respond(200);
			$scope.containerPropertyValues[1]['imageId'] = 'testImage';
			$scope.containerPropertyValues[1]['instance_type'] = 'm4.large';


		}));
	});

	


//$scope, $state, SmUser, Restangular)





});