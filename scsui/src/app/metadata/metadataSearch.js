angular.module( 'rpsmarf.metadata', [
  'ngSanitize',
  'rpsmarf.config',
  'rpsmarf.user.service',
  'restangular',
  'ui.router',
  'ui.select',
  'rpsmarf.foldernav',
  'ui.bootstrap',
  'rpsmarf.extendedinfo',
  'rpsmarf.file.editor',
  'rpsmarf.accessrequest',
  'rpsmarf.resourceconfig.service',
  'rpsmarf.constants',
  'rpsmarf.metadata.extractor',
  'rpsmarf.metadata.repo'
])

.config(function config( $stateProvider, RestangularProvider, SMARF_URL ) {
  
  $stateProvider.state('metadata', {
    url: '/metadata/',
    views: {
      'main': {
        controller: 'MetadataSearchCtrl',
        templateUrl: 'metadata/metadataSearch.tpl.html'
      },
      'footer': {
        templateUrl: 'metadata/metadataSearch.infopanel.tpl.html',
        controller: 'MetadataSearchInfoPanelCtrl'
      }
    },
    data: {pageTitle: 'Metadata Search'}

  });
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'MetadataSearchCtrl', ['$scope', 'Restangular', '$modal', 'SmUser', '$state', 'USER_STATE', 'filterFilter', 'DEFAULT_METADATA_ICON_URL', 
  function ( $scope, Restangular, $modal, SmUser, $state, USER_STATE, filterFilter, DEFAULT_METADATA_ICON_URL) {

  /***STANDARD VARIABLES***/
  var uid = SmUser.getProperty('id');
  var community = SmUser.getProperty('community').id;
  var selectedRepoIndex = null;
  var allReposApiString = 'md_repo/?schema__community=';
  
  var allReposApi = Restangular.one(allReposApiString+community);
  var allrepos = [];
  
  
  var allRootEntries = [];
  var allResources = [];
  var userPermMap = null;
  /** Search Filter variables **/
  var op1 = { label: "Equal", name: 'equals' };
  var op2 = { label: "Greater Than", name: 'gt' };
  var op3 = { label: "Greater Than Equal To", name: 'gte' };
  var op4 = { label: "Less Than", name: 'lt' };
  var op5 = { label: "Less Than Equal To", name: 'lte' };

  /***SCOPE VARIABLES***/
  $scope.repos = [];
  $scope.DEFAULT_METADATA_ICON_URL = DEFAULT_METADATA_ICON_URL;
  $scope.availableOperationTypesMaster = [op1, op2, op3, op4, op5];
  $scope.operationTypeMaster = {selected: op1};
  $scope.searchFilters = [];
  $scope.availableFilterTypesMaster = ["Include", "Exclude"];
  $scope.filterTypeMaster = {selected: "Include"};
  $scope.repoSelected = 'false';
  $scope.tabs = {first: true};
  $scope.extractorApi = null;


  /***SCOPE FUNCTIONS***/
  $scope.addFilter = function() {
    $scope.searchFilters.push({filterType: {selected: "Include"}, propertyType: {}, operationType: {selected: op1}, prop: true, propertyValue: ""});
  };

  $scope.removeFilter = function() {
    $scope.searchFilters.pop();
  };

  $scope.enableOp = function(searchFilter) {
    searchFilter.prop = false;
  };

  $scope.filterRepos = function(searchText){
    var text = searchText.toLowerCase();
    $scope.repos = filterFilter(allRepos, {name:searchText}, false);
  };

  $scope.refreshRepos = function(){
   
    initiateRepo();
    
    if(selectedRepoIndex !== null){
      selectedRepoIndex = null;
      $scope.selectedRepo = null;
      $scope.repoSelected = 'false';
    }
  };


  $scope.selectRepo = function(repo, index){
    var ownerId;
    if(repo.selected){
      return false;
    }
    else{
      if(selectedRepoIndex !== null){
        $scope.repos[selectedRepoIndex].selected = false;
      }
    }
      
      selectedRepoIndex =index;
      repo.selected = true;
      $scope.repoSelected = 'true';
      $scope.selectedRepo = repo;
      $scope.tabs.first = true;

      Restangular.one('md_property_type/?schema__id='+$scope.selectedRepo.schema.split('/')[3]+'&limit=0').get().then(function(response){
        $scope.availablePropertyTypesMaster = response.objects;
        $scope.selectedRepo.schemaPropertyTypes = response.objects;
        // console.log($scope.availablePropertyTypesMaster);
      });

      $scope.searchFilters = [];
      $scope.addFilter();

      ownerId = repo.owner.split('/')[3];
      $scope.userOwnsRepo = ownerId == uid;
      Restangular.one('user',ownerId+'/').get().then(function(response){
        var name = response.first_name + ' ' + response.last_name;
        $scope.selectedRepo['owner_info'] = {name: name, email: response.email};
      });

      $scope.$emit('SmObjectSelectInfoPanel', $scope.selectedRepo);
      
  };

  $scope.search = function() {
     var queryParts = [];
     var query = '';
     //queryParts.push("repo="+$scope.selectedRepo.id);
      for (var i = 0; i < $scope.searchFilters.length; i++) {
          if($scope.searchFilters[i].propertyValue) {
              // console.log($scope.searchFilters[i]);
              var include = "";
              if($scope.searchFilters[i].filterType.selected == "Exclude") {
                  include = "-";
              }
              queryParts.push(include+$scope.searchFilters[i].propertyType.selected.name_key+"__"+$scope.searchFilters[i].operationType.selected.name+"="+$scope.searchFilters[i].propertyValue);
          }
      }
      if(queryParts.length > 0){
        query = "?"+queryParts.join("&");  
      }
      
      // console.log(query);
      $scope.$emit('SmMDSearch', query );
  };

  $scope.getExtractor = function(){
    var repoId = $scope.selectedRepo.id, repoCopy;
    Restangular.all('md_extractor_full/?repo__id='+repoId+'&owner__id='+uid).get('').then(function(response){
      if(response.meta.total_count > 0){
        $scope.selectedRepo.extractorExists = true;
        $scope.selectedRepo.extractor = response.objects[0];
        $scope.selectedRepo.extractorComplete = $scope.selectedRepo.extractor.state === 'done';
        
      }
      else {
        $scope.selectedRepo.extractorExists = false;
        $scope.selectedRepo.extractorComplete = false;
        $scope.selectedRepo.extractor = {};
        repoCopy = angular.copy($scope.selectedRepo);
        $scope.selectedRepo.extractor.repo = repoCopy;
        
      }
      
    });
  };

  $scope.test = function(){
    console.log($scope.selectedRepo.extractor);
  };

  $scope.addRepo = function(){
    var modalInstance, action = {};
    modalInstance = $modal.open({
      templateUrl: 'metadata/repo/repo.modal.tpl.html',
      controller: 'MetadataRepoCtrl',
      size: 'lg',
      resolve: {
        repo: function(){return null;},
        action: function(){return action;}
      }
    });

    modalInstance.result.then(function(){
      if(action.value){
        $scope.refreshRepos();
      }
    }, function(){
      if(action.value){
        $scope.refreshRepos();
      }
    });
  };

  $scope.manageRepo = function(repo){
    var modalInstance, action = {};
    modalInstance = $modal.open({
      templateUrl: 'metadata/repo/repo.modal.tpl.html',
      controller: 'MetadataRepoCtrl',
      size: 'lg',
      resolve: {
        repo: function(){return repo;},
        action: function(){return action;}
      }
    });

    modalInstance.result.then(function(){
      if(action.value =='update'){
        repoUpdated(repo);
      }
      else if(action.value == 'delete'){
        $scope.refreshRepos();
      }
    }, function(){
      if(action.value =='update'){
        repoUpdated(repo);
      }
      else if(action.value == 'delete'){
        $scope.refreshRepos();
      }
    });
  };
  /***STANDARD FUNCTIONS***/

  var initiateRepo = function(){
    allReposApi.get().then(function(response){
      allRepos = response.objects;
      $scope.repos = allRepos;
    });
  };

  var repoUpdated = function(repo){
    $scope.selectedRepo = repo;
    $scope.tabs.first = true;

    Restangular.one('md_property_type/?schema__id='+$scope.selectedRepo.schema.split('/')[3]+'&limit=0').get().then(function(response){
      $scope.availablePropertyTypesMaster = response.objects;
      $scope.selectedRepo.schemaPropertyTypes = response.objects;
      // console.log($scope.availablePropertyTypesMaster);
    });

    $scope.searchFilters = [];
    $scope.addFilter();

    $scope.$emit('SmObjectSelectInfoPanel', $scope.selectedRepo);
  };

  

  /***EXE***/

  $scope.metadataCollapsed.value = false;
  initiateRepo();

  $scope.$on('SmCommunityChange', function(event){

    community = SmUser.getProperty('community').name_key;
    allReposApi = Restangular.one(allReposApiString+community);
    initiateRepo();
  });

}])
.controller('MetadataSearchInfoPanelCtrl', ['$sce', '$scope', 'Restangular', '$modal', 'SmUser', '$state', 'USER_STATE', 'filterFilter',
  function ( $sce, $scope, Restangular, $modal, SmUser, $state, USER_STATE, filterFilter) {

    /***STANDARD VARIABLES***/
    var uid = SmUser.getProperty('id');
    var resourceMap = {};
    var itemsToUpdate = {};

    /***SCOPE VARIABLES***/
    $scope.selectedRepo = null;
    $scope.selectedSchema = null;
    $scope.query = null;
    $scope.mdSearchResults = null;
    //$scope.schemaPropertyTypes = null;
    $scope.select = {first: false,second: false};
    //$scope.repoActions = [];
    $scope.queryTriggered = 'false';

    $scope.fileSet = null;
    $scope.fileSetItems = [];
    $scope.records = 0;

    $scope.resultsPage = {value: 1};
    $scope.resultsPerPage = {value: 10};

    $scope.searchActions = false;

    

    /***SCOPE FUNCTIONS***/
    $scope.infoPanelCollapseToggle = function(){
      $scope.infoPanel.collapsed = !$scope.infoPanel.collapsed;
      $scope.$emit('SmInfoPanelToggle', $scope.infoPanel.collapsed);
    };

    $scope.downloadResults = function(){
      $modal.open({
        templateUrl: 'metadata/metadataSearch.download.tpl.html',
        controller: 'MetadataSearchDownloadCtrl',
        resolve: {
          fileSet: function(){ return $scope.fileSet;}
        }
        
      });
    };
      
    $scope.getFileSetItems = function(){
      var offset = ($scope.resultsPage.value-1)*$scope.resultsPerPage.value;
      var limit = $scope.resultsPerPage.value;
      var fileSet = $scope.fileSet;
      var i;
      Restangular.all('file_set_item/?file_set='+fileSet.id+'&offset='+offset+'&limit='+limit+'&showProps=true').get('').then(function(response){
        $scope.fileSetItems = response.objects;
        for(i= $scope.fileSetItems.length-1; i>=0; i--){
          getResource($scope.fileSetItems[i]);
        }
      });
    };

    $scope.resultsInfo = function(){
      var resultsPage = $scope.resultsPage;
      var resultsPerPage = $scope.resultsPerPage;
      var records = $scope.records;
      var endLimit = Math.min((resultsPage.value-1)*parseInt(resultsPerPage.value,10) + parseInt(resultsPerPage.value,10),records);
      return 'Showing results '+ ((resultsPage.value-1)*parseInt(resultsPerPage.value,10) + 1) +' - '+endLimit + ' of ' + records;
    };

    $scope.saveToResource = function(){
      $modal.open({
        templateUrl: 'metadata/metadataSearch.resourcecopy.tpl.html',
        controller: 'MetadataSearchCopyToResourceCtrl',
        resolve:{
          fileSet: function(){ return $scope.fileSet; },
          records: function(){ return $scope.records; }
        }
      });
    };

    /***STANDARD FUNCTIONS***/
    var getUserInfo = function(idPath, userObj){
      var id = idPath.split('/')[3], extra='';
      Restangular.one('user',id+'/').get().then(function(response){
        if(id == uid){
          extra = ' (You)';
        }
        userObj.id = id;
        userObj.name = response.first_name + ' ' + response.last_name + extra;
      });
    };

    var properties = function(search){
      Restangular.one('md_property_with_type/?path='+search.id).get().then(function(response){
        
        search.properties = response.objects;
      });
    };

    var getResource = function(item){
      var rid = item.resource.split('/')[3];
      if(resourceMap[rid] && !resourceMap[rid].init){
        item.resourceName = resourceMap[rid].name;
      }
      else{
        if(!resourceMap[rid]){
          resourceMap[rid] = {init: true};
          Restangular.one('resource',rid+'/').get().then(function(response){
            response = response.plain();
            resourceMap[rid] = response;
            item.resourceName = resourceMap[rid].name;
            if(itemsToUpdate[rid] && itemsToUpdate[rid].length > 0){
              for(var i = itemsToUpdate[rid].length-1 ; i>=0; i--){
                itemsToUpdate[rid][i].resourceName = resourceMap[rid].name;
              }
              itemsToUpdate[rid] = []; 
            }
          });

          
        }
        else{
          if(!itemsToUpdate[rid]){
            itemsToUpdate[rid] = [];
          }
          itemsToUpdate[rid].push(item);
        }
        
      }
    };

    /***EXE***/
    $scope.$on('SmInfoPanelUpdate', function(event, selectedRepo){
      var schemaId = selectedRepo.schema.split('/')[3];
      $scope.mdSearchResults = null;
      $scope.queryTriggered = 'false';
      //$scope.repoActions = [];
      $scope.selectedRepo = selectedRepo;
      // console.log($scope.selectedRepo.schema.split('/')[3]);
      Restangular.one('md_schema_full',schemaId+'/').get().then(function(response){
        $scope.selectedSchema = response.plain();
        $scope.selectedSchema.owner.name = $scope.selectedSchema.owner.first_name + ' ' + $scope.selectedSchema.owner.last_name;
        // console.log($scope.selectedSchema);
      });
      //Restangular.one('md_property_type/?schema__id='+$scope.selectedRepo.schema.split('/')[3]).get().then(function(response){
      //$scope.schemaPropertyTypes = ;
      //});
    
      $scope.select.first = true;
    });

    $scope.$on('SmMDSearchUpdate', function(event, query){
      $scope.select.second = true;
      $scope.queryTriggered = 'true';
      $scope.select.first = false;
      $scope.query = query;

      Restangular.one('md_repo/'+$scope.selectedRepo.id+'/search/'+$scope.query).get().then(function(response){
        $scope.fileSet = response.file_set;
        $scope.records = response.records;
        $scope.resultsPage.value = 1;
        // console.log($scope.mdSearchResults);
        //for(var i = 0; i < $scope.mdSearchResults.length; i++) {
        //  properties($scope.mdSearchResults[i]);
        //  getResource($scope.mdSearchResults[i]);
          // console.log($scope.mdSearchResults[i]);
        //}

        $scope.getFileSetItems();


      });

      // console.log($scope.query);
    });

  }])
.controller('MetadataSearchDownloadCtrl', ['$scope','$modalInstance', 'fileSet','$window', 'Restangular', function($scope, $modalInstance,fileSet, $window, Restangular){
  /***STANDARD VARIABLES***/
  var baseUrl= Restangular.configuration.baseUrl;
  baseUrl = baseUrl.substring(baseUrl.length-1) === '/' ? baseUrl.substring(0,baseUrl.length-1) : baseUrl;

  /***SCOPE VARIABLES***/
  $scope.downloadType='';
  /***SCOPE FUNCTIONS***/
  $scope.download = function(){
    switch($scope.downloadType){
      case 'json':
        $window.open(baseUrl+'/file_set_item/?file_set__id='+fileSet.id+'&downloadAsFile=True');
        break;
      case 'csv':
        $window.open(baseUrl+'/file_set_item/?file_set__id='+fileSet.id+'&format=csv');
        break;
      case 'zip':
        $window.open(baseUrl+'/file_set/'+fileSet.id+'/downloadzip/');
        break;
      default:
        break;
    }
  };

  $scope.close = function(){
    $modalInstance.close();
  };

}])
.controller('MetadataSearchCopyToResourceCtrl',['$scope','$modalInstance', 'fileSet','$window', 'Restangular','SmUser','filterFilter','records','$state', function($scope, $modalInstance,fileSet, $window, Restangular, SmUser, filterFilter, records, $state){
   /***STANDARD VARIABLES***/
  var uid = SmUser.getProperty('id');
  var community = SmUser.getProperty('community').name_key;
  /***SCOPE VARIABLES***/
  $scope.selectedResource={value: null};
  $scope.resources = null;
  $scope.records = records;
  $scope.copyStarted = false;
  $scope.copy = null;
  /***SCOPE FUNCTIONS***/
  $scope.copyToResource = function(){
    Restangular.all('resource/'+$scope.selectedResource.value.id+'/copy_from_file_set/?file_set='+fileSet.id).get('').then(function(response){
      $scope.copyStarted = true;
      $scope.copy = response.plain();
    });
  };

  $scope.close = function(){
    $modalInstance.close();
  };

  $scope.goToTaskStatusPage = function(){
    $modalInstance.close();
    $state.go('tasklist');
  };
  /***EXE***/
  Restangular.all('resource/?communityFilter='+community+'&extForUser='+uid+'&limit=0&locked=false&status=up').get('').then(function(response){
    response = response.plain();
    $scope.resources = filterFilter(response.objects,function(value,index){return value.nature === 'data' && value.ext_perms.indexOf('w') >=0;},false);
  });

}]);

