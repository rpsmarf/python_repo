'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jan 8, 2015

    @author: rpsmarf
'''
from canarie_service.models import SmModelCanarieServiceProperties
import json
from smcommon.logging.sm_logger import smLoggingSetupForTest
from canarie_service.constants import INFO_NAME, INFO_SYNOPSIS, INFO_VERSION,\
    INFO_INSTITUTION, INFO_RELEASE_TIME_JSON, INFO_SUPPORT_EMAIL_JSON,\
    INFO_CATEGORY, INFO_RESEARCH_SUBJECT_JSON, INFO_TAGS, JSON_CONTENT,\
    STAT_LAST_RESET, STAT_UPTIME_IN_SEC, LICENCE_URL,\
    VIDEO_URL_FAVORITE_A_RESOURCE, VIDEO_URL_FAVORITE_A_TOOL,\
    VIDEO_URL_ACCESS_DATA, VIDEO_URL_RUN_A_TOOL_BATCH,\
    VIDEO_URL_RUN_A_TOOL_INTERACTIVE
from django.test.testcases import TransactionTestCase


DEFAULT_INFO = {
                INFO_NAME: "testName",
                INFO_SYNOPSIS: "testSynopsis",
                INFO_VERSION: "testVersion",
                INFO_INSTITUTION: "testInst",
                INFO_RELEASE_TIME_JSON: "2015-01-01T01:02:00Z",
                INFO_SUPPORT_EMAIL_JSON: "x@y.com",
                INFO_CATEGORY: "testCat",
                INFO_RESEARCH_SUBJECT_JSON: "testSubject",
                INFO_TAGS: "[\"t1\", \"t2\"]"
                }

BASE_URL = "http://a.b/"


def setupCsModel():
    
    for key in [LICENCE_URL, VIDEO_URL_FAVORITE_A_RESOURCE, VIDEO_URL_FAVORITE_A_TOOL,
                VIDEO_URL_ACCESS_DATA, VIDEO_URL_RUN_A_TOOL_BATCH,
                VIDEO_URL_RUN_A_TOOL_INTERACTIVE]:
        p = SmModelCanarieServiceProperties()
        p.key = key
        p.value = BASE_URL + key
        p.save()
    
    for k in DEFAULT_INFO.keys():
        p = SmModelCanarieServiceProperties()
        p.key = k
        p.value = DEFAULT_INFO[k]
        p.save()
        

# Create your tests here.
class ApiTest(TransactionTestCase):
    
    def setUp(self):
        setupCsModel()
        smLoggingSetupForTest()
        
    def test_platformPages(self):
        resp = self.client.get("/scs/platform/doc", )
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/platform/releasenotes")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/platform/support")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/platform/source")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/platform/tryme")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/platform/licence")
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(BASE_URL + LICENCE_URL, resp.url)

        resp = self.client.get("/scs/platform/videoFavoriteAResource")
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(BASE_URL + VIDEO_URL_FAVORITE_A_RESOURCE, resp.url)

        resp = self.client.get("/scs/platform/provenance")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/platform/factsheet")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/platform/info", HTTP_ACCEPT=JSON_CONTENT)
        self.assertEqual(resp.status_code, 200)        
        jsoncon = json.loads(resp.content.decode('utf-8'))
        for k in DEFAULT_INFO.keys():
            if k == "tags":
                self.assertEqual(2, len(jsoncon["tags"]))
                for tag in jsoncon["tags"]:
                    self.assertTrue(tag in ['t1', 't2'])
            else:
                self.assertEqual(DEFAULT_INFO[k], jsoncon[k])

        # Try HTML version
        resp = self.client.get("/scs/platform/info")
        self.assertEqual(resp.status_code, 200)        
        htmlResp = resp.content.decode('utf-8')
        for k in DEFAULT_INFO.keys():
            if k != "tags":
                self.assertTrue(DEFAULT_INFO[k] in htmlResp)
            
        resp = self.client.get("/scs/platform/stats", HTTP_ACCEPT=JSON_CONTENT)
        self.assertEqual(resp.status_code, 200)        
        jsoncon = json.loads(resp.content.decode('utf-8'))

        self.assertTrue(STAT_LAST_RESET in jsoncon)
        self.assertTrue(float(jsoncon[STAT_UPTIME_IN_SEC]) < 100.0)

        resp = self.client.get("/scs/platform/stats")
        self.assertEqual(resp.status_code, 200)        
        htmlResp = resp.content.decode('utf-8')
        self.assertTrue(STAT_LAST_RESET in htmlResp)
        self.assertTrue(STAT_UPTIME_IN_SEC in htmlResp)
        self.assertTrue("<table>" in htmlResp)
        self.assertTrue("</table>" in htmlResp)
        
    def test_cloudRpiPages(self):
        resp = self.client.get("/scs/cloud_rpi/service/doc", )
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/cloud_rpi/service/releasenotes")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/cloud_rpi/service/support")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/cloud_rpi/service/source")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/cloud_rpi/service/tryme")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/cloud_rpi/service/licence")
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(BASE_URL + LICENCE_URL, resp.url)

        resp = self.client.get("/scs/cloud_rpi/service/provenance")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/cloud_rpi/service/factsheet")
        self.assertEqual(resp.status_code, 200)

    def test_metadataRpiPages(self):
        resp = self.client.get("/scs/metadata_rpi/service/doc", )
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/metadata_rpi/service/releasenotes")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/metadata_rpi/service/support")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/metadata_rpi/service/source")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/metadata_rpi/service/tryme")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/metadata_rpi/service/licence")
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(BASE_URL + LICENCE_URL, resp.url)

        resp = self.client.get("/scs/metadata_rpi/service/provenance")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/metadata_rpi/service/factsheet")
        self.assertEqual(resp.status_code, 200)

