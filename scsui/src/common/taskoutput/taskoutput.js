angular.module('rpsmarf.taskoutput',[
	'ui.bootstrap',
	'ngSanitize'

])
.controller('TaskOutputCtrl', ['$scope', '$modalInstance', 'task', function($scope, $modalInstance, task){ 

	var stdoutMessage = 'No standard output', stderrMessage = 'No standard error';
	$scope.standardOutput = task.stdout === '' ? stdoutMessage : task.stdout;
	$scope.standardError = task.stderr === '' ? stderrMessage : task.stderr;

	$scope.close = function(){
		$modalInstance.close();
	};

}]);