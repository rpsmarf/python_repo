'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import time
import tempfile
import logging.config
import json
from os.path import os
from smremoteagent.init.envvar import \
    SRA_ENV_VAR_SCS_ICE_HOSTNAME, sraGetEnvVar, SRA_ENV_VAR_SCS_ICE_PORT,\
    sraGetEnvVarFloat, SRA_ENV_VAR_HEARTBEAT_INTERVAL, sraGetEnvVarInt,\
    SRA_ENV_VAR_AGENT_GUID, SRA_ENV_VAR_SRA_ICE_PORT
from smremoteagent.init.sra_ice import SraIceIf
from smcommon.heartbeats.heart import SmHeart
from smcommon.constants.sm_const import smConstSetMyAgentId


runningInProd = (os.path.realpath(__file__) == "/opt/smarf-sra/sra/smremoteagent/init/sra_main.py")

print("runningInProd IS " + str(runningInProd))
      
SRA_LOG_DIR = "/var/log/smarf-sra"
if (not os.path.exists(SRA_LOG_DIR) or not runningInProd):
    SRA_LOG_DIR = os.path.join(tempfile.gettempdir(), "smarf-sra-logs")
    try:
        os.mkdir(SRA_LOG_DIR)
    except:
        pass
    
SRA_LOG_LOG_FILE = os.path.join(SRA_LOG_DIR, "log.log")
SRA_LOG_ERROR_FILE = os.path.join(SRA_LOG_DIR, "error.log")
    
DEFAULT_LOGGING_DICT = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {
            "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        },
        "verbose": 
        {
            'format': "[%(asctime)s] %(levelname)-8s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },

    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "WARNING",
            "formatter": "simple"
        },

        "file_handler": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": "DEBUG",
            "formatter": "verbose",
            "filename": SRA_LOG_LOG_FILE,
            "maxBytes": 10485760,
            "backupCount": 20,
            "encoding": "utf8"
        },

        "error_file_handler": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": "ERROR",
            "formatter": "verbose",
            "filename": SRA_LOG_ERROR_FILE,
            "maxBytes": 10485760,
            "backupCount": 20,
            "encoding": "utf8"
        }
    },

    "loggers": {
        "": {
            "level": "DEBUG",
            "handlers": ["console", "file_handler", "error_file_handler"],
            "propagate": "yes"
        }
    },

}

FIRST_TIME = True
SRA_LOG_CONF_FILE = "/etc/smarf_sra/logging.conf"

logger = logging.getLogger(__name__)


def smLoggingSetupSra():
    
    if ((not os.path.exists(SRA_LOG_CONF_FILE)) or not runningInProd):
        # Write file
        loggingDict = DEFAULT_LOGGING_DICT 
        print("Using built in logging configuration file instead of " + SRA_LOG_CONF_FILE)
        print("Logging to directory " + SRA_LOG_DIR)             
    else:
        with open(SRA_LOG_CONF_FILE, 'r') as the_file:
            data = the_file.read(100000)
        loggingDict = json.loads(data)
        global FIRST_TIME
        if (FIRST_TIME):
            print("Loading logging configuration file from " + SRA_LOG_CONF_FILE)
            FIRST_TIME = False
         
    s = json.dumps(DEFAULT_LOGGING_DICT)
    print(s)         
                
    logging.config.dictConfig(loggingDict)
    logger.critical("==================================================================")
    logger.critical("LOGGING RESTARTED")
    logger.critical("==================================================================")
    print("Logging configuration done")
    
    
SRA_QUIT = False

global sraIceIf, sraHeart
sraIceIf = None
sraHeart = None

if __name__ == '__main__':
    # Initialize logging
    smLoggingSetupSra()
    
    logger.info("Environment variables:")
    for s in os.environ.keys():
        logger.info(s + " = " + os.environ[s])
    logger.info("End of environment variables")

    smConstSetMyAgentId(sraGetEnvVar(SRA_ENV_VAR_AGENT_GUID))
    
    # Start the ICE subsystem
    sraIceIf = SraIceIf(sraGetEnvVar(SRA_ENV_VAR_SCS_ICE_HOSTNAME),
                        sraGetEnvVarInt(SRA_ENV_VAR_SCS_ICE_PORT),
                        sraGetEnvVarInt(SRA_ENV_VAR_SRA_ICE_PORT),
                        "SmRemAgAsyncOpResps:tcp -h " + sraGetEnvVar(SRA_ENV_VAR_SCS_ICE_HOSTNAME) + 
                        " -p " + sraGetEnvVar(SRA_ENV_VAR_SCS_ICE_PORT),
                        None,
                        sraGetEnvVar(SRA_ENV_VAR_AGENT_GUID)
                        )
    
    # Start the hearbeat generator
    sraHeart = SmHeart(sraGetEnvVar(SRA_ENV_VAR_AGENT_GUID),  
                       sraGetEnvVarFloat(SRA_ENV_VAR_HEARTBEAT_INTERVAL),
                       sraIceIf.sendHeartbeat)
    sraHeart.start()
    
    # Sit in loop
    while (not SRA_QUIT):
        time.sleep(15)
