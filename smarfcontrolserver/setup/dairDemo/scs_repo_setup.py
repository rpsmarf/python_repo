# Setting up metadata...
# Add a schema
from scripts.scs_obj_create import scsCreateObjectPath
from scripts.scs_obj_get import scsGetObjectPath

print("Setting up repos.")

root_path = scsGetObjectPath("user", "username=rpsmarf")
community_path = scsGetObjectPath("community", "name_key=all_resources")
rt_generic_storage = scsGetObjectPath("resource_type",
                                      "name_key=generic_storage")
res_copy_src = scsGetObjectPath("resource", "name_key=carleton_demo_data_a")

schema_path = scsCreateObjectPath("md_schema", {"name": "Sample Schema",
                                                "description": "This is the example schema",
                                                "owner": root_path,
                                                "resource_type": rt_generic_storage,
                                                "community": community_path
                                                })


# Add 2 property types
ptype_wind_path = scsCreateObjectPath("md_property_type", {"name": "Wind Speed",
                                                           "property_type": "INT",
                                                           "schema": schema_path
                                                           })
ptype_temp_path = scsCreateObjectPath("md_property_type", {"name": "Temperature",
                                                           "property_type": "FLOAT",
                                                           "schema": schema_path
                                                           })

# Add 2 repos
repo1_path = scsCreateObjectPath("md_repo", 
                                 {"name": "Metadata repo 1",
                                  "description": "This is the first example metadata repository",
                                  "owner": root_path,
                                  "resources": [res_copy_src],
                                  "community": community_path,
                                  "schema": schema_path
                                  })

# Add 2 files to first repo
for i in range(0, 20):
    path_path = scsCreateObjectPath("md_path", 
                                    {"relative_path": "dir/f" + str(i),
                                     "repo": repo1_path,
                                     "resource": res_copy_src,
                                     })
    # Add 2 attrbibutes to each file
    scsCreateObjectPath("md_property", 
                    {"path": path_path,
                    "property_type": ptype_wind_path,
                    "value": str(i * 2)})
    scsCreateObjectPath("md_property", 
                    {"path": path_path,
                    "property_type": ptype_temp_path,
                    "value": str((i * 2) - 20)})


print("Repos setup.")
