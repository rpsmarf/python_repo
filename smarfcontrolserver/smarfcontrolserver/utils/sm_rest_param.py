'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 19, 2014

    @author: rpsmarf
'''
import logging
from smcommon.exceptions.sm_exc import SmException
import json
from tastypie.exceptions import ImmediateHttpResponse
from tastypie.http import HttpBadRequest

logger = logging.getLogger(__name__)


class SmParamInfo2(object):
    '''
    classdocs
    '''

    def __init__(self, apiDescDict, pk, request):
        '''
        Constructor
        '''
        slashIndex = pk.find("/") 
        if slashIndex == -1:
            self.pkValue = int(pk)
            self.pathAfterPk = ""
        else:
            self.pkValue = int(pk[:slashIndex])
            # WSGI vs runserver do different things with trailing / so always kill them
            self.pathAfterPk = pk[slashIndex + 1:].rstrip("/")
            
        if len(request.GET) != 0:
            paramsInRequest = request.GET
        else:
            paramsInRequest = request.POST
        # Go through the values in the descriptor passed in.  For each value, check if the 
        # value exists in the request and use that if OK otherwise plug in the default.
        self.paramDict = {}
        for p in apiDescDict["params"]:
            paramName = p["name"]
            valueInRequest = paramsInRequest.get(paramName)
            if valueInRequest is None:
                if p.get("default") is None:
                    raise ImmediateHttpResponse(HttpBadRequest("Parameter " + paramName + " is mandatory but not specified."))                    
                else:
                    self.paramDict[paramName] = p["default"]
            else:
                try:
                    ptype = p["type"]
                    if ptype == "string":
                        self.paramDict[paramName] = valueInRequest
                    elif ptype == "stringlist":
                        self.paramDict[paramName] = paramsInRequest.getlist(paramName)
                    elif ptype == "int":
                        self.paramDict[paramName] = int(valueInRequest)
                    elif ptype == "float":
                        self.paramDict[paramName] = float(valueInRequest)
                    elif ptype == "bool":
                        self.paramDict[paramName] = bool(valueInRequest.lower())
                    else:   
                        raise SmException("Unknown type " + ptype + " in descriptor " + json.dumps(apiDescDict))
                except:
                    raise ImmediateHttpResponse(HttpBadRequest("Invalid value '" + valueInRequest + 
                                                               "' for parameter " + paramName + " of type " + ptype))
                                                
    def getParam(self, param):
        return self.paramDict[param]
    
    def getObjectId(self):
        return self.pkValue
    
    def getPostObjectIdPath(self):
        return self.pathAfterPk