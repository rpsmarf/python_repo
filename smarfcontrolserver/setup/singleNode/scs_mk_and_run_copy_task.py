import json
import os
import sys
from scripts.scs_task_start import scsTaskStartByPath
import subprocess
from scripts.scs_obj_common import scsScriptGetDefaultUrl
import stat
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath
from scripts.scs_obj_create import scsCreateObjectPath

if len(sys.argv) > 1:
    size = sys.argv[1]
    subprocess.call(["/usr/bin/fallocate", "-l", size, "/tmp/src/a"])
else:
    size = None
    with open("/tmp/src/a", 'w') as the_file:
        the_file.write("Hello world")

try:
    os.chmod("/tmp/dest", stat.S_IROTH | stat.S_IWOTH | stat.S_IXOTH 
             | stat.S_IRGRP | stat.S_IWGRP | stat.S_IXGRP
             | stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
    os.remove("/tmp/dest/b")
except:
    pass

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

taskParams = {
              "input_ids": {
                            "1": {"path": "a"},
                            "2": {"path": "b"}
                            }
              }

tt_path = scsGetObjectPath("task_type", "name=copy")
root_path = scsGetObjectPath("user", "name=rpsmarf")
task_path = scsCreateObjectPath("task", {"name": "copy", 
                                         "task_type": tt_path,
                                         "owner": root_path,
                                         "parametersJson": json.dumps(taskParams),
                                         }
                                )

resource_type_task_type_data_dest_path = scsGetObjectPath("resource_type_task_type", "name_key=cp_dest")
resource_type_task_type_data_src_path = scsGetObjectPath("resource_type_task_type", "name_key=cp_src")


src_resource_path = scsGetObjectPath("resource", "name=copy_src")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": src_resource_path,
                                      "resource_type_task_type": resource_type_task_type_data_src_path,
                                      }
                    )

dest_resource_path = scsGetObjectPath("resource", "name=copy_dest")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": dest_resource_path,
                                      "resource_type_task_type": resource_type_task_type_data_dest_path,
                                      }
                    )

print("Task " + task_path + " to copy /tmp/src/a to /tmp/dest/b is setup, starting task...")

scsTaskStartByPath(task_path, verbose=True)

if size is None:
    with open("/tmp/dest/b", 'r') as the_file:
        s = the_file.read(1000)

    if s != "Hello world":
        print("ERROR - Invalid data read: %s", s)
        sys.exit(1)
else:
    size2 = os.path.getsize("/tmp/dest/b")
    if int(size) != size2: 
        print("ERROR - Invalid file size read: %d (expected %s)", size2, size)
        sys.exit(1)
