'''
    Copyright (C) 2015 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Mar 31, 2015

    @author: rpsmarf
'''

from Crypto.PublicKey import RSA 
from Crypto.Signature import PKCS1_v1_5 
from Crypto.Hash import SHA256 
from base64 import b64encode, b64decode 
from smcommon.utils.date_utils import smMakeDateString,\
    smMakeDateFromString


class SmCryptoSigner(object):
    '''
    This class computes a cryptographically secure string over a data string
    using public key/private key technology to allow a destination node 
    (which has access to the public key) to verify that the information 
    came from the node which holds the private key.
    '''

    def __init__(self, private_key_path, public_key_path):
        '''
        Constructor
        '''
        self.pub_key = None
        self.private_key = None
        self.private_key_loc = private_key_path
        self.public_key_loc = public_key_path
        
    def loadPrivateKey(self):
        if self.private_key is None:
            self.private_key = open(self.private_key_loc, "r").read() 

    def loadPublicKey(self):
        if self.pub_key is None:
            self.pub_key = open(self.public_key_loc, "r").read() 
        
    def signData(self, data):
        '''
        Create signature for the data specified based on the private key
        
        param: data Data to be signed
        return: base64 encoded signature
        '''
        self.loadPrivateKey()
        rsakey = RSA.importKey(self.private_key) 
        signer = PKCS1_v1_5.new(rsakey) 
        digest = SHA256.new() 
        digest.update(data) 
        sign = signer.sign(digest) 
        return b64encode(sign)

    def verifySignature(self, signature, data):
        '''
        Verifies with a public key from whom the data came that it was indeed 
        signed by their private key
        param: public_key_loc Path to public key
        param: signature String signature to be verified
        return: Boolean. True if the signature is valid; False otherwise. 
        '''
        self.loadPublicKey()
        rsakey = RSA.importKey(self.pub_key) 
        signer = PKCS1_v1_5.new(rsakey) 
        digest = SHA256.new() 
        # Assumes the data is base64 encoded to begin with
        digest.update(data) 
        if signer.verify(digest, b64decode(signature)):
            return True
        return False


class SmTokenMaker(object):
    '''
    This object allows creation of a "token" parameter based on a list of HTTP
    parameters.  Similarly the token can be validated.
    '''
    
    def _makeStringFromList(self, path, paramList, listOfParamToIgnore):
        items = []
        for param in paramList:
            if len(items) != 0:
                header = "&"
            else:
                header = ""
            if param[0] not in listOfParamToIgnore:
                items.append(header + param[0] + "=" + param[1])
        return path + "".join(items)
    
    def _getValueForKeyInList(self, paramList, key):
        for param in paramList:
            if param[0] == key:
                return param[1]        
        return None
    
    def __init__(self, signer):
        self.signer = signer
        
    def addTokenToParamList(self, path, paramList, expiryTime):
        '''
        This call adds a "token" parameter to the end of the list of parameters.
        For Guacamole we don't use the "path" parameter.  Note that it likely
        needs to be path + "?" for most applications.
        '''        
        expiryTimeString = smMakeDateString(expiryTime)
        paramList.append(("expiryTime", expiryTimeString))
        data = self._makeStringFromList(path, paramList, []).encode('utf-8')
        paramList.append(("token", self.signer.signData(data).decode('utf-8')))
        
    def verifyTokenInParamList(self, path, paramList, time):
        '''
        '''
        token = self._getValueForKeyInList(paramList, "token").encode('utf-8')
        expiryTimeString = self._getValueForKeyInList(paramList, "expiryTime")
        expiryTime = smMakeDateFromString(expiryTimeString)
        if expiryTime < time:
            raise Exception("Token expired at %s", expiryTimeString)
        data = self._makeStringFromList(path, paramList, ["token"]).encode('utf-8')
        if not self.signer.verifySignature(token, data):
            raise Exception("Token signature did not verify successfully")
            
