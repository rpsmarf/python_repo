'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 10, 2015

    @author: rpsmarf
'''

import unittest
from sm_controller.models import SmModelResource, SmModelContainer
from django.test.testcases import TransactionTestCase
from sm_controller.test_helper import ScsTestHelper
import json
import logging
from sm_controller.signal_handlers import smTriggerSignalInstall
from sm_controller.test_filenav_base import FileNavigationTestBase

logger = logging.getLogger(__name__) 
smTriggerSignalInstall()

        
class FileNavigationTest(FileNavigationTestBase):
    '''
    This class is derived from a "testless" base class and invokes some
    common tests.  What is tested is driven by the actions of the setUp method.
    
    In a perfect world, the test_xxx methods would not be needed here, but test
    tools get confused about test classes inheriting from other test
    classes so this keeps everything simple at the price of 20 lines of code.
    '''
    def setUp(self):
        self.helper = ScsTestHelper(self)
        self.helper.setupTempFolder()
        self.helper.setupModel()
        self.container = SmModelContainer.objects.get(name="test_container")
        self.container.containerUrl = "local://localhost" + self.helper.tempFolder.folder + "/"
        self.container.save()
        self.resource = SmModelResource.objects.get(name="resourcexxx")
        self.resource.parametersJson = json.dumps({"folder": "r_folder/"})
        self.resource.save()
        self.helper.setupRemoteAgentViaIce()
        self.helper.setupRestAuth("root")
        self.cloudTest = False
        
    def tearDown(self):
        self.helper.tearDown()
        TransactionTestCase.tearDown(self)
        
    def test_unicodeFilenames(self):
        self.basetest_unicodeFilenames()

    def test_listContent(self):
        self.basetest_listContent()

    def test_metadataContent(self):
        self.basetest_metadataContent()
    
    def test_moveRename(self):
        self.basetest_moveRename()

    def test_deleteContent(self):
        self.basetest_deleteContent()

    def test_getFolderSize(self):
        self.basetest_getFolderSize()

    def test_download(self):
        self.basetest_download()

    def test_errors(self):
        self.basetest_errors()
            
    def test_tempfile(self):
        self.basetest_tempfile()

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()