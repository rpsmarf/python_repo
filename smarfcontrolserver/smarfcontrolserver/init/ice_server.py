'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 11, 2014

    @author: rpsmarf
'''
import logging
import Ice
from smcommon.globals.sm_global_context import smGlobalContextGet

logger = logging.getLogger(__name__)


SCS_HEARTBEAT_NAME = "SmHeartbeat"


class ScsIceInterface(object):
    '''
    This class encapsulates the ICE endpoint for communication with the remote agents
    '''

    def __init__(self, port):
        '''
        Constructor
        '''
        self.port = port
        
    def startIceServer(self, heartbeatIf, asyncRespHandler):
        try:
            self.ic = smGlobalContextGet().ic
            logger.debug("Using global context ICE object")
        except: # For testing
            self.ic = Ice.initialize()
            logger.warning("Making new global context ICE object")
            
        adapter = self.ic.createObjectAdapterWithEndpoints("HeartbeatAdapter", "default -p " + str(self.port))
        adapter.add(heartbeatIf, self.ic.stringToIdentity(SCS_HEARTBEAT_NAME))
        if asyncRespHandler is not None:
            adapter.add(asyncRespHandler, self.ic.stringToIdentity("SmRemAgAsyncOpResps"))
        adapter.activate()
        
    def stopIceServer(self):
        self.ic.destroy()
