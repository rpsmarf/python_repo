'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 12, 2014

    @author: rpsmarf
'''
import logging
from smraicemsgs import SmRemAgFileOps  # @UnresolvedImport
from smcommon.file_access.data_access_desc import \
    SmDataContainer
import json
from smcommon.globals.sm_global_context import smGlobalContextGet
import uuid
import time
from smraicemsgs import FsError # @UnresolvedImport

logger = logging.getLogger(__name__)


class _SmFileAccessSession(object):
    '''
    Information about each open file access session
    '''
    def __init__(self, fileAcc, timeout):
        self.fileAcc = fileAcc
        self.lastAccessTime = time.time()
        self.openFileDict = {}
        self.timeout = timeout
    
    def connectSession(self):
        self.fileAcc.connect()
    
    def disconnectSession(self):
        for file in self.openFileDict.values():
            file.close()
        self.fileAcc.disconnect()
 
    
class _SmFileOpenRecord(object):
    '''
    Information about each open file access session
    '''
    def __init__(self, f, fileAccessSessionId):
        self.f = f
        self.lastAccessTime = time.time()
        self.fileAccessSessionId = fileAccessSessionId
        

class SmIceFsOps(SmRemAgFileOps): 
    '''
    Implementation of ICE class to provide ICE file operations.
    
    This class implements a filesystem operation handler for remote
    systems to request file operations on an agent.
    
    Because the agent may support multiple filesystems, some of which
    require session-type connections for reasonable performance there is a
    connect/disconnect model which creates a fileAccessor within the 
    ICE "servant" (server side code).
    '''
    def __init__(self):
        self.controller = smGlobalContextGet().copyController
        self.fileAccSessionDict = {}
         
    def raiseFsError(self, e):
        '''
        We assume that e is an exception of type SmFileAccessException
        '''
        raise FsError(422, e.reasonCode, e.path, e.description)
    
    def _getFileAccessor(self, fasId):
        fasRec = self.fileAccSessionDict[fasId]
        fasRec.lastAccessTime = time.time()
        return fasRec.fileAcc
        
    def connect(self, dataContainerJson, timeout, _ctx=None):
        try:
            uuidForFileAcc = str(uuid.uuid4())
            logger.debug("Connecting to " + dataContainerJson + " as " + uuidForFileAcc)
            dataContainer = SmDataContainer.fromJson(dataContainerJson)
            fileAcc = self.controller.makeFileAccessor(dataContainer)
            self.fileAccSessionDict[uuidForFileAcc] = _SmFileAccessSession(fileAcc, timeout)
            fileAcc.connectSession()
            return uuidForFileAcc
        except Exception as e:
            logger.exception("Failed to connect to dataContainter")
            raise e
        
    def disconnect(self, uuidForFileAcc, _ctx=None):
        logger.debug("Disconnecting from " + str(uuidForFileAcc))
        fileAcc = self._getFileAccessor(uuidForFileAcc)
        fileAcc.disconnectSession()
        del self.fileAccSessionDict[uuidForFileAcc]
        
    def delete(self, uuidForFileAcc, pathsToDeleteJson, _ctx=None):
        pathsToDelete = json.loads(pathsToDeleteJson)
        logger.debug("Session[%s] deleting %d path starting with %s", 
                     uuidForFileAcc, len(pathsToDelete), pathsToDelete[0])
        try:
            self._getFileAccessor(uuidForFileAcc).deletePaths(pathsToDelete)
        except Exception as e:
            self.raiseFsError(e)
        return 0
    
    def list(self, uuidForFileAcc, path, recursiveDepth, maxEntries, lastPathReturned, showHidden, _ctx=None):
        # ICE passes string None as "" so let's fix that up here
        if (lastPathReturned == ""):
            lastPathReturned = None
        logger.debug("Session[%s] doing list of %s max=%d depth=%d lastPath=%s showHidden=%s",
                     uuidForFileAcc, path, recursiveDepth, maxEntries, str(lastPathReturned), str(showHidden))
        try:
            l = self._getFileAccessor(uuidForFileAcc).list(path, recursiveDepth, maxEntries, lastPathReturned, showHidden)
        except Exception as e:
            self.raiseFsError(e)
        s = json.dumps(l)
        logger.debug("List results: %s", s)
        return s
    
    def mkdir(self, uuidForFileAcc, path, makeParentFolders, _ctx=None):
        logger.debug("Session[" + uuidForFileAcc + "] doing mkdir of " + path)
        try:
            return json.dumps(self._getFileAccessor(uuidForFileAcc).mkdir(path, makeParentFolders))
        except Exception as e:
            self.raiseFsError(e)

    def du(self, uuidForFileAcc, path, showHidden, _ctx=None):
        logger.debug("Session[" + uuidForFileAcc + "] doing du of " + path)
        try:
            return json.dumps(self._getFileAccessor(uuidForFileAcc).du(path, showHidden))
        except Exception as e:
            self.raiseFsError(e)

    def move(self, uuidForFileAcc, oldpath, newpath, _ctx=None):
        logger.debug("Session[%s] doing move of %s to %s", uuidForFileAcc, oldpath, newpath)
        try:
            return json.dumps(self._getFileAccessor(uuidForFileAcc).move(oldpath, newpath))
        except Exception as e:
            self.raiseFsError(e)

    def openForRead(self, uuidForFileAcc, path, _ctx=None):
        try:
            f = self._getFileAccessor(uuidForFileAcc).open(path, "rb")
        except Exception as e:
            self.raiseFsError(e)            
        fileId = str(uuid.uuid4())
        logger.debug("Session[" + uuidForFileAcc + "] opening for read " + path + " as file " + fileId)
        self.fileAccSessionDict[uuidForFileAcc].openFileDict[fileId] = f
        return fileId

    def openForWrite(self, uuidForFileAcc, path, _ctx=None):
        try:
            f = self._getFileAccessor(uuidForFileAcc).open(path, "wb")
        except Exception as e:
            self.raiseFsError(e)            
        fileId = str(uuid.uuid4())
        logger.debug("Session[" + uuidForFileAcc + "] opening for write " + path + " as file " + fileId)
        self.fileAccSessionDict[uuidForFileAcc].openFileDict[fileId] = f
        return fileId

    def writeData(self, uuidForFileAcc, fileId, data, _ctx=None):
        logger.debug("Session[%s] writing %d bytes via file %s", 
                     uuidForFileAcc, len(data), fileId)
        fileAccRec = self.fileAccSessionDict[uuidForFileAcc]
        f = fileAccRec.openFileDict[fileId]
        # We pass string via ICE because it is much faster than list of byte
        f.write(data)

    def readData(self, uuidForFileAcc, fileId, maxSize, _ctx=None):
        fileAccRec = self.fileAccSessionDict[uuidForFileAcc]
        f = fileAccRec.openFileDict[fileId]
        try:
            s = f.read(maxSize)
        except Exception as e:
            self.raiseFsError(e)            
        logger.debug("Session[" + uuidForFileAcc + "] read " + str(len(s)) + " bytes via file " + fileId)
        return s
    
    def close(self, uuidForFileAcc, fileId, _ctx=None):
        fileAccRec = self.fileAccSessionDict[uuidForFileAcc]
        f = fileAccRec.openFileDict[fileId]
        f.close()
        del fileAccRec.openFileDict[fileId]
        
