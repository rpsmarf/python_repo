'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 10, 2015

    @author: rpsmarf
'''

import unittest
from sm_controller.models import SmModelResource
from django.test.testcases import TransactionTestCase
from sm_controller.test_helper import ScsTestHelper
import json
import logging
from sm_controller.signal_handlers import smTriggerSignalInstall
from sm_controller.test_filenav import \
    FileNavigationTestBase
import time
from sm_controller.api import SmApiResource
from smcommon.tests.dad_test_utils import TempFolderViaFileAcc
from smcommon.globals.sm_copy_controller import SmCopyController
from smcommon.globals.sm_global_context import smGlobalContextInit

logger = logging.getLogger(__name__) 
smTriggerSignalInstall()


class FileNaviS3Test(FileNavigationTestBase):
    '''
    This is derived from FileNavigationTest which provides many (9 at last count)
    tests. This performs the test with a filesystem on the S3 cloud.
    The setup and teardown are specific to the S3 cloud system.
    '''
    def setUp(self):
        self.copyController = SmCopyController(["111", "testagentid"])
        smGlobalContextInit(self.copyController)
        self.helper = ScsTestHelper(self)
        self.helper.setupModel()
        self.helper.setupAwsCloudModel()
        self.resource = SmModelResource.objects.get(name="resourcexxx")
        bucket_name = "scs_test-" + str(int(time.time()))
        self.resource.parametersJson = json.dumps({"bucket": bucket_name})
        self.resource.container = self.helper.cloud_container
        self.resource.save()
        self.helper.setupRestAuth("root")
        self.fileAcc = SmApiResource.makeFileAccFromResource(self.resource)
        self.fileAcc.connectSession()
        # Create the container
        s3If = self.fileAcc.s3If
        s3If.createBucket()
        s3If.createSubFolder(".rpsmarf/") 
        s3If.createSubFolder(".rpsmarf/tmp/")
        self.helper.tempFolder = TempFolderViaFileAcc(self.fileAcc)
        self.helper.tempFolder.setPrefixToRemove("r_folder/")
        self.cloudTest = True
        
    def tearDown(self):
        self.helper.tearDown()
        TransactionTestCase.tearDown(self)
        self.helper.tempFolder.cleanup()
        self.fileAcc.disconnectSession()
        self.fileAcc.s3If.deleteBucket()
        
    def test_basicAccess(self):
        self.helper.tempFolder.mkdir("")
        self.helper.tempFolder.mkdir("a/")
        self.helper.tempFolder.rmdir("a/")

    def test_unicodeFilenames(self):
        self.basetest_unicodeFilenames()

    def test_listContent(self):
        self.basetest_listContent()

    def test_metadataContent(self):
        self.basetest_metadataContent()
    
    def test_deleteContent(self):
        self.basetest_deleteContent()

    def test_getFolderSize(self):
        self.basetest_getFolderSize()

    def test_download(self):
        self.basetest_download()

    def test_tempfile(self):
        self.basetest_tempfile()
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()