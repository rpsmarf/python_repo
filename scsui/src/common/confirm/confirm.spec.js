describe( 'Confirm dialog', function() {
	beforeEach( module( 'rpsmarf' ) );
	beforeEach (module('rpsmarf.confirm'));
	beforeEach (module('ui.bootstrap'));
	beforeEach(module('confirm/confirm.tpl.html'));
	//$scope, $modalInstance, title, body
	var $scope, $controller, $interval, $modalInstance, title, body;
	//init, starting, prep, running, cleanup and finished
	

	beforeEach(  inject(function(_$controller_, $rootScope, $modal ){
		title = "Test title";
		body = "Test body";
		$scope = $rootScope.$new();
		$modalInstance = {close: function(){return true;}};

		$controller = _$controller_('ConfirmCtrl', {$scope:$scope,$modalInstance: $modalInstance, title: title, body: body });
		
		
	}));

	it('should show the right title and body text and close on "ok" or "cancel" click', function(){
		expect($scope.title).toEqual(title);
		expect($scope.body).toEqual(body);

		spyOn($modalInstance, 'close');

		$scope.ok();

		expect($modalInstance.close).toHaveBeenCalled();

	});

});