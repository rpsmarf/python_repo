'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import logging
import threading
from smcommon.globals.sm_global_context import smGlobalContextGet
import os
from smcommon.exceptions.sm_exc import SmException
from smcommon.exceptions.file_access_exception import SmFileAccessException
import json
from smcommon.exceptions.exec_exception import SmExecutionException
from smcommon.file_access.data_access_desc import SmDataAccessDescriptor
from smcommon.task_runners.task_runner import TASK_STATE_STARTED,\
    TASK_STATE_RUNNING, TASK_STATE_FAILED, TASK_STATE_SUCCESS

logger = logging.getLogger(__name__)
SM_COPY_BUF_SIZE = 100000
THREAD_COUNT = 1 # Used to generate unique names for each copy thread


class SmCopyThread(threading.Thread):
    
    def __init__(self, iceCopier):
        global THREAD_COUNT
        threading.Thread.__init__(self)
        self.name = "copyThread-" + str(THREAD_COUNT)
        THREAD_COUNT += 1 
        self.iceCopier = iceCopier
        
    def run(self):
        self.iceCopier._copyFileOrFolderInThread()


class SmCopy(object):
    '''
    classdocs
    '''

    def __init__(self, srcDad, destDad, doneCallback):
        '''
        Constructor
        '''
        self._done = False
        self.started = False
        self.totalFileSize = 0
        self.bytesCopied = 0
        self._progressCallback = None
        self.doneCallback = doneCallback
        self.srcDad = srcDad
        self.destDad = destDad
        self.buffer_size = SM_COPY_BUF_SIZE
        self.quit = False
        self.state = TASK_STATE_STARTED
         
    def start(self):
        self.thread = SmCopyThread(self) 
        self.thread.start()
        self.state = TASK_STATE_RUNNING

    def setProgressCallback(self, callback):
        '''
        Registers an object to report progress to.
        The callback is of the form:
        <object>.progressCallback(<copier>, <progress>)
        where <progress> is a real number from 0.0 to 100.0
        '''
        self._progressCallback = callback
        
    def _setFileSize(self, fileSize):
        self.totalFileSize += fileSize

    def _calcProgress(self):
        if (self.totalFileSize == 0):
            return 0
        return round((self.bytesCopied * 100.0) / self.totalFileSize, 1)

    def _addProgress(self, bytesCopied):
        self.bytesCopied += bytesCopied
        if (self._progressCallback is not None):
            progress = self._calcProgress()
            self._progressCallback(self, {"progress": progress})
            
    def _setProgress(self, totalBytesCopied):
        self.bytesCopied = totalBytesCopied
        if (self._progressCallback is not None):
            self._progressCallback(self, self._calcProgress())
            
    def getStatus(self):
        '''
        '''
        return {"progress": (self._calcProgress()),
                "done": self._done,
                "state": self.state,
                }

    def cancel(self):
        self.quit = True
    
    def isDone(self):
        return self._done
    
    def _copyDone(self, newState, resultDict):
        logger.debug("_copyDone called with newState=%s", newState)
        
        # If it is a success and there are other copies to do then ignore this 
        # "done" event
        if newState == TASK_STATE_SUCCESS:
            if self.numCopyOperationsOutstanding > 1:
                self.numCopyOperationsOutstanding -= 1
                return
            
        self._done = True
        self.state = newState
        if self.doneCallback is not None:
            logger.debug("Invoking doneCallback, %s", json.dumps(resultDict))
            self.doneCallback(self, newState, resultDict)
            
    def __str__(self):
        return self.__class__.__name__

    def _copyFileInThread(self, srcFileAcc, srcPath, destFileAcc, destPath):
        fin = None
        fout = None
        try:
            fin = srcFileAcc.open(srcPath, "rb")
            fout = destFileAcc.open(destPath, "wb")
        
            while not self.quit:
                data = fin.read(self.buffer_size)
                dataLen = len(data)
                if (dataLen == 0):
                    break
                self._addProgress(dataLen)
                fout.write(data)
                self.state = TASK_STATE_SUCCESS
        except Exception as e:
            logger.error("Failure in copy of %s to %s", srcPath, destPath, exc_info=True)
            e2 = SmExecutionException.makeException(e)
            self._copyDone(TASK_STATE_FAILED, e2.toDict())
            raise e2
        finally:
            if fin:
                fin.close()
            if fout:
                fout.close()

    def makeSrcDestList(self, srcDad, destDad):
        '''
        There are several scenarios to consider:
        - copy file to file
        - copy file to folder
        - copy folder to folder
        - copy list of files to folder
           -- note that we need to collapse the output to a single folder
           even if the files came from multiple folders
        
        This method builds a list of tuples of source and destination DAD
        records which can then be used to copy
        '''
        srcDadList = srcDad.makeDadSinglePathList()
        if len(srcDadList) == 1:
            return [(srcDadList[0], destDad)]

        # Else we have multiple files
        return_list = []
        for srcDad in srcDadList:
            if srcDad.path.endswith("/"):
                new_name = os.path.basename(srcDad.path[0:len(srcDad.path) - 1])
            else:
                new_name = os.path.basename(srcDad.path)
            return_list.append((srcDad, 
                                SmDataAccessDescriptor(destDad.dataContainer,
                                                       os.path.join(destDad.path, 
                                                                    new_name), 
                                                       destDad.autoDelete)))
        return return_list
    
    def _copyFileOrFolderInThread(self):
        copyCon = smGlobalContextGet().copyController
        srcFileAcc = None
        destFileAcc = None
        try:
            srcFileAcc = copyCon.makeFileAccessor(self.srcDad.dataContainer)
            srcFileAcc.connectSession()
            destFileAcc = copyCon.makeFileAccessor(self.destDad.dataContainer)
            destFileAcc.connectSession()
            
            # The source DAD may refer contain many paths and thus
            # require multiple copies
            listOfSrcDestTuples = self.makeSrcDestList(self.srcDad, self.destDad)
            
            # Get total size
            self.totalFileSize = 0  
            self.numCopyOperationsOutstanding = 0
            for singleSrcDad, _ in listOfSrcDestTuples:
                self.numCopyOperationsOutstanding += 1
                if singleSrcDad.isFile():      
                    flist = srcFileAcc.list(singleSrcDad.path, 1, 1, None, True)
                    self.totalFileSize += flist[0]["size"] 
                else:
                    flist = srcFileAcc.list(singleSrcDad.path + "/", 1000000, 1000000, None, True)
                    for f in flist:
                        self.totalFileSize += f["size"] 
            
            # Copy files
            for singleSrcDad, singleDestDad in listOfSrcDestTuples:
                if singleSrcDad.isFile():
                    logger.debug("Doing single file copy")
                    self._copyFileInThread(srcFileAcc, singleSrcDad.path, 
                                           destFileAcc, singleDestDad.path)
                else:  # Folder copy
                    # List all files in folder
                    logger.debug("Doing multiple file copy")
    
                    if singleSrcDad.path.index("/") != -1:
                        prefixLen = len(os.path.dirname(singleSrcDad.path)) + 1
                    else:
                        prefixLen = 0
                    flist = srcFileAcc.list(singleSrcDad.path + "/", 1000000, 1000000, None, True)
                    for f in flist:
                        # Remove leading path from list operations
                        f["name"] = f["name"][prefixLen:]
                    for f in flist:
                        fname = f["name"]
                        srcPath = os.path.join(singleSrcDad.path, fname)
                        destPath = os.path.join(singleDestDad.path, fname)
                        if f["isDir"]:
                            logger.debug("Creating dir %s", destPath)
                            try:
                                destFileAcc.mkdir(destPath, False)
                            except SmFileAccessException as e:
                                # If the exception in mkdir is due to the folder already existing, ignore it
                                if e.reasonCode != "EEXIST":
                                    raise e
                        else:
                            logger.debug("Copying %s to %s", srcPath, destPath)
                            self._copyFileInThread(srcFileAcc, srcPath, 
                                                   destFileAcc, destPath)
                self._copyDone(TASK_STATE_SUCCESS, {})
        except SmException as e:
            logger.exception("Failure during copy")
            self._copyDone(TASK_STATE_FAILED, e.toDict())
        except Exception as e:
            logger.error("Failure in copy of %s to %s", str(self.srcDad), str(self.destDad), exc_info=True)
            e2 = SmExecutionException.makeException(e)
            self._copyDone(TASK_STATE_FAILED, e2.toDict())
        finally:
            if srcFileAcc:
                srcFileAcc.disconnectSession()
            if destFileAcc:
                destFileAcc.disconnectSession()
            