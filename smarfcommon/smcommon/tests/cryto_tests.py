'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Mar 31, 2015

    @author: rpsmarf
'''

import unittest
from smcommon.crypto.sm_crypto import SmCryptoSigner, SmTokenMaker
from smcommon.utils.date_utils import smMakeDateFromString
import datetime
import os


class CryptoTest(unittest.TestCase):

    def setUp(self):
        if os.environ.get("SMARF_GITDIR") is not None:
            gitdir = os.environ["SMARF_GITDIR"]
        else:
            gitdir = "/home/rpsmarf/git/python_repo"
        key_folder = os.path.join(gitdir, "tools/keys")

        self.signer = SmCryptoSigner(os.path.join(key_folder, "guac_key.private"), 
                                     os.path.join(key_folder, "guac_key.public"))

    def tearDown(self):
        pass

    def testKeyUsage(self):
        data = "Hello".encode("utf-8")
        signature = self.signer.signData(data)
        self.assertTrue(self.signer.verifySignature(signature, data))
        
        self.assertFalse(self.signer.verifySignature("x".encode("utf-8") + signature, data))
        
    def testToken(self):    
        '''
        Sample output data is:
        TOKEN is a=b&config=majumdar-11&expiryTime=20140101T010101Z&token=Q77/EYOMun
        DfsXNKeN2o22ZzXOiQlFhHfNplVp/XGCk51NfgOgCCxkerLlfWlxRrCuS3FBOAK9dkDlbwJgK3rK
        zHQ8bH2D4XZ8Cev+YPC3QfLfyH3akPYwh3IJamN07SEHFe9+KwCN3C6rs4nZjDBsc8XWdzTamvtg
        cfYiFnCYbT+Jw57vI7ayNTsaP5BdcPy3XWZV8pHW1q2xQSnWtdMrry+eCUhxoeiuJPHnkG6YHU8Z
        VysFYV5D/27bldaIbB3qtCJRN9HGL9aRHWRD+T54S9zYwFOqpj+1+U9VP5Obcnilj7YRd2HRyBzK
        J3dxwAKv9qgdQ3kgBAMA4lC+dT1Q==

        '''    
        params = [("a", "b"), ("config", "majumdar-11")]
        path = ""
        now = smMakeDateFromString("20200101T010101Z")
        tokenMaker = SmTokenMaker(self.signer)
        
        tokenMaker.addTokenToParamList(path, params, now)
        self.assertTrue(tokenMaker._getValueForKeyInList(params, "token") is not None)
        print("TOKEN is " + tokenMaker._makeStringFromList(path, params, []))
        
        tokenMaker.verifyTokenInParamList(path, params, now - datetime.timedelta(seconds=1))
        self.assertRaises(Exception, tokenMaker.verifyTokenInParamList, path, 
                          params, now + datetime.timedelta(seconds=1))
        self.assertRaises(Exception, tokenMaker.verifyTokenInParamList, path + "xx",
                          params, now - datetime.timedelta(seconds=1))
        
        params = params[0:len(params) - 1]
        self.assertRaises(Exception, tokenMaker.verifyTokenInParamList, path, 
                          params, now - datetime.timedelta(seconds=1))
        params.append(("token", "abc"))
        self.assertRaises(Exception, tokenMaker.verifyTokenInParamList, path, 
                          params, now - datetime.timedelta(seconds=1))
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testKeyUsage']
    unittest.main()