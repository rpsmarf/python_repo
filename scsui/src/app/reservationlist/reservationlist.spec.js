describe( 'reservation listing page (/reservations/)', function() {
	beforeEach (module('rpsmarf.config'));
	
	beforeEach(module('rpsmarf.reservationlist'));
	beforeEach(module('ui.router'));
	beforeEach(module('restangular'));
	beforeEach(module('rpsmarf.task.create.service'));
	beforeEach(module('rpsmarf.user.service'));
	beforeEach(module('confirm/confirm.tpl.html'));
	
	beforeEach(module('taskprogress/taskprogress.tpl.html'));
	//$scope, $modalInstance, taskObj, $interval,TASK_STATE_ENUM, modalTitle, interactive, $modal, TASK_STATE_FINISHED, TASK_STATE_RUNNING
	var $scope, $controller, SmTaskCreate, $state, $interval, $modalInstance, $modal, smarfurl, taskObject, $httpBackend, responseObj, prefix = 'src/app/reservationlist/';
	var SmUser, $rootScope, Restangular, r;
	//init, starting, prep, running, cleanup and finished
	var now = new Date();
	var sd = new Date(now.getTime() + 3600000);
	var ed = new Date(now.getTime() + 7200000);

	var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	beforeEach(  inject(function(_$controller_, _$rootScope_, _$interval_, _Restangular_, SMARF_URL, _$modal_, _SmTaskCreate_, _$state_, _$httpBackend_, _SmUser_){
		smarfurl = SMARF_URL+'/scs/';
		Restangular = _Restangular_;
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
		SmTaskCreate = _SmTaskCreate_;
		$interval = _$interval_;
		$httpBackend = _$httpBackend_;
		$rootScope = _$rootScope_;
		$scope = $rootScope.$new();
		$modal = _$modal_;
		$controller = _$controller_;
		SmUser = _SmUser_;
		//SmUser.setProperty('id',1);
		responseObj = JSON.parse(__html__[prefix+'reservationlist.spec.json']);
		r = responseObj.objects[1];
		r.start_time = sd.toISOString().substring(0,17)+'00Z';
		r.end_time = ed.toISOString().substring(0,17)+'00Z';
		//The order by is ignored for the mock return results
		$httpBackend.expect('GET',smarfurl+'reservation/?owner__id='+SmUser.getProperty('id')+'&order_by=start_time').respond(200,responseObj);
		$state = {go:function(a,b){}};

		//$scope, Restangular, $modal, SmUser, $state, $interval, SmTaskCreate
		$controller('ReservationListCtrl', {$scope: $scope, $modal:$modal, Restangular:Restangular, SmUser: SmUser, $state:$state , $interval:$interval, SmTaskCreate:SmTaskCreate });
		$httpBackend.flush();
		
	})) ;

	it('should create the list of reservations using the reservationUpdate filter', inject(['reservationUpdateFilter', function(reservationUpdateFilter){
		//Deep copy task using parse
		var reservation = JSON.parse(JSON.stringify(responseObj.objects[1]));
		reservation.type = 'manual';
		reservation.task_name = '-----';
		reservation.manual = true;
		reservation.resource_name = 'DAIR Alberta Computational Resource';
		reservation.startTimeNorm = monthNames[sd.getMonth()] + ' ' + sd.getDate() + ' ' + sd.getFullYear() + ' ' + sd.toTimeString().substring(0,5);
		reservation.endTimeNorm = monthNames[ed.getMonth()] + ' ' + ed.getDate() + ' ' + ed.getFullYear() + ' ' + ed.toTimeString().substring(0,5);
		reservation.status = 'waiting';
		reservation.descriptionJson = JSON.parse(responseObj.objects[1].description);


		expect(reservationUpdateFilter(angular.copy([responseObj.objects[1]]))).toEqual([reservation]);

		sd = new Date(now.getTime()-5400000);
		r = responseObj.objects[1];
		r.start_time = sd.toISOString().substring(0,17)+'00Z';
		r.end_time = ed.toISOString().substring(0,17)+'00Z';
		reservation.startTimeNorm = monthNames[sd.getMonth()] + ' ' + sd.getDate() + ' ' + sd.getFullYear() + ' ' + sd.toTimeString().substring(0,5);
		reservation.status = 'ready';
		reservation.start_time = r.start_time;

		expect(reservationUpdateFilter(angular.copy([responseObj.objects[1]]))).toEqual([reservation]);

		sd = new Date(now.getTime()-7200000);
		ed = new Date(now.getTime()-3600000);
		r = responseObj.objects[1];
		r.start_time = sd.toISOString().substring(0,17)+'00Z';
		r.end_time = ed.toISOString().substring(0,17)+'00Z';
		reservation.start_time = r.start_time;
		reservation.end_time = r.end_time;
		reservation.startTimeNorm = monthNames[sd.getMonth()] + ' ' + sd.getDate() + ' ' + sd.getFullYear() + ' ' + sd.toTimeString().substring(0,5);
		reservation.endTimeNorm = monthNames[ed.getMonth()] + ' ' + ed.getDate() + ' ' + ed.getFullYear() + ' ' + ed.toTimeString().substring(0,5);
		reservation.status = 'completed';

		expect(reservationUpdateFilter(angular.copy([responseObj.objects[1]]))).toEqual([reservation]);
		

	}]));

	it('should handle delete reservation actions', inject(['reservationUpdateFilter', function(reservationUpdateFilter){
		var rid = $scope.reservations[0].id;
		var length = $scope.reservations.length;
		var tid = JSON.parse(responseObj.objects[2].description).task_id;
		$httpBackend.expect('DELETE',smarfurl+'reservation/'+rid+'/').respond(200);

		$scope.deleteReservation($scope.reservations[0],0);
		$rootScope.$apply();

		angular.element('.modal-footer .btn-primary').click();
		$httpBackend.flush();

		expect($scope.reservations.length).toBe(length-1);

		rid = $scope.reservations[1].id;
		$httpBackend.expect('DELETE',smarfurl+'reservation/'+rid+'/').respond(200);
		$httpBackend.expect('DELETE',smarfurl+'task/'+tid+'/').respond(200);

		$scope.cancelReservation($scope.reservations[1],1); //third element is now second due to splice
		$rootScope.$apply();

		angular.element('.modal-footer .btn-primary').click();
		$httpBackend.flush();
		expect($scope.reservations.length).toBe(length-2);

	}]));

});