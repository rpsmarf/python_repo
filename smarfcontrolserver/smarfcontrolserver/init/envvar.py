'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 10, 2014

    @author: rpsmarf
'''
import os
from smcommon.globals.envvar import smCommonClearEnvVar
import json
import logging

SCS_ENV_VAR_MIN_HEARTBEAT_INTERVAL = "SCS_MIN_HEARTBEAT_INTERVAL"
SCS_ENV_VAR_MIN_TIME_AFTER_REMOTE_AGENT_ADD_BEFORE_ALARM = "SCS_MIN_TIME_AFTER_REMOTE_AGENT_ADD_BEFORE_ALARM"
SCS_ENV_VAR_ICE_PORT = "SCS_ICE_PORT"
SCS_ENV_VAR_AUTH_OFF = "SCS_AUTH_OFF"
# Limits on the amount of stdout and stderr to hold in the task object
SCS_ENV_VAR_STDOUT_LIMIT_IN_DB = "SCS_STDOUT_LIMIT_IN_DB"
SCS_ENV_VAR_STDERR_LIMIT_IN_DB = "SCS_STDERR_LIMIT_IN_DB"
# How often to check each resource for old tmp files to autodelete
SCS_ENV_VAR_TMP_EXPIRY_POLL_TIME = "SCS_TMP_EXPIRY_POLL_TIME"
SCS_ENV_VAR_TMP_EXPIRY_EXPIRY_TIME = "SCS_TMP_EXPIRY_EXPIRY_TIME"
SCS_ENV_VAR_RESOURCE_CAP_POLL_TIME = "SCS_RESOURCE_CAP_POLL_TIME"
SCS_ENV_VAR_WARN_THRESH_PRIVATE = "SCS_WARN_THRESH_PRIVATE_CLOUD_FOLDER"
SCS_ENV_VAR_FAIL_THRESH_PRIVATE = "SCS_FAIL_THRESH_PRIVATE_CLOUD_FOLDER"
SCS_ENV_VAR_WARN_THRESH_PUBLIC = "SCS_WARN_THRESH_PUBLIC_CLOUD_FOLDER"
SCS_ENV_VAR_FAIL_THRESH_PUBLIC = "SCS_FAIL_THRESH_PUBLIC_CLOUD_FOLDER"
SCS_ENV_VAR_TASK_DELETE_KEEP_COUNT_PER_USER = "SCS_TASK_DELETE_KEEP_COUNT"
SCS_ENV_VAR_TASK_DELETE_MIN_AGE_IN_SEC = "SCS_TASK_DELETE_MIN_AGE_IN_SEC"
SCS_ENV_VAR_TASK_DELETE_POLL_TIME = "SCS_TASK_DELETE_POLL_TIME"
SCS_ENV_VAR_DEFAULT_USER_AVATAR_URL = "SCS_DEFAULT_USER_AVATAR_URL"
SCS_ENV_VAR_DEFAULT_COMMUNITY_AVATAR_URL = "SCS_DEFAULT_COMMUNITY_AVATAR_URL"
SCS_ENV_VAR_DEFAULT_RESOURCE_AVATAR_URL = "SCS_DEFAULT_RESOURCE_AVATAR_URL"
SCS_ENV_VAR_DEFAULT_RESOURCE_COMPUTE_AVATAR_URL = "SCS_DEFAULT_RESOURCE_COMPUTE_AVATAR_URL"
SCS_ENV_VAR_DEFAULT_TASK_TYPE_AVATAR_URL = "SCS_DEFAULT_TASK_TYPE_AVATAR_URL"
SCS_ENV_VAR_MAILGUN_URL = "SCS_MAILGUN_URL"
SCS_ENV_VAR_MAILGUN_API_KEY = "SCS_MAILGUN_API_KEY"
SCS_ENV_VAR_MAILGUN_FROM = "SCS_MAILGUN_FROM"
SCS_ENV_VAR_MAILGUN_REPLY_TO = "SCS_MAILGUN_REPLY_TO"
SCS_ENV_VAR_FORUM_URL = "SCS_FORUM_URL"
SCS_ENV_VAR_QUEUED_TASK_POLL_TIME = "SCS_QUEUED_TASK_POLL_TIME"
SCS_ENV_VAR_METRIC_SEND_TIME = "SCS_METRIC_SEND_TIME"
SCS_ENV_VAR_METRIC_LONG_TASK_SECS = "SCS_SEC_FOR_LONG_RUNNING_TASK"
SCS_ENV_VAR_NUM_FILES_PER_JOB = "SCS_NUM_FILES_PER_JOB"
SCS_ENV_VAR_EXTRACT_DONE_TIMEOUT = "SCS_EXTRACT_DONE_TIMEOUT"
SCS_ENV_VAR_EXTRACT_POST_URL_BASE = "SCS_EXTRACT_POST_URL_BASE"


SCS_ENV_VAR_CLOUD_STATUS_POLL_TIME = "SCS_CLOUD_STATUS_POLL_TIME"

# This is the time between checks for cloud servers which are still up, but 
# there is no record about the server or the server is "stopped" in the DB.
# Increasing this leaves rogue servers up longer, decreasing it uses more 
# CPU.
SCS_ENV_VAR_CLOUD_ROGUE_SERVER_POLL_TIME = "SCS_CLOUD_ROGUE_SERVER_POLL_TIME"

# Seconds before we shutdown a cloud server which is no longer running a task
# Increasing this makes a future task which uses this resource more likely to start quickly,
# but makes costs a bit higher 
SCS_ENV_VAR_CLOUD_SERVER_SHUTDOWN_HOLDOFF_SEC = "SCS_CLOUD_SERVER_SHUTDOWN_HOLDOFF_SEC"

'''
 Test-related properties
'''
# Used to speed up sending of metrics
SCS_ENV_VAR_METRIC_MINUTE = "SCS_SEC_IN_METRIC_MINUTE"
SCS_ENV_VAR_METRIC_HOUR = "SCS_SEC_IN_METRIC_HOUR"
SCS_ENV_VAR_METRIC_DAY = "SCS_SEC_IN_METRIC_DAY"
# Used to create VMs during unit tests
SCS_ENV_VAR_TEST_AWS_KEY_ID = "SCS_TEST_AWS_KEY_ID" 
SCS_ENV_VAR_TEST_AWS_SECRET_KEY = "SCS_TEST_AWS_SECRET_KEY"
SCS_ENV_VAR_TEST_DAIR_PASSWORD = "SCS_TEST_DAIR_PASSWORD"
SCS_ENV_VAR_TEST_DISABLE_SLOW_TESTS = "SCS_TEST_DISABLE_SLOW_TESTS"
SCS_ENV_VAR_DISQUS_SECRET_KEY = "SCS_DISQUS_SECRET_KEY"
SCS_ENV_VAR_DISQUS_PUBLIC_KEY = "SCS_DISQUS_PUBLIC_KEY"

SM_ENV_VAR_SCS_DEFAULTS = {
                           SCS_ENV_VAR_MIN_HEARTBEAT_INTERVAL: "30",
                           SCS_ENV_VAR_MIN_TIME_AFTER_REMOTE_AGENT_ADD_BEFORE_ALARM: "60",
                           SCS_ENV_VAR_ICE_PORT: "8001",
                           SCS_ENV_VAR_AUTH_OFF: "False",
                           SCS_ENV_VAR_STDOUT_LIMIT_IN_DB: "10000",
                           SCS_ENV_VAR_STDERR_LIMIT_IN_DB: "10000",
                           SCS_ENV_VAR_TMP_EXPIRY_POLL_TIME: "86200",
                           SCS_ENV_VAR_TMP_EXPIRY_EXPIRY_TIME: "86400",
                           SCS_ENV_VAR_RESOURCE_CAP_POLL_TIME: "3517",
                           SCS_ENV_VAR_WARN_THRESH_PRIVATE: "8589934592",
                           SCS_ENV_VAR_FAIL_THRESH_PRIVATE: "10737418240",
                           SCS_ENV_VAR_WARN_THRESH_PUBLIC: "8589934592",
                           SCS_ENV_VAR_FAIL_THRESH_PUBLIC: "10737418240",
                           SCS_ENV_VAR_TASK_DELETE_KEEP_COUNT_PER_USER: "5",
                           # 604800s = 1 week
                           SCS_ENV_VAR_TASK_DELETE_MIN_AGE_IN_SEC: "604800",
                           SCS_ENV_VAR_TASK_DELETE_POLL_TIME: "86500",
                           SCS_ENV_VAR_DEFAULT_USER_AVATAR_URL: 
                           "https://www.rpsmarf.ca/icons-scs/defaultUserIcon.png",
                           SCS_ENV_VAR_DEFAULT_COMMUNITY_AVATAR_URL: 
                           "https://www.rpsmarf.ca/icons-scs/defaultCommunityIcon.png",
                           SCS_ENV_VAR_DEFAULT_RESOURCE_AVATAR_URL: 
                           "assets/icon-default-resource-data.png",
                           SCS_ENV_VAR_DEFAULT_RESOURCE_COMPUTE_AVATAR_URL: 
                           "assets/icon-default-resource-compute.png",
                           SCS_ENV_VAR_DEFAULT_TASK_TYPE_AVATAR_URL: 
                           "assets/icon-default-tool.png",
                           SCS_ENV_VAR_MAILGUN_URL: 
                           "https://api.mailgun.net/v3/mg.rpsmarf.ca/messages",
                           SCS_ENV_VAR_MAILGUN_API_KEY: None,
                           SCS_ENV_VAR_MAILGUN_FROM: "RP-SMARF System <mailgun@mg.rpsmarf.ca>",
                           SCS_ENV_VAR_MAILGUN_REPLY_TO: "RP-SMARF Support <support@rpsmarf.ca>",
                           SCS_ENV_VAR_FORUM_URL: "http://demo.rpsmarf.ca:8000",
                           SCS_ENV_VAR_QUEUED_TASK_POLL_TIME: "60",
                           SCS_ENV_VAR_METRIC_SEND_TIME: "300",
                           SCS_ENV_VAR_METRIC_MINUTE: "60",
                           SCS_ENV_VAR_METRIC_HOUR: "3600",
                           SCS_ENV_VAR_METRIC_DAY: "86400",
                           SCS_ENV_VAR_METRIC_LONG_TASK_SECS: "28800",  # 28800=8 hours
                           SCS_ENV_VAR_CLOUD_STATUS_POLL_TIME: "5",
                           SCS_ENV_VAR_CLOUD_ROGUE_SERVER_POLL_TIME: "600",
                           SCS_ENV_VAR_TEST_AWS_KEY_ID: "unset",
                           SCS_ENV_VAR_TEST_AWS_SECRET_KEY: "unset",
                           SCS_ENV_VAR_TEST_DAIR_PASSWORD: "unset",
                           SCS_ENV_VAR_CLOUD_SERVER_SHUTDOWN_HOLDOFF_SEC: "1800",
                           SCS_ENV_VAR_TEST_DISABLE_SLOW_TESTS: "False",
                           SCS_ENV_VAR_NUM_FILES_PER_JOB: "200",
                           SCS_ENV_VAR_EXTRACT_DONE_TIMEOUT: "400", # 2 sec/file
                           SCS_ENV_VAR_EXTRACT_POST_URL_BASE: "http://localhost",
                           SCS_ENV_VAR_DISQUS_SECRET_KEY: None,
                           SCS_ENV_VAR_DISQUS_PUBLIC_KEY: None,
                          }


def scsClearEnvVar():
    smCommonClearEnvVar()
    for var in SM_ENV_VAR_SCS_DEFAULTS:
        if var in os.environ:
            del os.environ[var]
                

def scsGetEnvVar(name):
    if (name not in os.environ):
        return SM_ENV_VAR_SCS_DEFAULTS[name]
    else:
        return os.environ[name]
    

def scsGetEnvVarFloat(name):
    return float(scsGetEnvVar(name))


def scsGetEnvVarInt(name):
    return int(scsGetEnvVar(name))


def scsGetEnvVarBool(name):
    return scsGetEnvVar(name).lower() == "true"

_SMARF_AUTH_ON = not scsGetEnvVarBool(SCS_ENV_VAR_AUTH_OFF)


def scsApiSetAuthOn(authOn):
    global _SMARF_AUTH_ON
    _SMARF_AUTH_ON = authOn
 
 
def scsApiGetAuthOn():
    global _SMARF_AUTH_ON
    return _SMARF_AUTH_ON
 
 
logger = logging.getLogger(__name__)


def scsLoadAwsKeys():
    try:
        with open('/etc/smarf-scs/keys/aws_keys') as json_data:
            aws_keys = json.load(json_data)
        os.environ[SCS_ENV_VAR_TEST_AWS_KEY_ID] = aws_keys[SCS_ENV_VAR_TEST_AWS_KEY_ID]
        os.environ[SCS_ENV_VAR_TEST_AWS_SECRET_KEY] = aws_keys[SCS_ENV_VAR_TEST_AWS_SECRET_KEY]
    except:
        logger.warning("Key file /etc/smarf-scs/keys/aws_keys was not opened")
        

def scsLoadDairKeys():
    try:
        with open('/etc/smarf-scs/keys/dair_keys') as json_data:
            dair_keys = json.load(json_data)
        os.environ[SCS_ENV_VAR_TEST_DAIR_PASSWORD] = dair_keys[SCS_ENV_VAR_TEST_DAIR_PASSWORD]
        logger.debug("DAIR password is %s", os.environ[SCS_ENV_VAR_TEST_DAIR_PASSWORD])
    except:
        logger.warning("Key file /etc/smarf-scs/keys/dair_keys was not opened")
        

