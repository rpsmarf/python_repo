'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jun 11, 2015

    @author: rpsmarf
'''

import unittest
from smcommon.file_access.swift_accessor import SwiftAccessor
from smcommon.tests.dad_test_utils import TempFolder
import time
from smcommon.utils.date_utils import sm_date_now, sm_date_to_unixtime_sec
from smcommon.logging.sm_logger import smLoggingSetupForTest
import json
import logging
from smcommon.globals.envvar import SM_ENV_VAR_TEST_PASSWORD, smCommonGetEnvVar


logger = logging.getLogger(__name__) 


class Test(unittest.TestCase):

    def setUp(self):
        self.tempFolder = TempFolder()
        smLoggingSetupForTest()
        self.container = "test_container-" + str(int(time.time()))
        
    def tearDown(self):
        try:
            swift_list = self.swift.listInsideContainer("", True, 10000, None)
            swift_list = reversed(swift_list)
            for item in swift_list:
                self.swift.deleteObjectFromContainer(item["name"])
        except:
            logger.debug("Failed to delete container " + self.container)
            
    def dataGen(self):
        yield b'12345'
        yield b'67890'

    def testDairAccess(self):
        '''
        Tests basic use of DAIR cloud storage
        '''
        start_mtime = sm_date_to_unixtime_sec(sm_date_now())
        
        swift = SwiftAccessor("https://swift-ab.dair-atir.canarie.ca:8080/v1/AUTH_4c8f4711207b4b6bb74a1c200e8ac18b",
                              "https://nova-ab.dair-atir.canarie.ca:35357/v2.0/",
                              "OMelendez",
                              smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD),
                              "NEP-Carleton University & Solana Networks", 
                              self.container, 3, "")
        self.swift = swift
        
        #Create the Container (with name 'test_container') First.Expecting a 2XX response if the container doesnt exist already
        self.assertFalse(swift.createContainerSubFolder("") > 299)
        
        #Create a subfolder inside the container 'test_folder'. Expecting 2XX response
        self.assertFalse(swift.createContainerSubFolder("test_folder/") > 299)
        
        #Upload a file named sampleFile.txt top the folder 'test_folder'. Expecting 2XX response 
        self.tempFolder.writeFile("sampleFile.txt", "1234")
        self.assertFalse(swift.uploadContent("test_folder", "sampleFile.txt", b"1234") > 299)
        
        #Upload another file named sampleFile2.txt top the folder 'test_folder'. Expecting 2XX response 
        self.tempFolder.writeFile("sampleFile2.txt", "1234567890")
        self.assertFalse(swift.uploadChunk("test_folder/sampleFile2.txt", self.dataGen) > 299)
        end_mtime = sm_date_to_unixtime_sec(sm_date_now())
        
        list_info = swift.listInsideContainer("", False, 10, None)
        self.assertEqual(1, len(list_info))
        folder_info = list_info[0]
        self.assertEqual("test_folder/", folder_info["name"])
        self.assertEqual(0, folder_info["size"])
        self.assertEqual(True, folder_info["isDir"])
        # Skipping mtime tests due to problems with dev system clocks vs network clocks
        self.assertTrue(True or (folder_info["mtime"] >= start_mtime and folder_info["mtime"] <= end_mtime))

        list_info = swift.listInsideContainer("test_folder/", True, 10, None)
        self.assertEqual(2, len(list_info))
        file_info = list_info[0]
        self.assertEqual("test_folder/sampleFile.txt", file_info["name"], json.dumps(list_info))
        self.assertEqual(4, file_info["size"])
        self.assertEqual(False, file_info["isDir"])
        # Skipping mtime tests due to problems with dev system clocks vs network clocks
        #self.assertTrue(file_info["mtime"] >= start_mtime and file_info["mtime"] <= end_mtime)
        
        file_info = list_info[1]
        self.assertEqual("test_folder/sampleFile2.txt", file_info["name"])
        self.assertEqual(10, file_info["size"])
        self.assertEqual(False, file_info["isDir"])
        # Skipping mtime tests due to problems with dev system clocks vs network clocks
        #self.assertTrue(file_info["mtime"] >= start_mtime and file_info["mtime"] <= end_mtime)

        # Download the content in one shot
        data = swift.downloadContent("test_folder/sampleFile2.txt")
        self.assertEqual(data, b"1234567890")
    
        # Download the file in big chunks
        downloadFileName = self.tempFolder.makeAbsolutePath("download.txt") 
        chunks = swift.downloadAsStream("test_folder/sampleFile2.txt")
        with open(downloadFileName, 'wb') as fd:
            for chunk in chunks:
                fd.write(chunk)
        data = self.tempFolder.readFile("download.txt", 100000)
        self.assertEqual(data, b"1234567890")

        # Download the file in small chunks
        downloadFileName = self.tempFolder.makeAbsolutePath("download.txt") 
        swift.downloadChunkSize = 2
        chunks = swift.downloadAsStream("test_folder/sampleFile2.txt")
        with open(downloadFileName, 'wb') as fd:
            for chunk in chunks:
                fd.write(chunk)
        data = self.tempFolder.readFile("download.txt", 100000)
        self.assertEqual(data, b"1234567890")

        #Now trying to delete the container. This test should fail as all the files need to be individually deleted 
        self.assertRaises(Exception, swift.deleteContainerSubFolder, "")
        
        #Deleting the first File sampleFile.txt. Expecting 2XX response 
        
        self.assertFalse(swift.deleteContainerSubFolder("test_folder/sampleFile.txt") > 299)
        
        #Deleting the second File sampleFile2.txt. Expecting 2XX response 
        
        self.assertFalse(swift.deleteContainerSubFolder("test_folder/sampleFile2.txt") > 299)
        
        #Deleting the container is also not possible until the4 container is empty. As we cannot delete the 
        # subfolder (even if its empty), we cannot delete the container for now. PASS
        
        #create another subfolder named test_folder2. 2XX response expected
        self.assertFalse(swift.createContainerSubFolder("test_folder2/") > 299)

        #create a subfolder inside the subfolder test_folder2
        self.assertFalse(swift.createContainerSubFolder("test_folder2/sub_test_folder1/") > 299)
        
        #now upload the file in the sub sub folder. 2XX response expected 
        self.assertFalse(swift.uploadContent("test_folder2/sub_test_folder1", "sampleFile2.txt", b"1234567890") > 299)    
    
        #now delete the file just uploaded 
        self.assertFalse(swift.deleteContainerSubFolder("test_folder2/sub_test_folder1/sampleFile2.txt") > 299)
        self.assertFalse(swift.deleteContainerSubFolder("test_folder2/sub_test_folder1/") > 299)
        self.assertFalse(swift.deleteContainerSubFolder("test_folder2/") > 299)
        self.assertFalse(swift.deleteContainerSubFolder("test_folder/") > 299)
        self.assertFalse(swift.deleteContainerSubFolder("") > 299)
        
    def testDairList(self):
        '''
        This method tests the list operation which has a number of parameters and modes
        '''
        swift = SwiftAccessor("https://swift-ab.dair-atir.canarie.ca:8080/v1/AUTH_4c8f4711207b4b6bb74a1c200e8ac18b",
                              "https://nova-ab.dair-atir.canarie.ca:35357/v2.0/",
                              "OMelendez",
                              smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD),
                              "NEP-Carleton University & Solana Networks", 
                              self.container, 3, "")
        self.assertFalse(swift.createContainerSubFolder("") > 299)
        start_mtime = sm_date_to_unixtime_sec(sm_date_now())

        # Create tree of folders and files
        self.assertFalse(swift.createContainerSubFolder("d1/") > 299)
        self.assertFalse(swift.uploadContent("d1", "f1a", b"a") > 299)
        self.assertFalse(swift.uploadContent("d1", "f1b", b"a") > 299)
        self.assertFalse(swift.uploadContent("d1", "f1c", b"a") > 299)
        self.assertFalse(swift.createContainerSubFolder("d1/d2/") > 299)
        self.assertFalse(swift.uploadContent("d1/d2", "f2a", b"ab") > 299)
        self.assertFalse(swift.uploadContent("d1/d2", "f2b", b"ab") > 299)
        self.assertFalse(swift.createContainerSubFolder("d1/d2/d3") > 299)
        self.assertFalse(swift.uploadContent("d1/d2/d3", "f3a", b"abc") > 299)
        end_mtime = sm_date_to_unixtime_sec(sm_date_now())

        # List 1 level 
        list_info = swift.listInsideContainer("d1/", False, 100, None)
        self.assertEqual(4, len(list_info))
        
        list_info = swift.listInsideContainer("d1/d2/", False, 100, None)
        self.assertEqual(3, len(list_info))
        
        list_info = swift.listInsideContainer("d1/d2/d3/", False, 100, None)
        self.assertEqual(1, len(list_info))
        
        # List all levels
        list_info = swift.listInsideContainer("d1/d2/", True, 100, None)
        self.assertEqual(4, len(list_info), json.dumps(list_info))

        list_info = swift.listInsideContainer("d1/", True, 100, None)
        self.assertEqual(8, len(list_info))
        recursive_list = list_info
          
        # List recursively folder in chunks of 2 files    
        full_list = []
        last_path = None
        while True:
            list_info = swift.listInsideContainer("d1/", True, 2, last_path)
            if len(list_info) == 0:
                break
            full_list += list_info
            last_path = list_info[-1]["name"]
        self.assertEqual(full_list, recursive_list)
        
        # Get metadata for file
        md_info = swift.getMetadata("d1/f1a")
        self.assertEqual(5, len(md_info))
        self.assertEqual("d1/f1a", md_info["name"])
        self.assertEqual(1, md_info["size"])
        self.assertEqual("rwx", md_info["mode"])
        # Skipping mtime tests due to problems with dev system clocks vs network clocks
        self.assertTrue(True or (md_info["mtime"] >= start_mtime and md_info["mtime"] <= end_mtime))
        
        # Get metadata for folder
        md_info = swift.getMetadata("d1/")
        self.assertEqual(5, len(md_info))
        self.assertEqual("d1/", md_info["name"])
        self.assertEqual(0, md_info["size"])

        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testDairAccess']
    unittest.main()