'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Nov 10, 2014

    @author: rpsmarf
'''
import time
import json
import threading
from smcommon.globals.envvar import \
    SM_ENV_VAR_COMMON_MIN_TIME_BETWEEN_PROGRESS,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_PREP_COUNT,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RUNNING_COUNT,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_CLEANUP_COUNT, \
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RESULT,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_MAX_PROGRESS,\
    SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_MAX_OUTPUT_SIZE, smCommonGetEnvVarInt,\
    smCommonGetEnvVarFloat, smCommonGetEnvVar
import logging

logger = logging.getLogger()


class SmMockOpHandlerBase(object):

    def __init__(self, smApiAsyncResps):
        self.respHandler = smApiAsyncResps
        self.progressPct = None
        self.progressOutputSize = None
        self.startTime = time.time()
        self.state = "prep"
        
    def setProgress(self, **kwargs):
        if "progressPct" in kwargs:
            self.progressPct = kwargs["progressPct"]
    
        if "state" in kwargs:
            self.state = kwargs["state"]
    
        if "progressOutputSize" in kwargs:
            self.progressOutputSize = kwargs["progressOutputSize"]
            
        if "noAccounce" not in kwargs or not kwargs["noAnnounce"]:
            self.announceProgress()
    
    def announceProgress(self):
        progressDict = {}
        progressDict["runtime"] = round(time.time() - self.startTime, 1)
        progressDict["state"] = self.state
        if self.progressPct is not None:
            progressDict["progress"] = self.progressPct
        if self.progressOutputSize is not None and self.progressOutputSize != -1:
            progressDict["outputSize"] = self.progressOutputSize
            
        self.respHandler.progress(self.opId, json.dumps(progressDict))
        
    def announceDone(self, status, respDict={}):
        self.respHandler.done(self.opId, status, json.dumps(respDict))
 
    def announceTaskOutput(self, outputType, outputData):
        self.respHandler.taskOutput(self.opId, outputType, outputData)
 
    
class SmMockOpHandlerRecorder(SmMockOpHandlerBase):
    
    def __init__(self, smApiAsyncResps, opId, opType, opParamsJson):
        SmMockOpHandlerBase.__init__(self, smApiAsyncResps)
        self.opId = opId
        self.opType = opType
        self.opParamsJson = opParamsJson
        self.eventList = []
        self.progressDict = {"runtime": 1.0}
        
    def startOp(self):
        self.eventList.append("start")
    
    def cancelOp(self):
        self.eventList.append("cancel")
    
    def getStatus(self):
        return json.dumps(self.progressDict)
    
    @classmethod
    def waitUntilHandlerStarted(self, mockAsyncOpsServer, opId, timeout):
        endTime = time.time() + timeout
        while endTime < time.time():
            if opId in mockAsyncOpsServer.activeHandlerDict:
                break
            
        handler = mockAsyncOpsServer.activeHandlerDict[opId]
        while endTime < time.time():
            if len(handler.eventList) > 0:
                break
            
    
SM_MOCK_OP_HANDLER_THREAD_COUNT = 1 # Used to generate unique names for each copy thre


class SmMockOpHandlerResponderThread(threading.Thread):
    
    def __init__(self, smMockOpHandlerResponder):
        global SM_MOCK_OP_HANDLER_THREAD_COUNT
        threading.Thread.__init__(self)
        self.name = "mockRespThread-" + str(SM_MOCK_OP_HANDLER_THREAD_COUNT)
        SM_MOCK_OP_HANDLER_THREAD_COUNT += 1 
        self.smMockOpHandlerResponder = smMockOpHandlerResponder
        
    def run(self):
        self.smMockOpHandlerResponder._runInThread()

        
class SmMockOpHandlerResponder(SmMockOpHandlerBase):
    
    def __init__(self, smApiAsyncResps, opId, opType, opParamsJson):
        SmMockOpHandlerBase.__init__(self, smApiAsyncResps)
        self.quit = False
        self.opId = opId
        self.opType = opType
        self.opParamsJson = opParamsJson
        self.thread = SmMockOpHandlerResponderThread(self)
        
    def loopAndProgress(self, loopCount):
        
        if smCommonGetEnvVarInt(SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_MAX_PROGRESS) is not None:
            pctPerLoop = smCommonGetEnvVarInt(SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_MAX_PROGRESS) / loopCount
        if smCommonGetEnvVarInt(SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_MAX_OUTPUT_SIZE) is not None:
            outputPerLoop = smCommonGetEnvVarInt(SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_MAX_OUTPUT_SIZE) / loopCount
        count = 0
        while not self.quit and count < loopCount:
            count += 1
            if smCommonGetEnvVarInt(SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_MAX_PROGRESS) is not None:
                self.setProgress(progressPct=count * pctPerLoop, noAnnounce=True)
            if smCommonGetEnvVarInt(SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_MAX_OUTPUT_SIZE) is not None:
                self.setProgress(progressOutputSize=count * outputPerLoop, noAnnounce=True)
            self.announceProgress()
            time.sleep(smCommonGetEnvVarFloat(SM_ENV_VAR_COMMON_MIN_TIME_BETWEEN_PROGRESS))
        logger.debug("Loop for state %s done", self.state)
    
    def _runInThread(self):
        logger.debug("Thread started")
        self.state = "prep"
        self.loopAndProgress(smCommonGetEnvVarInt(SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_PREP_COUNT))
        self.state = "running"
        self.loopAndProgress(smCommonGetEnvVarInt(SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RUNNING_COUNT))  
        self.state = "cleanup"
        self.loopAndProgress(smCommonGetEnvVarInt(SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_CLEANUP_COUNT))
        self.state = "finished"
        self.announceDone(smCommonGetEnvVar(SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RESULT))        
        logger.debug("Thread finished")
        
    def startOp(self):
        self.thread.start() # Start the thread
    
    def cancelOp(self):
        self.quit = True
    
    def getStatus(self):
        return json.dumps(self.progressDict)
                

class SmMockAsyncOpsServer(object):
    '''
    This is the mock async operations server.  This allows an SCS
    to perform task operations without one or more remote agents.
    '''

    def __init__(self, smApiAsyncResps):
        '''
        Constructor
        '''
        self.handlerClass = SmMockOpHandlerRecorder
        self.activeHandlerDict = {}
        self.smApiAsyncResps = smApiAsyncResps
        
    def startOperation(self, opId, opType, opParamsJson, current=None):
        handler = self.handlerClass(self.smApiAsyncResps, opId, opType, opParamsJson)
        self.activeHandlerDict[opId] = handler
        handler.startOp()
    
    def cancelOperation(self, opId, current=None):
        if opId in self.activeHandlerDict:
            self.activeHandlerDict[opId].cancelOp()
    
    def getStatusOfOperation(self, opId, current=None):
        return self.activeHandlerDict[opId].getStatus()
 
    def getHandler(self, opId):
        return self.activeHandlerDict[opId]
