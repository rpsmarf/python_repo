from distutils.core import setup
import py2exe
import sys
import os
import glob

sys.path.append('source')

# Run in quiet mode if invoked without arguments.
if len(sys.argv) == 1:
    sys.argv.append("py2exe")
    sys.argv.append("-q")



def find_data_files(source,target,patterns):
    """Locates the specified data-files and returns the matches
    in a data_files compatible format.

    source is the root of the source data tree.
        Use '' or '.' for current directory.
    target is the root of the target data tree.
        Use '' or '.' for the distribution directory.
    patterns is a sequence of glob-patterns for the
        files you want to copy.
    """
    if glob.has_magic(source) or glob.has_magic(target):
        raise ValueError("Magic not allowed in src, target")
    ret = {}
    for pattern in patterns:
        pattern = os.path.join(source,pattern)
        for filename in glob.glob(pattern):
            if os.path.isfile(filename):
                targetpath = os.path.join(target,os.path.relpath(filename,source))
                path = os.path.dirname(targetpath)
                ret.setdefault(path,[]).append(filename)
    return sorted(ret.items())


class Target(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        self.version = '0.0.1'
        self.company_name = 'Solana Networks'
        self.copyright = 'Copyright 2014. ALl Rights Reserved.'
        self.name = 'RRAService'
        
service = Target(
    description='RP-SMARF Remote Agent Service',
    modules=['smarf.agent.service'],
    cmdline_style='custom',
    dest_base='rpsra_svc',
    icon_resources=[(1, 'windows_install/sparrow.ico')],
)

gui = Target(
    description='RP-SMARF Remote Agent GUI',
    script='source/smarf/agent/main.py',
    dest_base='rpsra_gui',
    icon_resources=[(1, 'windows_install/sparrow.ico')],
)

setup(
    console=[gui],
    service=[service],
    zipfile='common.zip',
    data_files=find_data_files('windows_install', '', ['*', 'plugins/imageformats/*']),
    options={
        'py2exe': {
            'includes': ['ctypes', 'sip', 'PyQt4.QtCore', 'win32service'],
            'compressed': 1,
            'bundle_files': 3,
        }
    }
)
