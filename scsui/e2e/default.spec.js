describe('default test to ensure web application can run tasks', function() {
	var el, textEl, toolElement, promiseArray = [], btnElement, resources, p;
	it('should allow login, select tool, configure resource, start task', function() {
		
		var selectTool = function(element, container){
			var promise = element.getText();

			return promise.then(function(text){
				if(text === '3-Dimensional Line Generation Tool'){
					return container.element(by.css('button.btn-success'));
				}
			});
		};

		browser.get('/');
		expect(element(by.className('jumbotron')).isPresent()).toBe(true);
		expect(element(by.className('navbar-fixed-top')).isPresent()).toBe(false);
		element(by.id('emaillogin')).sendKeys('a@a.a');
		element(by.id('passlogin')).sendKeys('a');

		element(by.css('button[type="submit"]')).click();
		expect(browser.getCurrentUrl()).toContain('#/home/');
		expect(element(by.className('navbar-fixed-top')).isPresent()).toBe(true);
		expect(element(by.className('sidebar')).isPresent()).toBe(true);

		element.all(by.css('.sidebar li a[href="#/tools/"]')).first().click();

		expect(browser.getCurrentUrl()).toContain('#/tools/');
		expect(element(by.className('navbar-fixed-top')).isPresent()).toBe(false);
		expect(element(by.className('sidebar')).isPresent()).toBe(false);

		toolElement = element.all(by.repeater('tooltask in tooltasks')).then(function(rows){
			
			for(var i=0 ; i< rows.length; i++){
				el = rows[i];


				textEl = el.element(by.css('div.tool-name h5'));
				promiseArray.push(selectTool(textEl, el));
				
			}

			return protractor.promise.all(promiseArray);
			
		});

		toolElement.then(function(resp){
			for(var i=0; i<resp.length; i++){
				if(resp[i]){
					btnElement = resp[i];
					btnElement.click();
				}
			}
		});

		

		expect(browser.getCurrentUrl()).toMatch(/#\/tool\/[a-zA-Z_]+/);

		element(by.repeater('parameter in resParams').row(0)).element(by.css('button.btn-primary')).click();

		expect(browser.getCurrentUrl()).toMatch(/#\/resources\/[a-zA-Z_]+/);

		p = element.all(by.repeater('resource in resources')).then(function(els){
			expect(els.length).toBeGreaterThan(1);
			return els[0].element(by.tagName('a')).click();
		});

		//p.then(function(){});
		//console.log(element(by.css('div.resource-configuration')));
		//console.log(element(by.css('div.resource-configuration resource-nav-header')));
		element(by.css('div.resource-configuration .resource-nav-header input.root-selector-checkbox')).click();

		element(by.css('div.resource-configuration .resource-nav-header button.btn-success')).click();

		expect(browser.getCurrentUrl()).toMatch(/#\/tool\/[a-zA-Z_]+/);
		
		element(by.repeater('parameter in resParams').row(1)).element(by.css('button.btn-primary')).click();

		expect(browser.getCurrentUrl()).toMatch(/#\/resources\/[a-zA-Z_]+/);

		p = element.all(by.repeater('resource in resources')).then(function(els){
			expect(els.length).toBeGreaterThan(1);
			return els[0].element(by.tagName('a')).click();
		});

		element(by.repeater('entry in entries').row(1)).element(by.css('div.entry-container')).click();
		element(by.css('div.resource-configuration .resource-nav-header button.btn-success')).click();
		expect(browser.getCurrentUrl()).toMatch(/#\/tool\/[a-zA-Z_]+/);

		element(by.css('.taskstart-control button.btn-success')).click();

		element(by.css('.modal-footer button.btn-primary')).click();
		expect(browser.getCurrentUrl()).toContain('#/home/');

	});
});

