function ArrayList() {

	this.array = [];

}

ArrayList.prototype.indexOfObject = function(object){
	var obj;
	var eq = false;
	for(var i=this.length-1; i>=0; i--){
		obj = this.array[i];
		eq = false;
		for(var key in obj){
			if(obj[key] != object[key]){
				eq = false;
				break;
			}
			else{
				eq= true;
			}
		}

		if(eq){
			return i;
		}
	}

	return -1;

};


ArrayList.prototype.indexOfObjectWithPropVal = function(propertyName, propertyVal){
	var obj;
	var eq = false;
	for(var i=this.length-1; i>=0; i--){
		obj = this.array[i];
		if(obj[propertyName] == propertyVal){
			return true;
		}
		
	}

	return false;


};

ArrayList.prototype.add = function(object){
	this.array.push(object);
};

ArrayList.prototype.removeAtIndex = function(index){
	this.array.slice(index,1);
};

ArrayList.prototype.removeObject = function(object){
	var obj;
	var eq = false;
	for(var i=this.length-1; i>=0; i--){
		obj = this.array[i];
		eq = false;
		for(var key in obj){
			if(obj[key] != object[key]){
				eq = false;
				break;
			}
			else{
				eq= true;
			}
		}

		if(eq){
			this.array.slice(i,1);
			return true;
		}
	}

	return false;
};

ArrayList.prototype.size = function(object){
	return this.array.length;
};