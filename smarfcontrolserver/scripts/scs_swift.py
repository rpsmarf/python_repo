#!/usr/bin/env python 
# encoding: utf-8
'''
scripts.scs_obj_get -- script to get an object

scripts.scs_obj_get is a description

It defines classes_and_methods

@author:     user_name

@copyright:  2014 organization_name. All rights reserved.

@license:    license

@contact:    user_email
@deffield    updated: Updated
'''

import sys
import os
from scripts.scs_obj_common import scsScriptGetDefaultUrl, scsScriptGetVerify,\
    scsScriptGetHeaders
from requests.packages import urllib3
from smcommon.file_access.swift_accessor import SwiftAccessor
from smcommon.utils.json_utils import dumpsPretty
sys.path.insert(1, "/opt/smarf-scs/scs")

from argparse import ArgumentParser 
from argparse import RawDescriptionHelpFormatter
import requests

__all__ = []
__version__ = 0.1
__date__ = '2014-09-25'
__updated__ = '2014-09-25'

urllib3.disable_warnings()


class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''

    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
        
    def __str__(self):
        return self.msg
    
    def __unicode__(self):
        return self.msg


def scsGetToJson(pathAndQueryString, **kwargs):
    
    if ("url" in kwargs):
        url = kwargs["url"]
    else:
        url = scsScriptGetDefaultUrl()
    url = url + pathAndQueryString
    get_response = requests.get(url=url,
                                headers=scsScriptGetHeaders(), 
                                verify=scsScriptGetVerify())
    get_response.raise_for_status()
    return get_response.json()
    

def scsGetToFile(fileName, pathAndQueryString, **kwargs):
    
    if ("url" in kwargs):
        url = kwargs["url"]
    else:
        url = scsScriptGetDefaultUrl()
    url = url + pathAndQueryString
    with open(fileName, 'wb') as handle:
        response = requests.get(url=url,
                                headers=scsScriptGetHeaders(), 
                                verify=scsScriptGetVerify(), 
                                stream=True)
        response.raise_for_status()
        
        for block in response.iter_content(1024):
            if not block:
                break
        handle.write(block)
    
    
def scsPutFile(fileName, pathAndQueryString, **kwargs):
    
    if ("url" in kwargs):
        url = kwargs["url"]
    else:
        url = scsScriptGetDefaultUrl()
    url = url + pathAndQueryString
    hdrs = scsScriptGetHeaders()
    del(hdrs['Content-Type'])
    with open(fileName, "rb") as fp:
        resp = requests.post(files={'uploadfile': ('name', fp)},
                             url=url,
                             headers=hdrs, 
                             verify=scsScriptGetVerify())  
        resp.raise_for_status()
    

def scsGetObjectId(obj_type, queryString, **kwargs):
    
    if ("url" in kwargs):
        url = kwargs["url"]
    else:
        url = scsScriptGetDefaultUrl()
    url = url + "/scs/" + obj_type + "/?" + queryString
    get_response = requests.get(url=url, 
                                headers=scsScriptGetHeaders(), 
                                verify=scsScriptGetVerify())
    get_response.raise_for_status()
    return get_response.json()["objects"][0]["id"]
    

def scsMakePathFromId(obj_type, objId):
    return "/scs/" + obj_type + "/" + str(objId) + "/"


def scsGetObjectPath(obj_type, queryString, **kwargs):
    objId = scsGetObjectId(obj_type, queryString, **kwargs)
    return scsMakePathFromId(obj_type, objId)
    

def main(argv=None): 
    '''Command line options.
    '''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_shortdesc = "Script to operate on SWIFT objects"
    program_license = '''%s

  Created by Andrew McGregor on %s.
  Copyright 2015 Carleton. All rights reserved.
  
USAGE:
   -a <action> - actions are list or delete
   -d <container> - Container to delete or list.  If not container is specified on list, all containers are listed 
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-c", "--con", dest="container", default="",
                            help="Container to operate on")
        parser.add_argument("-a", "--action", dest="action", default="",
                            help="The action to perform (list or delete)")
        parser.add_argument("-f", "--format", dest="format", default="plain",
                            help="The format - 'plain' or 'json')")
        parser.add_argument("-v", "--verbose", action='store_true', dest="verbose",
                            help="Enable verbose mode")

        # Process arguments
        args = parser.parse_args()

        if args.verbose:
            print("Verbose mode on\n")
            print(("OS_STORAGE_URL: {}\n" +
                  "OS_AUTH_URL: {}\n" +
                  "OS_USERNAME: {}\n" +
                  "OS_PASSWORD: {}\n" +
                  "OS_TENANT: {}\n" +
                  "container: {}\n").format(os.environ["OS_STORAGE_URL"],
                                           os.environ["OS_AUTH_URL"],
                                           os.environ["OS_USERNAME"],
                                           os.environ["OS_PASSWORD"],
                                           os.environ["OS_TENANT"],
                                           args.container)
                  )

        if args.action == "":
            parser.print_help()
            raise CLIError("Action must be specified.\n")

        swiftIf = SwiftAccessor(os.environ["OS_STORAGE_URL"],
                                os.environ["OS_AUTH_URL"],
                                os.environ["OS_USERNAME"],
                                os.environ["OS_PASSWORD"],
                                os.environ["OS_TENANT"],
                                args.container, 3, ""
                                )
        if args.action == "list":
            if args.container == "":
                if args.verbose:
                    print("Listing containers...\n")
                list_info = swiftIf.listContainers()
                if args.format == 'json':
                    print(dumpsPretty(list_info))
                    print("\n")
                else:
                    for rec in list_info:
                        print("{}".format(rec["name"]))
            else:
                if args.verbose:
                    print("Listing containers {}...\n".format(args.container))
                list_info = swiftIf.listInsideContainer("", True, 1000, None)
                if args.format == 'json':
                    print(dumpsPretty(list_info))
                    print("\n")
                else:
                    for rec in list_info:
                        print("{}".format(rec["name"]))
        elif args.action == "delete":
            if args.container == "":
                raise CLIError("Container must be specified for delete.\n")
                
            if args.verbose:
                print("Listing objects in container " + args.container)
            list_info = swiftIf.listInsideContainer("", True, 1000, None)
            if args.verbose:
                print("Found {} objects in container {}".format(len(list_info), args.container))
            for rec in list_info:
                if args.verbose:
                    print("Deleting " + rec["name"])
                swiftIf.deleteObjectFromContainer(rec["name"])
            print("Deleting container " + args.container)
            swiftIf.deleteContainerSubFolder("")
        else:
            raise CLIError("Invalid action " + args.action + ". Must be 'list' or 'delete'")
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + str(e) + "\n")
        sys.stderr.write(indent + "  for help use --help\n")
        return 2

if __name__ == "__main__":
    sys.exit(main())
