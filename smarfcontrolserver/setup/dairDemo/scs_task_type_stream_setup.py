import json
import os
import sys
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath, scsGetToJson
from scripts.scs_obj_create import scsCreateObjectPath

try:
    os.mkdir("/tmp/src")
except:
    pass
try:
    os.mkdir("/tmp/dest")
except:
    pass

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

root_path = scsGetObjectPath("user", "username=rpsmarf")
user_a = scsGetObjectPath("user", "username=a")
user_b = scsGetObjectPath("user", "username=b")


carleton_container_path = scsGetObjectPath("container", "name_key=carleton_demos_data") 
dair_ab_container_path = scsGetObjectPath("container", "name_key=testab1_demos_data")
rt_cloud_files_data_path = scsGetObjectPath("resource_type", "name_key=generic_storage")


TASK_TYPE_VIDEO_STREAM_CFG_DICT = {
                            "args": [ 
                                     {"type": "string",
                                      "name": "Argument", 
                                      "value": "/opt/smarf-demos/demos/video_tool/vlcsnapshot.sh"},
                                     {"type": "resource", 
                                      "name": "Input stream of video data", 
                                      "pathType": "folder", 
                                      "input_id": 1, 
                                      "direction": "input", 
                                      "role": "vsrc", 
                                      "name": "Data to analyze",
                                      "description": "This specifies the folder of data to submit for analysis"},
                                     {"type": "resource", 
                                      "name": "Folder containing snapshot from the video", 
                                      "pathType": "folder", 
                                      "input_id": 2, 
                                      "direction": "output", 
                                      "role": "vdest", 
                                      "name": "Output folder",
                                      "description": "This specified the location to which the output data will be saved"},
                                     ],
                            "computeResourceRole": "vcompute",
                            "interactive": True
                       }
tt_video_path = scsCreateObjectPath("task_type", {"name": "Video Snapshot Tool",
                                                  "name_key": "video_snapshot_tool",
                                                  "description": "" +
                                                  " This tool streams a video to an interactive video viewing application." +
                                                  "The user can take snapshots of the video which are then copied back to " +
                                                  "the folder he defines",
                                                  "code_module": "smcommon.task_runners.task_runner_shell", 
                                                  "code_classname": "SmTaskRunnerShell", 
                                                  "owner": root_path,
                                                  "configurationJson": json.dumps(TASK_TYPE_VIDEO_STREAM_CFG_DICT)})
scsGetToJson(tt_video_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(tt_video_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

rt_video_stream_path = scsCreateObjectPath("resource_type", {"name": "Video Data Source", 
                                                             "name_key": "video_data_source",
                                                             "nature": "streamin",
                                                             "configuration": json.dumps({"pipeFolder": "/tmp"}),
                                                             "description": "This is a source of video data " +
                                                             "for the mock streaming application",
                                                             "code_module": "smcommon.task_runners.task_runner_stream",
                                                             "code_classname": "SmTaskRunnerStreamSource"})

scsCreateObjectPath("resource_type_task_type", {"role": "vsrc",
                                                "task_type": tt_video_path, 
                                                "resource_type": rt_video_stream_path,
                                                "nature": "streamin",       
                                                "name": "Video source stream",
                                                "name_key": "video_source_stream",                                         
                                                "description": "This is the source of data for a video " +
                                                "tool"})

VIDEO_STREAM1_RSRC_CFG = {"type": "stream",
                          "args": [{"type": "resource", "direction": "output", "role": "stream"},
                                   {"type": "string", "value": "-interval"},
                                   {"type": "float", "value": 0.00},
                                   {"type": "string", "value": "-objectsToSendPerInterval"},
                                   {"type": "int", "value": 10000},
                                   {"type": "string", "value": "-pattern"},
                                   {"type": "string", "value": "file_once"},
                                   {"type": "string", "value": "-patternFile"},
                                   {"type": "string", "value": "/opt/smarf-demos/demos/video1/Wildlife_512kb.mp4"},
                                   ]
                          }

res_path = scsCreateObjectPath("resource", {"name": "Video Stream 1", 
                                            "name_key": "testab_demos_data_stream",
                                            "resource_type": rt_video_stream_path,
                                            "owner": root_path,
                                            "container": dair_ab_container_path,
                                            "parametersJson": json.dumps(VIDEO_STREAM1_RSRC_CFG),
                                            "description": "This is the video stream '1'"
                                            })
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=r")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=r")

VIDEO_STREAM2_RSRC_CFG = {"type": "stream",
                          "args": [{"type": "resource", "direction": "output", "role": "stream"},
                                   {"type": "string", "value": "-interval"},
                                   {"type": "float", "value": 0.00},
                                   {"type": "string", "value": "-objectsToSendPerInterval"},
                                   {"type": "int", "value": 10000},
                                   {"type": "string", "value": "-pattern"},
                                   {"type": "string", "value": "file_once"},
                                   {"type": "string", "value": "-patternFile"},
                                   {"type": "string", "value": "/opt/smarf-demos/demos/video2/Bandit.mp4"},
                                   ]}

res_path = scsCreateObjectPath("resource", {"name": "Video Stream 2", 
                                            "name_key": "maj10_demos_data_stream",
                                            "resource_type": rt_video_stream_path,
                                            "owner": root_path,
                                            "container": carleton_container_path,
                                            "parametersJson": json.dumps(VIDEO_STREAM2_RSRC_CFG),
                                            "description": "This is the video stream '2'"
                                            }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=r")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=r")

rt_video_compute_path = scsCreateObjectPath("resource_type", 
                                            {"name": "Video Processor", 
                                             "name_key": "video_processor",
                                             "nature": "compute",
                                             "description": "This is a resource which can run the video " +
                                             "playing application"
                                             })
scsCreateObjectPath("resource_type_task_type", 
                    {"role": "vcompute", 
                     "task_type": tt_video_path, 
                     "resource_type": rt_video_compute_path,
                     "nature": "compute",
                     "name": "Video compute",
                     "name_key": "video_compute",                                         
                     "description": "This is the compute resource for the video capture tool"})

res_path = scsCreateObjectPath("resource", {"name": "Video Runner at DAIR Alberta",
                                            "name_key": "stream_testab1_compute", 
                                            "resource_type": rt_video_compute_path,
                                            "owner": root_path,
                                            "container": dair_ab_container_path,
                                            "parametersJson": 
                                            json.dumps({"taskViewBaseUrl": 
                                                        "http://testab1.rpsmarf.ca:8080/guacamole" +
                                                        "/#/client/c/default"}),
                                            "description": "This is the DAIR-Alberta-based computation " + 
                                            "resource to run the video processing tool"
                                            }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=x")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=x")

# Destination Cloud Folders
scsCreateObjectPath("resource_type_task_type", {"role": "vdest", 
                                                "task_type": tt_video_path, 
                                                "nature": "data",
                                                "name": "Video Destination Folder",
                                                "name_key": "video_dest",                                         
                                                "resource_type": rt_cloud_files_data_path,
                                                "description": "This is the compute resource for the video capture tool"})

 
print("Task type 'stream' objects setup.")
