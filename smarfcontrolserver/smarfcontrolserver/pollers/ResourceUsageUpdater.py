'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Feb 8, 2015

    @author: rpsmarf
'''
import logging
import threading
import time
from sm_controller.models import SmModelResource
import json
from django import db
import sm_controller.api

logger = logging.getLogger(__name__)


class ScsResourceCapUpdaterThread(threading.Thread):
    
    def __init__(self, updater):
        threading.Thread.__init__(self, name="ResourceCapUpdater")
        self.updater = updater

    def run(self):
        while not self.updater.quit:
            try:
                db.close_connection()
                time.sleep(self.updater.pollTimeInSec)
                self.updater.doOneCheck()
            except:
                logger.exception("Error during resource capacity updating")
                time.sleep(10)


class ScsResourceCapUpdater(object):
    '''
    This class implements a timer to purge files which have been abandoned
    in any resource's .rpsmarf/tmp folder
    '''

    def __init__(self, pollTimeInSec):
        '''
        Constructor
        '''
        self.pollTimeInSec = pollTimeInSec
        self.thread = None
        self.quit = False
        self.checkCount = 0
        
    @classmethod
    def updateResourceCapacity(self, resource):
        if resource.capacityJson == "":
            #logger.debug("Aborting due to capJson empty")
            return

        capacityDict = json.loads(resource.capacityJson)
        if not capacityDict.get("pollDiskUsed"):
            #logger.debug("Aborting due to pollDiskUsed")
            return

        # Open connection
        fileAcc = sm_controller.api.SmApiResource.makeFileAccFromResource(resource)
        fileAcc.connectSession()
        
        try:
            # Run the "disk utilization" operation to get the size
            duInfo = fileAcc.du("./")
            if duInfo["result"] != "ok":
                logger.error("DU operation failed: %s", json.dumps(duInfo))
                return
            
            sizeInBytes = duInfo["sizeInBytes"]
            logger.debug("Got duInfo for resource %d: %s", resource.id, json.dumps(duInfo))
            
            # Get the resource again, in case the capacityJson field
            # was changed while the du was running
            resource = SmModelResource.objects.get(id=resource.id)
            capacityDict = json.loads(resource.capacityJson)
            capacityDict["diskUsed"] = sizeInBytes
            
            if sizeInBytes > capacityDict["diskUsedFailThreshold"]:
                capacityDict["diskUsedState"] = "full"
            elif sizeInBytes > capacityDict["diskUsedWarnThreshold"]:
                capacityDict["diskUsedState"] = "warn"
            else:
                capacityDict["diskUsedState"] = "normal"
            
            # Save back the current settings
            resource.capacityJson = json.dumps(capacityDict)
            logger.debug("Updating capJson to: %s", resource.capacityJson)
            
            resource.save()
        except:
            logger.warning("Exception updating capacity for %s", str(resource), exc_info=True)
        finally:
            fileAcc.disconnectSession()
    
    def doOneCheck(self):
        self.checkCount += 1
        logger.debug("Checking capacity for resources...")
        resourceList = SmModelResource.objects.all()
        for resource in resourceList:
            self.updateResourceCapacity(resource)

    def start(self):
        logger.info("Starting ResourceCapUpdater task with pollTime %d", self.pollTimeInSec)
        self.thread = ScsResourceCapUpdaterThread(self)
        self.thread.start()
        
    def stop(self):
        ''' 
        This is only used during unit tests which have a small poll time
        so the "join" is OK
        '''
        logger.info("Stopping ResourceCapUpdater task")
        self.quit = True
        tmp = self.thread.join(2 * self.pollTimeInSec)
        logger.info("Stopped ResourceCapUpdater task")
        return tmp
        