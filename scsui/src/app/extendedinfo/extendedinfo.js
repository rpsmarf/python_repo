angular.module('rpsmarf.extendedinfo',[
	'ui.bootstrap',
	'restangular',
	'rpsmarf.helpers.service'
])
.controller('ExtendedInfoCtrl', ['$scope','$modalInstance', 'tagsObject', 'noteObject','Restangular','SmHelpers', function($scope, $modalInstance, tagsObject, noteObject, Restangular, SmHelpers){

	/***STANDARD VARIABLES***/
	var typeName;

	/***SCOPE VARIABLES***/
	$scope.hasTags = tagsObject === null ? false: true;
	$scope.tags = null;
	$scope.noteText = angular.isDefined(noteObject.noteText) ? noteObject.noteText : '';
	$scope.originalNoteText = $scope.noteText;
	$scope.title = '';
	//$scope.editingNote = false;
	//$scope.noteBtnText = 'Edit';

	if(!$scope.hasTags){
		$scope.title = 'Your Notes';
	}
	else{
		$scope.title = 'Details';
		$scope.tags = tagsObject.tags;
	}

	/***SCOPE FUNCTIONS***/
	$scope.modifyNote = function(){
		var obj;
		
		typeName = noteObject.typeName;
		
		if(noteObject.exists){
			obj = {owner__id:noteObject.ownerId};
			obj[typeName+'__id'] = noteObject.typeId;
			Restangular.one(typeName+'_note',noteObject.typeObject.ext_note_id+'/').patch({note: $scope.noteText}).then(noteSaved);
		}
		else{
			obj = {note: $scope.noteText, owner: '/scs/user/'+noteObject.ownerId+'/' };
			obj[typeName] = '/scs/'+typeName+'/'+noteObject.typeId+'/';
			Restangular.configuration.fullResponse = true;
			Restangular.all(typeName+'_note/').post(obj).then(function(response){
				noteObject.typeObject.ext_note_id =  SmHelpers.getObjectIdFromPathName(response.headers('Location'));
				Restangular.configuration.fullResponse = false;
				noteSaved();
			}, function(error){Restangular.configuration.fullResponse = false;});
		}
	};

	$scope.close = function(){
		$modalInstance.close();
	};

	/***STANDARD FUNCTIONS***/

	var noteSaved = function(){
		noteObject.exists = true;
		noteObject.typeObject.ext_note = $scope.noteText;
	};

}]);