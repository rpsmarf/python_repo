import sys
from PyQt4 import QtGui
from smarf.agent.systray import SystemTrayWidget
import logging
import logging.config

logger = logging.getLogger(__name__)


DEFAULT_LOGGING_DICT = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {
            "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        }
    },

    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "simple"
        }
    },

    "root": {
        "level": "DEBUG",
        "handlers": ["console"]
    }
}

if __name__ == '__main__':
    logging.config.dictConfig(DEFAULT_LOGGING_DICT)
    logger.info("==================================================================")
    logger.info("GUI Started")
    logger.info("==================================================================")    
    
    app = QtGui.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon('sparrow.ico'))
    app.setQuitOnLastWindowClosed(False)
    
    sys_tray = SystemTrayWidget(QtGui.QIcon("sparrow.ico"))
    sys_tray.show()
    sys_tray.showMessage('Welcome to RP-SMARF!', 'Click to get started.')
    
    sys.exit(app.exec_())
