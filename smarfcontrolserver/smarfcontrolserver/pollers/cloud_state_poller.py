'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Dec 8, 2014

    @author: rpsmarf
'''
import logging
import threading
import time
from sm_controller.models import SmModelCloudServer
from django import db
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_STATE_STOPPED,\
    SCS_CLOUDIF_STATE, SCS_CLOUDIF_HOST, SCS_CLOUDIF_STATE_USABLE,\
    SCS_CLOUDIF_STATE_RUNNING, SCS_CLOUDIF_STATE_INUSE,\
    SCS_CLOUDIF_STATE_BOOTING, SCS_CLOUDIF_STATE_INIT
from smarfcontrolserver.cloudif.cloudif_factory import scsCloudServerIfFactoryGet
from sm_controller.api_cloud import scsMakeCloudInfo
import socket
from smcommon.utils.date_utils import sm_date_now
from smarfcontrolserver.init.envvar import scsGetEnvVarFloat,\
    SCS_ENV_VAR_CLOUD_SERVER_SHUTDOWN_HOLDOFF_SEC
from datetime import timedelta

logger = logging.getLogger(__name__)


class ScsCloudStatePollerThread(threading.Thread):
    
    def __init__(self, ageOutObject):
        threading.Thread.__init__(self, name="CloudStatePoller")
        self.ageOutObject = ageOutObject

    def run(self):
        while not self.ageOutObject.quit:
            try:
                db.close_connection()
                time.sleep(self.ageOutObject.pollTimeInSec)
                self.ageOutObject.doOneCheck()
            except:
                logger.exception("Error during cloud state poller thread")
                time.sleep(10)
            

class ScsCloudStatePoller(object):
    '''
    This class implements a poller to find running cloud servers and update their
    state in the Django model. Post-save signals are then used to start something running 
    if required.
    '''

    def __init__(self, pollTimeInSec, transitionToUsableCallback):
        '''
        Constructor
        '''
        self.pollTimeInSec = pollTimeInSec
        self.thread = None
        self.quit = False
        self.checkCount = 0
        self.transitionToUsableCallback = transitionToUsableCallback

    def updateSingleVmState(self, cloud_server):
        doCallback = False
        # Build cloud and cloud_account info
        cloud_info, cloud_account_info = scsMakeCloudInfo(cloud_server)
        cloudIfObj = scsCloudServerIfFactoryGet().getCloudServerIf(cloud_info, cloud_account_info)
        current_info = cloudIfObj.get_server_info(cloud_server.server_id)
        actual_server_state = current_info[SCS_CLOUDIF_STATE]
        
        if actual_server_state == SCS_CLOUDIF_STATE_RUNNING:
            if (cloud_server.state == SCS_CLOUDIF_STATE_RUNNING):
                cloud_server.host = current_info[SCS_CLOUDIF_HOST]
                # Check if the SSH daemon is up and running 
                # If so, then we are actually transitioning to "USABLE"
                logger.debug("Checking if server %d at %s is up",  
                             cloud_server.id,
                             current_info[SCS_CLOUDIF_HOST])
                try:
                    with socket.create_connection((current_info[SCS_CLOUDIF_HOST], 22), 2):
                        pass
                    actual_server_state = SCS_CLOUDIF_STATE_USABLE
                    doCallback = True
                except socket.timeout:
                    logger.debug("Failed to connect to %s:%d", current_info[SCS_CLOUDIF_HOST], 22)                    
                except:
                    logger.exception("Failed to connect to %s:%d", current_info[SCS_CLOUDIF_HOST], 22)
            elif (cloud_server.state == SCS_CLOUDIF_STATE_USABLE):
                # Check if we have been in useable state too long and we should terminate
                logger.debug("Checking for cloud server ageout")
                if (sm_date_now() - cloud_server.last_state_change_time
                        > timedelta(seconds=scsGetEnvVarFloat(SCS_ENV_VAR_CLOUD_SERVER_SHUTDOWN_HOLDOFF_SEC))):
                    logger.info("Terminating server id %s due to non-use",
                        cloud_server.server_id)

                    cloudIfObj.terminate_server(cloud_server.server_id)
                    current_info = cloudIfObj.get_server_info(cloud_server.server_id)
                    if current_info is None:
                        actual_server_state = SCS_CLOUDIF_STATE_STOPPED
                    else:
                        actual_server_state = current_info[SCS_CLOUDIF_STATE]
                else:
                    actual_server_state = cloud_server.state
            elif (cloud_server.state == SCS_CLOUDIF_STATE_INUSE):
                actual_server_state = SCS_CLOUDIF_STATE_INUSE
            elif (cloud_server.state == SCS_CLOUDIF_STATE_BOOTING):
                pass
            elif (cloud_server.state == SCS_CLOUDIF_STATE_INIT):
                pass
            else:
                logger.error("Unknown state '%s' for %s", cloud_server.state, cloud_server)
                return
                
        if cloud_server.state != actual_server_state:
            logger.debug("Server state for server id %s (instance %d) is now '%s'",
                        cloud_server.server_id, cloud_server.id, actual_server_state)
            cloud_server.state = actual_server_state
            cloud_server.last_state_change_time = sm_date_now()
            cloud_server.save()
            
        if doCallback:
            if self.transitionToUsableCallback is not None:
                logger.debug("Called callback due to transition to 'usable'")
                actual_server_state = self.transitionToUsableCallback(cloud_server)
               
    def doOneCheck(self):
        self.checkCount += 1
        serverList = SmModelCloudServer.objects.all().exclude(state=SCS_CLOUDIF_STATE_STOPPED).exclude(server_id="")
        for server in serverList:
            self.updateSingleVmState(server)
            
    def start(self):
        logger.info("Starting CloudStatePoller task")
        self.thread = ScsCloudStatePollerThread(self)
        self.thread.start()
        
    def stop(self):
        ''' 
        This is only used during unit tests which have a small poll time
        so the "join" is OK
        '''
        logger.info("Stopping CloudStatePoller task")
        self.quit = True
        tmp = self.thread.join(2 * self.pollTimeInSec)
        logger.info("Stopped CloudStatePoller task")
        return tmp
        