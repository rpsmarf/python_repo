#!/usr/bin/env python 
'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.
 
    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jan 18, 2015

    
    @author: rpsmarf
'''
import os
import sys
from canarie_service.models import SmModelCanarieServiceProperties
from sm_controller.models import SmModelProperty
sys.path.insert(1, "/opt/smarf-scs/scs")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "smarfcontrolserver.settings")


def create_cs_prop(key, value):
    '''
    This function adds a row to the table of properties used
    by the CANARIE service interface.  See the document at
    
    https://rpsmarf.atlassian.net/wiki/download/attachments/491533/
    Research%20Service%20Support%20for%20the%20CANARIE%20Registry
    %20and%20Monitoring%20System.pdf?version=1&modificationDate=1407442870482&api=v2
    
    for details of the interface.
    '''
    row = SmModelCanarieServiceProperties.objects.create(key=key, value=value)
    row.save()
    
    
def create_prop(key, value):
    '''
    This function adds a row to the table of properties used
    by the UI.
    '''
    row = SmModelProperty.objects.create(key=key, value=value)
    row.save()
    
    
