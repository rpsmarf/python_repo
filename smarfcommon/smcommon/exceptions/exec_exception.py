'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jan 19, 2015

    @author: rpsmarf
'''
import logging
from smcommon.exceptions.sm_exc import SmException

logger = logging.getLogger(__name__)


class SmExecutionException(SmException):
    '''
    This exception is invoked when there is an exception during the execution of 
    a task.
    '''

    def __init__(self, httpCodeToReturn, reasonCode, execPath, description):
        '''
        Constructor
        '''
        super().__init__(httpCodeToReturn, reasonCode, execPath, description)        
 
    @classmethod
    def makeException(cls, e):
        '''
        Create an exception from the exception input
        '''
        if isinstance(e, SmException):
            return e
        e2 = SmExecutionException(422, "EOTHER", "", str(e))
        return e2
