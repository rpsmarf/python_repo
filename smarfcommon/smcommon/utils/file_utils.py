'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Oct 29, 2014

    @author: rpsmarf
'''
import logging

logger = logging.getLogger(__name__)


def sm_safe_close(file):
    '''
    Close the file if it is not "None".  Log exceptions, but do not fail
    '''
    if file:
        try:
            file.close()
        except:
            logger.warning("Exception during close", exc_info=True)