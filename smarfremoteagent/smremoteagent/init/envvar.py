'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 10, 2014

    @author: rpsmarf
'''
import os
from smcommon.globals.envvar import smCommonClearEnvVar

SRA_ENV_VAR_SCS_ICE_HOSTNAME = "SCS_HOST_NAME"
SRA_ENV_VAR_SCS_ICE_PORT = "SCS_HOST_PORT"
SRA_ENV_VAR_HEARTBEAT_INTERVAL = "HEARTBEAT_INTERVAL"
SRA_ENV_VAR_AGENT_GUID = "AGENT_GUID"
SRA_ENV_VAR_SRA_ICE_PORT = "SRA_ICE_PORT"

SM_ENV_VAR_SRA_DEFAULTS = {
                       SRA_ENV_VAR_SCS_ICE_HOSTNAME: "127.0.0.1",
                       SRA_ENV_VAR_SCS_ICE_PORT: "8001",
                       SRA_ENV_VAR_HEARTBEAT_INTERVAL: "15",
                       SRA_ENV_VAR_AGENT_GUID: "LOCAL_AGENT",
                       SRA_ENV_VAR_SRA_ICE_PORT: "9001",
                      }


def scsClearEnvVar():
    smCommonClearEnvVar()
    for var in SM_ENV_VAR_SRA_DEFAULTS:
        if var in os.environ:
            del os.environ[var]
    

def sraGetEnvVar(name):
    if (name not in os.environ):
        return SM_ENV_VAR_SRA_DEFAULTS[name]
    else:
        return os.environ[name]
 
    
def sraGetEnvVarFloat(name):
    return float(sraGetEnvVar(name))


def sraGetEnvVarInt(name):
    return int(sraGetEnvVar(name))