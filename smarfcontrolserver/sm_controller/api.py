'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import json
import logging
from urllib.parse import urlparse

from django.conf.urls import url
from django.contrib.auth.models import User, Group
from django.db.models.fields.related import ForeignKey
from django.http.response import HttpResponse, StreamingHttpResponse
from tastypie import fields
from tastypie.authentication import ApiKeyAuthentication, MultiAuthentication,\
    SessionAuthentication
from tastypie.authorization import Authorization
from tastypie.constants import ALL_WITH_RELATIONS
from tastypie.exceptions import ImmediateHttpResponse, BadRequest
from tastypie.http import HttpBadRequest, HttpForbidden
from tastypie.resources import ModelResource
from tastypie.utils.urls import trailing_slash
import zipstream 

from sm_controller.models import SmModelResourceType, SmModelResource, \
    SmModelFavouriteResource, SmModelTag, SmModelResourceTypeTaskType, SmModelTaskType, \
    SmModelResourceTag, SmModelTask, SmModelAgent, SmModelContainer, \
    SmModelTaskResource, SmModelTaskTypeNote,\
    SmModelResourceNote, SmModelNewsItems, SmModelCommunity, NATURE_CHOICES,\
    SmModelUserSetting, SmModelScript, SmModelProperty, SmModelTaskTypeTag,\
    SmModelFavouriteTaskType, SmModelReservation, SmModelFaq, SmModelRequest,\
    SmModelMdRepo, SmModelSchema, SmModelPropertyType, SmModelPathList,\
    SmModelMdExtractorType, SmModelMdExtractor, SmModelMdProperty,\
    SmModelCloudAccount, SmModelCloudServer, SmModelCloud, SmModelFileSet,\
    SmModelFileSetItem
from smarfcontrolserver.init.envvar import \
    scsApiGetAuthOn
from smarfcontrolserver.utils.mime_types import smUtilsGetMimeTypeForPath 
from smarfcontrolserver.utils.sm_rest_param import SmParamInfo2
from smcommon.file_access.data_access_desc import SmDataContainer
from smcommon.globals.sm_global_context import smGlobalContextGet
from smcommon.tasks.task_desc import SmTaskDesc
from smcommon.utils.date_utils import sm_date_now, smMakeDateFromString,\
    smMakeDateString
import uuid
from smcommon.utils import json_utils, net_utils
from smcommon.utils.json_utils import dumpsPretty
from tastypie.serializers import Serializer
from smcommon.globals.remote_agent_factory import smRemoteAgentAsyncOperationFactory
from os.path import os
from tastypie.validation import Validation
from smcommon.constants.sm_const import SM_TASK_TYPE_DELETE_RECURSIVE_NAME_KEY,\
    SM_COMMUNITY_ALL_NAME_KEY, SM_DATA_CONTAINER_PROTOCOL_SWIFT,\
    SM_DATA_CONTAINER_PROTOCOL_S3
from sm_controller.django_helpers import scsGetTaskIdFromTask,\
    scsGetStartTimeFromTask
from smarfcontrolserver.pollers.ResourceUsageUpdater import ScsResourceCapUpdater
import operator
from sm_controller.authorization import ScsGdnAuthVisibleOnlyToSelf,\
    ScsGdnAuthVisibleToAllEditOnlyByOwner, ScsGdnAuthVisibleToAllAlterWithPerm,\
    ResourceAuthorization, ScsGdnAuthVisibleToAllEditOnlySuperuser,\
    ScsGdnAuthVisibleToAllCreateWithPermEditByOwner, ScsGdnAuthTagLink,\
    ScsGdnAuthVisibleToAllEditOnlyByOwnerOrLinkedObjectOwner
from smcommon.exceptions.sm_exc import SmException, SmHttpUnauthorized
from guardian.shortcuts import assign_perm, remove_perm,\
    get_users_with_perms, get_perms
from actstream.signals import action
from datetime import timedelta
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from smarfcontrolserver.utils.mailer import REQUEST_ACCESS_EMAIL_TEMPLATE,\
    scsGetGlobalMailer, REQUEST_ACCESS_EMAIL_SUBJECT,\
    ACCESS_GRANTED_EMAIL_SUBJECT, ACCESS_GRANTED_EMAIL_TEMPLATE,\
    ACCESS_REMOVED_EMAIL_SUBJECT, ACCESS_REMOVED_EMAIL_TEMPLATE,\
    ACCESS_DENIED_EMAIL_SUBJECT, ACCESS_DENIED_EMAIL_TEMPLATE
from sm_controller import search
from tastypie.http import HttpConflict 
from smarfcontrolserver.init.metric_names import \
    SCS_METRIC_NUM_DOWNLOADS,\
    SCS_METRIC_NUM_LISTS, SCS_METRIC_NUM_DELETES, SCS_METRIC_NUM_ZIP,\
    SCS_METRIC_NUM_UNZIP, SCS_METRIC_NUM_DELETE_RECUR, SCS_METRIC_NUM_MKDIRS,\
    SCS_METRIC_NUM_RENAMES, SCS_METRIC_NUM_MOVES
from statsd.defaults.env import statsd
from smcommon.file_access.file_access_iter import SmFileAccessorIterator,\
    SmZipStreamWrapper
from smcommon.globals.envvar import SM_ENV_VAR_COMMON_GIT_FETCH_COMMAND,\
    SM_ENV_VAR_COMMON_GIT_TAG_GET_COMMAND, SM_ENV_VAR_COMMON_GIT_TAG_LIST_COMMAND, smCommonGetEnvVar
from sm_controller.api_usage import scsGetUsersForObject, scsGetUsageForObject
from sm_controller.api_cloud import scsCloudServerStop, scsCloudServerStart
from sm_controller.api_base import HttpError422, scsMakeExecInfo, HttpOk,\
    HttpConflict409, HttpError522, smRaiseFsOpException
import sm_controller.api_task
import sm_controller.api_extract
from sm_controller.api_extract import \
    SCS_API_EXTRACTION_STATE_DONE, SCS_API_EXTRACTION_STATE_CANCELLING
import csv
import urllib.parse
from smcommon.file_access.cloudif_const import SCS_CLOUDIF_SWIFT_ACC_PASSWORD,\
    SCS_CLOUDIF_TYPE_AWS,\
    SCS_CLOUDIF_TYPE_OPENSTACK, SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY

# TasyPie Resources for the model objects

logger = logging.getLogger(__name__)


class SmarfSerializer(Serializer): 
    json_indent = 4 

    def to_json(self, data, options=None): 
        options = options or {} 
        data = self.to_simple(data, options) 
        return dumpsPretty(data)

    def format_datetime(self, data):
        return data.strftime("%Y-%m-%dT%H:%M:%SZ")

   
def smMakeFilterAllFieldsFilter(x):
    f = {}
    for field in x.__dict__['_meta'].fields: 
        f[field.name] = ALL_WITH_RELATIONS
        if isinstance(field, ForeignKey):
            f[field.name + "_id"] = ALL_WITH_RELATIONS 
    return f


def smMakeOrderingFieldList(x):
    f = []
    for field in x.__dict__['_meta'].fields: 
        if field.name in ['id', 'name', 'name_key', 'start_time']:
            f.append(field.name) 
    return f


SM_EMPTY_PARAM_DICT = {
    "desc": "This URL allows access to files under a resource",
    "params": []
}

        
def scsMakeAuthentication():
    return MultiAuthentication(ApiKeyAuthentication(), SessionAuthentication())
 
   
def scsMakeAuthorization(normalAuthObj):
    if scsApiGetAuthOn():
        return normalAuthObj
    else:
        return Authorization()
    
    
SM_USER_GETPERM_DICT = {
    "desc": "This URL allows gets the permissions for a given user",
    "params": []
}        

SM_USER_SETPERM_DICT = {
    "desc": "This URL allows setting of global permissions for a specific user",
    "params": [{
                "name": "action",
                "type": "string",
                "desc": "Can be 'assign' to add permissions or 'remove' to remove permissions",
                "default": "",
               },
               {
                "name": "perm",
                "type": "string",
                "desc": "The name of the permission to add or remove",
                "default": "",
               }]
}        

SM_USER_ADDEVENT_DICT = {
    "desc": "This URL allows creating events by the user",
    "params": [{
                "name": "verb",
                "type": "string",
                "desc": "The verb in the event (login, logout, use, run, upload, download)",
                "default": "",
               },
               {
                "name": "end_time",
                "type": "string",
                "desc": "The end time (defaults to 'now').  Format is 20010102T235959Z",
                "default": "",
               },
               {
                "name": "count",
                "type": "int",
                "desc": "The number of event to create, each one offset by the 'interval' parameter offset",
                "default": 1,
               },
               {
                "name": "interval",
                "type": "string",
                "desc": "The interval between events in the format <count><unit> "
                "where unit is one of s-seconds, m-minutes, h-hours, d-days (defaults to 1h)",
                "default": "1h",
               },
               {
                "name": "target",
                "type": "string",
                "desc": "The target object for the verb in the API path (e.g. /scs/resource/1/)",
                "default": "",
               }]
}        

    
def scsApiPathToObject(target):
    if target == "":
        return None
    tokens = target.rsplit("/", 3)
    objid = int(tokens[2])
    objtype = tokens[1]
    if objtype == "resource":
        return SmModelResource.objects.get(id=objid)
    if objtype == "task_type":
        return SmModelTaskType.objects.get(id=objid)
    raise Exception("Invalid object type " + objtype)


class SmApiModelResource(ModelResource):
    pass
 
    
class SmApiGroup(SmApiModelResource):
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlySuperuser())
        queryset = Group.objects.all()
        resource_name = 'group'
        filtering = smMakeFilterAllFieldsFilter(Group)
        ordering = smMakeOrderingFieldList(Group)
        serializer = SmarfSerializer()


# SmApiUser resource
class SmApiUser(SmApiModelResource):
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlySuperuser())
        queryset = User.objects.all()
        resource_name = 'user'
        filtering = smMakeFilterAllFieldsFilter(User)
        ordering = smMakeOrderingFieldList(User)
        serializer = SmarfSerializer()

    def obj_create(self, bundle, **kwargs):
        '''
        Special create to set the password with hashing
        '''
        try:
            bundle = super().obj_create(bundle, **kwargs)
            bundle.obj.set_password(bundle.data.get('password'))
            bundle.obj.save() 
        except IntegrityError:
            raise BadRequest('That username already exists')
        
        return bundle
    
    def prepend_urls(self):
        '''
        Taken from http://stackoverflow.com/questions/9449749/filefield-in-tastypie
        See also https://django-tastypie.readthedocs.org/en/v0.9.11/cookbook.html
        '''
        return [
                # See views.py for the upload code
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/setperm%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('setperm_detail'),
                    name="api_setperm_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/getperm%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('getperm_detail'),
                    name="api_getperm_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/addevent%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('addevent_detail'),
                    name="api_addevent_detail"),
                ] 

    def getperm_detail(self, request, **kwargs):
        """
        Get the permissions for a user
        """
        logger.debug("Getting permissions")
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
        paramInfo = SmParamInfo2(SM_USER_GETPERM_DICT, kwargs["pk"], request)
        user = User.objects.get(id=paramInfo.getObjectId())
        resp = []
        perms = user.get_all_permissions()
        for perm in perms:
            resp.append(perm) 
        response = HttpOk(json_utils.dumpsPretty(resp).encode("utf-8"))
        return response 
        
    def setperm_detail(self, request, **kwargs):
        """
        Set permissions for a user
        """
        paramInfo = SmParamInfo2(SM_USER_SETPERM_DICT, kwargs["pk"], request)
        user = User.objects.get(id=paramInfo.getObjectId())
        
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
            if not request.user.is_superuser:
                raise ImmediateHttpResponse(SmHttpUnauthorized("You are not allowed to alter {}"
                                                             .format(str(user))))
        action = paramInfo.getParam("action")
        perm = paramInfo.getParam("perm")
        logger.debug("Setting permissions for task type %d by doing '%s' for perm '%s'",
                     user.id, action, perm)
        
        if action == "assign":
            assign_perm(perm, user, None)                
        elif action == "remove":
            remove_perm(perm, user, None)
        else:
            return HttpBadRequest("Invalid action: {}".format(action))
            
        return HttpOk(json.dumps({}).encode("utf-8"))
        
    def _intervalStringToTimedelta(self, interval):
        if "s" in interval:
            v = int(interval.replace("s", ""))
            return timedelta(seconds=v)
        if "m" in interval:
            v = int(interval.replace("m", ""))
            return timedelta(minutes=v)
        if "h" in interval:
            v = int(interval.replace("h", ""))
            return timedelta(hours=v)
        if "d" in interval:
            v = int(interval.replace("d", ""))
            return timedelta(days=v)
        
    def addevent_detail(self, request, **kwargs):
        """
        Add an event
        """
        paramInfo = SmParamInfo2(SM_USER_ADDEVENT_DICT, kwargs["pk"], request)
        user = User.objects.get(id=paramInfo.getObjectId())
        
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
            if not request.user.is_superuser:
                raise ImmediateHttpResponse(SmHttpUnauthorized("You must be superuser to add an event"))

        verb = paramInfo.getParam("verb")
        end_time_str = paramInfo.getParam("end_time")
        count = paramInfo.getParam("count")
        interval = paramInfo.getParam("interval")
        target = paramInfo.getParam("target")
        target_object = scsApiPathToObject(target)
        logger.debug("Adding %d events with verb %s and target %s",
                     count, verb, target)
        if end_time_str == "":
            next_time = sm_date_now()
        else:
            next_time = smMakeDateFromString(end_time_str)
        delta = self._intervalStringToTimedelta(interval)
        for _ in range(0, count):
            if target_object is not None:
                action.send(user, verb=verb, target=target_object, timestamp=next_time)
            else:
                action.send(user, verb=verb, timestamp=next_time)
            next_time -= delta
        return HttpOk(json.dumps({}).encode("utf-8"))
        

class SmOwnedModelResource(SmApiModelResource):
    def obj_create(self, bundle, **kwargs):
        scsApiSetOwnerField(bundle)
        return super().obj_create(bundle, **kwargs)

    def obj_update(self, bundle, **kwargs):
        '''
        Ensure that we remove any owner info to prevent the owner from changing
        '''
        if "owner" in bundle.data:
            del bundle.data["owner"]
        return super().obj_update(bundle, **kwargs)


# SmModelAgent related resources
class SmApiAgent(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')   
    status = fields.CharField('status', readonly=True)
    last_change = fields.DateTimeField('last_change', readonly=True, null=True, blank=True)

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication() 
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllAlterWithPerm
                                             (permission_code="sm_controller.add_smmodelagent"))
        queryset = SmModelAgent.objects.all()
        resource_name = 'agent'
        filtering = smMakeFilterAllFieldsFilter(SmModelAgent)
        ordering = smMakeOrderingFieldList(SmModelAgent)
        serializer = SmarfSerializer()
        
        
class SmApiCloud(SmApiModelResource):

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlySuperuser())
        queryset = SmModelCloud.objects.all()
        resource_name = 'cloud'
        filtering = smMakeFilterAllFieldsFilter(SmModelCloud)
        ordering = smMakeOrderingFieldList(SmModelCloud)
        serializer = SmarfSerializer()


class SmApiCloudAccount(SmOwnedModelResource):
    cloud = fields.ForeignKey(SmApiCloud, 'cloud')
    owner = fields.ForeignKey(SmApiUser, 'owner')

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlyByOwner())
        queryset = SmModelCloudAccount.objects.all()
        resource_name = 'cloud_account'
        filtering = smMakeFilterAllFieldsFilter(SmModelCloudAccount)
        ordering = smMakeOrderingFieldList(SmModelCloudAccount)
        serializer = SmarfSerializer()

    def dehydrate_accountInfoJson(self, bundle):
        return json.dumps({"message": "Values hidden for security reasons"})


class SmApiContainer(SmOwnedModelResource):
    agent = fields.ForeignKey(SmApiAgent, 'agent')
    owner = fields.ForeignKey(SmApiUser, 'owner')
    status = fields.CharField('status')
    cloud = fields.ForeignKey(SmApiCloud, "cloud", blank=True, null=True)
    cloud_account = fields.ForeignKey(SmApiCloudAccount, "cloud_account", blank=True, null=True)

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllAlterWithPerm
                                             (permission_code="sm_controller.add_smmodelcontainer"))
        queryset = SmModelContainer.objects.all()
        resource_name = 'container'
        filtering = smMakeFilterAllFieldsFilter(SmModelContainer)
        ordering = smMakeOrderingFieldList(SmModelContainer)
        serializer = SmarfSerializer()


class SmApiCloudServer(SmOwnedModelResource):
    cloud_account = fields.ForeignKey(SmApiCloudAccount, 'cloud_account')
    owner = fields.ForeignKey(SmApiUser, 'owner')
    container = fields.ForeignKey(SmApiContainer, 'container')
    status = fields.CharField('state', readonly=True)

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleOnlyToSelf())
        queryset = SmModelCloudServer.objects.all()
        resource_name = 'cloud_server'
        filtering = smMakeFilterAllFieldsFilter(SmModelCloudServer)
        ordering = smMakeOrderingFieldList(SmModelCloudServer)
        serializer = SmarfSerializer()

    def prepend_urls(self):
        '''
        Taken from http://stackoverflow.com/questions/9449749/filefield-in-tastypie
        See also https://django-tastypie.readthedocs.org/en/v0.9.11/cookbook.html
        '''
        return [
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/start%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('start_detail'),
                    name="api_start_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/stop%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('stop_detail'),
                    name="api_stop_detail"),
                ] 

    def start_detail(self, request, **kwargs):
        """
        Starts a cloud server
        """
        paramInfo = SmParamInfo2(SM_EMPTY_PARAM_DICT, kwargs["pk"], request)
        obj = SmModelCloudServer.objects.get(id=paramInfo.getObjectId())
        logger.debug("Starting cloud server for object %s", str(obj))
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
        
        resp = scsCloudServerStart(obj)
        response = HttpOk(json_utils.dumpsPretty(resp).encode("utf-8"))
        return response 
        
    def stop_detail(self, request, **kwargs):
        """
        Stops a cloud server
        """
        paramInfo = SmParamInfo2(SM_EMPTY_PARAM_DICT, kwargs["pk"], request)
        obj = SmModelCloudServer.objects.get(id=paramInfo.getObjectId())
        logger.debug("Stopping a cloud server for object %s", str(obj))
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
        
        resp = scsCloudServerStop(obj)
        response = HttpOk(json_utils.dumpsPretty(resp).encode("utf-8"))
        return response 
        

# SmApiResourceType related resources
class SmApiResourceType(SmApiModelResource):
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlySuperuser()) 
        queryset = SmModelResourceType.objects.all()
        resource_name = 'resource_type'
        filtering = smMakeFilterAllFieldsFilter(SmModelResourceType)
        ordering = smMakeOrderingFieldList(SmModelResourceType)
        serializer = SmarfSerializer()
        
SM_RESOURCE_LIST_PARAM_DICT = {
    "desc": "This URL allows access to files under a resource",
    "params": [{
                "name": "maxEntries",
                "type": "int",
                "desc": "This parameter specifies the maximum number of entries to " + 
                        "return.  If there are more entries, the list operation can " + 
                        "be repeated with the lastPathReturned parameter set to the " + 
                        "name of the last path returned",
                "default": 1000,
               },
               {
                "name": "recursiveDepth",
                "type": "int",
                "desc": "This parameter specifies the maximum depth to walk when " + 
                "listing a folder.  A value of 1 lists only the current folder",
                "default": 1,
               },
               {
                "name": "lastPathReturned",
                "type": "string",
                "desc": "This parameter specifies the next path to return in a 'paged' listing " +
                "of files.  This is done by setting this parameter to the path of the last file/folder " +
                "returned and the list operation will return entries which occur after the path " +
                "specified by this parameter.",
                "default": "",
               },
               {
                "name": "showHidden",
                "type": "bool",
                "desc": "This parameter controls whether hidden files (i.e. files " + 
                "whose names start with the '.' character are shown.  The default is False",
                "default": False,
               },
               ]
}        

SM_RESOURCE_MKDIR_PARAM_DICT = {
    "desc": "This URL allows access to make folders under a resource",
    "params": [{
                "name": "makeParentFolders",
                "type": "bool",
                "desc": "This parameter is used to have the mkdir command create any missing parent folders.",
                "default": False,
               },
               ]
}        

SM_RESOURCE_REQUEST_ACCESS_PARAM_DICT = {
    "desc": "This URL allows access to files under a resource",
    "params": [{
                "name": "reason",
                "type": "string",
                "desc": "This parameter allows the requester to provide an explanation for the access request",
                "default": "",
               }, {
                "name": "access",
                "type": "string",
                "desc": "This parameter specifies what access rights are sought.  One or more of 'r', 'w', 'x'",
                "default": "",
               }, {
                "name": "userid",
                "type": "int",
                "desc": "This parameter is used only when authentication is off and "
                "specifies the user id of the user making an access request",
                "default": -1,
               }]
}        

SM_RESOURCE_UPLOAD_PARAM_DICT = {
    "desc": "This URL allows access to files under a resource",
    "params": [{
                "name": "makeTempPath",
                "type": "bool",
                "desc": "This parameter, if True, uploads to a generated temp folder.  Note that" +
                " file will be deleted after a day or so,",
                "default": False,
               },
               {
                "name": "overwrite",
                "type": "bool",
                "desc": "This parameter, if True, allows the uploaded file to overwrite " +
                " the file specified",
                "default": False,
               }
               ]
}        

SM_GLOBAL_GETUSAGE_PARAM_DICT = {
    "desc": "This returns all events in JSON or CSV format",
    "params": [{
                "name": "duration",
                "type": "int",
                "desc": "The duration of the period for which events will be returned in days",
                "default": -1,
               },
               {
                "name": "format",
                "type": "string",
                "desc": "This parameter defines the format.  Formats include 'csv' and 'json'.",
                "default": "json",
               },
               {
                "name": "end_time",
                "type": "string",
                "desc": "This parameter defines the end of the period for which data is returned " + 
                "This defaults to 'now'.",
                "default": "",
               }
               ]
}        

SM_GLOBAL_FORMAT_PARAM_DICT = {
    "desc": "This returns data in JSON or CSV format",
    "params": [{
                "name": "format",
                "type": "string",
                "desc": "This parameter defines the format.  Formats include 'csv' and 'json'.",
                "default": "json",
               }]
}        

SM_GLOBAL_BULK_UPLOAD_PARAM_DICT = {
    "desc": "This returns all events in JSON or CSV format",
    "params": [{
                "name": "resource",
                "type": "int",
                "desc": "Resource to associate with md_path records",
                "default": -1,
               },
               {
                "name": "repo",
                "type": "int",
                "desc": "This is the repo into which the records are uploaded",
                "default": -1,
               },
               ]
}        

SM_RESOURCE_ZIP_PARAM_DICT = {
    "desc": "This URL allows zip operations on files under a resource",
    "params": [{
                "name": "makeTempPath",
                "type": "bool",
                "desc": "This parameter if True zips to a generated temp folder.  Note that" +
                " file will be deleted after a day or so,",
                "default": False,
               },
               {
                "name": "zipFilePath",
                "type": "string",
                "desc": "This parameter is the path to the zip file to create",
                "default": "",
               }
               ]
}        

SM_RESOURCE_UNZIP_PARAM_DICT = {
    "desc": "This URL allows unzip operations on a zip file",
    "params": [{
                "name": "zipFilePath",
                "type": "string",
                "desc": "This parameter is the path to the zip file to unzip",
               }
               ]
}        

SM_RESOURCE_RENAME_PARAM_DICT = {
    "desc": "This URL allows a file to be renamed",
    "params": [{
                "name": "newName",
                "type": "string",
                "desc": "This parameter is the new name for the file after renaming",
               }
               ]
}        

SM_RESOURCE_MOVE_PARAM_DICT = {
    "desc": "This URL allows a file to be moved to another name in other folder",
    "params": [{
                "name": "newPath",
                "type": "string",
                "desc": "This parameter is the new path for the file after renaming",
               }
               ]
}        

SM_RESOURCE_GET_PARAM_DICT = {
    "desc": "This URL allows access to perform operations on a resource",
    "params": [{
                "name": "extForUser",
                "type": "int",
                "desc": "This parameter, if set to a user id, adds several fields " +
                " which start with ext_ to simplify the operation of the UI",
                "default": -1,
               },
               ]
}        

SM_RESOURCE_DOWNLOAD_PARAM_DICT = {
    "desc": "This URL allows access to perform operations on a resource",
    "params": [{
                "name": "openInBrowser",
                "type": "bool",
                "desc": "This parameter, if set to True sets the Content-Disposition" +
                "header to allow a file to be viewed in the browser",
                "default": False,
               },
               ]
}        

SM_RESOURCE_GETPERM_DICT = {
    "desc": "This URL allows access to perform operations on a resource",
    "params": []
}        

SM_RESOURCE_SETPERM_DICT = {
    "desc": "This URL allows access to perform operations on a resource",
    "params": [{
                "name": "action",
                "type": "string",
                "desc": "Can be 'assign' to add permissions or 'remove' to remove permissions",
                "default": "",
               },
               {
                "name": "perm",
                "type": "string",
                "desc": "Contains one or more of 'r', 'w' or 'x' to set read, write or run (execute) permissions",
                "default": "",
               },
               {
                "name": "user",
                "type": "string",
                "desc": "The path to the user whose access permissions we are changing",
                "default": "",
               },
               {
                "name": "group",
                "type": "string",
                "desc": "The path to the group whose access permissions we are changing" +
                " Note this should not be combined with user.  Notify is not supported for groups.",
                "default": "",
               },
               {
                "name": "notify",
                "type": "bool",
                "desc": "This parameter, if set to True sends and email to the target user " +
                "informing him or her about the permission granted or removed",
                "default": False,
               }]
}        

SM_RESOURCE_COPY_PARAM_DICT = {
    "desc": "This URL allows copy operations to a destination folder",
    "params": [{
                "name": "srcResource",
                "type": "int",
                "desc": "This parameter is the id of the source resource (e.g. 17)",
               },
               {
                "name": "srcPath",
                "type": "stringlist",
                "desc": "This a parameter which can be repeated which specifies the paths to copy from",
               }               
               ]
}        

SM_RESOURCE_COPY_FROM_FILE_SET_PARAM_DICT = {
    "desc": "This URL allows copy operations to a destination folder",
    "params": [{
                "name": "file_set",
                "type": "int",
                "desc": "This parameter is the id of the file_set to copy from",
               },           
               ]
}        

SM_RESOURCE_MAKEFILE_PARAM_DICT = {
    "desc": "This URL allows copy operations to a destination folder",
    "params": [{
                "name": "format",
                "type": "string",
                "desc": "This is the format of the output data.  Value include csv and json",
               },
               {
                "name": "data",
                "type": "string",
                "desc": "This is the optional JSON formatted string.  " +
                "A file called 'data' can be used if this is not specified",
                "default": "",
               }]
}        

SM_RESOURCE_DOWNLOADZIP_PARAM_DICT = {
    "desc": "This URL allows copy operations to a destination folder",
    "params": [{
                "name": "name",
                "type": "string",
                "desc": "This is the name of the file to download.  Default is 'data.zip'",
                "default": "data.zip",
               },
               {
                "name": "data",
                "type": "string",
                "desc": "This is the optional JSON formatted string.  " +
                "A file called 'data' can be used if this is not specified",
                "default": "",
               }]
}        


def smCheckStringInTuple(key, value, choicesTuple, errors):
    for t in choicesTuple:
        if t[1] == value:
            return
    err = ""
    for t in choicesTuple:
        err += t[1] + " "
    errors[key] = "Must be one of [" + err + "]" 


class SmApiResourceValidation(Validation):
    def is_valid(self, bundle, request=None):
        errors = {}        
        if "resource_type" in bundle.data:
            rt_tokens = bundle.data.get("resource_type").rsplit("/", 2)
            tr_id = int(rt_tokens[1])
            nature = SmModelResourceType.objects.get(pk=tr_id).nature
            smCheckStringInTuple("nature", nature, NATURE_CHOICES, errors)
        
        paramJson = bundle.data.get("parametersJson")
                
        if paramJson is not None:
            try:
                paramDict = json.loads(paramJson)
                if nature == "data":
                    if (paramDict.get("folder") is None and 
                            paramDict.get("bucket") is None and 
                            paramDict.get("container") is None):
                        errors["parametersJson"] = "Must contain key 'folder', 'bucket' or 'container' for resource type " \
                            + str(tr_id)
            except:
                errors["parametersJson"] = "Must be valid JSON"

        return errors
    
    
class ScsDownloadWrapper:
    """Wrapper to convert file-like objects to iterables
    with special disconnect at the end"""

    def __init__(self, filelike, fileAcc, blksize=8192):
        self.filelike = filelike
        self.blksize = blksize
        self.fileAcc = fileAcc

    def _closeFile(self):
        self.filelike.close()
        self.fileAcc.disconnectSession()
        
    def __getitem__(self, key):
        data = self.filelike.read(self.blksize)
        if data:
            return data
        self._closeFile()
        raise IndexError

    def __iter__(self):
        return self

    def __next__(self):
        data = self.filelike.read(self.blksize)
        if data:
            return data
        self._closeFile()
        raise StopIteration

    
class SmApiResource(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    resource_type = fields.ForeignKey(SmApiResourceType, 'resource_type')
    container = fields.ForeignKey(SmApiContainer, 'container')
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = ResourceAuthorization(alter_permission_code="sm_controller.add_smmodelresource",
                                              read_data_permission_code="read_resource_data",
                                              write_data_permission_code="write_resource_data",
                                              run_permission_code="execute_on_resource")
        queryset = SmModelResource.objects.all()
        resource_name = 'resource'
        filtering = smMakeFilterAllFieldsFilter(SmModelResource)
        ordering = smMakeOrderingFieldList(SmModelResource)
        serializer = SmarfSerializer()
        validation = SmApiResourceValidation()
        
    def dehydrate(self, bundle):
        paramInfo = SmParamInfo2(SM_RESOURCE_GET_PARAM_DICT, str(bundle.obj.id), bundle.request)
        #logger.debug("paramInfo.getParam('ext'') = %s", str(paramInfo.getParam("extForUser")))
        extForUser = paramInfo.getParam("extForUser")
        if extForUser != -1:
            #logger.debug("In check for extra stuff, bundle user = %s..", str(bundle.request.user))
            user = User.objects.get(id=extForUser)
            resource = bundle.obj
            note = SmModelResourceNote.objects.filter(resource=resource)\
                .filter(owner=extForUser).first()
            if note is not None:
                bundle.data['ext_note'] = note.note
                bundle.data['ext_note_id'] = note.id
            favourite = SmModelFavouriteResource.objects.filter(resource=resource)\
                .filter(owner=extForUser).first()
            if favourite is not None:
                bundle.data['ext_favourite'] = True
            
            # Show information about the user's cloud account and this resource
            if ((resource.container.cloud is not None) and
                    (resource.container.cloud_account is None)):
                # Check if the cloud_acount is present for this cloud for this user
                cloud_acct_qs = SmModelCloudAccount.objects.filter(owner=user).filter(cloud=resource.container.cloud)
                if cloud_acct_qs.count() == 0:
                    bundle.data['ext_user_cloud_acct'] = "missing"
                else:
                    bundle.data['ext_user_cloud_acct'] = "used"
            else:
                bundle.data['ext_user_cloud_acct'] = "unused"

            tags = SmModelResourceTag.objects.filter(resource=resource)
            if tags.count() > 0:
                tagList = []
                for tagLinkRec in tags:
                    tagList.append(tagLinkRec.tag.tag_name) 
                bundle.data['ext_tags'] = tagList     
                
            # Extended permissions           
            permlist = ""
            perms = get_perms(user, resource)
            for perm in perms:
                if perm == "read_resource_data":
                    permlist += "r"
                if perm == "write_resource_data":
                    permlist += "w"
                if perm == "execute_on_resource":
                    permlist += "x"
            bundle.data['ext_perms'] = permlist
                
        return bundle

    def prepend_urls(self):
        '''
        Taken from http://stackoverflow.com/questions/9449749/filefield-in-tastypie
        See also https://django-tastypie.readthedocs.org/en/v0.9.11/cookbook.html
        '''
        return [
                # See views.py for the upload code
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/getusage%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('getusage_detail'),
                    name="api_getusage_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/getusers%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('getusers_detail'),
                    name="api_getusers_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/setperm%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('setperm_detail'),
                    name="api_setperm_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/getperm%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('getperm_detail'),
                    name="api_getperm_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/download%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('download_detail'),
                    name="api_download_detail"),
                # See views.py for the upload code
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/list%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('list_detail'),
                    name="api_list_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/metadata%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('metadata_detail'),
                    name="api_metadata_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/mkdir%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('mkdir_detail'),
                    name="api_mkdir_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/mkrootdir%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('mkrootdir_detail'),
                    name="api_mkrootdir_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/mktemppath%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('mktemppath_detail'),
                    name="api_mktemppath_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/zip%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('zip_detail'),
                    name="api_zip_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/unzip%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('unzip_detail'),
                    name="api_unzip_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/deleterecursive%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('deleterecursive_detail'),
                    name="api_deleterecursive_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/move%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('move_detail'),
                    name="api_move_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/rename%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('rename_detail'),
                    name="api_rename_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/update%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('update_detail'),
                    name="api_update_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/requestaccess%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('requestaccess_detail'),
                    name="api_requestaccess_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/copy%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('copy_detail'),
                    name="api_copy_detail"),
                url(r"^(?P<resource_name>%s)/downloadfilelist%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('downloadfilelist_detail'),
                    name="api_downloadfilelist_detail"),
                url(r"^(?P<resource_name>%s)/downloadzip%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('downloadzip_detail'),
                    name="api_downloadzip_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/getfoldersize%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('getfoldersize_detail'),
                    name="api_getfoldersize_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/copy_from_file_set%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('copy_from_file_set_detail'),
                    name="api_copy_from_file_set_detail"),
                ] 

    def getusage_detail(self, request, **kwargs):
        """
        Get the permissions for a task type
        """
        logger.debug("Getting usage for resource")
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
        paramInfo = SmParamInfo2(SM_OBJECT_GETUSAGE_DICT, kwargs["pk"], request)
        
        resp = scsGetUsageForObject(paramInfo, 
                                    SmModelResource.objects.get(id=paramInfo.getObjectId()))
        response = HttpOk(json_utils.dumpsPretty(resp).encode("utf-8"))
        return response 
        
    def getusers_detail(self, request, **kwargs):
        """
        Get a list of users for a resource
        """
        logger.debug("Getting usage for resource")
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
        paramInfo = SmParamInfo2(SM_OBJECT_GETUSERS_DICT, kwargs["pk"], request)
        resp = scsGetUsersForObject(paramInfo,
                                    SmModelResource.objects.get(id=paramInfo.getObjectId()))
        response = HttpOk(json_utils.dumpsPretty(resp).encode("utf-8"))
        return response 
        
    def requestaccess_detail(self, request, **kwargs):
        """
        Request access to a resource
        """
        paramInfo = SmParamInfo2(SM_RESOURCE_REQUEST_ACCESS_PARAM_DICT, kwargs["pk"], request)
        if scsApiGetAuthOn():
            self.is_authenticated(request)
            user = request.user    
        else:
            user = User.objects.get(id=paramInfo.getParam("userid"))
           
        resource = SmModelResource.objects.get(id=paramInfo.getObjectId())
        logger.debug("Requesting access for a resource %s for user %s", str(resource), str(user))
        scsRequestAccess(user,
                         paramInfo,
                         resource)
        return HttpOk("{}")
                                 
    def download_detail(self, request, **kwargs):
        """
        Send a file through TastyPie without loading the whole file into
        memory at once. The FileWrapper will turn the file object into an
        iterator for chunks of 8KB.

        No need to build a bundle here only to return a file, lets look into the DB directly
        """
        logger.debug("kwargs_download is %s", kwargs)
        paramInfo = SmParamInfo2(SM_RESOURCE_DOWNLOAD_PARAM_DICT, kwargs["pk"], request)
        pathAfterPk = paramInfo.getPostObjectIdPath()
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        logger.debug("Checking read data authorization")
        self._meta.authorization.check_read_data(self, resourceObject, request)
        fileAcc = None
        try:
            fileAcc = self.makeFileAcc(paramInfo, resourceObject)
            fileAcc.connectSession()
            # Check if listing of folder works - if so it is a folder
            try:
                flist = fileAcc.list(pathAfterPk + "/", 1, 1, None, True)
            except:
                flist = []
            if len(flist) == 1:
                client_filename = os.path.basename(pathAfterPk) + ".zip"
                logger.debug("Making zip stream for dir %s", pathAfterPk)
    
                # Add all files in the folder to the zip file
                flist = fileAcc.list(pathAfterPk + "/", 1000, 1000000, None, True)
                logger.debug("Found %d files in folder", len(flist))
                # Make ZIP Stream - note this special wrapper will disconnect the 
                # fileAccessor at the end
                zip_file = SmZipStreamWrapper(fileAcc,
                                              mode='w', compression=zipstream.ZIP_DEFLATED)

                for file in flist:
                    if not file["isDir"]:
                        fileAcc2 = self.makeFileAcc(paramInfo, resourceObject)
                        fileAcc2.connectSession()
                        file_name = file["name"]
                        zip_file.write_iter(file_name, 
                                            SmFileAccessorIterator(file_name, fileAcc2, True))
                        logger.debug("Writing file %s to the zip stream", file_name)
                content_type = smUtilsGetMimeTypeForPath(client_filename)
                response = StreamingHttpResponse(zip_file, content_type=content_type)
            else:
                flist = fileAcc.list(pathAfterPk, 1, 1, None, True)
                # Note that ScsDownloadWrapper does the fileAcc disconnect
                logger.debug("Making file downloader for path %s", pathAfterPk)
                wrapper = ScsDownloadWrapper(fileAcc.open(pathAfterPk, "rb"), fileAcc)
                content_type = smUtilsGetMimeTypeForPath(pathAfterPk)
                if content_type is None:
                    response = StreamingHttpResponse(wrapper)
                else:
                    response = StreamingHttpResponse(wrapper, content_type=content_type)
                response['Content-Length'] = flist[0]["size"]
                client_filename = os.path.basename(pathAfterPk)

            if paramInfo.getParam("openInBrowser"):
                response['Content-Disposition'] = 'filename="' + client_filename + '"'
            else:
                response['Content-Disposition'] = 'attachment; filename="' + client_filename + '"'
            if scsApiGetAuthOn():
                action.send(request.user, verb="download", target=resourceObject)
            else:
                action.send(resourceObject.owner, verb="download", target=resourceObject)
            statsd.incr(SCS_METRIC_NUM_DOWNLOADS)
            return response 
        except Exception as e:
            smRaiseFsOpException(e, "download", pathAfterPk)
            fileAcc.disconnectSession()
        finally:
            # We cannot disconnect here because the file is still being downloaded - this is why we
            # use an ScsDownloadWrapper, because when we get to the end of the file
            # we automatically disconnect
            pass
        
    @classmethod
    def makeDataContainerFromResource(self, resourceObject, includeFolderInContainerUrl=False):
        containerObject = resourceObject.container
        if containerObject.cloud is not None:
            cloud_account = containerObject.cloud_account
            cloud = cloud_account.cloud
            all_params = {}
            all_params.update(json.loads(resourceObject.parametersJson))
            all_params.update(json.loads(cloud_account.accountInfoJson))
            if cloud.cloud_type == SCS_CLOUDIF_TYPE_OPENSTACK:
                if all_params[SCS_CLOUDIF_SWIFT_ACC_PASSWORD] == "unset":
                    raise Exception("DAIR cloud password not set!")
                scheme = SM_DATA_CONTAINER_PROTOCOL_SWIFT
            elif cloud.cloud_type == SCS_CLOUDIF_TYPE_AWS:
                if all_params[SCS_CLOUDIF_CLOUD_ACCT_AWS_SECRET_ACCESS_KEY] == "unset":
                    raise Exception("AWS secret key not set!")
                scheme = SM_DATA_CONTAINER_PROTOCOL_S3
            else:
                raise Exception("Invalid cloud type: " + cloud.cloud_type)
            all_param_str = urllib.parse.urlencode(all_params)
            conUrl = urllib.parse.urlparse(containerObject.containerUrl)
            extendedUrlString = urllib.parse.urlunparse([scheme, 
                    conUrl[1], 
                    conUrl[2], 
                    conUrl[3], 
                    all_param_str, 
                    conUrl[5]])
        else:
            if includeFolderInContainerUrl:
                params = json.loads(resourceObject.parametersJson)
                extendedUrlString = net_utils.smExtendPathInUrl(containerObject.containerUrl, 
                                                                params["folder"])
            else:
                extendedUrlString = containerObject.containerUrl
        dataContainer = SmDataContainer(containerObject.agent.guid, 
                                        containerObject.agent.agentUrl, 
                                        extendedUrlString)
        return dataContainer

    @classmethod
    def makeFileAccFromResource(self, resourceObject):
        dataContainer = self.makeDataContainerFromResource(resourceObject, True)
        copyController = smGlobalContextGet().copyController
        fileAcc = copyController.makeFileAccessor(dataContainer)
        return fileAcc

    @classmethod
    def makeFileAcc(self, paramInfo, resourceObject=None):
        if resourceObject is None:
            resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        return self.makeFileAccFromResource(resourceObject)

    def list_detail(self, request, **kwargs):
        """
        List the contents of a folder
        """
        logger.debug("kwargs is %s", kwargs)
        paramInfo = SmParamInfo2(SM_RESOURCE_LIST_PARAM_DICT, kwargs["pk"], request)
        pathAfterPk = paramInfo.getPostObjectIdPath() 
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        self._meta.authorization.check_read_data_or_run(self, resourceObject, request)
        if "/." in ("/" + pathAfterPk):
            raise ImmediateHttpResponse(HttpForbidden("Path '" + pathAfterPk + "' may not contain dot folders"))
        fileAcc = None
        try:
            fileAcc = self.makeFileAcc(paramInfo, resourceObject)
            fileAcc.connectSession()
            flist = fileAcc.list(pathAfterPk + "/", 
                                 paramInfo.getParam("recursiveDepth"),
                                 paramInfo.getParam("maxEntries"),
                                 paramInfo.getParam("lastPathReturned"),
                                 paramInfo.getParam("showHidden")
                                 )
            # Add in basename element for UI
            for f in flist:
                name = f["name"]
                tokens = name.split("/")
                if name.endswith("/"):
                    f["basename"] = tokens[len(tokens) - 2] + "/"
                else:
                    f["basename"] = tokens[len(tokens) - 1]
                        
            response = HttpResponse(dumpsPretty(flist), 
                                    content_type="application/json")
            statsd.incr(SCS_METRIC_NUM_LISTS)
            return response 
        except Exception as e:
            smRaiseFsOpException(e, "list", pathAfterPk)
        finally:
            if fileAcc:
                fileAcc.disconnectSession()
           
    def downloadfilelist_detail(self, request, **kwargs):
        """
        Make download file from the data provided - this is run on /scs/resource/
        """
        logger.debug("kwargs is %s", kwargs)
        paramInfo = SmParamInfo2(SM_RESOURCE_MAKEFILE_PARAM_DICT, "1", request)
        fmt = paramInfo.getParam("format")
        data = paramInfo.getParam("data")
        if data == "":
            data = request.FILES["data"].read().decode('utf-8')
        jsonData = json.loads(data)
        if fmt == "json":                        
            response = HttpResponse(dumpsPretty(jsonData), 
                                    content_type="application/json")
            return response 
        elif fmt == "csv":
            stringList = []
            for record in jsonData:
                stringList.append(str(record["resource"]) + "," + record["relative_path"])                        
            response = HttpResponse("\n".join(stringList),
                                    content_type="text/csv")
            return response 
        else:
            return HttpBadRequest("Invalid format parameter value: '" + format + "'")                        
           
    def downloadzip_detail(self, request, **kwargs):
        """
        Make a zip file from the data provided - this is run on /scs/resource/
        """
        logger.debug("kwargs is %s", kwargs)
        paramInfo = SmParamInfo2(SM_RESOURCE_DOWNLOADZIP_PARAM_DICT, "1", request)
        name = paramInfo.getParam("name")
        data = paramInfo.getParam("data")
        logger.debug("name is %s and data is %s", name, data)
        if data == "":
            data = request.FILES["data"].read().decode('utf-8')
            
        # Make ZIP Stream
        zip_file = zipstream.ZipFile(mode='w', compression=zipstream.ZIP_STORED)
        
        resObjMap = {}
        jsonData = json.loads(data)
        logger.debug("Making zip stream for %d files", len(jsonData))
        for record in jsonData:
            res_id_str = str(record["resource"])
            if res_id_str not in resObjMap:
                resourceObject = SmModelResource.objects.get(id=record["resource"].split("/")[3])
                self._meta.authorization.check_read_data(self, resourceObject, request)
                resObjMap[res_id_str] = resourceObject
            fileAcc2 = self.makeFileAcc(paramInfo, resObjMap[res_id_str])
            file_name = record["relative_path"]
            zip_file.write_iter(file_name, 
                                SmFileAccessorIterator(file_name, fileAcc2, False))
            logger.debug("Writing file %s to the zip stream", file_name)
        response = StreamingHttpResponse(zip_file, content_type="application/zip")
        response['Content-Disposition'] = 'attachment; filename="' + name + '"'
        if scsApiGetAuthOn():
            action.send(request.user, verb="downloadzip", target=resourceObject)
        else:
            action.send(resourceObject.owner, verb="downloadzip", target=resourceObject)
        statsd.incr(SCS_METRIC_NUM_DOWNLOADS)
        return response 
           
    def metadata_detail(self, request, **kwargs):
        """
        Return the metadata for a file or folder
        """
        logger.debug("kwargs is %s", kwargs)
        paramInfo = SmParamInfo2(SM_EMPTY_PARAM_DICT, kwargs["pk"], request)
        pathAfterPk = paramInfo.getPostObjectIdPath() 
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        self._meta.authorization.check_read_data(self, resourceObject, request)
        fileAcc = None
        try:
            fileAcc = self.makeFileAcc(paramInfo, resourceObject)
            fileAcc.connectSession()
            try:
                flist = fileAcc.list(pathAfterPk, 1, 1, None, True)
                if len(flist) == 0:
                    raise Exception()
            except:
                # If the operation failed, try it again with the trailing / for cloud-based filesystems
                flist = fileAcc.list(pathAfterPk + "/", 1, 1, None, True)
                
            response = HttpResponse(dumpsPretty(flist), content_type="application/json")
            return response 
        except Exception as e:
            smRaiseFsOpException(e, "metadata", pathAfterPk)
        finally:
            if fileAcc:
                fileAcc.disconnectSession()
        
    def mkdir_detail(self, request, **kwargs):
        """
        Create a folder, possibly with parent folders
        """
        logger.debug("in mkdir_detail kwargs is %s", kwargs)
        paramInfo = SmParamInfo2(SM_RESOURCE_MKDIR_PARAM_DICT, kwargs["pk"], request)
        pathAfterPk = paramInfo.getPostObjectIdPath() 
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        self._meta.authorization.check_write_data(self, resourceObject, request)
        fileAcc = None
        try:
            fileAcc = self.makeFileAcc(paramInfo, resourceObject)
            fileAcc.connectSession()
            makeParents = paramInfo.getParam("makeParentFolders")
            logger.debug("In mkdir creating %s", pathAfterPk)
            resp = fileAcc.mkdir(pathAfterPk, makeParents)
            logger.debug("Got response: %s", json.dumps(resp))
            statsd.incr(SCS_METRIC_NUM_MKDIRS)
            return HttpOk("{}")
        except Exception as e:
            smRaiseFsOpException(e, "mkdir", pathAfterPk)
        finally:
            if fileAcc:
                fileAcc.disconnectSession()
        
    def mkrootdir_detail(self, request, **kwargs):
        """
        Create the root folder of a resource
        """
        paramInfo = SmParamInfo2(SM_EMPTY_PARAM_DICT, 
                                 kwargs["pk"], request)
        pathAfterPk = paramInfo.getPostObjectIdPath() 
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        self._meta.authorization.check_write_data(self, resourceObject, request)
        fileAcc = None
        try:
            fileAcc = self.makeFileAcc(paramInfo, resourceObject)
            fileAcc.connectSession()
            logger.debug("In mkdir creating %s", pathAfterPk)
            resp = fileAcc.mkdir(pathAfterPk, True)
            logger.debug("Got response: %s", json.dumps(resp))
            return HttpOk("{}")
        except Exception as e:
            smRaiseFsOpException(e, "mkrootdir", pathAfterPk)
        finally:
            if fileAcc:
                fileAcc.disconnectSession()
        
    def update_detail(self, request, **kwargs):
        """
        Update the capacityJson field
        """
        paramInfo = SmParamInfo2(SM_EMPTY_PARAM_DICT, kwargs["pk"], request)
        resource = SmModelResource.objects.get(id=paramInfo.getObjectId())
        # We do not check auth as this is not a dangerous operation
        # and write auth is rejected if the cap is full so we would not 
        # be able to use update to go down.
        #self._meta.authorization.check_write_data(self, resource, request)
        ScsResourceCapUpdater.updateResourceCapacity(resource)
        return HttpOk("{}")
        
    def rename_detail(self, request, **kwargs): 
        """
        Rename a file within a folder
        """
        logger.debug("kwargs is %s", kwargs)
        paramInfo = SmParamInfo2(SM_RESOURCE_RENAME_PARAM_DICT, kwargs["pk"], request)
        pathAfterPk = paramInfo.getPostObjectIdPath() 
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        self._meta.authorization.check_write_data(self, resourceObject, request)
        fileAcc = self.makeFileAcc(paramInfo, resourceObject)
        fileAcc.connectSession()
        try:
            newName = paramInfo.getParam("newName")
            if '/' in pathAfterPk:
                newPath = pathAfterPk.rsplit("/")[0] + "/" + newName
            else:
                newPath = newName
            logger.debug("In rename changing %s to %s (path = %s)", pathAfterPk, newName, newPath)
            resp = fileAcc.move(pathAfterPk, newPath)
            logger.debug("Got response: %s", json.dumps(resp))
            statsd.incr(SCS_METRIC_NUM_RENAMES)
            return HttpOk("{}")
        except Exception as e:
            smRaiseFsOpException(e, "rename", pathAfterPk)
        finally:
            fileAcc.disconnectSession()
        
    def move_detail(self, request, **kwargs):
        """
        Move a file to new name in a different folder
        """
        logger.debug("kwargs is %s", kwargs)
        paramInfo = SmParamInfo2(SM_RESOURCE_MOVE_PARAM_DICT, kwargs["pk"], request)
        pathAfterPk = paramInfo.getPostObjectIdPath() 
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        self._meta.authorization.check_write_data(self, resourceObject, request)
        fileAcc = self.makeFileAcc(paramInfo, resourceObject)
        fileAcc.connectSession()
        try:
            newPath = paramInfo.getParam("newPath")
            logger.debug("In rename changing %s to %s (path = %s)", pathAfterPk, newPath, newPath)
            resp = fileAcc.move(pathAfterPk, newPath)
            logger.debug("Got response: %s", json.dumps(resp))
            statsd.incr(SCS_METRIC_NUM_MOVES)
            return HttpOk()
        except Exception as e:
            smRaiseFsOpException(e, "move", pathAfterPk)
        finally:
            fileAcc.disconnectSession()
        
    def getfoldersize_detail(self, request, **kwargs):
        """
        Move a file to new name in a different folder
        """
        logger.debug("kwargs is %s", kwargs)
        paramInfo = SmParamInfo2(SM_EMPTY_PARAM_DICT, kwargs["pk"], request)
        pathAfterPk = paramInfo.getPostObjectIdPath() 
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        self._meta.authorization.check_read_data(self, resourceObject, request)
        fileAcc = self.makeFileAcc(paramInfo, resourceObject)
        fileAcc.connectSession()
        try:
            if pathAfterPk == "":
                pathAfterPk = "./"
            else:
                pathAfterPk = pathAfterPk + "/"
            resp = fileAcc.du(pathAfterPk, False)
            logger.debug("Got response: %s", json.dumps(resp))
            statsd.incr(SCS_METRIC_NUM_MOVES)
            return HttpOk(json.dumps(resp))
        except Exception as e:
            smRaiseFsOpException(e, "getfoldersize", pathAfterPk)
        finally:
            fileAcc.disconnectSession()
        
    def mktemppath_detail(self, request, **kwargs):
        """
        Create a temporary file name.  It is guaranteed unique.  It is automatically
        deleted after a day (or based on environment variable)
        """
        pathGenerated = ".rpsmarf/tmp/" + str(uuid.uuid4())
        logger.debug("Returning HttpOk with path %s", pathGenerated)
        response = HttpOk(json.dumps({"path": pathGenerated,
                           "name": os.path.basename(pathGenerated),
                           "parentPath": os.path.dirname(pathGenerated)}).encode("utf-8"))
        return response 
        
    def getperm_detail(self, request, **kwargs):
        """
        Get the permissions for a resource
        """
        logger.debug("Getting permissions")
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
        paramInfo = SmParamInfo2(SM_RESOURCE_GETPERM_DICT, kwargs["pk"], request)
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        users = get_users_with_perms(resourceObject)
        resp = {}
        for user in users:
            perms = get_perms(user, resourceObject)
            for perm in perms:
                userPath = "/scs/user/" + str(user.id) + "/"
                if userPath in resp:
                    resp[userPath].append(perm)
                else:
                    resp[userPath] = [perm]
        response = HttpOk(json_utils.dumpsPretty(resp).encode("utf-8"))
        return response 
        
    def setperm_detail(self, request, **kwargs):
        """
        Set permissions for an object
        """
        paramInfo = SmParamInfo2(SM_RESOURCE_SETPERM_DICT, kwargs["pk"], request)
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
            if resourceObject.owner != request.user and not request.user.is_superuser:
                raise ImmediateHttpResponse(SmHttpUnauthorized("You are not allowed write access to {}"
                                                             .format(str(resourceObject))))
        action = paramInfo.getParam("action")
        perm = paramInfo.getParam("perm")
        notify = paramInfo.getParam("notify")
        userPath = paramInfo.getParam("user")
        groupPath = paramInfo.getParam("group")
        if userPath != "":
            grantee = User.objects.get(id=userPath.rsplit("/", 2)[1])
        if groupPath != "":
            notify = False
            grantee = Group.objects.get(id=groupPath.rsplit("/", 2)[1])
        logger.debug("Setting permissions for resource %d by doing '%s' for perm '%s'",
                     resourceObject.id, action, perm)
        permList = []
        if action == "assign":
            if "r" in perm:
                assign_perm("read_resource_data", grantee, resourceObject)
                permList.append("read")
            if "w" in perm:
                assign_perm("write_resource_data", grantee, resourceObject)
                permList.append("write")
            if "x" in perm:
                assign_perm("execute_on_resource", grantee, resourceObject)                
                permList.append("execute")
            if notify:
                scsSendPermChangeEmail(grantee, ACCESS_GRANTED_EMAIL_SUBJECT, ACCESS_GRANTED_EMAIL_TEMPLATE, 
                                       "resource", resourceObject,
                                       permList)
                
        elif action == "remove":
            if "r" in perm:
                remove_perm("read_resource_data", grantee, resourceObject)
                permList.append("read")
            if "w" in perm:
                remove_perm("write_resource_data", grantee, resourceObject)
                permList.append("write")
            if "x" in perm:
                remove_perm("execute_on_resource", grantee, resourceObject)
                permList.append("execute")
            if notify:
                scsSendPermChangeEmail(grantee, ACCESS_REMOVED_EMAIL_SUBJECT, ACCESS_REMOVED_EMAIL_TEMPLATE, 
                                       "resource", resourceObject,
                                       permList)
        else:
            return HttpBadRequest("Invalid action: {}".format(action))
            
        return HttpOk(json.dumps({}).encode("utf-8"))
        
    def getMyUser(self, request):
        '''
        Get the user from the request 
        '''
        # This statement forces update of user field in request with values from request headers
        self.is_authenticated(request)        
        if request.user.is_authenticated():  
            return request.user
        else:
            # If auth is disabled just return the first user
            if not scsApiGetAuthOn():
                return User.objects.first()
            else:
                logger.warning("User not authenticated")
                raise SmException("No user logged in")
    
    def zip_detail(self, request, **kwargs):
        """
        Zip a file or folder into the name specified (or a generated tmp file)
        """
        paramInfo = SmParamInfo2(SM_RESOURCE_ZIP_PARAM_DICT, kwargs["pk"], request)
        pathAfterPk = paramInfo.getPostObjectIdPath() 
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        self._meta.authorization.check_read_data(self, resourceObject, request)
            
        try:
            if paramInfo.getParam("makeTempPath"):
                pathToSaveTo = ".rpsmarf/tmp/" + str(uuid.uuid4()) + ".zip"
            else:
                pathToSaveTo = paramInfo.getParam("zipFilePath")
            if (pathToSaveTo is None or pathToSaveTo == ""):
                raise ImmediateHttpResponse(HttpBadRequest("Parameter zipFilePath or makeTempPath must be specified"))
                
            logger.debug("Configuring zip task of '%s' to '%s'", pathAfterPk, pathToSaveTo)
            
            u = self.getMyUser(request)
            ttype = SmModelTaskType.objects.get(name="zip")
            task = SmModelTask()
            task.owner = u
            task.task_type = ttype
            task.parametersJson = json.dumps({
                                                   "input_ids": {
                                                                 "1": {"path": pathToSaveTo},
                                                                 "2": {"path": pathAfterPk}
                                                                 }
                                                   })
            task.uiVisible = False
            task.save()
            
            task_resource = SmModelTaskResource()
            task_resource.task = task
            task_resource.resource = resourceObject
            # We assume that there is only one resource type for the zip task - the "generic storage" resource type
            task_resource.resource_type_task_type = SmModelResourceTypeTaskType.objects\
                .filter(task_type=ttype)[0]
            task_resource.owner = u
            task_resource.save()
    
            logger.debug("Starting zip task...")
            resp = sm_controller.api_task.scsApiTaskStartTask(task, request, False)
            if resp.status_code != 200:
                raise ImmediateHttpResponse(resp)
            response = HttpOk(json.dumps({"zipFilePath": pathToSaveTo,
                                          "taskPath": "/scs/task/" + str(task.id) + "/"}))
            logger.debug("Zip task started. response code=%d", response.status_code)
            statsd.incr(SCS_METRIC_NUM_ZIP)
            return response 
        except ImmediateHttpResponse as e:
            logger.debug("Zip task started. response code=%d", e.response.status_code)
            raise e
        except Exception as e:
            logger.exception("Error during zip task starting")
            smRaiseFsOpException(e, "zip", pathAfterPk)
        
    def unzip_detail(self, request, **kwargs):
        """
        Unzip a file or folder into the name specified (or a generated tmp file)
        """
        paramInfo = SmParamInfo2(SM_RESOURCE_UNZIP_PARAM_DICT, kwargs["pk"], request)
        pathAfterPk = paramInfo.getPostObjectIdPath() 
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        self._meta.authorization.check_write_data(self, resourceObject, request)
        fileToUnzip = paramInfo.getParam("zipFilePath")
        logger.debug("Starting unzip of %s to %s", fileToUnzip, pathAfterPk)
        try:
            
            u = self.getMyUser(request)
            ttype = SmModelTaskType.objects.get(name="unzip")
            task = SmModelTask()
            task.owner = u
            task.task_type = ttype
            task.parametersJson = json.dumps({
                                                   "input_ids": {
                                                                 "1": {"path": pathAfterPk},
                                                                 "2": {"path": fileToUnzip}
                                                                 }
                                                   })
            task.uiVisible = False
            task.save()
    
            task_resource = SmModelTaskResource()
            task_resource.task = task
            task_resource.resource = resourceObject
            # We assume that there is only one resource type for the unzip task - the "generic storage" resource type
            task_resource.resource_type_task_type = SmModelResourceTypeTaskType.objects\
                .filter(task_type=ttype)[0]
            task_resource.owner = u
            task_resource.save()
            
            sm_controller.api_task.scsApiTaskStartTask(task, request, False)
            response = HttpOk(json.dumps({"taskPath": "/scs/task/" + str(task.id) + "/"}))
            statsd.incr(SCS_METRIC_NUM_UNZIP)
            return response 
        except ImmediateHttpResponse as e:
            raise e
        except Exception as e:
            smRaiseFsOpException(e, "unzip", pathAfterPk)
        
    def deleterecursive_detail(self, request, **kwargs):
        """
        Start a task to recusively delete a folder
        """
        paramInfo = SmParamInfo2(SM_EMPTY_PARAM_DICT, kwargs["pk"], request)
        pathAfterPk = paramInfo.getPostObjectIdPath() 
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        self._meta.authorization.check_write_data(self, resourceObject, request, False)
        
        logger.debug("Starting recursive delete of %s", pathAfterPk)
        
        u = self.getMyUser(request)
        ttype = SmModelTaskType.objects.get(name_key=SM_TASK_TYPE_DELETE_RECURSIVE_NAME_KEY)
        task = SmModelTask()
        task.owner = u
        task.task_type = ttype
        task.parametersJson = json.dumps({
                                          "input_ids": {
                                                        "1": {"path": (pathAfterPk + "/")}
                                                        }
                                          })
        task.uiVisible = False
        task.save()
        
        task_resource = SmModelTaskResource()
        task_resource.task = task
        task_resource.resource = resourceObject
        # We assume that there is only one resource type for the delete task - the "generic storage" resource type
        task_resource.resource_type_task_type = SmModelResourceTypeTaskType.objects\
            .filter(task_type=ttype)[0]
        task_resource.owner = u
        task_resource.save()

        sm_controller.api_task.scsApiTaskStartTask(task, request, False)
        statsd.incr(SCS_METRIC_NUM_DELETE_RECUR)
        response = HttpOk(json.dumps({"taskPath": "/scs/task/" + str(task.id) + "/"}))
        return response 
        
    def copy_detail(self, request, **kwargs):
        """
        Copy one or more files and/or folder into the folder specified
        """
        paramInfo = SmParamInfo2(SM_RESOURCE_COPY_PARAM_DICT, kwargs["pk"], request)
        pathAfterPk = paramInfo.getPostObjectIdPath() 
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        self._meta.authorization.check_write_data(self, resourceObject, request)
        srcResourceId = paramInfo.getParam("srcResource")
        filesToCopy = paramInfo.getParam("srcPath")
        logger.debug("Starting copy of %s to %d", json.dumps(filesToCopy), srcResourceId)
        try:
            
            u = self.getMyUser(request)
            ttype = SmModelTaskType.objects.get(name_key="copy")
            
            task = SmModelTask()
            task.owner = u
            task.task_type = ttype
            task.parametersJson = json.dumps({
                                                   "input_ids": {
                                                                 "1": {"path": filesToCopy},
                                                                 "2": {"path": pathAfterPk}
                                                                 }
                                                   })
            task.uiVisible = False
            task.save()
    
            task_resource = SmModelTaskResource()
            task_resource.task = task
            task_resource.resource = resourceObject
            # We assume that there is only one resource type for the copy task - the "generic storage" resource type
            task_resource.resource_type_task_type = SmModelResourceTypeTaskType.objects.filter(task_type=ttype).get(role="dest")
            task_resource.role = "dest"
            task_resource.owner = u
            task_resource.save()
            
            task_resource = SmModelTaskResource()
            task_resource.task = task
            task_resource.resource = SmModelResource.objects.get(id=srcResourceId)
            # We assume that there is only one resource type for the copy task - the "generic storage" resource type
            task_resource.resource_type_task_type = SmModelResourceTypeTaskType.objects.filter(task_type=ttype).get(role="src")
            task_resource.owner = u
            task_resource.role = "src"
            task_resource.save()
            
            sm_controller.api_task.scsApiTaskStartTask(task, request, False)
            response = HttpOk(json.dumps({"taskPath": "/scs/task/" + str(task.id) + "/"}))
            statsd.incr(SCS_METRIC_NUM_UNZIP)
            return response 
        except ImmediateHttpResponse as e:
            logger.exception("Exeception during copy setup")
            raise e
        except Exception as e:
            logger.exception("Exeception during copy setup")
            smRaiseFsOpException(e, "copy", pathAfterPk)
        
    def obj_delete(self, bundle, **kwargs):
        paramInfo = SmParamInfo2(SM_EMPTY_PARAM_DICT, kwargs["pk"], bundle.request)
        pathAfterPk = paramInfo.getPostObjectIdPath()
        # If no suffix then this is a resource object delete
        if pathAfterPk == "":
            return super().obj_delete(bundle, **kwargs)
        
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        self._meta.authorization.check_write_data(self, resourceObject, bundle.request, False)
        fileTagIndex = pathAfterPk.rfind("/file")
        if fileTagIndex != -1:
            path = pathAfterPk[:fileTagIndex]
        else:
            folderTagIndex = pathAfterPk.rfind("/folder")
            if folderTagIndex != -1:
                path = pathAfterPk[:folderTagIndex + 1] # Leave trailing / to indicate folder
            else:
                raise ImmediateHttpResponse(HttpBadRequest("Path '" + pathAfterPk + "' must end in /file or /folder"))
                
        # There is a path, so we need to delete the file or folder referenced
        fileAcc = None
        try:
            fileAcc = self.makeFileAcc(paramInfo)
            fileAcc.connectSession()
            fileAcc.deletePaths([path])
            statsd.incr(SCS_METRIC_NUM_DELETES)
        except Exception as e:
            smRaiseFsOpException(e, "delete", path)
        finally:
            if fileAcc:
                fileAcc.disconnectSession()

    def obj_get_list(self, bundle, **kwargs):
        '''
        Overriden list for resources
        Need to check for communityFilter argument and remove those records not tagged for that community 
        '''
        logger.debug("in obj_get_List kwargs is %s", kwargs)
        paramsInRequest = bundle.request.GET
        logger.debug("params: %s", json.dumps(paramsInRequest))
        communityToFilterFor = paramsInRequest.get("communityFilter")
        sortForUserId = paramsInRequest.get("sortForUser")
        
        records = super(SmApiModelResource, self).obj_get_list(bundle=bundle, kwargs=kwargs)
        if communityToFilterFor is not None and communityToFilterFor != SM_COMMUNITY_ALL_NAME_KEY:
            # Get list of resources in community
            resourceIds = set()
            try:
                tag = SmModelTag.objects.get(tag_name="community:" + communityToFilterFor)
                resourceTags = SmModelResourceTag.objects.filter(tag=tag).values()
                for resourceTag in resourceTags:
                    resourceIds.add(resourceTag["resource_id"])
            except Exception: # If community not found, carry on
                pass
                
            # Add in the personal folders
            if sortForUserId is not None:
                personalResources = SmModelResource.objects.filter(owner=sortForUserId).filter(personalFolder=True)
                for record in personalResources:
                    resourceIds.add(record.id)

            recordList = []
            for record in records:
                if record.id in resourceIds:
                    recordList.append(record)
            records = recordList 
                           
        if sortForUserId is not None:
            userList = User.objects.filter(id=sortForUserId)
            if len(userList) > 0:
                sortForUser = userList[0]
                # We sort personal folders first, then favorites, 
                # then read/writable then inaccessible (excluding other
                # peoples' personal folders
                sortInfo = []
                for record in records:
                    sortOrderChar = None
                    if record.personalFolder:
                        if record.owner == sortForUser:
                            sortOrderChar = 'c-' + record.name
                        elif record.owner != sortForUser and communityToFilterFor is not None:
                            sortOrderChar = 'd-' + record.name
                        else:
                            sortOrderChar = '!'

                    favoriteRecs = SmModelFavouriteResource.objects.\
                        filter(owner=sortForUser).\
                        filter(resource=record)
                    if len(favoriteRecs) > 0:
                        sortOrderChar = 'g-' + record.name
                    else:
                        if sortOrderChar is None:
                            sortOrderChar = 'm-' + record.name
                    
                    if sortOrderChar != "!":
                        sortInfo.append({"record": record, "sortKey": sortOrderChar + "_" + record.name})

                logger.debug("Sorting by id, ascending...")
                sortInfo.sort(key=operator.itemgetter('sortKey'))
                records = []
                for rec in sortInfo:
                    records.append(rec["record"]) 

        return records
        
    def copy_from_file_set_detail(self, request, **kwargs):
        """
        Copy files from the the file_set specified into the folder specified
        """
        paramInfo = SmParamInfo2(SM_RESOURCE_COPY_FROM_FILE_SET_PARAM_DICT, kwargs["pk"], request)
        pathAfterPk = paramInfo.getPostObjectIdPath() 
        if pathAfterPk == "":
            destPathBase = "file_set_" + smMakeDateString(sm_date_now())
        else:
            destPathBase = pathAfterPk + "/" + "file_set_" + smMakeDateString(sm_date_now())
        logger.debug("pathAfterPk is %s, destPathBase is %s", pathAfterPk, destPathBase)
        resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
        self._meta.authorization.check_write_data(self, resourceObject, request)
        file_set_id = paramInfo.getParam("file_set")
        file_set = SmModelFileSet.objects.get(id=file_set_id)
        src_resources = SmModelFileSetItem.objects.filter(file_set=file_set).values("resource").distinct()
        u = self.getMyUser(request)
        ttype = SmModelTaskType.objects.get(name_key="copy")
        rt3_dest = SmModelResourceTypeTaskType.objects.filter(task_type=ttype).filter(role="dest").first()
        rt3_src = SmModelResourceTypeTaskType.objects.filter(task_type=ttype).filter(role="src").first()
                
        task_list = []
        try:
            fileAcc = self.makeFileAcc(paramInfo, resourceObject)
            fileAcc.connectSession()
            fileAcc.mkdir(destPathBase)
            
            for src_resource_dict in src_resources:
                resource_id = src_resource_dict["resource"]
                src_resource = SmModelResource.objects.get(id=resource_id)
                # Check read permission on resource
                if scsApiGetAuthOn():
                    self._meta.authorization.check_read_data(self, src_resource, request)
                
                logger.debug("Setting up copy for files on resource %d", resource_id)
                destPath = destPathBase + "/resource_" + src_resource.name_key + "/"
                # Create folder for contents:
                fileAcc.mkdir(destPath)
          
                src_path_list = []
                items = SmModelFileSetItem.objects.filter(file_set=file_set).filter(resource=src_resource)
                for item in items:
                    src_path_list.append(item.relative_path)
                paramDict = {
                         "input_ids": {
                                       "1": {"path": src_path_list},
                                       "2": {"path": destPath}
                                       }
                         }
                task = SmModelTask(owner=u, task_type=ttype, parametersJson=json.dumps(paramDict))
                task.save()
    
                task_resource = SmModelTaskResource(task=task, resource=resourceObject,
                                                    resource_type_task_type=rt3_dest,
                                                    owner=u)
                task_resource.save()
                task_resource = SmModelTaskResource(task=task, resource=src_resource,
                                                    resource_type_task_type=rt3_src,
                                                    owner=u)
                task_resource.save()
            
                sm_controller.api_task.scsApiTaskStartTask(task, request, False)
                task_list.append("/scs/task/" + str(task.id) + "/")
            response = HttpOk(dumpsPretty({"tasks": task_list, "dest_path": destPathBase}))
            return response 
        except ImmediateHttpResponse as e:
            logger.exception("Exeception during copy setup")
            raise e
        except Exception as e:
            logger.exception("Exeception during copy setup")
            smRaiseFsOpException(e, "copy", pathAfterPk)
        finally:
            fileAcc.disconnectSession()
        

class SmApiFavouriteResource(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    resource = fields.ForeignKey(SmApiResource, 'resource')
    resource.full = True

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleOnlyToSelf())
        queryset = SmModelFavouriteResource.objects.all()
        resource_name = 'favourite_resource'
        filtering = smMakeFilterAllFieldsFilter(SmModelFavouriteResource)
        ordering = smMakeOrderingFieldList(SmModelFavouriteResource)
        serializer = SmarfSerializer()

                
class SmApiTag(SmApiModelResource):
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlySuperuser())   
        queryset = SmModelTag.objects.all()
        resource_name = 'tag'
        filtering = smMakeFilterAllFieldsFilter(SmModelTag)
        ordering = smMakeOrderingFieldList(SmModelTag)
        serializer = SmarfSerializer()
     
        
class SmApiResourceTag(SmApiModelResource):
    resource = fields.ForeignKey(SmApiResource, 'resource')
    tag = fields.ForeignKey(SmApiTag, 'tag')
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthTagLink("resource"))
        queryset = SmModelResourceTag.objects.all()
        resource_name = 'resource_tag'
        filtering = smMakeFilterAllFieldsFilter(SmModelResourceTag)
        ordering = smMakeOrderingFieldList(SmModelResourceTag)
        serializer = SmarfSerializer()
    
    
SM_TASK_TYPE_GET_PARAM_DICT = {
    "desc": "This URL allows access to perform operations on a task type",
    "params": [{
                "name": "extForUser",
                "type": "int",
                "desc": "This parameter, if set to a user id, adds several fields " +
                " which start wtih ext_ to simplify the operation of the UI",
                "default": -1,
               },
               {
                "name": "showComputeResources",
                "type": "bool",
                "desc": "This parameter, if set to True adds a field " +
                "called compute_resources which is a list of resources which " +
                "can run this task_type",
                "default": False,
               },
               ]
}        
    
SM_OBJECT_GETUSAGE_DICT = {
    "desc": "This URL allows access to get usage for an object (resource or task type)",
    "params": [{
                "name": "user",
                "type": "int",
                "desc": "This parameter, if set to a user id (e.g. 1), returns usage data " +
                "for this user",
                "default": -1,
               },
               {
                "name": "total",
                "type": "bool",
                "desc": "This parameter, if True will return usage totals",
                "default": True,
               },
               {
                "name": "periods",
                "type": "string",
                "desc": "This parametercontrols which period (w=last week, m=last month, y=last year) " +
                "usage data is returned for",
                "default": "wmy",
               },
               ]
}        
    
SM_OBJECT_GETUSERS_DICT = {
    "desc": "This URL allows access to get the list of users for an object (resource or task type)",
    "params": [{
                "name": "duration",
                "type": "int",
                "desc": "This parameter controls how far back to report data", 
                "default": 30,
               },
               {
                "name": "end_time",
                "type": "string",
                "desc": "Sets the end of the reporting period.  Defaults to 'now'.",
                "default": "",
               },
               ]
}        
    
    
def scsRequestAccess(requestingUser, paramInfo, obj):
    # Add action to log (in case the email is not sent
    action.send(obj, verb="requestaccess")
    if isinstance(obj, SmModelResource):
        targetObject = "/scs/resource/" + str(obj.id) + "/"
        objType = "resource"
    elif isinstance(obj, SmModelTaskType):
        targetObject = "/scs/task_type/" + str(obj.id) + "/"
        objType = "tool"
    else:
        objType = str(type(obj))
        
    reason = paramInfo.getParam("reason")
    access = paramInfo.getParam("access")
    requestObj = SmModelRequest(owner=obj.owner, 
                                requester=requestingUser, 
                                target=targetObject, 
                                reason=reason,
                                access_requested=access)
    requestObj.save()
    
    accessExpanded = ""
    if 'r' in access:
        accessExpanded += "read " 
    if 'w' in access:
        accessExpanded += "write " 
    if 'x' in access:
        accessExpanded += "execute " 
    if accessExpanded == "":
        raise ImmediateHttpResponse(HttpBadRequest("'access' parameter "
                                                   "must be specified and contain "
                                                   "at least one of 'r', 'w' and 'x'"))
    params = {"%requesterEmail%": requestingUser.email,
              "%requesterFirstName%": requestingUser.first_name,
              "%requesterLastName%": requestingUser.last_name,
              "%objectType%": objType,
              "%objectName%": obj.name,
              "%reason%": reason,
              "%access%": accessExpanded,
              }
    mailer = scsGetGlobalMailer()
    if mailer is not None:
        emailBody = mailer.merge(REQUEST_ACCESS_EMAIL_TEMPLATE, params)
        destEmail = obj.owner.first_name + " " + obj.owner.last_name + " <" + obj.owner.email + ">"
        mailer.sendEmail(destEmail, REQUEST_ACCESS_EMAIL_SUBJECT, emailBody)
    

def scsSendPermChangeEmail(user, subject, rpsmarf_template_dir, objType, obj, permList):
    # Ensure that permList has at least 3 items
    for _ in range(3 - len(permList)):
        permList.append("")
    mailer = scsGetGlobalMailer()
    if mailer is None:
        logger.info("Not sending email due to mailer not configured")
    else:
        params = {"%ownerEmail%": obj.owner.email,
                  "%ownerFirstName%": obj.owner.first_name,
                  "%ownerLastName%": obj.owner.last_name,
                  "%objectName%": obj.name,
                  "%objectType%": objType,
                  "%perm0%": permList[0],
                  "%perm1%": permList[1],
                  "%perm2%": permList[2]}
        emailBody = mailer.merge(rpsmarf_template_dir, params)
        destEmail = user.first_name + " " + user.last_name + " <" + user.email + ">"
        logger.debug("Sending email to %s with subject %s", destEmail, subject)
        mailer.sendEmail(destEmail, subject, emailBody)
    

def scsSendCommand(command, workingDir, computeResource):
    # Create task descriptor
    taskDesc = SmTaskDesc("", "")
    streamResourceArgList = []
    for token in command.split(" "):
        streamResourceArgList.append({"type": "string", 
                                      "value": token})
        
    taskDesc.addTaskParam("args", streamResourceArgList)
    execInfo = scsMakeExecInfo(computeResource)
    if "workingDir" is not None:
        execInfo["workingDir"] = {"type": "string", "value": workingDir}
    taskDesc.addTaskParam("execInfo", execInfo)                        
  
    # Connect to agent
    agentUrl = urlparse(computeResource.container.agent.agentUrl)
    logger.debug("Connecting to agent via %s (host %s, port %s)", str(agentUrl), agentUrl.hostname, agentUrl.port)
    try:
        raOps = smRemoteAgentAsyncOperationFactory.makeSyncOperationObject(agentUrl)
        
        # Start task on agent
        logger.debug("Sending start message to agent..")
        respJson = raOps.doOperation("shellcommand", taskDesc.toJson())
    except:
        logger.exception("Error in command " + command)
        raise ImmediateHttpResponse(HttpError422("Task execution failed on agent {}"
                                                 .format(computeResource.container.agent.agentUrl)))

    respDict = json.loads(respJson) 
    respDict["command"] = command
    return respDict


# SmApiTask related resources       
class SmApiTaskType(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = ResourceAuthorization(alter_permission_code="sm_controller.add_smmodeltasktype",
                                              run_permission_code="execute_task_type")
        queryset = SmModelTaskType.objects.all()
        resource_name = 'task_type'
        filtering = smMakeFilterAllFieldsFilter(SmModelTaskType)
        ordering = smMakeOrderingFieldList(SmModelTaskType)
        serializer = SmarfSerializer()
        
    def prepend_urls(self):
        '''
        Taken from http://stackoverflow.com/questions/9449749/filefield-in-tastypie
        See also https://django-tastypie.readthedocs.org/en/v0.9.11/cookbook.html
        '''
        return [
                # See views.py for the upload code
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/setperm%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('setperm_detail'),
                    name="api_setperm_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/getperm%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('getperm_detail'),
                    name="api_getperm_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/getusage%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('getusage_detail'),
                    name="api_getusage_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/getusers%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('getusers_detail'),
                    name="api_getusers_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/requestaccess%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('requestaccess_detail'),
                    name="api_requestaccess_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/getversions%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('getversions_detail'),
                    name="api_getversions_detail"),
                ] 

    def getversions_detail(self, request, **kwargs):
        """
        Request list of version control versions for a task type
        """
        logger.debug("Getting versions access for a task type")
        paramInfo = SmParamInfo2(SM_EMPTY_PARAM_DICT, kwargs["pk"], request)
        if scsApiGetAuthOn():
            self.is_authenticated(request)
           
        # Check if the task_type support versions
        task_type = SmModelTaskType.objects.get(id=paramInfo.getObjectId())
        configDict = json.loads(task_type.configurationJson)
        if configDict.get("versioning") != "git":
            return HttpError422("Versioning not supported on this task type")
                   
        if "workingDir" not in configDict:
            return HttpError422("No working directory specified on this task type")
        workingDir = configDict["workingDir"]["value"]
        
        # Find a resource
        compute_resource_role = configDict["computeResourceRole"]
        rt3_list = SmModelResourceTypeTaskType.objects.filter(task_type=task_type)\
            .filter(role=compute_resource_role)
        if rt3_list.count() == 0:
            return HttpError422("Did not find resource_type_task_type object for role " 
                         + compute_resource_role)
        
        rt3 = rt3_list.first()
        resource_list = SmModelResource.objects.filter(resource_type=rt3.resource_type)\
            .filter(status="up")
        if resource_list.count() == 0:
            return HttpError422("Did not find any working resources of type '{}'".format( 
                         rt3.resource_type.name))                         
        
        # Issue a command to that that resource
        resource = resource_list.first()
        respJson = scsSendCommand(smCommonGetEnvVar(SM_ENV_VAR_COMMON_GIT_FETCH_COMMAND),
                                  workingDir,
                                  resource)
        if respJson["resultcode"] != 0:
            return HttpError522(respJson)

        respJson = scsSendCommand(smCommonGetEnvVar(SM_ENV_VAR_COMMON_GIT_TAG_LIST_COMMAND),
                                  workingDir,
                                  resource)
        if respJson["resultcode"] != 0:
            return HttpError522(respJson)
        
        # Format results
        result_dict = {"resultcode": 0}
        tags = respJson["stdout"].split("\n")
        result_dict["tags"] = []
        for tag in tags:
            if tag == "":
                continue
            command = smCommonGetEnvVar(SM_ENV_VAR_COMMON_GIT_TAG_GET_COMMAND).replace("$TAG$", tag)
            respJson = scsSendCommand(command,
                                      workingDir,
                                      resource)
            if respJson["resultcode"] != 0:
                return HttpError522(respJson)
            sections = respJson["stdout"].split("\n\n")
            tokens = sections[0].split("\n")
            tagger = tokens[3]
            authorEnd = tagger.index(">")
            author = tagger[7:authorEnd + 1]
            timestamp = tagger[authorEnd + 1:tagger.index(" ", authorEnd + 2)]
            tagDict = {"tag": tag,
                       "author": author,
                       "timestamp": int(timestamp),
                       "message": sections[1]}
            result_dict["tags"].append(tagDict)              
        return HttpOk(dumpsPretty(result_dict))
                                 
    def requestaccess_detail(self, request, **kwargs):
        """
        Request access to a task type
        """
        logger.debug("Requesting access for a task type")
        paramInfo = SmParamInfo2(SM_RESOURCE_REQUEST_ACCESS_PARAM_DICT, kwargs["pk"], request)
        if scsApiGetAuthOn():
            self.is_authenticated(request)
            user = request.user    
        else:
            user = User.objects.get(id=paramInfo.getParam("userid"))
           
        scsRequestAccess(user,
                         paramInfo,
                         SmModelTaskType.objects.get(id=paramInfo.getObjectId()))
        return HttpOk("{}")
                                 
    def getusage_detail(self, request, **kwargs):
        """
        Get a summary of resource usage for a task type
        """
        logger.debug("Getting usage for task type")
        paramInfo = SmParamInfo2(SM_OBJECT_GETUSAGE_DICT, kwargs["pk"], request)
        resp = scsGetUsageForObject(paramInfo,
                                    SmModelTaskType.objects.get(id=paramInfo.getObjectId()))
        response = HttpOk(json_utils.dumpsPretty(resp).encode("utf-8"))
        return response 
        
    def getusers_detail(self, request, **kwargs):
        """
        Get a list of users for a task type
        """
        logger.debug("Getting usage for task type")
        paramInfo = SmParamInfo2(SM_OBJECT_GETUSERS_DICT, kwargs["pk"], request)
        resp = scsGetUsersForObject(paramInfo,
                                    SmModelTaskType.objects.get(id=paramInfo.getObjectId()))
        response = HttpOk(json_utils.dumpsPretty(resp).encode("utf-8"))
        return response 
        
    def getperm_detail(self, request, **kwargs):
        """
        Get the permissions for a task type
        """
        logger.debug("Getting permissions")
        if scsApiGetAuthOn():
            self.is_authenticated(request)        

        paramInfo = SmParamInfo2(SM_RESOURCE_GETPERM_DICT, kwargs["pk"], request)
        taskTypeObject = SmModelTaskType.objects.get(id=paramInfo.getObjectId())
        users = get_users_with_perms(taskTypeObject)
        resp = {}
        for user in users:
            perms = get_perms(user, taskTypeObject)
            for perm in perms:
                userPath = "/scs/user/" + str(user.id) + "/"
                if userPath in resp:
                    resp[userPath].append(perm)
                else:
                    resp[userPath] = [perm]
        response = HttpOk(json_utils.dumpsPretty(resp).encode("utf-8"))
        return response 
        
    def setperm_detail(self, request, **kwargs):
        """
        Set permissions for a task type
        """
        paramInfo = SmParamInfo2(SM_RESOURCE_SETPERM_DICT, kwargs["pk"], request)
        taskTypeObject = SmModelTaskType.objects.get(id=paramInfo.getObjectId())
        
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
            if taskTypeObject.owner != request.user and not request.user.is_superuser:
                raise ImmediateHttpResponse(SmHttpUnauthorized("You are not allowed to alter {}"
                                                             .format(str(taskTypeObject))))
        action = paramInfo.getParam("action")
        perm = paramInfo.getParam("perm")
        userPath = paramInfo.getParam("user")
        groupPath = paramInfo.getParam("group")
        notify = paramInfo.getParam("notify")
        if userPath != "":
            grantee = User.objects.get(id=userPath.rsplit("/", 2)[1])
        if groupPath != "":
            notify = False
            grantee = Group.objects.get(id=groupPath.rsplit("/", 2)[1])
            
        logger.debug("Setting permissions for task type %d by doing '%s' for perm '%s'",
                     taskTypeObject.id, action, perm)

        if action == "assign":
            if "x" in perm:
                assign_perm("execute_task_type", grantee, taskTypeObject)
                if notify:                
                    scsSendPermChangeEmail(grantee, ACCESS_GRANTED_EMAIL_SUBJECT, ACCESS_GRANTED_EMAIL_TEMPLATE, 
                                           "software tool", taskTypeObject,
                                           ["execute", "", ""])
        elif action == "remove":
            if "x" in perm:
                remove_perm("execute_task_type", grantee, taskTypeObject)
                if notify:
                    scsSendPermChangeEmail(grantee, ACCESS_REMOVED_EMAIL_SUBJECT, ACCESS_REMOVED_EMAIL_TEMPLATE, 
                                           "software tool", taskTypeObject,
                                           ["execute", "", ""])
        else:
            return HttpBadRequest("Invalid action: {}".format(action))
            
        return HttpOk(json.dumps({}).encode("utf-8"))
        
    def obj_get_list(self, bundle, **kwargs):
        '''
        Need to check for communityFilter argument and remove those records not tagged for that community 
        '''
        logger.debug("in obj_get_List kwargs is %s", kwargs)
        paramsInRequest = bundle.request.GET
        logger.debug("In obj_get_List SmApiTaskType params: %s", json.dumps(paramsInRequest))
        communityToFilterFor = paramsInRequest.get("communityFilter")
        sortForUserId = paramsInRequest.get("sortForUser")
        
        records = super(SmApiModelResource, self).obj_get_list(bundle=bundle, kwargs=kwargs)
        if communityToFilterFor is not None and communityToFilterFor != SM_COMMUNITY_ALL_NAME_KEY:
            # Get list of resources in community
            resourceIds = set()
            try:
                tag = SmModelTag.objects.get(tag_name="community:" + communityToFilterFor)
                resourceTags = SmModelResourceTag.objects.filter(tag=tag).values()
                for resourceTag in resourceTags:
                    resourceIds.add(resourceTag["resource_id"])
            except Exception: # If community not found, carry on
                pass
                
            # Get list of resource_type_task_type record which match the
            # resource ids found
            resourceTypeIds = set()
            rs = SmModelResource.objects.all().values()
            for r in rs:
                if r["id"] in resourceIds:
                    resourceTypeIds.add(r["resource_type_id"])
            
            # Get list of resource_type_task_type record which match the
            # resource ids found
            taskTypeIds = set()
            rttts = SmModelResourceTypeTaskType.objects.all().values()
            for rttt in rttts:
                if rttt["resource_type_id"] in resourceTypeIds:
                    taskTypeIds.add(rttt["task_type_id"])
            
            recordList = []
            for record in records:
                if record.id in taskTypeIds:
                    recordList.append(record)
            records = recordList
            
        if sortForUserId is not None:
            userList = User.objects.filter(id=sortForUserId)
            if len(userList) > 0:
                sortForUser = userList[0]
                # We sort favorited task types first, then others
                sortInfo = []
                for record in records:
                    sortOrderChar = None
                    favoriteRecs = SmModelFavouriteTaskType.objects.\
                        filter(owner=sortForUser).\
                        filter(task_type=record)
                    if len(favoriteRecs) > 0:
                        sortOrderChar = 'g-' + record.name
                    else:
                        sortOrderChar = 'm-' + record.name
                    
                    sortInfo.append({"record": record, "sortKey": sortOrderChar + "_" + record.name})

                logger.debug("Sorting by id, ascending...")
                sortInfo.sort(key=operator.itemgetter('sortKey'))
                records = []
                for rec in sortInfo:
                    records.append(rec["record"]) 

        return records

    def dehydrate(self, bundle):
        paramInfo = SmParamInfo2(SM_TASK_TYPE_GET_PARAM_DICT, str(bundle.obj.id), bundle.request)
        extForUser = paramInfo.getParam("extForUser")
        if extForUser != -1:
            task_type = bundle.obj
            user = User.objects.get(id=extForUser)
            note = SmModelTaskTypeNote.objects.filter(task_type=task_type)\
                .filter(owner=extForUser).first()
            if note is not None:
                bundle.data['ext_note'] = note.note
                bundle.data['ext_note_id'] = note.id
            favourite = SmModelFavouriteTaskType.objects.filter(task_type=task_type)\
                .filter(owner=extForUser).first()
            if favourite is not None:
                bundle.data['ext_favourite'] = True
            tags = SmModelTaskTypeTag.objects.filter(task_type=task_type)
            if tags.count() > 0:
                tagList = []
                for tagLinkRec in tags:
                    tagList.append(tagLinkRec.tag.tag_name) 
                bundle.data['ext_tags'] = tagList
            # Extended permissions           
            permlist = ""
            perms = get_perms(user, task_type)
            for perm in perms:
                if perm == "execute_task_type":
                    permlist += "x"
            bundle.data['ext_perms'] = permlist
                
        show_compute_resource = paramInfo.getParam("showComputeResources")
        if show_compute_resource:
            task_type = bundle.obj
            comp_res_list = []
            tt_config_dict = json.loads(task_type.configurationJson)
            if "computeResourceRole" in tt_config_dict:
                computeRole = tt_config_dict["computeResourceRole"]
                rt3_types = SmModelResourceTypeTaskType.objects.filter(task_type=task_type).filter(role=computeRole)
                for rt3_type in rt3_types:
                    res_list = SmModelResource.objects.filter(resource_type=rt3_type.resource_type)
                    for res in res_list:
                        comp_res_list.append({"resource_uri": "/scs/resource/" + str(res.id) + "/",
                                              "id": res.id,
                                              "locked": res.locked,
                                              "status": res.status,
                                              "name": res.name})
            bundle.data['compute_resources'] = comp_res_list

        return bundle
        
        
class SmApiFavouriteTaskType(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    task_type = fields.ForeignKey(SmApiTaskType, 'task_type')
    task_type.full = True
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleOnlyToSelf())
        queryset = SmModelFavouriteTaskType.objects.all() 
        resource_name = 'favourite_task_type'
        filtering = smMakeFilterAllFieldsFilter(SmModelFavouriteTaskType)
        ordering = smMakeOrderingFieldList(SmModelFavouriteTaskType)
        serializer = SmarfSerializer()
        

class SmApiTaskTypeTag(SmApiModelResource):
    task_type = fields.ForeignKey(SmApiTaskType, 'task_type')
    tag = fields.ForeignKey(SmApiTag, 'tag')
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthTagLink("task_type"))
        queryset = SmModelTaskTypeTag.objects.all()
        resource_name = 'task_type_tag'
        filtering = smMakeFilterAllFieldsFilter(SmModelTaskTypeTag)
        ordering = smMakeOrderingFieldList(SmModelTaskTypeTag)
        serializer = SmarfSerializer()
 
 
class SmApiResourceTypeTaskType(SmApiModelResource):    
    task_type = fields.ForeignKey(SmApiTaskType, 'task_type')
    resource_type = fields.ForeignKey(SmApiResourceType, 'resource_type')
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlySuperuser())
        queryset = SmModelResourceTypeTaskType.objects.all()
        resource_name = 'resource_type_task_type'
        filtering = smMakeFilterAllFieldsFilter(SmModelResourceTypeTaskType)
        ordering = smMakeOrderingFieldList(SmModelResourceTypeTaskType)
        serializer = SmarfSerializer()
    
    
SM_TASK_PARAM_DICT = {
    "desc": "This URL allows access to perform operations on a task",
    "params": [{
                "name": "action",
                "type": "string",
                "desc": "This parameter specifies the action to be performed on the " +  
                        "task.  The supported operations are 'start' and 'cancel'. " +
                        "The 'start' action starts the task running. 'cancel' " +
                        "stops the task from running as soon as possible. " +
                        "The 'status' field indicates if the task has completed, etc.."
                        "Note that state errors (starting a started task or " +
                        "canceling a cancelled task) return 409-Conflict.",
                "default": "",
               },
               {
                "name": "timeoutForTask",
                "type": "float",
                "desc": "This parameter specifies the maximum number of  " + 
                "seconds to wait for the task to complete before terminating it",
                "default": 1000.0,
               },
               {
                "name": "aggregateParam",
                "type": "bool",
                "desc": "This parameter, if true, makes the parametersJson field contain " +
                "a denormalized view of the parameters to make display of this information " +
                "in the UI faster",
                "default": False,
               },
               {
                "name": "orderByIdDesc",
                "type": "bool",
                "desc": "This parameter, if true, makes a task list operation order items " +
                "by task id in descending order.  It is useful when aggregateParam is set " +
                "because in this case the normal TastyPie ordering does not work.",
                "default": False,
               },
               {
                "name": "orderByStartTimeDesc",
                "type": "bool",
                "desc": "This parameter, if true, makes a task list operation order items " +
                "by start time in descending order.  It is useful when aggregateParam is set " +
                "because in this case the normal TastyPie ordering does not work.",
                "default": False,
               },
               {
                "name": "aggregateParam",
                "type": "bool",
                "desc": "This parameter, if true, makes the parametersJson field contain " +
                "a denormalized view of the parameters to make display of this information " +
                "in the UI faster",
                "default": False,
               },
               {
                "name": "maxStdout",
                "type": "int",
                "desc": "This parameter control the amount of information returned in the 'stdout' " +
                "field.  It defaults to 1000 bytes. Set this to -1 to get all available information.",
                "default": 1000,
               },
               {
                "name": "maxStderr",
                "type": "int",
                "desc": "This parameter control the amount of information returned in the 'stderr' " +
                "field.  It defaults to 1000 bytes. Set this to -1 to get all available information.",
                "default": 1000,
               },
               {
                "name": "allowQueueing",
                "type": "bool",
                "desc": "This parameter, if true, allows a task which cannot be run now " +
                "(for example because another task is running on the configured resource) " +
                "to be executed later when the resource becomes available. Note that setting a " +
                "task start_time in the future automatically enables queueing.",
                "default": False,
                }
               ]
}        
    
    
class SmApiTask(SmOwnedModelResource):
    task_type = fields.ForeignKey(SmApiTaskType, 'task_type')
    owner = fields.ForeignKey(SmApiUser, 'owner')
    computeResoure = fields.ForeignKey(SmApiResource, 'computeResource', blank=True, null=True)
    cloud_server = fields.ForeignKey(SmApiCloudServer, 'cloud_server', blank=True, null=True)
    end_time = fields.DateTimeField("end_time", readonly=True)
    state = fields.CharField('state', readonly=True)
    completion = fields.CharField('completion', readonly=True)
    completionReason = fields.CharField('completionReason', readonly=True)
    completionDescription = fields.CharField('completionDescription', readonly=True)
    stdout = fields.CharField('stdout', readonly=True)
    stderr = fields.CharField('stderr', readonly=True)

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleOnlyToSelf())
        queryset = SmModelTask.objects.all()
        resource_name = 'task'
        filtering = smMakeFilterAllFieldsFilter(SmModelTask)
        ordering = smMakeOrderingFieldList(SmModelTask)
        serializer = SmarfSerializer()
        
    def limitStdoutStrerrInfo(self, t, maxStdout, maxStderr):
        if maxStderr >= 0:
            if maxStderr == 0:
                t.stderr = ""
            if len(t.stderr) >= maxStderr:
                t.stderr = t.stderr[-maxStderr:]
        if maxStdout >= 0:
            if maxStdout == 0:
                t.stdout = ""
            elif len(t.stdout) >= maxStdout:
                t.stdout = t.stdout[-maxStdout:]
                
    def aggregateParam(self, taskObject):
        '''
        Expand parametersJson field as described in the models.py
        for SmModelTask
        '''
        parametersJson = taskObject.parametersJson
        if parametersJson is None or parametersJson == "":
            return
        paramDict = json.loads(parametersJson)
        if len(paramDict) == 0:
            return
        logger.debug("Aggregating parameters for task %d", taskObject.pk)
        taskInputIdDict = paramDict["input_ids"]
        pList = []
        rList = []
        rolesOutputAlready = []
        ttConfigDict = json.loads(taskObject.task_type.configurationJson)
        for ttCfgEntry in ttConfigDict["args"]:
            inputId = ttCfgEntry.get("input_id") 
            if inputId:
                inputIdDict = taskInputIdDict[str(inputId)]
                entry = {}
                entry["name"] = ttCfgEntry.get("name")
                entry["description"] = ttCfgEntry.get("description")
                entry["input_id"] = ttCfgEntry["input_id"]
                
                if ttCfgEntry["type"] == "resource":
                    # Add a resource to resource list
                    role = ttCfgEntry["role"]
                    rolesOutputAlready.append(role)
                    try:
                        taskResource = SmModelTaskResource.objects. \
                            filter(task=taskObject).filter(resource_type_task_type__role=role)[0]
                        entry["nature"] = taskResource.resource_type_task_type.resource_type.nature
                        entry["name"] = taskResource.resource_type_task_type.name
                        entry["description"] = taskResource.resource_type_task_type.description
                        entry["resourcePath"] = "/scs/resource/" + str(taskResource.resource.id) + "/" 
                        entry["resource_typePath"] = "/scs/resource_type/" + str(taskResource.resource.resource_type.id) + "/" 
                        entry["resource_type_task_typePath"] = "/scs/resource_type_task_type/" + \
                            str(taskResource.resource_type_task_type.id) + "/" 
                        entry["resourcePath"] = "/scs/resource/" + str(taskResource.resource.id) + "/" 
                        entry["resourceName"] = taskResource.resource.name 
                    except:
                        logger.warning("No task resource object found for role %s in task %d", role, taskObject.pk)
                    entry["direction"] = ttCfgEntry["direction"]
                    if "pathType" in ttCfgEntry:
                        entry["pathType"] = ttCfgEntry["pathType"] 
                    if inputIdDict.get("path"):
                        entry["path"] = inputIdDict["path"]
                    rList.append(entry)
                else: # not a resource, add to pDict
                    entry["type"] = ttCfgEntry["type"]
                    entry["value"] = inputIdDict["value"]
                    pList.append(entry)

        # Process compute resource - add it to the resource list, but don't add it if the
        # role was already output
        computeResourceRole = ttConfigDict["computeResourceRole"]
        taskResources = SmModelTaskResource.objects.filter(task=taskObject) \
            .filter(resource_type_task_type__role=computeResourceRole)
        if len(taskResources) > 0 and (computeResourceRole not in rolesOutputAlready):
            taskResource = taskResources[0]
            entry = {}
            entry["nature"] = "compute"
            entry["resourcePath"] = "/scs/resource/" + str(taskResource.resource.id) + "/"
            entry["resource_typePath"] = "/scs/resource_type/" + str(taskResource.resource.resource_type.id) + "/" 
            entry["resource_type_task_typePath"] = "/scs/resource_type_task_type/" + \
                str(taskResource.resource_type_task_type.id) + "/" 
            entry["resourceName"] = taskResource.resource.name 
            rList.append(entry)
        
        paramDict["task_type_name"] = taskObject.task_type.name
        if "interactive" in ttConfigDict:
            paramDict["interactive"] = ttConfigDict["interactive"]
        else:
            paramDict["interactive"] = False
        paramDict["parameters"] = pList
        paramDict["resources"] = rList
        paramDict["taskTypeImageUrl"] = taskObject.task_type.imageUrl
        taskObject.parametersJson = json.dumps(paramDict)
        
    def obj_get(self, bundle, **kwargs):
        paramInfo = SmParamInfo2(SM_TASK_PARAM_DICT, kwargs["pk"], bundle.request)
        pathAfterPk = paramInfo.getPostObjectIdPath()
        t = SmModelTask.objects.get(id=paramInfo.getObjectId())
        if t.owner != bundle.request.user and scsApiGetAuthOn():
            raise ImmediateHttpResponse(SmHttpUnauthorized("You are not allowed to access this task"))
        if pathAfterPk == "" and paramInfo.getParam("action") == "":
            if paramInfo.getParam("aggregateParam"):
                # Append extra info in parametersJson
                self.aggregateParam(t)
                
            self.limitStdoutStrerrInfo(t, paramInfo.getParam("maxStdout"), paramInfo.getParam("maxStderr"))
            return t
        
        action = paramInfo.getParam("action")
        logger.debug("Found action %s in request", action)
        
        try:
            if action == "start":
                resp = sm_controller.api_task.scsApiTaskStartTask(t, 
                                                                  bundle.request,
                                                                  paramInfo.getParam("allowQueueing"))
                raise ImmediateHttpResponse(resp)
            elif action == "cancel":
                sm_controller.api_task.scsApiTaskCancelTask(t)
                raise ImmediateHttpResponse(HttpOk("Task cancellation requested"))
        except ImmediateHttpResponse as e:
            raise e
        except Exception as e:
            logger.exception("Exception during cancel")
            raise ImmediateHttpResponse(HttpBadRequest(str(e)))
    
    def obj_get_list(self, bundle, **kwargs):
        '''
        Need to check for check if parametersJson should be be expanded 
        '''
        logger.debug("in obj_get_list for SmModelTask kwargs is %s", kwargs)
        paramsInRequest = bundle.request.GET
        logger.debug("params: %s, aggregateParam=%s", 
                     json.dumps(paramsInRequest), paramsInRequest.get("aggregateParam"))
        aggregateParam = paramsInRequest.get("aggregateParam") == "True"
        orderByIdDesc = paramsInRequest.get("orderByIdDesc") == "True"
        orderByStartTimeDesc = paramsInRequest.get("orderByStartTimeDesc") == "True"
        
        logger.debug("agg=%s, orderByIdDesc=%s, orderByStartTimeDesc=%s", 
                     aggregateParam, orderByIdDesc, orderByStartTimeDesc)
        records = super(SmApiModelResource, self).obj_get_list(bundle=bundle, kwargs=kwargs)
        recordList = []
        for record in records:
            # Append extra info in parametersJson
            if aggregateParam:
                self.aggregateParam(record)
            recordList.append(record)
        records = recordList
        
        if orderByIdDesc:
            logger.debug("Sorting by id, descending...")
            records.sort(key=scsGetTaskIdFromTask, reverse=True)
        elif orderByStartTimeDesc:
            logger.debug("Sorting by starttime, descending...")
            records.sort(key=scsGetStartTimeFromTask, reverse=True)
        return records
  
               
class SmApiTaskResource(SmApiModelResource):
    task = fields.ForeignKey(SmApiTask, 'task')
    resource = fields.ForeignKey(SmApiResource, 'resource')
    resource_type_task_type = fields.ForeignKey(SmApiResourceTypeTaskType, 'resource_type_task_type')
    owner = fields.ForeignKey(SmApiUser, 'owner')
    status = fields.CharField(readonly=True)

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleOnlyToSelf())
        queryset = SmModelTaskResource.objects.all()
        resource_name = 'task_resource'
        filtering = smMakeFilterAllFieldsFilter(SmModelTaskResource)
        ordering = smMakeOrderingFieldList(SmModelTaskResource)
        serializer = SmarfSerializer()

    def obj_create(self, bundle, **kwargs):
        scsApiSetOwnerField(bundle)
        return super(SmApiTaskResource, self).obj_create(bundle, **kwargs)


class SmApiTaskTypeNote(SmOwnedModelResource):
    task_type = fields.ForeignKey(SmApiTaskType, 'task_type')
    owner = fields.ForeignKey(SmApiUser, 'owner')
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleOnlyToSelf())
        queryset = SmModelTaskTypeNote.objects.all()
        resource_name = 'task_type_note'
        filtering = smMakeFilterAllFieldsFilter(SmModelTaskTypeNote)
        ordering = smMakeOrderingFieldList(SmModelTaskTypeNote)
        serializer = SmarfSerializer()


class SmApiResourceNote(SmOwnedModelResource):
    resource = fields.ForeignKey(SmApiResource, 'resource')
    owner = fields.ForeignKey(SmApiUser, 'owner')
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleOnlyToSelf())
        queryset = SmModelResourceNote.objects.all()
        resource_name = 'resource_note'
        filtering = smMakeFilterAllFieldsFilter(SmModelResourceNote)
        ordering = smMakeOrderingFieldList(SmModelResourceNote)
        serializer = SmarfSerializer()


def scsApiSetOwnerField(bundle):
    if scsApiGetAuthOn():
        bundle.data["owner"] = bundle.request.user
    else:
        if "owner" not in bundle.data:
            bundle.data["owner"] = User.objects.get(id=1)


class SmApiCommunity(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    tag = fields.ForeignKey(SmApiTag, 'tag', null=True, blank=True)
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllCreateWithPermEditByOwner
                                             (permission_code="sm_controller.add_smmodelcommunity"))
        queryset = SmModelCommunity.objects.all()
        resource_name = 'community'
        filtering = smMakeFilterAllFieldsFilter(SmModelCommunity)
        ordering = smMakeOrderingFieldList(SmModelCommunity)
        serializer = SmarfSerializer()
        always_return_data = True

SM_OBJECT_GETNEWS_DICT = {
    "desc": "This URL allows sets of news items to be returned (newest first)",
    "params": [{
                "name": "excludeAfterId",
                "type": "int",
                "desc": "If set, news items after (newer than) this id are exlcuded. "
                        "This allows paging through old news items",
                "default": 1000000000,
               },
               {
                "name": "limit",
                "type": "int",
                "desc": "The maximum number of items to return",
                "default": 5,
               },
               {
                "name": "communityId",
                "type": "int",
                "desc": "The id of the community whose news should be returned - note that "
                        "the 'everything' community new is always returned",
                "default": -1,
               }
               ]
}        

SM_OBJECT_GETSYSTEMALERTNEWS_DICT = {
    "desc": "This URL returns the next system alert applicable to the community specified.",
    "params": [{
                "name": "lastIdAcked",
                "type": "int",
                "desc": "If set, the alert returned is the one before this alert",
                "default": 1000000000,
               },
               {
                "name": "communityId",
                "type": "int",
                "desc": "The id of the community whose news should be returned - note that "
                        "the 'everything' community new is always returned",
                "default": -1,
               }
               ]
}        


class SmApiNewsItem(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    community = fields.ForeignKey(SmApiCommunity, 'community')

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlyByOwnerOrLinkedObjectOwner("community"))
        queryset = SmModelNewsItems.objects.all()
        resource_name = 'news_item'
        filtering = smMakeFilterAllFieldsFilter(SmModelNewsItems)
        ordering = smMakeOrderingFieldList(SmModelNewsItems)
        ordering.append('release_date_time')
        serializer = SmarfSerializer()

    def convertModelObjectListToJson(self, request, objList):
        bundles = [self.build_bundle(obj=q, request=request) for q in objList]

        data = [self.full_dehydrate(bundle) for bundle in bundles]

        return self.serialize(None, data, 'application/json')

    def prepend_urls(self):
        '''
        Taken from http://stackoverflow.com/questions/9449749/filefield-in-tastypie
        See also https://django-tastypie.readthedocs.org/en/v0.9.11/cookbook.html
        '''
        return [
                # See views.py for the upload code
                url(r"^(?P<resource_name>%s)/getsystemalert%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('getsystemalert_detail'),
                    name="api_getsystemalert_detail"),
                # See views.py for the upload code
                url(r"^(?P<resource_name>%s)/getmynews%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('getmynews_detail'),
                    name="api_getmynews_detail"),
                ] 

    def getmynews_detail(self, request, **kwargs):
        """
        Get the the next system alert (oldest first)
        """
        logger.debug("Getting next system alert from news_item table")
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
        paramInfo = SmParamInfo2(SM_OBJECT_GETNEWS_DICT, "0", request)
        communityId = paramInfo.getParam("communityId")
        limit = paramInfo.getParam("limit")
        excludeAfterId = paramInfo.getParam("excludeAfterId")
        
        items = SmModelNewsItems.objects.filter(pk__lt=excludeAfterId).\
            filter(expiry_date_time__gt=sm_date_now()).order_by("-id")
        resp = []
        for item in items:
            if item.community.name_key == SM_COMMUNITY_ALL_NAME_KEY or item.community.id == communityId:
                resp.append(item)
                if len(resp) == limit:
                    break
        response = HttpOk(self.convertModelObjectListToJson(request, resp).encode("utf-8"))
        return response 
        
    def getsystemalert_detail(self, request, **kwargs):
        """
        Get the the next system alert (oldest first)
        """
        logger.debug("Getting next system alert from news_item table")
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
        paramInfo = SmParamInfo2(SM_OBJECT_GETSYSTEMALERTNEWS_DICT, "0", request)
        communityId = paramInfo.getParam("communityId")
        lastIdAcked = paramInfo.getParam("lastIdAcked")
        
        items = SmModelNewsItems.objects.filter(alert=True).filter(pk__lt=lastIdAcked).\
            filter(expiry_date_time__gt=sm_date_now()).order_by("-id")
        resp = []
        for item in items:
            if item.community.name_key == SM_COMMUNITY_ALL_NAME_KEY or item.community.id == communityId:
                resp.append(item)
                break
        response = HttpOk(self.convertModelObjectListToJson(request, resp).encode("utf-8"))
        return response 
        

class SmApiUserSetting(SmOwnedModelResource):
    owner = fields.OneToOneField(SmApiUser, 'owner')
    #user.full = True
    community = fields.ForeignKey(SmApiCommunity, 'community')
    community.full = True
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authz = ScsGdnAuthVisibleToAllEditOnlyByOwner()
        authz.deletesAllowed = False
        authorization = scsMakeAuthorization(authz)
        queryset = SmModelUserSetting.objects.all()
        resource_name = 'user_setting'
        filtering = smMakeFilterAllFieldsFilter(SmModelUserSetting)
        ordering = smMakeOrderingFieldList(SmModelUserSetting)
        serializer = SmarfSerializer()


class SmApiScript(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ResourceAuthorization(alter_permission_code="sm_controller.add_smmodeltasktype",
                                                                   run_permission_code="execute_task_type"))
        queryset = SmModelScript.objects.all()
        resource_name = 'script'
        filtering = smMakeFilterAllFieldsFilter(SmModelScript)
        ordering = smMakeOrderingFieldList(SmModelScript)
        serializer = SmarfSerializer()


class SmApiProperty(SmApiModelResource):
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlySuperuser())
        queryset = SmModelProperty.objects.all()
        resource_name = 'property'
        filtering = smMakeFilterAllFieldsFilter(SmModelProperty)
        ordering = smMakeOrderingFieldList(SmModelProperty)
        serializer = SmarfSerializer()


class SmApiReservation(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    resource = fields.ForeignKey(SmApiResource, 'resource')
    resource.full = True
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlyByOwner())
        queryset = SmModelReservation.objects.all()
        resource_name = 'reservation'
        filtering = smMakeFilterAllFieldsFilter(SmModelReservation)
        ordering = smMakeOrderingFieldList(SmModelReservation)
        serializer = SmarfSerializer()

    def obj_create(self, bundle, **kwargs):
        # Check that user has execute permission on the target resource
        apiResource = SmApiResource()
        resourceId = int(bundle.data["resource"].rsplit("/", 2)[1])
        if scsApiGetAuthOn():
            apiResource._meta.authorization.check_run(apiResource, 
                                                      SmModelResource.objects.get(id=resourceId), 
                                                      bundle.request)
        try:
            return super().obj_create(bundle, **kwargs)
        except ValidationError as e:
            raise ImmediateHttpResponse(HttpConflict409(json.dumps(e.error_dict), content_type="application/json"))
    

class SmApiFaq(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlyByOwner())
        queryset = SmModelFaq.objects.all()
        resource_name = 'faq'
        filtering = smMakeFilterAllFieldsFilter(SmModelFaq)
        ordering = smMakeOrderingFieldList(SmModelFaq)
        serializer = SmarfSerializer()


SM_REQUEST_DELETE_PARAM_DICT = {
    "desc": "This URL returns the next system alert applicable to the community specified.",
    "params": [{
                "name": "deny",
                "type": "bool",
                "desc": "If set, the user is sent an email saying that permission was not granted",
                "default": False,
               },
               {
                "name": "reason",
                "type": "string",
                "desc": "The reason that the request was denied",
                "default": "No reason given",
               }
               ]
}        


class SmApiRequest(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    requester = fields.ForeignKey(SmApiUser, 'requester')
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleOnlyToSelf())
        queryset = SmModelRequest.objects.all()
        resource_name = 'request'
        filtering = smMakeFilterAllFieldsFilter(SmModelRequest)
        ordering = smMakeOrderingFieldList(SmModelRequest)
        serializer = SmarfSerializer()
        
    def obj_delete(self, bundle, **kwargs):
        paramInfo = SmParamInfo2(SM_REQUEST_DELETE_PARAM_DICT, kwargs["pk"], bundle.request)
        if paramInfo.getParam("deny"):
            req = SmModelRequest.objects.get(id=paramInfo.getObjectId()) 
            tokens = str(req.target).split("/")
            logger.debug("Target is %s - Tokens are:", str(self.target))
            for token in tokens:
                logger.debug("Token is %s", token)
            objType = tokens[2]
            if objType == "resource":
                obj = SmModelResource.objects.get(id=int(tokens[3]))
            if objType == "task_type":
                obj = SmModelTaskType.objects.get(id=int(tokens[3]))
            scsSendPermChangeEmail(req.requester, ACCESS_DENIED_EMAIL_SUBJECT, ACCESS_DENIED_EMAIL_TEMPLATE, 
                                   objType, obj, ["", "", ""])            

        return super().obj_delete(bundle, **kwargs)
        
        
class SmBaseApiSchema(SmOwnedModelResource):
    def obj_create(self, bundle, **kwargs):
        try:
            return super(SmBaseApiSchema, self).obj_create(bundle, **kwargs)
        except IntegrityError as e:
            if e.args[0] == 1062 or e.args[0].startswith('duplicate key'):
                raise ImmediateHttpResponse(HttpConflict("Error: " + e.args[1] + "\n"))
        
            
class SmApiSchemaFull(SmBaseApiSchema):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    community = fields.ForeignKey(SmApiCommunity, 'community')
    resource_type = fields.ForeignKey(SmApiResourceType, 'resource_type')
    community.full = True
    owner.full = True
    resource_type.full = True

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlyByOwner())
        queryset = SmModelSchema.objects.all()
        resource_name = 'md_schema_full'
        filtering = smMakeFilterAllFieldsFilter(SmModelSchema)
        ordering = smMakeOrderingFieldList(SmModelSchema)
        serializer = SmarfSerializer()
        
        
class SmApiSchema(SmBaseApiSchema):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    community = fields.ForeignKey(SmApiCommunity, 'community')
    resource_type = fields.ForeignKey(SmApiResourceType, 'resource_type')

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlyByOwner())
        queryset = SmModelSchema.objects.all()
        resource_name = 'md_schema'
        filtering = smMakeFilterAllFieldsFilter(SmModelSchema)
        ordering = smMakeOrderingFieldList(SmModelSchema)
        serializer = SmarfSerializer()
        

class SmBaseApiMdRepo(SmOwnedModelResource):
    def obj_create(self, bundle, **kwargs):
        try:
            return super(SmBaseApiMdRepo, self).obj_create(bundle, **kwargs)
        except IntegrityError as e:
            if e.args[0] == 1062 or e.args[0].startswith('duplicate key'):
                raise ImmediateHttpResponse(HttpConflict("Error: " + e.args[1] + "\n"))
            
        
class SmApiMdRepo(SmBaseApiMdRepo):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    schema = fields.ForeignKey(SmApiSchema, 'schema')
    resources = fields.ToManyField(SmApiResource, 'resources')

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlyByOwner())
        queryset = SmModelMdRepo.objects.all()
        resource_name = 'md_repo'
        filtering = smMakeFilterAllFieldsFilter(SmModelMdRepo)
        ordering = smMakeOrderingFieldList(SmModelMdRepo)
        serializer = SmarfSerializer()
 
    def prepend_urls(self):
        '''
        Taken from http://stackoverflow.com/questions/9449749/filefield-in-tastypie
        See also https://django-tastypie.readthedocs.org/en/v0.9.11/cookbook.html
        '''
        return [
                # See views.py for the upload code
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/purge%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('purge_detail'),
                    name="api_purge_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/search%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('search_detail'),
                    name="api_search_detail"),
                ] 

    def search_detail(self, request, **kwargs):
        if scsApiGetAuthOn():
            self.is_authenticated(request)        

        if scsApiGetAuthOn():
            user = request.user
        else:
            user = User.objects.first()

        # Find any previous file_set and delete it.
        logger.debug("Deleting auto_deletable file_set")
        repo_id = kwargs["pk"]
        deletable_file_sets = SmModelFileSet.objects.filter(owner=user).\
            filter(auto_delete=True).filter(source="search")
        deletable_file_sets.delete()
        
        logger.debug("Doing the search")
        '''
        This search created a file_set object and populates it with the search results.
        '''
        self.method_check(request, allowed=['get'])
        # self.is_authenticated(request)
        self.throttle_check(request)
        self.log_throttled_access(request)
        schema = SmModelMdRepo.objects.get(id=repo_id).schema.name_key

        query_parameters = request.GET.copy()

        # Exclude "format" as tastypie uses it to determine serialization format and it causes the
        # expression parser to fail.
        if 'format' in query_parameters:
            del query_parameters['format']

        filters = []
        for key, value_list in query_parameters.lists():
            for value in value_list:
                filters.append((key, value))
        parsed_filters = []
        # Rebuild the query string
        for f, value in filters:
            if f[0] == '-':
                key = '-' + schema + ":" + f[1:]
            else:
                key = schema + ":" + f
            parsed_filters.append((key, value))
        logger.debug("Calling MetadataSearchEngine")
        engine = search.MetadataSearchEngine()
        logger.debug("Done MetadataSearchEngine")
        results = engine.search(parsed_filters, repo_id)

        search_info = query_parameters.copy()
        search_info["repo"] = "/scs/repo/" + str(repo_id) + "/"
        # Creating result records
        logger.debug("Creating result records")
        file_set = SmModelFileSet(name="Default Search Result", 
                                  name_key="default_search_result",
                                  owner=user,
                                  source="search",
                                  source_info=json.dumps(search_info)
                                  )
        file_set.save()
        file_set_item_list = []
        count = 0
        for result in results:
            file_set_item_list.append(SmModelFileSetItem(owner=user,
                                                         file_set=file_set,
                                                         resource=result.resource,
                                                         relative_path=result.relative_path))
            count += 1
            
        SmModelFileSetItem.objects.bulk_create(file_set_item_list)

        file_set_api_object = SmApiFileSet()
        
        bundle = file_set_api_object.build_bundle(obj=file_set, request=request)
        bundle = file_set_api_object.full_dehydrate(bundle)
            
        resp = {"records": count,
                "file_set": bundle.data}
        return self.create_response(request, resp)                    

        return HttpResponse(dumpsPretty(resp))                    

    def purge_detail(self, request, **kwargs):
        logger.debug("Rebuilding repo")
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
        paramInfo = SmParamInfo2(SM_EMPTY_PARAM_DICT, kwargs["pk"], request)
        repo = SmModelMdRepo.objects.get(id=paramInfo.getObjectId())
        logger.debug("Purging all records for repo %d", repo.id)
        SmModelPathList.objects.filter(repo=repo).delete()
                
        
class SmBaseApiPropertyType(SmOwnedModelResource):
    def obj_create(self, bundle, **kwargs):
        try:
            return super(SmBaseApiPropertyType, self).obj_create(bundle, **kwargs)
        except IntegrityError as e:
            if e.args[0] == 1062 or e.args[0].startswith('duplicate key'):
                raise ImmediateHttpResponse(HttpConflict("Error: " + e.args[1] + "\n"))
                    

class SmApiPropertyType(SmBaseApiPropertyType):
    schema = fields.ForeignKey(SmApiSchema, 'schema')

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlySuperuser())
        queryset = SmModelPropertyType.objects.all()
        resource_name = 'md_property_type'
        filtering = smMakeFilterAllFieldsFilter(SmModelPropertyType)
        ordering = smMakeOrderingFieldList(SmModelPropertyType)
        serializer = SmarfSerializer()
        
        
class SmApiPathList(SmApiModelResource):
    repo = fields.ForeignKey(SmApiMdRepo, 'repo')
    resource = fields.ForeignKey(SmApiResource, 'resource')
    #resource.full = True
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlySuperuser())
        queryset = SmModelPathList.objects.all()
        resource_name = 'md_path'
        filtering = smMakeFilterAllFieldsFilter(SmModelPathList)
        ordering = smMakeOrderingFieldList(SmModelPathList)
        serializer = SmarfSerializer()
        
    def prepend_urls(self):
        rn = self._meta.resource_name
        return [url(r'^(?P<resource_name>%s)/search%s$' % (rn, trailing_slash()), self.wrap_view('search')), ]

    def search(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        # self.is_authenticated(request)
        self.throttle_check(request)
        self.log_throttled_access(request)

        query_parameters = request.GET.copy()

        # Exclude "format" as tastypie uses it to determine serialization format and it causes the
        # expression parser to fail.
        if 'format' in query_parameters:
            del query_parameters['format']

        filters = []
        schema = []
        repo = [] 
        for key, value_list in query_parameters.lists():
            for value in value_list:
                if key == "repo":
                    schema = SmModelMdRepo.objects.get(id=value).schema.name_key
                    repo = value
                else:
                    filters.append((key, value))
        parsed_filters = []
        # Rebuild the query string
        for f, value in filters:
            if f[0] == '-':
                key = '-' + schema + ":" + f[1:]
            else:
                key = schema + ":" + f
            parsed_filters.append((key, value))
        logger.debug("Calling MetadataSearchEngine")
        engine = search.MetadataSearchEngine()
        logger.debug("Done MetadataSearchEngine create")
        objects = []     
        logger.debug("Starting search...")
        res = engine.search(parsed_filters, repo)   
        logger.debug("Done search")
        for result in res:
            bundle = self.build_bundle(obj=result, request=request)
            bundle = self.full_dehydrate(bundle)
            
            objects.append(bundle)
  
        object_list = {
            'objects': objects,
        }

        return self.create_response(request, object_list)                    
        
        
class SmApiMdPropertywithType(SmApiModelResource):
    path = fields.ForeignKey(SmApiPathList, 'path')
    property_type = fields.ForeignKey(SmApiPropertyType, 'property_type')
    property_type.full = True
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlySuperuser())
        queryset = SmModelMdProperty.objects.all()
        resource_name = 'md_property_with_type'
        filtering = smMakeFilterAllFieldsFilter(SmModelMdProperty)
        ordering = smMakeOrderingFieldList(SmModelMdProperty)
        serializer = SmarfSerializer()
                                                
        
class SmApiMdProperty(SmApiModelResource):
    path = fields.ForeignKey(SmApiPathList, 'path')
    property_type = fields.ForeignKey(SmApiPropertyType, 'property_type')

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlySuperuser())
        queryset = SmModelMdProperty.objects.all()
        resource_name = 'md_property'
        filtering = smMakeFilterAllFieldsFilter(SmModelMdProperty)
        ordering = smMakeOrderingFieldList(SmModelMdProperty)
        serializer = SmarfSerializer()                                        


class SmApiMdExtractorType(SmApiModelResource):
    schema = fields.ForeignKey(SmApiSchema, 'schema')
    task_type = fields.ForeignKey(SmApiTaskType, 'task_type')
    resource_type = fields.ForeignKey(SmApiResourceType, 'resource_type')

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlySuperuser())
        queryset = SmModelMdExtractorType.objects.all()
        resource_name = 'md_extractor_type'
        filtering = smMakeFilterAllFieldsFilter(SmModelMdExtractorType)
        ordering = smMakeOrderingFieldList(SmModelMdExtractorType)
        serializer = SmarfSerializer()                                
        
        
class SmApiExtractor(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    compute_resource = fields.ForeignKey(SmApiResource, 'compute_resource')
    repo = fields.ForeignKey(SmApiMdRepo, 'repo')
    extractor_type = fields.ForeignKey(SmApiMdExtractorType, 'extractor_type')

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlyByOwner())
        queryset = SmModelMdExtractor.objects.all()
        resource_name = 'md_extractor'
        filtering = smMakeFilterAllFieldsFilter(SmModelMdExtractor)
        ordering = smMakeOrderingFieldList(SmModelMdExtractor)
        serializer = SmarfSerializer()                                
        
    def prepend_urls(self):
        '''
        Taken from http://stackoverflow.com/questions/9449749/filefield-in-tastypie
        See also https://django-tastypie.readthedocs.org/en/v0.9.11/cookbook.html
        '''
        return [
                # See views.py for the upload code
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/rebuild%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('rebuild_detail'),
                    name="api_rebuild_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/update%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('update_detail'),
                    name="api_update_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/cancel%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('cancel_detail'),
                    name="api_cancel_detail"),
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/retry_errors%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('retry_errors_detail'),
                    name="api_retry_errors_detail"),
                ] 

    def cancel_detail(self, request, **kwargs):
        logger.debug("Rebuilding repo")
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
        paramInfo = SmParamInfo2(SM_EMPTY_PARAM_DICT, kwargs["pk"], request)
        extractor = SmModelMdExtractor.objects.get(id=paramInfo.getObjectId())

        if extractor.state == SCS_API_EXTRACTION_STATE_DONE:
            return
        extractor.state = SCS_API_EXTRACTION_STATE_CANCELLING
        extractor.save()
        response = HttpOk(json_utils.dumpsPretty({}).encode("utf-8"))
        return response 

    def rebuild_detail(self, request, **kwargs):
        """
        Delete any preexisting metadata for the repo associated with this 
        extractor and start the extraction again.
        """
        logger.debug("Rebuilding repo")
        if scsApiGetAuthOn():
            self.is_authenticated(request)        
        paramInfo = SmParamInfo2(SM_EMPTY_PARAM_DICT, kwargs["pk"], request)
        extractor = SmModelMdExtractor.objects.get(id=paramInfo.getObjectId())

        for resource in extractor.repo.resources.all():
            SmApiResource._meta.authorization.check_read_data(self, resource, request)
        SmApiResource._meta.authorization.check_run(self, extractor.compute_resource, request)
        
        sm_controller.api_extract.scsExtractorMarkRunning(extractor)
        
        # Start thread to do the work
        thread = sm_controller.api_extract.ScsApiExtractorThread("rebuild", extractor)
        thread.start()
        
        response = HttpOk(json_utils.dumpsPretty({}).encode("utf-8"))
        return response 
     
        
class SmApiExtractorFull(SmOwnedModelResource):
    '''
    Clone of SmApiExtractor with all referenced objects expanded
    '''
    owner = fields.ForeignKey(SmApiUser, 'owner')
    compute_resource = fields.ForeignKey(SmApiResource, 'compute_resource', full=True)
    repo = fields.ForeignKey(SmApiMdRepo, 'repo', full=True)
    extractor_type = fields.ForeignKey(SmApiMdExtractorType, 'extractor_type', full=True)

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlyByOwner())
        queryset = SmModelMdExtractor.objects.all()
        resource_name = 'md_extractor_full'
        filtering = smMakeFilterAllFieldsFilter(SmModelMdExtractor)
        ordering = smMakeOrderingFieldList(SmModelMdExtractor)
        serializer = SmarfSerializer()                                
        
        
class SmApiFileSet(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    create_date_time = fields.CharField('create_date_time', readonly=True)
    source = fields.CharField('source', readonly=True)

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleOnlyToSelf())
        queryset = SmModelFileSet.objects.all()
        resource_name = 'file_set'
        filtering = smMakeFilterAllFieldsFilter(SmModelFileSet)
        ordering = smMakeOrderingFieldList(SmModelFileSet)
        serializer = SmarfSerializer()                                
        
    def prepend_urls(self):
        '''
        Taken from http://stackoverflow.com/questions/9449749/filefield-in-tastypie
        See also https://django-tastypie.readthedocs.org/en/v0.9.11/cookbook.html
        '''
        return [
                url(r"^(?P<resource_name>%s)/(?P<pk>.*)/downloadzip%s$" % 
                    (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('downloadzip_detail'),
                    name="api_downloadzip_detail")
                ]
        
    def downloadzip_detail(self, request, **kwargs):
        """
        Make a zip file from the files in the file set
        """            
        logger.debug("kwargs is %s", kwargs)
        paramInfo = SmParamInfo2(SM_RESOURCE_DOWNLOADZIP_PARAM_DICT, kwargs["pk"], request)
        name = paramInfo.getParam("name")
        # Make ZIP Stream
        zip_file = zipstream.ZipFile(mode='w', compression=zipstream.ZIP_STORED)
        
        file_set = SmModelFileSet.objects.get(id=paramInfo.getObjectId())
        file_set_items = SmModelFileSetItem.objects.filter(file_set=file_set)
        resObjMap = {}
        logger.debug("Making zip stream for %d files", file_set_items.count())
        for file_set_item in file_set_items:
            res_id_str = str(file_set_item.resource.id)
            if res_id_str not in resObjMap:
                resourceObject = file_set_item.resource
                resourceApiObject = SmApiResource()
                resourceApiObject._meta.authorization.check_read_data(resourceApiObject, resourceObject, request)
                resObjMap[res_id_str] = resourceObject
            fileAcc2 = resourceApiObject.makeFileAcc(paramInfo, resObjMap[res_id_str])
            file_name = file_set_item.relative_path
            zip_file.write_iter(file_name, 
                                SmFileAccessorIterator(file_name, fileAcc2, False))
            logger.debug("Writing file %s to the zip stream", file_name)
        response = StreamingHttpResponse(zip_file, content_type="application/zip")
        response['Content-Disposition'] = 'attachment; filename="' + name + '"'
        if scsApiGetAuthOn():
            action.send(request.user, verb="downloadzip", target=resourceObject)
        else:
            action.send(resourceObject.owner, verb="downloadzip", target=resourceObject)
        statsd.incr(SCS_METRIC_NUM_DOWNLOADS)
        return response 
           
SM_FILE_SET_ITEM_GET_PARAM_DICT = {
    "desc": "This URL allows copy operations to a destination folder",
    "params": [{
                "name": "showProps",
                "type": "bool",
                "desc": "This is true to return properties with a file_set_item'",
                "default": False,
               },
               {
                "name": "downloadAsFile",
                "type": "bool",
                "desc": "If this is true then the set of file_set_item records" +
                " is returned with headers to direct the browser to save it as a file",
                "default": False,
               }]
}        

        
class SmApiFileSetItem(SmOwnedModelResource):
    owner = fields.ForeignKey(SmApiUser, 'owner')
    resource = fields.ForeignKey(SmApiResource, 'resource')
    file_set = fields.ForeignKey(SmApiFileSet, 'file_set')

    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleOnlyToSelf())
        queryset = SmModelFileSetItem.objects.all()
        resource_name = 'file_set_item'
        filtering = {"file_set": ALL_WITH_RELATIONS}
        ordering = smMakeOrderingFieldList(SmModelFileSetItem)
        serializer = SmarfSerializer()                                
        
    def obj_get_list(self, bundle, **kwargs):
        '''
        Need to check for CSV request and return data in CSV format in that case
        '''
        paramsInRequest = bundle.request.GET
        fmt = paramsInRequest.get("format")
        resp = super().obj_get_list(bundle, **kwargs)
        if fmt == "csv":
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="results.csv"'
            writer = csv.writer(response)
            for item in resp:
                writer.writerow(["/scs/resource/" + str(item.resource.id) + "/", 
                                 item.relative_path])
            raise ImmediateHttpResponse(response)

        downloadAsFile = paramsInRequest.get("downloadAsFile")
        if downloadAsFile is not None and downloadAsFile.lower() == "true":
            bundles = [self.build_bundle(obj=q, request=bundle.request) for q in resp]
            data = [self.full_dehydrate(b) for b in bundles]
            response = HttpResponse(self.serialize(None, data, 'application/json'),
                                    content_type='application/json')
            response['Content-Disposition'] = 'attachment; filename="results.json"'
            raise ImmediateHttpResponse(response)
        return resp
    
    def dehydrate(self, bundle):
        paramInfo = SmParamInfo2(SM_FILE_SET_ITEM_GET_PARAM_DICT, str(bundle.obj.id), bundle.request)
        #logger.debug("paramInfo.getParam('ext'') = %s", str(paramInfo.getParam("extForUser")))
        showProps = paramInfo.getParam("showProps")
        if showProps:
            #logger.debug("In check for extra stuff, bundle user = %s..", str(bundle.request.user))
            props = SmModelMdProperty.objects.filter(path__relative_path=bundle.obj.relative_path).\
                filter(path__resource=bundle.obj.resource)
            propDict = {}
            for prop in props:
                propDict[prop.property_type.name] = prop.value
            bundle.data["props"] = propDict
                
        return bundle
    
        