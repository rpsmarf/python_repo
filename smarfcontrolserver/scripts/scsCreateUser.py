#!/usr/bin/env python 
'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import os
import sys
sys.path.insert(1, "/opt/smarf-scs/scs")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "smarfcontrolserver.settings")
from django.contrib.auth.models import User


def create_users(num_create):
    
    if(num_create <= 0):
        print("Cannot create 0 users. Exiting")
        return False
        
    num_users = User.objects.count()
    
    num_users_start = num_users
    
    for num_users in range(num_users, num_users + num_create):    
        #Line up users to id numbers
        n = str(num_users + 1)    
        User.objects.create_user('user' + n, 'user' + n + '@rpsmarf.ca', 'user' + n)

    print('Created ' + str(int(n) - num_users_start) + ' users')
    print('There are now ' + n + ' total users in the database')


if __name__ == "__main__":
    argv = sys.argv
    if len(argv) == 1:
        print('Usage: scsCreateUser number_to_create')
        sys.exit()
    elif len(argv) == 2:
        if argv[1] == '0':
            print('ERROR: Cannot add 0 users.')
            sys.exit()
        elif not argv[1].isdigit():
            print('ERROR: Please enter a positive integer.')
            sys.exit()
        print('Creating ' + str(argv[1]) + ' users.')
        num_create = argv[1]
    else:
        print('ERROR: Multiple arguments is not currently supported.')
        sys.exit()
        
    create_users(int(num_create))
