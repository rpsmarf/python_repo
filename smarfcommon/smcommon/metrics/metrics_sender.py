'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jun 23, 2015

    @author: rpsmarf
'''
from threading import Thread
import time
import logging
from statsd.defaults.env import statsd
from smcommon.globals.envvar import SM_ENV_VAR_COMMON_METRIC_DEST_HOST,\
    smCommonGetEnvVar, SM_ENV_VAR_COMMON_METRIC_DEST_PORT, smCommonGetEnvVarInt

logger = logging.getLogger(__name__)

METRIC_TYPE_COUNTER = "counter"
METRIC_TYPE_GUAGE = "guage"
 
 
class SmMetricSender(Thread):
    '''
    This class represents a thread which wakes up periodically to do
    callbacks to get and send metrics
    '''
    
    def __init__(self, timeBetweenChecks):
        '''
        Constructor
        '''
        super().__init__()
        self.timeBetweenChecks = timeBetweenChecks
        self.quit = False
        self.name = "MetricsSender"
        self.intervalDict = {}
        
    def registerCallback(self, metricName, metricType, intervalInSec, callback): 
        strInterval = str(intervalInSec)
        if strInterval not in self.intervalDict:
            intervalList = []
            self.intervalDict[strInterval] = intervalList
        else:
            intervalList = self.intervalDict[strInterval]
            
        intervalList.append((metricName, metricType, callback))
         
    def stop(self):
        self.quit = True
        if self.isAlive():
            self.join(2 * self.timeBetweenChecks)

    def _doCallbacks(self, secondsSinceStart):
        for intervalStr in self.intervalDict.keys():
            interval = int(intervalStr)
            if (interval == -1) or (secondsSinceStart % interval == 0):
                intervalList = self.intervalDict[intervalStr]
                for metricName, metricType, callback in intervalList:
                    try:
                        logger.debug("Doing callback for metric %s", metricName)
                        value = callback() 
                        logger.debug("Sending value %d for metric %s of type %s to %s:%d",
                                     value, metricName, metricType,
                                     smCommonGetEnvVar(SM_ENV_VAR_COMMON_METRIC_DEST_HOST),
                                     smCommonGetEnvVarInt(SM_ENV_VAR_COMMON_METRIC_DEST_PORT))
                        if metricType == METRIC_TYPE_COUNTER:
                            statsd.incr(metricName, value)
                        elif metricType == METRIC_TYPE_GUAGE:
                            statsd.gauge(metricName, value)        
                    except:
                        logger.exception("Error in metric sender")

    def run(self):
        logger.info("MetricSender thread now running")
        secondsSinceStart = 0
        while not self.quit:
            time.sleep(self.timeBetweenChecks)
            secondsSinceStart += self.timeBetweenChecks
            self._doCallbacks(secondsSinceStart)
            

            
