import json
import sys
from scripts.scs_obj_common import scsScriptGetDefaultUrl
from smcommon.globals.envvar import SM_ENV_VAR_TEST_PASSWORD, smCommonGetEnvVar
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath, scsGetToJson
from scripts.scs_obj_create import scsCreateObjectPath, scsCreateObjectId,\
    scsModifyObject

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

root_path = scsGetObjectPath("user", "username=rpsmarf")
all_users_group_path = scsGetObjectPath("group", "name=all_users")
userList = [scsGetObjectPath("user", "username=rpsmarf"),
            scsGetObjectPath("user", "username=andrewmcgregor"),
            scsGetObjectPath("user", "username=navdeepk"),
            scsGetObjectPath("user", "username=bnandy"),
            scsGetObjectPath("user", "username=sikharesh"),
            scsGetObjectPath("user", "username=marc_sthilaire"),
            ]

bearing_community_tag_path = scsCreateObjectPath("tag", 
                                                 {"tag_name": "community:bearing_mon"})
bearing_community_path = scsCreateObjectPath("community", 
                                     {"name": "Bearing Monitoring",
                                      "name_key": "bearing_mon",
                                      "description": "This community contains all "
                                      "resources associated with engine bearing monitoring.",
                                      "owner": root_path,
                                      "tag": bearing_community_tag_path,
                                      "imageUrl": "https://www.rpsmarf.ca/icons-scs/bearingCommunityIcon.jpg",
                                      })

user_kun_id = scsCreateObjectId("user", {"username": "kun",
                                         "email": "kun@carleton.ca",
                                         "password": smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD),
                                         "first_name": "Kun",
                                         "last_name": "Zhuang"})
scsModifyObject("/scs/user_setting/" + str(user_kun_id) + "/", {"community": bearing_community_path})
user_kun = scsGetObjectPath("user", "username=kun")
userList.append(user_kun)

scsCreateObjectPath("news_item", 
                    {"owner": root_path,
                     "headline": "New Version of WSAT Released",
                     "body": "Kun Zhuang has released a new version of WSAT.  It is compatible with MatLab 2014b "
                     "and includes features to generate new data files and process a folder of files.",
                     "community": bearing_community_path,
                     })

scsCreateObjectPath("news_item", 
                    {"owner": root_path,
                     "headline": "New Generated Data Files Available",
                     "body": "6 new data files are now available which are based on existing data files "
                     "with noise added.",
                     "community": bearing_community_path,
                     })


carleton_agent_path = scsGetObjectPath("agent", "name_key=carleton_agent")

rt_wsat_data_path = scsCreateObjectPath("resource_type", {"name": "wsat_tool_data_source", 
                                                     "nature": "data",
                                                     "description": "This is a source of WSAT data"})

wsat_container_path = scsCreateObjectPath("container", {"name": "WSAT Data Container", 
                                                        "name_key": "carleton_wsat_data",
                                                        "containerUrl": "local://localhost/mnt/wsat-data/",
                                                        "description": 
                                                        "This container provides access to WSAT data held at Carleton",
                                                        "agent": carleton_agent_path,
                                                        "status": "up"})

carleton_wsat_runner = scsCreateObjectPath("container", {"name": "Carleton WSAT Node", 
                                                         "name_key": "carleton_wsat_runner",   
                                                         "description": 
                                                         "This container provides access to a node to run WSAT",
                                                         "containerUrl": 
                                                         "ssh://majumdar-11.sce.carleton.ca:22/cygdrive/c/temp",
                                                         "agent": carleton_agent_path,
                                                         "status": "up"})
carleton_wsat_data = scsGetObjectPath("container", "name_key=maj11_storage") 

rt_cloud_files_data_path = scsGetObjectPath("resource_type", "name_key=generic_storage")

rt_wsat_compute_path = scsCreateObjectPath("resource_type", 
                                           {"name": "WSAT Tool Runner", 
                                            "nature": "compute",
                                            "description": "This is a computing resource which can run the WSAT tool"})

res_path = scsCreateObjectPath("resource", {"name": "WSAT Running Resource", 
                               "name_key": "maj11_win_wsat_runner",
                               "resource_type": rt_wsat_compute_path,
                               "owner": root_path,
                               "parametersJson": 
                               json.dumps({"taskViewBaseUrl": 
                                           "http://majumdar-10.sce.carleton.ca:9214/guacamole/client.xhtml?id=c%2Fmaj-11",
                                           "keyfile": "dropbear_key",
                                           "user": "rpsmarf",
                                           "tempDataFolder": "/cygdrive/c/temp"
                                           }),
                               "container": carleton_wsat_runner,
                               "description": "This is the Carleton-based WSAT Windows host"
                               }
                               )
scsGetToJson(res_path + "setperm/?action=assign&group=" + all_users_group_path + "&perm=x")

scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

rt_wsat_data_txt_path = scsCreateObjectPath("resource_type", 
                                        {"name": "WSAT Tool Data (Txt)", 
                                         "name_key": "wsat_txt",
                                         "nature": "data",
                                         "description": "Repository of data which can be consumed by the WSAT tool"})

rt_wsat_data_mat_path = scsCreateObjectPath("resource_type", 
                                        {"name": "WSAT Tool Data (Matlab)", 
                                         "name_key": "wsat_mat",
                                         "nature": "data",
                                         "description": "This is a repository of data in Matlab format"})

rt_wsat_data_folder_path = scsCreateObjectPath("resource_type", 
                                        {"name": "WSAT Tool Data Set", 
                                         "name_key": "wsat_folder",
                                         "nature": "data",
                                         "description": "This is a repository of a set of WSAT files"})

print("Creating the interactive task type for files...")

TASK_TYPE_wsat_INTERACTIVE_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", 
                                      "name": "Argument", 
                                      "value": "/cygdrive/c/Dev/RPSMARF/WSAT/WSAT_1.12/WSAT_START_BASH.sh"},
                                     {"type": "resource", 
                                      "name": "File of input data", 
                                      "pathType": "file", 
                                      "input_id": 1, 
                                      "direction": "input", 
                                      "role": "wsat_src", 
                                      "name": "Data to analyze",
                                      "description": "This specifies the data file to submit for analysis"},
                                     ],
                            "computeResourceRole": "wsat_compute_inter2",
                            "interactive": True
                       }
tt_interactive_path = scsCreateObjectPath("task_type", {"name": "Single File WSAT - Interactive",
                                            "name_key": "win_wsat_inter_file",
                                            "description": " This tool runs the WSAT application interactively "
                                            + "on a single file of data.", 
                                            "code_module": "smcommon.task_runners.task_runner_shell", 
                                            "code_classname": "SmTaskRunnerShell", 
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_wsat_INTERACTIVE_CFG_DICT)})
for userPath in userList:
    scsGetToJson(tt_interactive_path + "setperm/?action=assign&user=" + userPath + "&perm=x")
scsCreateObjectPath("task_type_tag", {"tag": bearing_community_tag_path, "task_type": tt_interactive_path})

scsCreateObjectPath("resource_type_task_type", {"role": "wsat_src", "task_type": tt_interactive_path, 
                                                "resource_type": rt_wsat_data_txt_path,
                                                "description": "This is the data resource for the WSAT tool",
                                                "nature": "data",
                                                "name": "WSAT RT3 Interactive",
                                                "name_key": "wsat_data_file_inter"})

scsCreateObjectPath("resource_type_task_type", {"role": "wsat_compute_inter2", 
                                                "task_type": tt_interactive_path, 
                                                "resource_type": rt_wsat_compute_path,
                                                "description": "This is the compute resource for the WSAT tool",
                                                "nature": "compute",
                                                "name": "WSAT RT3 Interactive compute2",
                                                "name_key": "wsat_compute_inter2"})

print("Creating the batch task type for file input...")

TASK_TYPE_wsat_BATCH_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", 
                                      "name": "Argument", 
                                      "value": "/cygdrive/c/Dev/RPSMARF/WSAT/WSAT_1.12/WSAT_START_BASH.sh"},
                                     {"type": "resource", 
                                      "name": "File of control parameters", 
                                      "pathType": "file", 
                                      "input_id": 1, 
                                      "direction": "input", 
                                      "role": "wsat_src", 
                                      "name": "Data to analyze",
                                      "description": "This specifies the file of data to submit for analysis"},
                                     {"type": "resource", 
                                      "name": "Output folder containing the generated graph", 
                                      "pathType": "folder", 
                                      "input_id": 2, 
                                      "direction": "output", 
                                      "role": "wsat_dest", 
                                      "name": "Output folder",
                                      "description": "This specified the location to which the output data will be saved"},
                                     ],
                            "computeResourceRole": "wsat_compute_batch",
                            "outputProgress": {"outputArg": 2, "maxSize": 120000},
                            "interactive": False
                       }
tt_batch_path = scsCreateObjectPath("task_type", {"name": "Single File WSAT - File Output",
                                            "name_key": "win_wsat_batch",
                                            "description": " This tool runs the WSAT application in batch mode " +
                                            "to create a graph and XLS file from a single file of input data.", 
                                            "code_module": "smcommon.task_runners.task_runner_shell", 
                                            "code_classname": "SmTaskRunnerShell", 
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_wsat_BATCH_CFG_DICT)})
for userPath in userList:
    scsGetToJson(tt_batch_path + "setperm/?action=assign&user=" + userPath + "&perm=x")
scsCreateObjectPath("task_type_tag", {"tag": bearing_community_tag_path, "task_type": tt_batch_path})

scsCreateObjectPath("resource_type_task_type", {"role": "wsat_src", "task_type": tt_batch_path, 
                                                "resource_type": rt_wsat_data_txt_path,
                                                "description": "This is the data resource for the WSAT tool",
                                                "nature": "data",
                                                "name": "WSAT RT3 Batch",
                                                "name_key": "wsat_data_batch"})

scsCreateObjectPath("resource_type_task_type", {"role": "wsat_dest", "task_type": tt_batch_path, 
                                                "resource_type": rt_cloud_files_data_path,
                                                "description": "This is the data resource for the WSAT tool batch output",
                                                "nature": "data",
                                                "name": "WSAT Batch Dest",
                                                "name_key": "wsat_batch_dest"})

scsCreateObjectPath("resource_type_task_type", {"role": "wsat_compute_batch", 
                                                "task_type": tt_batch_path, 
                                                "resource_type": rt_wsat_compute_path,
                                                "description": "This is the compute resource for the WSAT tool",
                                                "nature": "compute",
                                                "name": "WSAT RT3 Batch Compute",
                                                "name_key": "wsat_compute_batch"})

print("Creating the interactive task type for folders...")

TASK_TYPE_wsat_INTERACTIVE_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", 
                                      "name": "Argument", 
                                      "value": "/cygdrive/c/Dev/RPSMARF/WSAT/WSAT_1.12/WSAT_START_BASH.sh"},
                                     {"type": "resource", 
                                      "name": "Folder of input data", 
                                      "pathType": "folder", 
                                      "input_id": 1, 
                                      "direction": "input", 
                                      "role": "wsat_src", 
                                      "name": "Data to analyze",
                                      "description": "This specifies the data files to submit for analysis"},            
                                     ],
                            "computeResourceRole": "wsat_compute_inter",
                            "interactive": True
                       }
tt_interactive_path = scsCreateObjectPath("task_type", {"name": "Multi-File WSAT - Interactive",
                                            "name_key": "win_wsat_inter_folder",
                                            "description": " This tool runs the WSAT application on " + 
                                            "a set of data files interactively ", 
                                            "code_module": "smcommon.task_runners.task_runner_shell", 
                                            "code_classname": "SmTaskRunnerShell", 
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_wsat_INTERACTIVE_CFG_DICT)})
for userPath in userList:
    scsGetToJson(tt_interactive_path + "setperm/?action=assign&user=" + userPath + "&perm=x")
scsCreateObjectPath("task_type_tag", {"tag": bearing_community_tag_path, "task_type": tt_interactive_path})

scsCreateObjectPath("resource_type_task_type", {"role": "wsat_src", "task_type": tt_interactive_path, 
                                                "resource_type": rt_wsat_data_folder_path,
                                                "description": "This is the data resource for the WSAT tool",
                                                "nature": "data",
                                                "name": "WSAT RT3 Interactive Folder",
                                                "name_key": "wsat_data_folder_inter"})

scsCreateObjectPath("resource_type_task_type", {"role": "wsat_compute_inter", 
                                                "task_type": tt_interactive_path, 
                                                "resource_type": rt_wsat_compute_path,
                                                "description": "This is the compute resource for the WSAT tool",
                                                "nature": "compute",
                                                "name": "WSAT RT3 Interactive Folder compute",
                                                "name_key": "wsat_compute_inter"})

print("Creating the batch task type for folder input...")

TASK_TYPE_wsat_BATCH_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", 
                                      "name": "Argument", 
                                      "value": "/cygdrive/c/Dev/RPSMARF/WSAT/WSAT_1.12/WSAT_START_BASH.sh"},
                                     {"type": "resource", 
                                      "name": "File of control parameters", 
                                      "pathType": "folder", 
                                      "input_id": 1, 
                                      "direction": "input", 
                                      "role": "wsat_src", 
                                      "name": "Data to analyze",
                                      "description": "This specifies the file of data to submit for analysis"},
                                     {"type": "resource", 
                                      "name": "Output folder containing the generated graph", 
                                      "pathType": "folder", 
                                      "input_id": 2, 
                                      "direction": "output", 
                                      "role": "wsat_dest", 
                                      "name": "Output folder",
                                      "description": "This specified the location to which the output data will be saved"},
                                     ],
                            "outputProgress": {"outputArg": 2, "maxSize": 880000},
                            "computeResourceRole": "wsat_compute_batch",
                            "interactive": False
                       }
tt_batch_path = scsCreateObjectPath("task_type", {"name": "Multi-File WSAT - File Output",
                                            "name_key": "win_wsat_batch_folder",
                                            "description": " This tool runs the WSAT application in batch mode " + 
                                            "across a set of files producing a set of images", 
                                            "code_module": "smcommon.task_runners.task_runner_shell", 
                                            "code_classname": "SmTaskRunnerShell", 
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_wsat_BATCH_CFG_DICT)})
for userPath in userList:
    scsGetToJson(tt_batch_path + "setperm/?action=assign&user=" + userPath + "&perm=x")
scsCreateObjectPath("task_type_tag", {"tag": bearing_community_tag_path, "task_type": tt_batch_path})

scsCreateObjectPath("resource_type_task_type", {"role": "wsat_src", "task_type": tt_batch_path, 
                                                "resource_type": rt_wsat_data_folder_path,
                                                "description": "This is the data resource for the WSAT tool",
                                                "nature": "data",
                                                "name": "WSAT RT3 Batch2",
                                                "name_key": "wsat_data_batch2"})

scsCreateObjectPath("resource_type_task_type", {"role": "wsat_dest", "task_type": tt_batch_path, 
                                                "resource_type": rt_cloud_files_data_path,
                                                "description": "This is the data resource for the WSAT tool batch output",
                                                "nature": "data",
                                                "name": "WSAT Batch Dest2",
                                                "name_key": "wsat_batch_dest2"})

scsCreateObjectPath("resource_type_task_type", {"role": "wsat_compute_batch", 
                                                "task_type": tt_batch_path, 
                                                "resource_type": rt_wsat_compute_path,
                                                "description": "This is the compute resource for the WSAT tool",
                                                "nature": "compute",
                                                "name": "WSAT RT3 Batch Compute2",
                                                "name_key": "wsat_compute_batch2"})

print("Creating the resources...")

res_path = scsCreateObjectPath("resource", {"name": "Original Data - Normal", 
                                            "name_key": "original_normal",
                                            "resource_type": rt_wsat_data_txt_path,
                                            "owner": root_path,
                                            "container": wsat_container_path,
                                            "parametersJson": json.dumps({"folder": "original-txt/normal/"}),
                                            "description": "Original measured data without any fault present"
                                            })
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=rw")
scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

res_path = scsCreateObjectPath("resource", {"name": "Original Data - Inner", 
                                            "name_key": "original_inner",
                                            "resource_type": rt_wsat_data_txt_path,
                                            "owner": root_path,
                                            "container": wsat_container_path,
                                            "parametersJson": json.dumps({"folder": "original-txt/inner/"}),
                                            "description": "Original measured data with a fault in the inner race"
                                            })
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=rw")
scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

res_path = scsCreateObjectPath("resource", {"name": "Original Data - Outer", 
                                            "name_key": "original_outer",
                                            "resource_type": rt_wsat_data_txt_path,
                                            "owner": root_path,
                                            "container": wsat_container_path,
                                            "parametersJson": json.dumps({"folder": "original-txt/outer/"}),
                                            "description": "Original measured data with a fault in the outer race"
                                            })
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=rw")
scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

res_path = scsCreateObjectPath("resource", {"name": "Generated Data Data Set 1 - Normal", 
                                            "name_key": "generated_normal_1",
                                            "resource_type": rt_wsat_data_txt_path,
                                            "owner": root_path,
                                            "container": wsat_container_path,
                                            "parametersJson": json.dumps({"folder": "generated-txt/normal-1/"}),
                                            "description": "Generated measured data without any fault present"
                                            })
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=rw")
scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

res_path = scsCreateObjectPath("resource", {"name": "Generated Data Set 1 - Inner", 
                                            "name_key": "generated_inner_1",
                                            "resource_type": rt_wsat_data_txt_path,
                                            "owner": root_path,
                                            "container": wsat_container_path,
                                            "parametersJson": json.dumps({"folder": "generated-txt/inner-1/"}),
                                            "description": "Generated measured data with a fault in the inner race"
                                            })
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=rw")
scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

res_path = scsCreateObjectPath("resource", {"name": "Generated Data Set 1 - Outer", 
                                            "name_key": "generated_outer_1",
                                            "resource_type": rt_wsat_data_txt_path,
                                            "owner": root_path,
                                            "container": wsat_container_path,
                                            "parametersJson": json.dumps({"folder": "generated-txt/outer-1/"}),
                                            "description": "Generated measured data with a fault in the outer race"
                                            })
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=rw")
scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

res_path = scsCreateObjectPath("resource", {"name": "Generated Data Data Set 2 - Normal", 
                                            "name_key": "generated_normal_2",
                                            "resource_type": rt_wsat_data_txt_path,
                                            "owner": root_path,
                                            "container": wsat_container_path,
                                            "parametersJson": json.dumps({"folder": "generated-txt/normal-2/"}),
                                            "description": "Generated measured data without any fault present"
                                            })
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=rw")
scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

res_path = scsCreateObjectPath("resource", {"name": "Generated Data Set 2 - Inner", 
                                            "name_key": "generated_inner_2",
                                            "resource_type": rt_wsat_data_txt_path,
                                            "owner": root_path,
                                            "container": wsat_container_path,
                                            "parametersJson": json.dumps({"folder": "generated-txt/inner-2/"}),
                                            "description": "Generated measured data with a fault in the inner race"
                                            })
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=rw")
scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

res_path = scsCreateObjectPath("resource", {"name": "Generated Data Set 2 - Outer", 
                                            "name_key": "generated_outer_2",
                                            "resource_type": rt_wsat_data_txt_path,
                                            "owner": root_path,
                                            "container": wsat_container_path,
                                            "parametersJson": json.dumps({"folder": "generated-txt/outer-2/"}),
                                            "description": "Generated measured data with a fault in the outer race"
                                            })
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=rw")
scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

print("Creating the data set folder resources")
res_path = scsCreateObjectPath("resource", {"name": "Data Set 1 - Normal", 
                                            "name_key": "data_set_1",
                                            "resource_type": rt_wsat_data_folder_path,
                                            "owner": root_path,
                                            "container": wsat_container_path,
                                            "parametersJson": json.dumps({"folder": "data-set-1-normal/"}),
                                            "description": "Data set of files with a fault in the inner race"
                                            })
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=rw")
scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

res_path = scsCreateObjectPath("resource", {"name": "Data Set 2 - Inner Fault", 
                                            "name_key": "data_set_2",
                                            "resource_type": rt_wsat_data_folder_path,
                                            "owner": root_path,
                                            "container": wsat_container_path,
                                            "parametersJson": json.dumps({"folder": "data-set-2-inner-fault/"}),
                                            "description": "Data set of files with a fault in the inner race"
                                            })
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=rw")
scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

res_path = scsCreateObjectPath("resource", {"name": "Data Set 3 - Outer Fault", 
                                            "name_key": "data_set_3",
                                            "resource_type": rt_wsat_data_folder_path,
                                            "owner": root_path,
                                            "container": wsat_container_path,
                                            "parametersJson": json.dumps({"folder": "data-set-3-outer-fault/"}),
                                            "description": "Data set of files with a fault in the outer race"
                                            })
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=rw")
scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

res_path = scsCreateObjectPath("resource", {"name": "WSAT Local Data", 
                                            "name_key": "wsat_local_data",
                                            "resource_type": rt_cloud_files_data_path,
                                            "owner": root_path,
                                            "container": carleton_wsat_data,
                                            "parametersJson": json.dumps({"folder": "WSAT_Working_Data/"}),
                                            "description": "This is the Carleton-based SPPLASH Windows storage resource"
                                            })
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=rw")
scsCreateObjectPath("resource_tag", {"tag": bearing_community_tag_path, "resource": res_path})

print("Task type 'win_wsat' objects setup.")
