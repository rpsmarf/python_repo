'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
from django.contrib import admin
from sm_controller.models import SmModelResource, SmModelTask, SmModelTaskResource,\
    SmModelResourceNote, SmModelCommunity, SmModelNewsItems, SmModelUserSetting,\
    SmModelTaskTypeNote, SmModelFavouriteTaskType, SmModelTaskTypeTag,\
    SmModelReservation, SmModelFaq, SmModelProperty, SmModelScript,\
    SmModelMdRepo, SmModelSchema, SmModelMdExtractor, SmModelMdExtractorType, SmModelPropertyType,\
    SmModelPathList, SmModelFileSetItem, SmModelFileSet, SmModelRequest
from sm_controller.models import SmModelResourceTypeTaskType, SmModelAgent, SmModelContainer
from sm_controller.models import SmModelResourceTag, SmModelFavouriteResource
from sm_controller.models import SmModelResourceType, SmModelTaskType, SmModelTag
from guardian.admin import GuardedModelAdmin

admin.site.register(SmModelResource, GuardedModelAdmin)
admin.site.register(SmModelTask, GuardedModelAdmin)
admin.site.register(SmModelTaskResource, GuardedModelAdmin)
admin.site.register(SmModelResourceTypeTaskType, GuardedModelAdmin)
admin.site.register(SmModelAgent, GuardedModelAdmin)
admin.site.register(SmModelContainer, GuardedModelAdmin)
admin.site.register(SmModelResourceTag, GuardedModelAdmin)
admin.site.register(SmModelTaskTypeTag, GuardedModelAdmin)
admin.site.register(SmModelFavouriteResource, GuardedModelAdmin)
admin.site.register(SmModelFavouriteTaskType, GuardedModelAdmin)
admin.site.register(SmModelResourceType, GuardedModelAdmin)
admin.site.register(SmModelTaskType, GuardedModelAdmin)
admin.site.register(SmModelTaskTypeNote, GuardedModelAdmin)
admin.site.register(SmModelTag, GuardedModelAdmin)
admin.site.register(SmModelResourceNote, GuardedModelAdmin)
admin.site.register(SmModelCommunity, GuardedModelAdmin)
admin.site.register(SmModelNewsItems, GuardedModelAdmin)
admin.site.register(SmModelUserSetting, GuardedModelAdmin)
admin.site.register(SmModelScript, GuardedModelAdmin)
admin.site.register(SmModelProperty, GuardedModelAdmin)
admin.site.register(SmModelReservation, GuardedModelAdmin)
admin.site.register(SmModelFaq, GuardedModelAdmin)
admin.site.register(SmModelMdRepo, GuardedModelAdmin)
admin.site.register(SmModelSchema, GuardedModelAdmin)
admin.site.register(SmModelMdExtractor, GuardedModelAdmin)
admin.site.register(SmModelMdExtractorType, GuardedModelAdmin)
admin.site.register(SmModelPropertyType, GuardedModelAdmin)
admin.site.register(SmModelPathList, GuardedModelAdmin)  
admin.site.register(SmModelFileSet, GuardedModelAdmin)  
admin.site.register(SmModelFileSetItem, GuardedModelAdmin)  
admin.site.register(SmModelRequest, GuardedModelAdmin)  
