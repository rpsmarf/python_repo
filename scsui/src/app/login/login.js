/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'rpsmarf.login', [
  'rpsmarf.config',
  'rpsmarf.user.service',
  'ui.router',
  'ngResource',
  'rpsmarf.home',
  'rpsmarf.taskcreate',
  'rpsmarf.setup',
  'rpsmarf.helpers.service'
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider ) {
  $stateProvider.state( 'login', {
    url: '/',
    views: {
      "main": {
        controller: 'LoginCtrl',
        templateUrl: 'login/login.tpl.html',
        resolve: {"logout": function(){return false;}}
      }
    },
    data:{ pageTitle: 'Login' }
  }).state('logout', {
    url: '/',
    views: {
      "main": {
        controller: 'LoginCtrl',
        templateUrl: 'login/login.tpl.html',
        resolve: {"logout": function(){return true;}}
      }
    },
    data:{ pageTitle: 'Logout' }
  });

})

/**
 * And of course we define a controller for our route.
 */
.controller( 'LoginCtrl', ['$scope','$http', 'logout', 'SMARF_URL', '$state', 'SmUser', 'USER_STATE','SmHelpers',
  function ( $scope, $http, logout, SMARF_URL, $state, SmUser, USER_STATE, SmHelpers ) {
  
  $scope.logoutProcess = function(){
    var promise = $http.get(SMARF_URL+'/scs/logout/');
      promise.then(
        function success(response){
          SmUser.invalidate();
          $state.go('logout');
        }, 
        function error(){
          console.log('Error logging out');
          $state.go('logout');
      });
  };
  $scope.resetForm = function(){
    $scope.emailError = [];
    $scope.errorMessage = '';
    $scope.passwordError = [];
    $scope.authenticationError = false;
    $scope.logout = false;
  };

  $scope.error = function(){
    return $scope.emailError.length > 0 || $scope.passwordError.length > 0 || $scope.authenticationError;
  };

  $scope.loginProcess = function(){
    $scope.resetForm();
    var emailPatt = /[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+/;
    if($scope.email === '' || !emailPatt.test($scope.email)){
      $scope.emailError= ['has-error'];
      $scope.errorMessage = 'Please insert a proper email.';
    }
    else if($scope.password === '')
    {

      $scope.passwordError = ['has-error'];
      $scope.errorMessage = 'Please insert a password.';
    }
    else
    {
      var promise = $http.post(SMARF_URL+'/scs/authentication/',"email="+$scope.email+"&password="+$scope.password, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}});
      promise.then(
        function success(response){
          var userData = response.data;
          SmHelpers.completeUserLogin(userData);

          // SmUser.setProperty('id', userData.userid);
          // SmUser.setProperty('email', userData.email);
          // SmUser.setProperty('firstname', userData.firstname);
          // SmUser.setProperty('lastname', userData.lastname);
          // SmUser.setProperty('community', userData.community);
          // SmUser.setProperty('apikey', userData.apikey);
          // SmUser.setProperty('state', USER_STATE.loggingIn);
          // SmUser.setProperty('csrf', ipCookie('csrftoken'));
          // if(userData.imageUrl){
          //   SmUser.setProperty('imageUrl', userData.imageUrl);
          // }
          // if(userData.uiSettingsJson){
          //   SmUser.setProperty('uiSettings', JSON.parse(userData.uiSettingsJson));
          // }
          
          
          // FORUM_URL.value = userData.forumUrl; 

          $scope.authenticationError = false;

          if(userData.performSetupOnNextLogin){
            $state.go('setup.page');
          }
          else{
            $state.go('home');  
          }
          
        }, 
        function error(){
          $scope.errorMessage = 'Incorrect username/password.'; 
          $scope.authenticationError = true;  
      });
    }
  };

  $scope.logout = logout;
  if(logout){
    $scope.logoutProcess();
  }
  $scope.email = '';
  $scope.password = '';
  $scope.emailError = [];
  $scope.passwordError = [];
  $scope.errorMessage = '';
  $scope.authenticationError = false;
    
}])

;

