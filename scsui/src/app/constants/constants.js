angular.module('rpsmarf.constants',[

])
.constant('DEFAULT_TASK_TYPE_ICON_URL', 'assets/icon-default-tool.png')
.constant('DEFAULT_COMMUNITY_ICON_URL', 'https://www.rpsmarf.ca/icons-scs/defaultCommunityIcon.png')
.constant('DEFAULT_RESOURCE_ICON_URL', 'assets/icon-default-resource.png')
.constant('DEFAULT_METADATA_ICON_URL', 'assets/icon-default-metadata.png')
.constant('DEFAULT_USER_ICON_URL', 'assets/icon-default-user.png')
.constant('ROOT_FOLDER_ENTRY', {basename:'/root', isDir:true, mode: 'rwx', mtime:-1, name: '',size: 0, tooltip: 'This represents the root folder of this resource. Select this folder to perform actions directly to this folder.', isRoot: true})
.constant('ISSUE_COLLECTOR_ID', 'issuecollector')
;