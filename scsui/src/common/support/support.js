angular.module('rpsmarf.support', [
	'restangular'

])
.config(['$stateProvider', function($stateProvider){
	$stateProvider.state('support', {
		url: '/support/',
		views: {
			'main': {
				templateUrl: 'support/support.tpl.html'
			}
		},
		data:{ pageTitle: 'Support' }
	})
	.state('support.videos', {
		url: 'videos/',
		views: {
			"main@": {
				templateUrl: 'support/videos.tpl.html'
			}
		},
		data:{ pageTitle: 'Videos' }
	})
	.state('support.documents', {
		url: '^/documents',
		views: {
			"main@": {
				templateUrl: 'support/documents.tpl.html'
			}
		},
		data:{ pageTitle: 'Documents' }
	})
	.state('support.askquestion', {
		url: '^/askquestion',
		views: {
			"main@": {
				templateUrl: 'support/askquestion.tpl.html'
			}
		},
		data:{ pageTitle: 'Ask a Question' }
	})
	.state('support.contactus', {
		url: 'contact/',
		views: {
			"main@": {
				controller: 'ContactUsCtrl',
				templateUrl: 'support/contactus.tpl.html'
			}
		},
		data:{ pageTitle: 'Contact Us' }
	})
	.state('support.license', {
		url: '^/license',
		views: {
			"main@": {
				templateUrl: 'support/license.tpl.html'
			}
		},
		data:{ pageTitle: 'License' }
	})
	.state('support.help', {
		url: '^/help',
		views: {
			"main@": {
				templateUrl: 'support/help.tpl.html'
			}
		},
		data:{ pageTitle: 'Help' }
	});

}])

.controller('ContactUsCtrl',['$scope', 'Restangular', function($scope, Restangular){

	/***SCOPE VARIABLES***/
	$scope.name='';
	$scope.email='';
	$scope.aff='';
	$scope.phone='';
	$scope.comm='';

	/***SCOPE FUNCTIONS***/
	$scope.submit = function(){
		console.log('form submitted!');
	};


}]);
