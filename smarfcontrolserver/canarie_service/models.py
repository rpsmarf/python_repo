'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.


class SmModelCanarieServiceProperties(models.Model):
    key = models.CharField(max_length=255, primary_key=True, blank=False)
    key.help_text = "This is the name of the field.  See the documentation for the use of each field"
    value = models.CharField(max_length=10240)
    value.help_text = "This is the value of the field.  See the documentation for the use of each field"

    class Meta:
        db_table = "scs_cs_prop"
   
    def save(self, force_insert=False, force_update=False, using=None, 
             update_fields=None):
        if self.key == "":
            raise ValidationError(u'Field "key" may not be blank')
        return models.Model.save(self, 
                                 force_insert=force_insert, 
                                 force_update=force_update, 
                                 using=using, 
                                 update_fields=update_fields)
        
