'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
#from django.shortcuts import render

# Create your views here.
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import HttpResponseRedirect, HttpResponseNotAllowed,\
    StreamingHttpResponse, HttpResponseBadRequest
import json 
from smarfcontrolserver.utils.sm_rest_param import SmParamInfo2
import uuid
from sm_controller.models import SmModelResource, SmModelUserSetting, SmModelMdRepo,\
    SmModelPropertyType, SmModelPathList, SmModelMdProperty
from tastypie.models import ApiKey
import logging
from smarfcontrolserver.init.envvar import scsGetEnvVar,\
    SCS_ENV_VAR_FORUM_URL, scsApiGetAuthOn
from sm_controller.api import SM_RESOURCE_UPLOAD_PARAM_DICT, SmApiResource,\
    HttpOk, SM_GLOBAL_GETUSAGE_PARAM_DICT, SM_GLOBAL_BULK_UPLOAD_PARAM_DICT
from actstream.signals import action
import csv
from actstream.models import Action
from tastypie.exceptions import ImmediateHttpResponse
from smcommon.utils.date_utils import sm_date_now, smMakeDateFromString
from datetime import timedelta
from smcommon.utils import date_utils
from smarfcontrolserver.init.metric_names import SCS_METRIC_NUM_USERS_LOGGED_IN,\
    SCS_METRIC_NUM_UPLOADS, SCS_METRIC_NUM_USERS_LOGGED_OUT
from statsd.defaults.env import statsd
import time
from sm_controller.disqus.token_generator import get_disqus_sso

logger = logging.getLogger(__name__)


def index(request):
    return HttpResponseRedirect('/scs/index.html')


def makeResponseContent(request, user):
    contentDict = {'userid': user.id, 
                   'email': user.email, 
                   'firstname': user.first_name, 
                   'lastname': user.last_name}
    user_settings = SmModelUserSetting.objects.filter(owner=user)
    user_apikey = ApiKey.objects.filter(user_id=user.id)
    if len(user_apikey) > 0:
        key = user_apikey[0].key
    else:
        key = None
    contentDict["apikey"] = key
    if len(user_settings) > 0:
        community = list(user_settings)[0].community
        contentDict["community"] = {"id": community.id, 
                                    "name": community.name, 
                                    "name_key": community.name_key, 
                                    "description": community.description,
                                    "owner": "/scs/user/" + str(community.owner.id) + "/",
                                    "imageUrl": community.imageUrl}
        contentDict["performSetupOnNextLogin"] = user_settings[0].performSetupOnNextLogin
        contentDict["uiSettingsJson"] = user_settings[0].uiSettingsJson
    baseForumUrl = scsGetEnvVar(SCS_ENV_VAR_FORUM_URL)
    if (baseForumUrl != ""):
        forumUrl = baseForumUrl + "?smarf=" + request.session.session_key
        contentDict["forumUrl"] = forumUrl
    contentDict["disqusInfo"] = get_disqus_sso(user.email)
    return contentDict


@csrf_exempt
@require_http_methods(['POST'])
def sm_login(request):
    email = request.POST.get('email')
    password = request.POST.get('password')
    try:
        user = User.objects.get(email=email)
    except ObjectDoesNotExist:
        user = None
    if user is not None:
        #perform real authentication here
        user = authenticate(username=user.username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                request.session['user_id'] = user.id                
                contentDict = makeResponseContent(request, user)
                
                action.send(user, verb="login")
                statsd.incr(SCS_METRIC_NUM_USERS_LOGGED_IN)
                response = HttpResponse(status=200, content_type='application/json', 
                                        content=json.dumps(contentDict))
                return response
            else:
                response = HttpResponse(status=401)
                return response
        else:
            response = HttpResponse(status=401)
            return response
    else:
        response = HttpResponse(status=401)
        return response
 

@require_http_methods(['GET'])
def sm_test_session(request):
    if 'user_id' not in request.session.keys():
        uid = None
    else:
        uid = request.session['user_id']
    if uid is not None:
        user = User.objects.get(id=uid)
        contentDict = makeResponseContent(request, user)
        response = HttpResponse(status=200, content_type='application/json', 
            content=json.dumps(contentDict))
    else:
        response = HttpResponse(status=200, content_type='application/json', 
            content=json.dumps({'valid': False}))

    return response


@require_http_methods(['GET'])  
def sm_logout(request):
    try:
        action.send(request.user, verb="logout")
    except:
        pass
    logout(request)
    statsd.incr(SCS_METRIC_NUM_USERS_LOGGED_OUT)
    request.session['user_id'] = None  
    response = HttpResponse(status=200)
    return response


def handle_uploaded_file(f, destFile):
    for chunk in f.chunks():
        destFile.write(chunk)


class Http422(HttpResponse):
        
    def __init__(self, s, *args, **kwargs):
        self.status_code = 422
        super(Http422, self).__init__(s)


@csrf_exempt 
@require_http_methods(['POST'])
def sm_upload(request): 
    logger.debug("In upload pathtype IS %s...", type(request.path)) 
    try:
        logger.debug("got post with path %s...", request.path)
    except:
        logger.warning("Error in logging", exc_info=True) 
    # Handle file upload
    if request.method == 'POST':
        if not request.path.startswith("/scs/resource/"):
            logger.debug("Returning 422...")
            return Http422("Requests for upload are only allowed on resource objects")
        logger.debug("In upload, splitting path %s", request.path)
        pk = request.path.split("/", 3)[3]
        logger.warning("pk is %s...", str(pk))
        paramInfo = SmParamInfo2(SM_RESOURCE_UPLOAD_PARAM_DICT, pk, request)
        pathWithUploadAtEnd = paramInfo.getPostObjectIdPath() 
        logger.debug("got path %s...", pathWithUploadAtEnd)
        pathToMoveTo = None
        if paramInfo.getParam("makeTempPath"):
            uploadPath = ".rpsmarf/tmp/" + str(uuid.uuid4())
        elif paramInfo.getParam("overwrite"):
            # If we are overwriting then upload to a temp file then move it to the final file
            uploadPath = ".rpsmarf/tmp/" + str(uuid.uuid4())
            uploadIndex = pathWithUploadAtEnd.rfind("/upload")
            if uploadIndex == -1:
                return Http422("Requests must include a path then /upload")                
            pathToMoveTo = pathWithUploadAtEnd[0:uploadIndex]
        else:
            uploadIndex = pathWithUploadAtEnd.rfind("/upload")
            if uploadIndex == -1:
                return Http422("Requests must include a path then /upload")                
            pathAfterPk = pathWithUploadAtEnd[0:uploadIndex]
            uploadPath = pathAfterPk
                            
        fileAcc = None
        try:
            logger.debug("Uploading to %s (pathWithUploadAtEnd=%s)", uploadPath, pathWithUploadAtEnd)
            resourceObject = SmModelResource.objects.get(id=paramInfo.getObjectId())
            apiResourceObject = SmApiResource()
            if scsApiGetAuthOn():
                apiResourceObject.is_authenticated(request)
            apiResourceObject._meta.authorization.check_write_data_wo_tastypie(resourceObject, request.user)

            if resourceObject.capacityJson != "":
                capDict = json.loads(resourceObject.capacityJson)
                if capDict.get("diskUsedState") == "full":
                    returnString = json.dumps({"result": "failure",
                                               "code": "EDQUOT", 
                                               "description": "Task start rejected because resource named '" +
                                               resourceObject.name + "' is too full to proceed", 
                                               "operation": "upload", 
                                               "apiPathName": uploadPath})
                    logger.warning("Returning 422 on upload due to diskUsedState==full")
                    return Http422(returnString, content_type="application/json")
            fileAcc = SmApiResource.makeFileAcc(paramInfo, resourceObject)
            fileAcc.connectSession()
            mode = "wb"
            if pathToMoveTo is not None: 
                mode += "u"  # Update
            fdest = fileAcc.open(uploadPath, mode) 
            handle_uploaded_file(request.FILES['uploadfile'], fdest)
            fdest.close()
            if pathToMoveTo is not None:
                fileAcc.move(uploadPath, pathToMoveTo)
            if request.user.id is not None: # "AnonymousUser"
                action.send(request.user, verb="upload", target=resourceObject)
            statsd.incr(SCS_METRIC_NUM_UPLOADS)
            return HttpOk(json.dumps({"result": "success",
                                      "path": uploadPath}),
                          content_type="application/json")
        except Exception as e:
            logger.exception("Got exception during upload")
            # Read the file completely to keep clients happy
            for _ in request.FILES['uploadfile'].chunks():
                pass

            if isinstance(e, ImmediateHttpResponse): 
                tastypieResp = e.response
                resp = HttpResponse(tastypieResp.content)
                resp.status_code = tastypieResp.status_code 
                return resp
            
            returnString = json.dumps({"result": "failure",
                                      "code": e.reasonCode, 
                                      "description": e.description, 
                                      "osPath": e.path, 
                                      "operation": "upload", 
                                      "apiPathName": uploadPath})
            return Http422(returnString, content_type="application/json")
        finally:
            if fileAcc:
                fileAcc.disconnectSession()
    else:
        return HttpResponseNotAllowed(['POST'])
    
    
class SmEventWriter(object):
    """An object that implements just the write method of the file-like
    interface.
    """
    def write(self, event):
        """Write the value by returning it, instead of storing in a buffer."""
        return event


def sm_json_event_generator(events):
    yield "["
    firstTime = True
    for event in events:
        if firstTime:
            firstTime = False
        else:
            yield ","
        yield '{"timestamp":"' + date_utils.smMakeDateString(event.timestamp) +\
            '", "user": "' + event.actor.username +\
            '", "verb": "' + event.verb + '"}'
    yield "]"


@require_http_methods(['GET'])  
def sm_getusage(request):
    """A view that streams a large CSV file."""
    paramInfo = SmParamInfo2(SM_GLOBAL_GETUSAGE_PARAM_DICT, "0", request)
    if paramInfo.getParam("end_time") != "":
        end_time = smMakeDateFromString(paramInfo.getParam("end_time"))
    else:
        end_time = sm_date_now()
    duration = paramInfo.getParam("duration")
    if duration != -1:
        start_time = end_time - timedelta(days=duration)
    else:
        start_time = smMakeDateFromString("20150101")
        
    events = Action.objects.filter(timestamp__gte=start_time).filter(timestamp__lte=end_time)
    if paramInfo.getParam("format") == "json":
        response = StreamingHttpResponse(sm_json_event_generator(events),
                                         content_type="application/json")
    else:   
        pseudo_buffer = SmEventWriter()
        writer = csv.writer(pseudo_buffer)
        response = StreamingHttpResponse((writer.writerow([date_utils.smMakeDateString(event.timestamp),
                                                           event.actor.username, 
                                                           event.verb]) for event in events),
                                         content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="events.csv"'
    return response


@csrf_exempt
@require_http_methods(['POST'])  
def sm_bulk_upload(request):
    '''
    Call takes a few parameters:
    repo - integer id of the repo to upload into
    resource - the id of the resource that the paths provided refere to
    properties - a file type field which contains a JSON string of the form:
       {"data": { "d1/f1.txt": { "propName1": "propValue1", "propName2": "propValue2",...]},
                  "d1/f2.txt": { "propName1": "propValue1", "propName2": "propValue2",...]}
       }}
    '''
    paramInfo = SmParamInfo2(SM_GLOBAL_BULK_UPLOAD_PARAM_DICT, "0", request)
    jsonData = json.loads(request.FILES["properties"].read().decode("utf-8"))
    repo = paramInfo.getParam("repo")
    resource = paramInfo.getParam("resource")
    #data = {'resource': paramInfo.getParam("resource"), 'repo': paramInfo.getParam("repo"), 'json_data': jsonData['data']}
    tRepo = SmModelMdRepo.objects.get(id=repo)
    tResource = SmModelResource.objects.get(id=resource)
    schema = tRepo.schema
    propertyTypeIdDict = {}            
    path_ids_which_need_state_change = []
            
    logger.debug("Before loop time is %d", int((time.time() * 1000) % 100000))
    
    paths_dict = {}
    prop_list = []
    paths_to_bulk_create = []
    
    paths = SmModelPathList.objects.filter(repo=tRepo).filter(resource=tResource).\
        filter(relative_path__in=jsonData['data'].keys())
    for path in paths:
        paths_dict[path.relative_path] = path

    logger.debug("Before second loop time is %d", int((time.time() * 1000) % 100000))
    # Add missing items
    missing_rel_paths = []
    for key, value in jsonData['data'].items():
        path = paths_dict.get(key)
        if path is None:
            path = SmModelPathList(relative_path=key, state="loaded", repo=tRepo, resource=tResource)
            paths_to_bulk_create.append(path)
            missing_rel_paths.append(key)
        else:
            path_ids_which_need_state_change.append(path.pk)
            
    # Create missing paths and get the ids
    SmModelPathList.objects.bulk_create(paths_to_bulk_create)
    paths = SmModelPathList.objects.filter(repo=tRepo).filter(resource=tResource).filter(relative_path__in=missing_rel_paths)
    for path in paths:
        paths_dict[path.relative_path] = path
    
    # Now build list of properties to bulk create
    for key, value in jsonData['data'].items():
        path = paths_dict.get(key)        
        for propName, propValue in value.items():    
            # Look up property type object if not in cache
            propertyTypeObj = propertyTypeIdDict.get(propName)
            if propertyTypeObj is None:
                propertyTypes = SmModelPropertyType.objects.filter(name_key=propName).filter(schema=schema)
                if propertyTypes.count() == 0:
                    msg = 'Property Type with name_key "{}" does not exist'.format(propName)
                    return HttpResponseBadRequest(json.dumps({'msg': msg, 'code': 2}), content_type='application/json')
                propertyTypeObj = propertyTypes.first()
                propertyTypeIdDict[propName] = propertyTypeObj
                 
            # Add property record
            prop_list.append(SmModelMdProperty(path=path, property_type=propertyTypeObj, value=propValue))

    logger.debug("After loop, before bulk insert time is %d", int((time.time() * 1000) % 100000))

    SmModelMdProperty.objects.bulk_create(prop_list)
        
    logger.debug("After bulk insert time is  %d", int((time.time() * 1000) % 100000))

    # Change state of the paths updated to "loaded"
    SmModelPathList.objects.filter(pk__in=path_ids_which_need_state_change).update(state="loaded")
    
    logger.debug("After update time is  %d", int((time.time() * 1000) % 100000))

    data = {'msg': 'Data successfully posted', 'code': 0}  
    return HttpResponse(json.dumps(data), content_type='application/json')
