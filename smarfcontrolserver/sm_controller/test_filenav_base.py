'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 10, 2015

    @author: rpsmarf
'''

import unittest
from django.test import TestCase
import json
import os
import logging
import stat
from logging import CRITICAL, NOTSET
from sm_controller.signal_handlers import smTriggerSignalInstall
from zipfile import ZipFile
import tempfile
import time
from smcommon.tests.dad_test_utils import TempFolder

logger = logging.getLogger(__name__) 
smTriggerSignalInstall()


class FileNavigationTestBase(TestCase):
    '''
    This is a base class which contains a number of common tests for file operations. Derived
    classes then setup local, Swift or S3-based configurations and invoke the tests that are
    appropriate.
    
    Ideally these tests could have been called test_xxx instead of basetest_xxx but that confuses Eclipse
    and other tool running tools so we invoke the tests explicitly.
    '''
    
    def basetest_unicodeFilenames(self):
        arabicName = "عربي"
        frenchName = "ùûüÿàâæçé€èêëïîôœ"
        hindiName = "मानक हिन्दी"
        # Set up test folder
        # self.helper.tempFolder.mkdir("r_folder/") - created by resource
        self.helper.tempFolder.writeFile("r_folder/" + arabicName, "a1")
        self.helper.tempFolder.mkdir("r_folder/d1")
        self.helper.tempFolder.writeFile("r_folder/d1/" + frenchName, "a3")
        self.helper.tempFolder.mkdir("r_folder/" + hindiName)
        self.helper.tempFolder.writeFile("r_folder/" + hindiName + "/f4", "a4")
        self.helper.tempFolder.writeFile("r_folder/" + hindiName + "/f5", "12345")

        resp = self.client.get("/scs/resource/" + str(self.resource.pk) + "/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual("resourcexxx", jsoncon["name"])

        # List root or resource
        urlpath = "/scs/resource/" + str(self.resource.pk)
        resp = self.client.get(urlpath + "/" + "list/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode('utf-8'))
        jsoncon = json.loads(resp.content.decode('utf-8'))
        file_dict = self.make_file_dict(jsoncon)
        self.assertEqual(3, len(jsoncon), resp.content.decode('utf-8'))
        self.assertTrue("d1/" in file_dict,
                        "Data is " + json.dumps(jsoncon) + " expected name of d1/")
        self.assertEqual(0, file_dict["d1/"]["size"])
        self.assertTrue(hindiName + "/" in file_dict)
        self.assertTrue(file_dict[hindiName + "/"]["isDir"])
        
        resp = self.client.get(urlpath + "/" + "list/?recursiveDepth=10", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(6, len(jsoncon))
        self.assertTrue(jsoncon[0]["name"] == "d1/",
                        "Name is " + json.dumps(jsoncon[0]) + " expected d1")
        self.assertEqual(0, jsoncon[0]["size"])
        self.assertTrue(jsoncon[0]["isDir"])

        resp = self.client.get(urlpath + "/" + hindiName + "/list/?recursiveDepth=10", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(2, len(jsoncon), resp.content.decode('utf-8'))
        self.assertTrue(jsoncon[0]["name"] == hindiName + "/f4")
        self.assertEqual(2, jsoncon[0]["size"])
        self.assertFalse(jsoncon[0]["isDir"])
        self.assertTrue(jsoncon[1]["name"] == hindiName + "/f5")
        self.assertEqual(5, jsoncon[1]["size"])
        self.assertFalse(jsoncon[1]["isDir"])

        # Make a local filesystem tempFolder (helper tempFolder may be local or cloud-based)
        tempFolder = TempFolder()        
        tempFolder.writeFile("data", "a1")
        with open(tempFolder.mkAbsolutePath("data"), "rb") as fp:
            resp = self.client.post(urlpath + "/123" + hindiName + "/upload/",
                                    {'uploadfile': fp}, **self.helper.auth_header)    
        self.assertEqual(resp.status_code, 200)
        resp = self.client.get(urlpath + "/list/?recursiveDepth=1", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(4, len(jsoncon))
        self.assertTrue(jsoncon[0]["name"] == "123" + hindiName)
        self.assertEqual(2, jsoncon[0]["size"])
        self.assertFalse(jsoncon[0]["isDir"])

    def make_file_dict(self, file_list):
        file_dict = {}
        for file in file_list:
            file_dict[file["name"]] = file
        return file_dict
    
    def basetest_listContent(self):
        # Set up test folder
        # self.helper.tempFolder.mkdir("r_folder/") - created by resource
        self.helper.tempFolder.writeFile("r_folder/f1", "a1")
        self.helper.tempFolder.mkdir("r_folder/d1")
        self.helper.tempFolder.writeFile("r_folder/d1/f3", "a3")
        self.helper.tempFolder.mkdir("r_folder/d2")
        self.helper.tempFolder.writeFile("r_folder/d2/f4", "a4")
        self.helper.tempFolder.writeFile("r_folder/d2/f5", "12345")
        self.helper.tempFolder.mkdir("r_folder/.hidden")
        self.helper.tempFolder.writeFile("r_folder/.hidden/f5", "12345")
        self.helper.tempFolder.writeFile("r_folder/d2/.hidden", "12345")

        resp = self.client.get("/scs/resource/" + str(self.resource.pk) + "/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual("resourcexxx", jsoncon["name"])

        # List root or resource
        urlpath = "/scs/resource/" + str(self.resource.pk)
        resp = self.client.get(urlpath + "/" + "list/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode('utf-8'))
        jsoncon = json.loads(resp.content.decode('utf-8'))
        file_dict = self.make_file_dict(jsoncon)
        self.assertEqual(3, len(jsoncon), resp.content.decode('utf-8'))
        self.assertTrue(file_dict["d1/"]["name"].endswith("d1/"),
                        "Info is " + json.dumps(jsoncon[1]) + " expected name=d1/")
        
        self.assertEqual(0, file_dict["d1/"]["size"])
        self.assertTrue(file_dict["d1/"]["isDir"])
        
        resp = self.client.get(urlpath + "/" + "list/?recursiveDepth=10", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(6, len(jsoncon))
        self.assertTrue(jsoncon[0]["name"] == "d1/",
                        "Name is " + json.dumps(jsoncon[0]) + " expected d1")
        self.assertEqual(0, jsoncon[0]["size"])
        self.assertTrue(jsoncon[0]["isDir"])

        resp = self.client.get(urlpath + "/d2/" + "list/?recursiveDepth=10", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(2, len(jsoncon))
        self.assertTrue(jsoncon[0]["name"] == "d2/f4")
        self.assertEqual(2, jsoncon[0]["size"])
        self.assertFalse(jsoncon[0]["isDir"])
        self.assertTrue(jsoncon[1]["name"] == "d2/f5")
        self.assertEqual(5, jsoncon[1]["size"])
        self.assertFalse(jsoncon[1]["isDir"])

        resp = self.client.get(urlpath + "/d2/" + "list/?recursiveDepth=10&showHidden=True", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(3, len(jsoncon))
        self.assertTrue(jsoncon[0]["name"] == "d2/.hidden")
        self.assertEqual(5, jsoncon[0]["size"])
        self.assertFalse(jsoncon[0]["isDir"])
        self.assertTrue(jsoncon[1]["name"] == "d2/f4")
        self.assertEqual(2, jsoncon[1]["size"])
        self.assertFalse(jsoncon[1]["isDir"])
        self.assertTrue(jsoncon[2]["name"] == "d2/f5")
        self.assertEqual(5, jsoncon[2]["size"])
        self.assertFalse(jsoncon[2]["isDir"])
        
    def basetest_metadataContent(self):
        # Set up test folder
        # self.helper.tempFolder.mkdir("r_folder/") - created by resource
        self.helper.tempFolder.writeFile("r_folder/f1", "a1")
        self.helper.tempFolder.mkdir("r_folder/d1")
        self.helper.tempFolder.writeFile("r_folder/d1/f3", "a3")
        self.helper.tempFolder.mkdir("r_folder/d2")
        self.helper.tempFolder.writeFile("r_folder/d2/f4", "a4")
        self.helper.tempFolder.writeFile("r_folder/d2/f5", "12345")

        resp = self.client.get("/scs/resource/" + str(self.resource.pk) + "/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual("resourcexxx", jsoncon["name"])

        # List root or resource
        urlpath = "/scs/resource/" + str(self.resource.pk)
        resp = self.client.get(urlpath + "/" + "metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode('utf-8'))
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(1, len(jsoncon))
        self.assertEqual(jsoncon[0]["name"], "/")
        self.assertEqual(0, jsoncon[0]["size"])
        self.assertTrue(jsoncon[0]["isDir"])
        
        resp = self.client.get(urlpath + "/d1/" + "metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(1, len(jsoncon))
        self.assertEqual(jsoncon[0]["name"], "d1/")
        self.assertEqual(0, jsoncon[0]["size"])
        self.assertTrue(jsoncon[0]["isDir"])

        resp = self.client.get(urlpath + "/d2/" + "metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(1, len(jsoncon))
        self.assertEqual(jsoncon[0]["name"], "d2/")

        resp = self.client.get(urlpath + "/d2/f5/" + "metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(1, len(jsoncon))
        self.assertTrue(jsoncon[0]["name"] == "d2/f5")
        self.assertEqual(5, jsoncon[0]["size"])
        self.assertFalse(jsoncon[0]["isDir"])

    def basetest_moveRename(self):
        # Set up test folder - should fail because created by the resource        
        urlpath = "/scs/resource/" + str(self.resource.pk)
        
        self.helper.tempFolder.writeFile("r_folder/f1", "a1")
        self.helper.tempFolder.mkdir("r_folder/d1")
        self.helper.tempFolder.writeFile("r_folder/d1/f3", "a3")
        self.helper.tempFolder.mkdir("r_folder/d2")
        self.helper.tempFolder.writeFile("r_folder/d2/f4", "a4")
        self.helper.tempFolder.writeFile("r_folder/d2/f5", "12345")
        self.helper.tempFolder.mkdir("r_folder/d3")

        # Try rename without newName        
        resp = self.client.get(urlpath + "/f1/rename/?newName2=f2", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 400, resp.content.decode('utf-8'))
        resp = self.client.get(urlpath + "/f1/rename/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 400, resp.content.decode('utf-8'))
        
        # Try move without newPath
        resp = self.client.get(urlpath + "/f1/move/?newPath2=f2", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 400, resp.content.decode('utf-8'))
        resp = self.client.get(urlpath + "/f1/move/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 400, resp.content.decode('utf-8'))
        
        # Rename file in folder
        resp = self.client.get(urlpath + "/f1/rename/?newName=f2", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode('utf-8'))
        # Check old file gone
        resp = self.client.get(urlpath + "/f1/metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 422)
        # Check new file present
        resp = self.client.get(urlpath + "/f2/metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(1, len(jsoncon))
        self.assertEqual(jsoncon[0]["name"], "f2")
        self.assertEqual(2, jsoncon[0]["size"])
        self.assertFalse(jsoncon[0]["isDir"])

        # Rename folder in top level folder
        urlpath = "/scs/resource/" + str(self.resource.pk)
        resp = self.client.get(urlpath + "/d2/rename/?newName=folder2", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode('utf-8'))
        # Check old file gone
        resp = self.client.get(urlpath + "/d2/metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 422)
        # Check new file present
        resp = self.client.get(urlpath + "/folder2/metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(1, len(jsoncon))
        self.assertEqual(jsoncon[0]["name"], "folder2/")
        self.assertEqual(0, jsoncon[0]["size"])
        self.assertTrue(jsoncon[0]["isDir"])

        # Move file in folder
        urlpath = "/scs/resource/" + str(self.resource.pk)
        resp = self.client.get(urlpath + "/folder2/f5/move/?newPath=f5.2", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode('utf-8'))
        # Check old file gone
        resp = self.client.get(urlpath + "/folder2/f5/metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 422)
        # Check new file present
        resp = self.client.get(urlpath + "/f5.2/metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(1, len(jsoncon))
        self.assertEqual(jsoncon[0]["name"], "f5.2")
        self.assertEqual(5, jsoncon[0]["size"])
        self.assertFalse(jsoncon[0]["isDir"])

        # Move folder in top level folder
        urlpath = "/scs/resource/" + str(self.resource.pk)
        resp = self.client.get(urlpath + "/d3/move/?newPath=folder2/d3", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode('utf-8'))
        # Check old file gone
        resp = self.client.get(urlpath + "/d3/metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 422)
        # Check new file present
        resp = self.client.get(urlpath + "/folder2/d3/metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(1, len(jsoncon))
        self.assertEqual(jsoncon[0]["name"], "folder2/d3/")
        self.assertEqual(0, jsoncon[0]["size"])
        self.assertTrue(jsoncon[0]["isDir"])

    def basetest_deleteContent(self):
        # Set up test folder
        # self.helper.tempFolder.mkdir("r_folder/") - Created by resource
        self.helper.tempFolder.writeFile("r_folder/f1", "a1")
        self.helper.tempFolder.mkdir("r_folder/d1/")
        self.helper.tempFolder.writeFile("r_folder/d1/f3", "a3")
        self.helper.tempFolder.mkdir("r_folder/d2/")
        self.helper.tempFolder.writeFile("r_folder/d2/f4", "a4")
        self.helper.tempFolder.writeFile("r_folder/d2/f5", "12345")

        resp = self.client.get("/scs/resource/" + str(self.resource.pk) + "/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual("resourcexxx", jsoncon["name"])

        # Delete file without trailing /file or /folder
        urlpath = "/scs/resource/" + str(self.resource.pk)
        resp = self.client.delete(urlpath + "/d2/f5" + "xxx/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 400, resp.content.decode('utf-8'))

        # Delete file as folder
        urlpath = "/scs/resource/" + str(self.resource.pk)
        resp = self.client.delete(urlpath + "/d2/f5" + "folder/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 400, resp.content.decode('utf-8'))

        # Delete folder as file
        urlpath = "/scs/resource/" + str(self.resource.pk)
        resp = self.client.delete(urlpath + "/d2/" + "file/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 422, resp.content.decode('utf-8'))

        # Proper file delete
        urlpath = "/scs/resource/" + str(self.resource.pk)
        resp = self.client.delete(urlpath + "/d2/f5" + "/file/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 204, resp.content.decode('utf-8'))

        # Check file gone
        resp = self.client.get(urlpath + "/d2/f5/" + "metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 422)
        
        # Proper folder delete, but not empty
        if not self.cloudTest:
            urlpath = "/scs/resource/" + str(self.resource.pk)
            resp = self.client.delete(urlpath + "/d2" + "/folder/", **self.helper.auth_header)
            self.assertEqual(resp.status_code, 422, resp.content.decode('utf-8'))

        # Make folder empty, then delete folder
        urlpath = "/scs/resource/" + str(self.resource.pk)
        resp = self.client.delete(urlpath + "/d2/f4" + "/file/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 204, resp.content.decode('utf-8'))
        urlpath = "/scs/resource/" + str(self.resource.pk)
        resp = self.client.delete(urlpath + "/d2" + "/folder/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 204, resp.content.decode('utf-8'))
        
        # Check folder gone
        resp = self.client.get(urlpath + "/d2/" + "metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 422)

    def basetest_getFolderSize(self):
        # Set up test folder
        # self.helper.tempFolder.mkdir("r_folder/") - Created by resource
        self.helper.tempFolder.writeFile("r_folder/f1", "a1")
        self.helper.tempFolder.mkdir("r_folder/d1")
        self.helper.tempFolder.writeFile("r_folder/d1/f3", "a3")
        self.helper.tempFolder.mkdir("r_folder/d2")
        self.helper.tempFolder.writeFile("r_folder/d2/f4", "a4")
        self.helper.tempFolder.writeFile("r_folder/d2/f5", "12345")

        res_path = "/scs/resource/" + str(self.resource.pk) + "/"
        resp = self.helper.objGetViaRest(res_path + "getfoldersize/")
        self.assertEqual(11, resp["sizeInBytes"])
        self.assertEqual(6, resp["fileCount"])

        res_path = "/scs/resource/" + str(self.resource.pk) + "/"
        resp = self.helper.objGetViaRest(res_path + "d2/getfoldersize/")
        self.assertEqual(7, resp["sizeInBytes"])
        self.assertEqual(2, resp["fileCount"])

    def basetest_download(self):
        # Set up test folder
        # self.helper.tempFolder.mkdir("r_folder/") - created by resource
        self.helper.tempFolder.writeFile("r_folder/a1", "data_a1")
        self.helper.tempFolder.mkdir("r_folder/d1/")
        self.helper.tempFolder.writeFile("r_folder/d1/f3", "data_f3")
        self.helper.tempFolder.writeFile("r_folder/d1/f4.txt", "data_f4")
        self.helper.tempFolder.writeFile("r_folder/d1/f5.mp3", "data_f5")
        self.helper.tempFolder.mkdir("r_folder/d1/d2/")
        self.helper.tempFolder.writeFile("r_folder/d1/d2/f6.txt", "data_f6")

        # Try invalid path
        logging.disable(CRITICAL)  # Hide error
        resp = self.client.get("/scs/resource/" + str(self.resource.pk) + "/garbage/download/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 422, resp.content.decode("utf-8"))
        logging.disable(NOTSET)
        
        urlpath = "/scs/resource/" + str(self.resource.pk)
        resp = self.client.get(urlpath + "/a1" + "/download/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        for s in resp.streaming_content:
            logger.debug("Got string '%s'", s)
        self.assertEqual("data_a1", s.decode('utf-8'))
        
        resp = self.client.get(urlpath + "/d1/f3" + "/download/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        for s in resp.streaming_content:
            logger.debug("Got string '%s'", s)
        self.assertEqual("data_f3", s.decode('utf-8'))
        self.assertTrue(resp._headers["content-type"][1].startswith("text/html"))
        
        resp = self.client.get(urlpath + "/d1/f4.txt" + "/download/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        for s in resp.streaming_content:
            logger.debug("Got string '%s'", s)
        self.assertEqual("data_f4", s.decode('utf-8'))
        self.assertEqual(resp._headers["content-type"][1], "text/plain")
        
        resp = self.client.get(urlpath + "/d1/f5.mp3" + "/download/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        for s in resp.streaming_content:
            logger.debug("Got string '%s'", s)
        self.assertEqual("data_f5", s.decode('utf-8'))
        self.assertEqual(resp._headers["content-type"][1], "audio/mpeg")
        
        # Attempt folder download
        #=======================================================================
        # zip_file_path = self.helper.tempFolder.makeAbsolutePath("zip.zip", "local")
        #=======================================================================
        zip_file_path = self.helper.tempFolder.makeAbsolutePath("zip.zip", "local")
        
        resp = self.client.get(urlpath + "/d1/download/", **self.helper.auth_header)
        self.assertEqual(200, resp.status_code)
        logger.debug("Writing to %s", zip_file_path)
        with open(zip_file_path, "wb") as f:
            for s in resp.streaming_content:
                logger.debug("Wrote %d bytes", len(s))
                f.write(s)

        # Check folder contents
        zfile = ZipFile(zip_file_path)
        # Make my own load temp folder - for the cloud case where the helper tempFolder is in the cloud 
        tempFolder = TempFolder()
        unzip_dir = tempFolder.makeAbsolutePath("unzip_dir", "local")
        zfile.extractall(path=unzip_dir)
        zfile.close()
        self.assertEqual(tempFolder.readFile(os.path.join(unzip_dir, "d1/f3"), 10000), b"data_f3")
        self.assertEqual(tempFolder.readFile(os.path.join(unzip_dir, "d1/d2/f6.txt"), 10000), b"data_f6")

    def basetest_errors(self):
        self.helper.tempFolder.writeFile("f1", "data_a1")
        # Make temp temppath
        urlpath = "/scs/resource/" + str(self.resource.pk) + "/"
        
        try:
            # Create folder "readonly" with file f1
            self.helper.tempFolder.mkdir("r_folder/readonly")
            self.helper.tempFolder.writeFile("r_folder/readonly/f1", "123")
            os.chmod(self.helper.tempFolder.mkAbsolutePath("r_folder/readonly"),
                     stat.S_IREAD | stat.S_IEXEC)
            
            # Create folder "secret" which is not readable
            self.helper.tempFolder.mkdir("r_folder/secret")
            os.chmod(self.helper.tempFolder.mkAbsolutePath("r_folder/secret"), 0)
    
            # Create folder     
            self.helper.tempFolder.mkdir("r_folder/normal")
            self.helper.tempFolder.writeFile("r_folder/normal/f1", "123")
    
            # List something that doesn't exist, expect ENOENT
            resp = self.client.get(urlpath + "normal/xxx" + "/list/", **self.helper.auth_header)
            self.assertEqual(resp.status_code, 422, resp.content.decode('utf-8'))
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("normal/xxx", respDict["apiPathName"])
            self.assertEqual("ENOENT", respDict["code"])
            self.assertEqual("list", respDict["operation"])
    
            # List something w/o read permission, expect EPERM
            resp = self.client.get(urlpath + "secret" + "/list/", **self.helper.auth_header)
            self.assertEqual(resp.status_code, 422, resp.content.decode('utf-8'))
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("secret", respDict["apiPathName"])
            self.assertEqual("EACCES", respDict["code"])
            self.assertEqual("list", respDict["operation"])
            
            # Delete something that doesn't exist, expect ENOENT
            resp = self.client.delete(urlpath + "normal/f99" + "/file/", **self.helper.auth_header)
            self.assertEqual(resp.status_code, 422, resp.content.decode('utf-8'))
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("normal/f99", respDict["apiPathName"])
            self.assertEqual("ENOENT", respDict["code"])
            self.assertEqual("delete", respDict["operation"])
            
            # Delete something w/o folder modify permission, expect EACCES
            resp = self.client.delete(urlpath + "readonly/f1" + "/file/", **self.helper.auth_header)
            self.assertEqual(resp.status_code, 422, resp.content.decode('utf-8'))
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("readonly/f1", respDict["apiPathName"])
            self.assertEqual("EACCES", respDict["code"])
            self.assertEqual("delete", respDict["operation"])
            
            # Delete dir which is not a dir, expect ENOTDIR
            resp = self.client.delete(urlpath + "normal/f1" + "/folder/", **self.helper.auth_header)
            self.assertEqual(resp.status_code, 422, resp.content.decode('utf-8'))
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("normal/f1/", respDict["apiPathName"])
            self.assertEqual("ENOTDIR", respDict["code"])
            self.assertEqual("delete", respDict["operation"])
            
            # Delete dir which is not empty, expect ENOTEMPTY
            resp = self.client.delete(urlpath + "normal" + "/folder/", **self.helper.auth_header)
            self.assertEqual(resp.status_code, 422, resp.content.decode('utf-8'))
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("normal/", respDict["apiPathName"])
            self.assertEqual("ENOTEMPTY", respDict["code"])
            self.assertEqual("delete", respDict["operation"])
            
            # Download something that doesn't exist, expect ENOENT
            resp = self.client.get(urlpath + "bad" + "/download/", **self.helper.auth_header)
            self.assertEqual(resp.status_code, 422)
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("bad", respDict["apiPathName"])
            self.assertEqual("ENOENT", respDict["code"])
            self.assertEqual("download", respDict["operation"])
    
            # Download something w/o read permission, expect EACCES
            resp = self.client.get(urlpath + "secret/f1" + "/download/", **self.helper.auth_header)
            self.assertEqual(resp.status_code, 422)
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("secret/f1", respDict["apiPathName"])
            self.assertEqual("EACCES", respDict["code"])
            self.assertEqual("download", respDict["operation"])
    
            # Upload to a folder that doesn't exist, expect ENOENT
            with open(self.helper.tempFolder.mkAbsolutePath("f1"), "rb") as fp:
                resp = self.client.post(urlpath + "/bad/a" + "/upload/",
                                        {'uploadfile': fp}, **self.helper.auth_header)    
            self.assertEqual(resp.status_code, 422)
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("/bad/a", respDict["apiPathName"])
            self.assertEqual("ENOENT", respDict["code"])
            self.assertEqual("upload", respDict["operation"])
            
            # Upload to a read-only folder, expect EPERM
            with open(self.helper.tempFolder.mkAbsolutePath("f1"), "rb") as fp:
                resp = self.client.post(urlpath + "readonly/f2" + "/upload/",
                                        {'uploadfile': fp}, **self.helper.auth_header)    
            self.assertEqual(resp.status_code, 422)
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("readonly/f2", respDict["apiPathName"])
            self.assertEqual("EACCES", respDict["code"])
            self.assertEqual("upload", respDict["operation"])
            
            # Rename something w/o read permission, expect EACCES
            resp = self.client.get(urlpath + "secret/f1" + "/rename/?newName=xxx", **self.helper.auth_header)
            self.assertEqual(resp.status_code, 422)
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("secret/f1", respDict["apiPathName"])
            self.assertEqual("EACCES", respDict["code"])
            self.assertEqual("rename", respDict["operation"])
    
            # Move something w/o read permission, expect EACCES
            resp = self.client.get(urlpath + "secret/f1" + "/move/?newPath=xxx", **self.helper.auth_header)
            self.assertEqual(resp.status_code, 422)
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("secret/f1", respDict["apiPathName"])
            self.assertEqual("EACCES", respDict["code"])
            self.assertEqual("move", respDict["operation"])
            
            # Rename something non-existent, expect ENOENT
            resp = self.client.get(urlpath + "bad/f1" + "/rename/?newName=xxx", **self.helper.auth_header)
            self.assertEqual(resp.status_code, 422)
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("bad/f1", respDict["apiPathName"])
            self.assertEqual("ENOENT", respDict["code"])
            self.assertEqual("rename", respDict["operation"])
    
            # Move something w/o read permission, expect EACCES
            resp = self.client.get(urlpath + "bad/f1" + "/move/?newPath=xxx", **self.helper.auth_header)
            self.assertEqual(resp.status_code, 422)
            respDict = json.loads(resp.content.decode('utf-8'))
            self.assertEqual("bad/f1", respDict["apiPathName"])
            self.assertEqual("ENOENT", respDict["code"])
            self.assertEqual("move", respDict["operation"])
        finally:
            # Undo the permissions change so the temp folder can be deleted in the cleanup
            os.chmod(self.helper.tempFolder.mkAbsolutePath("r_folder/secret"),
                     stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
            os.chmod(self.helper.tempFolder.mkAbsolutePath("r_folder/readonly"),
                     stat.S_IREAD | stat.S_IWUSR | stat.S_IEXEC)
            
    def basetest_tempfile(self):
        self.helper.tempFolder.writeFile("f1", "data_a1")

        # Make temp temppath
        urlpath = "/scs/resource/" + str(self.resource.pk) + "/"
        
        try:
            self.client.get(urlpath + "a/mktemppath/", **self.helper.auth_header)
            self.fail("Expected exception")
        except:
            pass
        
        # Make temp temppath
        resp = self.client.get(urlpath + "mktemppath/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        jsoncon = json.loads(resp.content.decode('utf-8'))
        temppath = jsoncon.get("path")
        tempParentPath = jsoncon.get("parentPath")
        tempName = jsoncon.get("name")
        self.assertIsNotNone(temppath)
        self.assertIsNotNone(tempParentPath)
        self.assertIsNotNone(tempName)
        
        # Test upload
        tempname = os.path.join(tempfile.tempdir, "x-" + str(time.time()))
        with open(tempname, "wb") as temp:
            temp.write(b'data_a1')
        
        with open(tempname, "rb") as fp:
            resp = self.client.post(urlpath + temppath + "/upload/",
                                    {'uploadfile': fp}, **self.helper.auth_header)
        os.remove(tempname)    
        self.assertEqual(resp.status_code, 200)
        
        # Test download
        resp = self.client.get(urlpath + temppath + "/download/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        for s in resp.streaming_content:
            logger.debug("Got string '%s'", s)
        self.assertEqual("data_a1", s.decode('utf-8'))
        
        # Test list with file (expect failure)
        resp = self.client.get(urlpath + temppath + "/list/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 403, resp.content.decode('utf-8'))
        
        # Test list of .rpsmarf/tmp (expect failure)
        resp = self.client.get(urlpath + tempParentPath + "/list/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 403, resp.content.decode('utf-8'))

        # Test list of .rpsmarf (expect failure)
        resp = self.client.get(urlpath + "/.rpsmarf" + "/list/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 403, resp.content.decode('utf-8'))

        # Test metadata (expect OK)
        resp = self.client.get(urlpath + temppath + "/metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200, resp.content.decode('utf-8'))
        jsoncon = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(1, len(jsoncon))
        self.assertEqual(jsoncon[0]["name"], temppath)
        self.assertEqual(7, jsoncon[0]["size"])
        self.assertFalse(jsoncon[0]["isDir"])
        
        # Test delete
        resp = self.client.delete(urlpath + temppath + "/file/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 204, resp.content.decode('utf-8'))
        
        # Test metadata (expect fail)
        resp = self.client.get(urlpath + temppath + "/metadata/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 422, resp.content.decode('utf-8'))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()