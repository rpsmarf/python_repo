'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import glob
import os
import fnmatch
import re

pyFiles = []
pyFilesToCheck = []
for root, dirnames, filenames in os.walk('.'):
    for filename in fnmatch.filter(filenames, '*.py'):
        pyFiles.append(os.path.join(root, filename))
        if (not (filename.endswith("_ice.py") or filename.startswith("__"))):
            pyFilesToCheck.append(os.path.join(root, filename))

COVERAGE_REPORT_OMIT_LIST = "x"

SMARF_HOST = "unset"
SMARF_VERSION = "unset"
if ("SMARF_HOST" in os.environ):
    SMARF_HOST = os.environ["SMARF_HOST"]
if ("SMARF_VERSION" in os.environ):
    SMARF_VERSION = os.environ["SMARF_VERSION"]
if ("SMARF_GITDIR" in os.environ):
    SMARF_GITDIR = os.environ["SMARF_GITDIR"]
    
SSH_CMD = ("ssh  -o UserKnownHostsFile=/dev/null " +
"-o StrictHostKeyChecking=no " +
"-i ../tools/keys/smarf_dair_keys.pem ubuntu@" + SMARF_HOST)
FLAKE8_OPTIONS = "--ignore=W292,E291,E261,E262,E265,E292,E128,W293,W291,E123,E126,W391 --max-line-length=128"

DOIT_CONFIG = {'default_tasks': ['show']}


def task_show():
    """show all available tasks"""
    return {'actions': ['doit list'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }


def task_checker():
    """run flake8 on all project files"""
    for module in pyFilesToCheck:
        yield {'actions': ['../tools/flake8_wrapper smarfcommon ' + FLAKE8_OPTIONS + ' %(dependencies)s '
                           ],
               'name': module,
               'file_dep': [module],
               'verbosity': 0,
               }


def task_zip():
    """create ZIP file with SCS Python code.  
    Clean deletes all pycache folders"""
    return {'actions': [
            'rm -f ../build_output/smarf_common.zip',
            'zip -q -r ../build_output/smarf_common.zip . '
            '--exclude \*__pycache__\* '
            '--exclude .\* '
            '--exclude dodo.py '
            '--exclude install/\*'],
            'targets': ['../build_output/smarf_common.zip'],
            'file_dep': pyFiles,
            'task_dep': ['checker', 'msggen'],
            'verbosity': 2,
            'clean': ["rm -f ../build_output/smarf_common.zip",
                      "find . -name __pycache__ -prune -exec rm -r {} \;"
                      ],
            }


def task_test():
    """run unit test for the SMARF smcommon code"""
    return {'actions': ["coverage run --source smcommon -m unittest discover -s smcommon.tests -p '*tests.py'",
                        "coverage report" +
                        "|grep TOTAL|sed -e 's/TOTAL                    /CODE COVERAGE/'",
                        ],
            'file_dep': ['../build_output/smarf_common.zip'],
            'verbosity': 2,
            'clean': ["rm -f /tmp/unittestLogging.conf",
                      ],
            }


def task_coverage():
    """view code coverage for the SMARF remote agent"""
    return {'actions': ["rm -rf /tmp/htmlcoverage",
                        "coverage html  --omit " + COVERAGE_REPORT_OMIT_LIST +
                        " -d /tmp/htmlcoverage",
                        "firefox /tmp/htmlcoverage/index.html",
                        ],
            'task_dep': ['test'],
            'verbosity': 2,
            }


def task_patch():
    """replace the python COMMON files on the host referred to by SMARF_HOST"""
    return {
        'actions': 
        ['scp -q '
         '-o UserKnownHostsFile=/dev/null '
         '-o StrictHostKeyChecking=no '
         '-i ../tools/keys/smarf_dair_keys.pem '
         '../build_output/smarf_common.zip ubuntu@' + SMARF_HOST + ':/tmp',
         'ssh  -o UserKnownHostsFile=/dev/null '
         '-o StrictHostKeyChecking=no '
         '-i ../tools/keys/smarf_dair_keys.pem '
         'ubuntu@' + SMARF_HOST + 
         ' sudo unzip -o /tmp/smarf_common.zip -d /opt/smarf_common/smcommon'
         ],
        'file_dep': ['../build_output/smarf_common.zip'],
        'targets': [],
        'verbosity': 2,
           }

msgdefFiles = glob.glob("icemsgs/*.ice")


def task_msggen():
    """run slice2py on all ICE slice definition files"""
    for module in msgdefFiles:
        yield {'actions': ['slice2py %(dependencies)s',
                           "sed -i " +
                           "-e 's/__name__.*/\\0 # @ReservedAssignment/' " +
                           "-e 's/.*Ice.OperationMode.Normal.*/\\0 # @UndefinedVariable/' " + 
                           re.sub(".*/", "", module.replace(".ice", "_ice.py"))],
               'name': module,
               'file_dep': [module],
               'targets': [module.replace(".ice", "_ice.py").replace("icemsgs/", "")],
               'verbosity': 0,
               'clean': ["rm *_ice.py"],
               }


def task_docgen():
    """Runs slice2html to generate message documentation"""
    for module in msgdefFiles:
        yield {'actions': ['slice2html --output-dir ../build_output/iceMsgDocs %(dependencies)s'],
               'name': module,
               'file_dep': [module],
               'uptodate': [False],
               'verbosity': 0,
               }


def task_docupload():
    """Uploads generated documentation to the RPSMARF web server"""
    return {'actions': 
            ['scp -r -q -o UserKnownHostsFile=/dev/null '
             '-o StrictHostKeyChecking=no '
            '-i ../tools/keys/smarf_dair_keys.pem '
            '../build_output/iceMsgDocs '
            'ubuntu@sw.rpsmarf.ca:/var/www/iceMsgDocs'
             ],
            'task_dep': ['docgen'],
            'verbosity': 2,
            }


def task_swrpsmarfsave():
    """Saves the sw.rpsmarf.ca /var/www folder to the tools tree"""
    return {'actions': [
                        'ssh  -o UserKnownHostsFile=/dev/null '
                        '-o StrictHostKeyChecking=no '
                        '-i ../tools/keys/smarf_dair_keys.pem '
                        'ubuntu@sw.rpsmarf.ca ' 
                        ' sudo tar cvf /tmp/sw_rpsmarf_ca_var_www.tar '
                        '    --exclude debs/\* --exclude iceMsgDocs/\* /var/www',
                        'scp -r -q -o UserKnownHostsFile=/dev/null '
                        '-o StrictHostKeyChecking=no '
                        '-i ../tools/keys/smarf_dair_keys.pem '
                        'ubuntu@sw.rpsmarf.ca:/tmp/sw_rpsmarf_ca_var_www.tar '
                        '../tools/sw_rpsmarf_ca '
                        ],
            'task_dep': [],
            'verbosity': 2,
            }


def task_swrpsmarfrestore():
    """Uploads the saved /var/www folder to the node sw.rpsmarf.ca"""
    return {'actions': 
            ['scp -r -q -o UserKnownHostsFile=/dev/null '
             '-o StrictHostKeyChecking=no '
            '-i ../tools/keys/smarf_dair_keys.pem '
            '../tools/sw_rpsmarf_ca/sw_rpsmarf_ca_var_www.tar ',
            'ubuntu@sw.rpsmarf.ca:/tmp'
            'ssh  -o UserKnownHostsFile=/dev/null '
            '-o StrictHostKeyChecking=no '
            '-i ../tools/keys/smarf_dair_keys.pem '
            'ubuntu@sw.rpsmarf.ca ' 
            ' sudo tar xvf /tmp/sw_rpsmarf_ca_var_www.tar '
             ],
            'task_dep': [],
            'verbosity': 2,
            }


