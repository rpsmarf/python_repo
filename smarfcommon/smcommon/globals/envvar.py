'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 10, 2014

    @author: rpsmarf
'''
import os
import time
import json
import logging

logger = logging.getLogger(__name__)


SM_ENV_VAR_COMMON_MIN_TIME_BETWEEN_PROGRESS = "MIN_TIME_BETWEEN_PROGRESS_CALLBACKS"
SM_ENV_VAR_COMMON_TEMP_DIR = "TEMP_DIR"
SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_SLEEP_TIME = "MOCK_OP_HANDLER_SLEEP_TIME"
SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_PREP_COUNT = "MOCK_OP_HANDLER_PREP_COUNT"
SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RUNNING_COUNT = "MOCK_OP_HANDLER_RUNNING_COUNT"
SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_CLEANUP_COUNT = "MOCK_OP_HANDLER_CLEANUP_COUNT"
SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RESULT = "MOCK_OP_HANDLER_RESULT"
SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_MAX_OUTPUT_SIZE = "MOCK_OP_HANDLER_OUTPUT_SIZE"
SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_MAX_PROGRESS = "MOCK_OP_HANDLER_PROGRESS"
SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE = "MOCK_OP_HANDLER_ENABLE"
SM_ENV_VAR_COMMON_KEY_DIR = "KEY_DIR"
SM_ENV_VAR_COMMON_CERT_CHECK = "SMARF_CERT_CHECK"
SM_ENV_VAR_COMMON_AUTH_HEADER = "SMARF_AUTH_HDR"
SM_ENV_VAR_COMMON_GIT_TAG_LIST_COMMAND = "SM_GIT_TAG_LIST_COMMAND"
SM_ENV_VAR_COMMON_GIT_TAG_GET_COMMAND = "SM_GIT_TAG_GET_COMMAND"
SM_ENV_VAR_COMMON_GIT_FETCH_COMMAND = "SM_GIT_FETCH_COMMAND"
SM_ENV_VAR_COMMON_GIT_FETCH_TAGS_COMMAND = "SM_GIT_FETCH_TAGS_COMMAND"
SM_ENV_VAR_COMMON_GIT_CHECKOUT_COMMAND = "SM_GIT_CHECKOUT_COMMAND"
SM_ENV_VAR_COMMON_GIT_RESET_COMMAND = "SM_GIT_RESET_COMMAND"
SM_ENV_VAR_COMMON_METRIC_DEST_HOST = "STATSD_HOST"
SM_ENV_VAR_COMMON_METRIC_DEST_PORT = "STATSD_PORT"
SM_ENV_VAR_TEST_AWS_KEY_ID = "SCS_TEST_AWS_KEY_ID" 
SM_ENV_VAR_TEST_AWS_SECRET_KEY = "SCS_TEST_AWS_SECRET_KEY"

# This password is used in various tests to access various things, but we don't want it in the 
# source code library.  It is typically set to s******5
SM_ENV_VAR_TEST_PASSWORD = "SM_TEST_PASSWORD"

SM_ENV_VAR_COMMON_DEFAULTS = {
                              SM_ENV_VAR_COMMON_MIN_TIME_BETWEEN_PROGRESS: "0.5",
                              SM_ENV_VAR_COMMON_TEMP_DIR: "/tmp/smarfTmp-" + str(time.time()),
                              SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_SLEEP_TIME: "1.0",
                              SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_PREP_COUNT: "3",
                              SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RUNNING_COUNT: "5",
                              SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_CLEANUP_COUNT: "2",
                              SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_RESULT: "success",
                              SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_MAX_OUTPUT_SIZE: "",
                              SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_MAX_PROGRESS: "100",
                              SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE: "False",
                              SM_ENV_VAR_COMMON_KEY_DIR: "/etc/smarf-sra/keys",
                              SM_ENV_VAR_COMMON_CERT_CHECK: "ON",
                              SM_ENV_VAR_COMMON_AUTH_HEADER: "",
                              SM_ENV_VAR_COMMON_METRIC_DEST_HOST: "localhost",
                              SM_ENV_VAR_COMMON_METRIC_DEST_PORT: "8125",
                              SM_ENV_VAR_COMMON_GIT_TAG_LIST_COMMAND: "git tag -l *_rpsmarf",
                              SM_ENV_VAR_COMMON_GIT_TAG_GET_COMMAND: "git cat-file tag $TAG$",
                              SM_ENV_VAR_COMMON_GIT_FETCH_COMMAND: "git fetch",
                              SM_ENV_VAR_COMMON_GIT_FETCH_TAGS_COMMAND: "git fetch --tags",
                              SM_ENV_VAR_COMMON_GIT_CHECKOUT_COMMAND: "git checkout $VERSION$",
                              SM_ENV_VAR_COMMON_GIT_RESET_COMMAND: "git checkout master",
                              SM_ENV_VAR_TEST_AWS_KEY_ID: "unset",
                              SM_ENV_VAR_TEST_AWS_SECRET_KEY: "unset",
                              SM_ENV_VAR_TEST_PASSWORD: None
                              }


def smCommonClearEnvVar():
    for var in SM_ENV_VAR_COMMON_DEFAULTS:
        if var in os.environ:
            del os.environ[var]
 
    
def smCommonGetEnvVar(name):
    if (name not in os.environ):
        return SM_ENV_VAR_COMMON_DEFAULTS[name]
    else:
        return os.environ[name]

    
def smCommonGetEnvVarFloat(name):
    var = smCommonGetEnvVar(name)
    if (var is None) or (var == ""):
        return None
    return float(smCommonGetEnvVar(name))


def smCommonGetEnvVarInt(name):
    var = smCommonGetEnvVar(name)
    if (var is None) or (var == ""):
        return None
    return int(var)


def smCommonGetEnvVarBool(name):
    return bool(smCommonGetEnvVar(name).lower() == "true")


def smLoadAwsKeys():
    try:
        with open('/tmp/aws_keys') as json_data:
            aws_keys = json.load(json_data)
        os.environ[SM_ENV_VAR_TEST_AWS_KEY_ID] = aws_keys[SM_ENV_VAR_TEST_AWS_KEY_ID]
        os.environ[SM_ENV_VAR_TEST_AWS_SECRET_KEY] = aws_keys[SM_ENV_VAR_TEST_AWS_SECRET_KEY]
    except:
        logger.warning("Key file /tmp/aws_keys was not opened")
        

