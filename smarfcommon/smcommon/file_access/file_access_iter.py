'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 15, 2015

    @author: rpsmarf
'''
import logging
from zipfile import ZIP_STORED
import zipstream

logger = logging.getLogger(__name__)


class SmFileAccessorIterator: 
    """Wrapper to convert file-like objects to iterables
    with special disconnect at the end.  This is used to get a stream of 
    data from a file accessor-type interface """

    def __init__(self, filename, fileAcc, externalConnectDisconnect, blksize=1024000):
        self.blksize = blksize
        self.fileAcc = fileAcc
        self.filename = filename
        self.externalConnectDisconnect = externalConnectDisconnect
        self.filelike = None

    def _closeFile(self):
        try:
            self.filelike.close()
        except:
            pass
        if not self.externalConnectDisconnect:
            logger.debug("DISCONNECTING")
            self.fileAcc.disconnectSession()
        
    def __getitem__(self, key):
        data = self.filelike.read(self.blksize)
        if data:
            return data
        self._closeFile()
        raise IndexError

    def __iter__(self):
        return self

    def __next__(self):
        ''' 
        Only connect when data asked for
        '''
        if not self.fileAcc.connected:
            if not self.externalConnectDisconnect:
                logger.debug("CONNECTING to open %s", self.filename)
                self.fileAcc.connectSession()

        try:
            if self.filelike is None:
                self.filelike = self.fileAcc.open(self.filename, "rb")
        except:
            self._closeFile()
            logger.exception("Failed to open file '%s'", self.filename)
            raise StopIteration

        data = self.filelike.read(self.blksize)
        if data:
            logger.debug("Returning %d bytes of data", len(data))
            return data
        self._closeFile()
        logger.debug("Got to the end of the file %s", self.filename)
        raise StopIteration


class SmZipStreamWrapper(zipstream.ZipFile):
    '''
    This is a wrapper around the zipstream file class to download
    ZIP files which are built on the fly.
    
    It exists to ensure that the fileAccessor is closed at the
    end of the file.
    '''
    def __init__(self, fileAccessor, fileobj=None, mode='w', 
                 compression=ZIP_STORED, allowZip64=True):
        super().__init__(fileobj=fileobj, mode=mode, 
                 compression=compression, allowZip64=allowZip64)
        self.fileAccessor = fileAccessor
        
    def close(self):
        super().close()
        self.fileAccessor.disconnectSession()
