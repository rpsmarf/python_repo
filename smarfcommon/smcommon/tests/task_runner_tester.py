'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import threading
from time import sleep
import logging
from smcommon.task_runners.task_runner import SmTaskRunnerBase

logger = logging.getLogger(__name__)


class SmTaskRunningTesterThread(threading.Thread):
    
    def __init__(self, testRunner, sleepTimeSec, success):
        logger.debug("Initializing SmTaskRunningTesterThread with sleepTimeSec %s", sleepTimeSec)
        super(SmTaskRunningTesterThread, self).__init__()
        self.sleepTimeSec = sleepTimeSec
        self.success = success
        self.stop = False
        self.progress = 0.0
        self.testRunner = testRunner
        
    def run(self):
        logger.debug("Starting task")
        if self.sleepTimeSec != 0:
            increment = 100.0 / (self.sleepTimeSec * 10)
            count = int(self.sleepTimeSec * 10)
        else:
            count = -1
        while (count > 0):
            count -= 1
            self.progress += increment
            self.testRunner.announceProgress({"progress": self.progress})
            if (self.stop):
                self.testRunner.done({})
                print("Task stopped")
                return
            sleep(0.1)
        
        logger.debug("Done task")
        self.testRunner.done({})


class SmTaskRunningTester(SmTaskRunnerBase):
    '''
    classdocs
    '''

    def __init__(self, taskId, doneCallback, taskDesc):
        '''
        Constructor
        '''
        SmTaskRunnerBase.__init__(self, taskId, doneCallback, taskDesc)
        self.setProgressCallback(self.progressCallback)
        self.progressCount = 0
        self._thread = SmTaskRunningTesterThread(self,
                                                taskDesc.getArgDict(0)["value"],
                                                taskDesc.getArgDict(1)["value"])
       
    def start(self):
        # Start a thread
        self._thread.start()
    
    def cancel(self):
        self._thread.stop = True

    def getProgress(self):
        return self._thread.progress
        
    def done(self, resultDict):
        self._doneCallback(self.taskId, "success", resultDict)
        
    def progressCallback(self, taskId, progressDict):
        self.progressCount += 1
