#/bin/bash

if [ "$2" == "" ]; then
    echo "Syntax is $0 <folderName> <depth>"
    exit 1
fi


NAME=$1
COUNT=$2

if [ "$COUNT" == "0" ]; then
    exit 0
fi

echo Creating `readlink -m $NAME`
mkdir $NAME
cd $NAME
NEW_COUNT=$(($COUNT - 1))
smDeepDirCreate.sh `basename $NAME` $NEW_COUNT

