angular.module('rpsmarf.foldernav.dragdrop', [
])

.directive('smDragDropCtrl', [function(){

	return {
		restrict: 'A',
		scope: false,
		controller: [ '$scope', '$element', '$attrs', function($scope, $element, $attrs){

			/***VARIABLES***/
			/*Standard*/
			var srcInfo = null;

			/***FUNCTIONS***/
			/*Standard*/
			this.dragStart = function(el, targetEntrys, srcResource, multi){
				$scope.$broadcast('sm-drag-start', el);
				if(multi){
					srcInfo = {selectedEntries: targetEntrys, srcResource: srcResource, multi:true};
				}
				else{
					srcInfo = {targetEntry: targetEntrys, srcResource: srcResource, multi: false};	
				}
				
			};

			this.dragMultiStart = function(){
				//srcInfo;
			};

			this.dragEnd = function(){
				$scope.$broadcast('sm-drag-end');
				srcInfo = null;
			};

			this.getSrcInfo = function(){
				return srcInfo;
			};
		}]
	};
}])
.directive('smDropArea', [ function(){

	return {
		restrict: 'A',
		require: '?^smDragDropCtrl',
		scope: {
			entries: '=',
			resource: '=',
			smondrop: '&'
		},
		link: function($scope, $element, $attrs, smDragDropCtrl){
			
			/***VARIABLES***/
			/*Scope*/
			$scope.smDragDropCtrl = smDragDropCtrl;
			$scope.element = $element;
			/*Standard*/

			/***EXE***/
			$scope.$on('sm-drag-start',function(ev,el){

				if(el.is($element)){
					return;
				}
				
				$element.addClass('sm-drop-area');

				

				successListener = $scope.$on('sm-drop-success', function(){

					$element.removeClass('sm-drop-area');
				});

				dragEndListener = $scope.$on('sm-drag-end',function(ev,el){
					$element.removeClass('sm-drop-area');

					
				});
				
			});

			
		},
		controller: ['$scope', '$element', '$attrs',function($scope, $element, $attrs){
		
			this.dragStart = function(targetEntrys,srcResource,multi ){
				if($scope.smDragDropCtrl){

					if(multi){
						$scope.smDragDropCtrl.dragStart($element,targetEntrys,srcResource, multi);		
					}
					else{
						$scope.smDragDropCtrl.dragStart($element,targetEntrys,srcResource, multi);		
					}
					
				}
				
			};

			this.dragEnd = function(){
				if($scope.smDragDropCtrl){
					$scope.smDragDropCtrl.dragEnd($element);
				}
			};

			this.isElement = function(el){
				return el.is($scope.element);
			};

			this.getSrcInfo = function(){
				return $scope.smDragDropCtrl.getSrcInfo();
			};
		
		}]


	};

}])

.directive('smDraggable',[function(){

	return {
		restrict: 'A',
		scope: false,
		require: '^smDropArea',
		link: function($scope, $element, $attrs, smDropAreaCtrl){

			$element.attr('draggable', 'true');

			var pane, originalX, originalY, img, n, multi;
			//img = angular.element('<img>');
			//img.attr('src','');
			//img.css({opacity: '0'});

			$element.on("dragstart", function(e) {
				
				
				
				if(e.originalEvent){
					e.originalEvent.dataTransfer.setData('object','');
					//originalX = e.originalEvent.pageX;
					//originalY = e.originalEvent.pageY;
					//e.originalEvent.dataTransfer.setDragImage(img.get(0),0,0);
				}

				
				n=0;
				for(var k in $scope.selectedentries){
					n++;
					if(n > 1){
						multi = true;
					}
				}

				if(multi){
					$element.addClass('drag');
					$element.find('.num-dragging > span').html(n);
					$element.find('.num-dragging').addClass('drag');
					smDropAreaCtrl.dragStart($scope.selectedentries, $scope.resource, multi);
				}
				else{
					smDropAreaCtrl.dragStart($scope.entry, $scope.resource);	
				}
				
				//console.log($scope.selectedentries);
				//angular.element('section.main-body-grid').on('dragover', function(e){
					//console.log('moving element ');
					//console.log(e);
					//console.log(e.originalEvent.pageX);
					//console.log(e.originalEvent.pageY);
					//var x = e.originalEvent.pageX-originalX, y = e.originalEvent.pageY-originalY;
					//var translateString = x+'px,'+y+'px';
					//$element.css({transform: 'translate('+translateString+')'});
				//});

			});

			$element.on("dragend", function(e){
				
				//pane.data('drag-object',null);
				//$element.closest('foldernavpane').removeClass('sm-drag-src');
				smDropAreaCtrl.dragEnd();
				angular.element('section.main-body-grid').off('dragover');
				if(multi){
					$element.removeClass('drag');
					$element.find('.num-dragging').removeClass('drag');
					multi = false;
				}
			});
			
			
		}
		
		
	};




}])

.directive('smDropEntry', [function(){
	return {
		restrict: 'A',
		scope: false,
		require: '^smDropArea',
		link: function($scope, $element, $attrs, smDropAreaCtrl){

			var dragEndListener = null;
			var successListener = null;
			$scope.$on('sm-drag-start',function(ev,el){
				if(smDropAreaCtrl.isElement(el)){
					return;
				}

				$element.on('dragover', function(event){
										
					$element.addClass('sm-drop-allow');
					if(event.originalEvent){
						event.originalEvent.preventDefault();
						event.originalEvent.dataTransfer.dropEffect = 'move';
					}
				});

				$element.on('dragleave', function(event){

					$element.removeClass('sm-drop-allow');
				});

				$element.on('drop', function(ev){
					console.log('droppped!');
					var e;
					if(ev.originalEvent){
						e = ev.originalEvent;
						e.preventDefault();
						e.stopPropagation();
					}

					//var data = e.dataTransfer.getData('text/html');
					
					var srcInfo = smDropAreaCtrl.getSrcInfo();


					if(srcInfo.multi){
						$scope.smmultidrop({selectedEntries: srcInfo.selectedEntries, srcResource: srcInfo.srcResource, dstResource: $scope.resource, dstEntry: $scope.entry});
					}
					else{
						$scope.smondrop({srcEntry: srcInfo.targetEntry, srcResource: srcInfo.srcResource, dstResource: $scope.resource, dstEntry: $scope.entry});	
					}
					

				});

				successListener = $scope.$on('sm-drop-success', function(){

					$element.removeClass('sm-drop-area');
					$element.removeClass('sm-drop-allow');
				});

				dragEndListener = $scope.$on('sm-drag-end',function(ev,el){
					

					$element.off('dragover');

					$element.off('dragleave');

					$element.off('drop');

					if(successListener){
						successListener();
						successListener = null;
					}
					
					if(dragEndListener){
						dragEndListener();
						dragEndListener = null;
					}
					
				});
			});

		}
	};
}])

;