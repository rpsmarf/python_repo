'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import unittest
from smarfcontrolserver.utils.sm_rest_param import SmParamInfo2
from smcommon.metrics.metrics_sender import SmMetricSender, METRIC_TYPE_COUNTER,\
    METRIC_TYPE_GUAGE
import time
from smcommon.utils import net_utils
import os
from smcommon.globals.envvar import SM_ENV_VAR_COMMON_METRIC_DEST_PORT,\
    SM_ENV_VAR_COMMON_METRIC_DEST_HOST
from smcommon.tests.metric_receiver import TestMetricReceiver
from statsd.defaults.env import statsd
import logging
    
logger = logging.getLogger(__name__)
 

TEST_PARAM_DICT = {
    "desc": "This URL allows access to files under a resource",
    "params": [{
                "name": "pint",
                "type": "int",
                "default": 1000,
               },
               {
                "name": "pstring",
                "type": "string",
                "default": "abc",
               },
               {
                "name": "pfloat",
                "type": "float",
                "default": 3.0,
               },
               {
                "name": "pbool",
                "type": "bool",
                "default": False,
               },
               ]
}        


class TestRequest(object):
    def __init__(self):
        self.GET = {}
        self.POST = {}

        
testFunc1Count = 0    


def testFunc1():
    global testFunc1Count
    testFunc1Count += 1
    return 1


testFunc2Count = 0    


def testFunc2():
    global testFunc2Count
    testFunc2Count += 1
    return 2


class TestMisc(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testSmRestParamClass(self):
        request = TestRequest()
        paramInfo = SmParamInfo2(TEST_PARAM_DICT, "1/a/b/", request)
        self.assertEqual(1, paramInfo.getObjectId())
        self.assertEqual("abc", paramInfo.getParam("pstring"))
        self.assertEqual(1000, paramInfo.getParam("pint"))
        self.assertEqual(3.0, paramInfo.getParam("pfloat"))
        self.assertEqual(False, paramInfo.getParam("pbool"))
        request.GET["pstring"] = "v1"
        request.GET["pint"] = "99"
        request.GET["pbool"] = "True"
        request.GET["pfloat"] = "2.5"
        paramInfo = SmParamInfo2(TEST_PARAM_DICT, "1/a/b/", request)
        self.assertEqual(1, paramInfo.getObjectId())
        self.assertEqual("v1", paramInfo.getParam("pstring"))
        self.assertEqual(99, paramInfo.getParam("pint"))
        self.assertEqual(2.5, paramInfo.getParam("pfloat"))
        self.assertEqual(True, paramInfo.getParam("pbool"))
        
        request = TestRequest()
        paramInfo = SmParamInfo2(TEST_PARAM_DICT, "1/a/b/", request)
        self.assertEqual(1, paramInfo.getObjectId())
        self.assertEqual("abc", paramInfo.getParam("pstring"))
        self.assertEqual(1000, paramInfo.getParam("pint"))
        self.assertEqual(3.0, paramInfo.getParam("pfloat"))
        self.assertEqual(False, paramInfo.getParam("pbool"))
        request.GET["pstring"] = "v1"
        request.GET["pint"] = "99"
        request.GET["pbool"] = "True"
        request.GET["pfloat"] = "2.5"
        paramInfo = SmParamInfo2(TEST_PARAM_DICT, "1/a/b/", request)
        self.assertEqual(1, paramInfo.getObjectId())
        self.assertEqual("v1", paramInfo.getParam("pstring"))
        self.assertEqual(99, paramInfo.getParam("pint"))
        self.assertEqual(2.5, paramInfo.getParam("pfloat"))
        self.assertEqual(True, paramInfo.getParam("pbool"))
        
    def testMetricCallbacks(self):
        port = net_utils.smGetFreePort()
        statsd._addr = ("localhost", port)
        
        # Create sender which is called every 0.1 sec
        os.environ[SM_ENV_VAR_COMMON_METRIC_DEST_PORT] = str(port)
        os.environ[SM_ENV_VAR_COMMON_METRIC_DEST_HOST] = "localhost"
        sender = SmMetricSender(0.1)
        
        rxer = TestMetricReceiver(port)
        rxer.start()
        time.sleep(0.1)
        
        # Register a function
        sender.registerCallback("m1", METRIC_TYPE_COUNTER, -1, testFunc1)
        sender.registerCallback("m2", METRIC_TYPE_GUAGE, -1, testFunc2)
        sender.start()
        try:
            time.sleep(0.3)
            self.assertTrue(testFunc1Count >= 2)
            self.assertTrue(testFunc2Count >= 2)
            for _ in range(0, 10):
                if len(rxer.rxedMetrics) >= 4:
                    break
                else:
                    time.sleep(0.1)
            # Metric receiver not working - according to 
            # echo Hello | nc -4u -w1 localhost <port>  it gets a UDP message - could be a V6 issue?
            # self.assertTrue(len(rxer.rxedMetrics) >= 4, "Got " + str(len(rxer.rxedMetrics)))
        finally:
            sender.stop()
            rxer.stop()
         
         
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
