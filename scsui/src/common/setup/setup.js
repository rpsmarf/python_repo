angular.module('rpsmarf.setup', [
	'ui.router',
	'ui.bootstrap',
	'restangular',
	'rpsmarf.user.service'
])
.config(['$stateProvider',function($stateProvider){
	$stateProvider.state('setup',{
		url: '/setup/',
		abstract: true,
		views: {
			'main': {
				templateUrl: 'setup/setup.tpl.html',
				controller: 'SetupCtrl'
			}
		},
		data:{ pageTitle: 'Setup' }
	}).state('setup.page',{
		url: '',
		views: {
			'account-settings': {
				templateUrl: 'user.settings/user.settings.account.tpl.html',
				controller: 'UserSettingsCtrl'
			}
		}
	});

}])
.controller('SetupCtrl', ['$scope','$state','$modal','Restangular','SmUser', function($scope, $state, $modal, Restangular, SmUser){

	/***STANDARD VARIABLES***/
	var uid = SmUser.getProperty('id');
	/***SCOPE VARIABLES****/
	$scope.acked = false;
	$scope.userAckRequired = true;
	$scope.formError = false;
	$scope.userSettings = {value : []};
	/***SCOPE FUNCTIONS***/
	$scope.done = function(){
		console.log($scope.userSettings);
		if(!$scope.acked){
			$scope.formError = true;
			$modal.open({
				templateUrl: 'alert/alert.tpl.html',
				controller: 'AlertCtrl',
				resolve: {
					message: function(){ return 'You must agree to the Terms of Service before continuing.';},
					title: function(){ return null;}
				}
			});
		}
		else{

			var obj = {}, setting;
			for(var i = $scope.userSettings.value.length-1; i>=0; i--){
				setting = $scope.userSettings.value[i];
				if(!setting.editable){
					continue;
				}

				if(setting.select){
					obj[setting.obj_name] = setting.value.resource_uri;	
				}
				else{
					obj[setting.obj_name] = setting.value;
				}
				
				

				if(setting.obj_name === 'community'){
					SmUser.setProperty('community',setting.value);
				}
			}
			obj.performSetupOnNextLogin = false;
			Restangular.one('user_setting',uid+'/').patch(obj, null, {"Content-Type": "application/json"});
				
			//Restangular.one('user_setting',SmUser.getProperty('id')+'/').patch({performSetupOnNextLogin: false});
			$state.go('home');
		}
	};

	$scope.selectCheckbox = function(){
		$scope.acked = !$scope.acked;
		$scope.formError = false;
	};
}]);