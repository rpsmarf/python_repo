angular.module('rpsmarf.user.resources', [
	'rpsmarf.user.service',
	'restangular',
	'uiSwitch',
	'ui.router',
	'ui.bootstrap',
	'rpsmarf.confirm'

]).config(['$stateProvider',function($stateProvider){

	$stateProvider.state('user.resources',{
		url: '^/user/resources/',
		views:{
			'main@':{
				controller: 'UserResourcesCtrl',
				templateUrl: 'user.resources/user.resources.tpl.html'
			}
		},
		data:{
			pageTitle: 'Contributed Resources'
		}
	});

}])
.controller('UserResourcesCtrl', ['$scope','$state', 'SmUser', 'Restangular', '$modal',
	function($scope, $state, SmUser, Restangular, $modal){

		/***STANDARD VARIABLES***/
		var uid = SmUser.getProperty('id');
		/***SCOPE VARIABLES***/
		$scope.resources = null;
		$scope.tools = null;
		$scope.communities = null;
		$scope.resource_types = null;
		$scope.containers = null;
		$scope.selectedCommunity = {obj:null};
		$scope.resource = {name:null, resource_type:null,owner:null,container:null,nature:'data', parametersJson: null,description:null};
		$scope.current = {};

		/***SCOPE FUNCTIONS***/
		$scope.resourceAttachToggle = function(obj, type){
			obj.locked = !obj.locked;
			
			Restangular.one(type,obj.id+'/').patch({locked: obj.locked}).then(function(response){
				console.log(type + ' locked updated');
			});
			
		};

		$scope.getResourceRelatedTypes = function(){
			Restangular.all('resource_type/?nature=data').get('').then(function(response){
				$scope.resource_types = response.objects;
			});

			Restangular.all('container/').get('').then(function(response){
				$scope.containers = response.objects;
			});

			Restangular.all('community/').get('').then(function(response){
				$scope.communities = response.objects;
			});
		};

		$scope.setResourceNature = function(resource){
			var nature = resource.nature;
			Restangular.all('resource_type/?nature='+nature).get('').then(function(response){
				$scope.resource_types = response.objects;
			});

		};

		$scope.submitResourceForm = function(resourceForm){
			var form = resourceForm, obj, errMsg, resourceId,loc;
			var patt = /(\*|\?|\"|<|>|\|)/;
			if(patt.test(form.path)){
				form.errMsg = 'Path cannot contain the characters: * ? " < > |';
				form.$valid = false;
				form.hasEror = true;
			}	
			if(form.$valid){
				obj = angular.copy($scope.resource);
				obj.resource_type = obj.resource_type.resource_uri;
				obj.container = obj.container.resource_uri;
				obj.owner = '/scs/user/'+uid+'/';
				if(obj.nature === 'data'){
					if(obj.parametersJson === '/' || obj.parametersJson === null){
						obj.parametersJson = '';
					}
					else if(obj.parametersJson.substring(obj.parametersJson.length-1) !== '/'){
						obj.parametersJson += '/';
					}
					obj.parametersJson = JSON.stringify({folder: obj.parametersJson});
				}
				else{
					obj.parametersJson = '{}';
				}
				
				Restangular.configuration.fullResponse = true;
				Restangular.all('resource/').post(obj).then(function(response){
					Restangular.configuration.fullResponse = false;
					if($scope.selectedCommunity.obj.tag && $scope.selectedCommunity.obj.tag !== ''){
						loc = getLocation(response.headers('Location'));
						resourceId = loc.pathname.split('/')[3];
						Restangular.all('resource_tag/').post({resource: '/scs/resource/'+resourceId+'/', tag: $scope.selectedCommunity.obj.tag}).then(function(){
							resourceAddedSuccess();
						});
					}
					else{
						resourceAddedSuccess();
					}
					
				}, function(){
					Restangular.configuration.fullResponse = false;
					$modal.open({
						controller: 'AlertCtrl',
						templateUrl: 'alert/alert.tpl.html',
						resolve: {
							title: function(){ return 'Error';},
							message: function(){ return 'Could not add resource at this time.';}
						}
					});
				});
			}
			else{
				form.hasError = true;
				if(form.description.$error.minlength){
					errMsg = 'Description must be at least 20 characters long.';
				}
				$modal.open({
					controller: 'AlertCtrl',
					templateUrl: 'alert/alert.tpl.html',
					resolve: {
						title: function(){ return 'Error';},
						message: function(){ return errMsg;}
					}
				});
			}
		};

		$scope.refreshResources = function(){
			Restangular.all('resource/?owner__id='+uid+'&extForUser='+uid).get('').then(function(response){
				for(var i=response.objects.length-1; i>=0; i--){
					response.objects[i].attached = !response.objects[i].locked;
					
				}
				$scope.resources = response.objects;
			});
		};
		
		$scope.editResource = function(resource){
			$modal.open({
				templateUrl: 'user.resources/user.resources.modal.tpl.html',
				controller: 'UserResourcesEditCtrl',
				size: 'lg',
				resolve: {
					resource: function(){ return resource;}
				}
			});
		};

		$scope.deleteResource = function(resource, index){
			var modalInstance = $modal.open({
				templateUrl: 'confirm/confirm.tpl.html',
				controller: 'ConfirmCtrl',
				resolve: {
					title: function(){ return 'Confirm Deletion'; },
					body: function(){ return 'Delete '+ resource.name+'? Cannot be undone.';}
				}
			});

			modalInstance.result.then(function(result){
				Restangular.one('resource',resource.id+'/').remove().then(function(){
					console.log('resource deleted');
					$scope.resources.splice(index,1);
				});
			}, function(){
				console.log('Deletion cancelled.');
			});
		};

		/***STANDARD FUNCTIONS***/
		var resourceAddedSuccess = function(){
			$scope.resource = {name:null, resource_type:null,owner:null,container:null,nature:'data', parametersJson: null,description:null};
			$scope.selectedCommunity.obj = null;
			$modal.open({
				controller: 'AlertCtrl',
				templateUrl: 'alert/alert.tpl.html',
				resolve: {
					title: function(){ return 'Notice';},
					message: function(){ return 'Resource was successfully added.';}
				}
			});
		};

		var getLocation = function(href) {
			var loc = document.createElement("a");
			loc.href = href;
			// IE doesn't populate all link properties when setting .href with a relative URL,
			// however .href will return an absolute URL which then can be used on itself
			// to populate these additional fields.
			if (loc.host === "") {
				loc.href = loc.href;
			}
			return loc;
		};

		/***EXE***/
		Restangular.all('resource/?owner__id='+uid+'&extForUser='+uid).get('').then(function(response){
			for(var i=response.objects.length-1; i>=0; i--){
				response.objects[i].attached = !response.objects[i].locked;
				
			}
			$scope.resources = response.objects;
		});

		Restangular.all('task_type/?owner__id='+uid+'&extForUser='+uid).get('').then(function(response){
			for(var i=response.objects.length-1; i>=0; i--){
				response.objects[i].attached = !response.objects[i].locked;
				
			}
			$scope.tools = response.objects;
		});

}])
.controller('UserResourcesEditCtrl', ['$scope', 'Restangular', '$modalInstance', 'resource', '$modal',
	function($scope, Restangular, $modalInstance,resource, $modal){

		/***STANDARD VARIABLES***/
		var pj;
		var rtId = resource.resource_type.split('/')[3];
		var cId = resource.container.split('/')[3];
		var originalResourceTag;
		var obj;
		/***SCOPE VARIABLES***/
		$scope.editing = true;
		$scope.resource = angular.copy(resource);
		$scope.resources = null;
		$scope.tools = null;
		$scope.communities = null;
		$scope.resource_types = null;
		$scope.containers = null;
		$scope.selectedCommunity = {obj:null};
		$scope.current = {};

		if($scope.resource.parametersJson && $scope.resource.parametersJson !== ''){
			pj = JSON.parse($scope.resource.parametersJson);
			if(pj.folder){
				$scope.resource.parametersJson = pj.folder;
			}
			else{
				$scope.resource.parametersJson = '';
			}
		}
		

		
		/***SCOPE FUNCTIONS***/
		$scope.submitResourceForm = function(resourceForm){
			var form = resourceForm, errMsg;
			var patt = /(\*|\?|\"|<|>|\|)/;
			if(patt.test(form.path)){
				form.errMsg = 'Path cannot contain the characters: * ? " < > |';
				form.$valid = false;
				form.hasEror = true;
			}	
			if(form.$valid){
				obj = {description: $scope.resource.description, parametersJson: $scope.resource.parametersJson, nature: $scope.resource.nature };

				if(obj.nature === 'data'){
					if(obj.parametersJson === '/' || obj.parametersJson === null){
						obj.parametersJson = '';
					}
					else if(obj.parametersJson.substring(obj.parametersJson.length-1) !== '/'){
						obj.parametersJson += '/';
					}
					$scope.resource.parametersJson = obj.parametersJson;
					obj.parametersJson = JSON.stringify({folder: obj.parametersJson});
				}
				else{
					obj.parametersJson = '{"folder":""}';
					$scope.resource.parametersJson = "";
				}
				
				Restangular.one('resource',$scope.resource.id+'/').patch(obj).then(function(){
					if($scope.selectedCommunity.obj.tag && $scope.selectedCommunity.obj.tag !== ''){
						if(originalResourceTag){
							Restangular.one('resource_tag',originalResourceTag.id+'/').patch({tag_name: 'community:'+$scope.selectedCommunity.obj.name_key}).then(function(){
								resourcePatchSuccess();
							});
						}
						else{
							Restangular.all('resource_tag/').post({resource: '/scs/resource/'+$scope.resource.id+'/', tag: $scope.selectedCommunity.obj.tag}).then(function(){
								resourcePatchSuccess();
							});
						
						}
						
					}
					else if((!$scope.selectedCommunity.obj.tag || $scope.selectedCommunity.obj.tag === '') && originalResourceTag){
						Restangular.one('resource_tag',originalResourceTag.id+'/').remove({resource__id:$scope.resource.id}).then(function(){
							resourcePatchSuccess();
						});
					}
					else{
						resourcePatchSuccess();
					}
					
				}, function(){
					$modal.open({
						controller: 'AlertCtrl',
						templateUrl: 'alert/alert.tpl.html',
						resolve: {
							title: function(){ return 'Error';},
							message: function(){ return 'Error updating resource';}
						}
					});
				});
			}
			else{
				form.hasError = true;
				if(form.description.$error.minlength){
					errMsg = 'Description must be at least 20 characters long.';
				}
				else if(form.description.$error.required){
					errMsg = 'Description is required.';
				}
				$modal.open({
					controller: 'AlertCtrl',
					templateUrl: 'alert/alert.tpl.html',
					resolve: {
						title: function(){ return 'Error';},
						message: function(){ return errMsg;}
					}
				});
			}
		};

		$scope.close = function(){
			$modalInstance.close();
		};

		/***STANDARD FUNCTIONS***/
		var resourcePatchSuccess = function(){
			resource.description = $scope.resource.description;
			resource.parametersJson = obj.parametersJson;
			$modal.open({
				controller: 'AlertCtrl',
				templateUrl: 'alert/alert.tpl.html',
				resolve: {
					title: function(){ return 'Notice';},
					message: function(){ return 'Resource was successfully updated';}
				}
			});
		};

		var getLocation = function(href) {
			var loc = document.createElement("a");
			loc.href = href;
			// IE doesn't populate all link properties when setting .href with a relative URL,
			// however .href will return an absolute URL which then can be used on itself
			// to populate these additional fields.
			if (loc.host === "") {
				loc.href = loc.href;
			}
			return loc;
		};
		

		/***EXE***/
		Restangular.one('resource_type',rtId+'/').get().then(function(response){
			response = response.plain();
			$scope.resource.resource_type = response;
		});
		Restangular.one('container',cId+'/').get().then(function(response){
			response = response.plain();
			$scope.resource.container = response;
		});
		Restangular.all('resource_tag/?resource__id='+resource.id).get('').then(function(response){
			if(response.objects.length > 0){
				var resTag = response.objects[0];
				var resTagId = resTag.tag.split('/')[3];
				originalResourceTag = response;
				Restangular.one('tag', resTagId+'/').get().then(function(response){
					response = response.plain();
					var nameKey = response.tag_name.substring(response.tag_name.indexOf('community:')+10);
					console.log(nameKey);
					Restangular.all('community/?name_key='+nameKey).get('').then(function(response){
						$scope.selectedCommunity.obj = response.objects[0];
					});
				});
			}
			else{
				Restangular.all('community/?name_key=all_resources').get('').then(function(response){
					$scope.selectedCommunity.obj = response.objects[0];
				});
			}
			

		});
		Restangular.all('resource_type/?nature=data').get('').then(function(response){
			$scope.resource_types = response.objects;
		});

		Restangular.all('container/').get('').then(function(response){
			$scope.containers = response.objects;
		});

		Restangular.all('community/').get('').then(function(response){
			$scope.communities = response.objects;
		});
}]);