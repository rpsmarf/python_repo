import json
import sys
from scripts.scs_obj_common import scsScriptGetDefaultUrl
from smcommon.globals.envvar import SM_ENV_VAR_TEST_PASSWORD, smCommonGetEnvVar
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath, scsGetToJson
from scripts.scs_obj_create import scsCreateObjectPath, scsModifyObject,\
    scsCreateObjectId

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

root_path = scsGetObjectPath("user", "username=rpsmarf")
all_users_group_path = scsGetObjectPath("group", "name=all_users")

userList = [scsGetObjectPath("user", "username=rpsmarf"),
            scsGetObjectPath("user", "username=andrewmcgregor"),
            scsGetObjectPath("user", "username=navdeepk"),
            scsGetObjectPath("user", "username=bnandy"),
            scsGetObjectPath("user", "username=sikharesh"),
            scsGetObjectPath("user", "username=marc_sthilaire"),
            ]
bridge_community_tag_path = scsCreateObjectPath("tag", 
                                                {"tag_name": "community:bridge_mon"})
bridge_community_path = scsCreateObjectPath("community", 
                                     {"name": "Bridge Monitoring",
                                      "name_key": "bridge_mon",
                                      "description": "This community contains all "
                                      "resources associated with bridge monitoring.",
                                      "owner": root_path,
                                      "tag": bridge_community_tag_path,
                                      "imageUrl": "https://www.rpsmarf.ca/icons-scs/bridgeCommunityIcon.jpg",
                                      })

user_amir_id = scsCreateObjectId("user", {"username": "amir",
                                          "email": "amir@carleton.ca",
                                          "password": smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD),
                                          "first_name": "Amir",
                                          "last_name": "Tehranian"})

scsModifyObject("/scs/user_setting/" + str(user_amir_id) + "/", {"community": bridge_community_path})
user_amir = scsGetObjectPath("user", "username=amir")
userList.append(user_amir)

user_serge_id = scsCreateObjectId("user", {"username": "sdesjardins",
                                      "email": "serge.desjardins@umoncton.ca",
                                      "password": "5591-5651",
                                      "first_name": "Serge",
                                      "last_name": "Desjardins"})
scsModifyObject("/scs/user_setting/" + str(user_serge_id) + "/", {"community": bridge_community_path})
user_serge = scsGetObjectPath("user", "username=sdesjardins")
userList.append(user_serge)


scsCreateObjectPath("news_item", 
                    {"owner": root_path,
                     "headline": "Amir away from May to August",
                     "body": "Amir Tehranian is away from May to August 2015 inclusive.  For SPPLASH support "
                     "please contain Serge Dejardins.",
                     "community": bridge_community_path,
                     })

scsCreateObjectPath("news_item", 
                    {"owner": root_path,
                     "headline": "Serge visiting Carleton from June 9th to June 15th",
                     "body": "Serge Desjardins is visiting Carleton to discuss the evolution  "
                     "of the SPPLASH tool.",
                     "community": bridge_community_path,
                     })


print("Adding logger resource type and resources")
rt_spplash_data_path = scsCreateObjectPath("resource_type", {"name": "SPPLASH Logger Data Type", 
                                                             "name_key": "spplash_tool_data_source", 
                                                             "nature": "data",
                                                             "description": "This is a source of SPPLASH data"})

spplash_container_path = scsGetObjectPath("container", "name_key=carleton_bridge_data")
res_path = scsCreateObjectPath("resource", {"name": "Jan 2015 Sample Bridge Data", 
                                            "name_key": "sample_bridge_data",
                                            "resource_type": rt_spplash_data_path,
                                            "owner": root_path,
                                            "parametersJson": json.dumps({"folder": "Jan-2015-Dataset/"}),
                                            "container": spplash_container_path,
                                            "description": "This is the January 2015 version of the sample bridge data"
                                            }
                               )
scsCreateObjectPath("resource_tag", {"tag": bridge_community_tag_path, "resource": res_path})
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=r")

print("Adding animation resource type and resources")
rt_spplash_displacement_data_path = scsCreateObjectPath("resource_type", {"name": "SPPLASH Animation Data Type", 
                                                             "name_key": "spplash_animation_data_source", 
                                                             "nature": "data",
                                                             "description": "This is a source of SPPLASH "
                                                             "bridge animation data"})

spplash_container_path = scsGetObjectPath("container", "name_key=carleton_bridge_data")
res_path = scsCreateObjectPath("resource", {"name": "Bridge Displacement File - Set A", 
                                            "name_key": "bridge_animation_a",
                                            "resource_type": rt_spplash_displacement_data_path,
                                            "owner": root_path,
                                            "parametersJson": json.dumps({"folder": "animation_files/file_a/"}),
                                            "container": spplash_container_path,
                                            "description": "This is the first set of bridge animation data "
                                            "files for use with the SPPLASH tool"
                                            }
                               )
scsCreateObjectPath("resource_tag", {"tag": bridge_community_tag_path, "resource": res_path})
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=r")

res_path = scsCreateObjectPath("resource", {"name": "Bridge Displacement File - Set B", 
                                            "name_key": "bridge_animation_b",
                                            "resource_type": rt_spplash_displacement_data_path,
                                            "owner": root_path,
                                            "parametersJson": json.dumps({"folder": "animation_files/file_b/"}),
                                            "container": spplash_container_path,
                                            "description": "This is the second set of bridge animation data files "
                                            "for use with the SPPLASH tool"
                                            }
                               )
scsCreateObjectPath("resource_tag", {"tag": bridge_community_tag_path, "resource": res_path})
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=r")

res_path = scsCreateObjectPath("resource", {"name": "Bridge Displacement File - Set C", 
                                            "name_key": "bridge_animation_c",
                                            "resource_type": rt_spplash_displacement_data_path,
                                            "owner": root_path,
                                            "parametersJson": json.dumps({"folder": "animation_files/file_c/"}),
                                            "container": spplash_container_path,
                                            "description": "This is the third set of bridge animation data files "
                                                        "for use with the SPPLASH tool"
                                            }
                               )
scsCreateObjectPath("resource_tag", {"tag": bridge_community_tag_path, "resource": res_path})
for userPath in userList:
    scsGetToJson(res_path + "setperm/?action=assign&user=" + userPath + "&perm=r")

carleton_container_path = scsGetObjectPath("container", "name_key=carleton_bridge_data") 
carleton_spplash_runner = scsGetObjectPath("container", "name_key=carleton_spplash_runner") 
carleton_spplash_data = scsGetObjectPath("container", "name_key=maj11_storage") 
rt_cloud_files_data_path = scsGetObjectPath("resource_type", "name_key=generic_storage")

TASK_TYPE_SPPLASH_CFG_DICT = {
                            "args": [ 
                                     {"type": "string", 
                                      "name": "Argument", 
                                      "value": "/cygdrive/c/Users/rpsmarf/Desktop/spplash-rpsmarf-local.sh"}
                                     ],
                            "computeResourceRole": "spplash_compute",
                            "interactive": True
                       }
tt_path = scsCreateObjectPath("task_type", {"name": "SPPLASH - Use local data",
                                            "name_key": "win_spplash_local",
                                            "description": " This tool runs the SPPLASH application without selecting "
                                            "input files on the network.", 
                                            "code_module": "smcommon.task_runners.task_runner_shell", 
                                            "code_classname": "SmTaskRunnerShell", 
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_SPPLASH_CFG_DICT)})
scsCreateObjectPath("task_type_tag", {"tag": bridge_community_tag_path, "task_type": tt_path})
for userPath in userList:
    scsGetToJson(tt_path + "setperm/?action=assign&user=" + userPath + "&perm=x")

rt_spplash_compute_path = scsCreateObjectPath("resource_type", 
                                           {"name": "SPPLASH Tool Runner",                                      
                                            "nature": "compute",
                                            "description": "This is a computing resource which can run the SPPLASH tool"})

scsCreateObjectPath("resource_type_task_type", {"role": "spplash_compute", "task_type": tt_path, 
                                                "resource_type": rt_spplash_compute_path,
                                                "description": "This is the compute resource for the SPPLASH tool",
                                                "nature": "compute",
                                                "name": "SPPLASH Compute Resource Type Task Type",
                                                "name_key": "spplash_compute"})


TASK_TYPE_SPPLASH_ANIMATION_CFG_DICT = {
                                        "args": [ 
                                                 {"type": "string", 
                                                  "name": "Argument", 
                                                  "value": "/cygdrive/c/Users/rpsmarf/Desktop/spplash-rpsmarf.sh"},
                                                 {"type": "resource", 
                                                  "name": "File of bridge displacement data", 
                                                  "pathType": "file", 
                                                  "input_id": 1, 
                                                  "direction": "input", 
                                                  "role": "spplash_src", 
                                                  "description": "This specifies the file of data to submit for animation"},
                                                 ],
                                        "computeResourceRole": "spplash_compute",
                                        "interactive": True
                                        }
tt_path = scsCreateObjectPath("task_type", {"name": "SPPLASH - Animate Bridge",
                                            "name_key": "win_spplash_animation",
                                            "description": " This tool runs the SPPLASH application with input "
                                            "data selected from a network file reposoitory.", 
                                            "code_module": "smcommon.task_runners.task_runner_shell", 
                                            "code_classname": "SmTaskRunnerShell", 
                                            "owner": root_path,
                                            "configurationJson": json.dumps(TASK_TYPE_SPPLASH_ANIMATION_CFG_DICT)})
scsCreateObjectPath("task_type_tag", {"tag": bridge_community_tag_path, "task_type": tt_path})
for userPath in userList:
    scsGetToJson(tt_path + "setperm/?action=assign&user=" + userPath + "&perm=x")

scsCreateObjectPath("resource_type_task_type", {"role": "spplash_src", "task_type": tt_path, 
                                                "resource_type": rt_spplash_displacement_data_path,
                                                "description": "This is the data resource for the SPPLASH tool",
                                                "nature": "data",
                                                "name": "SPPLASH Displacement Data Resource Type Task Type",
                                                "name_key": "spplash_displacement_data"})

scsCreateObjectPath("resource_type_task_type", {"role": "spplash_compute", "task_type": tt_path, 
                                                "resource_type": rt_spplash_compute_path,
                                                "description": "This is the compute resource for the SPPLASH tool",
                                                "nature": "compute",
                                                "name": "SPPLASH Animation Resource Type Task Type",
                                                "name_key": "spplash_compute_animation"})


res_path = scsCreateObjectPath("resource", {"name": "SPPLASH Running Resource", 
                                            "name_key": "maj11_win_runner",
                                            "resource_type": rt_spplash_compute_path,
                                            "owner": root_path,
                                            "parametersJson": 
                                            json.dumps({"taskViewBaseUrl": 
                                                        "http://majumdar-10.sce.carleton.ca:9214/guacamole"
                                                        "/client.xhtml?id=c%2Fmaj-11",
                                                        "keyfile": "dropbear_key",
                                                        "user": "rpsmarf",
                                                        "tempDataFolder": "/cygdrive/c/temp"
                                                        }),
                                            "container": carleton_spplash_runner,
                                            "description": "This is the Carleton-based SPPLASH Windows host"
                                            }
                               )
scsCreateObjectPath("resource_tag", {"tag": bridge_community_tag_path, "resource": res_path})
scsGetToJson(res_path + "setperm/?action=assign&group=" + all_users_group_path + "&perm=x")

res_path = scsCreateObjectPath("resource", {"name": "SPPLASH Local Data Storage", 
                                            "name_key": "spplash_local_data",
                                            "resource_type": rt_cloud_files_data_path,
                                            "owner": root_path,
                                            "parametersJson": 
                                            json.dumps({"folder": 
                                                        "SPPLASH_Working_Data/"
                                                        }),
                                            "container": carleton_spplash_data,
                                            "description": "This is the Carleton-based SPPLASH Windows storage resource"
                                            }
                               )
scsCreateObjectPath("resource_tag", {"tag": bridge_community_tag_path, "resource": res_path})
scsGetToJson(res_path + "setperm/?action=assign&group=" + all_users_group_path + "&perm=x")


print("Task type 'win_spplash' objects setup.")
