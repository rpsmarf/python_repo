describe( 'taskservice module', function() {

	beforeEach (module('rpsmarf.config'));
	beforeEach(module('rpsmarf.taskservice'));	
	beforeEach(module('restangular'));	

	var $scope,  SmTaskCreate, $state,  $modalInstance, smarfurl, $httpBackend, tool_prefix = 'src/app/tools/', taskcreate_prefix='src/app/taskservice/';
	var rt3s, resource_types, copy_task, dataObj, SmTask;
	//init, starting, prep, running, cleanup and finished
	
	beforeEach(  inject(function( Restangular, SMARF_URL){
		smarfurl = SMARF_URL+'/scs/';
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
	}));


	describe('functionality', function(){


		beforeEach(  inject(function($rootScope, _$httpBackend_, _SmTask_){
			
			SmTask = _SmTask_;
			$httpBackend = _$httpBackend_;
			$scope = $rootScope.$new();
			//console.log(__html__[taskcreate_prefix+'task.create.service.spec.json']);
			dataObj = JSON.parse(__html__[taskcreate_prefix+'taskservice.spec.json']);
			copy_task = dataObj.copy_task;
			rt3s = dataObj.rt3s;
			//console.log(rt3s);
			
		}));

		

		it('should retrieve the copy task information and initialize the copy task info object', inject([function(){
			$httpBackend.expect('GET',smarfurl+'task_type/?name_key=copy').respond(200,copy_task);
			$httpBackend.expect('GET',smarfurl+'resource_type_task_type/?task_type__id='+copy_task.objects[0].id).respond(200,rt3s);
			
			$httpBackend.flush();

			var taskInfo = SmTask.getTaskInfo('copy');
			expect(taskInfo).toEqual({src:rt3s.objects[0],
				dest:rt3s.objects[1],
				init: true,
				name_key: "copy",
				task_type_id: copy_task.objects[0].id});



		}]));
	});

	
		
});