'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import glob
import os
import fnmatch
import datetime
from doit import get_var

def task_show():
    """show all available tasks"""
    return {'actions': ['doit list'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }


def task_update():
    """Updates the environment """
    return {'actions': ['bower install',
                        'bower update',
                        'npm install',
                        'npm update'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }


def task_build():
    """Build the softwares"""
    return {'actions': ['grunt build --target=staging',
                        'grunt compile'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }

env = get_var('env','')
integration = 'grunt integration --target=staging --env=' + env if env != '' else 'grunt integration --target=staging'

def task_integration():
    """Builds the software and runs the unit tests in PhantomJS"""
    return {'actions': [integration,
                        'grunt compile'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }
