angular.module('rpsmarf.postnews',[
	'ui.bootstrap',
	'restangular',
	'rpsmarf.user.service'
])
.controller('PostNewsCtrl', ['$scope', 'Restangular', '$timeout', '$modalInstance','info','SmUser', function($scope, Restangular, $timeout, $modalInstance, info, SmUser){
	/***STANDARD VARIABLES***/
	//Not needed when server authentication is on. 
	var uid = SmUser.getProperty('id');
	/***SCOPE VARIABLES***/
	$scope.showMessage = false;
	$scope.current = {};
	$scope.edit = info.edit;
	if(info.edit){
		$scope.news = {headline: info.headline, body: info.body};
	}
	else{
		$scope.news = {headline: '', body: ''};	
	}
	

	/***SCOPE FUNCTIONS***/
	$scope.close = function(){
		$modalInstance.close();
	};

	$scope.post = function(form){
		var obj;
		if(form.$valid){
			if(!info.edit){
				obj = {headline:$scope.news.headline, body:$scope.news.body, owner: '/scs/user/'+uid+'/', community:info.communityUri};
				Restangular.all('news_item/').post(obj).then(function(response){
					info.posted = true;
					$modalInstance.close();
					//$scope.showMessage = true;
					//$timeout(function(){
					//	$scope.showMessage = false;
					//}, 5000);
				});
			}
			else{
				obj = {headline:$scope.news.headline, body:$scope.news.body};
				Restangular.one('news_item',info.newsId+'/').patch(obj).then(function(response){
					info.posted = true;
					$modalInstance.close();
					//$scope.showMessage = true;
					//$timeout(function(){
					//	$scope.showMessage = false;
					//}, 5000);
				});
			}
				
		}
		else{
			form.$submitted = true;
		}
	};

}]);