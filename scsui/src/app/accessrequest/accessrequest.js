angular.module('rpsmarf.accessrequest', [
	'rpsmarf.user.service',
	'ui.bootstrap',
	'restangular'
]).controller('AccessRequestCtrl', ['$scope', 'object', 'type', '$modalInstance', '$modal', 'SmUser', 'Restangular',
function($scope, object, type, $modalInstance, $modal, SmUser, Restangular){
	
	/***STANDARD VARIABLES***/
	var name = SmUser.getProperty('firstname')+ ' ' + SmUser.getProperty('lastname');
	var uid = SmUser.getProperty('id');
	/***SCOPE VARIABLES***/
	$scope.type = type;
	$scope.typeObject = object;
	$scope.userPerm = object.userPerm;
	$scope.request = {read: false, write: false, execute: false};
	$scope.requestReason = {text: 'Hi, my name is ' + name + ' and I would like access to ' + object.name + ' in order to....'};
	$scope.state = 'requesting';

	/***SCOPE FUNCTIONS***/
	$scope.request = function(){
		var access = '';
		if(type === 'resource'){
			if($scope.request.read){
				access += 'r';
			}
			if($scope.request.write){
				access += 'w';
			}
			if($scope.request.execute){
				access += 'x';
			}
		}
		else{
			access = 'x';
		}
		Restangular.one(type,object.id+'/requestaccess/').get({userid:uid,access:access,reason:$scope.requestReason.text}).then(function(response){
			$scope.state = 'complete';
		});
	};

	$scope.closeMod = function(){
		if($scope.state == 'complete'){
			$modalInstance.close($scope.request);
		}
		else{
			$modalInstance.dismiss();
		}
	};


}]);