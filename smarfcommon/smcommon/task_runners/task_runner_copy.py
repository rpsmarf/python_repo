'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import logging
from smcommon.globals.sm_global_context import smGlobalContextGet
from smcommon.task_runners.task_runner import SmTaskRunnerBase

logger = logging.getLogger(__name__)


class SmTaskRunnerCopy(SmTaskRunnerBase):
    '''
    classdocs
    '''

    def __init__(self, taskId, doneCallback, taskDesc):
        '''
        Constructor
        '''
        SmTaskRunnerBase.__init__(self, taskId, doneCallback, taskDesc)
        
        srcDad = taskDesc.getArgDad(taskDesc.getArgDict(0))
        destDad = taskDesc.getArgDad(taskDesc.getArgDict(1))
        self.copier = smGlobalContextGet().copyController.makeCopier(srcDad, destDad, self.copyDone)
        arg = taskDesc.getArgDict(2)
        if arg is not None:
            self.copier.buffer_size = arg["value"] 
        self.copier.setProgressCallback(self.progressCallback)
        
    def start(self):
        self.setState("running")
        self.copier.start()
    
    def cancel(self):
        self.copier.cancel()

    def progressCallback(self, copier, progress):
        logger.debug("Copy task progress for %s to %s", self.taskId, progress)
        self.announceProgress(progress)
        
    def copyDone(self, copier, status, resultDict):
        logger.debug("Copy operation done, signalling taskDone")
        self.done(status, resultDict)
        