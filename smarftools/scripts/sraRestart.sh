#!/bin/bash

echo "Deleting logs..."
sudo rm -f /var/log/smarf-sra/*
echo "Restarting SRA..."
sudo service smarf-sra restart
echo "Done"
