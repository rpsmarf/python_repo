'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Dec 8, 2014

    @author: rpsmarf
'''
import logging
import threading
import time
from sm_controller.models import SmModelTask, SmModelReservation
from datetime import timedelta
from smarfcontrolserver.init.envvar import scsGetEnvVarFloat,\
    SCS_ENV_VAR_TASK_DELETE_MIN_AGE_IN_SEC, scsGetEnvVarInt,\
    SCS_ENV_VAR_TASK_DELETE_KEEP_COUNT_PER_USER
from smcommon.utils.date_utils import sm_date_now
from django import db

logger = logging.getLogger(__name__)


class ScsTaskAgeOutThread(threading.Thread):
    
    def __init__(self, ageOutObject):
        threading.Thread.__init__(self, name="TaskAgeOut")
        self.ageOutObject = ageOutObject

    def run(self):
        while not self.ageOutObject.quit:
            try:
                db.close_connection()
                time.sleep(self.ageOutObject.pollTimeInSec)
                time_threshold = sm_date_now() - timedelta(seconds=self.ageOutObject.ageThreshold)
                self.ageOutObject.doOneCheck(time_threshold,
                                             self.ageOutObject.limitPerUser)
            except:
                logger.exception("Error during task age out thread")
                time.sleep(10)
            

class ScsTaskAgeOut(object):
    '''
    This class implements a timer to purge tasks which have been hanging around 
    longer than the configured time 
    '''

    def __init__(self, pollTimeInSec):
        '''
        Constructor
        '''
        self.pollTimeInSec = pollTimeInSec
        self.thread = None
        self.quit = False
        self.checkCount = 0
        self.ageThreshold = scsGetEnvVarFloat(SCS_ENV_VAR_TASK_DELETE_MIN_AGE_IN_SEC)
        self.limitPerUser = scsGetEnvVarInt(SCS_ENV_VAR_TASK_DELETE_KEEP_COUNT_PER_USER)
        
    def makeTaskListToDelete(self, taskList, time_threshold, limitPerUser):
        deleteList = []
        perUserCounts = {}
        for task in taskList:
            owner = str(task.owner.id)
            if owner in perUserCounts:
                newCount = perUserCounts[owner] + 1
            else:
                newCount = 1
            if newCount > limitPerUser:
                logger.debug("Task %d exceeds number to always keep, checking end time of %s vs threshold of %s", 
                             task.id, task.end_time, time_threshold)
                if task.end_time < time_threshold:
                    logger.debug("Task %d added to delete list", task.id)
                    deleteList.append(task)                
            perUserCounts[owner] = newCount
            
        return deleteList
    
    def makeResListToDelete(self, resList, time_threshold):
        deleteList = []
        for res in resList:
            if res.end_time < time_threshold:
                logger.debug("Reservation %d added to delete list", res.id)
                deleteList.append(res)                
        return deleteList
    
    def doOneCheck(self, time_threshold, limitPerUser):
        
        self.checkCount += 1
        taskList = SmModelTask.objects.all().order_by('-id')
        logger.debug("Checking for tasks to delete - found %d to consider",
                     len(taskList))
        deleteList = self.makeTaskListToDelete(taskList, time_threshold, limitPerUser)
        for task in deleteList:
            logger.info("Aging out task: %s", str(task))
            task.delete()

        reservationList = SmModelReservation.objects.all().order_by('-id')
        logger.debug("Checking for reservations to delete - found %d to consider",
                     len(reservationList))
        deleteList = self.makeResListToDelete(reservationList, time_threshold)
        for reservation in deleteList:
            logger.info("Aging out reservation: %s", str(reservation))
            reservation.delete()

    def start(self):
        logger.info("Starting TaskAgeOut task")
        self.thread = ScsTaskAgeOutThread(self)
        self.thread.start()
        
    def stop(self):
        ''' 
        This is only used during unit tests which have a small poll time
        so the "join" is OK
        '''
        logger.info("Stopping TaskAgeOut task")
        self.quit = True
        tmp = self.thread.join(2 * self.pollTimeInSec)
        logger.info("Stopped TaskAgeOut task")
        return tmp
        