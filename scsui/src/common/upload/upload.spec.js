describe('upload module', function(){

	beforeEach(module('rpsmarf.upload'));
	beforeEach(module('ui.bootstrap'));
	beforeEach(module('upload/upload.tpl.html'));

	var modalInstance, $modal, $controller, uploadCtrl, $scope, $httpBackend;

	beforeEach(inject(function(_$modal_, _$controller_, $rootScope, _$httpBackend_){
		$modal = _$modal_;
		$controller = _$controller_;
		$scope = $rootScope.$new();
		$httpBackend = _$httpBackend_;

		$httpBackend.when('POST','url').respond(200);
		uploadCtrl = $controller('UploadCtrl', {$scope:$scope, url: 'url', $modal: $modal, $modalInstance: null});

		$modal.open({
			templateUrl: 'upload/upload.tpl.html',
			controller: 'UploadCtrl',
			scope: $scope,
			resolve: {
				url : function(){ return 'url';}
			}
		});

	}));

	it('should verify the file input value', inject(function(){
		$scope.upload();
		expect($scope.formError).toBe(true);
		expect($scope.formErrorNoFile).toBe(true);
		
		
		$scope.resetForm();
		expect($scope.formError).toBe(false);
		expect($scope.formErrorNoFile).toBe(false);
		expect($scope.formErrorCharacters).toBe(false);

		//test backslash
		expect($scope.checkFileName('\\name')).toBe(true);
		expect($scope.formError).toBe(true);
		expect($scope.formErrorCharacters).toBe(true);
		$scope.resetForm();

		//test forward slash
		expect($scope.checkFileName('/name')).toBe(true);
		expect($scope.formError).toBe(true);
		expect($scope.formErrorCharacters).toBe(true);
		$scope.resetForm();

		//test colon
		expect($scope.checkFileName('na:me')).toBe(true);
		expect($scope.formError).toBe(true);
		expect($scope.formErrorCharacters).toBe(true);
		$scope.resetForm();

		//test star
		expect($scope.checkFileName('name*')).toBe(true);
		expect($scope.formError).toBe(true);
		expect($scope.formErrorCharacters).toBe(true);
		$scope.resetForm();

		//test question mark
		expect($scope.checkFileName('name?')).toBe(true);
		expect($scope.formError).toBe(true);
		expect($scope.formErrorCharacters).toBe(true);
		$scope.resetForm();

		//test quotation
		expect($scope.checkFileName('"name"')).toBe(true);
		expect($scope.formError).toBe(true);
		expect($scope.formErrorCharacters).toBe(true);
		$scope.resetForm();

		//test less than
		expect($scope.checkFileName('name<')).toBe(true);
		expect($scope.formError).toBe(true);
		expect($scope.formErrorCharacters).toBe(true);
		$scope.resetForm();

		//test greater than
		expect($scope.checkFileName('name>')).toBe(true);
		expect($scope.formError).toBe(true);
		expect($scope.formErrorCharacters).toBe(true);
		$scope.resetForm();

		//test bitwise OR
		expect($scope.checkFileName('na|me')).toBe(true);
		expect($scope.formError).toBe(true);
		expect($scope.formErrorCharacters).toBe(true);
		$scope.resetForm();
		

	}));


});