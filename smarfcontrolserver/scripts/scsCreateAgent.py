#!/usr/bin/env python 
'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import os
import sys
import datetime
import pytz
sys.path.insert(1, "/opt/smarf-scs/scs")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "smarfcontrolserver.settings")

from sm_controller.models import SmModelAgent, SmModelContainer
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist


def create_agent(guid, user):
    agent_obj = SmModelAgent.objects.create(guid=guid,
                                            owner=user, 
                                            status="down",
                                            description="test description", 
                                            last_change=datetime.datetime.now(pytz.utc))  # @UndefinedVariable
    agent_obj.save()
    container_obj = SmModelContainer.objects.create(agent=agent_obj,
                                            name="container",
                                            description="test description", 
                                            status="down",
                                            containerUrl="local://localhost:1234/tmp/")
    container_obj.save()
    
    
if __name__ == "__main__":
    argv = sys.argv
    if len(argv) == 3:
        guid = argv[1]  
        user = argv[2]      
        if not user.isdigit():
            print('ERROR: Please enter a positive integer for the user\'s id.')
            sys.exit()  
        try:
            u = User.objects.get(id=int(user))
        except ObjectDoesNotExist: 
            print('ERROR: User with id ' + user + ' does not exist!')
            sys.exit() 

    else:
        print('Usage: scsCreateAgent ( guid, owner_id')
        sys.exit(1)
        
    create_agent(guid, u)
