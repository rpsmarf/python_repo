describe( 'Tool Selection page (/tools)', function() {
	beforeEach (module('rpsmarf.config'));
	
	beforeEach(module('rpsmarf.tools'));
	beforeEach(module('ui.router'));
	beforeEach(module('restangular'));
	beforeEach(module('rpsmarf.task.create.service'));
	beforeEach(module('rpsmarf.user.service'));
	
	beforeEach(module('taskprogress/taskprogress.tpl.html'));
	beforeEach(module('tools/tools.tpl.html'));
	beforeEach(module('tools/tools.infopanel.tpl.html'));
	beforeEach(module('breadcrumb/breadcrumb.tpl.html'));
	beforeEach(module('rpsmarf.reservation.service'));
	//$scope, $modalInstance, taskObj, $interval,TASK_STATE_ENUM, modalTitle, interactive, $modal, TASK_STATE_FINISHED, TASK_STATE_RUNNING
	var $scope, $controller, SmTaskCreate, $state, $interval, $modalInstance, $modal, smarfurl, taskObject, $httpBackend, responseObj, prefix = 'src/app/tools/';
	var SmUser, user, $event = {stopPropagation: function(){}}, SmReservation, $rootScope, Restangular, resServiceJson = 'src/app/reservation.service/reservation.service.spec.json', resJson;
	var $scopeInfoPanel;
	//init, starting, prep, running, cleanup and finished
	

	beforeEach(  inject(function(_$controller_, _$rootScope_, _$interval_, _Restangular_, SMARF_URL, _$modal_, _SmTaskCreate_, _$state_, _$httpBackend_, _SmUser_, DEBUG, _SmReservation_){
		$rootScope = _$rootScope_;
		Restangular = _Restangular_;
		smarfurl = SMARF_URL+'/scs/';
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
		SmUser = _SmUser_;
		user = SmUser.getObject();
		SmReservation = _SmReservation_;
	
		user.id = 2;
		user.community = {};
		user.community.name_key = 'rads';
		
		SmTaskCreate = _SmTaskCreate_;
		$interval = _$interval_;
		$httpBackend = _$httpBackend_;
		taskObject = Restangular.one('scs/task','1/');
		$scope = $rootScope.$new();
		//console.log(__html__);
		responseObj = JSON.parse(__html__[prefix+'tools.spec.json']);
		resJson = JSON.parse(__html__[resServiceJson]);
		for(var i=responseObj.objects.length-1; i>=0; i--){
			responseObj.objects[i].ext_favourite = responseObj.objects[i].ext_favourite ? responseObj.objects[i].ext_favourite : false;
		}
		$httpBackend.when('GET',smarfurl+'task_type/?communityFilter='+user.community.name_key+'&extForUser='+user.id+'&sortForUser='+user.id).respond(200,responseObj);
		$httpBackend.when('GET',smarfurl+'user/1/').respond(200,responseObj.owner);
		$state = _$state_;
		$state.go('tools');
		$rootScope.$apply();
		$modal = _$modal_;
		//'$scope', 'Restangular', '$state', 'SmTaskCreate'
		$controller = _$controller_;
		$controller('ToolsCtrl', {$scope: $scope, Restangular:Restangular,$state:$state , SmTaskCreate:SmTaskCreate, SmUser:SmUser, $modal: $modal });

	})) ;

	it('should set the tooltask that is clicked as either active or not active', inject([function(){
		spyOn($scope,'$emit');
		$httpBackend.flush();
		//length-1 since one of the tasks is uiVisible = false
		expect($scope.tooltasks.length).toEqual(responseObj.meta.total_count-1);
		var numActive =0;
		for(var i= $scope.tooltasks.length-1; i>=0; i--){
			if($scope.tooltasks[i].active){
				++numActive;
			}
		}
		expect(numActive).toEqual(0);
		$httpBackend.expect('GET',smarfurl+'user/1/').respond(200,responseObj.owner);
		$scope.clickToolTask($scope.tooltasks[0]);
		$httpBackend.flush();
		expect($scope.tooltasks[0].selected).toBe(true);

		$scope.clickToolTask($scope.tooltasks[0]);

		expect($scope.tooltasks[0].selected).toBe(false);

		$scope.clickToolTask($scope.tooltasks[0]);
		$httpBackend.flush();
		$scope.clickToolTask($scope.tooltasks[1]);
		expect($scope.tooltasks[0].selected).toBe(false);
		expect($scope.tooltasks[1].selected).toBe(true);
		$httpBackend.flush();

		expect($scope.tooltask.owner_info).toEqual({name: 'Henry Tory', email: 'rpsmarf@sce.carleton.ca'});
		expect($scope.$emit).toHaveBeenCalledWith('SmObjectSelectInfoPanel', $scope.tooltasks[1]);
	}]));

	it('should set the tooltask properly when selected (for reservation and non reservation)', inject([function(){
		$httpBackend.flush();
		expect($scope.tooltasks.length).toEqual(responseObj.meta.total_count-1);

		spyOn(SmTaskCreate, 'setTaskType').andCallThrough();

		//spyOn($state, 'go');

		$scope.startToolTask($scope.tooltasks[0], {stopPropagation: function(){}});


		expect(SmTaskCreate.setTaskType).toHaveBeenCalledWith($scope.tooltasks[0],undefined);

		$scope.startToolTask($scope.tooltasks[0], {stopPropagation: function(){}},true);


		expect(SmTaskCreate.setTaskType).toHaveBeenCalledWith($scope.tooltasks[0],true);

	}]));

	it('should filter the tooltasks based on the input into the search', inject([function(){
		$httpBackend.flush();
		expect($scope.tooltasks.length).toEqual(responseObj.meta.total_count-1);

		$scope.filterToolTasks('movies');
		expect($scope.tooltasks.length).toEqual(1);
		expect($scope.tooltasks[0]).toEqual(responseObj.objects[1]);

		$scope.filterToolTasks('windows');
		expect($scope.tooltasks.length).toEqual(2);
		expect($scope.tooltasks).toEqual([responseObj.objects[3], responseObj.objects[4]]);


	}]));

	it('should perform the correct calls when a tool is favourited/unfavourited', inject([function(){
		
		$httpBackend.flush();
		expect($scope.tooltasks.length).toEqual(responseObj.meta.total_count-1);
		//Restangular.all('favourite_task_type/').post({owner:uid, task_type: tooltask.id}).then(function(){
		$httpBackend.expect('POST', smarfurl+'favourite_task_type/',{owner: '/scs/user/'+user.id+'/', task_type: '/scs/task_type/'+$scope.tooltasks[0].id+'/'}).respond(200);
		$scope.favouriteToggle($scope.tooltasks[0],$event);

		$httpBackend.flush();

		$httpBackend.expect('DELETE', smarfurl+'favourite_task_type/?owner__id='+user.id+'&task_type__id='+$scope.tooltasks[0].id).respond(200);
		$scope.favouriteToggle($scope.tooltasks[0], $event);

		$httpBackend.flush();
		


	}]));

	it('should open the notes editor with the right parameters', inject([function(){
		$httpBackend.flush();
		expect($scope.tooltasks.length).toEqual(responseObj.meta.total_count-1);

		spyOn($modal, 'open');
		$scope.tooltasks[0].ext_note = "my note";
		
		$scope.openNotesEditor($scope.tooltasks[0],$event);

		var args = $modal.open.mostRecentCall.args;

		expect(args[0].controller).toBe('ExtendedInfoCtrl');
		expect(args[0].templateUrl).toBe('extendedinfo/extendedinfo.tpl.html');
		var tagsObj = args[0].resolve.tagsObject();
		var noteObj = args[0].resolve.noteObject();

		expect(tagsObj).toBe(null);
		expect(noteObj).toEqual({typeName: 'task_type',typeId: $scope.tooltasks[0].id, ownerId: user.id, exists: true, noteText: $scope.tooltasks[0].ext_note, typeObject: $scope.tooltasks[0]});
		
	}]));

	describe('tools.reservation state functions', function(){

		it('should make the proper calls when in the tools.reservation state (call to SmReservation.getTaskTypes())', inject(function(){
			$httpBackend.resetExpectations();
			$httpBackend.expect('GET', smarfurl+'resource/6/').respond(200, resJson.resource);
			$httpBackend.expect('GET', smarfurl+'resource_type_task_type/?resource_type__id=3').respond(200,resJson.rt3s);
			$httpBackend.expect('GET', smarfurl+'task_type/5/?extForUser=2&sortForUser=2').respond(200,resJson.task_types);

			SmReservation.setReservation(resJson.reservations[0]);

			spyOn(SmReservation, 'getTaskTypes').andCallThrough();
			$state.go('tools.reservation');
			$rootScope.$apply();
			$controller('ToolsCtrl', {$scope: $scope, Restangular:Restangular,$state:$state , SmTaskCreate:SmTaskCreate, SmUser:SmUser, $modal: $modal, SmReservation:SmReservation });
			expect(SmReservation.getTaskTypes).toHaveBeenCalled();
			$httpBackend.flush();
			expect($scope.tooltasks[0].plain()).toEqual(resJson.task_types);

		}));
	});

	describe('Info panel controller', function(){
		beforeEach(inject(function($rootScope){
			$scopeInfoPanel = $rootScope.$new();
			user.id=1;
			user.community = {name_key:'rads'};
			user.first_name = 'Henry';
			user.last_name = 'Tory';
			SmTaskCreate.reset();
			$httpBackend.expect('GET',smarfurl+'task_type/?communityFilter='+user.community.name_key+'&extForUser='+user.id+'&sortForUser='+user.id).respond(200,responseObj);
			$controller('ToolsCtrl', {$scope: $scope, Restangular:Restangular,$state:$state , SmTaskCreate:SmTaskCreate, SmUser:SmUser, $modal: $modal });
			$controller('ToolsInfoPanelCtrl', {$scope: $scopeInfoPanel, Restangular:Restangular,$state:$state , SmTaskCreate:SmTaskCreate, SmUser:SmUser, $modal: $modal });
			$httpBackend.flush();

			//expect($scope.userState).toEqual(USER_STATE.browse);
			$scopeInfoPanel.$broadcast('SmInfoPanelUpdate',$scope.tooltasks[1] );
		}));
		it('should allow resource owners to grant and revoke permissions', inject(function(){

			var accReqObj = angular.copy(responseObj.access_requests.objects[0]);
			accReqObj.requester_info = {name:'Bjorn Borg', id:'3'};

			
			$httpBackend.expect('GET', smarfurl+'task_type/2/getperm/').respond(200,responseObj.perms);
			$httpBackend.expect('GET',smarfurl+'request/?target=/scs/task_type/2/').respond(200, responseObj.access_requests);
			$httpBackend.expect('GET', smarfurl+'user/2/').respond(200, responseObj.users_info[0]);
			$httpBackend.expect('GET', smarfurl+'user/3/').respond(200, responseObj.users_info[1]);
			$httpBackend.expect('GET', smarfurl+'user/3/').respond(200, responseObj.users_info[1]);

			
			$scopeInfoPanel.getPermissions($scopeInfoPanel.tooltask);
			$httpBackend.flush();

			expect($scopeInfoPanel.tooltask.hasAccReq).toBe(true);
			var uar = $scopeInfoPanel.userAccessRequests;

			expect(uar[0]).toEqual(accReqObj);

			var numUsersWithPerm = 2;
			//$scopeInfoPanel.tooltask.users_perm[0].selected = true;
			//$scopeInfoPanel.tooltask.users_perm[1].selected = true;
			//$scopeInfoPanel.selectUserToggle($scopeInfoPanel.tooltask.users_perm[0],$scopeInfoPanel.tooltask);
			//$scopeInfoPanel.selectUserToggle($scopeInfoPanel.tooltask.users_perm[1],$scopeInfoPanel.tooltask);
			//expect($scopeInfoPanel.numSelectedUsers).toBe(2);
			$httpBackend.expect('GET', smarfurl+'task_type/2/setperm/?action=remove&notify=true&perm=x&user='+encodeURIComponent('/scs/user/2/')).respond(200);
			$scopeInfoPanel.setUsersPermission($scopeInfoPanel.tooltask, 'remove', $scopeInfoPanel.tooltask.users_perm[0],0);
			$httpBackend.flush();

			$httpBackend.expect('GET', smarfurl+'task_type/2/setperm/?action=remove&notify=true&perm=x&user='+encodeURIComponent('/scs/user/3/')).respond(200);
			$scopeInfoPanel.setUsersPermission($scopeInfoPanel.tooltask, 'remove', $scopeInfoPanel.tooltask.users_perm[0],0);
			$httpBackend.flush();

			expect($scopeInfoPanel.tooltask.users_perm.length).toBe(numUsersWithPerm-2);

			//grant access to user
			$httpBackend.expect('GET', smarfurl+'task_type/2/setperm/?action=assign&notify=true&perm=x&user='+encodeURIComponent('/scs/user/3/')).respond(200);
			$httpBackend.expect('DELETE', smarfurl+'request/1/').respond(200);
			$httpBackend.expect('GET', smarfurl+'user/3/').respond(200, responseObj.users_info[1]);
			$scopeInfoPanel.grantAccess(uar[0],0);
			$httpBackend.flush();
			expect($scopeInfoPanel.userAccessRequests.length).toBe(0);
			expect($scopeInfoPanel.tooltask.users_perm[0].execute).toBeTruthy();
			expect($scopeInfoPanel.tooltask.users_perm.length).toBe(1);

			//deny access
			$httpBackend.expect('DELETE', smarfurl+'request/21/?deny=true').respond(200);
			$scopeInfoPanel.userAccessRequests[0] = {};
			$scopeInfoPanel.denyAccess({id:'21'},0);
			$httpBackend.flush();
		}));

		it('should get the usage information for the selected task_type', inject(function(){

			$httpBackend.expect('GET', smarfurl+'task_type/2/getusage/?user='+user.id).respond(200,responseObj.usage_summary);
			$httpBackend.expect('GET', smarfurl+'task_type/2/getusers/').respond(200,responseObj.usage_users);

			$scopeInfoPanel.getUsageInfo($scopeInfoPanel.tooltask);
			$httpBackend.flush();
			//expect($scope.tooltask.usage_summary).toEqual([{user_name: "All Users", "m": 42,"w": 14,"y": 52},{user_name:"You","m": 30,"w": 7,"y": 40}]);
			expect($scopeInfoPanel.tooltask.usage_users).toEqual([{user_name: 'Dr Henry Tory', email: 'rpsmarf@sce.carleton.ca', use_count: 30, last_use: jasmine.any(String)}]);
		}));
	});
	
	
  
});

