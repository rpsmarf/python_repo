'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 28, 2015

    @author: rpsmarf
'''

import logging
from django.http.response import HttpResponse
import json
from urllib.parse import urlparse
from smcommon.exceptions.sm_exc import SmException
from tastypie.exceptions import ImmediateHttpResponse

logger = logging.getLogger(__name__)

scsGuacamoleTokenMaker = None


_smApiExtractorCallback = None


def scsApiExtractorCallbackSet(obj):
    global _smApiExtractorCallback
    _smApiExtractorCallback = obj
 
    
def scsApiExtractorCallbackGet():
    global _smApiExtractorCallback
    return _smApiExtractorCallback
 
    
class HttpOk(HttpResponse):
    status_code = 200


class HttpPreconditionFailed412(HttpResponse):
    status_code = 412


class HttpConflict409(HttpResponse):
    status_code = 409


class HttpError422(HttpResponse):

    def __init__(self, reason):
        super().__init__(json.dumps({"code": "EOTHER",
                                     "description": reason}))
        self.status_code = 422
 

class HttpError522(HttpResponse):

    def __init__(self, reasonJson):
        super().__init__(reasonJson)
        self.status_code = 522
 

def scsCheckOkToRun(apiObject, modelObj, request, access):
    if request is None:
        return  # We are starting a deferred task     
    if 'x' in access:
        apiObject._meta.authorization.check_run(apiObject, modelObj, request)
    if 'r' in access:
        apiObject._meta.authorization.check_read_data(apiObject, modelObj, request)
    if 'w' in access:
        apiObject._meta.authorization.check_write_data(apiObject, modelObj, request)


def scsMakeExecInfo(computeResource, cloud_server=None):
    # This resource defines where to run this task so we get its URL
    computeContainerUrl = urlparse(computeResource.container.containerUrl) # Set up execInfo in task desc
    execInfo = {}
    execInfo["protocol"] = computeContainerUrl.scheme
    if ":" in computeContainerUrl.netloc:
        tokens = computeContainerUrl.netloc.split(":")
        execInfo["host"] = tokens[0]
        execInfo["port"] = int(tokens[1])
    else:
        execInfo["host"] = computeContainerUrl.netloc
        execInfo["port"] = -1

    # If there is a cloud server use the host from the cloud server
    if cloud_server is not None:
        execInfo["host"] = cloud_server.host
    
    if computeResource.parametersJson != "":
        resourceParamsDict = json.loads(computeResource.parametersJson)
        execInfo["user"] = resourceParamsDict.get("user")
        execInfo["password"] = resourceParamsDict.get("password")
        execInfo["tempDataFolder"] = resourceParamsDict.get("tempDataFolder")
        execInfo["keyfile"] = resourceParamsDict.get("keyfile")            
    return execInfo


def smRaiseFsOpException(exc, opName, pathName):
    if isinstance(exc, SmException):
        detail = exc.toDict()
        httpCode = exc.httpCodeToReturn
    else:
        logger.error("Other exception for %s on %s", opName, pathName, exc_info=True)
        detail = {"code": "unspecified",
                  "description": type(exc).__name__ + ":" + str(exc)}
        httpCode = 400
    
    detail["operation"] = opName
    detail["apiPathName"] = pathName
    info = json.dumps(detail)
    resp = HttpResponse(info, content_type="application/json")
    resp.status_code = httpCode
    logger.info("Error in filesystem operation: %s", info)
    raise ImmediateHttpResponse(resp)
    


