/**
 * Tests sit right alongside the file they are testing, which is more intuitive
 * and portable than separating `src` and `test` directories. Additionally, the
 * build process will exclude all `.spec.js` files from the build
 * automatically.
 */
describe( 'drag and drop features for foldernav', function() {
	beforeEach( module( 'rpsmarf.foldernav') );

	beforeEach(module('foldernav/foldernav.tpl.html'));
	var $scope, $element, entries,$timeout, $controller;

	var homeSubEntries = [{basename:'documents/', path:'/home/documents/', isDir:true},{basename:'pictures/', path:'/home/pictures/',isDir:true}, {basename:'myfile', path:'/home/documents/myfile',isDir:false}],
		documentsSubEntries = [{basename:'report1.doc', path:'/home/documents/report1.doc',isDir:false},{basename:'paper.pdf', path:'/home/documents/paper.pdf',isDir:false}],
		dataSubEntries = [{basename:'bridge.dat', path:'/home/data/bridge.dat',isDir:false},{basename:'machinery.dat', path:'/home/data/machinery.dat',isDir:false}, {basename:'data.dat', path:'/home/data/data.dat',isDir:false}];
		srcEntries = [{basename:'home/', path:'/home/',isDir:true},{basename:'documents/', path:'/home/documents/',isDir:true},{basename:'data/', path:'/data/',isDir:true},{basename:'file.txt', path:'/file.txt',isDir:false}];
		dstEntries = [{basename:'report1.doc',isDir:false},{basename:'paper.pdf', isDir:false},{basename:'documents/', isDir:true}];
	
		
	beforeEach(inject(function($rootScope, $compile, _$timeout_, _$controller_){
		
		$scope = $rootScope.$new();
		$element = angular.element('<div sm-drag-drop-ctrl="true">'+
									'<foldernav resource="resource1" entries="srcEntries" ls="" selectentry="" selectall="" selectedentries="" smondrop="drop(srcEntry, srcResource, dstResource, dstEntry)"></foldernav>'+
									'<foldernav resource="resource2" entries="dstEntries" ls="" selectentry="" selectall="" selectedentries="" smondrop="drop(srcEntry, srcResource, dstResource, dstEntry)"></foldernav>'+
									'</div>'
									);
		
		
		$timeout = _$timeout_;
		$scope.srcEntries = srcEntries;
		$scope.dstEntries = dstEntries;
		$scope.resource1 = {name:'r1'};
		$scope.resource2 = {name:'r2'};
		

		

		$element = $compile($element)($scope);


		$scope.$digest();

		//angular.element(document.body).append(element);
		
		//scope = element.isolateScope();
	}));

	it('should follow the correct flow for dragstart -> dragover -> dragleave -> dragend', function(){
		var dragEntry = $element.find('foldernav:eq(0)').find('div.entry-container:eq(0)');
		//console.log(dragEntry[0]);
		var srcDragDropArea = $element.find('foldernav:eq(0) .foldernav-container');
		var dstDragDropArea = $element.find('foldernav:eq(1) .foldernav-container');
		var dstDropEntry = $element.find('foldernav:eq(1) div.entry-container:eq(2)');
		spyOn($scope, '$broadcast').andCallThrough();
		dragEntry.trigger('dragstart');
		expect($scope.$broadcast).toHaveBeenCalled();

		expect(srcDragDropArea.hasClass('sm-drop-area')).toBe(false);
		expect(dstDragDropArea.hasClass('sm-drop-area')).toBe(true);

		dstDropEntry.trigger('dragover');

		expect(dstDropEntry.hasClass('sm-drop-allow')).toBe(true);
		expect($element.find('.sm-drop-allow').length).toEqual(1);

		dstDropEntry.trigger('dragleave');

		expect(dstDropEntry.hasClass('sm-drop-allow')).toBe(false);
		expect($element.find('.sm-drop-allow').length).toEqual(0);

		dragEntry.trigger('dragend');
		expect($scope.$broadcast).toHaveBeenCalled();


	});

	it('should perform the proper drop functions', function(){
		$scope.drop = function(srcEntry, srcResource, dstResource, dstEntry){
			expect(srcEntry).toEqual($scope.srcEntries[0]);
			expect(srcResource).toEqual($scope.resource1);
			expect(dstResource).toEqual($scope.resource2);
			expect(dstEntry).toEqual($scope.dstEntries[2]);

			$scope.$broadcast('sm-drop-success');

		};

		var dragEntry = $element.find('foldernav:eq(0)').find('div.entry-container:eq(0)');
		var dstDropEntry = $element.find('foldernav:eq(1) div.entry-container:eq(2)');

		
		dragEntry.trigger('dragstart');
		dstDropEntry.trigger('drop');
		expect($element.find('.sm-drop-area').length).toEqual(0);
		expect($element.find('.sm-drop-allow').length).toEqual(0);
	

	});
  
});

