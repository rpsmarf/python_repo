
How to setup the necessary virtual environment on Windows
=========================================================

This assumes that python 3.3+ is already installed.  Windows is assumed, since
this example deals with creating and using windows services.


1. Create the python virtual environment

    C:\...\RPSMARF> C:\Python34\python -m venv env


2. Switch to the virtual environment.

    C:\...\RPSMARF> env\scripts\activate.bat


3a. Install required packages (simple)

    C:\...\RPSMARF> pip install py2exe


3b. Install pywin32 (less simple)

    i) Download pywin32 installer appropriate to your architecture and python version.
        - http://sourceforge.net/projects/pywin32/files/
    ii) Copy installer to your RPSMARF directory.
    iii) C:\...\RPSMARF> easy_install pywin32....exe


3c. Install PyQt4 & Sip (even less simple)
    i) Download appropriate PyQt4 installer
        - http://www.riverbankcomputing.com/software/pyqt/download
    ii) Install into global Python 3.3+ directory
    iii) Copy C:\Python3x\Lib\site-packages\PyQt4 to C:\...\RPSMARF\env\Lib\site-packages
    iv) Copy C:\Python3x\Lib\site-packages\sip.pyd to C:\...\RPSMARF\env\Lib\site-packages



At this point, you should be able to execute the setup script:

    C:\...\RPSMARF> python setup.py

This will create a distribution directory (dist) containing everything necessary to run the service and gui
executables.  A program, such as InnoSetup, can be used to package this directory into a windows installer.