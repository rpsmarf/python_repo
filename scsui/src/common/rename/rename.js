angular.module('rpsmarf.rename', [
	'ui.bootstrap'
])
.controller('RenameCtrl', ['$scope', '$modalInstance', 'originalName', function($scope, $modalInstance, originalName){

	/***STANDARD VARIABLES***/
	var patt = /(\\|\/|:|\*|\?|\"|<|>|\|)/;
	var index, sub;

	/***SCOPE VARIABLES***/
	$scope.entry = {name:''};
	$scope.formError = false;
	$scope.hasSuffix = false;
	$scope.keepSuffix = {value: true};
	$scope.suffix = null;

	/***SCOPE FUNCTIONS***/
	$scope.set = function(){
		
		if($scope.entry.name.length < 1 || patt.test($scope.entry.name)){
			$scope.formError = true;
			return;
		}
		if($scope.hasSuffix && $scope.keepSuffix.value){
			$scope.entry.name = $scope.entry.name+$scope.suffix;
			
		}
				
		$modalInstance.close($scope.entry);
	};

	$scope.cancel = function(){
		$modalInstance.dismiss('cancel');
	};

	/***EXE***/
	index = originalName.lastIndexOf('.');
	if(index >= 0){
		sub = originalName.substring(index+1);
		if(sub.length <= 4){
			$scope.hasSuffix = true;
			$scope.suffix = '.'+sub;
		}
	}
	


}]);