'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jul 28, 2015

    @author: rpsmarf
'''

import logging
from smcommon.utils.date_utils import sm_date_now, smMakeDateFromString,\
    smMakeDateStringForJson
from datetime import timedelta
from actstream.models import target_stream
from sm_controller.models import SmModelUserSetting

logger = logging.getLogger(__name__)


def scsGetUsageForObject(paramInfo, obj):
    total_sought = paramInfo.getParam("total")
    periods = paramInfo.getParam("periods")
    userid_sought = paramInfo.getParam("user")
    stream = target_stream(obj)
    resp = {}
    now = sm_date_now()
    week_ago = now - timedelta(days=7)
    month_ago = now - timedelta(days=30)
    year_ago = now - timedelta(days=365)
    user_week = 0
    total_week = 0
    user_month = 0
    total_month = 0
    user_year = 0
    total_year = 0
    for item in stream:
        if item.verb == "run_end":
            continue
        if item.timestamp > week_ago:
            if item.actor.id == userid_sought:
                user_week += 1
            total_week += 1
        if item.timestamp > month_ago:
            if item.actor.id == userid_sought:
                user_month += 1
            total_month += 1
        if item.timestamp > year_ago:
            if item.actor.id == userid_sought:
                user_year += 1
            total_year += 1
    
    if userid_sought != -1:
        userDict = {}
        if "w" in periods:
            userDict["w"] = user_week
        if "m" in periods:
            userDict["m"] = user_month
        if "y" in periods:
            userDict["y"] = user_year
        resp["user"] = userDict
    if total_sought:
        totalDict = {}
        if "w" in periods:
            totalDict["w"] = total_week
        if "m" in periods:
            totalDict["m"] = total_month
        if "y" in periods:
            totalDict["y"] = total_year
        resp["total"] = totalDict
    return resp


def scsGetUsersForObject(paramInfo, obj):
    duration = paramInfo.getParam("duration")
    endTime_str = paramInfo.getParam("end_time")
    if endTime_str == "":
        maxTime = sm_date_now()
    else:
        maxTime = smMakeDateFromString(endTime_str)
    minTime = maxTime - timedelta(days=duration)
    
    stream = target_stream(obj)
    resp = {}
    for item in stream:
        if item.verb == "run_end":
            continue
        if (item.timestamp > minTime) and (item.timestamp <= maxTime):
            userRec = resp.get(item.actor.username)
            if userRec is None:
                userRec = {"first_name": item.actor.first_name,
                           "last_name": item.actor.last_name,
                           "email": item.actor.email,
                           "use_count": 1,
                           "last_use": item.timestamp}
                userSettingList = SmModelUserSetting.objects.filter(owner=item.actor)
                if userSettingList.count() > 0:
                    userRec["title"] = userSettingList[0].title
                resp[item.actor.username] = userRec
            else:
                userRec["use_count"] += 1
                if item.timestamp > userRec["last_use"]:
                    userRec["last_use"] = item.timestamp
                     
    # Convert dates to strings
    for userRec in resp.values():
        d = userRec["last_use"]
        userRec["last_use"] = smMakeDateStringForJson(d)
    return resp


