angular.module( 'rpsmarf.taskcommservice', [
 
])
.service('taskInfo', function(){
	var taskInfo = {task_type: null, resource_rt3_map: null, parameters: null};

	var getTaskType = function(){
		return taskInfo.task_type;
	};

	var getResourceRt3Map = function(){
		return taskInfo.resource_rt3_map;
	};

	var getParameters = function(){
		return taskInfo.parameters;
	};

	var setTaskType = function(tt){
		taskInfo.task_type = tt;
	};

	var setResourceRt3Map = function(map){
		taskInfo.resource_rt3_map = map;
	};

	var setParameters = function(params){
		taskInfo.parameters = params;
	};

	

	return {getTaskType: getTaskType, getResourceRt3Map: getResourceRt3Map, getParameters:getParameters, setTaskType: setTaskType, setResourceRt3Map: setResourceRt3Map,setParameters: setParameters};
})
;