/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'rpsmarf.taskcreate', [
  'rpsmarf.config',
  'restangular',
  'ui.router',
  'rpsmarf.foldernav',
  'rpsmarf.foldernavpane',
  'ui.bootstrap',
  'ngSanitize',
  'rpsmarf.taskcommservice',
  'rpsmarf.filesetpane'
  
])
/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(['$stateProvider', 'RestangularProvider', 'SMARF_URL',function ( $stateProvider, RestangularProvider, SMARF_URL ) {
  $stateProvider.state( 'taskselect', {
    url: '/task',
    views: {
      "main": {
        controller: 'TaskSelectCtrl',
        templateUrl: 'taskcreate/taskcreate.tpl.html'
      }
    },
    data:{ pageTitle: 'Start a task' },
    resolve: {"taskInfo":"taskInfo"}
  })
  .state('taskresourceselect', {
    url: '/task/:taskTypeId/resources',
    views: {
      "main": {
        controller: 'TaskResourceSelectCtrl',
        templateUrl: 'taskcreate/taskResourceSelect.tpl.html'
      }

    },
    data:{ pageTitle: 'Start a task' },
    resolve: {"taskInfo":"taskInfo"}
  })
  .state('taskconfigure', {
    url: '/task/:taskTypeId/resources/?resourcesIds',
    views: {
      "main": {
        controller: 'TaskConfigureCtrl',
        templateUrl: 'taskcreate/taskConfigure.tpl.html'
      }
    },
    data:{ pageTitle: 'Start a task' },
    resolve: {"taskInfo":"taskInfo"}
  });
}])
.filter('normalize', function(){
  return function(input){
    
    var obj = null;
    var newInput = [];
    
    for(var key in input){
      if(key === '$$hashKey'){
        continue;
      }
      newInput.push(input[key]);
    }
    
    return newInput;
  };

})
.filter('flatten', function(){
  return function(input){
    
    var obj = null;
    var newInput = [];
    
    for(var key in input){
     
      newInput.push(input[key]);
    }
    
    return newInput;
  };

})

/**
 * And of course we define a controller for our route.
 */
.controller( 'TaskSelectCtrl', ['$scope', 'Restangular','$rootScope', '$modal', '$state', 'taskInfo',  function ( $scope, Restangular, $rootScope, $modal, $state, taskInfo) {


  $scope.tasktypes = [];
  $scope.taskSelected = false;
  $scope.rttts = [];
  
  var tasktypeApi = Restangular.all('task_type/');
  var restytastyApi = Restangular.all('resource_type_task_type/');
  var resourcetypeApi = Restangular.all('resource_type/');

  tasktypeApi.get("").then(function(response){
    $scope.tasktypes = response.objects;
  });

  $scope.selectTask = function($event, id){
    
    $event.stopPropagation();
    //$event.preventDefault();
    var target = angular.element($event.target);
    angular.element('input[type="checkbox"]').not(target).prop("checked","");
    if(target.prop("checked") !== ""){
      $scope.taskSelected = true;
     
      angular.element('#taskcreation').slideDown();
      //Restangular.configuration.requestSuffix = '';
      
    }
    else{
      
      $scope.taskSelected = false;
    }

  };

  $scope.createTaskType = function(tasktype){
    taskInfo.setTaskType(tasktype);
    $state.go('taskresourceselect',{taskTypeId: tasktype.id});
  };
  
}])

.controller('TaskResourceSelectCtrl',['$scope', 'Restangular', '$modal', '$state', '$location', '$anchorScroll', 'taskInfo',  function ( $scope, Restangular, $modal,  $state, $location, $anchorScroll, taskInfo) {

  /***VARIABLES***/
  /*Scope*/
  $scope.resources = [];
  $scope.taskroles = {};
  $scope.resourceTypes = {};
  $scope.selResourceTypes = {};
  $scope.taskParams = {taskResources:{}, args:{}, resourceRt3Map:{}};
  //$scope.selectedResources = {};

  /*Standard*/
  var taskTypeId = $state.params.taskTypeId;
  var rtts;
  Restangular.configuration.encodeIds = false;
  var restytastyApi = Restangular.all('resource_type_task_type/');
  var resourcetypeApi = Restangular.all('resource_type/');
  var resourceApi = Restangular.all('resource/');
  var tasktypeApi = Restangular.one('task_type',taskTypeId+'/');
  var tasktypeObj = taskInfo.getTaskType();
  var resourceRt3Map = {};
 
  /***SCOPE FUNCTIONS***/
  $scope.scrollTo = function(href){
    $location.hash(href);
    $anchorScroll();
  };
  $scope.numForRole = function(role){
   
    if($scope.taskroles[role]){
      return $scope.taskroles[role].num + " " + role + " resources" + " ("+($scope.taskroles[role].num-$scope.selResourceTypes[role])+" remaining)";
    }
    
    
  };

  $scope.selectResource = function(resource, role, $event, role_id){

    var lt = $scope.selResourceTypes[role] +1 <= $scope.taskroles[role].num;
    
    var index = -1;
    var resourceSet = $scope.taskParams.taskResources[role].resourceId;
    var inputId;
    
    

    if( resourceSet){
      $event.preventDefault();
      inputId = $scope.taskParams.taskResources[role].inputId;
      if( angular.element($event.target).hasClass('selected')){
        angular.element($event.target).removeClass('selected');
        $scope.taskParams.taskResources[role].resourceId = null;
        $scope.taskParams.taskResources[role].rtttId = null;
        $scope.taskParams.resourceRt3Map[resource.id] = null;
        if($scope.taskParams.args[inputId]){
          $scope.taskParams.args[inputId].resource = null;
        }
        $scope.selResourceTypes[role]--;

      }
      else{
        angular.element($event.target).closest('tbody').find('button').not($event.target).removeClass('selected');
        angular.element($event.target).addClass('selected');
        
        $scope.taskParams.taskResources[role].resourceId = resource.id;
        $scope.taskParams.taskResources[role].rtttId = role_id;
        $scope.taskParams.resourceRt3Map[resource.id] = role_id;
        if($scope.taskParams.args[inputId]){
          $scope.taskParams.args[inputId].resource = resource;
          
        }
        //resourceRt3Map[resource.id] = undefined;
        //taskParams.taskResources[role].resourceId = null;
        
        
      }

      return;
    }
    if(!resourceSet ){
      $scope.selResourceTypes[role]++;
      inputId = $scope.taskParams.taskResources[role].inputId;
      $scope.taskParams.taskResources[role].resourceId = resource.id;
      $scope.taskParams.taskResources[role].rtttId = role_id;
      $scope.taskParams.resourceRt3Map[resource.id] = role_id;
      if($scope.taskParams.args[inputId]){
        $scope.taskParams.args[inputId].resource = resource;
      }
      //resourceRt3Map[resource.id] = {rtttId:role_id,rtttRole: role, resource:{id:resource.id,name:resource.name, description:resource.description}};
      //taskParams.taskResources[role].resourceId = resource.id;
      angular.element($event.target).addClass('selected');
    }
    // else if( lt && angular.element($event.target).hasClass('selected')){
    //   $event.preventDefault();
    //   angular.element($event.target).removeClass('selected');
    // }else{
    //   $event.preventDefault();
    //   alert('You have already selected the required number of resources! Please unselect a resource.');
    // }
    
  };

  $scope.configureTask = function(){

    if(!validateResourceSelection()){
      alert("Please select all required resources.");
      return;
    }
    var i;
    var key;
    
    var resourcesIds = "";
    var resource;
    var roleResources;
    // for(i=0; i< $scope.selectedResources.length; i++)
    // {
    //   resource = $scope.selectedResources[i];
    //   resourcesIds += resource.id+"-"; 
    // }
    for(key in $scope.taskParams.taskResources){

      resourcesIds += $scope.taskParams.taskResources[key].resourceId + "-";
      
    }

    resourcesIds = resourcesIds.substring(0,resourcesIds.length-1);

  
    //taskInfo.setResourceRt3Map(resourceRt3Map);
    taskInfo.setParameters($scope.taskParams);
    $state.go('taskconfigure',{resourcesIds:resourcesIds, taskTypeId: taskTypeId});
  };

  
  /***HELPER FUNCTIONS***/
  /*Expressions*/
  var inArrayIndex =function(array, propertyName, propertyVal){
    var item;
    for( var i=array.length-1; i>=0; i--){
      item = array[i];
      if(item[propertyName] && item[propertyName] == propertyVal){
        return i;
      }
    }

    return -1;
    
  };


  var pushResources = function(role, resTypeId){

    resourcetypeApi.get(resTypeId+"/").then(function(response){

      if($scope.resourceTypes[role] === undefined){
        $scope.resourceTypes[role] = [];
        //$scope.selResourceTypes[role] = [];
        //$scope.resourceTypes[role][response.nature] = [];
        $scope.selResourceTypes[role] = 0;
      }
      
      
      resourceApi.get("?resource_type__id="+response.id).then(function(resourceResponse){
       
       
        $scope.resources.push.apply($scope.resources,resourceResponse.objects);
        $scope.resourceTypes[role].push.apply($scope.resourceTypes[role],resourceResponse.objects);
        
        
        
      });
      
    });

    
  };

  var validateResourceSelection = function(){
    var trs = $scope.taskParams.taskResources;//$scope.taskroles;

    for(var key in trs){
      if(!trs[key].resourceId){
        return false;
      }
    }

    return true;
    
  };

  var setRt3s = function(object){
    taskInfo.setTaskType(object);
    taskTypeObj = object;
    restytastyApi.get("?task_type__id="+taskTypeId).then(function(response){
      rttts = response.objects;
      var rttt = null;
      var resTypeId = null;
      var split = [];
      var role;
      var object = {};
      //var n
      for(var i=0; i< rttts.length;i++){
        rttt = rttts[i];
        role =rttt.role;
        split = rttt.resource_type.split("/");
        
        resTypeId = split[3];

        if(!$scope.taskroles[rttt.role]) {
          object = {num: 1, name:rttt.role, rtttId:rttt.id};
          $scope.taskroles[rttt.role] = object;
        }
        else{
          $scope.taskroles[rttt.role].num= $scope.taskroles[rttt.role].num +1;
        }
        
        $scope.taskParams.taskResources[rttt.role] ={resourceId: null, isInput: false, inputId: null, rtttId:null, nature:rttt.nature};
        pushResources(rttt.role,resTypeId);
        
        
      }
      var configJson = JSON.parse(taskTypeObj.configurationJson);
      var args = configJson.args;
      var arg;
      var argObj;
      for(i=args.length-1; i>=0;i--){
        arg = args[i];
        if((!arg.value || arg.value === "" || arg.type === "resource") && $scope.taskParams.taskResources[arg.role] && (!$scope.taskParams.taskResources[arg.role].nature || $scope.taskParams.taskResources[arg.role].nature.indexOf('stream') == -1)){
          $scope.taskParams.args[arg.input_id] = {type:arg.type,input_id: arg.input_id,name:arg.name,description:arg.description,role:arg.role, resource:null};
          $scope.taskParams.taskResources[arg.role].isInput =true;
          $scope.taskParams.taskResources[arg.role].inputId = arg.input_id;
          //taskParams.resourceArgsMap[]
          //$scope.resources[arg.input_id] = resource.resource;
          
        }
      }


        
      });
  };

  /***EXE***/
  if(tasktypeObj == null){
    tasktypeApi.get().then(function(object){
      
      setRt3s(object);
  
      
    });
  }
  else{
    setRt3s(tasktypeObj);
  }
  

  
}])

.controller('TaskConfigureCtrl', [ '$scope', '$q', 'Restangular', '$modal', '$state', '$location', '$anchorScroll', '$interval', '$rootScope', 'taskInfo', 'SMARF_URL', 'normalizeFilter', 
                          function ( $scope, $q, Restangular, $modal, $state, $location, $anchorScroll, $interval, $rootScope, taskInfo, SMARF_URL, normalizeFilter){


  /***VARIABLES***/
  /*Scope*/
  $scope.resources = {};
  $scope.resourcesNature = {};
  $scope.args = [];
  $scope.modalInstance = null;

  /*Standard*/
  Restangular.configuration.encodeIds = false;
  var restytastyApi = Restangular.all('resource_type_task_type/');
  var resourcetypeApi = Restangular.all('resource_type/');
  var resourceApi = Restangular.all('resource/');
  var tasktypeApi = Restangular.one('task_type',taskTypeId+'/');
  var taskApi = Restangular.all('task/');
  var taskresourceApi = Restangular.all('task_resource/');
  var modalTransferProgInst;
  var taskTypeId = $state.params.taskTypeId;
  var resourcesIds = $state.params.resourcesIds.replace(/-/g,';');
  var taskTypeObj;
  var taskParams; //resourceRt3Map
  var INPUT_TYPE_VALUE_MAP = {"resource": "path", "string": "value"};
  

  /*** SCOPE FUNCTIONS ***/
  $scope.levelUp = function(resource){
    if(resource.levels.length >1){
      
      resource.levels.pop();
      len = resource.levels.length;
      resource.entries = resource.levels[len-1];
      var patt = /\/[A-Za-z0-9\._\-]+\/$/i;

      var match = patt.exec(resource.path);
      resource.path = resource.path.substring(0,resource.path.length-(match[0].length-1));
    }
    else{
      alert('Already at root!');
    }
  };

  $scope.drop = function(srce, dste, entryObj, srcResource, dstResource){

    //Do error checking here!

    $scope.modalInstance = $modal.open({
      templateUrl: 'taskcreate/transferConfirmModal.tpl.html',
      controller: 'TransferConfModalCtrl',
      resolve: {
          srcPath: function(){return srcResource.name+":"+srcResource.path;},
          dstPath: function(){return dstResource.name+':'+dstResource.path;},
          entry: function(){return entryObj;}
      }

    });
    //Transfer entry from one entry array to the next
    
    $scope.modalInstance.result.then(function (status) {
      Restangular.configuration.fullResponse = true;

      
      taskApi.post({owner:'/scs/user/1/', task_type:'/scs/task_type/'+'addIdHere'+'/', status: "init", start_time : '2014-11-10T03:07:43', end_time : '2014-11-11T03:07:43', parametersJson:'{ "input_ids":{"1": {"path": "'+entryObj.name+'"}, "2": {"path": "'+(dstResource.path+entryObj.basename).substring(1)+'"}}}'},null,{"Content-Type":"application/json"})
        .then(function(response){


          Restangular.configuration.fullResponse = false;
          var taskUri = response.headers('Location');
          var pathInfo = getLocation(taskUri);
 
          var resource;
          
          var p = taskresourceApi.post({task:taskUri, resource: '/scs/resource/'+srcResource.id+'/', resource_type_task_type: '/scs/resource_type_task_type/'+taskParams.resourceRt3Map[srcResource.id]+'/'},null,{"Content-Type":"application/json"});

          p.then(function(){
            taskresourceApi.post({task:taskUri, resource: '/scs/resource/'+dstResource.id+'/', resource_type_task_type: '/scs/resource_type_task_type/'+taskParams.resourceRt3Map[dstResource.id]+'/'},null,{"Content-Type":"application/json"}).then(function(){
              var taskId = pathInfo.pathname.split('/')[3];

              taskApi.get(taskId+'/?action=start');
              modalTransferProgInst = $modal.open({
                templateUrl: 'taskcreate/taskProgressModal.tpl.html',
                controller: 'TaskProgModalCtrl',
                resolve: {
                  taskObj: function(){return Restangular.one('task',taskId+'/');},
                  modalTitle: function(){return "Data Transfer progress";},
                  interactive: function(){return false;}
                }

              });   


            });
          });

        },function(){
          console.log('Error posting task');
        });
      
  

    }, function () {
      console.log('Transfer cancelled!');
    });

    $rootScope.$broadcast('sm-drop-success');


  };

  $scope.entryLsPane = function($event, entry, resource){

    if(entry.isDir){
      resourceApi.get(resource.id+"/"+entry.name.substring(0,entry.name.length-1)+"/list/").then(function(entries){
        resource.levels.push(entries);
        resource.path= '/'+entry.name;
        resource.entries = entries;
      });
    }
  };

  $scope.refresh = function(resource){
  
    var path = resource.path === "/" ? "" : resource.path.substring(0,resource.path.length-1);
    resourceApi.get(resource.id+path+"/list").then(function(entries){
        resource.entries = entries;
      });

  };

  $scope.startTask = function(){
    $scope.modalInstance = $modal.open({
      templateUrl: 'taskcreate/ConfirmModal.tpl.html',
      controller: 'ConfirmModalCtrl',
      resolve: {
        title: function(){return "Confirm task start";},
        body: function(){return "Do you want to start this task?";}
      }
      
    });
    //Transfer entry from one entry array to the next
    
    $scope.modalInstance.result.then(function (status) {
     
      var parametersJson = {"input_ids":{}};
      var input;
      var path;
      var valName;
      for(var i = $scope.args.length-1; i>=0 ; i--){
        input = $scope.args[i];
        valName = INPUT_TYPE_VALUE_MAP[input.type];
        switch(input.type){
          case 'resource':
            if(input.inputValue.substring(0,1)==="/"){
              path = input.inputValue.substring(1);
            }
            else{
              path = input.inputValue;
            }
            parametersJson.input_ids[input.input_id]={};
            parametersJson.input_ids[input.input_id][valName] = path;
            break;
          case 'string':
            if(arg.value && arg.value !== ""){
              break;
            }
            parametersJson.input_ids[input.input_id]={};
            parametersJson.input_ids[input.input_id][valName] = input.inputValue;
            break;
          default:
            break;
        }
        
      }
      Restangular.configuration.fullResponse = true;

      
      taskApi.post({owner:'/scs/user/1/', task_type:'/scs/task_type/'+taskTypeId+'/', status: "init", start_time : '2014-11-10T03:07:43', end_time : '2014-11-11T03:07:43', parametersJson: JSON.stringify(parametersJson)},null,{"Content-Type":"application/json"})
        .then(function (response){ //success callback

          Restangular.configuration.fullResponse = false;
          var taskUri = response.headers('Location');
          var pathInfo = getLocation(taskUri);

          var resource;
          var taskResourcePromises = [];
          var promise;
          var res;
          var taskResourceObjs = taskParams.taskResources;
          var taskResourceObj;
          //var preSetRt3s = JSON.parse(JSON.stringify(resourceRt3Map));
          for(var key in taskResourceObjs){
            taskResourceObj = taskResourceObjs[key];
            promise = taskresourceApi.post({task:taskUri, resource: '/scs/resource/'+taskResourceObj.resourceId+'/', resource_type_task_type: '/scs/resource_type_task_type/'+taskResourceObj.rtttId+'/'},null,{"Content-Type":"application/json"});
            taskResourcePromises.push(promise);            
          }
          
          $q.all(taskResourcePromises).then(function(){
            var taskId = pathInfo.pathname.split('/')[3];
            var configJson = JSON.parse(taskTypeObj.configurationJson);
            taskApi.get(taskId+'/?action=start'); //?action=start
            if(configJson.interactive){
              modalTransferProgInst = $modal.open({
                templateUrl: 'taskcreate/taskProgressModal.tpl.html',
                controller: 'TaskProgModalCtrl',
                resolve: {
                  taskObj: function(){return Restangular.one('task',taskId+'/');},
                  modalTitle: function(){return "Preparing " +taskTypeObj.name;},
                  interactive: function(){return true;}
                }

              });
            }else{
              modalTransferProgInst = $modal.open({
                templateUrl: 'taskcreate/taskProgressModal.tpl.html',
                controller: 'TaskProgModalCtrl',
                resolve: {
                  taskObj: function(){return Restangular.one('task',taskId+'/');},
                  modalTitle: function(){return taskTypeObj.name + " task progress";},
                  interactive: function(){return false;}

                }

              });  
            }
               
          });
              


           

        },function(){ //Error callback
          console.log('Error posting task');
        });
      
  

    }, function () {
      console.log('Transfer cancelled!');
    });

    


    
  };

  $scope.setAndReturnArgVal = function(path, arg){
    arg.inputValue = path;
    return path;
  };

  $scope.download = function(entry){
    var resource = this.resource;
    $scope.modalInstance = $modal.open({
      templateUrl: 'taskcreate/ConfirmModal.tpl.html',
      controller: 'ConfirmModalCtrl',
      resolve: {
        title: function(){return "Confirm download";},
        body: function(){return "Do you want to download <strong>"+entry.basename+"</strong>?";}
      }
      
    });
    //Transfer entry from one entry array to the next
    
    $scope.modalInstance.result.then(function (status) {    
      console.log('Downloaded!');
      window.open(SMARF_URL+'/scs/resource/'+resource.id+'/'+entry.name+'/download/');
    },function(status){
      console.log('not Downloaded');
    });
  };

  $scope.roleContains = function(r){
    if(r.indexOf("src") >=0){
      return "src";
    }
    else if(r.indexOf("dest") >=0){
      return "dest";
    }

  };
  /*** HELPER FUNCTIONS ***/

  /*Expressions*/
  var getResourceNature = function(resource){
    var split = resource.resource_type.split('/');
    resourcetypeApi.get(split[3]+'/').then(function(response){
      var obj = response;

      $scope.resourcesNature[resource.id] = obj.nature;

      if(obj.nature === 'data'){
        getResourceEntries(resource);
      }


    });
  };

  var getResourceEntries = function(resource) {
    resourceApi.get(resource.id+"/list/").then(function(entries){
      resource.entries = entries;

      if(resource.levels === undefined){
        resource.levels = [];
      }
      
      resource.levels.push(entries);
      resource.path = '/';
    });

  };
  /*Definitions*/
  function getLocation(href) {
    var location = document.createElement("a");
    location.href = href;
    // IE doesn't populate all link properties when setting .href with a relative URL,
    // however .href will return an absolute URL which then can be used on itself
    // to populate these additional fields.
    if (location.host === "") {
      location.href = location.href;
    }
    return location;
  }

  /***EXE***/
  
  //resourceRt3Map = taskInfo.getResourceRt3Map();
  taskParams = taskInfo.getParameters();
  if(!taskParams){
    $state.go('taskresourceselect',{taskTypeId:taskTypeId});
    return;
  }
  taskTypeObj = taskInfo.getTaskType();
  $scope.args = normalizeFilter(taskParams.args);
  for(var i = $scope.args.length-1; i >= 0; i--){
    
    getResourceEntries($scope.args[i].resource);
    
  }
  // var configJson = JSON.parse(taskTypeObj.configurationJson);
  // var args = configJson.args;
  // var arg;
  // var resources = resourceRt3Map;
  // var argObj;
  // for(var i=args.length-1; i>=0;i--){
  //   arg = args[i];
  //   for(var key in resources){
  //     var resource = resources[key];

  //     if((!arg.value || arg.value === "") && arg.role && arg.role == resource.rtttRole){
  //       $scope.args.push({type:arg.type,input_id: arg.input_id,name:arg.name,description:arg.description,role:arg.role});
  //       $scope.resources[arg.input_id] = resource.resource;
  //       getResourceEntries(resource.resource);
  //     }
  //   }
  // }
  
  

  
  
}])

.controller( 'TransferConfModalCtrl', ['$scope', '$modalInstance', 'srcPath', 'dstPath', 'entry', function($scope, $modalInstance, srcPath, dstPath, entry){
  

  $scope.srcPath = srcPath;
  $scope.dstPath = dstPath;
  $scope.entryName = entry.basename;
  $scope.ok = function(){
    $modalInstance.close('ok');
  };

  $scope.cancel = function(){
    $modalInstance.dismiss('cancel');
  };
}])
.controller( 'TaskProgModalCtrl', ['$scope', '$modalInstance', 'taskObj', '$interval','TASK_STATE_ENUM','modalTitle','interactive','$modal','TASK_STATE_FINISHED','TASK_STATE_RUNNING', function($scope, $modalInstance, taskObj, $interval,TASK_STATE_ENUM, modalTitle, interactive, $modal, TASK_STATE_FINISHED, TASK_STATE_RUNNING){
  $scope.status = ['active','progress-striped'];
  $scope.percentComplete = 0;
  $scope.title = modalTitle;
  $scope.interactive = interactive;
  $scope.disabled = false;
  var stop;
  $scope.taskState = TASK_STATE_ENUM['init'];
  if(interactive){
    
    $scope.disabled = true;
    stop = $interval(function() {
      taskObj.get().then(function(response){
        
        if(response.progressJson !== ''){
          $scope.percentComplete = JSON.parse(response.progressJson).progress;
        }

        if(response.state === TASK_STATE_RUNNING){
          $scope.percentComplete = 100;
          $modalInstance.close('ok');
        }
        $scope.taskState = TASK_STATE_ENUM[response.state];

      });
    },1000);
    $modalInstance.result.then(function(){
      $interval.cancel(stop);
      startInteractiveTool();
    }, function(){
      $interval.cancel(stop);
    });
  }
  else{
    stop = $interval(function() {
      taskObj.get().then(function(response){
        
        if(response.progressJson !== ''){
          $scope.percentComplete = JSON.parse(response.progressJson).progress;
        }

        if(response.state === TASK_STATE_FINISHED){
          $scope.percentComplete = 100;
        }
        $scope.taskState = TASK_STATE_ENUM[response.state];
      });
    },1000);

    // $scope.$watch('percentComplete',function(newValue, oldValue){
    
    // if(newValue >= 100 && ){
    //     $interval.cancel(stop);
    //     $scope.status = ['progress-striped'];
    //   }
    // });

    $modalInstance.result.then(function(){
      $interval.cancel(stop);
    }, function(){
      $interval.cancel(stop);
    });

  }
  
  
  
  $scope.ok = function(){
    $modalInstance.close('ok');
    //$interval.cancel(stop);
  };

  $scope.cancel = function(){
    $modalInstance.dismiss('cancel');
    //$interval.cancel(stop);
  };

  function startInteractiveTool(){
    var $interactiveModalConf = $modal.open({
      templateUrl: 'taskcreate/ConfirmModal.tpl.html',
      controller: 'ConfirmModalCtrl',
      resolve: {
        title: function(){return 'Confirm Interactive Task';},
        body: function(){return 'Your task is ready to start in interactive mode. Start interactive mode now? ';}

      }

    });

    $interactiveModalConf.result.then(function(){
      taskObj.get().then(function(response){
        var json;
        if(response.runningDisplayJson){
          json = JSON.parse(response.runningDisplayJson);
          window.open(json.taskView.url);
        }
        
      });
    }, function(){
      console.log('Interactive mode cancelled :\'(');
    });
  }

}])
.controller( 'ConfirmModalCtrl', ['$scope', '$modalInstance','title', 'body', function($scope, $modalInstance, title, body){
  $scope.title = title;
  $scope.body = body;
  $scope.ok = function(){
    $modalInstance.close('ok');
  };

  $scope.cancel = function(){
    $modalInstance.dismiss('cancel');
  };

}])
;



