angular.module('rpsmarf.metadata.extractionlist', [
	'rpsmarf.user.service',
	'rpsmarf.config',
	'restangular',
	'rpsmarf.helpers.service',
	'rpsmarf.interval.service',
	'ui.bootstrap'


])
.config(function($stateProvider){
	$stateProvider.state( 'extractionlist', {
		url: '/extractionlist/',
		views: {
			"main": {
				controller: 'ExtractionListCtrl',
				templateUrl: 'metadata/extractionlist/extractionlist.tpl.html'
			}
		},
		data:{ pageTitle: 'Extraction List' }
	});
})
.filter('extractionUpdate', ['SmHelpers','SmInterval','$interval','Restangular', function(SmHelpers, SmInterval, $interval, Restangular){

	var setProgressInterval = function(extraction){
		return $interval(function(){
			Restangular.one('md_extractor_full',extraction.id+'/').get().then(function(response){
				response = response.plain();
				var pj;
				
				if(response.state === 'done'){
				
					$interval.cancel(extraction.progressInterval);
				
				}

				for(var key in response){
					extraction[key] = response[key];
				}
				
				extraction.progress = JSON.parse(response.progressJson);
				extraction.progressPercentage = (extraction.progress.files_processed/extraction.progress.files_to_process*100).toFixed(2);				
			});
		},3000);
	};

	//var numIts = 0;

	return function(extractions){
		//console.log(++numIts);
		var extraction, json;
		var newExtractions = [];
		for(var i = 0; i< extractions.length; i++){
			extraction = extractions[i];
			extraction.startTimeNorm = SmHelpers.normalizeTime(extraction.last_extract_start);
			extraction.duration = SmHelpers.duration(extraction.last_extract_start, extraction.last_extract_end);
			extraction.progress = JSON.parse(extraction.progressJson);
			extraction.progressPercentage = (extraction.progress.files_processed/extraction.progress.files_to_process*100).toFixed(2);
			if(!extraction.progressInterval){
				switch (extraction.state){
					case 'purging':
					case 'extracting':
					case 'scanning':
					case 'updating':
						extraction.progressInterval = setProgressInterval(extraction);
						SmInterval.addInterval(extraction.progress.interval);
						break;
					default:
						angular.noop();
				}
			}
			
			newExtractions.push(extraction);
		}

		return newExtractions;
	};
}])
.controller('ExtractionListCtrl', ['$scope','Restangular','SmUser','$modal', function($scope, Restangular, SmUser, $modal){
	/***STANDARD VARIABLES***/
	var uid = SmUser.getProperty('id');
	var availableRepos;
	/***SCOPE VARIABLES***/
	$scope.extractions = [];

	/***SCOPE FUNCTIONS***/
	$scope.rebuildExtraction = function(extraction, index){
		var modalInstance = $modal.open({
			templateUrl: 'metadata/extractionlist/extractionlist.modal.tpl.html',
			controller: 'ExtractorModalCtrl',
			resolve: {
				extractor: function(){return angular.copy(extraction);},
				extractorExists: function(){return true;},
				availableRepos: function(){return null;}
			},
			size: 'lg'
		});

		modalInstance.result.then(function(obj){
			if(obj.action === 'rebuild'){
				Restangular.one('md_extractor_full',extraction.id+'/').get().then(function(response){
					response = response.plain();
					$scope.extractions[index] = response;
				});
			}
		});
	};

	$scope.createExtractor = function(){
		var modalInstance = $modal.open({
			templateUrl: 'metadata/extractionlist/extractionlist.modal.tpl.html',
			controller: 'ExtractorModalCtrl',
			resolve: {
				extractor: function(){return {};},
				extractorExists: function(){return false;},
				availableRepos: function(){return availableRepos;}
			},
			size: 'lg'
		});

		modalInstance.result.then(function(obj){
			if(obj.action === 'create'){
				obj.promise.then(function(response){
					Restangular.one('md_extractor_full',response.id+'/').get().then(function(response){
						response = response.plain();
						$scope.extractions.push(response);
					});
				});
			}
		});

	};

	/***EXE***/
	Restangular.all('md_extractor_full/?owner__id='+uid+'&limit=0').get('').then(function(response){
		$scope.extractions = response.objects;

		Restangular.all('md_repo/?limit=0').get('').then(function(response){
			availableRepos = response.objects;
			//var availableRepos = [];
			for(var i = availableRepos.length-1; i >= 0; i--){
				for( var j= $scope.extractions.length-1; j>= 0; j--){
					if(availableRepos[i].id == $scope.extractions[j].repo.id){
						availableRepos.splice(i,1);
						break;
					}
				}
				
			}

		});
	});


}])
.controller('ExtractorModalCtrl',['$scope','$modalInstance','extractor','extractorExists','Restangular','availableRepos', function($scope, $modalInstance,extractor,extractorExists, Restangular,availableRepos){
	/***SCOPE VARIABLES***/
	$scope.extractorApi = {value:null};
	$scope.extractor = extractor;
	$scope.extractorExists = extractorExists;
	$scope.repos = availableRepos;

	/***SCOPE FUNCTIONS***/
	$scope.rebuildExtraction = function(){
		var p = $scope.extractorApi.value.rebuildExtractor();
		p.then(function(response){
			if(response.rebuild){
				$modalInstance.close({action:'rebuild'});	
			}
			
		});
		
	};

	$scope.startExtraction = function(){
		var p = $scope.extractorApi.value.startExtraction();
		$modalInstance.close({action:'create', promise: p});
	};

	$scope.close = function(){
		$modalInstance.close({action:'close'});
	};

	/***EXE***/

}]);