/**
 * Tests sit right alongside the file they are testing, which is more intuitive
 * and portable than separating `src` and `test` directories. Additionally, the
 * build process will exclude all `.spec.js` files from the build
 * automatically.
 */
describe( 'drag and drop features', function() {
	beforeEach( module( 'rpsmarf.foldernavpane') );

	beforeEach(module('foldernavpane/foldernavpane.tpl.html'));
	var scope, element, entries,timeout;

	var homeSubEntries = [{basename:'documents/', path:'/home/documents/', isDir:true},{basename:'pictures/', path:'/home/pictures/',isDir:true}, {basename:'myfile', path:'/home/documents/myfile',isDir:false}],
		documentsSubEntries = [{basename:'report1.doc', path:'/home/documents/report1.doc',isDir:false},{basename:'paper.pdf', path:'/home/documents/paper.pdf',isDir:false}],
		dataSubEntries = [{basename:'bridge.dat', path:'/home/data/bridge.dat',isDir:false},{basename:'machinery.dat', path:'/home/data/machinery.dat',isDir:false}, {basename:'data.dat', path:'/home/data/data.dat',isDir:false}];
		var srcEntries, dstEntries;
	describe('Display folder/file entries', function(){

		
		beforeEach(inject(function($rootScope, $compile, $timeout){
			srcEntries = [{basename:'home/', path:'/home/',isDir:true},{basename:'documents/', path:'/home/documents/',isDir:true},{basename:'data/', path:'/data/',isDir:true},{basename:'file.txt', path:'/file.txt',isDir:false}];
			dstEntries = [{basename:'report1.doc', path:'/home/documents/report1.doc',isDir:false},{basename:'paper.pdf', path:'/home/documents/paper.pdf',isDir:false}];
			scope = $rootScope.$new();
			element = angular.element('<foldernavpane id="src" entries="srcEntries" ls="entryLs(entry)" levelup="levelUp()" smondrop="drop(srcEntries, dstEntries, entryObj)"></foldernavpane>'+
										'<foldernavpane id ="dst" entries="dstEntries" ls="entryLs(entry)" levelup="levelUp()" smondrop="drop(srcEntries, dstEntries, entryObj)"></foldernavpane>'
										);
			
			
			timeout = $timeout;
			scope.srcEntries = srcEntries;
			scope.dstEntries = dstEntries;
			scope.levels = [entries];
			scope.drop = function(srce, dste, entryObj){
				
				var index = srce.indexOf(entryObj);
				scope.$apply(function(){
					srce.splice(index,1);
					dste.push(entryObj);
				});

				scope.$broadcast('sm-drop-success');
				
			};

			

			element = $compile(element)(scope);


			scope.$digest();

			angular.element(document.body).append(element);
			
			//scope = element.isolateScope();

			
			


		}));

		it( 'should set the right classes to the appropriate elements for the various events', function() {
			//console.log(element);
			var $isolated = element.isolateScope();
			var srcFnp = element.eq(0).children();
			var dstFnp = element.eq(1);
			
			var target = srcFnp.find('div.entry-container').eq(3);

			//console.log(target);
			expect(target.attr('id')).toEqual(srcEntries[3].$$hasKey);
			expect(target.find('p.entry-name').html()).toEqual(srcEntries[3].basename);

			target.trigger('dragstart');
			//console.log()
			
				//Make sure the pane is highlighted
				//console.log(dstFnp);
			expect(dstFnp.find('div.panel-body').hasClass('sm-drop-area')).toBe(true);
			target.trigger('dragend');

			
				//Make sure the pane is no longer highlighted
			expect(dstFnp.find('div.panel-body').hasClass('sm-drop-area')).toBe(false);

			dstFnp.find('div.panel-body').trigger('dragover');

				
			expect(dstFnp.find('div.panel-body').hasClass('sm-drop-area')).toBe(false);
			expect(dstFnp.find('div.panel-body').hasClass('sm-drop-allow')).toBe(true);

			dstFnp.find('div.panel-body').trigger('dragleave');

		
			expect(dstFnp.find('div.panel-body').hasClass('sm-drop-area')).toBe(true);
			expect(dstFnp.find('div.panel-body').hasClass('sm-drop-allow')).toBe(false);
						
			//timeout.flush();
			

		});

	it('should perform drag and drop', function(){
		var $isolated = element.isolateScope();
		var srcFnp = element.eq(0);
		var dstPanel = element.eq(1).find('div.panel-body');
		var target = srcFnp.find('div.entry-pane:eq(3)');
		var oldLengthDst = dstEntries.length;
		var oldLengthSrc = srcEntries.length;
		var srcObject = srcEntries[3];
		target.trigger('dragstart');

		dstPanel.trigger('drop');
		
		var newLengthDst = dstEntries.length;
		var newLengthSrc = srcEntries.length;
		expect(oldLengthDst + 1).toEqual(newLengthDst);
		expect(oldLengthSrc -1 ).toEqual(newLengthSrc);

		expect(srcObject).toEqual(dstEntries[newLengthDst-1]);
		//console.log(srcFnp);
		expect(srcFnp.hasClass('sm-drag-src')).toBe(false);
		
		//timeout.flush();
	});



		


		

		

	});
  
});

