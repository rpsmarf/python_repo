/**
 * Config application for storing application default values
 */
angular.module( 'rpsmarf.config', [

	'rpsmarf.constants'
])

//Constant values
.constant('DEBUG', <%= debug %>)
.constant('SMARF_URL', '<%= url %>')
.constant('DISQUS_FORUM_SHORTNAME', '<%= disqusShortName %>')
.constant('TASK_COMPLETION_ENUM',{0:"notCompleted",1:"completedWithoutError", 2:"completedWithError" })
.constant('TASK_STATE_ENUM', { init : 'Initializing', booting: 'Booting VM', starting: 'Starting', prep: 'Preparing', running: 'Running', cleanup: 'Cleaning up', finished:'Finished'} )
.constant('TASK_STATE_FINISHED', 'finished' )
.constant('TASK_STATE_RUNNING', 'running' )
.constant('TASK_STATE_QUEUED', 'queued')
.constant('USER_STATE', {unAuth: 0, loggingIn: 1, browse: 2, task_start: 3})

//Retrieved constant values
.service('FORUM_URL', function(){
	var url = {value: ''};
	return url;
})
;
