'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import logging
from tastypie.resources import ModelResource
from sm_controller.api import smMakeFilterAllFieldsFilter,\
    SmarfSerializer, scsMakeAuthorization, scsMakeAuthentication
from canarie_service.models import SmModelCanarieServiceProperties
from smarfcontrolserver.init.envvar import scsApiGetAuthOn
from sm_controller.authorization import ScsGdnAuthVisibleToAllEditOnlySuperuser

logger = logging.getLogger(__name__)

 
# CANARIE service object
class SmApiCanarieServiceProperties(ModelResource):
    
    class Meta:
        if scsApiGetAuthOn():
            authentication = scsMakeAuthentication()
        authorization = scsMakeAuthorization(ScsGdnAuthVisibleToAllEditOnlySuperuser())
        queryset = SmModelCanarieServiceProperties.objects.all()
        resource_name = 'csprop'
        filtering = smMakeFilterAllFieldsFilter(SmModelCanarieServiceProperties)
        serializer = SmarfSerializer()
        
        

