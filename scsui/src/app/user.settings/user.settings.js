angular.module('rpsmarf.user.settings', [
'ui.router',
'ui.bootstrap',
'rpsmarf.user.service',
'restangular',
'rpsmarf.helpers.service',
'rpsmarf.confirm'

])
.config(['$stateProvider', function($stateProvider){

	$stateProvider.state('user',{
		abstract:true
	}).
	state('user.settings',{
		abstract:true,
		url: '^/account/',
		views:{
			'main@':{
				controller: 'UserSettingsCtrl',
				templateUrl: 'user.settings/user.settings.tpl.html'
			}
		},
		data:{
			pageTitle: 'Account Settings'
		}
	}).
	state('user.settings.account',{
		url: '^/account/',
		views:{
			'settings': {
				templateUrl: 'user.settings/user.settings.account.tpl.html'
			},
			'clouds': {
				templateUrl: 'user.settings/user.settings.cloud.tpl.html',
				controller: 'UserCloudAccountsCtrl'
			}

		}
		
		
	}).
	state('user.settings.notification',{
		views:{
			'settings': {
				templateUrl: 'user.settings/user.settings.notification.tpl.html'
			}

		}
		
		
	});
}])
.controller('UserSettingsCtrl', ['$scope','$state','SmUser','Restangular', '$modal',
 function($scope, $state, SmUser, Restangular, $modal){

	/***STANDARD VARIABLES***/
	var uid = SmUser.getProperty('id');
	/***SCOPE VARIABLES***/
	$scope.data = {communities:[]};
	if(!$scope.userSettings){
		$scope.userSettings = {value: []};
	}
	$scope.userObj = SmUser.getObject();
	$scope.accountSetup = $state.is('setup.page');
	/***STANDARD FUNCTIONS***/
	var getSettingValue = function(objs, name_key){
		for(var i=objs.length-1; i>=0; i--){
			if(objs[i].name_key === name_key){
				return objs[i];
			}
		}
		return null;
	};

	var createUserAccountSettings = function(){
		
		Restangular.one('community/').get().then(function(response){
			$scope.data.communities = response.objects;
			var arr = [];
			var userObj = SmUser.getObject();

			var i, prop;
			
			//Restangular.one('user/' + uid +'/').get().then(function(res){
			//	arr.push({name:'User name', value: res.username, editable:false, editing:false, required:true });
			//	arr.push({name:'First name', value: userObj.firstname, editable:false, editing:false, required:true });
			//	arr.push({name:'Last name', value: userObj.lastname, editable:false, editing:false, required:true });
			//	arr.push({name:'Email', value: userObj.email, editable:false, editing:false, required:true });
			//	arr.push({name:'Profile picture', value: userObj.imageUrl, editable:false, editing:false, required:false });
			//	arr.push({name:'Community', value: getSettingValue(response.objects,userObj.community.name_key), name_key:userObj.community.name_key, editable:true, editing:false, select: true, data_name: 'communities', obj_name:'community', see_more:true });
			//	arr.push({name:'Api Key', value: userObj.apikey, editable: false, editing:false, 'class': 'api-input', how:'api', see_more:true});
			//	$scope.userSettings = arr;
			//});


			arr.push({name:'First name', value: userObj.firstname, editable:false, editing:false, required:true });
			arr.push({name:'Last name', value: userObj.lastname, editable:false, editing:false, required:true });
			arr.push({name:'Email', value: userObj.email, editable:false, editing:false, required:true });
			arr.push({name:'Profile picture', value: userObj.imageUrl, editable:false, editing:false, required:false });
			arr.push({name:'Community', value: getSettingValue(response.objects,userObj.community.name_key), name_key:userObj.community.name_key, editable:true, editing:false, select: true, data_name: 'communities', obj_name:'community', see_more:true });
			arr.push({name:'Api Key', value: userObj.apikey, editable: false, editing:false, 'class': 'api-input', how:'api', see_more:true});
			$scope.userSettings.value = arr;

			Restangular.one('resource/?owner__id='+uid+'&personalFolder=true').get().then(function(response){
				var i,obj, json, percent, info;
				for(i=0;i<response.objects.length; i++){
					obj = response.objects[i];
					json = JSON.parse(obj.capacityJson);
					if(!json.diskUsed){
						json.diskUsed = 0;
					}
					if(obj.name_key.indexOf('private') >=0){
						arr.push({name:'Private Cloud Folder Usage', value: ((json.diskUsed/json.diskUsedFailThreshold)*100).toFixed(2) + '% ' + '('+(json.diskUsed/1073741824).toFixed(3) +' GB of ' +(json.diskUsedFailThreshold/1073741824).toFixed(2) + ' GB used'+')', editable:false, editing:false, see_more:true });
					}
					else if(obj.name_key.indexOf('public') >=0){
						arr.push({name:'Public Cloud Folder Usage', value: ((json.diskUsed/json.diskUsedFailThreshold)*100).toFixed(2) + '% ' + '('+(json.diskUsed/1073741824).toFixed(3) +' GB of ' +(json.diskUsedFailThreshold/1073741824).toFixed(2) + ' GB used'+')', editable:false, editing:false, see_more:true });
					}
				}
			});
		});
		
		
	};
	$scope.see_more = function(event)
	{
		if ( $(event.target).text() != "See less")
		{
			$(event.target).text("See less");
			$("div.see-more").show();
		}
		else
		{
			$(event.target).text("See more");
			$("div.see-more").hide();
		}
	};

	/***SCOPE FUNCTIONS***/
	$scope.settingAction = function(setting){

		setting.editing = !setting.editing;
		if(setting.editing){
			return;
		}

		var obj = {};
		if(setting.select){
			obj[setting.obj_name] = setting.value.resource_uri;	
		}
		else{
			obj[setting.obj_name] = setting.value;
		}
		
		Restangular.one('user_setting',uid+'/').patch(obj, null, {"Content-Type": "application/json"});

		if(setting.obj_name === 'community'){
			SmUser.setProperty('community',setting.value);
		}
	};

	$scope.howTo = function(setting){
		switch(setting.how){
			case 'api':
				openApiHowTo();
				break;
			default:
				break;
		}
	};

	$scope.updateSettingVal = function(setting, index){
		setting.value = $index;
	};

	/***STANDARD FUNCTIONS***/
	var openApiHowTo = function(){
		var text = 'To use this mechanism, you can either specify an Authorization header or pass the username/api_key combination as GET/POST parameters. Examples:<br>';
		text += '<pre># As a header<br># Format is ``Authorization: ApiKey &lt;username&gt;:&lt;api_key&gt;<br>Authorization: ApiKey daniel:204db7bcfafb2deb7506b89eb3b9b715b09905c8<br>';
		text += '<br># As GET params<br>http://127.0.0.1:8000/api/v1/entries/?username=daniel&api_key=204db7bcfafb2deb7506b89eb3b9b715b09905c8</pre>';
		text += '<br><br>CURL example:<br><pre>curl --header "Authorization: ApiKey daniel:204db7bcfafb2deb7506b89eb3b9b715b09905c8" www.rpsmarf.ca/scs/resource/</pre>';


		$modal.open({
			controller: 'AlertCtrl',
			templateUrl: 'alert/alert.tpl.html',
			size: 'lg',
			resolve: {
				title: function(){return 'How to use the Api Key';},
				message: function(){return text;}
			}
		});
	};

	/***EXE***/
	if($state.$current.name.split('.').pop() === 'account' || $state.$current.name === 'setup.page'){
		createUserAccountSettings();
	}

	
}])
.controller('UserCloudAccountsCtrl', ['$scope','Restangular','SmUser','$modal','SmHelpers','$timeout', function($scope, Restangular, SmUser, $modal, SmHelpers, $timeout){
	/***STANDARD VARIABLES***/
	var uid = SmUser.getProperty('id');
	var clouds, userClouds, json, timeout;

	/***SCOPE VARIABLES***/
	$scope.propertyValues = {};
	$scope.containerPropertyValues = {};
	$scope.clouds = null;
	$scope.userClouds = null;
	$scope.accountSettingsSaved = false;

	/***SCOPE FUNCTIONS***/
	$scope.saveCloudAccount = function(cloud){
		var allSet = true, missing, dataObj = {};
		for(var k in cloud.properties){
			if(!$scope.propertyValues[cloud.id][k]){
				missing = cloud.properties[k].title;
				allSet = false;
				break;
			}
			dataObj[k] = $scope.propertyValues[cloud.id][k];
		}
		if(!allSet){
			$modal.open({
				controller: 'AlertCtrl',
				templateUrl: 'alert/alert.tpl.html',
				resolve:{
					title: function(){ return 'Error';},
					message: function(){ return missing + ' field is required.';}
				}
			});
			return;
		}

		if(cloud.userSet){
			
			Restangular.all('cloud_account/'+cloud.cloudAccountId+'/').patch({accountInfoJson: JSON.stringify(dataObj)}).then(function(response){
				console.log('cloud account updated');
			});	
		}
		else{
			var postObj = {};
			postObj['owner'] = '/scs/user/'+uid+'/';
			postObj['cloud'] = cloud.resource_uri;
			postObj['accountInfoJson'] = JSON.stringify(dataObj);
			Restangular.configuration.fullResponse = true;
			Restangular.all('cloud_account/').post(postObj).then(function(response){
				cloud.userSet = true;
				cloud.cloudAccountId = SmHelpers.getObjectIdFromPathName(response.headers('Location'));
				Restangular.configuration.fullResponse = false;
			}, function(response){
				Restangular.configuration.fullResponse = false;
			});
		}
		
	};

	$scope.deleteCloudAccount = function(cloud){
		var modalInstance = $modal.open({
			controller: 'ConfirmCtrl',
			templateUrl: 'confirm/confirm.tpl.html',
			resolve: {
				title: function(){ return 'Confirm Deletion'; },
				body: function(){ return 'Delete cloud account? You will may not be able to use cloud based compute resources for certain tools.';} 
			}
		});

		modalInstance.result.then(function(){
			Restangular.all('cloud_account/'+cloud.cloudAccountId+'/').remove().then(function(){
				console.log('cloud account deleted');
				cloud.userSet = false;
				cloud.cloudAccountId = null;
			});
		});
	};

	$scope.saveCloudAccountSettings = function(cloud){
		$scope.accountSettingsSaved = false;
		var dataObj = {};
		for(var k in cloud.containerProperties){
			dataObj[k] = $scope.containerPropertyValues[cloud.id][k];
		}
		Restangular.all('container/'+cloud.cloudContainerId+'/').patch({parametersJson:JSON.stringify(dataObj)}).then(function(){
			$scope.accountSettingsSaved = true;
			if(timeout){
				$timeout.cancel(timeout);	
			}
			
			timeout = $timeout(function(){ $scope.accountSettingsSaved = false;}, 5000);
			
			console.log('container settings updated');
		});
	};

	/***STANDARD FUNCTIONS***/
	var setContainerIdAndValues = function(cloud){
		var params;
		Restangular.all('container/?cloud_account__id='+cloud.cloudAccountId).get('').then(function(response){
			if(response.objects.length > 0 ){
				cloud.cloudContainerId = response.objects[0].id;	
				params = JSON.parse(response.objects[0].parametersJson);
				if(!$scope.containerPropertyValues[cloud.id]){
					$scope.containerPropertyValues[cloud.id] = {};
				}
				for(var k in params){
					$scope.containerPropertyValues[cloud.id][k] = params[k];
				}
			}
			
		});
	};

	/***EXE***/
	Restangular.all('cloud/').get('').then(function(response){
		clouds = response.objects;
		Restangular.all('cloud_account/?user__id='+uid).get('').then(function(response){
			userClouds = response.objects;

			for(var j = clouds.length-1; j>=0; j--){
				if(!$scope.propertyValues[clouds[j].id]){
					$scope.propertyValues[clouds[j].id] = {};
				}
				if(!clouds[j].properties){
					json = JSON.parse(clouds[j].cloud_account_param_json_schema);
					clouds[j].properties = json.properties;

					json = JSON.parse(clouds[j].cloud_container_param_json_schema);
					clouds[j].containerProperties = json.properties;
				}

				for(var i = userClouds.length-1; i>=0; i--){

				
					if(userClouds[i].cloud == clouds[j].resource_uri){
						clouds[j].userSet = true;
						clouds[j].cloudAccountId = userClouds[i].id;
						setContainerIdAndValues(clouds[j]);
						
					}
				}
			}
			$scope.clouds = clouds;
			//console.log($scope.clouds);
		});
	});

}]);