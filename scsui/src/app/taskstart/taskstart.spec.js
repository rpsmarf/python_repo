describe( 'Task start module', function() {
	//beforeEach( module( 'rpsmarf' ) );
	beforeEach (module('rpsmarf.config'));
	beforeEach (module('rpsmarf.user.service'));
	
	beforeEach(module('rpsmarf.task.create.service'));
	beforeEach(module('restangular'));
	beforeEach(module('rpsmarf.taskstart'));
	beforeEach(module('ui.bootstrap'));
	beforeEach(module('rpsmarf.home'));
	beforeEach(module('rpsmarf.confirm'));
	beforeEach(module('confirm/confirm.tpl.html'));
	beforeEach(module('home/home.tpl.html'));
	beforeEach(module('alert/alert.tpl.html'));
	beforeEach(module('taskstart/taskstart.tpl.html'));
	beforeEach(module('breadcrumb/breadcrumb.tpl.html'));
	beforeEach(module('rpsmarf.reservation.service'));
	
	var $scope, $rootScope, SmUser,  SmTaskCreate, $controller,  $state, $modal,  $modalInstance, smarfurl, taskObject, $httpBackend, tool_prefix = 'src/app/tools/', taskcreate_prefix='src/app/task.create.service/';
	var rt3s, resource_types, task_type, dataObj, ra, $interval, user, SmReservation;
	//init, starting, prep, running, cleanup and finished
	
	describe('standard flow', function(){

		beforeEach(  inject(function(_$rootScope_, Restangular, SMARF_URL,  _SmTaskCreate_, _$httpBackend_, _$controller_, _$modal_, _$state_, _SmUser_, _$interval_, _SmReservation_){
			smarfurl = SMARF_URL+'/scs/';
			Restangular.configuration.baseUrl=smarfurl;
			Restangular.configuration.encodeIds = false;
			ra = Restangular;
			$interval = _$interval_;
			SmTaskCreate = _SmTaskCreate_;
			SmUser = _SmUser_;
			$rootScope = _$rootScope_;
			$httpBackend = _$httpBackend_;
			taskObject = Restangular.one('scs/task','1/');
			$scope = $rootScope.$new();
			$controller = _$controller_;
			$modal = _$modal_;
			$state = _$state_;
			user = SmUser.getObject();
			user.id = 2;
			SmReservation = _SmReservation_;
			dataObj = JSON.parse(__html__[taskcreate_prefix+'task.create.service.spec.json']);
			task_type = dataObj.task_type;
			rt3s = dataObj.rt3s;

			resource_types = JSON.parse(__html__[taskcreate_prefix+'task.create.service.spec.json']).resource_types;
		

			$httpBackend.when('GET',smarfurl+'resource_type_task_type/?task_type__id='+task_type.id).respond(200,{objects:rt3s});
			$httpBackend.when('GET',smarfurl+'resource_type/1/').respond(200,resource_types[0]);
			$httpBackend.when('GET',smarfurl+'resource_type/3/').respond(200,resource_types[2]);
			$httpBackend.when('GET',smarfurl+'resource_type/4/').respond(200,resource_types[3]);
			$httpBackend.when('GET',smarfurl+'resource/?resource_type__id=4').respond(200,{meta: {total_count:1}, objects:[dataObj.res_type_4]});
			$httpBackend.when('GET',smarfurl+'resource/?resource_type__id=4&extForUser='+user.id).respond(200,{meta: {total_count:1}, objects:[dataObj.res_type_4]});
			SmTaskCreate.setTaskType(task_type);

			$httpBackend.flush();

			$controller('TaskStartCtrl', {$scope:$scope,Restangular:Restangular, $modal:$modal, $state:$state, SmTaskCreate:SmTaskCreate });
			//expect($scope.compParam.noSelect).toBe(true);

		})) ;

		it('should validate task to start and give errors where appropriate', inject([function(){
			spyOn($modal, 'open').andCallThrough();
			//Make compute param resource null to test validation
			$scope.compParam.resource = null; $scope.compParam.noSelect = undefined;
			expect($scope.valParams.length).toEqual(1);
			expect($scope.resParams.length).toEqual(2);
			expect($scope.requiresComputeResource).toBe(true);

			$scope.start();

			expect($scope.valParams[0].error).toBe(true);
			$scope.valParams[0].value = "test";

			$scope.start();
			expect($scope.resParams[1].error).toBe(true);

			$scope.resParams[1].resource = {};
			$scope.resParams[1].resource.id = 1;
			$scope.resParams[1].resource.path = '';
			$scope.resParams[1].resource.inputVal = '';
			
			$scope.start();
			expect($scope.resParams[1].error).toBe(true);

			$scope.resParams[1].resource = {};
			$scope.resParams[1].resource.path = '/path/1';
			$scope.resParams[1].resource.id = 1;
			$scope.start();


			expect($scope.resParams[0].error).toBe(true);
			$scope.resParams[0].resource = {};
			$scope.resParams[0].resource.path = '/path/0';
			$scope.resParams[0].resource.id = 2;
			$scope.start();

			expect($scope.compParam.error).toBe(true);

			$scope.compParam.resource = {id:1};
			
			$scope.start();
			
			expect($modal.open).toHaveBeenCalled();
			




		}]));

		it('should start the task and emit an event notifying listeners that a task has started ', inject([function(){
			spyOn($scope, '$emit');
			var inputs = SmTaskCreate.getInputs();
			var compute = dataObj.res_type_4;
			var uid = SmUser.getProperty('id');
			var ttid = task_type.id;
			var value = "A test value";
			var tid = 1;
			
			//Input file resource
			var source = dataObj.res_type_3;
			source.path = "/folder/mock.data";

			//Output location resource
			var dest = dataObj.res_type_1;
			dest.path = "/home/user/";

			var parametersJson = '{"input_ids":{"1":{"path":"'+source.path+'"},"2":{"path":"'+dest.path+'"},"3":{"value":"'+value+'"}}}';

			compute.rt3_id = inputs.computeResource.rt3_id;
			source.rt3_id = inputs.resourceParameters[1].rt3_id;
			dest.rt3_id = inputs.resourceParameters[0].rt3_id;
			
			SmTaskCreate.setInput('string', value, 3 );
			/**Old way of setting input values**/
			//SmTaskCreate.setInput('compute', compute);
			//SmTaskCreate.setInput('resource', source, 1);
			//SmTaskCreate.setInput('resource', dest, 2);
			SmTaskCreate.setActiveResourceParameterId(0);
			SmTaskCreate.setValueForActiveResourceParameter(compute);
			SmTaskCreate.setActiveResourceParameterId(1);
			SmTaskCreate.setValueForActiveResourceParameter(source);
			SmTaskCreate.setActiveResourceParameterId(2);
			SmTaskCreate.setValueForActiveResourceParameter(dest);

			var paramsJsonTest = function(string){
				var data = JSON.parse(string);
				var patt = /[a-zA-Z0-9\/]+\/[0-9]{8}T[0-9]{6}Z/;
				if(data.owner !== '/scs/user/'+uid+'/'){
					return;
				}
				if(data.task_type !== '/scs/task_type/'+ttid+'/'){
					return;
				}
				if(data.status !== 'init'){
					return;
				}
				var pj = JSON.parse(data.parametersJson);
				if(!pj.input_ids || pj.input_ids[1].path !== source.path){
					return;
				}
				if(pj.input_ids[3].value !== value){
					return;
				}
				if(!patt.test(pj.input_ids[2].path)){
					return false;
				}

				return true;

			};


			$httpBackend.expect('POST',smarfurl+'task/',paramsJsonTest,{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200,{headers:function(val){ return '/scs/task/'+tid+'/'; }},{Location:'/scs/task/'+tid+'/'});
		
			//Compute resource
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+compute.id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+compute.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			//Source resource
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+source.id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+source.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			//Dest resource
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+dest.id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+dest.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			$httpBackend.expect('GET',smarfurl+'task/'+tid+'/?action=start').respond(200);

			//SmTaskCreate.createAndStartTask();	

			$scope.start();
			$rootScope.$apply();
			$scope.modalInstance.close('ok');
			//$rootScope

			$httpBackend.flush();

			expect($scope.$emit).toHaveBeenCalledWith('SmTaskStart',jasmine.any(Object),$scope.tasknameUser, false);


		}]));

		it('should create a reservation when starting a manual reservation (no task configuration)', inject([function(){
			

			var inputs = SmTaskCreate.getInputs();
			var compute = dataObj.res_type_4;
			compute.rt3_id = inputs.computeResource.rt3_id;
			var ttid = task_type.id;
			var value = "A test value";
			var tid = 1;
			var startIso = '2015-03-25T18:25:00Z';
			var endIso = '2015-03-25T19:25:00Z';
			var uid = SmUser.getProperty('id');
			var obj = {owner: '/scs/user/'+uid+'/', resource: '/scs/resource/'+$scope.compParam.resource.id+'/', start_time:startIso, description:JSON.stringify({resource_name:$scope.compParam.resource.name, manual: true, resource_namekey:$scope.compParam.resource.name_key}),end_time:endIso };
			SmTaskCreate.reset();
			SmTaskCreate.setTaskType(task_type, true);
			$state.go('taskstart.reservation');
			$rootScope.$apply();

			$httpBackend.flush();

			$controller('TaskStartCtrl', {$scope:$scope,Restangular:ra, $modal:$modal, $state:$state, SmTaskCreate:SmTaskCreate });
			$scope.reservation.starttime = new Date('2015-03-25T18:25:13Z');
			$scope.reservation.startdate = new Date('2015-03-25T18:25:13Z');


			$httpBackend.flush();
			//expect($scope.compParam.noSelect).toBe(true);

			$scope.reservation.manual = true;

			$scope.start();

			//create reservation
			$httpBackend.expect('POST', smarfurl+'reservation/', obj).respond(200);

			$rootScope.$apply();
			angular.element('.modal-footer .btn-primary').click();
			
			$rootScope.$apply();
			$httpBackend.flush();

		}]));

		it('should create a reservation and task objects when starting an automatic reservation (task configuration)', inject([function(){
			$httpBackend.resetExpectations();
			spyOn($state, 'go').andCallThrough();
			spyOn(SmReservation,'areReservationParamsSaved');
			spyOn(SmReservation,'saveReservationParameters');
			spyOn(SmReservation,'getReservationParameters');
			var uid = SmUser.getProperty('id');
			var startIso = '2015-03-25T18:25:00Z';
			var endIso = '2015-03-25T19:25:00Z';
			var obj = {owner: '/scs/user/'+uid+'/', resource: '/scs/resource/'+$scope.compParam.resource.id+'/', 
						start_time:startIso, description:JSON.stringify({resource_name:$scope.compParam.resource.name, manual: false, 
						resource_namekey:$scope.compParam.resource.name_key,task_name : task_type.name}), end_time:endIso};
			var obj2 = {};
			SmTaskCreate.reset();
			SmTaskCreate.setTaskType(task_type, true);
			//flush task type decomposing
			$httpBackend.flush();
			var inputs = SmTaskCreate.getInputs();
			var compute = dataObj.res_type_4;
			
			var ttid = task_type.id;
			var value = "A test value";
			var tid = 1;
			
			//Input file resource
			var source = dataObj.res_type_3;
			source.path = "/folder/mock.data";

			//Output location resource
			var dest = dataObj.res_type_1;
			dest.path = "/home/user/";

			var parametersJson = '{"input_ids":{"1":{"path":"'+source.path+'"},"2":{"path":"'+dest.path+'"},"3":{"value":"'+value+'"}}}';

			compute.rt3_id = inputs.computeResource.rt3_id;
			source.rt3_id = inputs.resourceParameters[1].rt3_id;
			dest.rt3_id = inputs.resourceParameters[0].rt3_id;
			
			SmTaskCreate.setInput('string', value, 3 );
			
			SmTaskCreate.setActiveResourceParameterId(0);
			SmTaskCreate.setValueForActiveResourceParameter(compute);
			SmTaskCreate.setActiveResourceParameterId(1);
			SmTaskCreate.setValueForActiveResourceParameter(source);
			SmTaskCreate.setActiveResourceParameterId(2);
			SmTaskCreate.setValueForActiveResourceParameter(dest);

			var paramsJsonTest = function(string){
				var data = JSON.parse(string);
				var patt = /[a-zA-Z0-9\/]+\/[0-9]{8}T[0-9]{6}Z/;
				if(data.owner !== '/scs/user/'+uid+'/'){
					return;
				}
				if(data.task_type !== '/scs/task_type/'+ttid+'/'){
					return;
				}
				if(data.status !== 'init'){
					return;
				}
				var pj = JSON.parse(data.parametersJson);
				if(!pj.input_ids || pj.input_ids[1].path !== source.path){
					return;
				}
				if(pj.input_ids[3].value !== value){
					return;
				}
				if(!patt.test(pj.input_ids[2].path)){
					return false;
				}
				if(data.start_time !== '2015-03-25T18:25:00Z'){
					return false;
				}
				if(data.end_time !== '2015-03-25T19:25:00Z'){
					return false;
				}

				return true;

			};

			$state.go('taskstart.reservation');
			$rootScope.$apply();

			//$httpBackend.flush();

			$controller('TaskStartCtrl', {$scope:$scope,Restangular:ra, $modal:$modal, $state:$state, SmTaskCreate:SmTaskCreate });
			$httpBackend.flush();
			expect(SmReservation.areReservationParamsSaved).toHaveBeenCalled();

			$httpBackend.expect('POST', smarfurl+'reservation/', obj,{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200, {headers: function(v){ return '/scs/reservation/18/';}},{Location:'/scs/task/18/'});

			$httpBackend.expect('POST',smarfurl+'task/',paramsJsonTest,{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200,{headers:function(val){ return '/scs/task/'+tid+'/'; }},{Location:'/scs/task/'+tid+'/'});
		
			//Compute resource
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+compute.id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+compute.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			//Source resource
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+source.id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+source.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			//Dest resource
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+dest.id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+dest.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			$httpBackend.expect('GET',smarfurl+'task/'+tid+'/?action=start').respond(200);


			obj2.description = JSON.parse(obj.description);
			obj2.description['task_id'] = tid;
			obj2.description = JSON.stringify(obj2.description);
			$httpBackend.expect('PATCH', smarfurl+'reservation/18/', {description: obj2.description},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			//SmTaskCreate.createAndStartTask();	

				
			
			
			
			
			$scope.reservation.starttime = new Date('2015-03-25T18:25:13Z');
			$scope.reservation.startdate = new Date('2015-03-25T18:25:13Z');



			$scope.reservation.manual = false;

			$scope.start();
			$rootScope.$apply();
			
			angular.element('.modal-footer .btn-primary').click();
			
			$rootScope.$apply();
			//$rootScope

			$httpBackend.flush();
			$rootScope.$apply();
			angular.element('.modal-footer .btn-primary').click();
			expect($state.go).toHaveBeenCalledWith('home');
			

		}]));

		it('should reject task start when the compute resource is already in use and allow queueing', inject([function(){

			$httpBackend.resetExpectations();
			var uid = SmUser.getProperty('id');
			
			var inputs = SmTaskCreate.getInputs();
			var compute = dataObj.res_type_4;
			
			var ttid = task_type.id;
			var value = "A test value";
			var tid = 1;
			
			//Input file resource
			var source = dataObj.res_type_3;
			source.path = "/folder/mock.data";

			//Output location resource
			var dest = dataObj.res_type_1;
			dest.path = "/home/user/";

			var parametersJson = '{"input_ids":{"1":{"path":"'+source.path+'"},"2":{"path":"'+dest.path+'"},"3":{"value":"'+value+'"}}}';

			compute.rt3_id = inputs.computeResource.rt3_id;
			source.rt3_id = inputs.resourceParameters[1].rt3_id;
			dest.rt3_id = inputs.resourceParameters[0].rt3_id;
			
			SmTaskCreate.setInput('string', value, 3 );
			
			SmTaskCreate.setActiveResourceParameterId(0);
			SmTaskCreate.setValueForActiveResourceParameter(compute);
			SmTaskCreate.setActiveResourceParameterId(1);
			SmTaskCreate.setValueForActiveResourceParameter(source);
			SmTaskCreate.setActiveResourceParameterId(2);
			SmTaskCreate.setValueForActiveResourceParameter(dest);

			var paramsJsonTest = function(string){
				var data = JSON.parse(string);
				var patt = /[a-zA-Z0-9\/]+\/[0-9]{8}T[0-9]{6}Z/;
				if(data.owner !== '/scs/user/'+uid+'/'){
					return;
				}
				if(data.task_type !== '/scs/task_type/'+ttid+'/'){
					return;
				}
				if(data.status !== 'init'){
					return;
				}
				var pj = JSON.parse(data.parametersJson);
				if(!pj.input_ids || pj.input_ids[1].path !== source.path){
					return;
				}
				if(pj.input_ids[3].value !== value){
					return;
				}
				if(!patt.test(pj.input_ids[2].path)){
					return false;
				}

				return true;

			};

			$state.go('taskstart');
			$rootScope.$apply();

			//$httpBackend.flush();

			$controller('TaskStartCtrl', {$scope:$scope,Restangular:ra, $modal:$modal, $state:$state, SmTaskCreate:SmTaskCreate });
			//$httpBackend.flush();

			$httpBackend.expect('POST',smarfurl+'task/',paramsJsonTest,{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200,{headers:function(val){ return '/scs/task/'+tid+'/'; }},{Location:'/scs/task/'+tid+'/'});
		
			//Compute resource
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+compute.id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+compute.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			//Source resource
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+source.id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+source.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			//Dest resource
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+dest.id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+dest.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			$httpBackend.expect('GET',smarfurl+'task/'+tid+'/?action=start').respond(409,{status: "reserved", reservation: {end_time:'2015-03-25T19:25:00Z' }});
			

			spyOn($modal, 'open').andCallThrough();

			$scope.start();
			$rootScope.$apply();
			
			angular.element('.modal-footer .btn-primary').click();
			$rootScope.$apply();
			$httpBackend.flush();
			$httpBackend.expect('GET',smarfurl+'task/'+tid+'/?action=start&allowQueueing=True').respond(200);
			$rootScope.$apply();
			expect($modal.open.mostRecentCall.args[0].resolve.body()).toEqual('Error starting '+ task_type.name+':<br>The selected compute resource is currently in use and is reserved until 7:25 PM on '+ 'March' + ' ' + '25' + ' ' + '2015'+'. Would you like to add your task to the queue for this resource? (Task will run once the resource is free)');
			angular.element('.modal-footer .btn-primary').click();
			
			$rootScope.$apply();		
			$httpBackend.flush();

		}]));

		it('should display proper messages when resevation creation is rejected', inject([function(){
			
			spyOn($modal, 'open').andCallThrough();
			var inputs = SmTaskCreate.getInputs();
			var compute = dataObj.res_type_4;
			compute.rt3_id = inputs.computeResource.rt3_id;
			var ttid = task_type.id;
			var value = "A test value";
			var tid = 1;
			var startIso = '2015-03-25T18:25:00Z';
			var endIso = '2015-03-25T19:25:00Z';
			var uid = SmUser.getProperty('id');
			var obj = {owner: '/scs/user/'+uid+'/', resource: '/scs/resource/'+$scope.compParam.resource.id+'/', start_time:startIso, description:JSON.stringify({resource_name:$scope.compParam.resource.name, manual: true, resource_namekey:$scope.compParam.resource.name_key}),end_time:endIso };
			SmTaskCreate.reset();
			SmTaskCreate.setTaskType(task_type, true);
			$state.go('taskstart.reservation');
			$rootScope.$apply();

			$httpBackend.flush();

			$controller('TaskStartCtrl', {$scope:$scope,Restangular:ra, $modal:$modal, $state:$state, SmTaskCreate:SmTaskCreate });
			$scope.reservation.starttime = new Date('2015-03-25T18:25:13Z');
			$scope.reservation.startdate = new Date('2015-03-25T18:25:13Z');


			$httpBackend.flush();
			//expect($scope.compParam.noSelect).toBe(true);

			$scope.reservation.manual = true;

			$scope.start();

			//create reservation
			$httpBackend.expect('POST', smarfurl+'reservation/', obj).respond(400,  {conflictingReservation: '/scs/reservation/4/', message: 'Reservation made by Test User (test@test.com) conflicts with this reservation'});

			$rootScope.$apply();
			angular.element('.modal-footer .btn-primary').click();
			
			$rootScope.$apply();
			$httpBackend.flush();

			expect($modal.open.mostRecentCall.args[0].resolve.message()).toBe('Could not create reservation due to:<br>Reservation made by Test User (test@test.com) conflicts with this reservation');

		}]));

		it('should reattempt starting a task when a user who has reserved the compute reource starts a task but it is rejected due to cancelling the previous task', inject(['$interval', function($interval){

			$httpBackend.resetExpectations();
			var uid = SmUser.getProperty('id');
			
			var inputs = SmTaskCreate.getInputs();
			var compute = dataObj.res_type_4;
			
			var ttid = task_type.id;
			var value = "A test value";
			var tid = 1;
			
			//Input file resource
			var source = dataObj.res_type_3;
			source.path = "/folder/mock.data";

			//Output location resource
			var dest = dataObj.res_type_1;
			dest.path = "/home/user/";

			var parametersJson = '{"input_ids":{"1":{"path":"'+source.path+'"},"2":{"path":"'+dest.path+'"},"3":{"value":"'+value+'"}}}';

			compute.rt3_id = inputs.computeResource.rt3_id;
			source.rt3_id = inputs.resourceParameters[1].rt3_id;
			dest.rt3_id = inputs.resourceParameters[0].rt3_id;
			
			SmTaskCreate.setInput('string', value, 3 );
			
			SmTaskCreate.setActiveResourceParameterId(0);
			SmTaskCreate.setValueForActiveResourceParameter(compute);
			SmTaskCreate.setActiveResourceParameterId(1);
			SmTaskCreate.setValueForActiveResourceParameter(source);
			SmTaskCreate.setActiveResourceParameterId(2);
			SmTaskCreate.setValueForActiveResourceParameter(dest);

			var paramsJsonTest = function(string){
				var data = JSON.parse(string);
				var patt = /[a-zA-Z0-9\/]+\/[0-9]{8}T[0-9]{6}Z/;
				if(data.owner !== '/scs/user/'+uid+'/'){
					return;
				}
				if(data.task_type !== '/scs/task_type/'+ttid+'/'){
					return;
				}
				if(data.status !== 'init'){
					return;
				}
				var pj = JSON.parse(data.parametersJson);
				if(!pj.input_ids || pj.input_ids[1].path !== source.path){
					return;
				}
				if(pj.input_ids[3].value !== value){
					return;
				}
				if(!patt.test(pj.input_ids[2].path)){
					return false;
				}

				return true;

			};

			$state.go('taskstart');
			$rootScope.$apply();

			//$httpBackend.flush();

			$controller('TaskStartCtrl', {$scope:$scope,Restangular:ra, $modal:$modal, $state:$state, SmTaskCreate:SmTaskCreate, $interval:$interval });
			//$httpBackend.flush();

			$httpBackend.expect('POST',smarfurl+'task/',paramsJsonTest,{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200,{headers:function(val){ return '/scs/task/'+tid+'/'; }},{Location:'/scs/task/'+tid+'/'});
		
			//Compute resource
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+compute.id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+compute.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			//Source resource
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+source.id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+source.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			//Dest resource
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+dest.id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+dest.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			$httpBackend.expect('GET',smarfurl+'task/'+tid+'/?action=start').respond(409,{status: "cancelling" });

			spyOn($modal, 'open').andCallThrough();

			$scope.start();
			$rootScope.$apply();
			
			angular.element('.modal-footer .btn-primary').click();
			$rootScope.$apply();
			$httpBackend.flush();

			expect($modal.open.mostRecentCall.args[0].resolve.message()).toEqual('The system is preparing the resource for your task. It will begin once it is ready.');

			$httpBackend.resetExpectations();

			$httpBackend.expect('GET',smarfurl+'task/'+tid+'/?action=start').respond(200);

			spyOn($interval, 'cancel');
			spyOn($scope, '$emit');

			$interval.flush(5000);
			$httpBackend.flush();

			expect($interval.cancel).toHaveBeenCalled();
			expect($scope.$emit).toHaveBeenCalled();

		}]));
	});
		
	describe('version flow', function(){

		beforeEach(module('taskstart/version.modal.tpl.html'));
		var configJson;
		beforeEach(inject(function(_$rootScope_, Restangular, SMARF_URL,  _SmTaskCreate_, _$httpBackend_, _$controller_, _$modal_, _$state_, _SmUser_, _$interval_, _SmReservation_){
			smarfurl = SMARF_URL+'/scs/';
			Restangular.configuration.baseUrl=smarfurl;
			Restangular.configuration.encodeIds = false;
			ra = Restangular;
			$interval = _$interval_;
			SmTaskCreate = _SmTaskCreate_;
			SmUser = _SmUser_;
			$rootScope = _$rootScope_;
			$httpBackend = _$httpBackend_;
			taskObject = Restangular.one('scs/task','1/');
			$scope = $rootScope.$new();
			$controller = _$controller_;
			$modal = _$modal_;
			$state = _$state_;
			user = SmUser.getObject();
			user.id = 2;

			SmReservation = _SmReservation_;
			dataObj = JSON.parse(__html__[taskcreate_prefix+'task.create.service.spec.json']);
			task_type = dataObj.task_type;
			configJson = JSON.parse(task_type.configurationJson);
			configJson.versioning = 'git';
			task_type.configurationJson = JSON.stringify(configJson);

			user.uiSettings.setToolVersions = {};
			user.uiSettings.setToolVersions[task_type.id] =  "v2.0_rpsmarf";
			rt3s = dataObj.rt3s;

			resource_types = JSON.parse(__html__[taskcreate_prefix+'task.create.service.spec.json']).resource_types;
		

			$httpBackend.when('GET',smarfurl+'resource_type_task_type/?task_type__id='+task_type.id).respond(200,{objects:rt3s});
			$httpBackend.when('GET',smarfurl+'resource_type/1/').respond(200,resource_types[0]);
			$httpBackend.when('GET',smarfurl+'resource_type/3/').respond(200,resource_types[2]);
			$httpBackend.when('GET',smarfurl+'resource_type/4/').respond(200,resource_types[3]);
			$httpBackend.when('GET',smarfurl+'resource/?resource_type__id=4').respond(200,{meta: {total_count:1}, objects:[dataObj.res_type_4]});
			$httpBackend.when('GET',smarfurl+'resource/?resource_type__id=4&extForUser='+user.id).respond(200,{meta: {total_count:1}, objects:[dataObj.res_type_4]});
			SmTaskCreate.setTaskType(task_type);

			$httpBackend.flush();

			$controller('TaskStartCtrl', {$scope:$scope,Restangular:Restangular, $modal:$modal, $state:$state, SmTaskCreate:SmTaskCreate });
		}));

		it('should set and save versions for tools/task_types that have multiple versions available', inject(function(){
			var response = { "resultcode": 0, "tags": [ { "author": "Andrew McGregor ", "message": "First simple version.\n", "tag": "v1.0_rpsmarf", "timestamp": 1437600852 }, { "author": "Andrew McGregor ", "message": "Made bullets larger and red. Gave users 5 lives.\n", "tag": "v2.0_rpsmarf", "timestamp": 1437600926 } ] };
			var patch = {setToolVersions:{}};
			patch.setToolVersions[task_type.id] = '';
			$httpBackend.expect('GET',smarfurl+'task_type/'+task_type.id+'/getversions/').respond(200, response );
			$httpBackend.flush();
			expect($scope.hasVersions).toBeTruthy();
			expect($scope.versionSet.value).toBe("v2.0_rpsmarf");
			expect($scope.selectedVersion.value).toEqual({tag:"v2.0_rpsmarf"});

			$scope.setVersion();

			$rootScope.$apply();
			$scope.selectedVersion.value = {author: '', message: 'Latest build of this software', tag: 'latest' };
			$httpBackend.expect('PATCH',smarfurl+'user_setting/'+user.id+'/',{uiSettingsJson:JSON.stringify(patch)}).respond(200, response );

			angular.element('.modal-footer .btn-primary').click();
			
			$rootScope.$apply();

			expect($scope.versionSet.value).toBe('latest');
			expect(user.uiSettings.setToolVersions[task_type.id]).toBe('');
			
			$httpBackend.flush();

		}));
	});
	
	
});

