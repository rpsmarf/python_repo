'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 11, 2014

    @author: rpsmarf
'''

import logging
import socket
import os
from smcommon.globals.envvar import SM_ENV_VAR_COMMON_KEY_DIR, smCommonGetEnvVar
import paramiko
import urllib

logger = logging.getLogger(__name__)


def smGetFreePort():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(('', 0))
    port = sock.getsockname()[1]
    sock.close()
    return port
 
    
def smGetFreePorts(count):
    ports = []
    socks = []
    for _ in range(0, count):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind(('', 0))
        socks.append(sock)
        ports.append(sock.getsockname()[1])
    
    for sock in socks:
        sock.close()
    return ports

    
def smLoadKeyFile(keyfile):
    if keyfile is None:
        return None
    key_path = os.path.join(smCommonGetEnvVar(SM_ENV_VAR_COMMON_KEY_DIR), 
                            keyfile)
    logger.debug("Loading keypath %s", key_path)
    mykey = paramiko.RSAKey.from_private_key_file(key_path) 
    logger.debug("Loaded key")
    return mykey


def smExtendPathInUrl(container_url, path):
    conUrl = urllib.parse.urlparse(container_url)
    return urllib.parse.urlunparse([conUrl[0], 
                                    conUrl[1], 
                                    conUrl[2] + path, 
                                    conUrl[3], 
                                    conUrl[4], 
                                    conUrl[5]])


