'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import importlib
import logging
import time

from smcommon.globals.envvar import  \
    SM_ENV_VAR_COMMON_MIN_TIME_BETWEEN_PROGRESS, \
    smCommonGetEnvVarFloat


logger = logging.getLogger(__name__)


TASK_STATE_RUNNING = "running"
TASK_STATE_STARTED = "started"
TASK_STATE_FAILED = "failed"
TASK_STATE_SUCCESS = "success"
TASK_STATE_CMD_ERROR = "cmdError"


class SmTaskRunnerFactory(object):
    '''
    This class implements
    '''
    def __init__(self):
        pass

    @classmethod
    def makeTaskRunner(self, taskId, callback, taskDesc):
        logger.debug("Creating task runner for id=%s with taskDesc %s", taskId, taskDesc.toJson())
        module = importlib.import_module(taskDesc.module)
        cls = getattr(module, taskDesc.cls)
        instance = cls(taskId, callback, taskDesc)
        return instance


class SmTaskRunnerBase(object):
    '''
    This is the base class for TaskRunner objects.
    These are the objects which take a set of parameters and "run" the
    task to do the action described.
    '''

    def __init__(self, taskId, doneCallback, taskDesc):
        '''
        Constructor
        '''
        self.taskId = taskId
        self._doneCallback = doneCallback
        self._progressCallback = None
        self.progress = {"progress": 0.0, "outputSize": 0}
        self.minTimeBetweenProgressCallbacks = smCommonGetEnvVarFloat(SM_ENV_VAR_COMMON_MIN_TIME_BETWEEN_PROGRESS)
        self.lastCallbackTime = 0
        self.startTime = time.time()
        self.taskDesc = taskDesc
        self.state = "prep"
        self.isDone = False
        self.stdoutLinesSeen = 0
        self.outputProgressMaxStdoutLines = None
        self._outputCallback = None
        
    def setProgressCallback(self, progressCallback):
        self._progressCallback = progressCallback
        
    def setTaskOutputCallback(self, outputCallback):
        self._outputCallback = outputCallback
        
    def getProgress(self):
        return self.progress
    
    def annouceTaskOutput(self, outputType, outputData):
        self.stdoutLinesSeen += outputData.count("\n") 
        self.announceProgress(self.progress)
        if self._outputCallback:
            try:
                self._outputCallback(self.taskId, outputType, outputData)
            except Exception as e:
                logger.debug("Error in output callback %s", str(e))
                
    def announceProgress(self, progress):
        for p in progress:
            self.progress[p] = progress[p]
        if self._progressCallback is not None:
            # Only send progress if enough time has elapsed...
            now = time.time()
            if (now - self.lastCallbackTime) >= self.minTimeBetweenProgressCallbacks:
                self.progress["runtime"] = round(now - self.startTime, 1)
                self.progress["state"] = self.state
                if self.outputProgressMaxStdoutLines:
                    self.progress["progress"] = round(((self.stdoutLinesSeen * 100) 
                                                       / self.outputProgressMaxStdoutLines), 1)
                logger.debug("Progress being announced is: %s", self.progress)
                self._progressCallback(self.taskId, self.progress)
                self.lastCallbackTime = now
        
    def start(self):
        raise NotImplementedError()
    
    def cancel(self):
        raise NotImplementedError()

    def setState(self, newState):
        self.state = newState
        self.progress = {"progress": 0.0}
        
    def cleanup(self):
        '''
        This method can be overriden by derived classed to do any required
        cleanup 
        '''
        pass
    
    def done(self, status, resultDict):
        self.cleanup()
        self.isDone = True
        self._doneCallback(self.taskId, status, resultDict)
