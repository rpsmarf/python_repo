'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
from django.test import TestCase
from sm_controller.models import SmModelResourceType, SmModelResourceTag, SmModelResource, SmModelTag, SmModelTaskType, \
    SmModelTask, SmModelAgent, SmModelContainer, SmModelTaskResource, \
    RESOURCE_PARAMETERS_JSON_SCHEMA, \
    TASK_PARAMETERS_JSON_SCHEMA, SmModelResourceTypeTaskType, \
    SmModelFavouriteResource, SmModelFavouriteTaskType, SmModelTaskTypeNote, \
    SmModelTaskTypeTag, SmModelResourceNote, SmModelCommunity
from django.contrib.auth.models import User
import json
from tastypie.models import ApiKey
from django.db.models.signals import post_save, pre_delete
import logging
from smarfcontrolserver.remoteagentif.heartbeats import ScsHeartbeatIf
from smarfcontrolserver.init.envvar import SCS_ENV_VAR_MIN_HEARTBEAT_INTERVAL, \
    SCS_ENV_VAR_MIN_TIME_AFTER_REMOTE_AGENT_ADD_BEFORE_ALARM, \
    SCS_ENV_VAR_STDERR_LIMIT_IN_DB, SCS_ENV_VAR_STDOUT_LIMIT_IN_DB, \
    scsApiGetAuthOn, scsApiSetAuthOn, SCS_ENV_VAR_DISQUS_PUBLIC_KEY,\
    SCS_ENV_VAR_DISQUS_SECRET_KEY
import time
import os
import math
from django.test.testcases import TransactionTestCase
from smarfcontrolserver.init.ice_server import ScsIceInterface, \
    SCS_HEARTBEAT_NAME
import smmonicemsgs
import Ice
from smcommon.utils.net_utils import smGetFreePort
from smcommon.utils.date_utils import sm_date_now, \
    smMakeDateFromString, smMakeDateString
from sm_controller import test_helper
from smcommon.task_runners.mock_async_ops_server import SmMockAsyncOpsServer
from smcommon.globals.remote_agent_factory import smRemoteAgentAsyncOperationFactory
from smcommon.globals.envvar import SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE
from sm_controller.signal_handlers import smTriggerSignalInstall
from sm_controller.test_helper import ScsTestHelper, DUMMY_DESCRIPTION
from datetime import timedelta
import jsonschema
from jsonschema.exceptions import ValidationError
from smcommon.constants.sm_const import SM_TASK_TYPE_DELETE_RECURSIVE_NAME_KEY,\
    SM_COMMUNITY_ALL_NAME_KEY
from guardian.shortcuts import assign_perm
from sm_controller.api_task import SmTaskUtilsAsyncResps
from smarfcontrolserver.pollers.QueuedTaskStarter import ScsQueuedTaskStarter
from actstream.signals import action
from actstream.models import actor_stream, target_stream
import datetime

logger = logging.getLogger(__name__) 
smTriggerSignalInstall()


# Create your tests here.
class ModelTest(TransactionTestCase):

    def test_resource_models(self):
        
        u = User()
        u.save()
        
        agent = SmModelAgent()
        agent.guid = "222"
        agent.owner = u
        agent.last_change = str(sm_date_now())
        agent.status = "down"
        agent.description = test_helper.DUMMY_DESCRIPTION
        agent.name = "XXX"
        agent.save()

        container = SmModelContainer()
        container.status = "up"
        container.name = "YYY"
        container.agent = agent
        container.description = test_helper.DUMMY_DESCRIPTION
        container.owner = u
        container.save()

        rtype = SmModelResourceType()
        rtype.name = "namexxx"
        rtype.description = test_helper.DUMMY_DESCRIPTION
        rtype.save()
        r_list = list(SmModelResourceType.objects.all())
        self.assertEqual(1, len(r_list))
        r2_type = r_list[0]
        self.assertEqual(r2_type, rtype)
        self.assertEqual(rtype.name, r2_type.name)
        str_r = str(rtype)
        self.assertTrue("SmModelResourceType" in str_r)
        self.assertTrue("namexxx" in str_r)

        t2 = SmModelTag()
        t2.tag_name = "tagabc"
        t2.save()
        
        r = SmModelResource()
        r.resource_type = r2_type
        r.owner = u
        r.container = container
        r.description = test_helper.DUMMY_DESCRIPTION
        r.name = "R"
        r.parametersJson = '{}'
        r.save()
        
        rtag = SmModelResourceTag()
        rtag.resource = r
        rtag.tag = t2
        rtag.save()
        
    def test_task_models(self):
        u = User()
        u.save()

        ttype = SmModelTaskType()
        ttype.name = "Name 1#1"
        ttype.description = test_helper.DUMMY_DESCRIPTION
        ttype.configurationJson = '{"args": []}'
        ttype.owner = User.objects.all()[0]
        ttype.save()
        t2_type = SmModelTaskType.objects.get(name="Name 1#1")
        self.assertEqual(t2_type, ttype)
        self.assertEqual(ttype.name, t2_type.name)
        str_t = str(ttype)
        self.assertTrue("SmModelTaskType" in str_t)
        self.assertTrue("Name 1#1" in str_t)
        t3_type = SmModelTaskType.objects.get(name_key="name_1_1")
        self.assertEqual(t3_type, ttype)
        self.assertEqual(ttype.name_key, "name_1_1")
        
        agent = SmModelAgent()
        agent.guid = "222"
        agent.name = "XXX"
        agent.owner = u
        agent.last_change = str(sm_date_now())
        agent.status = "down"
        agent.description = test_helper.DUMMY_DESCRIPTION
        agent.save()

        container = SmModelContainer()
        container.name = "YYY"
        container.status = "up"
        container.agent = agent
        container.description = test_helper.DUMMY_DESCRIPTION
        container.owner = u
        container.save()

        rtype = SmModelResourceType()
        rtype.name = "Name_123:\"'<>?.,,Abc !@##$$%^^&**((_+="
        rtype.description = test_helper.DUMMY_DESCRIPTION
        rtype.save()
        # Should have generated name_key
        self.assertEqual("name_123_abc_", rtype.name_key)
        # Can be explicitly set too
        rtype = SmModelResourceType()
        rtype.name = "XXName_123:\"'<>?.,,Abc !@##$$%^^&**((_+="
        rtype.name_key = "name_key_value"
        rtype.description = test_helper.DUMMY_DESCRIPTION
        rtype.save()
        self.assertEqual("name_key_value", rtype.name_key)

        r = SmModelResource()
        r.resource_type = rtype
        r.owner = u
        r.container = container
        r.name = "&*^*(%^%$%$#"
        r.description = test_helper.DUMMY_DESCRIPTION      
        r.name = "Q"  
        r.parametersJson = '{}'
        r.save()
        self.assertEqual("q", r.name_key)
        r.name_key = "name_key_value"
        r.save()
        self.assertEqual("name_key_value", r.name_key)

        t = SmModelTask()
        t.src_resource = r
        t.dst_resource = r
        t.owner = u
        t.community = "abc"
        t.task_type = ttype
        t.status = "abc"
        t.start_time = str(sm_date_now())
        t.end_time = str(sm_date_now())
        t.parametersJson = '{}'
        t.save()
        
        t2 = SmModelTask.objects.all()[0]
        self.assertEqual(t2.state, "init")
        self.assertEqual(t2.community, "abc")
                
    def test_jsonSchemaValidation(self):
        jsonschema.validate({}, RESOURCE_PARAMETERS_JSON_SCHEMA)
        cfgJson = {"taskViewBaseUrl": "http://a.b.c/x",
                   "args": [{"type": "string", "value": "11"},
                            {"type": "resource",
                            "direction": "output",
                            "role": "xxx"}
                            ]}
        jsonschema.validate(cfgJson, RESOURCE_PARAMETERS_JSON_SCHEMA)
        cfgJson = {"taskViewBaseUrl": 77,
                   "args": [{"type": "string", "value": "11"},
                            {"type": "resource",
                            "direction": "output",
                            "role": "xxx"}
                            ]}
        self.assertRaises(ValidationError, jsonschema.validate, cfgJson, RESOURCE_PARAMETERS_JSON_SCHEMA)
        cfgJson = {"taskViewBaseUrlX": "abc",
                   "args": [{"type": "string", "value": "11"},
                            {"type": "resource",
                            "direction": "output",
                            "role": "xxx"}
                            ]}
        self.assertRaises(ValidationError, jsonschema.validate, cfgJson, RESOURCE_PARAMETERS_JSON_SCHEMA)
        taskParamJson = {"input_ids": {
                                       "2": {"path": "june_2014/"},
                                       "3": {"path": "my_output/"},
                                       "1": {"value": "-r"}
                                       }}
        jsonschema.validate(taskParamJson, TASK_PARAMETERS_JSON_SCHEMA)

        taskParamJson = {"input_ids": {
                                       "2": {"pth": "june_2014/"},
                                       "3": {"path": "my_output/"},
                                       "1": {"value": "-r"}
                                       }}
        self.assertRaises(ValidationError, jsonschema.validate, cfgJson, RESOURCE_PARAMETERS_JSON_SCHEMA)

        taskParamJson = {"inpu_ids": {
                                      "2": {"path": "june_2014/"},
                                      "3": {"path": "my_output/"},
                                      "1": {"value": "-r"}
                                      }}
        self.assertRaises(ValidationError, jsonschema.validate, cfgJson, RESOURCE_PARAMETERS_JSON_SCHEMA)


class ApiTest(TransactionTestCase):
    
    def setUp(self):
        self.helper = ScsTestHelper(self)
        self.helper.setupTempFolder()
        
    def tearDown(self):
        self.helper.tearDown()
        TransactionTestCase.tearDown(self)
        
    def test_access(self):
        scsApiSetAuthOn(True)
        self.helper.setupModel()
        self.helper.setupRestAuth("root")
        self.helper.setupRemoteAgentViaIce()
        time.sleep(0.2)
        
        u3 = User(username="xxx", first_name="a", last_name="b")
        u3.save()
        
        # Each resource is tested for unsuccessful and successful authentication
        # No resource should be accessible without proper authentication
        resp = self.client.get("/scs/user/")
        self.assertEqual(resp.status_code, 401)
        
        resp = self.client.get("/scs/user/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        
        con = resp.content.decode('utf-8')
        jsoncon = json.loads(con)
        u1 = jsoncon["objects"][0]
        self.assertEqual("AnonymousUser", u1["username"])

        resp = self.client.get("/scs/resource/")
        self.assertEqual(resp.status_code, 401)
        
        resp = self.client.get("/scs/resource/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
 
        resp = self.client.get("/scs/resource_type/")
        self.assertEqual(resp.status_code, 401)
        
        resp = self.client.get("/scs/resource_type/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
 
        resp = self.client.get("/scs/task_type/")
        self.assertEqual(resp.status_code, 401)
        
        resp = self.client.get("/scs/task_type/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
 
        resp = self.client.get("/scs/task/")
        self.assertEqual(resp.status_code, 401)
        
        resp = self.client.get("/scs/task/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
 
        resp = self.client.get("/scs/tag/")
        self.assertEqual(resp.status_code, 401)
        
        resp = self.client.get("/scs/tag/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
 
        resp = self.client.get("/scs/resource_tag/")
        self.assertEqual(resp.status_code, 401)
        
        resp = self.client.get("/scs/resource_tag/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
 
        resp = self.client.get("/scs/resource_type_task_type/")
        self.assertEqual(resp.status_code, 401)
        
        logger.debug("Invoking API for resource_type_task_type")
        resp = self.client.get("/scs/resource_type_task_type/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
 
        resp = self.client.get("/scs/task_resource/")
        self.assertEqual(resp.status_code, 401)
        
        resp = self.client.get("/scs/task_resource/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
 
        resp = self.client.get("/scs/agent/")
        self.assertEqual(resp.status_code, 401)
        
        resp = self.client.get("/scs/agent/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
 
        resp = self.client.get("/scs/container/")
        self.assertEqual(resp.status_code, 401)
        
        resp = self.client.get("/scs/container/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)

        # Try a delete - expect failure due to lack of credentials
        resp = self.client.delete("/scs/user/" + str(u3.id) + "/")
        self.assertEqual(resp.status_code, 401)
                
        # Check personal cloud folders are present
        # self.helper.auth_header['HTTP_AUTHORIZATION'] += "garbage" force auth to fail
        resourceDict = self.helper.objGetViaRest("/scs/resource/?personalFolder=true&owner=" + 
                                          str(u3.id))
        self.assertEqual(2, resourceDict["meta"]["total_count"])
        
        # Check that directory can be created in each one
        for rDict in resourceDict["objects"]:
            ruri = rDict["resource_uri"]
            self.helper.objGetViaRest(ruri + "deleterecursive/")
        
        resp = self.client.delete("/scs/user/" + str(u3.id) + "/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 204, resp.content)
        
        # Verify object is gone
        resp = self.client.get("/scs/user/" + str(u3.id) + "/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 404)

    def test_requestaccess(self):
        ''' 
        This test checks that a user can request access.  The complete flow is:
        - check user B does not have access to resource/1
        - user B requests access to resource/1 (which is owned by user A)
        - user A requests list of request for him - sees request
        - user A requests list of request for resource/1 - sees request
        - user B requests lists of requests - sees nothing
        - user A deletes request
        - user A adds user B with permission requested
        - check user B now has access to resource/1
        '''
        
        scsApiSetAuthOn(True)
        self.helper.setupModel()
        self.helper.setupRestAuth("userxxx")
        self.helper.setupRemoteAgentViaIce()
        
        userB = User(username="userB")
        userB.save()
        userB_path = "/scs/user/" + str(userB.id) + "/"
        
        res = "/scs/resource/1/"
        
        # check user B does not have access to resource/1
        respJson = self.helper.objGetViaRest(res + "getperm/")
        self.assertEqual(0, len(respJson))
        
        # user B requests access to resource/1 (which is owned by user A)
        self.helper.setupRestAuth("userB")
        self.helper.objGetViaRest(res + "requestaccess/?access=r")
        self.helper.setupRestAuth("userxxx")

        # user A requests list of request for him - sees request
        respJson = self.helper.objGetViaRest("/scs/request/?access=r")
        self.assertEqual(1, respJson["meta"]["total_count"])
        
        # user A requests list of request for resource/1 - sees request
        respJson = self.helper.objGetViaRest("/scs/request/?access=r&target=" + res)
        self.assertEqual(1, respJson["meta"]["total_count"])
        req = respJson["objects"][0]
        
        # user B requests lists of requests - sees nothing
        self.helper.setupRestAuth("userB")
        respJson = self.helper.objGetViaRest("/scs/request/?access=r")
        self.assertEqual(0, respJson["meta"]["total_count"])

        # user A deletes request
        self.helper.setupRestAuth("userxxx")
        self.helper.objDeleteViaRest("/scs/request/" + str(req["id"]) + "/")
                         
        # Check request is gone            
        respJson = self.helper.objGetViaRest("/scs/request/?access=r&target=" + res)
        self.assertEqual(0, respJson["meta"]["total_count"])
        
        # user A adds user B with x permission 
        self.helper.objGetViaRest(res + "setperm/?action=assign&perm=x&user=" + userB_path)
        
        # check user B now has access to resource/1
        respJson = self.helper.objGetViaRest(res + "getperm/")
        self.assertEqual(1, len(respJson))
        self.assertTrue("execute_on_resource" in respJson['/scs/user/3/'])

        # user A adds user B with permission requested
        self.helper.objGetViaRest(res + "setperm/?action=assign&perm=wr&user=" + userB_path)
        
        # check user B now has access to resource/1
        respJson = self.helper.objGetViaRest(res + "getperm/")
        self.assertEqual(1, len(respJson))
        self.assertTrue("read_resource_data" in respJson[userB_path])
        self.assertTrue("write_resource_data" in respJson[userB_path])
   
    def makeTaskTypeWithInput(self):
        testTt = SmModelTaskType()
        testTt.name = "Test Task"
        testTt.name_key = "test_task"
        testTt.owner = self.helper.superuser
        testTt.description = DUMMY_DESCRIPTION
        testTt.code_module = "smcommon.tests.task_runner_tester"
        testTt.code_classname = "SmTaskRunningTester"
        testTt.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"name": "Command",
                                      "type": "int",
                                      "value": 1},
                                     {"name": "Command",
                                      "type": "int",
                                      "value": 1},
                                     {"type": "resource",
                                      "name": "File to use",
                                      "input_id": 1,
                                      "direction": "input", 
                                      "role": "storage"},
                                     ],
                            "computeResourceRole": "xxx",
                       }
                   )
        testTt.save()
        rttt1 = SmModelResourceTypeTaskType()
        rttt1.resource_type = SmModelResourceType.objects.get(name_key="default_resource")
        rttt1.task_type = testTt
        rttt1.role = "storage"
        rttt1.name = "test1"
        rttt1.description = DUMMY_DESCRIPTION
        rttt1.save()    
        rttt2 = SmModelResourceTypeTaskType()
        rttt2.resource_type = self.helper.resourceSleep.resource_type
        rttt2.task_type = testTt
        rttt2.role = "xxx"
        rttt2.name = "test2"
        rttt2.description = DUMMY_DESCRIPTION
        rttt2.save()    
        return testTt     
        
    def makeTaskWithInput(self, task_type, user, resource):
        task = SmModelTask()
        task.owner = user
        task.task_type = task_type
        task.parametersJson = json.dumps({"input_ids": {
                                                        "1": {"path": "f1"}, 
                                                        }})
        task.save()
        
        taskResource = SmModelTaskResource()
        taskResource.task = task
        taskResource.resource = resource
        taskResource.resource_type_task_type = SmModelResourceTypeTaskType.objects.get(name="test1")
        taskResource.role = "storage"
        taskResource.owner = user
        taskResource.save()
        
        taskResource = SmModelTaskResource()
        taskResource.task = task
        taskResource.resource = self.helper.resourceSleep
        taskResource.resource_type_task_type = SmModelResourceTypeTaskType.objects.get(name="test2")
        taskResource.role = "compute"
        taskResource.owner = user
        taskResource.save()
        
        return task

    def test_taskaccess(self):
        ''' 
        This test checks that a rules around using resources in a task
        are enforced correctly.
        '''
        
        scsApiSetAuthOn(True)
        self.helper.setupModel()
        self.helper.setupRestAuth("userxxx")
        self.helper.setupRemoteAgentViaIce()
        
        userB = User(username="userB")
        userB.save()
        userB_path = "/scs/user/" + str(userB.id) + "/"
        
        ttype = self.makeTaskTypeWithInput()
        
        res = SmModelResource.objects.get(id=1)
        res_path = "/scs/resource/" + str(res.id) + "/"
        res_compute_path = "/scs/resource/" + str(self.helper.resourceSleep.id) + "/"
        
        # Start task with userB who does not have access to resource/1 - expect error
        task1 = self.makeTaskWithInput(ttype, userB, res)
        self.helper.setupRestAuth("userB")
        resp = self.client.get("/scs/task/" + str(task1.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 401)
        
        # Grant user/B read access to resource/1 and start task - expect fail due to execute perm on compute resource missing
        self.helper.setupRestAuth("root")
        self.helper.objGetViaRest(res_path + "setperm/?action=assign&perm=r&user=" + userB_path)
        task1 = self.makeTaskWithInput(ttype, userB, res)
        self.helper.setupRestAuth("userB")
        resp = self.client.get("/scs/task/" + str(task1.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 401)
        
        # Grant user/B execute access to compute resource and start task - expect OK
        self.helper.setupRestAuth("root")
        self.helper.objGetViaRest(res_compute_path + "setperm/?action=assign&perm=x&user=" + userB_path)
        self.helper.objGetViaRest("/scs/task_type/" + str(ttype.id) + "/setperm/?action=assign&perm=x&user=" + userB_path)
        task1 = self.makeTaskWithInput(ttype, userB, res)
        self.helper.setupRestAuth("userB")
        resp = self.client.get("/scs/task/" + str(task1.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.helper.waitForTaskDone(task1.id)
        
        # Grant user/B only execute access and start task - expect error
        self.helper.setupRestAuth("root")
        self.helper.objGetViaRest(res_path + "setperm/?action=assign&perm=x&user=" + userB_path)
        self.helper.objGetViaRest(res_path + "setperm/?action=remove&perm=r&user=" + userB_path)
        task1 = self.makeTaskWithInput(ttype, userB, res)
        self.helper.setupRestAuth("userB")
        resp = self.client.get("/scs/task/" + str(task1.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 401)
                
        # Change task type to accept execute perm - expect OK
        ttype.configurationJson = \
            json.dumps(
                       {
                            "args": [ 
                                     {"name": "Command",
                                      "type": "int",
                                      "value": 1},
                                     {"name": "Command",
                                      "type": "int",
                                      "value": 1},
                                     {"type": "resource",
                                      "name": "File to use",
                                      "input_id": 1,
                                      "direction": "input", 
                                      "role": "storage"},
                                     ],
                            "computeResourceRole": "xxx",
                            "executePermOnDataOk": True
                       }
                   )
        ttype.save()
        task1 = self.makeTaskWithInput(ttype, userB, res)
        self.helper.setupRestAuth("userB")
        resp = self.client.get("/scs/task/" + str(task1.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.helper.waitForTaskDone(task1.id)

        # Grant user/B read access to resource/1 and start task - expect OK
        self.helper.setupRestAuth("root")
        self.helper.objGetViaRest(res_path + "setperm/?action=assign&perm=r&user=" + userB_path)
        self.helper.setupRestAuth("userB")
        task1 = self.makeTaskWithInput(ttype, userB, res)
        resp = self.client.get("/scs/task/" + str(task1.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.helper.waitForTaskDone(task1.id)

    def test_validation(self):
        """ 
        This method tests various Tasty Pie validations
        """
        self.helper.setupModel()
        self.helper.setupRestAuth(self.helper.user.username)
        time.sleep(0.5)
        
        resource_type = SmModelResourceType.objects.first()
        container = SmModelContainer.objects.first()
        user = self.helper.user
        assign_perm("sm_controller.add_smmodelresource", self.helper.user)

        # Test good case passes
        self.helper.objCreateViaRest("/scs/resource/",
                                     {"parametersJson": json.dumps({"folder": "xxx"}),
                                      'owner': "/scs/user/" + str(user.id) + "/",
                                      'resource_type': "/scs/resource_type/" + str(resource_type.id) + "/",
                                      'container': "/scs/container/" + str(container.id) + "/",
                                      'name': 'n1',
                                      'description': test_helper.DUMMY_DESCRIPTION
                                      })
        
        # Test that empty parametersJson is rejected
        self.assertRaises(Exception,
                          self.helper.objCreateViaRest,
                          "/scs/resource/",
                          {"parametersJson": "",
                           'owner': "/scs/user/" + str(user.id) + "/",
                           'resource_type': "/scs/resource_type/" + str(resource_type.id) + "/",
                           'container': "/scs/container/" + str(container.id) + "/",
                           'name': 'n2',
                           'description': test_helper.DUMMY_DESCRIPTION})
        
        # Test that JSON w/o folder is rejected if "nature==data"
        self.assertRaises(Exception,
                          self.helper.objCreateViaRest,
                          "/scs/resource/",
                          {"parametersJson": json.dumps({}),
                           'owner': "/scs/user/" + str(user.id) + "/",
                           'resource_type': "/scs/resource_type/" + str(resource_type.id) + "/",
                           'container': "/scs/container/" + str(container.id) + "/",
                           'name': 'n2',
                           'description': test_helper.DUMMY_DESCRIPTION
                           })

    def test_notes(self):
        # Create, get, delete and get again the notes objects
        # Task notes
        self.helper.setupModel()
        self.helper.setupRestAuth("userxxx")
        user = self.helper.user
        task_type = SmModelTaskType.objects.all().first()
        resource = SmModelResource.objects.all().first()        
        
        task_type_note_path = self.helper.objCreateViaRest("/scs/task_type_note/",
                                                  {'note': 'a',
                                                   'owner': "/scs/user/" + str(user.id) + "/",
                                                   'task_type': "/scs/task_type/" + str(task_type.id) + "/"})
        respDict = self.helper.objGetViaRest(task_type_note_path)
        self.assertEqual(respDict["note"], "a")
        self.assertTrue(("user/" + str(user.id)) in respDict["owner"])
        respDict = self.helper.objDeleteViaRest(task_type_note_path)
        resp = self.client.get(task_type_note_path, **self.helper.auth_header)
        self.assertEqual(resp.status_code, 404)
        
        resource_note_path = self.helper.objCreateViaRest("/scs/resource_note/",
                                                  {'note': 'a',
                                                   'owner': "/scs/user/" + str(user.id) + "/",
                                                   'resource': "/scs/resource/" + str(resource.id) + "/"})
        respDict = self.helper.objGetViaRest(resource_note_path)
        self.assertEqual(respDict["note"], "a")
        self.assertTrue(("user/" + str(user.id)) in respDict["owner"])
        respDict = self.helper.objDeleteViaRest(resource_note_path)
        resp = self.client.get(resource_note_path, **self.helper.auth_header)
        self.assertEqual(resp.status_code, 404)
        
    def test_news(self):
        # Create, get, delete and get again the news_items objects
        self.helper.setupModel()
        user = self.helper.user
        community = SmModelCommunity()
        community.name = 'Test Community'
        community.name_key = "test_community"
        community.owner = user
        community.description = "This is the test community "
        community.save()
    
        self.helper.setupRestAuth("root")
        testCommunityIdString = str(community.id)
        communityIdString = str(SmModelCommunity.objects.first().id)
        allCommunityPath = "/scs/community/" + communityIdString + "/"
        testCommunityPath = "/scs/community/" + testCommunityIdString + "/"
        news_item = self.helper.objCreateViaRest("/scs/news_item/",
                                          {'owner': "/scs/user/" + str(user.id) + "/",
                                           'release_date_time': str(sm_date_now()),
                                           'headline': 'Awesome Headline',
                                           'external_link': 'http://example.com/x',
                                           'community': allCommunityPath,
                                           'body': 'body body body'
                                           })
        respDict = self.helper.objGetViaRest(news_item)
        self.assertEqual(respDict["external_link"], "http://example.com/x")
        self.assertEqual(respDict["headline"], "Awesome Headline")
        self.assertEqual(respDict["body"], "body body body")
        self.assertEqual(respDict["community"], allCommunityPath)
        self.assertEqual(respDict["alert"], False)
        self.assertEqual(respDict["owner"], "/scs/user/1/") # Should be owned by root, not userxxx
        respDict = self.helper.objDeleteViaRest(news_item)
        resp = self.client.get(news_item, **self.helper.auth_header)
        self.assertEqual(resp.status_code, 404)
                
        # Add 2 system alerts (A and B)
        news_item_A = self.helper.objCreateViaRest("/scs/news_item/",
                                          {'owner': "/scs/user/" + str(user.id) + "/",
                                           'release_date_time': str(sm_date_now()),
                                           'headline': 'Awesome Headline A',
                                           "alert": True,
                                           'community': allCommunityPath,
                                           'body': 'AAA'
                                           })
        news_item_B = self.helper.objCreateViaRest("/scs/news_item/",
                                          {'owner': "/scs/user/" + str(user.id) + "/",
                                           'release_date_time': str(sm_date_now()),
                                           'headline': 'Awesome Headline B',
                                           "alert": True,
                                           'community': testCommunityPath,
                                           'body': 'BBB'
                                           })
        
        # Add 2 news items (C and D)
        news_item_C = self.helper.objCreateViaRest("/scs/news_item/",
                                          {'owner': "/scs/user/" + str(user.id) + "/",
                                           'release_date_time': str(sm_date_now()),
                                           'headline': 'Awesome Headline C',
                                           "alert": False,
                                           'community': allCommunityPath,
                                           'body': 'CCC'
                                           })
        news_item_D = self.helper.objCreateViaRest("/scs/news_item/",
                                          {'owner': "/scs/user/" + str(user.id) + "/",
                                           'release_date_time': str(sm_date_now()),
                                           'headline': 'Awesome Headline D',
                                           "alert": False,
                                           'community': testCommunityPath,
                                           'body': 'DDD'
                                           })

        # Add expired alert - will be ignored in alert and news listing      
        self.helper.objCreateViaRest("/scs/news_item/",
                                     {'owner': "/scs/user/" + str(user.id) + "/",
                                      'release_date_time': str(sm_date_now()),
                                      'expiry_date_time': (str(sm_date_now() - datetime.timedelta(seconds=1))),
                                      'headline': 'Awesome Headline E',
                                      "alert": True,
                                      'community': testCommunityPath,
                                      'body': 'EEE'
                                      })
        
        # Get first system alert
        resp = self.helper.objGetViaRest("/scs/news_item/getsystemalert/?communityId=" + testCommunityIdString)
        self.assertEqual("BBB", resp[0]["body"])

        # Pretend user acked, get next alert
        resp = self.helper.objGetViaRest("/scs/news_item/getsystemalert/?communityId=" + testCommunityIdString 
                                         + "&lastIdAcked=" + str(resp[0]["id"]))
        self.assertEqual("AAA", resp[0]["body"])

        # Ack this one, next call returns no item
        resp = self.helper.objGetViaRest("/scs/news_item/getsystemalert/?communityId=" + testCommunityIdString 
                                         + "&lastIdAcked=" + str(resp[0]["id"]))
        self.assertEqual(0, len(resp))

        # Get first system alert with all community
        resp = self.helper.objGetViaRest("/scs/news_item/getsystemalert/?communityId=" + communityIdString)
        self.assertEqual("AAA", resp[0]["body"])

        # Pretend user acked, get next alert - expect no other as this alert is for testCommunity only
        resp = self.helper.objGetViaRest("/scs/news_item/getsystemalert/?communityId=" + communityIdString 
                                         + "&lastIdAcked=" + str(resp[0]["id"]))
        self.assertEqual(0, len(resp))
        
        # Get all news, expect 4 and check order
        resp = self.helper.objGetViaRest("/scs/news_item/getmynews/?communityId=" + testCommunityIdString)
        self.assertEqual(4, len(resp))
        self.assertEqual(news_item_D, resp[0]["resource_uri"])
        self.assertEqual(news_item_C, resp[1]["resource_uri"])
        self.assertEqual(news_item_B, resp[2]["resource_uri"])
        self.assertEqual(news_item_A, resp[3]["resource_uri"])
        
        # Get newest 2, get D and C
        resp = self.helper.objGetViaRest("/scs/news_item/getmynews/?communityId=" + testCommunityIdString +
                                         "&limit=2")
        self.assertEqual(2, len(resp))
        self.assertEqual(news_item_D, resp[0]["resource_uri"])
        self.assertEqual(news_item_C, resp[1]["resource_uri"])

        # Get next 2, get B and A
        resp = self.helper.objGetViaRest("/scs/news_item/getmynews/?communityId=" + testCommunityIdString +
                                         "&limit=2&excludeAfterId=" + str(resp[1]["id"]))
        self.assertEqual(2, len(resp))
        self.assertEqual(news_item_B, resp[0]["resource_uri"])
        self.assertEqual(news_item_A, resp[1]["resource_uri"])
        
        # Get with another community, returns only A and C
        resp = self.helper.objGetViaRest("/scs/news_item/getmynews/?communityId=" + communityIdString +
                                         "&limit=2")
        self.assertEqual(2, len(resp))
        self.assertEqual(news_item_C, resp[0]["resource_uri"])
        self.assertEqual(news_item_A, resp[1]["resource_uri"])
        
    def test_reservation(self):
        # Create, get, delete and get again the reservation objects
        now = sm_date_now()
        self.helper.setupModel()
        user = self.helper.user
        self.helper.setupRestAuth(user.username)
        assign_perm("sm_controller.add_smmodelresource", user)

        resource_path = "/scs/resource/1/"
        resource2_path = self.helper.objCreateViaRest("/scs/resource/",
                                          {'name': "abc",
                                           'parametersJson': json.dumps({"folder": "tmp/"}),
                                           'capacityJson': json.dumps({}),
                                           'container': "/scs/container/1/",
                                           'resource_type': "/scs/resource_type/1/",
                                           'description': 'body body body body'
                                           })

        # Attempt to create reservation which conflicts due to end time earlier than start time
        self.assertRaises(AssertionError,
                          self.helper.objCreateViaRest,
                          "/scs/reservation/",
                          {'owner': "/scs/user/99/",
                           'resource': resource_path,
                           'start_time': str(now - timedelta(seconds=1)),
                           'end_time': str(now - timedelta(seconds=9)),
                           'description': 'body body body'})

        res1 = self.helper.objCreateViaRest("/scs/reservation/",
                                            {'owner': "/scs/user/99/",
                                             'resource': resource_path,
                                             'start_time': str(now),
                                             'end_time': str(now + timedelta(seconds=10)),
                                             'description': 'body body body'})
        respDict = self.helper.objGetViaRest(res1)
        self.assertEqual(respDict["owner"], "/scs/user/" + str(user.id) + "/")
        self.assertEqual(respDict["description"], "body body body")
        self.assertEqual(respDict["resource"]["resource_uri"], resource_path)
        
        # Attempt to create another reservation before this one
        self.helper.objCreateViaRest("/scs/reservation/",  
                                     {'owner': "/scs/user/99/",
                                      'resource': resource_path,
                                      'start_time': str(now - timedelta(seconds=10)),
                                      'end_time': str(now),
                                      'description': 'body body body'})
        
        # Attempt to create another reservation after this one
        self.helper.objCreateViaRest("/scs/reservation/",
                                     {'owner': "/scs/user/99/",
                                      'resource': resource_path,
                                      'start_time': str(now + timedelta(seconds=10)),
                                      'end_time': str(now + timedelta(seconds=20)),
                                      'description': 'body body body'})

        # Attempt to create a resource within the first one in time, but on another resource
        self.helper.objCreateViaRest("/scs/reservation/",
                                     {'owner': "/scs/user/99/",
                                      'resource': resource2_path,
                                      'start_time': str(now + timedelta(seconds=1)),
                                      'end_time': str(now + timedelta(seconds=2)),
                                      'description': 'body body body'})

        # Attempt to create reservation just like the previous one - expect failure
        resp = self.client.post("/scs/reservation/",
                                json.dumps({'owner': "/scs/user/99/",
                                            'resource': resource_path,
                                            'start_time': str(now + timedelta(seconds=1)),
                                            'end_time': str(now + timedelta(seconds=2)),
                                            'description': 'body body body'}),
                                "application/json", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 409, resp.content)
        respDict = json.loads(resp.content.decode("utf-8"))

        self.assertEqual("/scs/reservation/1/", respDict["conflictingReservation"], resp.content.decode("utf-8"))

        # Attempt to create reservation which conflicts due to bad end time
        self.assertRaises(AssertionError,
                          self.helper.objCreateViaRest,
                          "/scs/reservation/",
                          {'owner': "/scs/user/99/",
                           'resource': resource_path,
                           'start_time': str(now - timedelta(seconds=100)),
                           'end_time': str(now - timedelta(seconds=9)),
                           'description': 'body body body'})

        # Attempt to create reservation which conflicts due to bad start time
        self.assertRaises(AssertionError,
                          self.helper.objCreateViaRest,
                          "/scs/reservation/",
                          {'owner': "/scs/user/99/",
                           'resource': resource_path,
                           'start_time': str(now + timedelta(seconds=19)),
                           'end_time': str(now + timedelta(seconds=90)),
                           'description': 'body body body'})

        # Attempt to create reservation which conflicts due to time within reservation
        self.assertRaises(AssertionError,
                          self.helper.objCreateViaRest,
                          "/scs/reservation/",
                          {'owner': "/scs/user/99/",
                           'resource': resource_path,
                           'start_time': str(now + timedelta(seconds=5)),
                           'end_time': str(now + timedelta(seconds=6)),
                           'description': 'body body body'})

        # Attempt to create reservation which conflicts due times which bracket a reservation
        self.assertRaises(AssertionError,
                          self.helper.objCreateViaRest,
                          "/scs/reservation/",
                          {'owner': "/scs/user/99/",
                           'resource': resource_path,
                           'start_time': str(now - timedelta(seconds=999)),
                           'end_time': str(now + timedelta(seconds=999)),
                           'description': 'body body body'})

        respDict = self.helper.objDeleteViaRest(res1)
        resp = self.client.get(res1, **self.helper.auth_header)
        self.assertEqual(resp.status_code, 404)
                
    def test_taskQueueing(self):
        self.helper.setupModel()
        root = User.objects.get(username="root")        
        self.helper.setupRestAuth("root")
        self.helper.setupRemoteAgentViaIce()
        self.resource = SmModelResource.objects.get(name="resourcexxx")

        queuedTaskStarter = ScsQueuedTaskStarter(0)
        
        self.helper.makeTaskTypeSleep()
        
        # Create a task which runs now for 1 seconds
        task1 = self.helper.makeSleepTask(root, 2)
        resp = self.client.get("/scs/task/" + str(task1.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        
        # Create another task on the same resource (expect 409 - conflict)
        task1a = self.helper.makeSleepTask(root, 1)
        resp = self.client.get("/scs/task/" + str(task1a.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 409)
        
        # Create another task on the same resource with queueingAllowed (expect queueing state)
        task1b = self.helper.makeSleepTask(root, 1)
        resp = self.client.get("/scs/task/" + str(task1b.pk) + "/?action=start&allowQueueing=True", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual("queued", self.helper.getTask(task1b.id)["state"])
        
        # Wait until task1 is done
        self.helper.waitForTaskDone(task1.id)
        
        # Run the queued task checker - expect to see queued task promoted
        queuedTaskStarter.doOneCheck()

        # Poll until task completes
        self.helper.waitForTaskDone(task1b.id)

        # Create a reservation on another resource for 1 seconds
        now = sm_date_now()
        self.helper.setupRestAuth("userxxx")
        userxxx = User.objects.get(username="userxxx")
        res1 = self.helper.objCreateViaRest("/scs/reservation/",
                                            {'owner': "/scs/user/" + str(userxxx.id) + "/",
                                             'resource': "/scs/resource/" + str(self.resource.id) + "/",
                                             'start_time': str(now),
                                             'end_time': str(now + timedelta(seconds=2)),
                                             'description': 'body body body'})
        
        # Start a task on the reserved resource (expect 409 - conflict)
        self.helper.setupRestAuth("root")
        task2 = self.helper.makeSleepTask(root, 1)
        resp = self.client.get("/scs/task/" + str(task2.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 409)
        
        # Start a task on the reserved resource with queueingAllows (expect queueing state)
        task2b = self.helper.makeSleepTask(root, 1)
        resp = self.client.get("/scs/task/" + str(task2b.pk) + "/?action=start&allowQueueing=True", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual("queued", self.helper.getTask(task2b.id)["state"])
        
        time.sleep(0.1)
        # Set reservation end time to the time at the start of the test
        self.helper.objModifyViaRest(res1,
                                     {'end_time': str(now)})
        # Run the queued task checker - expect to see a task promoted
        queuedTaskStarter.doOneCheck()
        
        # Poll the queued task for completion
        self.helper.waitForTaskDone(task2b.id)
           
    def test_scriptTbl(self):
        # Create, get, modify, get, delete and get again the script objects
        self.helper.setupModel()
        self.helper.setupRestAuth("root")

        script_item = self.helper.objCreateViaRest("/scs/script/",
                                          {'key': "abc",
                                           'owner': "/scs/user/" + str(self.helper.user.id) + "/",
                                           'value': "hello"
                                           })
        respDict = self.helper.objGetViaRest(script_item)
        self.assertEqual(respDict["key"], "abc")
        self.assertEqual(respDict["value"], "hello")
        self.helper.objModifyViaRest(script_item, {'key': "abc", 'value': "xyz"})
        respDict = self.helper.objGetViaRest(script_item)
        self.assertEqual(respDict["key"], "abc")
        self.assertEqual(respDict["value"], "xyz")
        respDict = self.helper.objDeleteViaRest(script_item)
        resp = self.client.get(script_item, **self.helper.auth_header)
        self.assertEqual(resp.status_code, 404)
                
    def test_propertyTbl(self):
        # Create, get, modify, get, delete and get again the property objects
        self.helper.setupModel()
        self.helper.setupRestAuth("root")

        property_item = self.helper.objCreateViaRest("/scs/property/",
                                          {'key': "abc",
                                           'value': "hello"
                                           })
        respDict = self.helper.objGetViaRest(property_item)
        self.assertEqual(respDict["key"], "abc")
        self.assertEqual(respDict["value"], "hello")
        self.helper.objModifyViaRest(property_item, {'key': "abc", 'value': "xyz"})
        respDict = self.helper.objGetViaRest(property_item)
        self.assertEqual(respDict["key"], "abc")
        self.assertEqual(respDict["value"], "xyz")
        respDict = self.helper.objDeleteViaRest(property_item)
        resp = self.client.get(property_item, **self.helper.auth_header)
        self.assertEqual(resp.status_code, 404)
                
    def test_community(self):
        self.helper.setupModel()
        self.helper.setupRestAuth("root")
        # Create, get, delete and get again the community object
        user = self.helper.user
        user_a = User.objects.get(username="userxxx")
        user_root = User.objects.get(username="root")
        resource = list(SmModelResource.objects.all())[0]
        r = SmModelResource()
        r.resource_type = resource.resource_type
        r.name = "r"
        r.owner = user
        r.container = resource.container
        r.description = test_helper.DUMMY_DESCRIPTION
        r.parametersJson = "{}"
        r.save()
    
        # Create community object
        community_path = self.helper.objCreateViaRest("/scs/community/",
                                               {'name': 'Test Community',
                                                'name_key': 'test_community',
                                                'description': "This is the test community"})
        respDict = self.helper.objGetViaRest(community_path)
        self.assertEqual(respDict["name"], "Test Community")
        self.assertEqual(respDict["name_key"], "test_community")
        tag_path = respDict["tag"]
        self.assertTrue(tag_path != "")
        self.assertTrue(("user/" + str(user_root.id)) in respDict["owner"])

        # Check tag created for community
        respDict = self.helper.objGetViaRest(tag_path)
        self.assertEqual(respDict["tag_name"], "community:test_community")
        community_tag = SmModelTag.objects.get(id=respDict["id"])
        # Query resources in community, expect none
        taglist = self.helper.objGetViaRest("/scs/resource_tag/?tag__tag_name=community:test_community")
        self.assertEqual(0, len(taglist["objects"]))
        
        # Tag resource as part of community
        rtag = SmModelResourceTag()
        rtag.resource = r
        rtag.tag = community_tag
        rtag.save()
        
        # Query task types in community, expect 1
        taglist = self.helper.objGetViaRest("/scs/resource_tag/?tag__tag_name=community:test_community")
        self.assertEqual(1, len(taglist["objects"]))
        tag_path = taglist["objects"][0]["resource_uri"]
        
        # Test that user settings can be accessed and that community defaults to all
        user_setting = self.helper.objGetViaRest("/scs/user_setting/" + str(user_a.pk) + "/")
        self.assertEqual("/scs/community/1/", user_setting["community"]["resource_uri"])
        
        # Test that community can be changed to the newly created community
        self.helper.objModifyViaRest("/scs/user_setting/" + str(user_a.pk) + "/",
                                     {'community': community_path})
        
        # Test that user settings returns the new community
        user_setting = self.helper.objGetViaRest("/scs/user_setting/" + str(user_a.pk) + "/")
        self.assertEqual(community_path, user_setting["community"]["resource_uri"])

        # Modify the user to check if an additional user_setting object is added
        prevUserSettingCount = self.helper.objGetViaRest("/scs/user_setting/")["meta"]["total_count"]
        self.helper.objModifyViaRest("/scs/user/" + str(user_a.pk) + "/", {"first_name": "yyy"})    
        newCount = self.helper.objGetViaRest("/scs/user_setting/")["meta"]["total_count"]
        self.assertEqual(prevUserSettingCount, newCount)
        
        # Make user_setting point to the community
        self.helper.objModifyViaRest(user_setting["resource_uri"], {"community": community_path})
        
        respDict = self.helper.objDeleteViaRest(community_path)
        resp = self.client.get(community_path, **self.helper.auth_header)
        self.assertEqual(resp.status_code, 404)
        # Check that the user_setting was not deleted and now points to the everything community
        resp = self.helper.objGetViaRest(user_setting["resource_uri"])
        self.assertEqual(resp["community"]["name_key"], SM_COMMUNITY_ALL_NAME_KEY)

        resp = self.client.get(tag_path, **self.helper.auth_header)
        self.assertEqual(resp.status_code, 404)
        
    def test_communityListing(self):
        # Test that resources and task types can be listed filtered
        # by community
        self.helper.setupModel()
        self.helper.setupRestAuth("root")
        user = self.helper.user
        
        # Make task type 2.  Only resource 2 uses this task type so the 
        # the task type shows up in listing of c1 and c2 communities
        ttype2 = SmModelTaskType()
        ttype2.name = "Main Task Type2"
        ttype2.name_key = "tt2"
        ttype2.configurationJson = json.dumps({"args": []})
        ttype2.description = test_helper.DUMMY_DESCRIPTION
        ttype2.owner = self.helper.user
        ttype2.save()
        rtype2 = SmModelResourceType()
        rtype2.name = "ResourceType2"
        rtype2.nature = "data"
        rtype2.description = test_helper.DUMMY_DESCRIPTION
        rtype2.save()
    
        rttt2 = SmModelResourceTypeTaskType()
        rttt2.resource_type = rtype2
        rttt2.task_type = ttype2
        rttt2.role = "compute"
        rttt2.name = "compute_rttt2"
        rttt2.description = test_helper.DUMMY_DESCRIPTION
        rttt2.save()        
        
        resource = list(SmModelResource.objects.all())[0]
        r1 = SmModelResource()
        r1.resource_type = resource.resource_type
        r1.name = "r1"
        r1.owner = user
        r1.container = resource.container
        r1.description = test_helper.DUMMY_DESCRIPTION
        r1.parametersJson = "{}"
        r1.save()
        r2 = SmModelResource()
        r2.resource_type = rtype2
        r2.name = "r2"
        r2.owner = user
        r2.container = resource.container
        r2.parametersJson = "{}"
        r2.description = test_helper.DUMMY_DESCRIPTION
        r2.save()
        r3 = SmModelResource()
        r3.resource_type = resource.resource_type
        r3.name = "r3"
        r3.owner = user
        r3.container = resource.container
        r3.description = test_helper.DUMMY_DESCRIPTION
        r3.parametersJson = "{}"
        r3.save()        
    
        # Create community1 object
        self.helper.objCreateViaRest("/scs/community/",
                     {'name': 'Test Community',
                      'name_key': 'c1',
                      'leader': "/scs/user/" + str(user.id) + "/",
                      'admin': "/scs/user/" + str(user.id) + "/",
                      'description': "This is the test community"})        
        # Create tag for community1
        community1_tag = SmModelTag.objects.get(tag_name="community:c1")
        
        # Create community2 object
        self.helper.objCreateViaRest("/scs/community/",
                                     {'name': 'Test Community 2',
                                      'name_key': 'c2',
                                      'leader': "/scs/user/" + str(user.id) + "/",
                                      'admin': "/scs/user/" + str(user.id) + "/",
                                      'description': "This is the test community"})        
        # Create tag for community1
        community2_tag = SmModelTag.objects.get(tag_name="community:c2")
        
        # Tag resource r1 as part of community c1 and c2
        rtag = SmModelResourceTag()
        rtag.resource = r1
        rtag.tag = community1_tag
        rtag.save()
        rtag = SmModelResourceTag()
        rtag.resource = r1
        rtag.tag = community2_tag
        rtag.save()
        
        # Tag resource r2 as part of community c2
        rtag = SmModelResourceTag()
        rtag.resource = r2
        rtag.tag = community2_tag
        rtag.save()

        # Query resource types in community c1, expect 1
        taglist = self.helper.objGetViaRest("/scs/resource/?communityFilter=c1")
        self.assertEqual(1, len(taglist["objects"]))
        
        # Query resources in community c2, expect 2
        taglist = self.helper.objGetViaRest("/scs/resource/?communityFilter=c2")
        self.assertEqual(2, len(taglist["objects"]))                
                
        # Query resources in community c3, expect 0
        taglist = self.helper.objGetViaRest("/scs/resource/?communityFilter=c3")
        self.assertEqual(0, len(taglist["objects"]))                
                
        # Query task types in community c1, expect 0
        taglist = self.helper.objGetViaRest("/scs/task_type/?communityFilter=c1")
        self.assertEqual(2, len(taglist["objects"]))
        
        # Query task types in community c2, expect 1
        taglist = self.helper.objGetViaRest("/scs/task_type/?communityFilter=c2")
        self.assertEqual(3, len(taglist["objects"]))                
                
        # Query task types in community c99, expect error
        self.assertRaises(Exception,
                          self.helper.objGetViaRest("/scs/task_type/?communityFilter=c99"))
                           
    def test_resourceSorting(self):
        # Test that resources and task types can be listed filtered
        # by community
        self.helper.setupModel()
        self.helper.setupRestAuth("root")
        user = self.helper.user
        
        # Make task type 2.  Only resource 2 uses this task type so the 
        # the task type shows up in listing of c1 and c2 communities
        ttype2 = SmModelTaskType()
        ttype2.name = "Main Task Type2"
        ttype2.name_key = "tt2"
        ttype2.configurationJson = json.dumps({"args": []})
        ttype2.owner = self.helper.user        
        ttype2.description = test_helper.DUMMY_DESCRIPTION
        ttype2.owner = self.helper.user
        ttype2.save()
        rtype2 = SmModelResourceType()
        rtype2.name = "ResourceType2"
        rtype2.nature = "data"
        rtype2.description = test_helper.DUMMY_DESCRIPTION
        rtype2.save()
    
        rttt2 = SmModelResourceTypeTaskType()
        rttt2.resource_type = rtype2
        rttt2.task_type = ttype2
        rttt2.role = "compute"
        rttt2.name = "compute_rttt2"
        rttt2.description = test_helper.DUMMY_DESCRIPTION
        rttt2.save()        
        
        resource = list(SmModelResource.objects.all())[0]
        r1 = SmModelResource()
        r1.resource_type = resource.resource_type
        r1.name = "r1"
        r1.owner = user
        r1.container = resource.container
        r1.description = test_helper.DUMMY_DESCRIPTION
        r1.parametersJson = "{}"
        r1.save()
        r2 = SmModelResource()
        r2.resource_type = rtype2
        r2.name = "r2"
        r2.owner = user
        r2.container = resource.container
        r2.parametersJson = "{}"
        r2.description = test_helper.DUMMY_DESCRIPTION
        r2.save()
        r3 = SmModelResource()
        r3.resource_type = resource.resource_type
        r3.name = "r3"
        r3.owner = user
        r3.container = resource.container
        r3.description = test_helper.DUMMY_DESCRIPTION
        r3.personalFolder = True
        r3.parametersJson = "{}"
        r3.save()        
        r4 = SmModelResource()
        r4.resource_type = rtype2
        r4.name = "r4"
        r4.owner = user
        r4.container = resource.container
        r4.parametersJson = "{}"
        r4.description = test_helper.DUMMY_DESCRIPTION
        r4.save()
    
        rf4 = SmModelFavouriteResource()
        rf4.resource = r4
        rf4.owner = user
        rf4.save()
        
        rf2 = SmModelFavouriteResource()
        rf2.resource = r2
        rf2.owner = self.helper.superuser
        rf2.save()
        
        # Create community1 object
        rlist = self.helper.objGetViaRest("/scs/resource/?sortForUser=" + str(user.pk))["objects"]     
        self.assertEqual(5, len(rlist))
        # r3 is first because it is the personal folder
        self.assertEqual("r3", rlist[0]["name"])
        # r4 is next because it is a favorite folder
        self.assertEqual("r4", rlist[1]["name"])
        # r1 then r2 sorted alphabetically
        self.assertEqual("r1", rlist[2]["name"])
        self.assertEqual("r2", rlist[3]["name"])

        rlist = self.helper.objGetViaRest("/scs/resource/?sortForUser=" + str(user.pk + 99))["objects"] 
        self.assertEqual(5, len(rlist))
        self.assertEqual("resourcexxx", rlist[0]["name"])
        self.assertEqual("r1", rlist[1]["name"])
        self.assertEqual("r2", rlist[2]["name"])
        self.assertEqual("r3", rlist[3]["name"])
        self.assertEqual("r4", rlist[4]["name"])

    def test_deletes_banned(self):
        """ Create set of objects"""
        u = User(username="userxxx")
        u.save()
        api_key = ApiKey.objects.get(user_id=u).key
        api_key_header = {"HTTP_AUTHORIZATION": "ApiKey " + u.username + ":" + api_key}
        ttype = SmModelTaskType()
        ttype.name = "tasktypexxx"
        ttype.description = test_helper.DUMMY_DESCRIPTION
        ttype.configurationJson = '{ "args": []}'
        ttype.owner = u
        ttype.save()

        rtype = SmModelResourceType()
        rtype.name = "resourcetypexxx"
        rtype.description = test_helper.DUMMY_DESCRIPTION
        rtype.save()

        rttt = SmModelResourceTypeTaskType()
        rttt.task_type = ttype
        rttt.resource_type = rtype
        rttt.metadataJson = json.dumps({})
        rttt.name = "rttt name"
        rttt.description = test_helper.DUMMY_DESCRIPTION
        rttt.save()

        agent = SmModelAgent()
        agent.guid = "111"
        agent.owner = u
        agent.last_change = str(sm_date_now())
        agent.status = "down"
        agent.name = "a 1 name"
        agent.description = test_helper.DUMMY_DESCRIPTION
        agent.save()
        
        container = SmModelContainer()
        container.agent = agent
        container.status = "up"
        container.name = "test_container"
        container.description = "test container description"
        container.owner = u
        container.save()
    
        r = SmModelResource()
        r.resource_type = rtype
        r.name = "resourcexxx"
        r.owner = u
        r.container = container
        r.description = test_helper.DUMMY_DESCRIPTION
        r.parametersJson = "{}"
        r.save()
        
        t = SmModelTask()
        t.src_resource = r
        t.dst_resource = r
        t.owner = u
        t.task_type = ttype
        t.status = "abc"
        t.start_time = str(sm_date_now())
        t.end_time = str(sm_date_now())
        t.parametersJson = '{}'
        t.save()

        taskResult = SmModelTaskResource()
        taskResult.task = t
        taskResult.resource = r
        taskResult.resource_type_task_type = rttt
        taskResult.role = "role"
        taskResult.owner = u
        taskResult.save()
        
        tag2 = SmModelTag()
        tag2.tag_name = "tagxxx"
        tag2.save()
                
        self.assertEqual(1, SmModelTaskResource.objects.all().count())
        # Try a delete
        resp = self.client.get("/scs/task/", **api_key_header)
        self.assertEqual(resp.status_code, 200)
        
        resp = self.client.get("/scs/task_resource/", **api_key_header)
        self.assertEqual(resp.status_code, 200)
        
        # Try a delete
        resp = self.client.delete("/scs/task/" + str(t.id) + "/", **api_key_header)
        self.assertEqual(resp.status_code, 204)

        # Try a delete
        resp = self.client.delete("/scs/tag/" + str(tag2.id) + "/", **api_key_header)
        self.assertNotEqual(resp.status_code, 204)

    def testUpload(self):
        self.helper.tempFolder.mkdir("uploadFolder")
        scsApiSetAuthOn(False)
        self.assertFalse(scsApiGetAuthOn())
            
        """ Create set of objects"""
        u = User(username="userxxx")
        u.save()
        rtype = SmModelResourceType()
        rtype.name = "resourcetypexxx"
        rtype.description = test_helper.DUMMY_DESCRIPTION
        rtype.save()

        agent = SmModelAgent()
        agent.guid = "111"
        agent.owner = u
        agent.last_change = str(sm_date_now())
        agent.status = "up"
        agent.name = "test_agent"
        agent.description = test_helper.DUMMY_DESCRIPTION
        agent.save()
        
        container = SmModelContainer()
        container.agent = agent
        container.status = "up"
        container.name = "test_container"
        container.description = "test container description"
        container.containerUrl = "local://localhost" + self.helper.tempFolder.folder + "/"
        container.description = test_helper.DUMMY_DESCRIPTION
        container.owner = u
        container.save()
    
        self.helper.setupRemoteAgentViaIce()

        r = SmModelResource()
        r.resource_type = rtype
        r.name = "resourcexxx"
        r.owner = u
        r.container = container
        r.parametersJson = json.dumps({"folder": "r_folder/"})
        r.description = test_helper.DUMMY_DESCRIPTION
        r.status = "up"
        r.save()
        
        resource_path = "/scs/resource/" + str(r.id) + "/"
        
        self.helper.tempFolder.writeFile("f1", "12345")
        with open(self.helper.tempFolder.mkAbsolutePath("f1"), "rb") as fp:
            resp = self.client.post('/scs/upload/', {'name': 'fred',
                                                     'uploadfile': fp})    
        self.assertEqual(resp.status_code, 404)
 
        # Try with missing folder
        with open(self.helper.tempFolder.mkAbsolutePath("f1"), "rb") as fp:
            resp = self.client.post(resource_path + 'folder/file/upload/',
                                    {'name': 'fred',
                                     'uploadfile': fp})    
        self.assertEqual(resp.status_code, 422, resp.content)
        
        # Make directory without parent folders and without create parents, expect fail
        resp = self.client.post(resource_path + 'f1/f2/f3/mkdir/')
        self.assertEqual(resp.status_code, 422, resp.content)
        
        # Make directory with create parents
        resp = self.client.post(resource_path + 'f1/f2/f3/mkdir/?makeParentFolders=True')
        self.assertEqual(resp.status_code, 200, resp.content)
        
        # Make directory
        resp = self.client.post(resource_path + 'folder/mkdir/')
        self.assertEqual(resp.status_code, 200, resp.content)
        
        # Try with directory
        with open(self.helper.tempFolder.mkAbsolutePath("f1"), "rb") as fp:
            resp = self.client.post(resource_path + 'folder/file/upload/',
                                    {'name': 'fred',
                                     'uploadfile': fp})    
        self.assertEqual(resp.status_code, 200, resp.content)
        uploadedData = self.helper.tempFolder.readFile("r_folder/folder/file", 1000)
        self.assertEqual(uploadedData, b"12345")
 
        # Try overwriting file - expect failure
        resp = self.client.post(resource_path + '.rpsmarf/tmp/mkdir/?makeParentFolders=True')
        self.assertEqual(resp.status_code, 200, resp.content)
        with open(self.helper.tempFolder.mkAbsolutePath("f1"), "rb") as fp:
            resp = self.client.post(resource_path + 'folder/file/upload/',
                                    {'name': 'fred',
                                     'uploadfile': fp})    
        self.assertEqual(resp.status_code, 422, resp.content)

        # Write overwriting with overwrite flag set
        self.helper.tempFolder.writeFile("f2", "98765")
        with open(self.helper.tempFolder.mkAbsolutePath("f2"), "rb") as fp:
            resp = self.client.post(resource_path + 'folder/file/upload/?overwrite=True',
                                    {'name': 'fred',
                                     'uploadfile': fp})    
        self.assertEqual(resp.status_code, 200, resp.content.decode("utf-8"))
        uploadedData = self.helper.tempFolder.readFile("r_folder/folder/file", 1000)
        self.assertEqual(uploadedData, b"98765")

    def testParameterAggregation(self):
        self.helper.setupModel()
        self.helper.setupRestAuth("userxxx")
        rtype = SmModelResourceType.objects.all()[0]
        container = SmModelContainer.objects.all()[0]

        ttype = SmModelTaskType()
        ttype.name = "Main Task Type 2"
        ttype.configurationJson = json.dumps({"args": [
                                                       {"type": "string",
                                                        "input_id": 1,
                                                        "name": "flags",
                                                        "description": test_helper.DUMMY_DESCRIPTION},
                                                       {"type": "resource",
                                                        "name": "Input file",
                                                        "input_id": 2,
                                                        "direction": "input",
                                                        "role": "src_resource"},
                                                       {"type": "resource",
                                                        "name": "Output file",
                                                        "input_id": 3,
                                                        "direction": "output",
                                                        "role": "dest_resource"}
                                                       ],
                                              "computeResourceRole": "compute"})
        ttype.owner = self.helper.user
        ttype.description = test_helper.DUMMY_DESCRIPTION
        ttype.owner = self.helper.user
        ttype.imageUrl = "myurl"
        ttype.save()
        rttt1 = SmModelResourceTypeTaskType()
        rttt1.resource_type = rtype
        rttt1.task_type = ttype
        rttt1.role = "src_resource"
        rttt1.name = "Source Resource"
        rttt1.description = test_helper.DUMMY_DESCRIPTION
        rttt1.save()
    
        rttt2 = SmModelResourceTypeTaskType()
        rttt2.resource_type = rtype
        rttt2.task_type = ttype
        rttt2.role = "dest_resource"
        rttt2.name = "Destination Resource"
        rttt2.description = test_helper.DUMMY_DESCRIPTION
        rttt2.save()
    
        rttt3 = SmModelResourceTypeTaskType()
        rttt3.resource_type = rtype
        rttt3.task_type = ttype
        rttt3.role = "compute"
        rttt3.name = "Compute Resource"
        rttt3.description = test_helper.DUMMY_DESCRIPTION
        rttt3.save()
    
        t = SmModelTask()
        t.owner = self.helper.user
        t.task_type = ttype
        t.status = "init"
        t.parametersJson = json.dumps({"input_ids": {
                                                    "2": {"path": "june_2014/"},
                                                    "3": {"path": "my_output/"},
                                                    "1": {"value": "-r"}
                                                    }
                                       })
        t.start_time = str(sm_date_now())
        t.end_time = str(sm_date_now())
        t.save()
            
        resource1 = SmModelResource()
        resource1.resource_type = rtype
        resource1.name = "ResourceName1"
        resource1.owner = self.helper.user
        resource1.container = container
        resource1.description = test_helper.DUMMY_DESCRIPTION
        resource1.parametersJson = "{}"
        resource1.save()
            
        resource2 = SmModelResource()
        resource2.resource_type = rtype
        resource2.name = "ResourceName2"
        resource2.owner = self.helper.user
        resource2.container = container
        resource2.description = test_helper.DUMMY_DESCRIPTION
        resource2.parametersJson = "{}"
        resource2.save()
            
        resource3 = SmModelResource()
        resource3.resource_type = rtype
        resource3.name = "ResourceName3"
        resource3.owner = self.helper.user
        resource3.container = container
        resource3.description = test_helper.DUMMY_DESCRIPTION
        resource3.parametersJson = "{}"
        resource3.save()
            
        taskResource1 = SmModelTaskResource()
        taskResource1.task = t
        taskResource1.resource = resource1
        taskResource1.resource_type_task_type = rttt1
        taskResource1.owner = self.helper.user
        taskResource1.save()
    
        taskResource2 = SmModelTaskResource()
        taskResource2.task = t
        taskResource2.resource = resource2
        taskResource2.resource_type_task_type = rttt2
        taskResource2.owner = self.helper.user
        taskResource2.save()
    
        taskResource3 = SmModelTaskResource()
        taskResource3.task = t
        taskResource3.resource = resource3
        taskResource3.resource_type_task_type = rttt3
        taskResource3.owner = self.helper.user
        taskResource3.save()
            
        apiResp = self.helper.objGetViaRest("/scs/task/" + str(t.id) + "/?aggregateParam=True")
        resp = json.loads(apiResp["parametersJson"])
        taskTypeName = resp["task_type_name"]
        self.assertEqual("Main Task Type 2", taskTypeName)
        self.assertEqual("myurl", resp["taskTypeImageUrl"])
        inputIdsDict = resp["input_ids"]
        self.assertEqual("-r", inputIdsDict["1"]["value"])
        self.assertEqual("june_2014/", inputIdsDict["2"]["path"])
        self.assertEqual("my_output/", inputIdsDict["3"]["path"])
        parametersDict = resp["parameters"]
        self.assertEqual("flags", parametersDict[0]["name"])
        self.assertEqual(1, parametersDict[0]["input_id"])
        self.assertEqual("string", parametersDict[0]["type"])
        self.assertEqual("-r", parametersDict[0]["value"])
        self.assertEqual(test_helper.DUMMY_DESCRIPTION, parametersDict[0]["description"])
        resources = resp["resources"]

        # First resource
        self.assertEqual("Source Resource", resources[0]["name"])
        self.assertEqual(2, resources[0]["input_id"])
        self.assertEqual("data", resources[0]["nature"])
        self.assertEqual("ResourceName1", resources[0]["resourceName"])
        self.assertEqual("/scs/resource/" + str(resource1.id) + "/", resources[0]["resourcePath"])
        self.assertEqual("input", resources[0]["direction"])
        self.assertEqual(test_helper.DUMMY_DESCRIPTION, resources[0]["description"])
        
        # Second resource
        self.assertEqual("Destination Resource", resources[1]["name"])
        self.assertEqual(3, resources[1]["input_id"])
        self.assertEqual("data", resources[1]["nature"])
        self.assertEqual("ResourceName2", resources[1]["resourceName"])
        self.assertEqual("/scs/resource/" + str(resource2.id) + "/", resources[1]["resourcePath"])
        self.assertEqual("output", resources[1]["direction"])
        self.assertEqual(test_helper.DUMMY_DESCRIPTION, resources[1]["description"])

        # Compute 
        self.assertEqual("compute", resources[2]["nature"])
        self.assertEqual("ResourceName3", resources[2]["resourceName"])
        self.assertEqual("/scs/resource/" + str(resource3.id) + "/", resources[2]["resourcePath"])

        apiResp = self.helper.objGetViaRest("/scs/task/?aggregateParam=True&orderByIdDesc=True")
        self.assertEqual(2, len(apiResp["objects"]))
        resp = json.loads(apiResp["objects"][0]["parametersJson"])
        taskTypeName = resp["task_type_name"]
        self.assertEqual("Main Task Type 2", taskTypeName)
        inputIdsDict = resp["input_ids"]
        self.assertEqual("-r", inputIdsDict["1"]["value"])
        self.assertEqual("june_2014/", inputIdsDict["2"]["path"])
        self.assertEqual("my_output/", inputIdsDict["3"]["path"])
        parametersDict = resp["parameters"]
        self.assertEqual("flags", parametersDict[0]["name"])
        self.assertEqual(1, parametersDict[0]["input_id"])
        self.assertEqual("string", parametersDict[0]["type"])
        self.assertEqual("-r", parametersDict[0]["value"])
        self.assertEqual(test_helper.DUMMY_DESCRIPTION, parametersDict[0]["description"])

        # Now ordered differently
        apiResp = self.helper.objGetViaRest("/scs/task/?aggregateParam=True")
        self.assertEqual(2, len(apiResp["objects"]))
        resp = json.loads(apiResp["objects"][1]["parametersJson"])
        taskTypeName = resp["task_type_name"]
        self.assertEqual("Main Task Type 2", taskTypeName)
        inputIdsDict = resp["input_ids"]
        self.assertEqual("-r", inputIdsDict["1"]["value"])
        self.assertEqual("june_2014/", inputIdsDict["2"]["path"])
        self.assertEqual("my_output/", inputIdsDict["3"]["path"])
        parametersDict = resp["parameters"]
        self.assertEqual("flags", parametersDict[0]["name"])
        self.assertEqual(1, parametersDict[0]["input_id"])
        self.assertEqual("string", parametersDict[0]["type"])
        self.assertEqual("-r", parametersDict[0]["value"])
        self.assertEqual(test_helper.DUMMY_DESCRIPTION, parametersDict[0]["description"])

        apiResp = self.helper.objGetViaRest("/scs/task/?aggregateParam=True&limit=1")
        oList = apiResp["objects"]
        self.assertEqual(1, len(oList))
        self.assertEqual("/scs/task_type/1/", oList[0]["task_type"])
        
        apiResp = self.helper.objGetViaRest("/scs/task/?aggregateParam=True&limit=1&offset=1")
        oList = apiResp["objects"]
        self.assertEqual(1, len(oList))
        self.assertEqual("/scs/task_type/3/", oList[0]["task_type"])
        
    def testTaskTypeExt(self):
        ''' 
        This test checks that the ext parameters does add the fields specified in the 
        documentation at https://rpsmarf.atlassian.net/wiki/display/RPS/REST+API+Special+Get+Operations
        '''
        self.helper.setupModel()
        self.helper.setupRestAuth("userxxx")
        user = self.helper.user
        task_type = SmModelTaskType.objects.get(name_key=SM_TASK_TYPE_DELETE_RECURSIVE_NAME_KEY)
        # Get with ext set - expect no ext fields
        info = self.helper.objGetViaRest("/scs/task_type/" + str(task_type.id) + "/?extForUser=" + str(user.id))
        self.assertTrue("ext_note" not in info)
        self.assertTrue("ext_tags" not in info)
        self.assertTrue("ext_favourite" not in info)

        # Add favourite
        fav = SmModelFavouriteTaskType(task_type=task_type, owner=user)
        fav.save()
        
        # Add note
        note = SmModelTaskTypeNote(task_type=task_type, owner=user, note="note text")
        note.save()
        
        # Add 2 tags
        tag1 = SmModelTag(tag_name="tag1")
        tag1.save()
        tag1Link = SmModelTaskTypeTag(task_type=task_type, tag=tag1)
        tag1Link.save()
        tag2 = SmModelTag(tag_name="tag2")
        tag2.save()
        tag2Link = SmModelTaskTypeTag(task_type=task_type, tag=tag2)
        tag2Link.save()
        
        # Get without ext set - expect no ext fields
        info = self.helper.objGetViaRest("/scs/task_type/" + str(task_type.id) + "/")
        self.assertTrue("ext_note" not in info)
        self.assertTrue("ext_tags" not in info)
        self.assertTrue("ext_favourite" not in info)
        
        # Get with ext, expect extra field
        info = self.helper.objGetViaRest("/scs/task_type/" + str(task_type.id) + "/?extForUser=" + str(user.id))
        self.assertEqual(info["ext_note"], "note text")
        self.assertEqual(len(info["ext_tags"]), 2)
        self.assertTrue("tag1" in info["ext_tags"])
        self.assertTrue("tag2" in info["ext_tags"])
        self.assertEqual(info["ext_favourite"], True)

        info = self.helper.objGetViaRest("/scs/task_type/" + str(task_type.id) +
                                         "/?extForUser=" + str(user.id) +
                                         "&showComputeResources=true")
        self.assertEqual(info["ext_note"], "note text")
        self.assertEqual(len(info["ext_tags"]), 2)
        self.assertTrue("tag1" in info["ext_tags"])
        self.assertTrue("tag2" in info["ext_tags"])
        self.assertEqual(info["ext_favourite"], True)
        self.assertTrue("compute_resources" in info)
        comp_res = info["compute_resources"]
        self.assertEqual(1, len(comp_res))
        self.assertEqual(comp_res[0]["name"], "resourcexxx")
        self.assertEqual(comp_res[0]["locked"], False)
        self.assertEqual(comp_res[0]["id"], 1)
        self.assertEqual(comp_res[0]["status"], "up")
        
    def testTaskList(self):
        self.helper.setupModel()
        self.helper.setupRestAuth("userxxx")
        user = self.helper.user

        # Make three task types with names a1, a2 and a3
        ttype_a2 = SmModelTaskType()
        ttype_a2.name = "A2"
        ttype_a2.configurationJson = json.dumps({"args": []})
        ttype_a2.description = DUMMY_DESCRIPTION
        ttype_a2.owner = user
        ttype_a2.save()
        ttype_a3 = SmModelTaskType()
        ttype_a3.name = "A3"
        ttype_a3.configurationJson = json.dumps({"args": []})
        ttype_a3.description = DUMMY_DESCRIPTION
        ttype_a3.owner = user
        ttype_a3.save()
        ttype_a1 = SmModelTaskType()
        ttype_a1.name = "A1"
        ttype_a1.configurationJson = json.dumps({"args": []})
        ttype_a1.description = DUMMY_DESCRIPTION
        ttype_a1.owner = user
        ttype_a1.save()
        
        # List - ensure that they are returned in order a1, a2, a3
        info = self.helper.objGetViaRest("/scs/task_type/?sortForUser=" + str(user.id))
        self.assertEqual(5, len(info["objects"]))
        self.assertEqual("A1", info["objects"][0]["name"])
        self.assertEqual("A2", info["objects"][1]["name"])
        self.assertEqual("A3", info["objects"][2]["name"])

        # Favourite a3
        fav = SmModelFavouriteTaskType(task_type=ttype_a3, owner=user)
        fav.save()

        # List - ensure that they are returned in order a3, a1, a2
        info = self.helper.objGetViaRest("/scs/task_type/?sortForUser=" + str(user.id))
        self.assertEqual(5, len(info["objects"]))
        self.assertEqual("A3", info["objects"][0]["name"])
        self.assertEqual("A1", info["objects"][1]["name"])
        self.assertEqual("A2", info["objects"][2]["name"])
           
    def testResourceExt(self):
        ''' 
        This test checks that the ext parameters does add the fields specified in the 
        documentation at https://rpsmarf.atlassian.net/wiki/display/RPS/REST+API+Special+Get+Operations
        '''
        self.helper.setupModel()
        self.helper.setupRestAuth("userxxx")
        user = self.helper.user
        resource = SmModelResource.objects.all().first()
        # Get with ext set - expect no ext fields
        info = self.helper.objGetViaRest("/scs/resource/" + str(resource.id) + "/?extForUser=" + str(user.id))
        self.assertTrue("ext_note" not in info, json.dumps(info))
        self.assertTrue("ext_tags" in info, json.dumps(info))
        self.assertEqual("tagxxx", info["ext_tags"][0], json.dumps(info))
        
        self.assertTrue("ext_favourite" not in info, json.dumps(info))

        # Add favourite
        fav = SmModelFavouriteResource(resource=resource, owner=user)
        fav.save()
        
        # Add note
        note = SmModelResourceNote(resource=resource, owner=user, note="note text")
        note.save()
        
        # Add 2 tags
        tag1 = SmModelTag(tag_name="tag1")
        tag1.save()
        tagLink1 = SmModelResourceTag(resource=resource, tag=tag1)
        tagLink1.save()
        tag2 = SmModelTag(tag_name="tag2")
        tag2.save()
        tagLink2 = SmModelResourceTag(resource=resource, tag=tag2)
        tagLink2.save()
        
        # Get without ext set - expect no ext fields
        info = self.helper.objGetViaRest("/scs/resource/" + str(resource.id) + "/")
        self.assertTrue("ext_note" not in info)
        self.assertTrue("ext_tags" not in info)
        self.assertTrue("ext_favourite" not in info)
        
        # Get with ext, expect extra field
        info = self.helper.objGetViaRest("/scs/resource/" + str(resource.id) + "/?extForUser=" + str(user.id))
        self.assertEqual(info["ext_note"], "note text")
        self.assertEqual(len(info["ext_tags"]), 3)
        self.assertTrue("tag1" in info["ext_tags"])
        self.assertTrue("tag2" in info["ext_tags"])
        self.assertTrue("tagxxx" in info["ext_tags"])
        self.assertEqual(info["ext_favourite"], True)
        
    def testStdoutStdErr(self):
        self.helper.setupModel()
        self.helper.setupRestAuth("root")
        asyncRespObj = SmTaskUtilsAsyncResps()
        mockRaServer = SmMockAsyncOpsServer(asyncRespObj)
        smRemoteAgentAsyncOperationFactory.setMockAsyncOpsServer(mockRaServer)
        os.environ[SM_ENV_VAR_COMMON_MOCK_OP_HANDLER_ENABLE] = "True"
        os.environ[SCS_ENV_VAR_STDOUT_LIMIT_IN_DB] = "11"
        os.environ[SCS_ENV_VAR_STDERR_LIMIT_IN_DB] = "12" 
       
        # Find task
        self.helper.setupRestAuth(self.helper.user.username)
        task = list(SmModelTask.objects.all())[0]
        self.client.get("/scs/task/" + str(task.pk) + "/?action=start",
                        **self.helper.auth_header)
        mockHandler = mockRaServer.getHandler(str(task.pk))
        for name in ["stdout", "stderr"]:
            if name == "stdout":
                param = "maxStdout"
            else:
                param = "maxStderr"
            # Check stdxxx empty
            tDict = self.helper.objGetViaRest("/scs/task/" + str(task.id) + "/")
            self.assertEqual("", tDict[name])
            tDict = self.helper.objGetViaRest("/scs/task/" + str(task.id) + "/?" + param + "=0")
            self.assertEqual("", tDict[name])
            tDict = self.helper.objGetViaRest("/scs/task/" + str(task.id) + "/?" + param + "=-1")
            self.assertEqual("", tDict[name])
            
            # Put 10 byte string in stdxxx 
            mockHandler.announceTaskOutput(name, "0123456789")
                
            # Get 100 bytes of stdxxx - expect all
            tDict = self.helper.objGetViaRest("/scs/task/" + str(task.id) + "/")
            self.assertEqual("0123456789", tDict[name])
            tDict = self.helper.objGetViaRest("/scs/task/" + str(task.id) + "/?" + param + "=100")
            self.assertEqual("0123456789", tDict[name])
            tDict = self.helper.objGetViaRest("/scs/task/" + str(task.id) + "/?" + param + "=0")
            self.assertEqual("", tDict[name])
            tDict = self.helper.objGetViaRest("/scs/task/" + str(task.id) + "/?" + param + "=-1")
            self.assertEqual("0123456789", tDict[name])
            
            # Get 5 bytes of stdxxx - expect last 5 bytes
            tDict = self.helper.objGetViaRest("/scs/task/" + str(task.id) + "/?" + param + "=5")
            self.assertEqual("56789", tDict[name])
                    
        # Get all
        tDict = self.helper.objGetViaRest("/scs/task/" + str(task.id) + "/")
        self.assertEqual("0123456789", tDict["stdout"])
        self.assertEqual("0123456789", tDict["stderr"])

        # Get both limited to 5
        tDict = self.helper.objGetViaRest("/scs/task/" + str(task.id) + "/?maxStdout=5&maxStderr=5")
        self.assertEqual("56789", tDict["stdout"])
        self.assertEqual("56789", tDict["stderr"])
        
        # Send 10 more bytes to stdout and stderr, shoudl end up with 11 in stdout string and 12 in stderr
        mockHandler.announceTaskOutput("stdout", "abcdefghij")
        mockHandler.announceTaskOutput("stderr", "abcdefghij")
        tDict = self.helper.objGetViaRest("/scs/task/" + str(task.id) + "/?maxStdout=-1&maxStderr=-1")
        self.assertEqual("9abcdefghij", tDict["stdout"])
        self.assertEqual("89abcdefghij", tDict["stderr"])

        
class ModelCreateDeleteTest(TestCase):

    def setUp(self):
        self.helper = ScsTestHelper(self)
        self.helper.setupModel()
    
    def tearDown(self):
        self.helper.tearDown()
        TransactionTestCase.tearDown(self)

    def test_deleteUser(self):
        u = User.objects.get_by_natural_key("userxxx")
        self.assertTrue(u)
        
        self.assertEqual(3, User.objects.all().count())
        self.assertEqual(1, SmModelResource.objects.all().count())
        self.assertEqual(1, SmModelTask.objects.all().count())
        self.assertEqual(1, SmModelTaskResource.objects.all().count())
        self.assertEqual(1, SmModelAgent.objects.all().count())
        self.assertEqual(2, SmModelContainer.objects.all().count())
        self.assertEqual(2, SmModelTaskType.objects.all().count())
        self.assertEqual(2, SmModelResourceType.objects.all().count())
        self.assertEqual(2, SmModelTag.objects.all().count())
        self.assertEqual(1, SmModelResourceTag.objects.all().count())
        
        # Delete user, everything except resource type and 
        # tag gets deleted
        u.delete()
        
        self.assertEqual(2, User.objects.all().count())
        self.assertEqual(0, SmModelResource.objects.all().count())
        self.assertEqual(0, SmModelTask.objects.all().count())
        self.assertEqual(0, SmModelTaskResource.objects.all().count())
        self.assertEqual(0, SmModelAgent.objects.all().count())
        self.assertEqual(0, SmModelContainer.objects.all().count())
        self.assertEqual(0, SmModelResourceTag.objects.all().count())
        self.assertEqual(0, SmModelTaskType.objects.all().count())
        self.assertEqual(2, SmModelResourceType.objects.all().count())
        self.assertEqual(2, SmModelTag.objects.all().count())

    def test_deleteResource(self):
        r = SmModelResource.objects.get(name="resourcexxx")
        self.assertTrue(r)
        
        self.assertEqual(3, User.objects.all().count())
        self.assertEqual(1, SmModelResource.objects.all().count())
        self.assertEqual(1, SmModelTask.objects.all().count())
        self.assertEqual(1, SmModelTaskResource.objects.all().count())
        self.assertEqual(1, SmModelAgent.objects.all().count())
        self.assertEqual(2, SmModelContainer.objects.all().count())
        self.assertEqual(2, SmModelTaskType.objects.all().count())
        self.assertEqual(2, SmModelResourceType.objects.all().count())
        self.assertEqual(2, SmModelTag.objects.all().count())
        self.assertEqual(1, SmModelResourceTag.objects.all().count())
        
        # Delete resource, everything related to the resource gets deleted
        r.delete()
    
        self.assertEqual(3, User.objects.all().count())
        self.assertEqual(0, SmModelResource.objects.all().count())
        self.assertEqual(1, SmModelTask.objects.all().count())
        self.assertEqual(0, SmModelTaskResource.objects.all().count())
        self.assertEqual(1, SmModelAgent.objects.all().count())
        self.assertEqual(2, SmModelContainer.objects.all().count())
        self.assertEqual(0, SmModelResourceTag.objects.all().count())
        self.assertEqual(2, SmModelTaskType.objects.all().count())
        self.assertEqual(2, SmModelResourceType.objects.all().count())
        self.assertEqual(2, SmModelTag.objects.all().count())


class LoginLogoutTest(TransactionTestCase):
    
    def setUp(self):
        TransactionTestCase.setUp(self)
        self.helper = ScsTestHelper(self)

    def tearDown(self):
        self.helper.tearDown()
        TransactionTestCase.tearDown(self)
        
    def test_login(self):
        os.environ[SCS_ENV_VAR_DISQUS_SECRET_KEY] = "abc"
        os.environ[SCS_ENV_VAR_DISQUS_PUBLIC_KEY] = "123"
        """ Create simple user"""
        username = 'testuser'
        email = 'test@rpsmarf.ca'
        password = 'smarf'
        u = User.objects.create_user(username=username, email=email, password=password,
                                     first_name="first", last_name="last")
        u.is_active = False
        u.save()
        
        # Failure tests
        # None existant user
        resp = self.client.post("/scs/authentication/", {"email": "fail", "password": password})
        self.assertEqual(resp.status_code, 401)
        
        # Wrong password
        resp = self.client.post("/scs/authentication/", {"email": email, "password": "fail"})
        self.assertEqual(resp.status_code, 401)
        
        # User is not active
        resp = self.client.post("/scs/authentication/", {"email": email, "password": password})
        self.assertEqual(resp.status_code, 401)
        
        # Authenticate
        u.is_active = True
        u.save()
        resp = self.client.post("/scs/authentication/", {"email": email, "password": password})
        self.assertEqual(resp.status_code, 200)
        
        # Check login status
        last_login = u.last_login.timestamp()
        now = sm_date_now().timestamp()
        self.assertAlmostEqual(now, last_login, delta=10)

        respDict = json.loads(resp.content.decode("utf-8"))
        self.assertEqual(respDict["userid"], 1)
        self.assertTrue(respDict.get("apikey") is not None)
        self.assertEqual(respDict["email"], "test@rpsmarf.ca")
        self.assertEqual(respDict["firstname"], "first")
        self.assertEqual(respDict["lastname"], "last")
        
    def test_logout(self):
        """ Create simple user"""
        username = 'testuser'
        email = 'test@rpsmarf.ca'
        password = 'smarf'
        u = User.objects.create_user(username=username, email=email, password=password)
        u.save()
        
        # Not logged in
        resp = self.client.get("/scs/logout/")
        self.assertEqual(resp.status_code, 200)
        
        # Logged in 
        resp = self.client.post("/scs/authentication/", {"email": email, "password": password})
        self.assertEqual(resp.status_code, 200)
        resp = self.client.get("/scs/logout/")
        self.assertEqual(resp.status_code, 200)


class HeartTests(TransactionTestCase):
    '''
    WARNING - We derive this test from TransactionTestCase to ensure that Django
    database operations are committed t
    o disk immediately so that other threads
    (namely the HeartMonitor thread) can see them.
    '''
    def setUp(self):
        self.helper = ScsTestHelper(self)
        self.helper.setupModel()
        self.postSaveSignalCallbackCount = 0
        self.preDeleteSignalCallbackCount = 0
        self.agentGuid = None
        
    def tearDown(self):
        self.helper.tearDown()
        TransactionTestCase.tearDown(self)
        
    def my_postSaveSignalCallback(self, sender, **kwargs):
        logger.debug("Got post save signal callback")
        self.postSaveSignalCallbackCount += 1
        self.agentGuid = kwargs["instance"].guid
        
    def my_preDeleteSignalCallback(self, sender, **kwargs):
        logger.debug("Got pre delete signal callback")
        self.preDeleteSignalCallbackCount += 1
        self.agentGuid = kwargs["instance"].guid
        
    def test_signals(self):
        post_save.connect(self.my_postSaveSignalCallback, sender=SmModelAgent)
        pre_delete.connect(self.my_preDeleteSignalCallback, sender=SmModelAgent)
        
        # Add an agent and save - expect post save callback
        u = User.objects.get_by_natural_key("userxxx")
        self.assertTrue(u)
        agent = SmModelAgent()
        agent.guid = "222"
        agent.owner = u
        agent.last_change = str(sm_date_now())
        agent.status = "down"
        agent.name = "a 2 name"
        agent.description = test_helper.DUMMY_DESCRIPTION
        agent.agentUrl = "http://1.2"
        self.assertEqual(0, self.postSaveSignalCallbackCount)
        agent.save()
        self.assertEqual(1, self.postSaveSignalCallbackCount)
        self.assertEqual("222", self.agentGuid)
        
        # Delete the agent - expect pre delete callback
        self.assertEqual(0, self.preDeleteSignalCallbackCount)
        self.agentGuid = None
        agent.delete()
        self.assertEqual(1, self.preDeleteSignalCallbackCount)
        self.assertEqual("222", self.agentGuid)
        
    def test_heartbeating(self):
        u = User.objects.get_by_natural_key("userxxx")
        self.assertTrue(u)
        agent = SmModelAgent.objects.get(guid="111")
        agent.status = "down"
        agent.save()
        
        # Setup up heart beat if
        os.environ[SCS_ENV_VAR_MIN_HEARTBEAT_INTERVAL] = "0.2"
        os.environ[SCS_ENV_VAR_MIN_TIME_AFTER_REMOTE_AGENT_ADD_BEFORE_ALARM] = "0.2"
        timeBeforeInit = math.floor(time.time())
        heartIf = ScsHeartbeatIf(0.2, 0.2, 0.02)
        heartIf.start()
        
        # Test that the heartbeatIf found the pre-built agent
        self.assertEqual(heartIf.heartMonitor.getHeartStatus("111").lastBeatCount, -1)
        
        # Test that agent timeout works
        time.sleep(0.15)
        timeAfterDelay = time.time()
        agent = SmModelAgent.objects.get(guid="111")
        self.assertEqual("down", agent.status)
        last_change_timestamp = time.mktime(agent.last_change.timetuple())
        # -1 below to cover rounding issues from DB time storage
        self.assertGreaterEqual(last_change_timestamp, timeBeforeInit - 1)
        self.assertLessEqual(last_change_timestamp, timeAfterDelay)
        
        # Send heartbeat
        logger.debug("Sending heartbeat...")
        heartIf.heartMonitor.heartbeat("111", 30)
        logger.debug("Sent heartbeat")
        time.sleep(0.07)  # Wait for heartMonitor to observe agent is up
        agent = SmModelAgent.objects.get(guid="111")
        logger.debug("Got agent")
        
        # Create new agent to see if the signal-catching about adding and deleting agents is working
        agent = SmModelAgent()
        agent.guid = "222"
        agent.owner = u
        agent.name = "agent 2"
        agent.description = test_helper.DUMMY_DESCRIPTION
        agent.agentUrl = "http://x.y"
        agent.last_change = str(sm_date_now())
        agent.status = "down"
        self.assertEqual(0, self.postSaveSignalCallbackCount)
        logger.debug("Saving agent 222...")
        agent.save()
        logger.debug("Saved agent 222")

        self.assertEqual(heartIf.heartMonitor.getHeartStatus("222").lastBeatCount, -1)
        # Send heartbeat
        timeBeforeHeartbeat = math.floor(time.time()) - 0.1
        heartIf.heartMonitor.heartbeat("222", 30)
        timeAfterHeartbeat = time.time() + 0.1
        for _ in range(0, 19):
            time.sleep(0.07)
            agent = SmModelAgent.objects.get(guid="222")
            logger.debug("Check agent status, got %s", agent.status)
            if agent.status == "up":
                break
        self.assertEqual("up", agent.status)
        last_change_timestamp = time.mktime(agent.last_change.timetuple())
        self.assertGreaterEqual(last_change_timestamp, timeBeforeHeartbeat)
        self.assertLessEqual(last_change_timestamp, timeAfterHeartbeat)

        agent.delete()

        self.assertRaises(Exception, heartIf.heartMonitor.getHeartStatus, "222")
        heartIf.heartMonitor.stopMonitor()
        
    def test_heartbeatViaIce(self):
        heartbeatIf = ScsHeartbeatIf(0.1, 0.1, 0.02)
        iceIf = None
        iceClient = None
        try:
            port = smGetFreePort()
            iceIf = ScsIceInterface(port)
            iceIf.startIceServer(heartbeatIf, None)

            agent = SmModelAgent.objects.get(guid="111")
            agent.status = "down"
            agent.save()
            container = SmModelContainer.objects.get(name="test_container")
            container.status = "down"
            container.save()
            resource = SmModelResource.objects.get(name="resourcexxx")
            resource.status = "down"
            resource.save()
            
            # Check that agent is down            
            heartbeatIf.heartMonitor.runOnePass()
            agent = SmModelAgent.objects.get(guid="111")
            self.assertEqual("down", agent.status)
            container = SmModelContainer.objects.get(name="test_container")
            self.assertEqual("down", container.status)
            resource = SmModelResource.objects.get(name="resourcexxx")
            self.assertEqual("down", resource.status)

            iceClient = Ice.initialize()
            base = iceClient.stringToProxy(SCS_HEARTBEAT_NAME + ":tcp -h localhost -p " + str(port))
            heart = smmonicemsgs.SmHeartbeatPrx.checkedCast(base)  # @UndefinedVariable
        
            logger.debug("Sending test heartbeat...")
            result = heart.sendHeartbeat("111", 8)
            self.assertEqual(result, 0)
            
            heartbeatIf.heartMonitor.runOnePass()

            agent = SmModelAgent.objects.get(guid="111")
            self.assertEqual("up", agent.status)
            container = SmModelContainer.objects.get(name="test_container")
            self.assertEqual("up", container.status)
            resource = SmModelResource.objects.get(name="resourcexxx")
            self.assertEqual("up", resource.status)
            
            u = User.objects.get(username="userxxx")
            # Add a new container under "up" agent - should be status=up
            container = SmModelContainer()
            container.agent = agent
            container.name = "test_container2"
            container.name_key = "test_container2"
            container.description = test_helper.DUMMY_DESCRIPTION
            container.owner = u
            container.save()
            container = SmModelContainer.objects.get(name="test_container2")
            self.assertEqual("up", container.status)
            
            # Add a new resource under "up" container - should be status=up
            r = SmModelResource()
            r.resource_type = SmModelResourceType.objects.get(name_key="default_resource")
            r.name = "resource2"
            r.owner = u
            r.container = container
            r.description = test_helper.DUMMY_DESCRIPTION
            r.parametersJson = "{}"
            r.save()
            resource = SmModelResource.objects.get(name="resource2")
            self.assertEqual("up", resource.status)
            
            # Make Agent down
            agent.status = "down"
            agent.save()
            
            container = SmModelContainer.objects.get(name="test_container")
            self.assertEqual("down", container.status)
            resource = SmModelResource.objects.get(name="resourcexxx")
            self.assertEqual("down", resource.status)

        finally:
            if (iceIf is not None):
                iceIf.stopIceServer()
            if (iceClient is not None):
                iceClient.destroy()

         
class ActivityStreamTest(TransactionTestCase):

    def setUp(self):
        self.helper = ScsTestHelper(self)
        self.count = 0
        
    def tearDown(self):
        self.helper.tearDown()
        TransactionTestCase.tearDown(self)
        
    def test_basicActivtyFlow(self):
        self.helper.setupTempFolder()
        self.helper.setupModel()
        user = User.objects.first()
        resource = SmModelResource.objects.first()
        task_type = SmModelTaskType.objects.first()
        
        # Generate login action for user
        action.send(user, verb="login")

        # Generate logout action for user
        action.send(user, verb="logout")
        
        # Generate task_type start
        action.send(user, verb="start", target=task_type)
        
        # Generate resource use
        action.send(user, verb="use", target=resource)
        
        stream = actor_stream(user)
        
        self.assertEqual(4, stream.count())
        for item in stream:
            self.assertEqual("AnonymousUser", item.actor.username)
            if item.verb == "login" or item.verb == "logout":
                pass
            elif item.verb == "start":
                self.assertEqual(item.target, task_type)
            elif item.verb == "use":
                self.assertEqual(item.target, resource)
            else:
                self.error("Unknown verb " + item.verb)
        stream = target_stream(task_type)
        self.assertEqual(1, stream.count())
        stream = target_stream(resource)
        self.assertEqual(1, stream.count())

    def test_testEventInjection(self):
        self.helper.setupModel()
        root = User.objects.get(username="root")        
        self.helper.setupRestAuth("root")
        self.helper.setupRemoteAgentViaIce()
        fileResource = SmModelResource.objects.get(name="resourcexxx")
        resourceId = fileResource.id
        self.helper.makeTaskTypeSleep()
        taskTypeSleepId = self.helper.taskTypeSleep.id

        # Add 5 events for user (default is 1 hour sep)
        self.client.get("/scs/user/" + str(root.pk) + "/addevent/?count=5&verb=hello", **self.helper.auth_header)
        stream = actor_stream(root)
        self.assertEqual(5, stream.count())
        # Add 10 events for resource (1 per day)
        self.client.get("/scs/user/" + str(root.pk) + 
                        "/addevent/?interval=1d&count=10&verb=user&target=/scs/resource/" + str(resourceId) + "/",
                        **self.helper.auth_header)
        stream = target_stream(fileResource)
        self.assertEqual(10, stream.count())
        # Add 5 events for task_type (1 per second)
        self.client.get("/scs/user/" + str(root.pk) + 
                        "/addevent/?interval=1s&count=5&verb=user&target=/scs/task_type/" + str(taskTypeSleepId) + "/",
                        **self.helper.auth_header)
        stream = target_stream(self.helper.taskTypeSleep)
        self.assertEqual(5, stream.count())
        # Add event for user (on Jan 1, 2001)
        self.client.get("/scs/user/" + str(root.pk) + 
                        "/addevent/?end_time=20150201T010101Z&verb=use", 
                        **self.helper.auth_header)
        stream = actor_stream(root)
        self.assertEqual(21, stream.count())
        
        # Test getting all events
        resp = self.client.get("/scs/getusage/",
                               **self.helper.auth_header)
        self.assertEqual(200, resp.status_code)
        #for k in resp.streaming_content:
        #    logger.debug("k is %s", k.decode("utf-8"))
        s = "".join(k.decode("utf-8") for k in resp.streaming_content)
        respJson = json.loads(s)
        self.assertEqual(21, len(respJson))
        
        time.sleep(1)
        resp = self.client.get("/scs/getusage/?duration=1", 
                               **self.helper.auth_header)
        self.assertEqual(200, resp.status_code)
        s = "".join(k.decode("utf-8") for k in resp.streaming_content)
        respJson = json.loads(s)
        self.assertEqual(11, len(respJson))
        
        yesterday = sm_date_now() - timedelta(days=1)
        resp = self.client.get("/scs/getusage/?end_time=" + smMakeDateString(yesterday),
                               **self.helper.auth_header)
        self.assertEqual(200, resp.status_code)
        s = "".join(k.decode("utf-8") for k in resp.streaming_content)
        respJson = json.loads(s)
        
        resp = self.client.get("/scs/getusage/?format=csv",
                               **self.helper.auth_header)
        self.assertEqual(200, resp.status_code)
        #for k in resp.streaming_content:
        #    logger.debug("k is %s", k.decode("utf-8"))
        s = "".join(k.decode("utf-8") for k in resp.streaming_content)
        lines = s.split("\n")
        self.assertEqual(22, len(lines))
        
    def test_runTask(self):
        startTime = sm_date_now() - timedelta(seconds=1)
        self.helper.setupModel()
        root = User.objects.get(username="root")        
        self.helper.setupRestAuth("root")
        self.helper.setupRemoteAgentViaIce()
        fileResource = SmModelResource.objects.get(name="resourcexxx")
        self.resource = fileResource

        self.helper.makeTaskTypeSleep()
        
        resp = self.client.post("/scs/authentication/", {"email": "a@b.com", "password": "hello"})
        self.assertEqual(resp.status_code, 200)
        
        # Create a task which runs now for 0 seconds
        task1 = self.helper.makeSleepTask(root, 0)
        resp = self.client.get("/scs/task/" + str(task1.pk) + "/?action=start", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        self.helper.waitForTaskDone(task1.id)

        resource_path = "/scs/resource/" + str(fileResource.id) + "/"
        self.helper.tempFolder.writeFile("f1", "12345")
        with open(self.helper.tempFolder.mkAbsolutePath("f1"), "rb") as fp:
            resp = self.client.post(resource_path + 'a1/upload/',
                                    {'name': 'fred',
                                     'uploadfile': fp})    
        self.assertEqual(resp.status_code, 200)
 
        resp = self.client.get(resource_path + "a1" + "/download/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/scs/logout/", **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)

        stream = actor_stream(root)
        self.assertEqual(7, stream.count())
        for item in stream:
            self.assertEqual("root", item.actor.username)
            if item.verb == "login" or item.verb == "logout":
                pass
            elif item.verb == "upload":
                self.assertEqual(item.target, fileResource)
            elif item.verb == "download":
                self.assertEqual(item.target, fileResource)
            elif item.verb == "run_start":
                self.assertEqual(item.target, self.helper.resourceSleep)
            elif item.verb == "run_end":
                self.assertEqual(item.target, self.helper.resourceSleep)
            elif item.verb == "run":
                self.assertEqual(item.target, self.helper.taskTypeSleep)
            else:
                self.error("Unknown verb " + item.verb)
        stream = target_stream(self.helper.taskTypeSleep)
        self.assertEqual(1, stream.count())
        stream = target_stream(self.helper.resourceSleep)
        self.assertEqual(4, stream.count())  # resourceSleep and fileResource are the same
        
        # Test the getusage api for task type
        resp = self.client.get("/scs/task_type/" + str(self.helper.taskTypeSleep.id) 
                               + "/getusage/?periods=wmy&user=" + str(root.id) + "&total=True",
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        respJson = json.loads(resp.content.decode("utf-8"))
        self.assertEqual(1, respJson["user"]["w"])
        self.assertEqual(1, respJson["user"]["m"])
        self.assertEqual(1, respJson["user"]["y"])
        self.assertEqual(1, respJson["total"]["w"])
        self.assertEqual(1, respJson["total"]["m"])
        self.assertEqual(1, respJson["total"]["y"])
        
        resp = self.client.get("/scs/task_type/" + str(self.helper.taskTypeSleep.id) 
                               + "/getusage/?periods=y&user=99&total=True",
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        respJson = json.loads(resp.content.decode("utf-8"))
        self.assertFalse("w" in respJson["user"])
        self.assertFalse("m" in respJson["user"])
        self.assertEqual(0, respJson["user"]["y"])
        self.assertFalse("w" in respJson["total"])
        self.assertFalse("m" in respJson["total"])
        self.assertEqual(1, respJson["total"]["y"])
        
        # Test the getusage api for resource (1 use, 1 upload and 1 download)
        resp = self.client.get("/scs/resource/" + str(self.helper.resourceSleep.id) 
                               + "/getusage/?periods=wmy&user=" + str(root.id) + "&total=True",
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        respJson = json.loads(resp.content.decode("utf-8"))
        self.assertEqual(3, respJson["user"]["w"])
        self.assertEqual(3, respJson["user"]["m"])
        self.assertEqual(3, respJson["user"]["y"])
        self.assertEqual(3, respJson["total"]["w"])
        self.assertEqual(3, respJson["total"]["m"])
        self.assertEqual(3, respJson["total"]["y"])
        
        resp = self.client.get("/scs/resource/" + str(self.helper.resourceSleep.id)
                               + "/getusage/?periods=m&user=99&total=True",
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        respJson = json.loads(resp.content.decode("utf-8"))
        self.assertFalse("w" in respJson["user"])
        self.assertFalse("y" in respJson["user"])
        self.assertEqual(0, respJson["user"]["m"])
        self.assertFalse("w" in respJson["total"])
        self.assertFalse("y" in respJson["total"])
        self.assertEqual(3, respJson["total"]["m"])
        
        resp = self.client.get("/scs/resource/" + str(self.helper.resourceSleep.id)
                               + "/getusers/",
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        respJson = json.loads(resp.content.decode("utf-8"))
        self.assertEqual(1, len(respJson))
        rec = respJson["root"]
        self.assertEqual("a@b.com", rec["email"])
        self.assertEqual(3, rec["use_count"])
        self.assertEqual("", rec["title"])
        self.assertEqual("", rec["first_name"])
        self.assertEqual("", rec["last_name"])
        last_use = smMakeDateFromString(rec["last_use"])
        self.assertTrue(last_use <= sm_date_now())
        self.assertTrue(last_use >= startTime,
                        "Last use was " + str(last_use) + " but start time was " + str(startTime))
        
        resp = self.client.get("/scs/resource/" + str(self.helper.resourceSleep.id)
                               + "/getusers/?end_time=20990101T010101Z&duration=20",
                               **self.helper.auth_header)
        self.assertEqual(resp.status_code, 200)
        respJson = json.loads(resp.content.decode("utf-8"))
        self.assertEqual(0, len(respJson))
 
        
