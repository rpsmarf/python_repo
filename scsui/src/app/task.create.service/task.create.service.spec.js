describe( 'Task create service', function() {
	beforeEach (module('rpsmarf.config'));
	beforeEach (module('rpsmarf.user.service'));
	
	beforeEach(module('rpsmarf.task.create.service'));
	beforeEach(module('taskprogress/taskprogress.tpl.html'));
	beforeEach(module('restangular'));
	
	var $scope,  SmTaskCreate, $state,  $modalInstance, smarfurl, taskObject, $httpBackend, tool_prefix = 'src/app/tools/', taskcreate_prefix='src/app/task.create.service/';
	var rt3s, resource_types, task_type, dataObj, $rootScope;
	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	//init, starting, prep, running, cleanup and finished
	

	beforeEach(  inject(function(_$rootScope_, Restangular, SMARF_URL,  _SmTaskCreate_, _$httpBackend_){
		smarfurl = SMARF_URL+'/scs/';
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
		SmTaskCreate = _SmTaskCreate_;
		$httpBackend = _$httpBackend_;
		taskObject = Restangular.one('scs/task','1/');
		$rootScope = _$rootScope_;
		$scope = $rootScope.$new();
		//console.log(__html__[taskcreate_prefix+'task.create.service.spec.json']);
		dataObj = JSON.parse(__html__[taskcreate_prefix+'task.create.service.spec.json']);
		task_type = dataObj.task_type;
		rt3s = dataObj.rt3s;
		//console.log(rt3s);
		resource_types = JSON.parse(__html__[taskcreate_prefix+'task.create.service.spec.json']).resource_types;
		//$httpBackend.when('GET',smarfurl+'task_type/').respond(200,responseObj);

		$httpBackend.when('GET',smarfurl+'resource_type_task_type/?task_type__id='+task_type.id).respond(200,{objects:rt3s});
		$httpBackend.when('GET',smarfurl+'resource_type/1/').respond(200,resource_types[0]);
		$httpBackend.when('GET',smarfurl+'resource_type/3/').respond(200,resource_types[2]);
		$httpBackend.when('GET',smarfurl+'resource_type/4/').respond(200,resource_types[3]);
		$httpBackend.when('GET',smarfurl+'resource/?resource_type__id=4').respond(200,{meta: {total_count:1}, objects:[dataObj.res_type_4]});

	})) ;

	it('should initialize the service and decompose the task type', inject([function(){
		
		var t  = SmTaskCreate.setTaskType(task_type);

		t.then(function(resp){
			expect(SmTaskCreate.getTaskType()).toEqual(task_type);
			var inputs = SmTaskCreate.getInputs();
			var rt1 = resource_types[0];
			expect(inputs.valueParameters[0]).toEqual({value: "", type: "string", input_id: 3, name: "Test Val", description: "A test value to take user input"});
			expect(inputs.resourceParameters[0]).toEqual({description: "This specified the location to which the output data will be saved", direction: "output", 
					type: "resource", input_id: 2, role: "dest", name: "Output folder", task_type_id: 2, resource: {path: jasmine.any(String), touched: false}, rt3_id: rt3s[1].id, resource_type: {
					id: rt1.id,
					name : rt1.name,
					description : rt1.description,
					nature : rt1.nature,
					name_key : rt1.name_key
				}
			});

			var rt2 = resource_types[2];
			expect(inputs.resourceParameters[1]).toEqual({description: "This specifies the folder of data to submit for analysis", direction: "input", 
				type: "resource", input_id: 1, role: "src", name: "Data to analyze", task_type_id: 2, rt3_id: rt3s[0].id, resource_type: {
					id: rt2.id,
					name : rt2.name,
					description : rt2.description,
					nature : rt2.nature,
					name_key : rt2.name_key
				}
			});

			var rt3 = resource_types[3];
			//Since there is only one compute resource the compute resource parameter's resource value is set
			expect(inputs.computeResource).toEqual({name:"Compute Resource", role: "compute",input_id: 0,isCompute: true,  rt3_id: rt3s[2].id,resource_type:{
					id: rt3.id,
					name : rt3.name,
					description : rt3.description,
					nature : rt3.nature,
					name_key : rt3.name_key
				},
				resource : dataObj.res_type_4
				//noSelect : true 

			});

			SmTaskCreate.reset();

			expect(SmTaskCreate.getInputs()).toEqual({valueParameters:[], resourceParameters:[], computeResource: {} });
		});

		$httpBackend.flush();
		

		


	}]));

	it('should create the task and associated objects for the user\'s selected input values', inject([ 'SmUser', function(SmUser){
		SmTaskCreate.setTaskType(task_type);
		$httpBackend.flush();

		var inputs = SmTaskCreate.getInputs();
		var compute = dataObj.res_type_4;
		var uid = SmUser.getProperty('id');
		var ttid = task_type.id;
		var value = "A test value";
		var tid = 15;
		
		//Input file resource
		var source = dataObj.res_type_3;
		source.path = "/folder/mock.data";

		//Output location resource
		var dest = dataObj.res_type_1;
		dest.path = "/home/user/";

		var parametersJson = '{"input_ids":{"1":{"path":"'+source.path+'"},"2":{"path":"'+dest.path+'"},"3":{"value":"'+value+'"}}}';

		compute.rt3_id = inputs.computeResource.rt3_id;
		source.rt3_id = inputs.resourceParameters[1].rt3_id;
		dest.rt3_id = inputs.resourceParameters[0].rt3_id;
		
		SmTaskCreate.setInput('string', value, 3 );
		/**Old way of setting input values**/
		//SmTaskCreate.setInput('compute', compute);
		//SmTaskCreate.setInput('resource', source, 1);
		//SmTaskCreate.setInput('resource', dest, 2);
		SmTaskCreate.setActiveResourceParameterId(0);
		SmTaskCreate.setValueForActiveResourceParameter(compute);
		SmTaskCreate.setActiveResourceParameterId(1);
		SmTaskCreate.setValueForActiveResourceParameter(source);
		SmTaskCreate.setActiveResourceParameterId(2);
		SmTaskCreate.setValueForActiveResourceParameter(dest);

		var paramsJsonTest = function(string){
			var data = JSON.parse(string);
			var patt = /[a-zA-Z0-9\/]+\/[0-9]{8}T[0-9]{6}Z/;
			if(data.owner !== '/scs/user/'+uid+'/'){
				return;
			}
			if(data.task_type !== '/scs/task_type/'+ttid+'/'){
				return;
			}
			if(data.status !== 'init'){
				return;
			}
			var pj = JSON.parse(data.parametersJson);
			if(!pj.input_ids || pj.input_ids[1].path !== source.path){
				return;
			}
			if(pj.input_ids[3].value !== value){
				return;
			}
			if(!patt.test(pj.input_ids[2].path)){
				return false;
			}

			return true;

		};

		$httpBackend.expect('POST',smarfurl+'task/',paramsJsonTest,{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200,{headers:function(val){ return '/scs/task/'+tid+'/'; }},{Location:'/scs/task/'+tid+'/'});
	
		//Compute resource
		$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+compute.id+'/', 
			resource_type_task_type: '/scs/resource_type_task_type/'+compute.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

		//Source resource
		$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+source.id+'/', 
			resource_type_task_type: '/scs/resource_type_task_type/'+source.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

		//Dest resource
		$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+dest.id+'/', 
			resource_type_task_type: '/scs/resource_type_task_type/'+dest.rt3_id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

		$httpBackend.expect('GET',smarfurl+'task/'+tid+'/?action=start').respond(200);

		SmTaskCreate.createAndStartTask();	

		$httpBackend.flush();

	}]));

	it('should rerun a task based on an aggregate task object', inject([ 'SmUser', function(SmUser){

		var to = dataObj.taskAggregate;
		var pj = JSON.parse(to.parametersJson);
		var tid = 15;
		var uid = SmUser.getProperty('id');
		var parametersJson = JSON.stringify({input_ids:pj.input_ids});
		var resParams = pj.resources;
		var inputIds = pj.input_ids;
		var rp, p, i;
		var now = new Date();
		var newTaskName = pj.task_type_name + '_' + months[now.getMonth()].substring(0,3) + '_' + now.getDate() + '_' + now.getFullYear();

		if(resParams.length >0){
			rp = [];
			for(i=0;i<resParams.length;i++){
				p = resParams[i];
				rp.push(p);
			}

			to.resParams = rp;
		}

		expect(to.resParams.length).toEqual(3);

		$httpBackend.expect('POST',smarfurl+'task/',{owner:'/scs/user/'+uid+'/', task_type:'/scs/task_type/'+4+'/', status: "init", 
			parametersJson: parametersJson, name: newTaskName},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200,{headers:function(val){ return '/scs/task/'+tid+'/'; }},{Location:'/scs/task/'+tid+'/'});
	
		//Compute resource
		$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'scs/task/'+tid+'/', resource: '/scs/resource/'+6+'/', 
			resource_type_task_type: '/scs/resource_type_task_type/'+7+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);
		
		//Dest resource
		$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'scs/task/'+tid+'/', resource: '/scs/resource/'+1+'/', 
			resource_type_task_type: '/scs/resource_type_task_type/'+6+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

		//Source resource
		$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'scs/task/'+tid+'/', resource: '/scs/resource/'+4+'/', 
			resource_type_task_type: '/scs/resource_type_task_type/'+5+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

		

		$httpBackend.expect('GET',smarfurl+'task/'+tid+'/?action=start').respond(200);

		var promise = SmTaskCreate.rerunTask(to);	

		$httpBackend.flush();

		promise.then(function(response){
			expect(response.interactive).toBeFalsy();
			expect(response.taskId).toEqual(''+tid);
		});

		$rootScope.$apply();


	}]));


	it('should set the task info appropriately when a reconfigure is triggered', inject([ 'SmUser', function(SmUser){
		var to = dataObj.taskAggregate;
		var pj = JSON.parse(to.parametersJson);
		var tid = 15;
		var uid = SmUser.getProperty('id');
		var parametersJson = JSON.stringify({input_ids:pj.input_ids});
		var resParams = pj.resources;
		var inputIds = pj.input_ids;
		var rp, p, i, inputs;
		var ttid = to.task_type.split('/')[3];

		if(resParams.length >0){
			rp = [];
			for(i=0;i<resParams.length;i++){
				p = resParams[i];
				rp.push(p);
			}

			to.resParams = rp;
		}

		expect(to.resParams.length).toEqual(3);

		$httpBackend.expect('GET',smarfurl+'task_type/'+ttid+'/').respond(200,dataObj.task_type_2);

		$httpBackend.when('GET',smarfurl+'resource_type_task_type/?task_type__id='+ttid).respond(200,{objects:dataObj.rt3s});

		$httpBackend.when('GET',smarfurl+'resource_type/1/').respond(200,dataObj.resource_types[0]);
		$httpBackend.when('GET',smarfurl+'resource_type/3/').respond(200,dataObj.resource_types[2]);
		$httpBackend.when('GET',smarfurl+'resource_type/4/').respond(200,dataObj.resource_types[3]);

		$httpBackend.when('GET',smarfurl+'resource/6/').respond(200,dataObj.resourcePath6);
		$httpBackend.when('GET',smarfurl+'resource/1/').respond(200,dataObj.resourcePath1);
		$httpBackend.when('GET',smarfurl+'resource/4/').respond(200,dataObj.resourcePath4);
	
		
		SmTaskCreate.reconfigureTask(to);

		$httpBackend.flush();

		inputs = SmTaskCreate.getInputs();
		rp = inputs.resourceParameters;
		for(i = rp.length-1; i>=0;i--){
			p = rp[i];
			expect(inputIds[p.input_id].path).toEqual(p.resource.path);
		}

		expect(SmTaskCreate.getTaskType().id+'').toEqual(ttid);

	}]));

	it('should decompose an aggregate task object and perform a rerun', inject([ 'SmUser', function(SmUser){

		var to = dataObj.taskAggregate;
		var pj = JSON.parse(to.parametersJson);
		var tid = 15;
		var uid = SmUser.getProperty('id');
		var parametersJson = JSON.stringify({input_ids:pj.input_ids});
		
		var inputIds = pj.input_ids;

		var now = new Date();
		var newTaskName = pj.task_type_name + '_' + months[now.getMonth()].substring(0,3) + '_' + now.getDate() + '_' + now.getFullYear();

		$httpBackend.expect('POST',smarfurl+'task/',{owner:'/scs/user/'+uid+'/', task_type:'/scs/task_type/'+4+'/', status: "init", 
			parametersJson: parametersJson, name: newTaskName},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200,{headers:function(val){ return '/scs/task/'+tid+'/'; }},{Location:'/scs/task/'+tid+'/'});
	
		//Compute resource
		$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'scs/task/'+tid+'/', resource: '/scs/resource/'+6+'/', 
			resource_type_task_type: '/scs/resource_type_task_type/'+7+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);
		
		//Dest resource
		$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'scs/task/'+tid+'/', resource: '/scs/resource/'+1+'/', 
			resource_type_task_type: '/scs/resource_type_task_type/'+6+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

		//Source resource
		$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'scs/task/'+tid+'/', resource: '/scs/resource/'+4+'/', 
			resource_type_task_type: '/scs/resource_type_task_type/'+5+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

		

		$httpBackend.expect('GET',smarfurl+'task/'+tid+'/?action=start').respond(200);

		var promise = SmTaskCreate.decomposeAndRerunTask(to);	

		$httpBackend.flush();

		promise.then(function(response){
			expect(response.interactive).toBeFalsy();
			expect(response.taskId).toEqual(''+tid);
		});

		$rootScope.$apply();


	}]));

	it('should handle the task reservation scenario, restarting a task, and queueing', inject([ 'SmUser', function(SmUser){
		//verify task is set to be reserved
		var t  = SmTaskCreate.setTaskType(task_type, true);

		t.then(function(resp){
			expect(SmTaskCreate.isCurrentTaskReservation()).toBe(true);

		});

		$httpBackend.flush();

		$httpBackend.expect('GET',smarfurl+'task/'+45+'/?action=start').respond(200);

		//verify task restart
		SmTaskCreate.restartTask(45,false);
		$httpBackend.flush();

		//verify task restart with queueing
		$httpBackend.expect('GET',smarfurl+'task/'+45+'/?action=start&allowQueueing=True').respond(200);
		SmTaskCreate.restartTaskWithQueueing(45,false);

	}]));


});

