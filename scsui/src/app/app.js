angular.module( 'rpsmarf', [
  'templates-app',
  'templates-common',
  'rpsmarf.config',
  'restangular',
  'rpsmarf.home',
  'rpsmarf.tools',
  'ngBoilerplate.about',
  'ui.router',
  'rpsmarf.login',
  'rpsmarf.taskcreate',
  'ui.bootstrap',
  'rpsmarf.user.service',
  'rpsmarf.taskservice',
  'rpsmarf.taskstart',
  'rpsmarf.tasklist',
  'rpsmarf.taskbreadcrumb',
  'rpsmarf.taskcopydata',
  'rpsmarf.task.create.service',
  'rpsmarf.support',
  'rpsmarf.feedback',
  'rpsmarf.user.settings',
  'ngAnimate',
  'rpsmarf.interval.service',
  'rpsmarf.jira.issuecollector',
  'rpsmarf.setup',
  'rpsmarf.communities',
  'rpsmarf.communityinfo',
  'rpsmarf.reservationlist',
  'rpsmarf.user.resources',
  'rpsmarf.helpers.service',
  'rpsmarf.simpleinput',
  'rpsmarf.metadata',
  'ivpusic.cookie',
  'rpsmarf.constants',
  'rpsmarf.reservation.service',
  'rpsmarf.disqus.embed',
  'rpsmarf.metadata.extractionlist'
])

.config( ['$urlRouterProvider','RestangularProvider','SMARF_URL',function ($urlRouterProvider, RestangularProvider, SMARF_URL) {
  $urlRouterProvider.otherwise( '/' );
  //RestangularProvider.setRequestSuffix('');
  RestangularProvider.setBaseUrl(SMARF_URL+'/scs/');
  RestangularProvider.setEncodeIds(false);
}])
.value('testSessionPromise', {p:null, resolved: false, init:true, initialState:null, performSetupOnNextLogin: false, post: true})
.run( ['Restangular','$state','SmUser','DEBUG','testSessionPromise','FORUM_URL', function (Restangular,$state, SmUser, DEBUG, testSessionPromise, FORUM_URL) {
    testSessionPromise.p = Restangular.one('test_session/').get().then(function(response){
      response = response.plain();
      if(!response.valid){
        $state.go('login');
      }
      else{
        delete response.valid;
        SmUser.setProperty('id', response.userid);
        SmUser.setProperty('email', response.email);
        SmUser.setProperty('firstname', response.firstname);
        SmUser.setProperty('lastname', response.lastname);
        SmUser.setProperty('community', response.community);
        SmUser.setProperty('apikey', response.apikey);

        if(response.imageUrl){
          SmUser.setProperty('imageUrl', response.imageUrl);  
        }
        if(response.uiSettingsJson){
          SmUser.setProperty('uiSettings', JSON.parse(response.uiSettingsJson));
        }
        
        testSessionPromise.performSetupOnNextLogin = response.performSetupOnNextLogin;
        FORUM_URL.value = response.forumUrl;
      }
      testSessionPromise.resolved = true;

    });
}])

.controller( 'AppCtrl', ['$scope', '$location', '$state','SmUser', '$interval', '$modal', 'TASK_STATE_ENUM', 'TASK_STATE_RUNNING', 'TASK_STATE_FINISHED', 'SmTaskCreate', 'Restangular', 'SmInterval',
                          'testSessionPromise', 'USER_STATE', 'ipCookie', 'DEBUG', 'FORUM_URL', '$anchorScroll', 'SmHelpers', '$sce', '$window', 'ISSUE_COLLECTOR_ID', 'SmReservation', 'DISQUS_FORUM_SHORTNAME', 'SmDisqus',
 function AppCtrl ( $scope, $location, $state, SmUser, $interval, $modal, TASK_STATE_ENUM, TASK_STATE_RUNNING, TASK_STATE_FINISHED, SmTaskCreate, Restangular, SmInterval, 
                    testSessionPromise, USER_STATE, ipCookie, DEBUG, FORUM_URL, $anchorScroll, SmHelpers, $sce, $window, ISSUE_COLLECTOR_ID, SmReservation, DISQUS_FORUM_SHORTNAME, SmDisqus
  ) {
  /***VARIABLES***/
  /*Scope*/
  $scope.user = SmUser.getObject();
  $scope.communities = [];
  $scope.year = '';
  $scope.menu = {community: false, user: false, support:false, action: false, resourceActions: false, resourceCopyActions: [false,false]};
  $scope.$state = $state;
  $scope.infoPanel = {collapsed: true};
  $scope.forumUrl = {value: FORUM_URL.value};
  $scope.smProperties = null;
  $scope.$window = $window;
  $scope.mainClasses = [];
  $scope.disqusEnabled = false;
  $scope.metadataCollapsed = {value: true};

  $scope.issueCollectorId = null;

  //Folder info variables
  $scope.folderInfo = {"public":{"class":null, percentageUsed:null}, "private":{"class":null, percentageUsed:null}};
  //Current Task (taskbar) variables
  $scope.taskStarted = false;
  $scope.taskname = '---';
  $scope.taskWrapperCollapsed = {is:true};
  $scope.currentTaskStatus = '';
  $scope.currentTaskApi = null;
  $scope.currentTaskObj = null;
  $scope.progressbarType = 'success';
  $scope.taskCompletionError = false;
  $scope.currentTaskInterval = null;
  $scope.progressbarClasses = [];
  $scope.taskProgressValue = 0;
  $scope.taskState = '';
  $scope.showTaskbar = true;
  $scope.taskNotification = false;
  $scope.progressType = 'progressbar';
  $scope.interactiveTaskUrl = '';
  $scope.additionalInfo = null;
  $scope.multiStepTask = false;

  /***STANDARD VARIABLES***/
  var today = new Date();
  var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

  /***FUNCTIONS***/
  /*Scope*/
  $scope.toggleDropdown = function($event, menu){
    var parts;
    $event.preventDefault();
    $event.stopPropagation();
    

    if(menu === 'community' && $scope.communities.length === 1 && $scope.communities[0].name_key === $scope.user.community.name_key){
      return;
    }
    
    if(menu.indexOf('resourceCopyActions')>=0){
      parts = menu.split('-');
      menu = parts[0];
      $scope.menu[menu][parts[1]] = !$scope.menu[menu][parts[1]];
    }
    else{
      $scope.menu[menu] = !$scope.menu[menu];
    }
    
  };

  $scope.menuClass = function(menu){

    if($scope.menu[menu]){
      return "fa-angle-up";
    }
    else{
      return "fa-angle-down";
    }
  };

  $scope.feedback = function(){

    $modal.open({
      controller: 'FeedbackCtrl',
      templateUrl: 'feedback/feedback.tpl.html',
      size: 'lg'
    });
  };

  $scope.changeCommunity = function(community){
    var uid = $scope.user.id;
    Restangular.one('user_setting',uid+'/').patch({community:community}, null, {"Content-Type": "application/json"}).then(function(){
      SmUser.setProperty('community',community);
      $scope.$broadcast('SmCommunityChange');
    },function(){
      $modal.open({
        templateUrl: 'alert/alert.tpl.html',
        controller: 'AlertCtrl',
        resolve:{
          title: function(){return null;},
          message: function(){return 'Cannot change your community at this time';}
        }
      });
    });

    
  };

  $scope.startInteractiveTool = function(){
    var taskApi = $scope.currentTaskApi;
    if(!taskApi){
      throw 'Attempting to start interactive tool with null task object';
    }
    else{
      //$scope.taskWrapperCollapsed.is = true;
      $window.open($scope.interactiveTaskUrl);
    }
  };

  $scope.rerunTask = function(){
    var modalInstance, nameModal;
    if(!$scope.currentTaskObj){
      throw 'Attempting to rerun task with null task object';
    }
    else{
      modalInstance = $modal.open({
        controller: 'ConfirmCtrl',
        templateUrl: 'confirm/confirm.tpl.html',
        resolve: {
          title: function(){return 'Confirm run';},
          body: function(){return 'You are about to rerun '+ $scope.taskname + '. Continue?';}
        }
      });

      modalInstance.result.then(function(){
        var now = new Date();
        var nameModal = $modal.open({
          controller: 'SimpleInputCtrl',
          templateUrl: 'simpleinput/simpleinput.tpl.html',
          resolve: {
            name: function(){return $scope.taskname + '_' + months[now.getMonth()].substring(0,3) + '_' + now.getDate() + '_' + now.getFullYear();}
          }
        });
    
        nameModal.result.then(function(name){

          Restangular.one('task/'+$scope.currentTaskObj.id+'/?aggregateParam=True').get().then(function(response){
            $scope.currentTaskObj = response.plain();
            var paramsJson = JSON.parse($scope.currentTaskObj.parametersJson);
            var promise = SmTaskCreate.decomposeAndRerunTask($scope.currentTaskObj);
            promise.then(function(taskInfo){
              $scope.$emit('SmTaskStart',Restangular.one('task',taskInfo.taskId+'/'),name, taskInfo.interactive);
              //Shouldn't be needed but just in case
              SmTaskCreate.reset();
            }, function(error){
              SmHelpers.handleErrorOnTaskStart(error, {toolName: paramsJson.task_type_name,taskName: name,interactive: paramsJson.interactive},$scope);
            });
          });

        });
       

      },function(){
        console.log('app.rerun cancelled');
      });

      
    }
    
  };

  $scope.openIssueCollector = function(){
    angular.element('#'+ISSUE_COLLECTOR_ID).trigger('SmOpenIssueCollector');
  };

  $scope.openAskAQuestionCollector = function(){
    angular.element('#'+ISSUE_COLLECTOR_ID).trigger('SmOpenAskAQuestionCollector');
  };

  $scope.displayTaskDetails = function(){
    var resJson = JSON.parse($scope.currentTaskObj.resultsJson);
    var error='', alteredPaths='', key, val, errMsg = false, fullMsg;

    if(resJson.description){
      error = '<h5>Errors</h5>An error occured during execution of the last task:<br>'+resJson.description;
      errMsg = true;
    }
    if(resJson.alteredPaths && Object.keys(resJson.alteredPaths).length > 0){
      alteredPaths = '<h5>Modified file/folder names</h5>';
      alteredPaths += '<table class="table"><tr><th>Original path</th><th>New Path</th></tr>';                      

      for(key in resJson.alteredPaths){
        val = resJson.alteredPaths[key];
        alteredPaths += '<tr><td>'+key+'</td><td>'+val+'</td></tr>';
      }

      alteredPaths += '</table>';
    }
    fullMsg = error;
    if(alteredPaths){
      fullMsg += '<hr>';
    }
    fullMsg += alteredPaths;

    $modal.open({
      controller: 'AlertCtrl',
      templateUrl: 'alert/alert.tpl.html',
      resolve: {
        title: function(){ return 'Details';},
        message: function(){return fullMsg; }
      }

    });
  };

  $scope.scrollTo = function(section){
    $location.hash(section+'-anchor');
  };

  /***STANDARD FUNCTIONS***/
  var postAppInit = function(){
    var csrf = ipCookie('csrftoken');
    SmUser.setProperty('csrf', csrf);
    //console.log('csrf: ' + csrf);
    Restangular.setDefaultHeaders({"X-CSRFToken": csrf});
    resetTaskProgressBarVars();
    $scope.metadataCollapsed.value=true;
    Restangular.one('community/').get().then(function(response){
      $scope.communities = response.objects;
    });

    if($scope.user.id){
      Restangular.one('task/?aggregateParam=True&orderByIdDesc=True&limit=1&owner__id='+$scope.user.id).get().then(function(response){
        var taskInfo, paramsJson, progressJson, pc, resJson, runtime;
        if(response.meta.total_count === 0 || (response.objects[0] && !response.objects[0].uiVisible)){
          $scope.showTaskbar = true;
        }
        else{
          taskInfo = response.objects[0];
          paramsJson = JSON.parse(taskInfo.parametersJson);
          resJson = (taskInfo.resultsJson !== '' ) ? JSON.parse(taskInfo.resultsJson) : '';
          if(taskInfo.progressJson !== ''){
            progressJson = JSON.parse(taskInfo.progressJson);
            if(progressJson.progress){
              pc = progressJson.progress;
              if(pc > 100){
                pc = 100;
              }
              $scope.progressType = 'progressbar';
              $scope.taskProgressValue = pc;
            }
            else if(progressJson.outpuSize && progressJson.outputSize !== -1){
              $scope.taskProgressValue = (progressJson.outputSize/(1024*1024)).toFixed(2) + ' MB';
              $scope.progressType = 'outputsize';
            }
            else if(progressJson.runtime && progressJson.runtime !== -1){
              runtime = progressJson.runtime;
              if( runtime < 60){
                runtime += ' sec(s)';
              }
              else if(runtime > 60 && runtime < 3600){
                runtime = (runtime/60).toFixed(2) + ' min(s)';
              }
              else{
                runtime = (runtime/3600).toFixed(2) + ' hr(s)';
              }
              $scope.taskProgressValue = runtime;
              $scope.progressType = 'runtime';
            }
            else {
              $scope.taskProgressType = '?';
              $scope.taskProgressValue = '-----';
            }
            
          }

          
          
          $scope.taskname = paramsJson.task_type_name;
          
          $scope.currentTaskApi =  Restangular.one('task',taskInfo.id+'/');
          $scope.currentTaskObj = taskInfo;
          //$scope.taskPercentComplete = (angular.isNumber(pc)) ? pc : 0;
          $scope.taskState = taskInfo.state;
          $scope.showTaskbar = true;
          if(taskInfo.state !== TASK_STATE_FINISHED){
            $scope.taskWrapperCollapsed.is = false;
            $scope.taskStarted = true;
            $scope.progressbarType = 'success';
            
            $scope.progressbarClasses = ['active','progress-striped'];
            $scope.currentTaskInterval  = setTaskbarInterval($scope.currentTaskApi,paramsJson.interactive );
          }
          else{
            if(taskInfo.completion === 'completedWithError'){
              $scope.taskCompletionError = true;
              $scope.taskNotification = true;
              $scope.progressbarType = 'danger';
            }
            else if(resJson && resJson.alteredPaths && Object.keys(resJson.alteredPaths).length > 0){
              $scope.taskNotification = true;
            }
            $scope.progressbarClasses = [];
            $scope.currentTaskStatus = 'completed';
          }
          
        
        }
      });  

      
      Restangular.one('resource/?owner__id='+$scope.user.id+'&personalFolder=true').get().then(function(response){
        var i,obj, json;
        for(i=0;i<response.objects.length; i++){
          obj = response.objects[i];
          json = JSON.parse(obj.capacityJson);
          if(!json.diskUsed){
            json.diskUsed = 0;
          }
          if(obj.name_key.indexOf('private') >=0){
            $scope.folderInfo['private'].class = json.diskUsedState;
            $scope.folderInfo['private'].percentageUsed = ((json.diskUsed/json.diskUsedFailThreshold)*100).toFixed(2);
            $scope.folderInfo['private'].info = (json.diskUsed/1073741824).toFixed(3) +' GB of ' +(json.diskUsedFailThreshold/1073741824).toFixed(2) + ' GB used';
          }
          else if(obj.name_key.indexOf('public') >=0){
            $scope.folderInfo['public'].class = json.diskUsedState;
            $scope.folderInfo['public'].percentageUsed = ((json.diskUsed/json.diskUsedFailThreshold)*100).toFixed(2);
            $scope.folderInfo['public'].info = (json.diskUsed/1073741824).toFixed(3) +' GB of ' +(json.diskUsedFailThreshold/1073741824).toFixed(2) + ' GB used';
          }
        }
      });

      Restangular.one('property/').get().then(function(response){
        var objs = response.plain().objects;
        var newObjs = {}, o;
        for(var i = objs.length-1; i>=0; i--){
          o = objs[i];
          o.value = $sce.trustAsResourceUrl(o.value);
          newObjs[o.key] = o;
        }
        $scope.smProperties = newObjs;
      });
    }
  };

  var setTaskbarInterval = function(taskApi, interactive){
    var stop = $interval(function(){
      taskApi.get().then(function(response){
          $scope.currentTaskObj = response;
          var pc ='', resJson, pj, runtime;
          if(response.progressJson !== ''){
            pj = JSON.parse(response.progressJson);
            if(pj.progress){
              pc = pj.progress;
              if(pc > 100){
                pc = 100;
              }
              $scope.progressType = 'progressbar';
              $scope.taskProgressValue = pc;
            }
            else if(pj.outpuSize && pj.outpuSize !== -1){
              $scope.taskProgressValue = (pj.outpuSize/(1024*1024)).toFixed(2) + ' MB';
              $scope.progressType = 'outputsize';
            }
            else if(pj.runtime && pj.runtime !== -1){
              runtime = pj.runtime;
              if( runtime < 60){
                runtime += ' sec(s)';
              }
              else if(runtime > 60 && runtime < 3600){
                runtime = (runtime/60).toFixed(2) + 'min(s)';
              }
              else{
                runtime = (runtime/3600).toFixed(2) + 'hr(s)';
              }
              $scope.taskProgressValue = runtime;
              $scope.progressType = 'runtime';
            }
          }

          if(interactive && response.state === TASK_STATE_RUNNING){
            
            $scope.interactiveTaskUrl = JSON.parse(response.runningDisplayJson).taskView.url;
            $scope.currentTaskStatus = 'interactive-ready';
            //$scope.taskProgressValue = 100;
          }

          if(response.state === TASK_STATE_FINISHED){
            $interval.cancel(stop);
            $scope.currentTaskStatus = 'completed';
            $scope.taskWrapperCollapsed.is = false;

            if(response.completion === 'completedWithError'){
              $scope.progressbarType = 'danger';
              $scope.taskCompletionError = true;
              $scope.taskNotification = true;
            }
            resJson = (response.resultsJson !== '') ? JSON.parse(response.resultsJson) : '';
            if(resJson && resJson.alteredPaths && Object.keys(resJson.alteredPaths).length > 0){
              $scope.taskNotification = true;
            }
            $scope.progressbarClasses = [];
            //$scope.taskPercentComplete = 100;
            
            $scope.taskState = TASK_STATE_ENUM['finished'];

            if(response.completion === 'completedWithoutError' && $scope.multiStepTask){
              if($scope.additionalInfo.downloadFolder){
                SmHelpers.completeFolderDownload($scope.additionalInfo);
                $scope.multiStepTask = false;
                $scope.additionalInfo = null;
              }
            }
            
            return;

          }
          $scope.taskState = TASK_STATE_ENUM[response.state];
        });
    }, 1000);

    return stop;

  };

  var resetTaskProgressBarVars = function(){
    //Folder info variables
    $scope.folderInfo = {"public":{"class":null, percentageUsed:null}, "private":{"class":null, percentageUsed:null}};
    //Current Task (taskbar) variables
    $scope.taskStarted = false;
    $scope.taskname = '---';
    $scope.taskWrapperCollapsed = {is:true};
    $scope.currentTaskStatus = '';
    $scope.currentTaskApi = null;
    $scope.currentTaskObj = null;
    $scope.progressbarType = 'success';
    $scope.taskCompletionError = false;
    $scope.currentTaskInterval = null;
    $scope.progressbarClasses = [];
    $scope.taskProgressValue = 0;
    $scope.taskState = '';
    $scope.showTaskbar = true;
    $scope.taskNotification = false;
    $scope.progressType = 'progressbar';
  };

  /***EXE***/
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
    var validStates = ['logout','login'], modalInstance;
    var csrf = SmUser.getProperty('csrf');
    var cookieCsrf = ipCookie('csrftoken');
    if(!DEBUG && !testSessionPromise.init && cookieCsrf != csrf && validStates.indexOf(toState.name) <0)
    {
      event.preventDefault();
      modalInstance = $modal.open({
        templateUrl: 'alert/alert.tpl.html',
        controller: 'AlertCtrl',
        resolve:{
          title: function(){ return 'Notice';},
          message: function(){ return 'Your Session has expired.';}
        }

      });

      modalInstance.result.then(function(){
        $state.go('logout');
      });

      return;
    }
    
    if(!testSessionPromise.resolved && validStates.indexOf(toState.name) <0){
      event.preventDefault();
      testSessionPromise.initialState = toState.name;
    }
    
    if(testSessionPromise.init){
      testSessionPromise.p.then(function(){
        var userValid = SmUser.isValid();
        if(!userValid && validStates.indexOf(toState.name) <0 ){
          event.preventDefault();
          $state.go('login');
          testSessionPromise.post = false;
          testSessionPromise.init = false;
        }else if(userValid && testSessionPromise.init){
          event.preventDefault();
          testSessionPromise.init = false;
          testSessionPromise.post = true;
          SmUser.setProperty('state',USER_STATE.loggingIn);
          if(testSessionPromise.performSetupOnNextLogin){
            $state.go('setup.page');
          }
          else if(testSessionPromise.initialState){
            $state.go(testSessionPromise.initialState);  
          }
          
        }
      });
    }
    
  });
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if(SmUser.isValid() && SmUser.getProperty('state') == USER_STATE.loggingIn){
      testSessionPromise.post = false;
      SmUser.setProperty('state', USER_STATE.browse);
      postAppInit();
    }

    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | RP-SMARF' ;
    }

    if(fromState.name.indexOf('taskstart') >= 0 && toState.name.indexOf('taskstart') <0 && toState.name.indexOf('tools') < 0){
      SmTaskCreate.reset();
      SmReservation.reset();
    }
    if(fromState.name.indexOf('taskstart') >= 0 && toState.name === 'tools'){
      SmReservation.reset();
    }
    if(fromState.name === 'tasklist' || fromState.name === 'extractionlist'){
      SmInterval.stopAllIntervals();
    }
    if(toState.name === 'logout' && $scope.currentTaskInterval){
      $interval.cancel($scope.currentTaskInterval);
    }
    if(toState.name === 'login' || toState.name === 'logout' || toState.name === 'setup.page'){
      $scope.showCommon = false;
      $scope.showSideNavbar = false;
      $scope.menu.community = false;
      $scope.menu.user = false;
      $scope.mainClasses = [];
    }
    else if(toState.name === 'resourceconfig.taskstart' || toState.name.indexOf('taskstart') >=0 || toState.name.indexOf('tools') >= 0 || toState.name.indexOf('communities') >= 0 || toState.name.indexOf('communityinfo') >= 0 ){
      $scope.showCommon = true;
      $scope.showSideNavbar = true;
      $scope.mainClasses = ['col-md-offset-2', 'col-md-10', 'main'];
    }
    // else if(toState.name.indexOf('user.settings') >=0){
    //   $scope.showSideNavbar = true;
    //   $scope.showCommon=true;
    //   $scope.mainClasses = ['container-fluid', 'main'];
    // }
    else{
      $scope.showCommon=true;
      $scope.showSideNavbar = true;
      if(toState.name === 'home'){
        $scope.mainClasses = ['col-md-offset-2', 'col-md-10', 'main', 'main-home'];  
      }
      else {
        $scope.mainClasses = ['col-md-offset-2', 'col-md-10', 'main'];
      }
      
    }

    if(toState.name === 'communityinfo'){
      $anchorScroll.yOffset = 50;
    }
    else{
      $anchorScroll.yOffset = 0;
    }

    $window.scrollTo(0,0);
    
   
    $scope.infoPanel.collapsed = true;  
   
    
  });

  $scope.$on('SmTaskStart', function(event, taskApi, taskname, interactive, additionalInfo ){

    $scope.taskStarted = true;
    $scope.taskname = taskname;
    $scope.taskWrapperCollapsed.is = false;
    $scope.currentTaskStatus = '';
    $scope.currentTaskApi = taskApi;
    $scope.currentTaskObj = null;
    $scope.progressbarType = 'success';
    $scope.taskCompletionError = false;
    $scope.taskNotification = false;
    $scope.progressbarClasses = ['active','progress-striped'];
    $scope.taskProgressValue = 0;
    $scope.taskProgressType = 'progressbar';
    $scope.taskState = TASK_STATE_ENUM['init'];
    $scope.showTaskbar = true;
    $scope.multiStepTask = false;
    $scope.additionalInfo = null;
    
    if($scope.currentTaskInterval){
      $interval.cancel($scope.currentTaskInterval);

    }

    if(additionalInfo && additionalInfo.downloadFolder){
      $scope.multiStepTask = true;
      $scope.additionalInfo = additionalInfo;
    }
    
    $scope.currentTaskInterval = setTaskbarInterval(taskApi,interactive);
    
  });

  $scope.$on('SmInfoPanelToggle', function(event, collapsed){
    //var i, c;
    $scope.infoPanel.collapsed = collapsed;

    // if($scope.infoPanel.collapsed){
    //   for(i = $scope.mainClasses.length-1; i>=0; i--){
    //     c = $scope.mainClasses[i];
    //     if(c === 'main-half-height'){
    //       $scope.mainClasses.splice(i,1);
    //       break;
    //     }
    //   }
    // }
    // else{
    //   $scope.mainClasses.push('main-half-height');
    // }
  });

  $scope.$on('SmObjectSelectInfoPanel', function(event, object){
    $scope.infoPanel.collapsed = false;
    $scope.$broadcast('SmInfoPanelUpdate', object);
  });

  $scope.$on('SmMDSearch', function(event, object){
    $scope.infoPanel.collapsed = false;
    $scope.$broadcast('SmMDSearchUpdate', object);
  });

  $scope.$watch(function(scope){
    return FORUM_URL.value;
  }, function(newVal, oldVal, scope){
    $scope.forumUrl.value = newVal;
  });

  $scope.$watch(function(scope){
    return SmDisqus.isConfigSet();
  }, function(newVal, oldVal, scope){
    if(newVal && DISQUS_FORUM_SHORTNAME !== ''){
      $scope.disqusEnabled = true;
    }
    else{
      $scope.disqusEnabled = false;
    }
  });
  
  $scope.year = today.getFullYear();


  
}])

;

