angular.module("rpsmarf.foldernavpane", [
	'rpsmarf.foldernavpane.dragdrop',
	'ui.bootstrap'

])

.directive('foldernavpane',[function(){

	return {
		restrict: 'E',
		scope:{
			entries: '=',
			ls: '&',
			levelup: '&',
			smondrop: '&',
			resource: '=',
			refresh: '&',
			download: '&'
		},
		templateUrl:  'foldernavpane/foldernavpane.tpl.html',
		controller: function($scope, $element, $attrs){

			//$scope.entries = $scope.resource.entries;
			//scope.entries = [{basename:'home/', path:'/home/',isDir:true},{basename:'documents/', path:'/home/documents/',isDir:true},{basename:'data/', path:'/data/',isDir:true},{basename:'file.txt', path:'/file.txt',isDir:false}];

			$scope.entryType = function(entry){

				
				if(entry.isDir ){
					return ["fa"," fa-folder-open","entry-icon-folder"];
				}
				else{
					return ["fa"," fa-file-text", "entry-icon-file"];
				}

			};

			$scope.highlight = function($event){
				var target = angular.element($event.target);
				if(target.closest('div.entry-container').length>0 ){
					target = target.closest('div.entry-container');
				}
				
				$event.stopPropagation();
				target.css("background-color", "rgba(255, 230, 0, 0.5)");
				var containers = angular.element('div.foldernavpane div.entry-container').not(target);

				containers.css("background-color", "");
				console.log($scope.entries);

			};

			
			
		}
		
		
	};




}]);