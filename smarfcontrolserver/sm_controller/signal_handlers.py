'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Feb 3, 2015

    @author: rpsmarf
'''

from smcommon.constants.sm_const import SM_COMMUNITY_ALL_NAME_KEY,\
    SM_RESOURCE_TYPE_USER_STORAGE_NAME_KEY, SM_CONTAINER_USER_STORAGE_NAME_KEY
from sm_controller.models import SmModelUserSetting, SmModelCommunity,\
    SmModelResourceType, SmModelContainer, SmModelResource, SmModelAgent,\
    RESOURCE_PARAMETERS_JSON_SCHEMA, TASK_TYPE_CONFIGURATION_JSON_SCHEMA,\
    SmModelResourceTypeTaskType, SmModelTaskType, SmModelTask,\
    TASK_PARAMETERS_JSON_SCHEMA, SmModelReservation, SmModelTag
import json
from django.core.exceptions import ValidationError
from django.db.models import signals
from tastypie.models import create_api_key
from django.contrib.auth.models import User, Group
import logging
from smarfcontrolserver.init.envvar import scsGetEnvVarInt,\
    SCS_ENV_VAR_WARN_THRESH_PRIVATE, SCS_ENV_VAR_FAIL_THRESH_PRIVATE,\
    SCS_ENV_VAR_WARN_THRESH_PUBLIC, SCS_ENV_VAR_FAIL_THRESH_PUBLIC
import jsonschema
from django.contrib.contenttypes.models import ContentType
from guardian.models import UserObjectPermission, GroupObjectPermission
from django.db.models.query_utils import Q
from guardian.shortcuts import assign_perm
from sm_controller.api import SmApiResource


logger = logging.getLogger()

        
def smCreateUserSettings(sender, **kwargs):
    '''
    This method is invoked after a user is saved.  It auto-creates 
    the user settings object (if it does not already exist)
    and sets the user up in the "everything" community.
    '''
    user = kwargs.get('instance')
    userSettingsList = SmModelUserSetting.objects.filter(owner=user)
    if len(userSettingsList) != 0:
        logger.debug("Skipping creating user settings for user/{} because it already exists".format(user.pk))        
        return

    try:
        logger.debug("Creating user settings object for user/{}".format(user.pk))
        userSettings = SmModelUserSetting()
        userSettings.owner = user
        userSettings.community = SmModelCommunity.objects.get(name_key=SM_COMMUNITY_ALL_NAME_KEY)
        if userSettings.community is None:
            raise ValidationError("Community with name_key {} not found".format(SM_COMMUNITY_ALL_NAME_KEY))
        userSettings.save()
        logger.debug("Created user settings object for user/{}".format(user.pk))
    except Exception as e:
        logger.info("User settings could not be created for %s, exc = %s", str(kwargs.get('instance')), str(e))


def smCreateUserResources(sender, **kwargs):
    '''
    This method is invoked after a user is saved.  It auto-creates 
    the private and public cloud resource objects (if they do not already
    exist)
    '''
    user = kwargs.get('instance')
    
    if (user.first_name == "") and (user.last_name == ""):
        logger.debug("Skipping cloud folder creation until admin panel has set the name")
        return
    
    resources = SmModelResource.objects.filter(owner=user).filter(personalFolder=True)
    if len(resources) != 0:
        logger.debug("Skipping creating cloud resource object for user/{} because they already exist"
                     .format(user.pk))        
        return
    
    try:
        rtype = SmModelResourceType.objects.get(name_key=SM_RESOURCE_TYPE_USER_STORAGE_NAME_KEY)
        container = SmModelContainer.objects.get(name_key=SM_CONTAINER_USER_STORAGE_NAME_KEY)
        logger.debug("Creating private resource object for user/{}".format(user.pk))
        private_resource = SmModelResource()
        private_resource.container = container
        private_resource.personalFolder = True
        private_resource.description = "Private cloud folder for {} {}".format(user.first_name, user.last_name)
        private_resource.name = "Private Cloud Folder - {} {}".format(user.first_name, user.last_name)
        private_resource.name_key = "private_cloud_{}".format(user.username.lower())
        private_resource.owner = user
        private_resource.parametersJson = json.dumps({"folder": "private_{}/".format(user.username)})
        private_resource.capacityJson = json.dumps({"pollDiskUsed": True,
                                                    "diskUsedWarnThreshold": 
                                                    scsGetEnvVarInt(SCS_ENV_VAR_WARN_THRESH_PRIVATE),
                                                    "diskUsedFailThreshold": 
                                                    scsGetEnvVarInt(SCS_ENV_VAR_FAIL_THRESH_PRIVATE),
                                                    })
        private_resource.resource_type = rtype
        private_resource.save()
        assign_perm("read_resource_data", user, private_resource)
        assign_perm("write_resource_data", user, private_resource)
        logger.debug("Creating public resource object for user/{}".format(user.pk))
        public_resource = SmModelResource()
        public_resource.container = container
        public_resource.personalFolder = True
        public_resource.description = "Public cloud folder for {} {}".format(user.first_name, user.last_name)
        public_resource.name = "Public Cloud Folder - {} {}".format(user.first_name, user.last_name)
        public_resource.name_key = "public_cloud_{}".format(user.username.lower())
        public_resource.owner = user
        public_resource.parametersJson = json.dumps({"folder": "public_{}/".format(user.username)})
        public_resource.capacityJson = json.dumps({"pollDiskUsed": True,
                                                   "diskUsedWarnThreshold": scsGetEnvVarInt(SCS_ENV_VAR_WARN_THRESH_PUBLIC),
                                                   "diskUsedFailThreshold": scsGetEnvVarInt(SCS_ENV_VAR_FAIL_THRESH_PUBLIC),
                                                   })
        public_resource.resource_type = rtype
        public_resource.save()
        all_users_group = Group.objects.get(name="all_users")
        assign_perm("read_resource_data", all_users_group, public_resource)
        assign_perm("read_resource_data", user, public_resource)
        assign_perm("write_resource_data", user, public_resource)
        
    except Exception as e:
        logger.info("Cloud resources could not be created for %s, exc = %s", str(kwargs.get('instance')), str(e))


def smCreateResourceFolder(sender, **kwargs):
    '''
    This method is invoked after a resource is saved.  It auto-creates 
    the associated folder if the folder does not exist
    '''
    resource = kwargs.get('instance')
    if resource.nature != "data":
        return
    
    fileAcc = None
    try:
        fileAcc = SmApiResource.makeFileAccFromResource(resource)
        fileAcc.connectSession()
        try:
            fileAcc.list("", 1, 1, None, False)
        except:
            logger.info("Creating folder for resource/{}".format(resource.pk))
            resp = fileAcc.mkdir(".rpsmarf/tmp", True)
            logger.debug("Got mkdir response: %s", json.dumps(resp))
    except Exception as e:
        logger.info("Cloud resource folder could not be created for %s, exc = %s", str(kwargs.get('instance')), str(e))
    finally:
        if fileAcc:
            fileAcc.disconnectSession()


def smStatusUpdateOnAgentSave(sender, **kwargs):
    '''
    This method is invoked automatically after an agent is saved. We use this
    to upload the status field in all associated container and resource objects.
    '''
    agent = kwargs.get('instance')
    newStatus = agent.status
    # Get containers for this agent
    containers = SmModelContainer.objects.filter(agent=agent)
    for container in containers:
        container.status = newStatus
        container.save()
        resources = SmModelResource.objects.filter(container=container)
        for resource in resources:
            resource.status = newStatus
            resource.save()


def smCheckUserDataDeleted(sender, **kwargs):
    '''
    This method is invoked automatically before a user is deleted. It
    checks if the cloud folder resources associated with the user 
    '''
    user = kwargs.get('instance')
    raiseException = False
    # Get containers for this agent
    resources = SmModelResource.objects.filter(owner=user).filter(personalFolder=True)
    for resource in resources:
        fileAcc = None
        try:
            logger.debug("In smCheckUserDataDeleted - checking resource/{} folder".format(resource.pk))
            fileAcc = SmApiResource.makeFileAccFromResource(resource)
            fileAcc.connectSession()
            files = fileAcc.list("", 10, 1000, None, True)
            if (len(files) == 1000):
                logger.warning("In smCheckUserDataDeleted - raising ValidationError because resource/{} folder has > 1000 files"
                        .format(resource.pk))                
                raiseException = True
            logger.debug("Found %d files to delete", len(files))
            files.reverse()
            fileAcc.deleteFileInfos(files)
            logger.debug("Deleting root")
            fileAcc.deletePaths("./")
        except Exception:
            logger.exception("Exception during check for cloud folders")
            logger.debug("Resource folder is non existent - delete proceeding")
        finally:
            if fileAcc:
                fileAcc.disconnectSession()
        if raiseException:
            raise ValidationError("resource/{} is not empty, user deletion not allowed".format(resource.pk))


def smCheckDescription(sender, **kwargs):
    '''
    This method is invoked automatically before a an object with a description is saved.
    It checks that the description is a minimum size. 
    '''
    o = kwargs.get('instance')
    if len(o.description) < 15:
        raise ValidationError("Description field must has at least 15 characters, found '{}'".format(o.description))


def smCheckResourceJson(sender, **kwargs):
    '''
    This method is invoked automatically before a resource is saved. It
    checks that the JSON is valid 
    '''
    o = kwargs.get('instance')
    paramJsonDict = json.loads(o.parametersJson)
    jsonschema.validate(paramJsonDict, RESOURCE_PARAMETERS_JSON_SCHEMA)


def smCheckTaskTypeJson(sender, **kwargs):
    '''
    This method is invoked automatically before a resource is saved. It
    checks that the JSON is valid 
    '''
    o = kwargs.get('instance')
    cfgJsonDict = json.loads(o.configurationJson)
    jsonschema.validate(cfgJsonDict, TASK_TYPE_CONFIGURATION_JSON_SCHEMA)


def smCheckTaskJson(sender, **kwargs):
    '''
    This method is invoked automatically before a resource is saved. It
    checks that the JSON is valid 
    '''
    o = kwargs.get('instance')
    paramJsonDict = json.loads(o.parametersJson)
    jsonschema.validate(paramJsonDict, TASK_PARAMETERS_JSON_SCHEMA)


def sm_remove_obj_perms_connected_with_user(sender, instance, **kwargs):
    filters = Q(content_type=ContentType.objects.get_for_model(instance),
        object_pk=instance.pk)
    UserObjectPermission.objects.filter(filters).delete()
    GroupObjectPermission.objects.filter(filters).delete()


def smCheckReservationForConflicts(sender, **kwargs):
    o = kwargs.get('instance')
    
    if o.start_time > o.end_time:
        errorDict = {"message": "End time must be greater than start time"}
        logger.warning(json.dumps(errorDict))
        raise ValidationError(errorDict)

    resourceQset = SmModelReservation.objects.filter(resource=o.resource)
    # If we are modifying a reservation do not check against ourself
    if o.id is not None:
        resourceQset = resourceQset.exclude(id=o.id)
        
    # Check if new start time is in an existing reservation
    conflictingReservations = resourceQset.\
        filter(start_time__lte=o.start_time).filter(end_time__gt=o.start_time)
    if conflictingReservations.count() == 0:
        # Check if new end time is in an existing reservation
        conflictingReservations = resourceQset.\
                    filter(start_time__lt=o.end_time).filter(end_time__gt=o.end_time)
    if conflictingReservations.count() == 0:
        # Check if there are reservations with start time > my start time AND end time < my end time
        # (e.g. contained in my reservation)
        conflictingReservations = resourceQset.\
                    filter(start_time__gt=o.start_time).filter(end_time__lt=o.end_time)
    if conflictingReservations.count() > 0:
        firstConflict = conflictingReservations[0]
        errorDict = {
            "message": "Reservation {} by user {} conflicts with this reservation".
            format(firstConflict.id, firstConflict.owner.username),
            "conflictingReservation": "/scs/reservation/{}/".format(firstConflict.id)
            }
        errorString = json.dumps(errorDict)
        logger.warning(errorString)
        raise ValidationError(errorDict)
 
              
def smAddUserToAllUserGroup(sender, **kwargs):
    user = kwargs.get('instance')
    try:
        all_users_group = Group.objects.get(name="all_users")
        all_users_group.user_set.add(user)
        logger.debug("Added user {} to group 'all_users'")
    except:
        logger.warning("Exception during adding of user to group")
 
                              
def smAddCommunityTag(sender, **kwargs):
    '''
    This method is invoked after a community is saved.  It auto-creates 
    the tag object for the community,  Note that no tag is created for the
    "all_resource" (aka Everything) community.
    '''
    o = kwargs.get('instance')
    if o.name_key == SM_COMMUNITY_ALL_NAME_KEY:
        return
    
    communityTagName = "community:" + o.name_key
    if SmModelTag.objects.filter(tag_name=communityTagName).count() == 0:
        communityTag = SmModelTag()
        communityTag.tag_name = communityTagName
        communityTag.save()
        logger.debug("Created community tag with name %s", communityTagName)

        # Associate newly created tag with the commmunity which was just saved
        # Fun fact: This will invoke this function recursively but it won't recurse infinitely
        # because of the check above
        o.tag = communityTag
        o.save()
        

def smDeleteCommunityTag(sender, **kwargs):
    '''
    This method is invoked after a community is saved.  It auto-creates 
    the tag object for the community,  Note that no tag is created for the
    "all_resource" (aka Everything) community.
    '''
    o = kwargs.get('instance')
    communityTagName = "community:" + o.name_key
    for tag in SmModelTag.objects.filter(tag_name=communityTagName):
        tag.delete()
        

def smTriggerSignalInstall():
    pass

# This signal updates the status of dependent containers and resources
# based on the status of an agent
signals.post_save.connect(smStatusUpdateOnAgentSave, sender=SmModelAgent)

# This connects the creation of API keys to the creation of users
signals.post_save.connect(create_api_key, sender=User)

# This creates a user_settings object when a user is created.
signals.post_save.connect(smCreateUserSettings, sender=User)

# This signal handler creates the private and public resource
# objects associated with each user.
signals.post_save.connect(smCreateUserResources, sender=User)

# This signal creates the folder on the folder associated with the 
# resource if the folder does not already exist.  This is important
# for the personal cloud resources which are created with each user.
signals.post_save.connect(smCreateResourceFolder, sender=SmModelResource)

# This checks that the cloud folders associated with a user
# have been deleted before allowing the user delete to proceed. 
signals.pre_delete.connect(smCheckUserDataDeleted, sender=User)

# Add descrition check before save_instance
signals.pre_save.connect(smCheckDescription, sender=SmModelResource)
signals.pre_save.connect(smCheckDescription, sender=SmModelAgent)
signals.pre_save.connect(smCheckDescription, sender=SmModelContainer)
signals.pre_save.connect(smCheckDescription, sender=SmModelResourceType)
signals.pre_save.connect(smCheckDescription, sender=SmModelResourceTypeTaskType)
signals.pre_save.connect(smCheckDescription, sender=SmModelTaskType)
signals.pre_save.connect(smCheckDescription, sender=SmModelCommunity)
signals.pre_save.connect(smCheckDescription, sender=SmModelAgent)

# Add JSON validator before resource save_instance
signals.pre_save.connect(smCheckResourceJson, sender=SmModelResource)
signals.pre_save.connect(smCheckTaskTypeJson, sender=SmModelTaskType)
signals.pre_save.connect(smCheckTaskJson, sender=SmModelTask)

# Delete permissions when user deleted
signals.pre_delete.connect(sm_remove_obj_perms_connected_with_user, sender=User)

# Prevent saving of invalid reservations
signals.pre_save.connect(smCheckReservationForConflicts, sender=SmModelReservation)

# Add membership in all_users
signals.post_save.connect(smAddUserToAllUserGroup, sender=User)

# Auto-create tag for Community
signals.post_save.connect(smAddCommunityTag, sender=SmModelCommunity)
signals.post_delete.connect(smDeleteCommunityTag, sender=SmModelCommunity)
