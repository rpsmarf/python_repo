describe( 'Task creation', function() {
	beforeEach (module('rpsmarf.config'));
	
	beforeEach(module('rpsmarf.taskcreate'));
	beforeEach(module('ui.router'));
	beforeEach(module('ui.bootstrap'));
	beforeEach(module('restangular'));

	beforeEach( module( 'rpsmarf.taskcommservice') );
	beforeEach(module('taskcreate/taskcreate.tpl.html'));
	beforeEach(module('taskcreate/taskResourceSelect.tpl.html'));
	beforeEach(module('taskcreate/ConfirmModal.tpl.html'));

	var scope, element, entries,timeout,homeSubEntries,documentsSubEntries,dataSubEntries;
	var task_types, httpBackend, taskParams;
	task_types =	[
					{
						"code_classname": "file_read_class",
						"code_module": "file_read",
						"configurationJson": "",
						"description": "This task type is for file read operations",
						"gid": 3,
						"id": 1,
						"name": "file_read",
						"resource_uri": "/scs/task_type/1/"
					},
					{
						"code_classname": "file_write_class",
						"code_module": "file_write",
						"configurationJson": "",
						"description": "This task type is for file write operations",
						"gid": 4,
						"id": 2,
						"name": "file_write",
						"resource_uri": "/scs/task_type/2/"
					},
					{
						"code_classname": "folder_read_class",
						"code_module": "folder_read",
						"configurationJson": "",
						"description": "This task type is for folder read operations",
						"gid": 5,
						"id": 3,
						"name": "folder_read",
						"resource_uri": "/scs/task_type/3/"
					},
					{
						"code_classname": "folder_write_class",
						"code_module": "folder_write",
						"configurationJson": "",
						"description": "This task type is for folder write operations",
						"gid": 6,
						"id": 4,
						"name": "folder_write",
						"resource_uri": "/scs/task_type/4/"
					},
					{
						"code_classname": "SmTaskRunnerCopy",
						"code_module": "smcommon.task_runners.task_runner_copy",
						"configurationJson": "{\"outputProgressMaxSize\": 1234, \"computeResourceRole\": \"dest\", \"args\": [{\"role\": \"src\", \"type\": \"resource\", \"direction\": \"input\", \"input_id\": 1}, {\"role\": \"dest\", \"type\": \"resource\", \"direction\": \"output\", \"input_id\": 2}], \"outputProgressSource\": \"dest\"}",
						"description": "Task to copy data",
						"gid": 9,
						"id": 5,
						"name": "copy",
						"resource_uri": "/scs/task_type/5/"
					},
					{
						"code_classname": "SmTaskRunnerShell",
						"code_module": "smcommon.task_runners.task_runner_shell",
						"configurationJson": "{\"computeResourceRole\": \"dest\", \"args\": [{\"type\": \"string\", \"value\": \"cp\"}, {\"type\": \"resource\", \"name\": \"Copy Source\", \"direction\": \"input\", \"input_id\": 1, \"role\": \"src\", \"description\": \"A file to copy.\"}, {\"type\": \"resource\", \"name\": \"Copy Destination\", \"direction\": \"output\", \"input_id\": 2, \"role\": \"dest\", \"description\": \"A folder to copy data to.\"}], \"outputProgressSource\": \"dest\"}",
						"description": "Task to copy data",
						"gid": 10,
						"id": 6,
						"name": "shell",
						"resource_uri": "/scs/task_type/6/"
					}
					];
	var task = {	"code_classname": "SmTaskRunnerShell",
					"code_module": "smcommon.task_runners.task_runner_shell",
					"configurationJson": "{\"computeResourceRole\": \"dest\", \"args\": [{\"type\": \"string\", \"value\": \"cp\"}, {\"type\": \"resource\", \"name\": \"Copy Source\", \"direction\": \"input\", \"input_id\": 1, \"role\": \"src\", \"description\": \"A file to copy.\"}, {\"type\": \"resource\", \"name\": \"Copy Destination\", \"direction\": \"output\", \"input_id\": 2, \"role\": \"dest\", \"description\": \"A folder to copy data to.\"}], \"outputProgressSource\": \"dest\"}",
					"description": "Task to copy data",
					"gid": 10,
					"id": 6,
					"name": "shell",
					"resource_uri": "/scs/task_type/6/"
				};
	var foo;
	var smarfurl;
	var state;

	beforeEach(  inject(function(SMARF_URL,$controller, $rootScope, $state, Restangular){
		scope = $rootScope.$new();
		smarfurl = SMARF_URL+'/scs/';
		Restangular.configuration.baseUrl = smarfurl;
		Restangular.configuration.encodeIds = false;

	})) ;

	it("shows the correct task types and sets the taskService tasktype properly", inject(function(taskInfo, $httpBackend, $controller) {
		$controller('TaskSelectCtrl',{$scope: scope});
		$httpBackend.when('GET',smarfurl+'task_type/').respond(200,{objects:task_types});
		$httpBackend.flush();
		expect(scope.tasktypes).toEqual(task_types);
		scope.createTaskType(task);
		expect(taskInfo.getTaskType()).toEqual(task);

	}));

	it("shows the correct resources for the tasktype", inject(function(taskInfo, $httpBackend, $state, $controller) {
		var t = { "code_classname": "SmTaskRunnerShell",
					"code_module": "smcommon.task_runners.task_runner_shell",
					"configurationJson": "{\"outputProgressSource\": \"dest\", \"computeResourceRole\": \"dest\", \"args\": [{\"type\": \"string\", \"value\": \"cp\"}, {\"role\": \"src\", \"type\": \"resource\", \"direction\": \"input\", \"name\": \"Copy Source\", \"input_id\": 1, \"description\": \"A file to copy.\"}, {\"role\": \"dest\", \"type\": \"resource\", \"direction\": \"output\", \"name\": \"Copy Destination\", \"input_id\": 2, \"description\": \"A folder to copy data to.\"}]}",
					"description": "Task to copy data",
					"gid": 29,
					"id": 8,
					"name": "shell_cp",
					"resource_uri": "/scs/task_type/8/"

				};

		$controller('TaskResourceSelectCtrl',{$scope: scope, $state:{params:{taskTypeId:t.id}, go:function(){} }});
		
		var rttts = [{"description": "","gid": 31,
					"id": 5,
					"metadataJson": "",
					"name": "",
					"resource_type": "/scs/resource_type/6/",
					"resource_uri": "/scs/resource_type_task_type/5/",
					"role": "src",
					"task_type": "/scs/task_type/8/"
					},
					{
					"description": "",
					"gid": 32,
					"id": 6,
					"metadataJson": "",
					"name": "",
					"resource_type": "/scs/resource_type/6/",
					"resource_uri": "/scs/resource_type_task_type/6/",
					"role": "dest",
					"task_type": "/scs/task_type/8/"
					}

					];

		var resourceType = {
							"codename": "",
							"configuration": "",
							"description": "",
							"gid": 34,
							"id": 6,
							"name": "d1",
							"nature": "data",
							"resource_uri": "/scs/resource_type/6/"
							};		

		var resources = [
						{
							"container": "/scs/container/4/",
							"description": "This is a short description of this resource.",
							"gid": 40,
							"icon": "",
							"id": 5,
							"name": "copy_src",
							"owner": "/scs/user/1/",
							"parametersJson": "{\"folder\": \"src/\"}",
							"public": false,
							"resource_type": "/scs/resource_type/6/",
							"resource_uri": "/scs/resource/5/",
							"status": ""
						},
						{
							"container": "/scs/container/4/",
							"description": "This is a long description of this resource. The main purpose of this description is to see what effects it will have on the UI. This folder lives witin the /tmp of your local file system. It should show an empty folder on page load, but thanks to the copy task we should be able to copy a file/folder from the src resource.",
							"gid": 41,
							"icon": "",
							"id": 6,
							"name": "copy_dest",
							"owner": "/scs/user/1/",
							"parametersJson": "{\"folder\": \"dest/\"}",
							"public": false,
							"resource_type": "/scs/resource_type/6/",
							"resource_uri": "/scs/resource/6/",
							"status": ""
						}
						];


		$httpBackend.when('GET',smarfurl+'resource_type_task_type/?task_type__id='+t.id).respond(200,{objects:rttts});
		$httpBackend.when('GET',smarfurl+'resource_type/6/').respond(200,resourceType);
		$httpBackend.when('GET',smarfurl+'resource/?resource_type__id=6').respond(200,{objects:resources});
		$httpBackend.when('GET',smarfurl+'task_type/'+t.id+'/').respond(200,t);
		$httpBackend.flush();

		var i=0;
		for(var key in scope.taskroles){
			i++;
		}
		expect(i).toEqual(rttts.length);
		expect(scope.taskroles[rttts[0].role]).toBeTruthy();
	
		expect(scope.resourceTypes['src']).toEqual(resources);
		expect(scope.resourceTypes['dest']).toEqual(resources);
	
		expect(scope.numForRole('src').indexOf('1 remaining')).toBeGreaterThan(-1);

		scope.selectResource(resources[0], 'src', {preventDefault: function(){}, target: angular.element('<button></button>')}, 5);
	
		expect(scope.numForRole('src').indexOf('0 remaining')).toBeGreaterThan(-1);
		expect(scope.taskParams.taskResources['src']).toBeTruthy();
		expect(scope.selResourceTypes['src']).toEqual(1);

		scope.selectResource(resources[1], 'dest', {preventDefault: function(){}, target: angular.element('<button></button>')}, 6);
		expect(scope.numForRole('dest').indexOf('0 remaining')).toBeGreaterThan(-1);
		expect(scope.taskParams.taskResources['dest']).toBeTruthy();
		expect(scope.selResourceTypes['dest']).toEqual(1);

		scope.configureTask();
		taskParams = taskInfo.getParameters();
		expect(taskParams).toBeTruthy();


	}));
	
	it("allows the user to configure inputs and start task", inject(function(taskInfo, $httpBackend, $state, $controller, $modal) {

		var t = { "code_classname": "SmTaskRunnerShell",
					"code_module": "smcommon.task_runners.task_runner_shell",
					"configurationJson": "{\"outputProgressSource\": \"dest\", \"computeResourceRole\": \"dest\", \"args\": [{\"type\": \"string\", \"value\": \"cp\"}, {\"role\": \"src\", \"type\": \"resource\", \"direction\": \"input\", \"name\": \"Copy Source\", \"input_id\": 1, \"description\": \"A file to copy.\"}, {\"role\": \"dest\", \"type\": \"resource\", \"direction\": \"output\", \"name\": \"Copy Destination\", \"input_id\": 2, \"description\": \"A folder to copy data to.\"}]}",
					"description": "Task to copy data",
					"gid": 29,
					"id": 8,
					"name": "shell_cp",
					"resource_uri": "/scs/task_type/8/"

				};
		taskInfo.setTaskType(t);	
		taskInfo.setParameters(taskParams);

		$controller('TaskConfigureCtrl',{$scope: scope, $state:{params:{taskTypeId:t.id, resourcesIds:'5-6'}, go:function(){} }, $modal: $modal});
		
		var rttts = [{"description": "","gid": 31,
					"id": 5,
					"metadataJson": "",
					"name": "",
					"resource_type": "/scs/resource_type/6/",
					"resource_uri": "/scs/resource_type_task_type/5/",
					"role": "src",
					"task_type": "/scs/task_type/8/"
					},
					{
					"description": "",
					"gid": 32,
					"id": 6,
					"metadataJson": "",
					"name": "",
					"resource_type": "/scs/resource_type/6/",
					"resource_uri": "/scs/resource_type_task_type/6/",
					"role": "dest",
					"task_type": "/scs/task_type/8/"
					}

					];

		var resourceType = {
							"codename": "",
							"configuration": "",
							"description": "",
							"gid": 34,
							"id": 6,
							"name": "d1",
							"nature": "data",
							"resource_uri": "/scs/resource_type/6/"
							};		

		var resources = [
						{
							"container": "/scs/container/4/",
							"description": "This is a short description of this resource.",
							"gid": 40,
							"icon": "",
							"id": 5,
							"name": "copy_src",
							"owner": "/scs/user/1/",
							"parametersJson": "{\"folder\": \"src/\"}",
							"public": false,
							"resource_type": "/scs/resource_type/6/",
							"resource_uri": "/scs/resource/5/",
							"status": ""
						},
						{
							"container": "/scs/container/4/",
							"description": "This is a long description of this resource. The main purpose of this description is to see what effects it will have on the UI. This folder lives witin the /tmp of your local file system. It should show an empty folder on page load, but thanks to the copy task we should be able to copy a file/folder from the src resource.",
							"gid": 41,
							"icon": "",
							"id": 6,
							"name": "copy_dest",
							"owner": "/scs/user/1/",
							"parametersJson": "{\"folder\": \"dest/\"}",
							"public": false,
							"resource_type": "/scs/resource_type/6/",
							"resource_uri": "/scs/resource/6/",
							"status": ""
						}
						];

		homeSubEntries = [{basename:'documents/', path:'/home/documents/', isDir:true},{basename:'pictures/', path:'/home/pictures/',isDir:true}, {basename:'myfile', path:'/home/documents/myfile',isDir:false}];
		documentsSubEntries = [{basename:'report1.doc', path:'/home/documents/report1.doc',isDir:false},{basename:'paper.pdf', path:'/home/documents/paper.pdf',isDir:false}];

		
		$httpBackend.when('GET',smarfurl+'resource/5/list/').respond(200,homeSubEntries);
		$httpBackend.when('GET',smarfurl+'resource/6/list/').respond(200,homeSubEntries);

		$httpBackend.when('GET',smarfurl+'resource/?resource_type__id=6').respond(200,{objects:resources});
		$httpBackend.when('GET',smarfurl+'task_type/'+t.id+'/').respond(200,t);
		$httpBackend.flush();

		
		expect(scope.args.length).toEqual(resources.length);

		var arg = {type:'resource',input_id: 1,name:'Copy Source',description:'A file to copy.',role:'src', resource:resources[0]};
		arg.resource.entries = homeSubEntries;
		arg.resource.levels = [homeSubEntries];
		arg.resource.path = '/';
		
		//expect(angular.equals(scope.args.toString().indexOf(arg.toString())).toBeGreaterThan(-1);

		
		expect(angular.equals(angular.copy(scope.args[0].resource.entries),homeSubEntries)).toBe(true);
		var argP;
		for(var i=0; i<scope.args.length;i++){
			argP = scope.args[i];
			if(argP.role === 'src'){
				argP.inputValue = 'a.txt';
			}
			else if(argP.role === 'dest'){
				argP.inputValue = '/';
			}
		}

		scope.startTask();
		
		scope.modalInstance.opened.then(function(){
			scope.modalInstance.close();
			$httpBackend.expect('POST',smarfurl+'/scs/task/',{owner:'/scs/user/1/', task_type:'/scs/task_type/'+t.id+'/', status: "init", start_time : '2014-11-10T03:07:43', end_time : '2014-11-11T03:07:43', parametersJson: "{\"input_ids\": {\"1\":{\"path\": \"a.txt\"}, \"2\":{\"path\": \"/\"}}}"},null,{"Content-Type":"application/json"})
					.respond({headers: function(name){return "/scs/task/1/";}});

			$httpBackend.expect('POST', smarfurl+'task_resource/',{task:"/scs/task/1/", resource: '/scs/resource/'+resources[0].id+'/', resource_type_task_type: '/scs/resource_type_task_type/5/'},null,{"Content-Type":"application/json"});
			$httpBackend.expect('POST', smarfurl+'task_resource/',{task:"/scs/task/1/", resource: '/scs/resource/'+resources[1].id+'/', resource_type_task_type: '/scs/resource_type_task_type/6/'},null,{"Content-Type":"application/json"});
			$httpBackend.expect('GET',smarfurl+'task/1/?action=start');

			$httpBackend.flush();
		});
		

	}));

	it('should show progress for a task', inject(function($controller, $rootScope, Restangular, $interval,$httpBackend, $modal){
		
		
		var scope = $rootScope.$new();
		var taskObject = Restangular.one('task','1/');
		
		var controller = $controller('TaskProgModalCtrl',{$scope:scope,taskObj: taskObject, $interval:$interval, $modalInstance: {result:{then:function(param){ return true;}}}, modalTitle:"", interactive:false} );

		$httpBackend.expect('GET', smarfurl+'task/1/').respond({progressJson:"{\"progress\":50}"});
		$interval.flush(1000);
		$httpBackend.flush();
		expect(scope.percentComplete).toEqual(50);
		expect(scope.status).toEqual(['active', 'progress-striped']);

		$httpBackend.expect('GET', smarfurl+'task/1/').respond({progressJson:"{\"progress\":100}"});
		$interval.flush(1000);
		$httpBackend.flush();

		expect(scope.percentComplete).toEqual(100);


	}));
	
  
});

