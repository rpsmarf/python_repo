''' 
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
 
    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jan 15, 2015

    @author: rpsmarf
'''

import logging

logger = logging.getLogger(__name__)

TEXT_FIELD_SIZE = 254

TIME_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
STATS_NAME = 'name'
STATS_VALUE = 'value'
JSON_CONTENT = 'application/json'

INFO_NAME = 'name'
INFO_SYNOPSIS = 'synopsis'
INFO_VERSION = 'version'
INFO_INSTITUTION = 'institution'
INFO_RELEASE_TIME_JSON = 'releaseTime'
INFO_SUPPORT_EMAIL_JSON = 'supportEmail'
INFO_CATEGORY = 'category'
INFO_RESEARCH_SUBJECT_JSON = 'researchSubject'
INFO_TAGS = 'tags'

FIELD_NOT_SET_ERROR = 'Field {0} not set'

STAT_LAST_RESET = "lastReset"
STAT_UPTIME_IN_SEC = "uptimeInSec"

LICENCE_URL = "licenseUrl"
VIDEO_URL_FAVORITE_A_RESOURCE = "videoFavoriteAResource"
VIDEO_URL_FAVORITE_A_TOOL = "videoFavoriteATool"
VIDEO_URL_ACCESS_DATA = "videoAccessData"
VIDEO_URL_RUN_A_TOOL_BATCH = "videoRunAToolBatch"
VIDEO_URL_RUN_A_TOOL_INTERACTIVE = "videoRunAToolInteractive"
