import json
import os
import stat
import sys
from scripts.scs_task_start import scsTaskStartByPath
from scripts.scs_obj_common import scsScriptGetDefaultUrl
import shutil
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath
from scripts.scs_obj_create import scsCreateObjectPath

try:
    os.mkdir("/tmp/src/adir")
except:
    pass

with open("/tmp/src/adir/f1", 'w') as the_file:
    the_file.write("Hello world 1")
with open("/tmp/src/adir/f2", 'w') as the_file:
    the_file.write("Hello world 2")

try:
    os.chmod("/tmp/dest", stat.S_IROTH | stat.S_IWOTH | stat.S_IXOTH 
             | stat.S_IRGRP | stat.S_IWGRP | stat.S_IXGRP
             | stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
    shutil.rmtree("/tmp/dest/bdir")
except:
    pass

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

taskParams = {
              "input_ids": {
                            "1": {"path": "adir/"},
                            "2": {"path": "bdir/"}
                            }
              }

tt_path = scsGetObjectPath("task_type", "name=shell_cp_folder_to_folder")
root_path = scsGetObjectPath("user", "name=rpsmarf")
task_path = scsCreateObjectPath("task", {"name": "shell_cp", 
                                         "task_type": tt_path,
                                         "owner": root_path,
                                         "parametersJson": json.dumps(taskParams),
                                         }
                                )

resource_type_task_type_data_dest_path = scsGetObjectPath("resource_type_task_type", "role=dest")
resource_type_task_type_data_src_path = scsGetObjectPath("resource_type_task_type", "role=src")


src_resource_path = scsGetObjectPath("resource", "name=copy_src")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": src_resource_path,
                                      "resource_type_task_type": resource_type_task_type_data_src_path,
                                      }
                    )

dest_resource_path = scsGetObjectPath("resource", "name=copy_dest")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": dest_resource_path,
                                      "resource_type_task_type": resource_type_task_type_data_dest_path,
                                      }
                    )

print("Task " + task_path + " to copy /tmp/src/adir to /tmp/dest/bdir is setup, starting task...")

scsTaskStartByPath(task_path, verbose=True)

with open("/tmp/dest/bdir/f1", 'r') as the_file:
    s = the_file.read(1000)
if s != "Hello world 1":
        print("ERROR - Invalid data read: %s", s)
        sys.exit(1)

with open("/tmp/dest/bdir/f2", 'r') as the_file:
    s = the_file.read(1000)
if s != "Hello world 2":
        print("ERROR - Invalid data read: %s", s)
        sys.exit(1)


