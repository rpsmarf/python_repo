'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jun 16, 2015

    @author: rpsmarf
'''
import logging
import requests
import json
from email.utils import parsedate_to_datetime
from smcommon.utils.date_utils import sm_date_to_unixtime_sec,\
    smMakeDateFromString
from smcommon.exceptions.file_access_exception import SmFileAccessException

logger = logging.getLogger(__name__)

   
class SwiftAccessor(object):  
    
    def __init__(self, swiftEndpointURI, identityEndpointURI,
                userName, password, tenantName, containerName, 
                retryCount, optionalXAuthToken):
        self.swiftEndpointURI = swiftEndpointURI
        self.identityEndpointURI = identityEndpointURI
        self.userName = userName
        self.password = password
        self.tenantName = tenantName
        self.containerName = containerName
        if retryCount < 1 or retryCount is None:
            self.retryCount = 1
        else:
            self.retryCount = retryCount
        self.XAuthToken = optionalXAuthToken
        self.reautheticate = 0
        self.retryForToken = 0   
        self.downloadChunkSize = 10000
        
    def getToken(self):
        '''
        Fetches new XAuth token 
        '''
        identityURI = self.identityEndpointURI + "tokens"
        data = {"auth": {"tenantName": self.tenantName, "passwordCredentials": {"username": self.userName,
                                                                                "password": self.password}}}
        headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
        r = requests.post(identityURI, data=json.dumps(data), headers=headers)
        rsp = json.loads(r.text)
        rsp = rsp['access']['token']['id']
        return rsp
                           
    def statusCodeTranslator(self, httpStatusCode):
        '''
        Takes decision based on the response status code. If there is authentication
        error : 403, a new XAuth token is fetched and 
        exception is raised , so that the system can retry using the new XAuth token
        '''
        self.reautheticate = 0
        if httpStatusCode <= 299:
            self.reautheticate = 0
       
        elif httpStatusCode == 401:
            logger.debug("Got HTTP code 401 - considering reauthenticating")
            self.retryForToken = self.retryForToken + 1
            if self.retryForToken > 3:
                logger.warning("Too many tries")
                self.reautheticate = 0
            else: 
                logger.debug("Getting new token")
                self.reautheticate = 1
                self.XAuthToken = self.getToken()
            raise Exception("Authentication error, retry = " + str(self.reautheticate))
        else:
            logger.warning("HTTP Exception code " + str(httpStatusCode))
            raise Exception("HTTP Exception code " + str(httpStatusCode))
    
    def listInsideContainer(self, path, recursive, maxEntries, lastPathReturned):
        '''
        Returns the content present in the directory (provided as a parameter) in JSON format. 
        '''            
        if maxEntries > 10000:
            maxEntries = 10000
            
        if lastPathReturned is not None and lastPathReturned != "":
            marker_param = "&marker=" + lastPathReturned
        else:
            marker_param = ""
        
        if recursive:
            if path != "":
                path_param = "&prefix=" + path
            else:
                path_param = ""
        else:
            path_param = "&path=" + path
            
        retryCount = self.retryCount
        dairURL = self.swiftEndpointURI + "/" + self.containerName + \
            "?format=json" + path_param + "&limit=" + str(maxEntries) + marker_param         
        while True:
            try:
                commonHeader = {'X-Auth-Token': self.XAuthToken, 'Accept': 'application/json'}
                rd = requests.get(dairURL, headers=commonHeader)
                self.statusCodeTranslator(rd.status_code)
                break
            except Exception as e:
                retryCount = retryCount - 1
                if retryCount == 0 or self.reautheticate == 0:
                    raise SmFileAccessException(422, "EOTHER", path, "Cloud list failed: " + str(e))
              
        result_list = json.loads(rd.text)
        return_list = []
        for result in result_list:
            name = result["name"]
            if name == path and name.endswith("/"):
                continue  # Skip the directory entry itself which shows up sometimes
            return_list.append({"name": name,
                                "mode": "rwx",
                                "isDir": name.endswith("/"),
                                "size": result["bytes"],
                                "mtime": int(sm_date_to_unixtime_sec(smMakeDateFromString(result["last_modified"])))})
        return return_list

    def listContainers(self):
        '''
        Returns the list of containers 
        '''            
        retryCount = self.retryCount
        dairURL = self.swiftEndpointURI + \
            "?format=json" 
        while True:
            try:
                commonHeader = {'X-Auth-Token': self.XAuthToken, 'Accept': 'application/json'}
                rd = requests.get(dairURL, headers=commonHeader)
                self.statusCodeTranslator(rd.status_code)
                break
            except Exception as e:
                retryCount = retryCount - 1
                if retryCount == 0 or self.reautheticate == 0:
                    raise SmFileAccessException(422, "EOTHER", "", "Container list failed: " + str(e))
              
        result_list = json.loads(rd.text)
        return result_list

    def getMetadata(self, path):
        '''
        Returns metadata for the path provided 
        '''            
            
        retryCount = self.retryCount
        dairURL = self.swiftEndpointURI + "/" + self.containerName + "/" + path + \
            "?format=json"         
        while True:
            try:
                commonHeader = {'X-Auth-Token': self.XAuthToken, 'Accept': 'application/json'}
                rd = requests.head(dairURL, headers=commonHeader)
                self.statusCodeTranslator(rd.status_code)
                break
            except Exception as e:
                retryCount = retryCount - 1
                if retryCount == 0 or self.reautheticate == 0:
                    raise SmFileAccessException(422, "EOTHER", path, "Cloud metadata request failed: " + str(e))
              
        if "last-modified" in rd.headers:
            last_modified_time = sm_date_to_unixtime_sec(parsedate_to_datetime(rd.headers["Last-Modified"]))
        else:
            last_modified_time = float(rd.headers["X-Timestamp"])
        if path == "":
            path = "/"
        result_dict = {"name": path, 
                       "size": int(rd.headers["Content-Length"]),
                       "mode": "rwx",
                       "isDir": path.endswith("/"),
                       "mtime": int(last_modified_time)}
        return result_dict
        
    def createContainerSubFolder(self, folderPath):
        '''
        Creates a (virtual) sub-folder under the container (specified in the constructor)
        param @folderPath expects the name of the complete sub-folder path with NO
        initial slash ('/') and mandatory trailing slash('/'_
        example : test_folder/inside_test_folder/  
        To create the container if it does not exist : pass " " as @folderPath value
        '''
        retryCount = self.retryCount
        dairURL = self.swiftEndpointURI + "/" + self.containerName + "/" + folderPath            
        while True:
            try:
                commonHeader = {'X-Auth-Token': self.XAuthToken, 'Accept': 'application/json'}
                rd = requests.put(dairURL, headers=commonHeader)
                self.statusCodeTranslator(rd.status_code)
                break
            except Exception as e:
                retryCount = retryCount - 1
                if retryCount == 0 or self.reautheticate == 0:
                    raise SmFileAccessException(422, "EOTHER", folderPath, "Cloud folder create request failed: " + str(e))
        return rd.status_code
    
    def deleteContainerSubFolder(self, folderPath):
        '''
        Deletes the subfolder inside the container. The subfolder path is provided as parameter @folderPath
        '''
        retryCount = self.retryCount
        dairURL = self.swiftEndpointURI + "/" + self.containerName + "/" + folderPath            
        while True:
            try:
                commonHeader = {'X-Auth-Token': self.XAuthToken, 'Accept': 'application/json'}
                rd = requests.delete(dairURL, headers=commonHeader)
                self.statusCodeTranslator(rd.status_code)
                break
            except Exception as e:
                retryCount = retryCount - 1
                if retryCount == 0 or self.reautheticate == 0:
                    raise SmFileAccessException(422, "EOTHER", folderPath, "Cloud folder delete request failed: " + str(e))
        return rd.status_code
    
    def _deleteContainer(self, containername):
        '''
        Deletes the container . Parameter @containername : Name of the container to be deleted
        '''
        retryCount = self.retryCount
        dairURL = self.swiftEndpointURI + "/" + containername + "/"            
        while True:
            try:
                commonHeader = {'X-Auth-Token': self.XAuthToken, 'Accept': 'application/json'}
                rd = requests.delete(dairURL, headers=commonHeader)
                self.statusCodeTranslator(rd.status_code)
                break
            except Exception as e:
                retryCount = retryCount - 1
                if retryCount == 0 or self.reautheticate == 0:
                    raise SmFileAccessException(422, "EOTHER", containername, "Container delete failed: " + str(e))
        return rd.status_code
    
    def uploadContent(self, pathInsideContainer, fileName, data):
        retryCount = self.retryCount
        dairURL = self.swiftEndpointURI + "/" + self.containerName +\
            "/" + pathInsideContainer + "/" + fileName
        while True:
            commonHeader = {'X-Auth-Token': self.XAuthToken, 'Accept': 'application/json'}
            try:
                rd = requests.put(dairURL, data=data, headers=commonHeader)
                self.statusCodeTranslator(rd.status_code)
                break
            except Exception as e:
                retryCount = retryCount - 1
                if retryCount == 0 or self.reautheticate == 0:
                    raise SmFileAccessException(422, "EOTHER", pathInsideContainer + "/" + fileName, 
                                                "File upload failed: " + str(e))
        return rd.status_code
                   
    def uploadChunk(self, path, gen):
        retryCount = self.retryCount
        dairURL = self.swiftEndpointURI + "/" + self.containerName + "/" + path
        while True:
            commonHeader = {'X-Auth-Token': self.XAuthToken, 
                            'Accept': 'application/json',
                            'Transfer-Encoding': 'chunked'}
            try:
                rd = requests.put(dairURL, data=gen(), headers=commonHeader)
                self.statusCodeTranslator(rd.status_code)
                break
            except Exception as e:
                retryCount = retryCount - 1
                if retryCount == 0 or self.reautheticate == 0:
                    raise SmFileAccessException(422, "EOTHER", path, 
                                                "File upload failed: " + str(e))
        return rd.status_code
                   
    def deleteObjectFromContainer(self, fullPathToFile):
        retryCount = self.retryCount
        dairURL = self.swiftEndpointURI + "/" + self.containerName + "/" + fullPathToFile            
        while True:
            try:
                commonHeader = {'X-Auth-Token': self.XAuthToken, 'Accept': 'application/json'}
                rd = requests.delete(dairURL, headers=commonHeader)
                self.statusCodeTranslator(rd.status_code)
                break
            except Exception as e:
                retryCount = retryCount - 1
                if retryCount == 0 or self.reautheticate == 0:
                    raise SmFileAccessException(422, "EOTHER", fullPathToFile, 
                                                "Object delete failed: " + str(e))
        return rd.status_code   
     
    def downloadAsStream(self, pathToFile):
        '''
        This method returns a response object which can be iterated over
        as binary chunks to get the contents in a "streamed" fashion.
        '''
        retryCount = self.retryCount
        dairURL = self.swiftEndpointURI + "/" + self.containerName + "/" + pathToFile + ""            
        while True:
            try:
                commonHeader = {'X-Auth-Token': self.XAuthToken, 'Accept': 'application/json'}
                rd = requests.get(dairURL, headers=commonHeader, stream=True)
                self.statusCodeTranslator(rd.status_code)
                return rd.iter_content(self.downloadChunkSize)
            except Exception as e:
                retryCount = retryCount - 1
                if retryCount == 0 or self.reautheticate == 0:
                    raise SmFileAccessException(422, "EOTHER", pathToFile, 
                                                "Download failed: " + str(e))
                
        raise Exception("Timeout on authentication retries")

    def downloadContent(self, pathToFile):
        '''
        This method returns a the content of the file specified
        '''
        retryCount = self.retryCount
        dairURL = self.swiftEndpointURI + "/" + self.containerName + "/" + pathToFile + ""            
        while True:
            try:
                commonHeader = {'X-Auth-Token': self.XAuthToken, 'Accept': 'application/json'}
                rd = requests.get(dairURL, headers=commonHeader)
                self.statusCodeTranslator(rd.status_code)
                break
            except Exception as e:
                retryCount = retryCount - 1
                if retryCount == 0 or self.reautheticate == 0:
                    raise SmFileAccessException(422, "EOTHER", pathToFile, 
                                                "Download content failed: " + str(e))
        return rd.content




