angular.module( 'rpsmarf.confirm', [
	'ui.bootstrap',
	'ngSanitize'
  
])

.controller( 'ConfirmCtrl', ['$scope', '$modalInstance','title', 'body', function($scope, $modalInstance, title, body){
  $scope.title = title;
  $scope.body = body;
  $scope.ok = function(){
    $modalInstance.close('ok');
  };

  $scope.cancel = function(){
    $modalInstance.dismiss('cancel');
  };

  $scope.okBtnText = function(){
    return 'Ok';
  };

  $scope.cancelBtnText = function(){
    return 'Cancel';
  };

}])
.controller( 'ConfirmQueueCtrl', ['$scope', '$modalInstance','title', 'body', function($scope, $modalInstance, title, body){
  $scope.title = title;
  $scope.body = body;

  $scope.ok = function(){
    $modalInstance.close('queue');
  };

  $scope.cancel = function(){
    $modalInstance.dismiss('cancel');
  };

  $scope.okBtnText = function(){
    return 'Queue';
  };

  $scope.cancelBtnText = function(){
    return 'Cancel';
  };

}])
;
