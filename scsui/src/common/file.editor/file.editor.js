angular.module('rpsmarf.file.editor',[
	'ui.bootstrap',
	'restangular',
	'rpsmarf.alert',
	'rpsmarf.user.service'
])
.controller('FileEditorCtrl', ['$scope','$modalInstance', 'file', 'Restangular','SmUser',  function($scope, $modalInstance, file, Restangular, SmUser){

	/***STANDARD VARIABLES***/
	var typeName;
	var fileUrl = file.url;
	var xhr;
	var csrf = SmUser.getProperty('csrf');
	/***SCOPE VARIABLES***/
	
	$scope.fileContent = '';
	$scope.fileName = file.name;
	$scope.editingFile = false;
	$scope.buttonText = 'Edit';

	/***SCOPE FUNCTIONS***/
	$scope.editFile = function(){
		$scope.editingFile = !$scope.editingFile;

		if($scope.editingFile){
			$scope.buttonText = 'Save';
		}
		else{
			$scope.buttonText = 'Edit';

			var formData = new FormData();
			var blob = new Blob([$scope.fileContent], {type:'text/plain'});
			formData.append('uploadfile', blob, 'uploadfile');
			xhr = new XMLHttpRequest();
			xhr.addEventListener('error', onload, false);
			
			//xhr.upload.addEventListener('progress', onprogress, false);
			xhr.upload.addEventListener('error', onload, false);
			xhr.addEventListener('load', onload, false);

			xhr.open('POST', Restangular.configuration.baseUrl+'/'+fileUrl+'upload/?overwrite=True', true);
			xhr.setRequestHeader('X-CSRFToken',csrf);
			xhr.responseType = 'json';
			xhr.send(formData);
			
			
		}
	};

	$scope.close = function(){
		//if($scope.exi)
		$modalInstance.close();
	};

	/***STANDARD FUNCTIONS***/
	var onload = function(e){
		if(xhr.status == 200){
			//$scope.$apply('percentComplete = 100');	
		}
		else{
			if(xhr.response && xhr.response.result === 'failure'){
				message = 'Could not save the file due to the following error:<br>' + xhr.response.description;
			}
			else{
				message = 'An error occured while saving the file';
			}
			$modal.open({
				controller: 'AlertCtrl',
				templateUrl: 'alert/alert.tpl.html',
				resolve: {
					message: function(){ return message; },
					title: function(){ return 'Error'; }
				}
			});
		}
		
	};
	
	/***EXE***/
	Restangular.configuration.fullResponse = true;
	Restangular.all(fileUrl+'download/').get('').then(function(response){
		Restangular.configuration.fullResponse = false;
		$scope.fileContent = response.data;
	});


}]);