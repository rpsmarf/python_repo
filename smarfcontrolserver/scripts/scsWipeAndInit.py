#!/usr/bin/env python 
'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''

# Delete the log file in case it was created with root user 
from subprocess import call
import logging
from smcommon.globals.envvar import SM_ENV_VAR_TEST_PASSWORD, smCommonGetEnvVar
logger = logging.getLogger()
try:
    call(["sudo", "/bin/rm", "/tmp/common_unittest.log"])
except:
    logger.warning("Failure in rm", exc_info=True)


import os
import sys
import time
sys.path.append("/opt/smarf-scs/scs")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "smarfcontrolserver.settings")
from django.conf import settings
from collections import OrderedDict
from subprocess import Popen
from scripts.scsCreateUser import create_users
from scripts.scsCreateCsProp import create_cs_prop, create_prop
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.db import models
from tastypie.models import create_api_key

#Resource type definitions. There should no be duplicates
rt = {'file': 1, 'folder': 2}
#Task type definitions. There should no be duplicates
tt = {'file_read': 1, 'file_write': 2, 'folder_read': 3, 'folder_write': 4}

#Dictionaries sorted by their values which will correspond to their ids within the database
resource_types = OrderedDict(sorted(rt.items(), key=lambda t: t[1]))
task_types = OrderedDict(sorted(tt.items(), key=lambda t: t[1]))
global testing 

testing = False


def set_testing(t):
    global testing
    testing = t


def clear_and_init(num, nopt):
    
    global testing
    
    mysql_user = settings.DATABASES['default']['USER'] 
    mysql_password = settings.DATABASES['default']['PASSWORD']
    if testing:
        mysql_db = settings.DATABASES['default']['TEST_NAME']
    else:
        mysql_db = settings.DATABASES['default']['NAME']
    if not testing:
        print('Dropping database with name ' + mysql_db) 
        Popen(["mysql", "--user=" + mysql_user, "--password=" + mysql_password,
               "-e", "DROP DATABASE IF EXISTS " + mysql_db])
        Popen(["mysql", "--user=" + mysql_user, "--password=" + mysql_password, 
               "-e", "CREATE DATABASE IF NOT EXISTS " + mysql_db])
        time.sleep(2)
        print('Syncing database with name ' + settings.DATABASES['default']['NAME'])
        if nopt:
            call([os.getcwd() + "/manage.py", "syncdb", "--noinput"])
        else:
            call(["/opt/smarf-scs/scs/manage.py", "syncdb", "--noinput"])
     
    models.signals.post_save.connect(create_api_key, sender=User)
     
    Group.objects.create(name='all_users')
    User.objects.create_superuser('rpsmarf', 'support@rpsmarf.ca', smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD), 
                                  first_name="Henry", last_name="Tory")
     
    if(num > 0):
        create_users(num)
        User.objects.all().iterator().__next__()
    
    create_cs_prop("licenseUrl", "http://www.gnu.org/licenses/gpl-3.0-standalone.html")
    create_cs_prop("videoFavoriteAResource", 'https://youtube.com/embed/E24gHJxgWcw')
    create_cs_prop("videoFavoriteATool", 'https://youtube.com/embed/h2Y5hAaNpBI')
    create_cs_prop("videoAccessData", 'https://youtube.com/embed/Te55EtPeta8')
    create_cs_prop("videoRunAToolBatch", 'https://youtube.com/embed/y5lKDNExnGU')
    create_cs_prop("videoRunAToolInteractive", 'https://youtube.com/embed/6GSKgBca0Ak')
    create_cs_prop("videoIntroduction", 'https://youtube.com/embed/y5lKDNExnGU')
    create_cs_prop("docUserManual", 'https://www.rpsmarf.ca/docs/RP-SMARF%20Getting%20Started%20User%20Guide.pdf')
    create_cs_prop("docWhitePaper", 'https://www.rpsmarf.ca/docs/RP-SMARF%20White%20Paper.pdf')
    create_cs_prop("paperOverview", 'https://www.rpsmarf.ca/docs/RP-SMARF%20Overview%20Paper.pdf')
    
    create_prop("licenseUrl", "http://www.gnu.org/licenses/gpl-3.0-standalone.html")
    create_prop("videoFavoriteAResource", 'https://youtube.com/embed/E24gHJxgWcw')
    create_prop("videoFavoriteATool", 'https://youtube.com/embed/h2Y5hAaNpBI')
    create_prop("videoAccessData", 'https://youtube.com/embed/Te55EtPeta8')
    create_prop("videoRunAToolBatch", 'https://youtube.com/embed/y5lKDNExnGU')
    create_prop("videoRunAToolInteractive", 'https://youtube.com/embed/6GSKgBca0Ak')
    create_prop("videoIntroduction", 'https://youtube.com/embed/y5lKDNExnGU')
    create_prop("docUserManual", 'https://www.rpsmarf.ca/docs/RP-SMARF%20Getting%20Started%20User%20Guide.pdf')
    create_prop("docWhitePaper", 'https://www.rpsmarf.ca/docs/RP-SMARF%20White%20Paper.pdf')
    create_prop("paperOverview", 'https://www.rpsmarf.ca/docs/RP-SMARF%20Overview%20Paper.pdf')
    create_prop("paperOverviewCitation", 'A McGregor, D. Bennett, S. Majumdar, B. Nandy,'
                ' J.O. Melendez,, M. St-Hilaire, D. Lau, J. Liu, '
                '"A Cloud-Based Platform for Supporting Research Collaboration",'
                '8th IEEE International Conference on Cloud Computing, June, 2015, New York, USA' 
                )       
       
    create_cs_prop('name', "RP-SMARF")
    create_cs_prop('synopsis', "The Research Platform for Smart Facilities Management (RP-SMARF) "
          "is a middleware platform whose main object is to facilitate resource and data sharing "
          "among researchers. Within the context of this project "
          "a resource may refer to a computing resource (physical or virtual computer), a data "
          "storage resource, files or folders located on a computer and a software application "
          "called a tool. By facilitating resource sharing researchers are able "
          "to access a large and diverse set of resources and data through a network which will "
          "lead to higher productivity and effectiveness for the various collaborative efforts ")
    create_cs_prop('version', "V0.9")
    create_cs_prop('institution', "Carleton University")
    create_cs_prop('releaseTime', "2015-01-19T15:55:12Z")
    create_cs_prop('supportEmail', "support@rpsmarf.ca")
    create_cs_prop('category', "Other")
    create_cs_prop('researchSubject', "Multi-discipline")
    create_cs_prop('tags', '["civil-engineering","mechanical-engineering"]')
    
    
def check_user_params(num):
   
    if num == '0' or num is 0:
        print('ERROR: Cannot add 0 users.')
        return False
    elif type(num) is str and not num.isdigit():
        print('ERROR: Please enter a positive integer.')
        return False
    else:
        return True
   

if __name__ == "__main__":
    argv = sys.argv
    if len(argv) > 3:
        print('Usage: scsWipeAndInit.py [--nopt]')
        print('OR')
        print('scsWipeAndInit.py [--nopt] number_users_to_create')
        print('No actions were performed')
        print(' If the --nopt option is specified then a version of the code')
        print(' relative to the current path is used instead of the version')
        print(' stored in the standard system folder in /opt/smarf-scs')
        print(' Note that if --nopt is specified then manage.py must be in current folder')
        sys.exit()
    elif len(argv) == 2 and argv[1] == '--nopt':
        clear_and_init(0, True)
    elif len(argv) == 2 and check_user_params(argv[1]):    
        clear_and_init(int(argv[1], False))
    elif len(argv) == 3 and argv[1] == '--nopt' and check_user_params(argv[2]):
        clear_and_init(int(argv[2], True))
    else:  
        clear_and_init(0, False)   
