'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
from smcommon.constants.sm_const import smConstGetMyAgentId,\
    SM_DATA_CONTAINER_PROTOCOL_ICE 
import logging
import json
import os
from smcommon.utils.json_utils import dumpsPretty
import urllib
import stat

logger = logging.getLogger(__name__)


class SmDataContainer(object):
    '''
    Object representing a file system somewhere in the world.
    
    The filesystem may be accessed locally or via SFTP (controlled
    by the protocol member)
    
    The filesystem may also be accessible via another remote agent 
    (controlled by the agentId member) 
    '''
    
    def __init__(self, agentId, agentUrl, containerUri):
        self.agentId = agentId
        self.containerUri = urllib.parse.urlparse(containerUri)
        self.paramDict = urllib.parse.parse_qs(self.containerUri.query)
        self.agentUri = agentUrl  # We use "" because this is more ICE friendly
        logger.debug("Created container with container URL: " + containerUri)
        
    @classmethod
    def fromJson(cls, dataContainerJson):
        d = json.loads(dataContainerJson)
        dc = SmDataContainer(d["agentId"],
                             d["agentUri"],
                             d["containerUri"])
        return dc
    
    def toJson(self):
        d = {"agentId": self.agentId,
             "agentUri": self.agentUri,
             "containerUri": urllib.parse.urlunparse(self.containerUri)
             }
        return dumpsPretty(d)
        
    def getAgentUri(self):
        return self.agentUri
    
    def getProtocol(self):
        return self.containerUri.scheme
    
    def getPort(self):
        return self.containerUri.port
    
    def getHost(self):
        return self.containerUri.hostname
    
    def getPath(self):
        return self.containerUri.path
    
    def getUsername(self):
        return self.containerUri.username
    
    def getParam(self, name):
        if name not in self.paramDict:
            return None
        paramList = self.paramDict[name]
        if len(paramList) == 0:
            return None
        return urllib.parse.unquote(paramList[0]) 
    
    def __str__(self):
        return "dataCon:" + self.agentId + "@" + self.agentUri + \
            "-" + urllib.parse.urlunparse(self.containerUri)
               
    def __eq__(self, other):
        if (self.agentId != other.agentId):
            return False
        if (self.containerUri != other.containerUri):
            return False
        if (self.agentUri != other.agentUri):
            return False
        return True
    
    def __hash__(self):
        return hash(self.agentId) + hash(self.agentUri) + hash(self.containerUri)
        

class SmDataAccessDescriptor(object):
    '''
    Class representing an object (file or folder) on a filesystem
    somewhere in the world.
    Note that the path component can a string or a list of strings to 
    support the selection of multiple files for a copy (or possibly 
    another tool)
    '''

    def __init__(self, dataContainer, pathOrPathList, autoDelete=False):
        '''
        Constructor
        '''
        self.dataContainer = dataContainer
        self.path = pathOrPathList
        self.autoDelete = autoDelete
        self.modifier = ""

    def hasSinglePath(self):
        '''
        Returns true if the path element is a simple string (not a list) 
        '''
        return isinstance(self.path, str)
    
    def _raiseExeceptionIfPathList(self):
        '''
         Throws an exception if the path is not a single value
         '''
        if not self.hasSinglePath():
            raise Exception("Data Access Description " + str(self) +
                            " has a list of paths but this call expects one path only")

    def makeDadSinglePathList(self):
        '''
        Creates and returns a list of DADs which each refer to a single 
        path
        '''
        if self.hasSinglePath():
            return [self]
        else:
            return_list = []
            for path in self.path:
                return_list.append(SmDataAccessDescriptor(self.dataContainer,
                                                          path, 
                                                          self.autoDelete))
            return return_list
               
    def getAbsPath(self):
        '''
        Gets the absolute path for the item referred to by this DAD
        '''
        self._raiseExeceptionIfPathList()
        return os.path.join(self.dataContainer.getPath(), self.path)
        
    def getPathWithModifier(self):
        self._raiseExeceptionIfPathList()
        path = os.path.join(self.dataContainer.getPath(), self.path)
        if path.endswith("/"):
            path = os.path.dirname(path)
        
        if self.modifier == "parent":
            return os.path.dirname(path)
        if self.modifier == "filename":
            return os.path.basename(path)
        return path
    
    def isLocal(self):
        return self.dataContainer.agentId == smConstGetMyAgentId()
    
    def isFile(self):
        if not self.hasSinglePath():
            return False
        
        return not self.path.endswith("/")
    
    def getProtocol(self):
        if self.dataContainer.agentId != smConstGetMyAgentId():
            return SM_DATA_CONTAINER_PROTOCOL_ICE
        else:
            return self.dataContainer.getProtocol()  
          
    def __str__(self):
        return "[" + str(self.dataContainer) + ",path=" + str(self.path) + "]"

    @classmethod
    def makeFromJson(cls, jsonString):
        d = json.loads(jsonString)
        return SmDataAccessDescriptor.makeFromDict(d)    
    
    @classmethod
    def makeFromDict(cls, d):
        dc = SmDataContainer(d["agentId"],
                             d["agentUri"],
                             d["containerUri"])
        dad = SmDataAccessDescriptor(dc, d["path"])
        dad.modifier = d["modifier"]  
        return dad      
    
    def toJson(self):
        return dumpsPretty(self.toDict())

    def toDict(self):
        d = {"agentId": self.dataContainer.agentId,
             "agentUri": self.dataContainer.agentUri,
             "containerUri": urllib.parse.urlunparse(self.dataContainer.containerUri),
             "modifier": self.modifier,
             "path": self.path}
        return d

    def join(self, path):
        self._raiseExeceptionIfPathList()
        self.path = os.path.join(self.path, path)
 
 
def smFileEntryCreate(name, size, mtime, isDir, mode, fileUid, fileGid, processUidGidDict):
    s = ""
    if fileUid == processUidGidDict["uid"]:
        if mode & stat.S_IRUSR:
            s += "r"
        if mode & stat.S_IWUSR:
            s += "w"
        if mode & stat.S_IXUSR:
            s += "x"
    elif fileGid in processUidGidDict["gid_list"]:
        if mode & stat.S_IRGRP:
            s += "r"
        if mode & stat.S_IWGRP:
            s += "w"
        if mode & stat.S_IXGRP:
            s += "x"
    else:
        if mode & stat.S_IROTH:
            s += "r"
        if mode & stat.S_IWOTH:
            s += "w"
        if mode & stat.S_IXOTH:
            s += "x"
    return {"name": name, "size": size, "mtime": mtime, "isDir": isDir,
            "mode": s}
