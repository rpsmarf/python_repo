describe( 'Community management module (/communityinfo)', function() {
	beforeEach (module('rpsmarf.config'));
	
	beforeEach(module('rpsmarf.communityinfo'));
	beforeEach(module('ui.router'));
	beforeEach(module('restangular'));
	beforeEach(module('rpsmarf.task.create.service'));
	beforeEach(module('rpsmarf.user.service'));
	
	beforeEach(module('taskprogress/taskprogress.tpl.html'));
	//$scope, $modalInstance, taskObj, $interval,TASK_STATE_ENUM, modalTitle, interactive, $modal, TASK_STATE_FINISHED, TASK_STATE_RUNNING
	var $scope, $controller, SmTaskCreate, $state, $interval, $modalInstance, $modal, smarfurl, taskObject, $httpBackend, responseObj, prefix = 'src/app/communityinfo/';
	var SmUser, user, $event = {stopPropagation: function(){}}, $stateParams, empty = {objects:[]}, communityInfoRespObj;
	//init, starting, prep, running, cleanup and finished
	

	beforeEach(  inject(function(_$controller_, $rootScope, _$interval_, Restangular, SMARF_URL, _$modal_, _SmTaskCreate_, _$state_, _$httpBackend_, _SmUser_, DEBUG, _$stateParams_){
		smarfurl = SMARF_URL+'/scs/';
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
		SmUser = _SmUser_;
		user = SmUser.getObject();

		$stateParams = _$stateParams_;
		$stateParams.communityNameKey = 'bearings';
		user.id = 1;
		user.community = {};
		user.community.name_key = 'bearings';
		user.community.id = 2;
		
		SmTaskCreate = _SmTaskCreate_;
		$interval = _$interval_;
		$httpBackend = _$httpBackend_;
		$scope = $rootScope.$new();
		$state = {go:function(a,b){}};

		responseObj = JSON.parse(__html__['src/app/communities/communities.spec.json']);
		communityInfoRespObj = JSON.parse(__html__[prefix+'communityinfo.spec.json']);

		$httpBackend.when('GET',smarfurl+'community/?name_key=bearings').respond(200, {objects: [angular.copy(responseObj.objects[1])]});
		$httpBackend.when('GET',smarfurl+'user/1/').respond(200,responseObj.user);
		$httpBackend.when('GET',smarfurl+'faq/?target=/scs/community/2/').respond(200, empty);
		$httpBackend.when('GET',smarfurl+'task_type/?communityFilter=bearings&uiVisible=true').respond(200, {meta:{total_count:1},objects:[communityInfoRespObj.objects[0]]});
		$httpBackend.when('GET',smarfurl+'resource/?communityFilter=bearings').respond(200, communityInfoRespObj.resources);
		$httpBackend.when('GET',smarfurl+'news_item/?community=2&limit=10&order_by=-release_date_time').respond(200, responseObj.news_item);
		$httpBackend.when('GET',smarfurl+'user_setting/?community__name_key=bearings').respond({meta: {total_count:21}});
		$httpBackend.when('GET',smarfurl+"community/").respond(200,responseObj.communities);
		$httpBackend.when('GET',smarfurl+'news_item/').respond(200,responseObj.news_items);
		$httpBackend.when('GET',smarfurl+'user_setting/1/').respond(200,responseObj.owner_settings);
		$httpBackend.when('GET',smarfurl+'user/1/').respond(200,responseObj.user);
/*
var communityresources = Restangular.one('resource/?communityFilter=' + $obj.name_key +'&limit=1');
var communitytasktypes = Restangular.one('task_type/?communityFilter=' + $obj.name_key + '&uiVisible=true&limit=1');
var communityusers = Restangular.one('user_setting/?community__id=' + $obj.id + '&limit=1');
*/
		$modal = _$modal_;
		$controller = _$controller_;
		$controller('CommunityinfoCtrl', {$scope: $scope, Restangular:Restangular,$state:$state , SmTaskCreate:SmTaskCreate, SmUser:SmUser, $modal: $modal, $stateParams: $stateParams });
		$httpBackend.flush();

	})) ;

	it('should get the information for the community', inject(function(){

		expect($scope.community.toolcount).toBe(1);
		expect($scope.community.resourcecount).toBe(1);
		expect($scope.community.memberscount).toBe(21);
		expect($scope.community.ownerfirstname).toBe('Henry');


	}));

	describe('edit community module', function(){

		var $editCommunityController, $editCommunityScope, community, $modalInstance;
		beforeEach(inject(function($controller, $rootScope){
			$editCommunityScope = $rootScope.$new();
			community = angular.copy(responseObj.objects[1]);
			$modalInstance = {close: function(){}};
			$editCommunityController = $controller('EditCommunityCtrl',{$scope: $editCommunityScope, community: community, $modalInstance: $modalInstance});
		}));

		it('should perform the proper actions when a user attempts to edit a community', inject(function(){

			//{imageUrl: $scope.community.imageUrl, description: $scope.community.description, shortDescription: $scope.community.shortDescription, name: $scope.community.name};
			expect($editCommunityScope.community).toEqual(community);
			var formObj = {$valid: false};
			
			spyOn($modalInstance, 'close');
			$editCommunityScope.save(formObj);
			
			expect(formObj.$submitted).toBe(true);


			formObj.$valid = true;
			formObj.$submitted = false;
			$editCommunityScope.community.name = 'test community';
			$editCommunityScope.community.imageUrl = 'http://www.someurl.com';
			$editCommunityScope.community.description = 'this is a test community';
			$editCommunityScope.community.shortDescription = 'test';
			var postObj = {name: 'test community', imageUrl: 'http://www.someurl.com', description: 'this is a test community', shortDescription: 'test'};
			
			$httpBackend.expect('PATCH', smarfurl+'community/2/',postObj).respond(200);
			$editCommunityScope.save(formObj);
			$httpBackend.flush();
			expect($editCommunityScope.showMessage).toBe(true);
			
			$editCommunityScope.close();
			
			expect($modalInstance.close).toHaveBeenCalledWith({saved: true, object: $editCommunityScope.community});

		}));
	});

	describe('assignation module', function(){
		//'$scope', '$modalInstance', 'Restangular', '$modal', 'object', 'SmUser','userOwnsCommunity','community','$timeout'
		var $assignToCommunityController, $assignToCommunityScope, userOwnsCommunity = true, community, object={type:'resource', assigned:false};
		beforeEach(inject(function($controller, $rootScope){
			community = angular.copy(responseObj.objects[1]);
			$assignToCommunityScope = $rootScope.$new();

			$httpBackend.expect('GET', smarfurl+'tag/?tag_name=community:bearings').respond(200, communityInfoRespObj.tags);
			$httpBackend.expect('GET', smarfurl+'resource/?limit=0&personalFolder=false').respond(200, communityInfoRespObj.assign_resources);
			$httpBackend.expect('GET', smarfurl+'resource/?owner__id=1&personalFolder=true').respond(200,communityInfoRespObj.personal_folders);
			$assignToCommunityController = $controller('AssignToCommunityCtrl',{$scope: $assignToCommunityScope, community: community, object: object, $modalInstance: {}, userOwnsCommunity: userOwnsCommunity});
			$httpBackend.flush();

		}));

		it('should make the proper calls when performing assignation', inject(function(){
			expect(angular.copy($assignToCommunityScope.objects[0])).toEqual(communityInfoRespObj.personal_folders.objects[0]);
			$assignToCommunityScope.selected = $assignToCommunityScope.objects[2];
			$httpBackend.expect('GET', smarfurl+'resource_tag/?resource__id=7&tag__tag_name=community:bearings').respond(200, {meta:{total_count:0}});
			$httpBackend.expect('POST', smarfurl+'resource_tag/',{tag:'/scs/tag/1/',resource: '/scs/resource/7/'}).respond(200);
			$assignToCommunityScope.assign();
			$httpBackend.flush();
			expect($assignToCommunityScope.showMessage).toBe(true);
			

		}));
	});
});

