import sys
import json
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath, scsGetToJson
from scripts.scs_obj_create import scsCreateObjectPath

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

root_path = scsGetObjectPath("user", "username=rpsmarf")


community_path = scsCreateObjectPath("community", 
                                     {"name": "Everything",
                                      "name_key": "all_resources",
                                      "description": "This community represents all tools",
                                      "leader": root_path,
                                      "admin": root_path,
                                      })

scsCreateObjectPath("news_item", 
                    {"owner": root_path,
                     "headline": "System Upgrade on June 30, 2015",
                     "body": "The system will be upgraded on that day.  Please expect periods of service outage that day.",
                     "community": community_path,
                     })

bearing_community_tag_path = scsCreateObjectPath("tag", 
                                                {"tag_name": "community:bearings"})
bearing_community_path = scsCreateObjectPath("community", 
                                     {"name": "Bearing Monitoring",
                                      "name_key": "bearings",
                                      "description": "This community contains all "
                                      "resources associated with bearing monitoring.",
                                      "owner": root_path,
                                      "tag": bearing_community_tag_path,
                                      })

bridge_community_tag_path = scsCreateObjectPath("tag", 
                                                {"tag_name": "community:bridge_mon"})
bridge_community_path = scsCreateObjectPath("community", 
                                     {"name": "Bridge Monitoring",
                                      "name_key": "bridge_mon",
                                      "description": "This community contains all "
                                      "resources associated with bridge monitoring.",
                                      "owner": root_path,
                                      "tag": bridge_community_tag_path,
                                      })

scsCreateObjectPath("news_item", 
                    {"owner": root_path,
                     "headline": "Amir away from May to August",
                     "body": "Amir Tehranian is away from May to August 2015 inclusive.  For SPPLASH support "
                     "please contain Serge Dejardins.",
                     "community": bridge_community_path,
                     })

scsCreateObjectPath("news_item", 
                    {"owner": root_path,
                     "headline": "Serge visiting Carleton from June 9th to June 15th",
                     "body": "Serge Desjardins is visiting Carleton to discuss the evolution  "
                     "of the SPPLASH tool.",
                     "community": bridge_community_path,
                     })


scsCreateObjectPath("user_setting",
                    {"user": root_path,
                     "community": community_path})

cloud_agent_data_path = scsCreateObjectPath("agent", 
                                       {"guid": "LOCAL_AGENT",
                                        "name": "cloud_file_agent",
                                        "description": "This remote agent holds users' cloud files",
                                        "owner": root_path,
                                        "last_change": '2013-01-01',
                                        "agentUrl": "ice://demo.rpsmarf.ca:9001"
                                        })

cloud_container_path = scsCreateObjectPath("container", {"name": "cloud_file_container", 
                                                         "name_key": "user_storage",
                                                         "containerUrl": "local://localhost/tmp/cloud_files/",
                                                         "agent": cloud_agent_data_path,
                                                         "description": "This is the local agent.",
                                                         "status": "up"})

rt_storage_path = scsCreateObjectPath("resource_type", 
                                      {"name": "Cloud Files Resource Type", 
                                       "name_key": "generic_storage",
                                       "nature": "data",
                                       "description": "This is a cloud file folder for end-user data storage"})

user_a = scsCreateObjectPath("user", {"username": "a",
                                      "email": "a@a.a",
                                      "password": "a",
                                      "first_name": "Albert",
                                      "last_name": "Allen"})

user_b = scsCreateObjectPath("user", {"username": "b",
                                      "email": "b@b.b",
                                      "password": "b",
                                      "first_name": "Bjorn",
                                      "last_name": "Borg"})

user_c = scsCreateObjectPath("user", {"username": "c",
                                      "email": "c@c.c",
                                      "password": "c",
                                      "first_name": "Charlie",
                                      "last_name": "Chaplin"})

carleton_agent_path = scsCreateObjectPath("agent", 
                                          {"guid": "CARLETON",
                                           "name": "carleton_agent",
                                           "description": 
                                           "This remote agent provides access to data held at Carleton",
                                           "owner": root_path,
                                           "last_change": '2013-01-01',
                                           "agentUrl": "ice://majumdar-10.sce.carleton.ca:9111"
                                           })
    
carleton_container_path = scsCreateObjectPath("container", {"name": "mock_data_container", 
                                                            "name_key": "carleton_demos_data",
                                                            "containerUrl": "local://localhost/opt/smarf-demos/demos/data/",
                                                            "description": 
                                                            "This container provides access to data held at Carleton",
                                                            "agent": carleton_agent_path,
                                                            "status": "up"})

carleton_ssh_container_path = scsCreateObjectPath("container", {"name": "Carleton Windows Running", 
                                                                "name_key": "carleton_win_runner", 
                                                                "description": 
                                                                "This container provides access to node " +
                                                                "to run SPPLASH and WSAT",
                                                                "containerUrl": "ssh://win1.rpsmarf.ca:22/cygdrive/c/temp",
                                                                "agent": carleton_agent_path,
                                                                "status": "up"})

carleton_maj06_ssh_container_path = scsCreateObjectPath("container", {"name": "Carleton SPPLASH Node", 
                                                                      "name_key": "carleton_spplash_runner",
                                                                      "description": 
                                                                      "This container provides access to node " +
                                                                      "to run SPPLASH and WSAT",
                                                                      "containerUrl": 
                                                                      "ssh://majumdar-06.sce.carleton.ca:22/cygdrive/c/temp",
                                                                      "agent": carleton_agent_path,
                                                                      "status": "up"})

dair_ab_agent_path = scsCreateObjectPath("agent", 
                                         {"guid": "DAIR_AB",
                                          "name": "dair_ab_agent",
                                          "description": 
                                          "This remote agent provides access to data held at the DAIR Albert node",
                                          "owner": root_path,
                                          "last_change": '2013-01-01',
                                          "agentUrl": "ice://testab1.rpsmarf.ca:9001"
                                          })

dair_ab_container_path = scsCreateObjectPath("container", 
                                             {"name": "mock_ab_container", 
                                              "name_key": "testab1_demos_data",
                                              "containerUrl": "local://localhost/opt/smarf-demos/demos/data/",
                                              "description": "The container to hold data in Alberta",
                                              "agent": dair_ab_agent_path,
                                              "status": "up"})

res_path = scsCreateObjectPath("resource", {"name": "Common Cloud Storage",
                                            "name_key": "demo_cloud_files",
                                            "resource_type": rt_storage_path,
                                            "owner": root_path,
                                            "container": cloud_container_path,
                                            "parametersJson": json.dumps({"folder": "files/"}),
                                            "description": "This is your cloud files storage."
                                            }
                               )
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_a + "&perm=rw")
scsGetToJson(res_path + "setperm/?action=assign&user=" + user_b + "&perm=r")

# Setting up common task types

TASK_TYPE_ZIP_DICT = {
    "args": [ 
        {"type": "string", 
         "name": "Argument", 
         "value": "zip"},
        {"type": "string", 
         "name": "Argument", 
         "value": "-rdc"},
        {"type": "resource", 
         "name": "ZIP file to create", 
         "description": "The name of the zip file which is created by the zip task",
         "pathType": "file", 
         "input_id": 1,
         "direction": "output", 
         "role": "zip"},
        {"type": "resource", 
         "name": "Folder to zip up", 
         "description": "The name of the folder whose contents are zipped into a file",
         "pathType": "folder", 
         "input_id": 2, 
         "direction": "input", 
         "role": "zip", 
         "modifier": "filename"},
        ],
    "computeResourceRole": "zip",
    "skipRunPermCheck": True,
    "workingDir": {"type": "resource", "input_id": 2, "role": "zip", "modifier": "parent"}
    }

tt_zip = scsCreateObjectPath("task_type", {"name": "zip", 
                                           "code_module": "smcommon.task_runners.task_runner_zip", 
                                           "code_classname": "SmTaskRunnerZip", 
                                           "description": "Task to zip up files",
                                           "uiVisible": False,
                                           "owner": root_path,
                                           "configurationJson": json.dumps(TASK_TYPE_ZIP_DICT)})

scsCreateObjectPath("resource_type_task_type", {"role": "zip", "task_type": tt_zip, 
                                                "resource_type": rt_storage_path,
                                                "name": "Generic Storage for Zip",
                                                "name_key": "generic_storage_rttt",
                                                "nature": "data",
                                                "description": 
                                                "This links the zip task type to all storage resources"})


TASK_TYPE_UNZIP_DICT = {
                            "args": [ 
                                     {"type": "string", 
                                      "name": "Argument", 
                                      "value": "unzip"},
                                     {"type": "string", 
                                      "name": "Argument", 
                                      "value": "-d"},
                                     {"type": "resource", 
                                      "name": "Folder to unzip into", 
                                      "description": "This is the folder into which the data will be unzipped",
                                      "pathType": "folder", 
                                      "input_id": 2, 
                                      "direction": "output", 
                                      "role": "unzip"},
                                     {"type": "resource", 
                                      "name": "File to unzip", 
                                      "description": "This is the name of the file to unzip",
                                      "pathType": "file", 
                                      "input_id": 1, 
                                      "direction": "input", 
                                      "role": "unzip"},
                                     ],
                            "computeResourceRole": "unzip",
                            "skipRunPermCheck": True,
                       }

tt_unzip = scsCreateObjectPath("task_type", {"name": "unzip", 
                                             "code_module": "smcommon.task_runners.task_runner_shell", 
                                             "code_classname": "SmTaskRunnerShell", 
                                             "description": "Task to unzip data",
                                             "uiVisible": False,
                                             "owner": root_path,
                                             "configurationJson": json.dumps(TASK_TYPE_UNZIP_DICT)})

scsCreateObjectPath("resource_type_task_type", {"role": "unzip", "task_type": tt_unzip, 
                                                "resource_type": rt_storage_path,
                                                "nature": "data",
                                                "name": "Unzip generic storage",
                                                "name_key": "unzip_generic_storage",
                                                "description": "This links the unzip task type to all unzip resources"})


TASK_TYPE_DELETE_DICT = {
                            "args": [
                                     {"type": "resource", 
                                      "name": "Folder to delete", 
                                      "description": "This is the name of the folder to delete",
                                      "input_id": 1, 
                                      "direction": "input", 
                                      "role": "storage",
                                      "pathType": "folder"},
                                     ],
                            "computeResourceRole": "storage",
                            "skipRunPermCheck": True,
                       }
tt_delete = scsCreateObjectPath("task_type", {"name": "Recursive Delete",
                                             "name_key": "delete_recusive",
                                             "code_module": "smcommon.task_runners.task_runner_delete",
                                             "code_classname": "SmTaskRunnerDelete",
                                             "description": "Task to delete data asynchronously",
                                             "uiVisible": False,
                                             "owner": root_path,
                                             "configurationJson": json.dumps(TASK_TYPE_DELETE_DICT)})

scsCreateObjectPath("resource_type_task_type", {"role": "storage", "task_type": tt_delete,
                                                "resource_type": rt_storage_path,
                                                "nature": "data",
                                                "name": "Delete data",
                                                "description": "This links the delete task type to all storage resources"})


print("Core DAIR demo objects setup.")
