describe( 'folder and file view/navigation for a resource', function() {
	beforeEach( module( 'rpsmarf.foldernav') );
	beforeEach(module('foldernav/foldernav.tpl.html'));
	var $scope, $element, entries, $isolateScope;

	var homeSubEntries = [{basename:'documents/', name:'/home/documents/', isDir:true},{basename:'pictures/', name:'/home/pictures/',isDir:true}, {basename:'myfile', name:'/home/documents/myfile',isDir:false}],
		documentsSubEntries = [{basename:'report1.doc', name:'/home/documents/report1.doc',isDir:false},{basename:'paper.pdf', name:'/home/documents/paper.pdf',isDir:false}],
		dataSubEntries = [{basename:'bridge.dat', name:'/home/data/bridge.dat',isDir:false},{basename:'machinery.dat', name:'/home/data/machinery.dat',isDir:false}, {basename:'data.dat', name:'/home/data/data.dat',isDir:false}];

	beforeEach(inject(function($rootScope, $compile){
		entries = [{basename:'home/', name:'/home/',isDir:true},{basename:'documents/', name:'/home/documents/',isDir:true},{basename:'data/', name:'/data/',isDir:true},{basename:'file1.txt/', name:'/file1.txt',isDir:false}];
		$scope = $rootScope.$new();
		$element = angular.element(' <foldernav resource="resource" entries="entries" ls="entryLs(resource,entry)" selectentry="selectEntry(entry)" selectall="allEntriesSelected[$index]" selectedentries="selectedEntries" smondrop="drop(srcEntry, srcResource, dstResource, dstEntry)" data="test"></foldernav>');

		
		$scope.resource = {};
		$scope.entries = entries;
		$scope.selectedEntries = {};
		$scope.entryLs = function(resource, entry){
			switch(entry.name){
				case '/home/':
					entry.entries = homeSubEntries;
					break;
				case '/home/documents/':
					entry.entries = documentsSubEntries;
					break;
				case '/data/':
					entry.entries = dataSubEntries;
					break;
				default:
					entry.entries = [];
					return;
			}
			
		};
		
		$element = $compile($element)($scope);



		$scope.$digest();

		
		$isolateScope = $element.isolateScope();

		
		


	}));

	it( 'should return the correct type and amount of entries', function() {
		var target =  $element.find('.entry-container');
		expect(target.length).toEqual(entries.length);
		target = $element.find('i.fa-folder-open-o');
		expect(target.length).toEqual(3); //3 folder entries
		target = $element.find('i.fa-file-text-o');
		expect(target.length).toEqual(1);
		target = $element.find('div.checkbox-selector-container ul > li');
		expect(target.length).toEqual(entries.length);
		

	});

	it('should list sub-entries when a folder is expanded and hide them when closed', function() {
		
		var target = $element.find('.entry-container:eq(0) > i.fa-caret-right');
		target.click();
		target = $element.find('.entry-container');
		expect(target.length).toEqual(entries.length + homeSubEntries.length);


		target = $element.find('div.checkbox-selector-container ul > li');
		expect(target.length).toEqual(entries.length + homeSubEntries.length);
		expect($isolateScope.linearEntries.length).toEqual(entries.length + homeSubEntries.length);

		target = $element.find('.entry-container:eq(0) > i.fa-caret-down');
		target.click();

		target = $element.find('div.checkbox-selector-container ul > li');
		expect(target.length).toEqual(entries.length);
		expect($isolateScope.linearEntries.length).toEqual(entries.length);

	});

	it('should highlight and set selected when the entry or checkbox is clicked', function() {
		
		var numSelected;
		var i;
		var sum;
		var obj = {};
		//Element click
		//Select first entry
		var target = $element.find('.entry-container:eq(0)');
		target.click();
		target = $element.find('.entry-container.active');
		expect(target.length).toEqual(1);
		expect($isolateScope.linearEntries[0].selected).toBe(true);
		target = $element.find('div.checkbox-selector-container ul > li input:checked');
		expect(target.length).toEqual(1);
		obj[entries[0].$$hashKey] = entries[0];
		expect($scope.selectedEntries).toEqual(obj);
		expect($isolateScope.linearEntries[0].selected).toBe(true);

		//Unselect first entry
		target = $element.find('.entry-container:eq(0)');
		target.click();
		target = $element.find('.entry-container.active');
		expect(target.length).toEqual(0);
		expect($isolateScope.linearEntries[0].selected).toBe(false);
		target = $element.find('div.checkbox-selector-container ul > li input:checked');
		expect(target.length).toEqual(0);
		expect($scope.selectedEntries).toEqual({});
		expect($isolateScope.linearEntries[0].selected).toBe(false);

		//Expand first entry and select first entry
		target = $element.find('.entry-container:eq(0) > i.fa-caret-right');
		target.click();
		target = $element.find('.entry-container:eq(0)');
		target.click();
		target = $element.find('.entry-container.active');
		expect(target.length).toEqual(homeSubEntries.length+1);
		expect($scope.selectedEntries).toEqual(obj);

		numSelected=0;
		for(i=$isolateScope.linearEntries.length-1; i>=0; i-- ){
			if($isolateScope.linearEntries[i] && $isolateScope.linearEntries[i].selected){
				++numSelected;
			}
		}
		expect(numSelected).toEqual(homeSubEntries.length+1);

		target = $element.find('div.checkbox-selector-container ul > li input:checked');
		expect(target.length).toEqual(homeSubEntries.length+1);
		expect($isolateScope.linearEntries[0].selected && $isolateScope.linearEntries[3].selected).toBe(true);

		//Unselect expanded first entry
		target = $element.find('.entry-container:eq(0)');
		target.click();

		target = $element.find('.entry-container.active');
		expect(target.length).toEqual(0);

		numSelected=0;
		
		for(i=$isolateScope.linearEntries.length-1; i>=0; i-- ){
			if($isolateScope.linearEntries[i] && $isolateScope.linearEntries[i].selected){
				++numSelected;
			}
		}

		expect(numSelected).toEqual(0);

		target = $element.find('div.checkbox-selector-container ul > li input:checked');
		expect(target.length).toEqual(0);
		expect($scope.selectedEntries).toEqual({});
		expect($isolateScope.linearEntries[0].selected && $isolateScope.linearEntries[3].selected).toBe(false);

		//3 levels using checkboxes, select first root entry
		target = $element.find('ul.root-entry-container > li:eq(0) ul li:eq(0) i.fa-caret-right');
		target.click();
		$scope.$digest();
		target = $element.find('div.checkbox-selector-container ul > li:eq(0) input');
		target.click();

		target = $element.find('.entry-container.active');
		sum = homeSubEntries.length+documentsSubEntries.length+ 1;
		expect(target.length).toEqual(sum);

		numSelected=0;
		for(i=$isolateScope.linearEntries.length-1; i>=0; i-- ){
			if($isolateScope.linearEntries[i] && $isolateScope.linearEntries[i].selected){
				++numSelected;
			}
		}
		expect(numSelected).toEqual(sum);

		target = $element.find('div.checkbox-selector-container ul > li input:checked');
		expect(target.length).toEqual(sum);
		expect($scope.selectedEntries).toEqual(obj);
		expect($isolateScope.linearEntries[0].selected && $isolateScope.linearEntries[5].selected).toBe(true);

		//select a subentry within the expanded (and selected) first entry
		target = $element.find('ul.root-entry-container > li:eq(0) ul li:eq(0) ul li:eq(1) .entry-container');
		
		expect(target.find('p').html()).toEqual(documentsSubEntries[1].basename);
		target.click();

		target = $element.find('.entry-container.active');
		sum =1;
		expect(target.length).toEqual(sum);

		numSelected=0;
		for(i=$isolateScope.linearEntries.length-1; i>=0; i-- ){
			if($isolateScope.linearEntries[i] && $isolateScope.linearEntries[i].selected){
				++numSelected;
			}
		}
		expect(numSelected).toEqual(sum);
		target = $element.find('div.checkbox-selector-container ul > li input:checked');
		expect(target.length).toEqual(sum);
		obj = {};
		obj[$scope.entries[0].entries[0].entries[1].$$hashKey] = $scope.entries[0].entries[0].entries[1];
		expect($scope.selectedEntries).toEqual(obj);
		expect($isolateScope.linearEntries[3].selected).toBe(true);

		//select parent (root) entry of the above subentry, selected entry should only be the parent
		target = $element.find('div.checkbox-selector-container ul > li:eq(0) input');
		target.click();
		target = $element.find('.entry-container.active');
		sum =homeSubEntries.length+documentsSubEntries.length+1;
		expect(target.length).toEqual(sum);

		numSelected=0;
		for(i=$isolateScope.linearEntries.length-1; i>=0; i-- ){
			if($isolateScope.linearEntries[i] && $isolateScope.linearEntries[i].selected){
				++numSelected;
			}
		}
		expect(numSelected).toEqual(sum);
		target = $element.find('div.checkbox-selector-container ul > li input:checked');
		expect(target.length).toEqual(sum);
		obj = {};
		obj[entries[0].$$hashKey] = entries[0];
		expect($scope.selectedEntries).toEqual(obj);
		expect($isolateScope.linearEntries[0].selected).toBe(true);

		//select the third entry (add to original selection) in the root entry container (data)
		target = $element.find('div.checkbox-selector-container ul > li:eq('+(sum+1)+') input');
		target.click();
		target = $element.find('.entry-container.active');
		sum =sum+1;
		expect(target.length).toEqual(sum);

		numSelected=0;
		for(i=$isolateScope.linearEntries.length-1; i>=0; i-- ){
			if($isolateScope.linearEntries[i] && $isolateScope.linearEntries[i].selected){
				++numSelected;
			}
		}
		expect(numSelected).toEqual(sum);
		target = $element.find('div.checkbox-selector-container ul > li input:checked');
		expect(target.length).toEqual(sum);
		//Add new object property
		obj[entries[2].$$hashKey] = entries[2];
		expect($scope.selectedEntries).toEqual(obj);
		expect($isolateScope.linearEntries[0].selected && $isolateScope.linearEntries[7].selected).toBe(true);
		//select first sub entry of documentssubentries (report1.doc) and to selected, should remove
		//the root entry (home/) beacause it is this entry's parent
		target = $element.find('div.checkbox-selector-container ul > li:eq(2) input');
		target.click();
		sum = 2; //data entry and just clicked entry should be the only selected entries
		target = $element.find('.entry-container.active');
		expect(target.length).toEqual(sum);
		numSelected=0;
		for(i=$isolateScope.linearEntries.length-1; i>=0; i-- ){
			if($isolateScope.linearEntries[i] && $isolateScope.linearEntries[i].selected){
				++numSelected;
			}
		}
		//console.log($isolateScope.linearEntries);
		expect(numSelected).toEqual(sum);
		target = $element.find('div.checkbox-selector-container ul > li input:checked');
		expect(target.length).toEqual(sum);

		obj = {};
		obj[entries[2].$$hashKey] = entries[2];
		obj[documentsSubEntries[0].$$hashKey] = documentsSubEntries[0];
		expect($scope.selectedEntries).toEqual(obj);
		expect($isolateScope.linearEntries[2].selected && $isolateScope.linearEntries[7].selected).toBe(true);
		//console.log($isolateScope.linearEntries);

	});

		

	
  
});

