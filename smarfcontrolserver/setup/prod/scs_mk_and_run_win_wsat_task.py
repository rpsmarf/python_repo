import json
import sys
from scripts.scs_task_start import scsTaskStartByPath
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath
from scripts.scs_obj_create import scsCreateObjectPath

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

taskParams = {
             }

tt_path = scsGetObjectPath("task_type", "name_key=win_wsat")
root_path = scsGetObjectPath("user", "name=rpsmarf")
task_path = scsCreateObjectPath("task", {"name": "Windows SPPLASH", 
                                         "task_type": tt_path,
                                         "owner": root_path,
                                         "parametersJson": json.dumps(taskParams),
                                         }
                                )

resource_type_task_type_compute_path = scsGetObjectPath("resource_type_task_type", "name_key=wsat_compute")

compute_resource_path = scsGetObjectPath("resource", "name_key=maj06_win_wsat_runner")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": compute_resource_path,
                                      "resource_type_task_type": resource_type_task_type_compute_path,
                                      }
                    )

print("Task " + task_path + " to run the WSAT tool setup, starting task...")
scsTaskStartByPath(task_path, verbose=False)

