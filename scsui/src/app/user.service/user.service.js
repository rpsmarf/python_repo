angular.module( 'rpsmarf.user.service', [
 'rpsmarf.config',
 'rpsmarf.constants'
])
.service('SmUser', [ 'DEBUG', 'USER_STATE','DEFAULT_COMMUNITY_ICON_URL','DEFAULT_USER_ICON_URL', function(DEBUG, USER_STATE, DEFAULT_COMMUNITY_ICON_URL, DEFAULT_USER_ICON_URL){

	var user = {id: null, email: null, firstname:null,lastname:null, state: USER_STATE.browse, community: null, apikey: null,
				csrf: null, imageUrl: DEFAULT_USER_ICON_URL, uiSettings:{}};
	var userValid = false;
	if(DEBUG){
		user.id= 1, user.email='support@rpsmarf.ca', user.firstname='Henry',user.lastname='Tory', user.imageUrl=DEFAULT_USER_ICON_URL,
		user.community= {
            "owner": "/scs/user/1/",
            "description": "This community represents all tools",
            "gid": 2,
            "id": 1,
            "leader": "/scs/user/1/",
            "name": "Everything",
            "name_key": "all_resources",
            "resource_uri": "/scs/community/1/",
            "imageUrl": DEFAULT_COMMUNITY_ICON_URL
        };
		
		userValid = true;
				
	}
	
	
	var getObject = function(){
		return user;
	};

	var getProperty = function(propertyName){
		if(user[propertyName] === undefined){
			return false;
		}else{
			return user[propertyName];
		}
	};
	var setProperty = function(propertyName, value){

		if(user[propertyName] === undefined){
			return false;
		}else{
			if(propertyName === 'id'){
				userValid = true;
			}
			user[propertyName] = value;
			return true;
		}
	};

	var invalidate = function(){
		for(var key in user){
			user[key] = null;
		}

		userValid = false;
	};

	var isValid = function(){
		return userValid;
	};

	

	return {getProperty: getProperty, 
			setProperty: setProperty, 
			invalidate:invalidate, 
			getObject: getObject,
			isValid: isValid
			};
}])
;