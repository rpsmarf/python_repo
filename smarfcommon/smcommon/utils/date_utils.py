'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 25, 2014

    @author: rpsmarf
'''
import datetime
import pytz
import dateutil.parser
import time


def sm_date_now():
    return datetime.datetime.now(pytz.utc)  # @UndefinedVariable


def sm_date_far_future():
    return smMakeDateFromString("22000101T010101Z")


def sm_date_to_unixtime_sec(dt):
    return time.mktime(dt.timetuple())


def smContainsDateString(s):
    '''
    This method returns the offset of the string if the string passed in contains
    a date string of the form yyymmddThhmmssZ (e.g. 20150220T135911Z) or
    -1 if there is no date string.
    This is used to alter output folders when a a task is being rerun
    '''
    if s is None:
        return -1
    slen = len(s)
    for i in range(0, slen - 14):  # No point in trying strings with less than 15 char
        if (s[i:i + 8].isdigit() and (s[i + 8:i + 9] == "T") and s[i + 9:i + 15].isdigit() and (s[i + 15:i + 16] == "Z")):
            return i
    return -1


def smMakeDateStringForJson(t):
    return t.strftime("%Y-%m-%dT%H:%M:%SZ")


def smMakeDateString(t):
    return t.strftime("%Y%m%dT%H%M%SZ")


def smMakeDateFromString(s):
    return dateutil.parser.parse(s)


def smMakeNewPath(oldPath, nowString):
    '''
    Usually now is passed in from smMakeDateString(sm_date_now())
    '''
    offset = smContainsDateString(oldPath)
    if offset == -1:
        return oldPath + "_" + nowString
    else:
        return oldPath[0:offset] + nowString + oldPath[16 + offset:]
    