'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
from django.test import TestCase
from sm_controller.models import SmModelResourceType, SmModelResource, SmModelTaskType, SmModelTask,\
    SmModelContainer
from django.contrib.auth.models import User
from scripts import scsWipeAndInit
from scripts import scsCreateUser
from scripts import scsCreateResource
from scripts import scsCreateTask


class ScriptTest(TestCase):
    
    def test_wipe_and_init(self):
        scsWipeAndInit.set_testing(True)
        string_num = "0"
        int_num = 0
        resp = scsWipeAndInit.check_user_params(string_num)
        self.assertFalse(resp, "String 0 not accepted")
        resp = scsWipeAndInit.check_user_params(int_num)
        self.assertFalse(resp, "Int value 0 not accepted")
        
        string_num = "10"
        int_num = 10
        resp = scsWipeAndInit.check_user_params(string_num)
        self.assertTrue(resp, "String 10 accepted")
        resp = scsWipeAndInit.check_user_params(int_num)
        self.assertTrue(resp, "Int 10 accepted")
        
        num_to_create = 10
        scsWipeAndInit.clear_and_init(num_to_create, True)
        num_users = User.objects.count()
        self.assertEqual(num_users, num_to_create + 2)
        superuser = User.objects.get(username='rpsmarf')
        
        self.assertTrue(superuser.is_superuser, "First user is superuser")
        
        num_resourcetype = SmModelResourceType.objects.count()
        
        self.assertEqual(num_resourcetype, 0)
        
        num_tasktype = SmModelTaskType.objects.count()
        
        self.assertEqual(num_tasktype, 0) 
        
    def test_create_user(self):
        scsWipeAndInit.set_testing(True)
        start_users = User.objects.count()
        num_to_create = 0
        resp = scsCreateUser.create_users(num_to_create)
        self.assertFalse(resp)
                
        num_to_create = 2
       
        resp = scsCreateUser.create_users(num_to_create)
        
        num_users = User.objects.count()
        self.assertEqual(num_users, start_users + num_to_create)
        
    def xtest_create_resource(self):
        scsWipeAndInit.set_testing(True)
        scsWipeAndInit.clear_and_init(10)
        
        container = SmModelContainer.objects.all().iterator().__next__()
        rtype = 'fail'
        num_to_create = 0
        u = None
        
        resp = scsCreateResource.create_resources(num_to_create, rtype, u, container)
        self.assertFalse(resp, "Number to create is 0")

        num_to_create = 2
        resp = scsCreateResource.create_resources(num_to_create, rtype, u, container)
        self.assertFalse(resp, "User is None")
                        
        u = User.objects.get(username='rpsmarf')
        
        resp = scsCreateResource.create_resources(num_to_create, rtype, u, container)
        self.assertFalse(resp, "Unknown resource type")
        
        rtype = "file"
        start_resources = SmModelResource.objects.count()
        scsCreateResource.create_resources(num_to_create, rtype, u, container)
        num_resources = SmModelResource.objects.count()
        self.assertEqual(num_resources, start_resources + num_to_create)
        
    def xtest_create_task(self):
        scsWipeAndInit.set_testing(True)
        scsWipeAndInit.clear_and_init(10)
        users = User.objects.all()
        u = users.iterator().__next__()
        containers = SmModelContainer.objects.all() 
        c = containers.iterator().__next__()
        
        scsCreateResource.create_resources(2, 'file', u, c)
        scsCreateResource.create_resources(2, 'folder', u, c)
        num_to_create = 0
        
        resp = scsCreateTask.create_random_tasks(num_to_create)
        self.assertFalse(resp, "0 tasks not accepted")
        
        num_to_create = 2
        
        start_tasks = SmModelTask.objects.count()
        scsCreateTask.create_random_tasks(num_to_create)
        num_tasks = SmModelTask.objects.count()
        self.assertEqual(num_tasks, start_tasks + num_to_create)
