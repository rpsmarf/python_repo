'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
from django.conf.urls import patterns, include, url
from sm_controller.api import SmApiResource, SmApiTaskTypeNote,\
    SmApiResourceNote, SmApiCommunity, SmApiNewsItem, SmApiUserSetting,\
    SmApiScript, SmApiProperty, SmApiFavouriteTaskType, SmApiReservation, SmApiTaskTypeTag,\
    SmApiFaq, SmApiRequest, SmApiMdRepo, SmApiSchema, SmApiPropertyType,\
    SmApiPathList, SmApiExtractor, SmApiMdProperty, SmApiSchemaFull,\
    SmApiMdPropertywithType, SmApiGroup, SmApiMdExtractorType, SmApiCloud,\
    SmApiCloudAccount, SmApiCloudServer, SmApiFileSet, SmApiFileSetItem,\
    SmApiExtractorFull
from sm_controller.api import SmApiTask
from sm_controller.api import SmApiTaskType
from sm_controller.api import SmApiTaskResource
from sm_controller.api import SmApiResourceTag
from sm_controller.api import SmApiResourceType
from sm_controller.api import SmApiResourceTypeTaskType
from sm_controller.api import SmApiFavouriteResource
from sm_controller.api import SmApiAgent
from sm_controller.api import SmApiContainer
from sm_controller.api import SmApiTag
from sm_controller.api import SmApiUser

from django.contrib import admin
from sm_controller import views as sm_views
from canarie_service import views as cs_views
from canarie_service.api import SmApiCanarieServiceProperties
admin.autodiscover()

#Create resources to configure url patterns 
obj_list = []
obj_list.append(SmApiUser())
obj_list.append(SmApiGroup())
obj_list.append(SmApiResource())
obj_list.append(SmApiFavouriteResource())
obj_list.append(SmApiTask())
obj_list.append(SmApiTaskType())
obj_list.append(SmApiResourceType())
obj_list.append(SmApiAgent())
obj_list.append(SmApiContainer())
obj_list.append(SmApiTag())
obj_list.append(SmApiResourceTag())
obj_list.append(SmApiResourceTypeTaskType())
obj_list.append(SmApiTaskResource())
obj_list.append(SmApiFavouriteTaskType())
obj_list.append(SmApiTaskTypeNote())
obj_list.append(SmApiTaskTypeTag())
obj_list.append(SmApiResourceNote())
obj_list.append(SmApiCommunity())
obj_list.append(SmApiNewsItem())
obj_list.append(SmApiReservation())
obj_list.append(SmApiCanarieServiceProperties())
obj_list.append(SmApiUserSetting())
obj_list.append(SmApiScript())
obj_list.append(SmApiProperty())
obj_list.append(SmApiFaq())
obj_list.append(SmApiRequest())
obj_list.append(SmApiSchema())
obj_list.append(SmApiSchemaFull())
obj_list.append(SmApiMdRepo())
obj_list.append(SmApiPropertyType())
obj_list.append(SmApiPathList())
obj_list.append(SmApiExtractor())
obj_list.append(SmApiExtractorFull())
obj_list.append(SmApiMdExtractorType())
obj_list.append(SmApiMdProperty())
obj_list.append(SmApiMdPropertywithType())
obj_list.append(SmApiCloud())
obj_list.append(SmApiCloudAccount())
obj_list.append(SmApiCloudServer())
obj_list.append(SmApiFileSet())
obj_list.append(SmApiFileSetItem())

#Set the base urls to be used for CRUD operations on TastyPie object
base_urls = []
for obj in obj_list:
    base_urls.append(url(r'^', include(obj.urls)))
    
    
#Set the urls to be rooted under / such that resources are accessed
#directly from their resource_name such as /user/1/ or /resource/1/ etc.
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'smarfcontrolserver.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^scs/admin/', include(admin.site.urls)),
    url(r'^scs/resource/.*/upload/$', sm_views.sm_upload),
    url(r'^scs/', include(base_urls)),
    url(r'^scs/platform/info$', cs_views.info, name='info'),
    url(r'^scs/platform/stats$', cs_views.stats, name='stats'),
    url(r'^scs/platform/licence$', cs_views.licence, name='license'),
    url(r'^scs/platform/(?P<page>video.*)$', cs_views.videos),
    url(r'^scs/platform/(?P<page>.*)$', cs_views.serve_page, {"section": "platform"}),
    url(r'^scs/cloud_rpi/service/licence$', cs_views.licence, name='license'),
    url(r'^scs/cloud_rpi/service/(?P<page>.*)$', cs_views.serve_page, {"section": "cloud_rpi"}),
    url(r'^scs/cloud_rpi$', cs_views.serve_page, {"section": "cloud_rpi", "page": "all"}),
    url(r'^scs/metadata_rpi/service/licence$', cs_views.licence, name='license'),
    url(r'^scs/metadata_rpi/service/(?P<page>.*)$', cs_views.serve_page, {"section": "metadata_rpi"}),
    url(r'^scs/authentication/', sm_views.sm_login),
    url(r'^scs/test_session/', sm_views.sm_test_session),
    url(r'^scs/logout/', sm_views.sm_logout),
    url(r'^scs/getusage/', sm_views.sm_getusage),
    url(r'^scs/bulkupload/', sm_views.sm_bulk_upload),
    url(r'^scs/$', 'sm_controller.views.index'),
    url(r'^scs$', 'sm_controller.views.index'),
    url(r'^scs/(?P<path>.*\.(html|js|css|png|woff|woff2))$', 'django.views.static.serve', 
        {'document_root': 'sm_controller/ui/'}),
)
