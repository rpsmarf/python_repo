angular.module('rpsmarf.jira.issuecollector', [
	'rpsmarf.user.service',
	'rpsmarf.config'
])
.directive('issuecollector',['SmUser', 'DEBUG', function(SmUser, DEBUG){

	return {
		restrict: 'E',
		scope: false,
		templateUrl: 'jira/issuecollector/issuecollector.tpl.html',
		link: function($scope, $element, $attrs){
			var userName, userEmail, init = false, called = false;
			var listener = $scope.$watch(function(scope){
				return SmUser.getProperty('id');
			}, function(newVal, oldVal, scope){
	
				if((!oldVal && newVal) || (DEBUG && newVal && !init)){

					init = true;
					userName = SmUser.getProperty('firstname') + ' ' + SmUser.getProperty('lastname');
					userEmail = SmUser.getProperty('email');

					if(window.ATL_JQ_PAGE_PROPS){
						//delete window.ATL_JQ_PAGE_PROPS;
					}
					
					if(!called){
						//Initialize option values
						//Adding JIRA Issue collector
						window.ATL_JQ_PAGE_PROPS =  {
							//Submit an issue options
							'dc993902': {
								triggerFunction: function(showCollectorDialog) {
									//Requires that jQuery is available! 
									$element.on('SmOpenIssueCollector',function(e) {
										if(e && e.preventDefault){
											e.preventDefault();
										}
										showCollectorDialog();
									});
								},
								fieldValues: {
									fullname: userName,
									email: userEmail,
									components: '10003',
									priority: '2'

								}
							},
							//Ask A question options
							'd0431285': {
								triggerFunction: function(showCollectorDialog) {
									//Requires that jQuery is available! 
									$element.on('SmOpenAskAQuestionCollector',function(e) {
										if(e && e.preventDefault){
											e.preventDefault();
										}
										showCollectorDialog();
									});
								},
								fieldValues: {
									fullname: userName,
									email: userEmail,
									components: '10002',
									priority: '3'
								}
							}
							
						};
					}else {
						//Update option values (user info)
						//submit an issue
						window.ATL_JQ_PAGE_PROPS['dc993902'].fieldValues.email = userEmail;
						window.ATL_JQ_PAGE_PROPS['dc993902'].fieldValues.fullname = userName;
						//ask a question
						window.ATL_JQ_PAGE_PROPS['d0431285'].fieldValues.email = userEmail;
						window.ATL_JQ_PAGE_PROPS['d0431285'].fieldValues.fullname = userName;
					}

					if(!called){
						called = true;
						//Submit an issue
						angular.element.ajax({
							url: "https://rpsmarf.atlassian.net/s/67745d9625f8384867d0fe0cfc476f67-T/en_US-1g0czs/65007/10/1.4.25/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=en-US&collectorId=dc993902",
							type: "get",
							cache: true,
							dataType: "script"
						});
						//Ask a question
						angular.element.ajax({
							url: "https://rpsmarf.atlassian.net/s/67745d9625f8384867d0fe0cfc476f67-T/en_US-1g0czs/65007/10/1.4.25/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=en-US&collectorId=d0431285",
							type: "get",
							cache: true,
							dataType: "script"
						});
					}
					
				}
			});
			
		}

	};
}]);