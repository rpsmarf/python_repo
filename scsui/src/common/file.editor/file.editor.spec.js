describe('file editor (file.editor) module', function(){

	var $scope, Restangular, file, fileContent, smarfurl, $httpBackend;
	beforeEach(module('rpsmarf.config'));
	beforeEach(module('rpsmarf.file.editor'));
	beforeEach(module('restangular'));

	beforeEach(inject(function($rootScope, $controller, _Restangular_, SMARF_URL, _$httpBackend_){
		$scope = $rootScope.$new();
		$httpBackend = _$httpBackend_;
		smarfurl = SMARF_URL+'/scs/';
		Restangular = _Restangular_;
		Restangular.configuration.baseUrl = smarfurl;
		Restangular.configuration.encodeIds = false;
		file = {name: 'My File', url: 'resource/4/'+encodeURIComponent('/a/b/My File')+'/'};
		fileContent = 'Distillery 3 wolf moon Shoreditch, messenger bag Austin YOLO biodiesel Odd Future VHS normcore'+ 
		'Etsy hashtag yr Banksy art party. Letterpress beard street art meh, normcore ennui pug hella PBR&B skateboard umami cred farm-to-table biodiesel post-ironic.'+ 
		'Shabby chic fixie cornhole, twee ennui Wes Anderson pop-up migas Shoreditch Vice. Cardigan try-hard quinoa skateboard, meditation occupy irony before they sold '+
		'out tousled hella mustache. 3 wolf moon PBR&B PBR brunch plaid, Echo Park migas. Slow-carb Blue Bottle chambray, Schlitz dreamcatcher lumbersexual Kickstarter. '+
		'Leggings biodiesel roof party drinking vinegar, mixtape small batch single-origin coffee YOLO PBR bespoke.';

		$httpBackend.expect('GET',smarfurl+'resource/4/'+encodeURIComponent('/a/b/My File')+'/download/').respond(200,fileContent);

	}));

	it('should get file content and perform an upload operation on file save', inject(['$controller', function($controller){
		$controller('FileEditorCtrl', {$scope: $scope, Restangular: Restangular, file:file, $modalInstance: null});
		$httpBackend.flush();
		expect($scope.fileName).toBe('My File');
		expect($scope.fileContent).toBe(fileContent);
		expect($scope.editingFile).toBe(false);
		$scope.editFile();
		expect($scope.editingFile).toBe(true);
		$scope.fileContent = 'World Hello!';
		//$httpBackend.expect('POST', smarfurl+'resource/'+encodeURIComponent('/a/b/My File')+'/',jasmine.any(Object)).respond(200);
		//$scope.editFile();

		//$httpBackend.flush();

		//expect($scope.buttonText).toBe('Edit');
		

	}]));

	

});