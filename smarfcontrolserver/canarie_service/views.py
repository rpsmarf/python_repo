'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jan 8, 2015

    @author: rpsmarf 
'''
from django.shortcuts import render

# Create your views here.
import logging
import json
from canarie_service.constants import JSON_CONTENT, INFO_NAME, INFO_SYNOPSIS, INFO_VERSION,\
    INFO_INSTITUTION, INFO_RESEARCH_SUBJECT_JSON, INFO_SUPPORT_EMAIL_JSON, INFO_CATEGORY, INFO_TAGS,\
    INFO_RELEASE_TIME_JSON, STAT_LAST_RESET, STAT_UPTIME_IN_SEC, LICENCE_URL
from django.http.response import HttpResponse, HttpResponseRedirect
import time
from canarie_service.models import SmModelCanarieServiceProperties
import datetime

logger = logging.getLogger(__name__)

rpsmarf_template_dir = 'platform/'

# Research Middleware API implementation views and methods


def get_db_prop_value(name):
    prop = SmModelCanarieServiceProperties.objects.get(key=name)
    return prop.value


def info(request):
    """ Return a JSON or HTML representaion of the latest info depending up on
        supplied accept header.
    """
    contentDict = {
                   INFO_NAME: get_db_prop_value(INFO_NAME),
                   INFO_SYNOPSIS: get_db_prop_value(INFO_SYNOPSIS),
                   INFO_VERSION: get_db_prop_value(INFO_VERSION),
                   INFO_INSTITUTION: get_db_prop_value(INFO_INSTITUTION),
                   INFO_RELEASE_TIME_JSON: get_db_prop_value(INFO_RELEASE_TIME_JSON),
                   INFO_RESEARCH_SUBJECT_JSON: get_db_prop_value(INFO_RESEARCH_SUBJECT_JSON),
                   INFO_SUPPORT_EMAIL_JSON: get_db_prop_value(INFO_SUPPORT_EMAIL_JSON),
                   INFO_CATEGORY: get_db_prop_value(INFO_CATEGORY),
                   INFO_TAGS: json.loads(get_db_prop_value(INFO_TAGS)),
                   }
    if 'HTTP_ACCEPT' in request.META and request.META['HTTP_ACCEPT'] == JSON_CONTENT:
        return HttpResponse(status=200, content_type='application/json', content=json.dumps(contentDict))

    return render(request, rpsmarf_template_dir + 'info.html', {'info': contentDict})


lastReset_g = time.time()
lastResetDateTime_g = datetime.datetime.utcnow()


def stat_get_lastReset():
    d = lastResetDateTime_g.replace(microsecond=0)
    return d.isoformat() + "Z"
    pass


def stat_calc_uptime():
    return round(time.time() - lastReset_g)


def stats(request):
    """ Return a JSON or HTML representaion of the current statistics depending
        up on supplied accept header.
    """
    stats = {
             STAT_LAST_RESET: stat_get_lastReset(),
             STAT_UPTIME_IN_SEC: stat_calc_uptime()
             }
    if 'HTTP_ACCEPT' in request.META and request.META['HTTP_ACCEPT'] == JSON_CONTENT:
        return HttpResponse(status=200, content_type='application/json', content=json.dumps(stats))
    return render(request, rpsmarf_template_dir + 'stats.html', {'stats': stats})


def serve_page(request, **kwargs):
    """ Return a HTML representation of the current documentation """
    path = kwargs["section"] + "/" + kwargs["page"] + ".html"
    logger.debug("Path is %s, section %s, page %s", path, kwargs["section"], kwargs["page"])
    return render(request, path, {})


def licence(request):
    """ Return a HTML representation of the current license """
    return HttpResponseRedirect(get_db_prop_value(LICENCE_URL))


def videos(request, **kwargs):
    """ Return a HTML representation of the current license """
    page = kwargs["page"]
    return HttpResponseRedirect(get_db_prop_value(page))
