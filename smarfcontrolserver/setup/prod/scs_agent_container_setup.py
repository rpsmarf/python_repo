import os
import sys
import json
from scripts.scs_obj_common import scsScriptGetDefaultUrl
from smcommon.globals.envvar import SM_ENV_VAR_TEST_PASSWORD, smCommonGetEnvVar
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath
from scripts.scs_obj_create import scsCreateObjectPath

try:
    os.mkdir("/tmp/src")
except:
    pass
try:
    os.mkdir("/tmp/dest")
except:
    pass
print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

root_path = scsGetObjectPath("user", "username=rpsmarf")


community_path = scsCreateObjectPath("community", 
                                     {"name": "Everything",
                                      "name_key": "all_resources",
                                      "description": "This community represents all tools",
                                      "leader": root_path,
                                      "admin": root_path,
                                      })

rt_storage_path = scsCreateObjectPath("resource_type", 
                                      {"name": "Cloud Files Resource Type", 
                                       "name_key": "generic_storage",
                                       "nature": "data",
                                       "description": "This is a cloud storage for end-user data storage"})

# This allows us to run local tools on a non-www node
myHost = "www.rpsmarf.ca"
if 'SMARF_MY_HOST' in os.environ:
    myHost = os.environ['SMARF_MY_HOST']
    
cloud_agent_data_path = scsCreateObjectPath("agent", 
                                       {"guid": "LOCAL_AGENT",
                                        "name": "cloud_file_agent",
                                        "description": "This remote agent holds users' cloud files",
                                        "owner": root_path,
                                        "last_change": '2013-01-01',
                                        "agentUrl": "ice://" + myHost + ":9001"
                                        })

cloud_container_path = scsCreateObjectPath("container", {"name": "cloud_file_container", 
                                                         "name_key": "user_storage",
                                                         "containerUrl": "local://localhost/tmp/cloud_files/",
                                                         "agent": cloud_agent_data_path,
                                                         "description": "This is the local agent.",
                                                         "status": "up"})

scsCreateObjectPath("user_setting",
                    {"user": root_path,
                     "community": community_path})

user_b = scsCreateObjectPath("user", {"username": "andrewmcgregor",
                                      "email": "andrewmcgregor@sce.carleton.ca",
                                      "password": smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD),
                                      "first_name": "Andrew",
                                      "last_name": "McGregor"})

user_c = scsCreateObjectPath("user", {"username": "navdeepk",
                                      "email": "navdeepk@sce.carleton.ca",
                                      "password": smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD),
                                      "first_name": "Navdeep",
                                      "last_name": "Kapoor"})

user_c = scsCreateObjectPath("user", {"username": "melendez",
                                      "email": "melendez@sce.carleton.ca",
                                      "password": smCommonGetEnvVar(SM_ENV_VAR_TEST_PASSWORD),
                                      "first_name": "Orlando",
                                      "last_name": "Melendez"})

user_c = scsCreateObjectPath("user", {"username": "bnandy",
                                      "email": "bnandy@solananetworks.com",
                                      "password": "4087-3589",
                                      "first_name": "Biswajit",
                                      "last_name": "Nandy"})

user_c = scsCreateObjectPath("user", {"username": "sikharesh",
                                      "email": "shikharesh.majumdar@sce.carleton.ca",
                                      "password": "7420-5441",
                                      "first_name": "Shikharesh",
                                      "last_name": "Majumdar"})

user_c = scsCreateObjectPath("user", {"username": "marc_sthilaire",
                                      "email": "Marc.StHilaire@carleton.ca",
                                      "password": "8345-2983",
                                      "first_name": "Marc",
                                      "last_name": "StHilaire"})

user_c = scsCreateObjectPath("user", {"username": "aditya",
                                      "email": "aditya.sriram.rajasekaran@ericsson.com",
                                      "password": "6209-3456",
                                      "first_name": "Aditya",
                                      "last_name": "Rajasekaran"})

carleton_agent_path = scsCreateObjectPath("agent", 
                                          {"guid": "CARLETON-BRIDGE",
                                           "name": "carleton_agent",
                                           "description": 
                                           "This remote agent provides access to data held at Carleton",
                                           "owner": root_path,
                                           "last_change": '2013-01-01',
                                           "agentUrl": "ice://majumdar-10.sce.carleton.ca:9211"
                                           })
    
spplash_container_path = scsCreateObjectPath("container", {"name": "Carleton Bridge Data Container", 
                                                           "name_key": "carleton_bridge_data",
                                                           "containerUrl": "local://localhost/mnt/smarf-bridge-data/",
                                                           "description": 
                                                           "This container provides access to bridge data held at Carleton",
                                                           "agent": carleton_agent_path,
                                                           "status": "up"})

carleton_maj11_ssh_container_path = scsCreateObjectPath("container", {"name": "Carleton SPPLASH Node", 
                                                                      "name_key": "carleton_spplash_runner",
                                                                      "description": 
                                                                      "This container provides access to a node to run SPPLASH",
                                                                      "containerUrl": 
                                                                      "ssh://majumdar-11.sce.carleton.ca:22/cygdrive/c/temp",
                                                                      "agent": carleton_agent_path,
                                                                      "status": "up"})

carleton_maj11_sftp_container_path = scsCreateObjectPath("container", {"name": "Carleton Majumdar-11 Storage", 
                                                                      "name_key": "maj11_storage",
                                                                      "description": 
                                                                      "This container provides storage to a node to run "
                                                                      "SPPLASH and WSAT so files can "
                                                                      "be copied to this node before a tool is started",
                                                                      "containerUrl": 
                                                                      "sftp://rpsmarf@majumdar-11.sce.carleton.ca:22/cygdrive/"
                                                                      "c/Users/rpsmarf/Desktop/?keyfile=dropbear_key",
                                                                      "agent": carleton_agent_path,
                                                                      "status": "up"})

dair_container_path = scsCreateObjectPath("container", 
                                          {"name": "mock_data_container", 
                                           "name_key": "mock_data_container",
                                           "containerUrl": "local://localhost/opt/smarf-demos/demos/data/",
                                           "description": "The container to hold data",
                                           "agent": cloud_agent_data_path,
                                           "status": "up"})


# Setting up common task types

TASK_TYPE_ZIP_DICT = {
    "args": [ 
        {"type": "string", 
         "name": "Argument", 
         "value": "zip"},
        {"type": "string", 
         "name": "Argument", 
         "value": "-rdc"},
        {"type": "resource", 
         "name": "ZIP file to create", 
         "description": "The name of the zip file which is created by the zip task",
         "pathType": "file", 
         "input_id": 1,
         "direction": "output", 
         "role": "zip"},
        {"type": "resource", 
         "name": "Folder to zip up", 
         "description": "The name of the folder whose contents are zipped into a file",
         "pathType": "folder", 
         "input_id": 2, 
         "direction": "input", 
         "role": "zip", 
         "modifier": "filename"},
        ],
    "computeResourceRole": "zip",
    "skipRunPermCheck": True,
    "workingDir": {"type": "resource", "input_id": 2, "role": "zip", "modifier": "parent"}
    }

tt_zip = scsCreateObjectPath("task_type", {"name": "zip", 
                                           "code_module": "smcommon.task_runners.task_runner_zip", 
                                           "code_classname": "SmTaskRunnerZip", 
                                           "description": "Task to zip up files",
                                           "uiVisible": False,
                                           "owner": root_path,
                                           "configurationJson": json.dumps(TASK_TYPE_ZIP_DICT)})

scsCreateObjectPath("resource_type_task_type", {"role": "zip", "task_type": tt_zip, 
                                                "resource_type": rt_storage_path,
                                                "name": "Generic Storage for Zip",
                                                "name_key": "generic_storage_rttt",
                                                "nature": "data",
                                                "description": 
                                                "This links the zip task type to all storage resources"})


TASK_TYPE_UNZIP_DICT = {
                            "args": [ 
                                     {"type": "string", 
                                      "name": "Argument", 
                                      "value": "unzip"},
                                     {"type": "string", 
                                      "name": "Argument", 
                                      "value": "-d"},
                                     {"type": "resource", 
                                      "name": "Folder to unzip into", 
                                      "description": "This is the folder into which the data will be unzipped",
                                      "pathType": "folder", 
                                      "input_id": 2, 
                                      "direction": "output", 
                                      "role": "unzip"},
                                     {"type": "resource", 
                                      "name": "File to unzip", 
                                      "description": "This is the name of the file to unzip",
                                      "pathType": "file", 
                                      "input_id": 1, 
                                      "direction": "input", 
                                      "role": "unzip"}
                                     ],
                            "computeResourceRole": "unzip",
                            "skipRunPermCheck": True,
                       }

tt_unzip = scsCreateObjectPath("task_type", {"name": "unzip", 
                                             "code_module": "smcommon.task_runners.task_runner_shell", 
                                             "code_classname": "SmTaskRunnerShell", 
                                             "description": "Task to unzip data",
                                             "uiVisible": False,
                                             "owner": root_path,
                                             "configurationJson": json.dumps(TASK_TYPE_UNZIP_DICT)})

scsCreateObjectPath("resource_type_task_type", {"role": "unzip", "task_type": tt_unzip, 
                                                "resource_type": rt_storage_path,
                                                "nature": "data",
                                                "name": "Unzip generic storage",
                                                "name_key": "unzip_generic_storage",
                                                "description": "This links the unzip task type to all unzip resources"})


TASK_TYPE_DELETE_DICT = {
                            "args": [
                                     {"type": "resource", 
                                      "name": "Folder to delete", 
                                      "description": "This is the name of the folder to delete",
                                      "input_id": 1, 
                                      "direction": "input", 
                                      "role": "storage",
                                      "pathType": "folder"},
                                     ],
                            "computeResourceRole": "storage",
                            "skipRunPermCheck": True,
                       }
tt_delete = scsCreateObjectPath("task_type", {"name": "Recursive Delete",
                                             "name_key": "delete_recusive",
                                             "code_module": "smcommon.task_runners.task_runner_delete",
                                             "code_classname": "SmTaskRunnerDelete",
                                             "description": "Task to delete data asynchronously",
                                             "uiVisible": False,
                                             "owner": root_path,
                                             "configurationJson": json.dumps(TASK_TYPE_DELETE_DICT)})

scsCreateObjectPath("resource_type_task_type", {"role": "storage", "task_type": tt_delete,
                                                "resource_type": rt_storage_path,
                                                "nature": "data",
                                                "name": "Delete data",
                                                "description": "This links the delete task type to all storage resources"})

TASK_TYPE_COPY_CFG_DICT = {
                            "args": [ 
                                     {"type": "resource", 
                                      "name": "Source file", 
                                      "pathType": "file", 
                                      "input_id": 1,
                                      "direction": "input", 
                                      "role": "src", 
                                      "description": "A file to copy."},
                                     {"type": "resource", 
                                      "name": "Destination file", 
                                      "pathType": "file", 
                                      "input_id": 2,
                                      "direction": "output", 
                                      "role": "dest", 
                                      "description": "A filename or folder to copy data to"},
                                     ],
                            "outputProgress": {"outputArg": 1},
                            "computeResourceRole": "dest",
                            "skipRunPermCheck": True,
                            "preExistingOutputFolderOk": True
                           }

TASK_TYPE_COPY_CFG = json.dumps(TASK_TYPE_COPY_CFG_DICT)
tt_path = scsCreateObjectPath("task_type", {"name": "copy",
                                            "code_module": "smcommon.task_runners.task_runner_copy",
                                            "code_classname": "SmTaskRunnerCopy",
                                            "owner": root_path,
                                            "configurationJson": TASK_TYPE_COPY_CFG,
                                            "uiVisible": False,
                                            "description": "Allows the user to copy files or folders from one " + 
                                            "resource/folder to another resource/folder"})

rt_data_path = scsCreateObjectPath("resource_type", {"name": "d1", "nature": "data",
                                                    "description": "This is the resource type for the cp"})
scsCreateObjectPath("resource_type_task_type", {"role": "src",
                                                "name": "cp_src",
                                                "task_type": tt_path,
                                                "resource_type": rt_storage_path,
                                                "description": "This is the source data for the cp command"})
scsCreateObjectPath("resource_type_task_type", {"role": "dest",
                                                "name": "cp_dest",
                                                "task_type": tt_path,
                                                "resource_type": rt_storage_path,
                                                "description": "This is the destination data for the cp command"})


print("Core production objects setup.")
