#/bin/bash

if [ "$3" == "" ]; then
    echo "This script creates a number of folders in the folder specified"
    echo " with the root name specified.  Each folder has an incrementing"
    echo " number suffix"
    echo "Syntax is $0 <destFolder> <folderBaseName> <width in folders>"
    exit 1
fi

DEST_FOLDER=$1
BASE_NAME=$2
COUNT=$3

mkdir -p $DEST_FOLDER
cd $DEST_FOLDER

if [ "$COUNT" == "0" ]; then
    exit 0
fi

for i in `seq 1 $COUNT`;
do
    NAME=$BASE_NAME$i
    echo Creating `readlink -m $NAME`
    mkdir $NAME
done
