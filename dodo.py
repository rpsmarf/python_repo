''' 
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
from doit import get_var

DOIT_CONFIG = {'default_tasks': ['show']}


def task_show():
    """show all available tasks"""
    return {'actions': ['doit list'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }

env = get_var('env', '')
integration = 'doit --dir scsui integration env=' + env if env != '' else 'doit --dir scsui integration'

def task_debup():
    """show all available tasks"""
    return {'actions': [integration,
                        'doit --dir smarfcontrolserver debup',
                        'doit --dir smarfremoteagent debup',
                        ],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }

def task_checker():
    """show all available tasks"""
    return {'actions': ['doit --dir smarfcommon checker',
                        'doit --dir smarfcontrolserver checker',
                        'doit --dir smarfremoteagent checker',
                        ],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }

def task_test():
    """show all available tasks"""
    return {'actions': ['rm -f /tmp/unittestLogging.conf',
                        'echo RUNNING TESTS ON SMARFCOMMON...',
                        'doit --dir smarfcommon test',
                        'echo RUNNING TESTS ON SMARFCONTROLSERVER...',
                        'doit --dir smarfcontrolserver test',
                        'echo RUNNING TESTS ON SMARFREMOTEAGENT...',
                        'doit --dir smarfremoteagent test',
                        ],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            'clean': ["cd smarfcommon/ ;doit forget test",
                      "cd smarfcontrolserver/ ;doit forget test",
                      "cd smarfremoteagent/ ;doit forget test",
                      ],
            }

def task_install():
    """show all available tasks"""
    return {'actions': ['doit --dir scsui integration',
                        'doit --dir smarfcontrolserver install',
                        'doit --dir smarfremoteagent install',
                        ],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }

def task_rinstall():
    """show all available tasks"""
    return {'actions': ['doit --dir scsui integration',
                        'doit --dir smarfcontrolserver rinstall',
                        'doit --dir smarfremoteagent rinstall',
                        ],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }

def task_rrestart():
    """show all available tasks"""
    return {'actions': ['doit --dir smarfcontrolserver rrestart',
                        'doit --dir smarfremoteagent rrestart',
                        ],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }

def task_restart():
    """show all available tasks"""
    return {'actions': ['doit --dir smarfcontrolserver restart',
                        'doit --dir smarfremoteagent restart',
                        ],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }

def task_clearlog():
    """Clears local logs"""
    return {'actions': ['doit --dir smarfcontrolserver clearlog',
                        'doit --dir smarfremoteagent clearlog',
            ],
            'uptodate': [False],
            'verbosity': 2,
            }

