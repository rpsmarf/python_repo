This folder contains a script with a number of curl commands and pauses
in between each step to demonstrate the RP-SMARF Metadata Search RPI.

To control the host which is used, set the environment variable
SCS_URL which defaults to 'localhost' if not set.  SCS_RESOURCE
(defaults to 1) controls which resource is accessed.
