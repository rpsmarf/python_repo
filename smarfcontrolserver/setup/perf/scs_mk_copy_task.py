import json
import os
import sys
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath
from scripts.scs_obj_create import scsCreateObjectPath

with open("/tmp/src/a", 'w') as the_file:
    the_file.write("Hello world")

try:
    os.remove("/tmp/dest/b")
except:
    pass

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

taskParams = {
              "input_ids": {
                            "1": {"path": "a"},
                            "2": {"path": "b"}
                            }
              }

tt_path = scsGetObjectPath("task_type", "name=copy")
root_path = scsGetObjectPath("user", "name=rpsmarf")
task_path = scsCreateObjectPath("task", {"name": "copy", 
                                         "task_type": tt_path,
                                         "owner": root_path,
                                         "parametersJson": json.dumps(taskParams),
                                         }
                                )

resource_type_task_type_data_dest_path = scsGetObjectPath("resource_type_task_type", "role=dest")
resource_type_task_type_data_src_path = scsGetObjectPath("resource_type_task_type", "role=src")

src_resource_path = scsGetObjectPath("resource", "name=copy_src")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": src_resource_path,
                                      "resource_type_task_type": resource_type_task_type_data_src_path,
                                      }
                    )

dest_resource_path = scsGetObjectPath("resource", "name=copy_dest")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": dest_resource_path,
                                      "resource_type_task_type": resource_type_task_type_data_dest_path,
                                      }
                    )

print("Task " + task_path + " to copy /tmp/src/a to /tmp/dest/b is setup")

