'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 22, 2014

    @author: rpsmarf
'''
from django.contrib.auth.models import User
import datetime
import pytz


def scsScriptGetDefaultOwner():
    user_obj = User.objects.get(name="rpsmarf")
    return user_obj


def scsScriptGetNow():
    return datetime.datetime.now(pytz.utc)  # @UndefinedVariable

