describe( 'AppCtrl', function() {
  var $rootScope, $httpBackend, smarfurl, $state, Restangular, SmUser, moduleLoaded = false, data, SmReservation, $interval;
  beforeEach( module( 'rpsmarf' ) );
  beforeEach( module( 'rpsmarf.config' ) );
  beforeEach( module( 'restangular' ) );
  beforeEach( module( 'ui.router' ) );
  beforeEach( module( 'ui.bootstrap' ) );
  beforeEach( module( 'rpsmarf.user.service' ) );
  beforeEach( module( 'rpsmarf.task.create.service' ) );
  beforeEach( module( 'rpsmarf.constants'));
  beforeEach( module( 'rpsmarf.reservation.service'));
  beforeEach( module( 'rpsmarf.helpers.service'));

  beforeEach( inject( function( _$rootScope_, _$httpBackend_, _SMARF_URL_, _Restangular_, _$state_, _SmUser_, testSessionPromise, $q, _SmReservation_, _SmHelpers_, _$interval_){
    smarfurl = _SMARF_URL_+'/scs/';
    Restangular = _Restangular_;
    Restangular.configuration.baseUrl = smarfurl;
    Restangular.configuration.encodeIds = false;
    SmUser = _SmUser_;
    $rootScope = _$rootScope_;
    SmUser = _SmUser_;
    SmReservation = _SmReservation_;
    $state = _$state_;
    spyOn($state,'go').andCallThrough();
    $httpBackend = _$httpBackend_;
    SmHelpers = _SmHelpers_;
    $interval = _$interval_;

    if(!moduleLoaded){
      var runBlock = angular.module('rpsmarf')._runBlocks[0][6];
      data = {valid: true, userid:35,email: 'test@test.com',firstname:'firstname', lastname:'lastname',community:{id:12,name:'the community', name_key:'the_community', description:'this is the community'}, performSetupOnNextLogin:true};
      var u = {id:35,email: 'test@test.com',firstname:'firstname', lastname:'lastname', state:0, community:{id:12,name:'the community', name_key:'the_community', description:'this is the community'}};
      
      $httpBackend.expect('GET', smarfurl+'test_session/').respond(200,data);
      
      $httpBackend.flush();
    }
    
    
    
  }));
  describe( 'isCurrentUrl', function() {
    var AppCtrl,user, $location, $scope, respObj, SmTaskCreate;

    
    
    beforeEach( inject( function( $controller, _$location_, _SmTaskCreate_, DEBUG, testSessionPromise, $q ) {
      respObj = JSON.parse(__html__['src/app/app.spec.json']);
      
      $location = _$location_;
      $scope = $rootScope.$new();
      
      
      user = SmUser.getObject();
      if(!DEBUG){
        SmUser.setProperty('id', 1);
        user.email='smarfer@rpsmarf.ca', user.firstname='Amelia',user.lastname='Brand',
        user.community= {

          "admin": "/scs/user/2/",
          "description": "The RADS group at carleton university.",
          "gid": 97,
          "id": 2,
          "leader": "/scs/user/2/",
          "name": "RADS",
          "name_key": "rads",
          "resource_uri": "/scs/community/2/"

        };
      }
      SmTaskCreate = _SmTaskCreate_;
      
      

      
      
      AppCtrl = $controller( 'AppCtrl', { $location: $location, $scope: $scope, SmTaskCreate:SmTaskCreate, SmUser:SmUser, $state: $state });
      if(!moduleLoaded){
        $httpBackend.expect('GET',smarfurl+'community/').respond(200, respObj );
        $httpBackend.expect('GET',smarfurl+'task/?aggregateParam=True&orderByIdDesc=True&limit=1&owner__id='+SmUser.getProperty('id')).respond(200, respObj.completedTask );
        $httpBackend.expect('GET',smarfurl+'resource/?owner__id='+SmUser.getProperty('id')+'&personalFolder=true').respond(200, respObj.personal_folders );
        $httpBackend.expect('GET',smarfurl+'property/').respond(200, respObj.property );
        
        $state.go('home');
        $rootScope.$apply();
        
        

        
        $httpBackend.flush();
        moduleLoaded = true;
      }
      else {
        $httpBackend.expect('GET', smarfurl+'test_session/').respond(200,data);
        $httpBackend.flush();
      }
      

    }));

    //Good test for determining dependency issues
    it( 'should pass a dummy test', inject( function() {
      
      expect( AppCtrl ).toBeTruthy();
      expect(angular.copy($scope.communities)).toEqual(respObj.objects);
    }));

  
    it('should perform the correct actions when transitioning between states', inject(function(){
      
      $state.go('home');
      $rootScope.$apply();
      expect($scope.pageTitle).toEqual('Dashboard | RP-SMARF');
      expect($scope.showCommon).toBe(true);
      expect($scope.showSideNavbar).toBe(true);
      expect(angular.copy($scope.mainClasses)).toEqual(['col-md-offset-2', 'col-md-10', 'main', 'main-home']);
      expect($scope.infoPanel.collapsed).toBe(true);

      $state.go('tools');
      $rootScope.$apply();
      expect($scope.showCommon).toBe(true);
      expect($scope.showSideNavbar).toBe(true);
      expect(angular.copy($scope.mainClasses)).toEqual(['col-md-offset-2', 'col-md-10', 'main']);
      expect($scope.infoPanel.collapsed).toBe(true);

      $state.go('taskstart');
      $rootScope.$apply();
      expect($scope.showCommon).toBe(true);
      expect($scope.showSideNavbar).toBe(true);
      expect(angular.copy($scope.mainClasses)).toEqual(['col-md-offset-2', 'col-md-10', 'main']);
      expect($scope.infoPanel.collapsed).toBe(true);

      spyOn(SmTaskCreate,'reset');
      spyOn(SmReservation, 'reset');
      $state.go('home');
      $rootScope.$apply();
      expect(SmTaskCreate.reset).toHaveBeenCalled();
      expect(SmReservation.reset).toHaveBeenCalled();
      expect($scope.infoPanel.collapsed).toBe(true);

      $state.go('user.settings.account');
      $rootScope.$apply();
      expect($scope.showCommon).toBe(true);
      expect($scope.showSideNavbar).toBe(true);
      expect(angular.copy($scope.mainClasses)).toEqual(['col-md-offset-2', 'col-md-10', 'main']);    
      expect($scope.infoPanel.collapsed).toBe(true);  

      $state.go('login');
      $rootScope.$apply();
      expect($scope.showCommon).toBe(false);
      expect($scope.showSideNavbar).toBe(false);
      expect(angular.copy($scope.mainClasses)).toEqual([]);
      expect($scope.infoPanel.collapsed).toBe(true);

    }));

    it('should check for session validity on app run', inject(function(testSessionPromise, $q, DEFAULT_USER_ICON_URL, $sce){
      var runBlock = angular.module('rpsmarf')._runBlocks[0][6];
      var data = {valid: true, userid:35,email: 'test@test.com',firstname:'firstname', lastname:'lastname',community:{id:12,name:'the community', name_key:'the_community', description:'this is the community'}, performSetupOnNextLogin:true, imageUrl: 'http://www.avatar.com/', uiSettingsJson:JSON.stringify({something:false})};
      var u = {id:35,email: 'test@test.com',firstname:'firstname', lastname:'lastname', state:2, community:{id:12,name:'the community', name_key:'the_community', description:'this is the community'}, imageUrl:'http://www.avatar.com/',uiSettings: {something:false}};

      $httpBackend.expect('GET', smarfurl+'test_session/').respond(200,{valid:false});
      
      runBlock(Restangular,$state,SmUser,true,testSessionPromise, $q);
      $httpBackend.flush();
      expect($state.go).toHaveBeenCalledWith('login');
      expect(testSessionPromise.resolved).toBe(true);
      expect(testSessionPromise.p).toBeTruthy();
      testSessionPromise.init = true;
      
      $httpBackend.expect('GET', smarfurl+'test_session/').respond(200,data);
      $httpBackend.expect('GET',smarfurl+'community/').respond(200, respObj );
      $httpBackend.expect('GET',smarfurl+'task/?aggregateParam=True&orderByIdDesc=True&limit=1&owner__id=35').respond(200, respObj.completedTask );
      $httpBackend.expect('GET',smarfurl+'resource/?owner__id=35&personalFolder=true').respond(200, respObj.personal_folders );
      $httpBackend.expect('GET',smarfurl+'property/').respond(200, respObj.property );
      runBlock(Restangular,$state,SmUser,true,testSessionPromise, $q);

      $state.go('resourceconfig.browse');

      $httpBackend.flush();
      expect(testSessionPromise.resolved).toBe(true);
      expect(testSessionPromise.p).toBeTruthy();
      expect(testSessionPromise.performSetupOnNextLogin).toBe(true);
      expect(SmUser.getObject()).toEqual(u);
      $rootScope.$apply();
      expect($state.go).toHaveBeenCalledWith('setup.page');
      expect($scope.smProperties).toBeTruthy();
      expect($scope.smProperties.videoIntroduction).toEqual({
          "gid": 7,
          "key": "videoIntroduction",
          "resource_uri": "/scs/property/videoIntroduction/",
          "value": jasmine.any(Object)
      });

    }));

    it('should get the user\'s last run task' , inject(function(testSessionPromise, $q, USER_STATE){
      if(!testSessionPromise.post){
        SmUser.setProperty('state',USER_STATE.loggingIn);
      }
      var runBlock = angular.module('rpsmarf')._runBlocks[0][6];
      var data = {valid: true, userid:35,email: 'test@test.com',firstname:'firstname', lastname:'lastname',community:{id:12,name:'the community', name_key:'the_community', description:'this is the community'}, performSetupOnNextLogin:true, imageUrl: 'http://www.avatar.com/'};
      var u = {id:35,email: 'test@test.com',firstname:'firstname', lastname:'lastname', state:1, community:{id:12,name:'the community', name_key:'the_community', description:'this is the community'}, imageUrl: 'http://www.avatar.com/'};

      $httpBackend.expect('GET', smarfurl+'test_session/').respond(200,data);
      
      runBlock(Restangular,$state,SmUser,true,testSessionPromise, $q);
      
      $httpBackend.flush();

      $state.go('resourceconfig.browse');
      $httpBackend.expect('GET',smarfurl+'community/').respond(200, respObj );
      $httpBackend.expect('GET',smarfurl+'task/?aggregateParam=True&orderByIdDesc=True&limit=1&owner__id=35').respond(200, respObj.completedTask );
      $httpBackend.expect('GET',smarfurl+'resource/?owner__id=35&personalFolder=true').respond(200, respObj.personal_folders );
      $httpBackend.expect('GET',smarfurl+'property/').respond(200, respObj.property );
      $httpBackend.flush();

      $rootScope.$apply();
      

      var obj = respObj.completedTask.objects[0];
      var pj = JSON.parse(obj.parametersJson);
      expect($scope.taskname).toEqual(pj.task_type_name);
      expect($scope.taskWrapperCollapsed.is).toBe(true);
      expect($scope.currentTaskApi.getRestangularUrl()).toEqual(smarfurl+'task/'+obj.id+'/');
      expect($scope.currentTaskObj).toEqual(obj);
      expect($scope.taskProgressValue).toBe(100);
      expect($scope.taskState).toEqual(obj.state);
      expect($scope.showTaskbar).toBe(true);
      expect($scope.currentTaskStatus).toBe('completed');
      expect($scope.progressbarClasses).toEqual([]);
    }));

    it('should get the user\'s personal cloud folder usage info', inject(function(testSessionPromise, $q, USER_STATE){
      if(!testSessionPromise.post){
        SmUser.setProperty('state',USER_STATE.loggingIn);
      }
      var runBlock = angular.module('rpsmarf')._runBlocks[0][6];
      var data = {valid: true, userid:35,email: 'test@test.com',firstname:'firstname', lastname:'lastname',community:{id:12,name:'the community', name_key:'the_community', description:'this is the community'}, performSetupOnNextLogin:true};
      var u = {id:35,email: 'test@test.com',firstname:'firstname', lastname:'lastname', state:1, community:{id:12,name:'the community', name_key:'the_community', description:'this is the community'}};

      $httpBackend.expect('GET', smarfurl+'test_session/').respond(200,data);
      
      runBlock(Restangular,$state,SmUser,true,testSessionPromise, $q);
      
      $httpBackend.flush();

      $state.go('resourceconfig.browse');
      $httpBackend.expect('GET',smarfurl+'community/').respond(200, respObj );
      $httpBackend.expect('GET',smarfurl+'task/?aggregateParam=True&orderByIdDesc=True&limit=1&owner__id=35').respond(200, respObj.completedTask );
      $httpBackend.expect('GET',smarfurl+'resource/?owner__id=35&personalFolder=true').respond(200, respObj.personal_folders );
      $httpBackend.expect('GET',smarfurl+'property/').respond(200, respObj.property );
      $httpBackend.flush();
      
      $rootScope.$apply();
      
      var folders = {"public":{"class":"fail", "percentageUsed": ((10000000000/10000000000)*100).toFixed(2),"info":(10000000000/1073741824).toFixed(3) + ' GB of ' + (10000000000/1073741824).toFixed(2) + ' GB used'},
                     "private":{"class":"normal", "percentageUsed": ((4011976/10000000000)*100).toFixed(2),"info":(4011976/1073741824).toFixed(3) + ' GB of ' + (10000000000/1073741824).toFixed(2) + ' GB used'}
                    };
      expect(angular.copy($scope.folderInfo)).toEqual(folders);
    }));

    it('should be able to rerun tasks using the info panel task progress bar ', inject(function(){
      $scope.currentTaskObj = respObj.completedTask.objects[0];
      $httpBackend.expect('GET', smarfurl + 'task/1/'+'?aggregateParam=True').respond(200,respObj.completedTask.objects[0]);
      spyOn(SmTaskCreate, 'decomposeAndRerunTask').andReturn({ then: function(cb){ cb({taskId:37,interactive: false});}});
      spyOn($scope,'$emit');
      spyOn(SmTaskCreate,'reset');
      $scope.rerunTask();

      $rootScope.$apply();
      angular.element('.modal-footer .btn-primary').click();

      angular.element('body .modal-body input').val('mytestname');
      angular.element('body .modal-body input').trigger('input');
      angular.element('body .modal-footer .btn-primary').click();
      //$rootScope.$apply();
      $httpBackend.flush();
      expect($scope.currentTaskObj.parametersJson).toEqual(respObj.completedTask.objects[0].parametersJson);
      expect(SmTaskCreate.decomposeAndRerunTask).toHaveBeenCalledWith(respObj.completedTask.objects[0]);
      expect($scope.$emit).toHaveBeenCalled();
      expect(SmTaskCreate.reset).toHaveBeenCalled();

    }));

    it('should update the user\'s community through the navbar', inject(function(){

      spyOn($scope,'$broadcast');
      var newCommunity = {

          "admin": "/scs/user/2/",
          "description": "Test community.",
          "gid": 97,
          "id": 78,
          "leader": "/scs/user/2/",
          "name": "Test",
          "name_key": "test",
          "resource_uri": "/scs/community/78/"

      };

      $httpBackend.expect('PATCH', smarfurl+'user_setting/'+user.id+'/', {community: newCommunity}).respond(200);
      $scope.changeCommunity(newCommunity);
      $httpBackend.flush();
      expect($scope.$broadcast).toHaveBeenCalledWith('SmCommunityChange');
      expect(user.community).toEqual(newCommunity);


    }));

    it('should complete multi step tasks', inject(function(){
      expect($scope.multiStepTask).toBe(false);
      expect($scope.additionalInfo).toBe(null);
      spyOn(SmHelpers,'completeFolderDownload');
      var taskInfo = respObj.taskExample;
      $httpBackend.resetExpectations();
      $httpBackend.expect('GET', smarfurl+'task/12/').respond(200, taskInfo);

      $scope.$emit('SmTaskStart', Restangular.one('task/12/'),'Preparing to download folder', false, {downloadFolder: true, resourceId: 45, zipFilePath: '.rpsmarf/tmp/8329c3ca-1f03-4351-8984-2fddc8a18514.zip'});
      expect($scope.multiStepTask).toBe(true);
      expect($scope.additionalInfo).toBeTruthy();
      $interval.flush(1000);
      $httpBackend.flush();
      taskInfo.state = 'finished';
      taskInfo.completion = 'completedWithoutError';
      $httpBackend.expect('GET', smarfurl+'task/12/').respond(200, taskInfo);
      $interval.flush(1000);
      $httpBackend.flush();
      expect(SmHelpers.completeFolderDownload).toHaveBeenCalledWith({downloadFolder: true, resourceId: 45, zipFilePath: '.rpsmarf/tmp/8329c3ca-1f03-4351-8984-2fddc8a18514.zip'});
      expect($scope.multiStepTask).toBe(false);
      expect($scope.additionalInfo).toBe(null);

    }));

  });
});
