'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import glob
import os
import fnmatch
import datetime

installFiles = glob.glob("install/*")
debFiles = installFiles
debFiles += glob.glob("phpbb/deltas/*.php")
debFiles += ["db_setup/phpbb.dump"]
debFiles += glob.glob("db_setup/cmds/*")

SMARF_USER = "unset"
SMARF_HOST = "unset"
if ("SMARF_HOST" in os.environ):
    SMARF_HOST = os.environ["SMARF_HOST"]
if ("SMARF_GITDIR" in os.environ):
    SMARF_GITDIR = os.environ["SMARF_GITDIR"]
if ("SMARF_USER" in os.environ):
    SMARF_USER = os.environ["SMARF_USER"]
t = datetime.datetime.today()
SMARF_VERSION = t.strftime("%Y%m%d%H%M%S") + "-" + SMARF_USER
    
SSH_CMD = ("ssh  -o UserKnownHostsFile=/dev/null " + 
"-o StrictHostKeyChecking=no " + 
"-i ../tools/keys/smarf_dair_keys.pem ubuntu@" + SMARF_HOST)

DOIT_CONFIG = {'default_tasks': ['show']}


def task_show():
    """show all available tasks"""
    return {'actions': ['doit list'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }


def task_keysetup():
    """show all available tasks"""
    return {'actions': ['rm -f ../build_output/smarf_dair_keys.pem',
                        'cp ../tools/keys/smarf_dair_keys.pem ../build_output/smarf_dair_keys.pem',
                        'chmod 400 ../build_output/smarf_dair_keys.pem'],
            'file_dep': ["../tools/keys/smarf_dair_keys.pem"],
            'targets': ["../build_output/smarf_dair_keys.pem"],
            'verbosity': 2,
            }


def task_vlist():
    """show all available versions of smarf-forum on the software server"""
    return {'actions': ['sudo apt-get update > /dev/null',
                        'apt-cache show smarf-forum | grep Version'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }


def task_deb():
    """create .deb file for the SMARF forum"""
    return {'actions': [
            'rm -rf ../build_output/smarf-forum',
            'mkdir ../build_output/smarf-forum',
            'mkdir ../build_output/smarf-forum/DEBIAN',
            'sed -e s/@VERSION/' + SMARF_VERSION + 
'/ install/control > ../build_output/smarf-forum/DEBIAN/control',
            'cp install/postinst ../build_output/smarf-forum/DEBIAN',
            'chmod 755 ../build_output/smarf-forum/DEBIAN/postinst',
            'mkdir -p ../build_output/smarf-forum/opt/smarf-forum/',
            'cp db_setup/phpbb.dump ../build_output/smarf-forum/opt/smarf-forum/phpbb.dump',
            'mkdir -p ../build_output/smarf-forum/usr/local/bin',
            'cp db_setup/cmds/* ../build_output/smarf-forum/usr/local/bin',
            'chmod 755 ../build_output/smarf-forum/usr/local/bin/*',
            'mkdir -p ../build_output/smarf-forum/var/www/',
            'unzip phpbb/*.zip -d ../build_output/smarf-forum/var/www',
            'mv ../build_output/smarf-forum/var/www/phpBB3 ../build_output/smarf-forum/var/www/forum',
            'mkdir -p ../build_output/smarf-forum/etc/nginx/sites-enabled',
            'cp -r phpbb/deltas/* ../build_output/smarf-forum/var/www/forum',
            'mv  ../build_output/smarf-forum/var/www/forum/install ../build_output/smarf-forum/var/www/forum/install.disabled',
            'cp install/etc_nginx_smarf-forum.conf ../build_output/smarf-forum/etc/nginx/sites-enabled/smarf-forum.conf',
            'mkdir -p ../build_output/smarf-forum/etc/default',
            'cp install/etc_default_php-fastcgi ../build_output/smarf-forum/etc/default/php-fastcgi',
            'mkdir -p ../build_output/smarf-forum/etc/init',
            'cp install/etc_init_smarf-forum.conf ../build_output/smarf-forum/etc/init/smarf-forum.conf',
            'cd ../build_output;dpkg -b smarf-forum',
            ],
            'targets': ['../build_output/smarf-forum.deb'],
            'file_dep': debFiles,
            'clean': True,
            }


def task_install():
    """install SMARF Forum DEB package locally"""
    return {'actions': ['sudo dpkg -r smarf-forum',
                        'sudo dpkg -i ../build_output/smarf-forum.deb'],
            'file_dep': ['../build_output/smarf-forum.deb'],
            'targets': ['/etc/init.d/smarf-forum'],
            'verbosity': 2,
            'clean': ["sudo dpkg --purge smarf-forum"],
            }

def task_rinstall():
    """install the currently building SRA DEB package remotely"""
    return {'actions': [
            'scp -q -o UserKnownHostsFile=/dev/null '
'-o StrictHostKeyChecking=no '
'-i ../tools/keys/smarf_dair_keys.pem ../build_output/smarf-forum.deb ubuntu@' + 
SMARF_HOST + ':/tmp',
            SSH_CMD + ' "sudo dpkg -i /tmp/smarf-forum.deb"',
            ],
            'file_dep': ['../build_output/smarf-forum.deb',
                         '../build_output/smarf_dair_keys.pem'],
            'verbosity': 2,
            }

def task_debup():
    """create and upload .deb file for the SRA"""
    return {'actions': 
            ['scp -q -o UserKnownHostsFile=/dev/null '
             '-o StrictHostKeyChecking=no '
            '-i ../tools/keys/smarf_dair_keys.pem '
            '../build_output/smarf-forum.deb '
            'ubuntu@sw.rpsmarf.ca:/var/www/debs/all/smarf-forum-' + 
             SMARF_VERSION + '.deb',
           'ssh  -o UserKnownHostsFile=/dev/null '
            '-o StrictHostKeyChecking=no '
            '-i ../tools/keys/smarf_dair_keys.pem ubuntu@sw.rpsmarf.ca '
            '"cd /var/www/debs;dpkg-scanpackages '
            '-m all| gzip -9c > all/Packages.gz"'
             ],
            'file_dep': ['../build_output/smarf-forum.deb',
                         '../build_output/smarf_dair_keys.pem'],
            'targets': [],
            'verbosity': 2,
            }


