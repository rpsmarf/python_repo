'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Jun 23, 2015

    @author: rpsmarf
'''
import socket
import logging
from threading import Thread

logger = logging.getLogger(__name__)


class TestMetricReceiver(Thread):
    '''
    classdocs
    '''

    def __init__(self, port):
        '''
        Constructor
        '''
        super().__init__()
        self.quit = False
        self.name = "MetricsReceiver"
        self.port = port
        self.rxedMetrics = []
        self.sock = None
        self.connection = None
        self.daemon = True
                  
    def stop(self):
        self.quit = True
        if self.sock:
            self.sock.close()
        if self.connection:
            self.connection.close()
        if self.isAlive():
            self.join(1.0)

    def run(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        # Bind the socket to the address given on the command line
        logger.debug("Binding metric receiver to port %d", self.port)
        server_address = ("0.0.0.0", self.port)
        self.sock.bind(server_address)

        while not self.quit:
            try:
                data, _ = self.sock.recvfrom(4096)
                logger.debug("Got metric packet with %d bytes", len(data))
                lines = data.decode("ascii").split("\n")
                for line in lines:
                    if len(line) > 0:
                        self.rxedMetrics.append(line)
            finally:
                pass
            