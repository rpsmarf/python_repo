import json
import os
import sys
from scripts.scs_task_start import scsTaskStartByPath
import time
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath
from scripts.scs_obj_create import scsCreateObjectPath


try:
    os.remove("/tmp/dest/b")
except:
    pass

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

taskParams = {
              "input_ids": {
                            "1": {"path": "2014-09-01.data"},
                            "2": {"path": "output" + str(time.time()) + "/"}
                            }
              }

tt_path = scsGetObjectPath("task_type", "name_key=mock_tool")
root_path = scsGetObjectPath("user", "name=rpsmarf")
task_path = scsCreateObjectPath("task", {"name": "shell_lines", 
                                         "task_type": tt_path,
                                         "owner": root_path,
                                         "parametersJson": json.dumps(taskParams),
                                         }
                                )

resource_type_task_type_data_dest_path = scsGetObjectPath("resource_type_task_type", "name_key=mock_data_output")
resource_type_task_type_data_src_path = scsGetObjectPath("resource_type_task_type", "name_key=mock_data_source")
resource_type_task_type_compute_path = scsGetObjectPath("resource_type_task_type", "name_key=mock_compute_resource")


src_resource_path = scsGetObjectPath("resource", "name_key=carleton_demo_data_a")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": src_resource_path,
                                      "resource_type_task_type": resource_type_task_type_data_src_path,
                                      }
                    )

dest_resource_path = scsGetObjectPath("resource", "name_key=demo_cloud_files")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": dest_resource_path,
                                      "resource_type_task_type": resource_type_task_type_data_dest_path,
                                      }
                    )

compute_resource_path = scsGetObjectPath("resource", "name_key=testab1_compute")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": compute_resource_path,
                                      "resource_type_task_type": resource_type_task_type_compute_path,
                                      }
                    )

print("Task " + task_path + " to run the lines command setup, starting task...")
scsTaskStartByPath(task_path, verbose=False)

