'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public LSwiftnse as published by
    the Free Software Foundation, either version 3 of the LSwiftnse, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public LSwiftnse for more details.

    You should have received a copy of the GNU General Public LSwiftnse
    along with RP-SMARF.  If not, see <http://www.gnu.org/lSwiftnses/>.
   
    Created on Sep 12, 2014

    @author: rpsmarf
'''
import logging
from smcommon.exceptions.file_access_exception import SmFileAccessException,\
    SM_FS_EXC_NOT_CONN
from smcommon.file_access.swift_accessor import SwiftAccessor
from queue import Queue
import threading
import time

logger = logging.getLogger(__name__)


class SmFileWriterWrapperSwift(object):
    '''
    '''
    def __init__(self):
        self.q = Queue(100)

    def write(self, data):
        self.q.put({"type": "data", "data": data}, block=True)

    def close(self):
        self.q.put({"type": "stop"}, block=True)
        while not self.q.empty():
            time.sleep(0.1)
    
    def generateData(self):
        while True:
            d = self.q.get()
            item_type = d["type"]
            if item_type == "data":
                yield d["data"]
            elif item_type == "stop":
                return


class SmFileReadWrapperSwift(object):
    '''
    '''
    def __init__(self, data_iter):
        self.data_iter = data_iter

    def read(self, maxBytes):
        try:
            return next(self.data_iter)
        except StopIteration:
            return b''

    def close(self):
        return 0


class SmFileAccessorSwift(object):
    '''
    This class implements the standard file access class for the OpenStack
    DAIR cloud.
    '''

    def __init__(self, dataContainer):
        '''
        Constructor
        '''
        self.dataContainer = dataContainer
        self.connected = False
        self.urlInfo = dataContainer.containerUri
        netloc = self.urlInfo.netloc
        path = self.urlInfo.path
        self.swiftUrl = "https://" + netloc + path
        logger.debug("Swift url is %s", self.swiftUrl)
        self.swiftIf = SwiftAccessor(self.swiftUrl,
                                     self.dataContainer.getParam("tokenUrl"),
                                     self.dataContainer.getParam("username"),
                                     self.dataContainer.getParam("password"),
                                     self.dataContainer.getParam("tenant"),
                                     self.dataContainer.getParam("container"), 
                                     3, "")
        
    def connectSession(self):
        if self.connected:
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, "", 
                                        "File accessor is already connected")
        if self.swiftIf.XAuthToken == "":
            self.swiftIf.XAuthToken = self.swiftIf.getToken()
        self.connected = True
        
    def disconnectSession(self):
        self.connected = False
        
    def deleteFileInfos(self, fileInfosToDelete):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, "", 
                                        "Delete of paths failed because connect operation was not done")
        
        try:
            for f in fileInfosToDelete:
                self.swiftIf.deleteObjectFromContainer(f["name"])
        except Exception as e: 
            raise SmFileAccessException(422, "EOTHER", "<multiple>", e.description)

    def deletePaths(self, pathsToDelete):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, "<multiple>", 
                                        "Delete of paths failed because connect operation was not done")
        
        try:
            for path in pathsToDelete:
                self.swiftIf.deleteObjectFromContainer(path)
        except Exception as e:
            raise SmFileAccessException(422, "EOTHER", e.path, e.description)
        
    def list(self, path, recursiveDepth, maxEntries, lastPathReturned, showHidden):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, path, "List failed because connect operation was not done")
        
        if path == "/":
            path = ""
            
        # If the path ends with // then we want the path of the folder as well as the children so 
        # in  open stack we take off the / at the end to get that
        if path.endswith("//"):
            path = path[:-2]
                
        if maxEntries == 1:
            return [self.swiftIf.getMetadata(path)]
            
        slashesInPath = path.count("/")
        returnList = []
        try:
            while True:
                swiftList = self.swiftIf.listInsideContainer(path, (recursiveDepth > 1), 
                                                             maxEntries, lastPathReturned)
                
                if len(swiftList) == 1 and path == swiftList[0]["name"]:
                    return swiftList
                
                if recursiveDepth == 1 and showHidden:
                    return swiftList
                else:
                    fullListReturned = len(swiftList) == maxEntries
                    # Operate on copy of string ([:]) to avoid problems of iterating 
                    # on changing list
                    discards = 0
                    for item in swiftList:
                        if not showHidden and (item["name"][0] == '.' or "/." in item['name']):
                            discards += 1
                            continue
                        if recursiveDepth > 1:
                            if (item["name"].count("/") - slashesInPath) > recursiveDepth:
                                discards += 1
                                continue
                        returnList.append(item)
                    if fullListReturned and discards > 0:
                        # Need to go back and get more because we threw away some entries and now we are 
                        # short some entries
                        maxEntries = maxEntries - len(returnList)
                        lastPathReturned = swiftList[-1]["name"]
                    else:
                        return returnList                        
                
        except Exception as e:
            raise SmFileAccessException(422, "EOTHER", path, e.description)
        return returnList
    
    def mkdir(self, path, makeParentFolders=False):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, path, "Mkdir failed because connect operation was not done")

        try:
            self.swiftIf.createContainerSubFolder(path)
        except Exception as e:
            raise SmFileAccessException(422, "EOTHER", path, e.description)
        
    def du(self, path, includeHidden=True):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, path, "du failed because connect operation was not done")

        try:
            size = 0
            if not path.endswith("/"):
                path += "/"
            if path == "/" or path == "./":
                path = ""
            swift_list_info = self.swiftIf.listInsideContainer(path, True, 10000, None)

            count = 0
            for file in swift_list_info:
                if includeHidden or not (file["name"].startswith(".") or "/." in file["name"]): 
                    size += file["size"]
                    count += 1
                
            return {"sizeInBytes": size, "fileCount": count}
        except Exception as e:
            raise SmFileAccessException(422, "EOTHER", path, e.description)
        
    def move(self, oldpath, newpath):
        raise Exception("Move not supported")
    
    def open(self, path, access_mode):
        if (not self.connected):
            raise SmFileAccessException(422, SM_FS_EXC_NOT_CONN, 
                                        "Open of path " + path + " failed because connect operation was not done")

        if "r" in access_mode:
            return SmFileReadWrapperSwift(self.swiftIf.downloadAsStream(path))
        else:
            wrapper = SmFileWriterWrapperSwift()
            t1 = threading.Thread(name="swift uploader", 
                                  daemon=True,
                                  target=self._uploadWrapper, 
                                  args=(path, wrapper.q, wrapper.generateData))
            t1.start()
            return wrapper
                        
    def _uploadWrapper(self, path, q, gen):
        try:
            self.swiftIf.uploadChunk(path, gen)
        except:
            logger.exception("Error during upload of " + str(path))
            # Consume all output so that the close works
            while True:
                d = q.get()    
                if d["type"] == "stop":
                    return
