'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import tempfile
from smcommon.constants.sm_const import SM_TEST_AGENT_ID, \
    SM_DATA_CONTAINER_PROTOCOL_LOCAL, SM_DATA_CONTAINER_PROTOCOL_SFTP
from os.path import os
from smcommon.file_access.data_access_desc import SmDataContainer, \
    SmDataAccessDescriptor
from smcommon.constants import sm_const
from smcommon.globals.sm_copy_controller import SmCopyController
import shutil
import uuid
import logging

logger = logging.getLogger(__name__)


class TempFolder(object):
    '''
    This class contains methods for working on a temp folder used
     for tests.  It allows a test to create files and folders
     with relative paths and then these are converted to paths
     within a uniquely named folder.
    '''

    def __init__(self):
        '''
        Constructor
        '''
        sm_const.smConstSetMyAgentId(SM_TEST_AGENT_ID)
        self.folder = tempfile.mkdtemp(prefix="unit_test")
        self.localDc = SmDataContainer(SM_TEST_AGENT_ID, "", SM_DATA_CONTAINER_PROTOCOL_LOCAL + "://localhost/")
        self.sftpDc = SmDataContainer(SM_TEST_AGENT_ID, "", SM_DATA_CONTAINER_PROTOCOL_SFTP + "://127.0.0.1:8222/")
        
    def makeAbsolutePath(self, name, dadType="local"):
        if dadType == "local":
            return os.path.join(self.folder, name)
        elif dadType == "sftp":
            return os.path.join(self.folder, name)
        elif dadType == "remote":
            return name
        else:
            raise Exception("Invalid type: " + dadType)
        
    def getLocalPathForTestDadObject(self, dad):
        if not dad.isLocal():
            return os.path.join(sm_const.SM_COPY_FAKE_ICE_PREFIX, dad.path)
        else:
            return dad.path
        
    def makeLocalDad(self, name):
        dad = SmDataAccessDescriptor(self.localDc, self.makeAbsolutePath(name, "local"))
        return dad

    def makeLocalDadList(self, nameList):
        pathList = []
        for name in nameList:
            pathList.append(self.makeAbsolutePath(name, "local"))
        dad = SmDataAccessDescriptor(self.localDc, pathList)
        return dad

    def makeFifoDad(self):
        name = str(uuid.uuid4())
        dad = SmDataAccessDescriptor(self.localDc, self.makeAbsolutePath(name, "local"))
        os.mkfifo(dad.getAbsPath())
        return dad

    def makeRemoteDad(self, name):
        dc = SmDataContainer("REMOTE_AGENT", "", SM_DATA_CONTAINER_PROTOCOL_LOCAL + "://localhost/")
        dad = SmDataAccessDescriptor(dc, self.makeAbsolutePath(name, "remote"))
        return dad

    def makeSftpDad(self, name):
        dad = SmDataAccessDescriptor(self.sftpDc, self.makeAbsolutePath(name, "sftp"))
        return dad

    def mkAbsolutePath(self, fname):
        if (not fname.startswith("/")):
            fname = os.path.join(self.folder, fname)
        return fname

    def writeFile(self, fname, content): 
        fname = self.mkAbsolutePath(fname)
        logger.debug("Writing file %s", fname)
        with open(fname, 'w') as the_file:
            the_file.write(content)
     
    def writeFileBinary(self, fname, content): 
        fname = self.mkAbsolutePath(fname)
        logger.debug("Writing file %s", fname)
        with open(fname, 'wb') as the_file:
            the_file.write(content)
     
    def readFile(self, fname, maxSize): 
        fname = self.mkAbsolutePath(fname)
        with open(fname, 'rb') as the_file:
            return the_file.read(maxSize)
     
    def mkdir(self, fname): 
        fname = self.mkAbsolutePath(fname)
        os.mkdir(fname)
             
    def rm(self, fname): 
        fname = self.mkAbsolutePath(fname)
        os.remove(fname)
             
    def rmdir(self, fname): 
        fname = self.mkAbsolutePath(fname)
        os.rmdir(fname)

    def mktree(self, pathList):
        '''
        Takes a list of paths and creates folders and files.
        Note that files contain the string "data-<filename>"
        '''
        for p in pathList:
            if p.endswith("/"):
                self.mkdir(p)
            else:
                self.writeFile(p, "data-" + p)
                
    def cleanup(self):
        '''
        Delete all of the contents of the temporary folder
        '''
        shutil.rmtree(self.folder, True)
        
    def makeCopyController(self):
        dc = SmDataContainer(SM_TEST_AGENT_ID, "", SM_DATA_CONTAINER_PROTOCOL_LOCAL + "//localhost/")
        return SmCopyController([dc.agentId])

    
class TempFolderViaFileAcc(TempFolder):
    '''
    This class contains methods for working on a temp folder used
     for tests where the folder is a cloud folder.
    '''

    def __init__(self, fileAcc):
        '''
        Constructor
        '''
        super().__init__()
        self.fileAcc = fileAcc
        # This is used to make tests that write to "resource_folder/d1/f1" work
        # with the cloud by removing resource_folder/ before writing the file or doing mkdir
        self.prefixToRemove = None
        
    def setPrefixToRemove(self, prefix):
        '''
        This is used to make tests that write to "resource_folder/d1/f1" work
        with the cloud by removing resource_folder/ before writing the file or doing mkdir
        '''
        self.prefixToRemove = prefix
        
    def _adjustName(self, fname):
        '''
        This is used to make tests that write to "resource_folder/d1/f1" work
        with the cloud by removing resource_folder/ before writing the file or doing mkdir
        '''
        if self.prefixToRemove is not None:
            fname = fname.replace(self.prefixToRemove, "")
        return fname
        
    def writeFile(self, fname, content): 
        fname = self._adjustName(fname)
        logger.debug("Writing file %s", fname)
        try:
            f = self.fileAcc.open(fname, "w")
            f.write(content.encode('utf-8'))
        finally:
            f.close()
             
    def writeFileBinary(self, fname, content): 
        fname = self._adjustName(fname)
        logger.debug("Writing file %s", fname)
        try:
            f = self.fileAcc.open(fname, "wb")
            f.write(content)
        finally:
            f.close()
     
    def readFile(self, fname, maxSize): 
        fname = self._adjustName(fname)
        f = None
        try:
            f = self.fileAcc.open(fname, "rb")
            return f.read(maxSize)
        finally:
            if f:
                f.close()
     
    def mkdir(self, fname):
        if not fname.endswith("/"):
            fname = fname + "/"
        fname = self._adjustName(fname)
        self.fileAcc.mkdir(fname, False)
             
    def rm(self, fname): 
        fname = self._adjustName(fname)
        self.fileAcc.deletePaths([fname])
             
    def rmdir(self, fname): 
        fname = self._adjustName(fname)
        self.fileAcc.deletePaths([fname])

    def cleanup(self):
        '''
        Delete all of the contents of the temporary folder
        '''
        # List the contents
        try:
            path_info = self.fileAcc.list("/", 1000, 1000, None, True)
        
            # Delete the contents
            self.fileAcc.deleteFileInfos(reversed(path_info))
        
            # Delete the container
            self.fileAcc.deletePaths([""])
        except:
            logger.debug("List of container failed, not deleting container")            
