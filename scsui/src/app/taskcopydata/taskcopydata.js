angular.module( 'rpsmarf.taskcopydata', [
  'rpsmarf.constants',
  'rpsmarf.config',
  'rpsmarf.user.service',
  'rpsmarf.taskservice',
  'restangular',
  'ui.router',
  'rpsmarf.foldernav',
  'ui.bootstrap',
  'rpsmarf.foldernav.dragdrop',
  'rpsmarf.taskprogress',
  'rpsmarf.container.resource.service',
  'rpsmarf.extendedinfo',
  'rpsmarf.alert',
  'rpsmarf.accessrequest'  
])

.config(function config( $stateProvider, RestangularProvider, SMARF_URL ) {
  
  $stateProvider.state('taskcopydata', {
    url: '/copy/',
    views: {
      'main': {
        controller: 'TaskCopyDataCtrl',
        templateUrl: 'taskcopydata/taskcopydata.tpl.html'
      }
    },
    data: {pageTitle: 'Copy data'}
  });

})
.filter('singleSelected', function(){

  return function(entries){
    var key,num=0;

    for(key in entries){
      num++;
      if(num > 1){
        return false;
      }
    }

    return num <= 1;
  };
})
.controller( 'TaskCopyDataCtrl', ['$scope', 'Restangular', '$modal', 'SmUser', '$state', 'SmTask', 'SMARF_URL', 'SmContainerResource', 'filterFilter', 'singleSelectedFilter', 'ROOT_FOLDER_ENTRY',
  function ( $scope, Restangular, $modal, SmUser, $state, SmTask, SMARF_URL, SmContainerResource, filterFilter, singleSelectedFilter, ROOT_FOLDER_ENTRY ) {

  /***VARIABLES***/
  /*Scope*/
  $scope.arr = [0,1];
  $scope.resourceSelected = ['false', 'false'];
  $scope.resources = [[],[]];
  $scope.selectedResource = [{entries:null}, {entries:null}];
  $scope.allEntriesSelected = [false,false];
  $scope.selectedEntries = [{},{}];
  $scope.searchText = ['',''];
  $scope.resourceSearchText = ['',''];
  $scope.entriesSearch = 'false';

  /*Standard*/
  //Map resource api to 1 since the returned results for /resource/ is an object that contains
  //the list of resources
  var uid = SmUser.getProperty('id');
  var community = SmUser.getProperty('community').name_key;
  var resourceApi = Restangular.one('resource/?sortForUser='+uid+'&extForUser='+uid+'&communityFilter='+community+'&limit=0');
  var taskresourceApi = Restangular.all('task_resource/');
  var rtttApi = Restangular.one('resource_type_task_type/');
  var taskApi = Restangular.all('task/');
  var rootEntries = [[],[]];
  var allResources = [[],[]];
  var selectedResourceIndex = [];
  var rootFolderEntry = angular.copy(ROOT_FOLDER_ENTRY);

  /***FUNCTIONS***/
  /*Scope*/
  $scope.selectResource = function(resource, containerIndex, index){
    //console.log(containerIndex);
    
    if(resource.selected){
      return false;
    }
    else{
      //console.log($scope.resources[containerIndex][selectedResourceIndex[containerIndex]]);
      if(angular.isNumber(selectedResourceIndex[containerIndex])){
        $scope.resources[containerIndex][selectedResourceIndex[containerIndex]].selected = false;
      }
      $scope.resourceSelected[containerIndex] = "true";
      resource.selected = true;
      selectedResourceIndex[containerIndex] = index;
      
      $scope.selectedResource[containerIndex] = resource;
      //For testing!!
      if($scope.selectedResource[containerIndex].name === 'copy_dest')
      {
        $scope.selectedResource[containerIndex].type = "stream";
        return;
      }
      rootFolderEntry = angular.copy(ROOT_FOLDER_ENTRY);
      Restangular.all('resource/'+resource.id+'/list/').get('').then(function(objects){
        objects.unshift(rootFolderEntry);
        for(var i= objects.length-1; i>=0; i--){
          objects[i].smHashData = index;
        }
        rootEntries[containerIndex] = objects;
        $scope.selectedResource[containerIndex].entries = objects;
        $scope.selectedResource[containerIndex].selectedEntry = null;
        //console.log($scope.selectedResource[index].entries);
      });
    }
      
  };

  $scope.userAccess = function(user, action){
    if(user[action] && user[action] === "true"){
      return 'fa-check';
    }
    else{
      return 'fa-ban';
    }
  };

  $scope.userHasAccess = function(resource){

    if(resource.locked){
      return "locked";
    }
    else if((!resource.ext_perms || resource.ext_perms === "") && (resource.owner.split('/')[3] != uid)){
      return "false";
    }
    else{
      return "true";
    }
  };

  $scope.entryLs = function(resource, entry){

    Restangular.all('resource/'+resource.id+'/'+entry.name +'list/').get('').then(function(objects){
        entry.entries = objects;
    },function(response){
      entry.expanded = false;
      $modal.open({
        controller: 'AlertCtrl',
        templateUrl: 'alert/alert.tpl.html',
        resolve: {
          title: function(){ return 'Error';},
          message: function(){ return 'Could not list the contents of '+entry.basename+' due to error:<br>' + response.data.description;}
        }
      });
      
    });
    
    
  };

  $scope.selectEntry = function(entry){
    $scope.selectedResource[index].selectedEntry = null;
    console.log('in select!');
    var parent = entry.name.substring(0,entry.name.indexOf(entry.basename));
    var dateModified = new Date(Math.ceil(entry.mtime*1000));
    console.log(dateModified);
    var size = entry.size/1024;
    if(size > 1000){
      size = (size/1024).toFixed(2) + " MB";
    }
    else{
      size = size.toFixed(2) + " KB";
    }
    var entryObj = {name:entry.basename,parent: parent,  size:size, dateModified: dateModified.toDateString()};
    $scope.selectedResource.selectedEntry = entryObj;
  };

  $scope.done = function(){
    $state.go('taskstart');
  };

  $scope.copy = function(srcIndex, $event){
    if(!$scope.isSingleDestSelected(srcIndex)){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    console.log($scope.selectedEntries[srcIndex]);
    
    var dstIndex = srcIndex === 1 ? 0: 1;
    var srcSelectedEntries = $scope.selectedEntries[srcIndex];
    var srcResource = $scope.selectedResource[srcIndex];
    var dstResource = $scope.selectedResource[dstIndex];

    var dstSelectedEntries = $scope.selectedEntries[dstIndex];
    var message;
    var error = false;
    var dstEntryNameResolve = null;
    
    var f;
    for(var k in dstSelectedEntries){
      f = dstSelectedEntries[k];
      break;
    }

    if(!dstResource){
      error = true;
      message = "Please select a destination resource";
    }
    
    else if(!f.isDir){
      error = true;
      message = "Destination location must be a folder";
    }
    //else if(e.mode != null && e.mode.indexOf('r') < 0 ){
    //  error = true;
    //  message = "You do not have permission to copy " + e.basename;
    //}
    else if(f.mode != null && f.mode.indexOf('w') < 0 ){
      error = true;
      message = "You do not have permission to copy to " + f.basename;
    }

    if(error){
      $modal.open({
        templateUrl: 'alert/alert.tpl.html',
        controller: 'AlertCtrl',
        resolve: {
          message: function(){return message;},
          title: function(){return 'Error';}
        }

      });

      return;
    }

    //console.log(srcSelectedEntries);
    //console.log(srcResource);
    //console.log(dstResource);
    

    
    //$scope.drop(e, srcResource, dstResource, f);
    //'srcResource', 'dstResource', 'selectedEntries', 'dstEntry'
    var modalInstance = $modal.open({
      templateUrl: 'taskcopydata/copyconfirm.tpl.html',
      controller: 'CopyConfirmCtrl',
      resolve: {
          srcResource: function(){return srcResource.name;},
          dstResource: function(){return dstResource.name;},
          selectedEntries: function(){return srcSelectedEntries;},
          dstEntry: function(){return f;}
      }

    });

    modalInstance.result.then(function (numSrcEntries) {
      var completeSrcPath = '', destinationPath;
      for(var k in srcSelectedEntries){
        completeSrcPath += 'srcPath='+encodeURIComponent(srcSelectedEntries[k].name)+'&';
      }
      completeSrcPath = completeSrcPath.substring(0,completeSrcPath.length-1);
      destinationPath = encodeURIComponent(f.name.substring(0,f.name.length-1));
      Restangular.all('resource/'+dstResource.id+'/'+destinationPath+'/copy/?srcResource='+srcResource.id+'&'+completeSrcPath).get('').then(function(response){
        var taskPath = response.taskPath;
        var taskId = taskPath.split('/')[3];
        $scope.$emit('SmTaskStart',Restangular.one('task',taskId+'/'),'Copy '+numSrcEntries + ' items to ' + (!f.basename ? dstResource.name : f.basename) , false);
      });
    });
    
    
  };

  $scope.dropMultipleItems = function(selectedEntries, srcResource, dstResource, dstEntry){

    var error;

    if(dstEntry.mode != null && dstEntry.mode.indexOf('w') < 0 ){
      error = true;
      message = "You do not have permission to copy to " + dstEntry.basename;
    }

    if(error){
      $modal.open({
        templateUrl: 'alert/alert.tpl.html',
        controller: 'AlertCtrl',
        resolve: {
          message: function(){return message;},
          title: function(){return 'Error';}
        }

      });

      return;
    }

    var modalInstance = $modal.open({
      templateUrl: 'taskcopydata/copyconfirm.tpl.html',
      controller: 'CopyConfirmCtrl',
      resolve: {
          srcResource: function(){return srcResource.name;},
          dstResource: function(){return dstResource.name;},
          selectedEntries: function(){return selectedEntries;},
          dstEntry: function(){return dstEntry;}
      }

    });

    modalInstance.result.then(function (numSrcEntries) {
      var completeSrcPath = '', destinationPath;
      for(var k in selectedEntries){
        completeSrcPath += 'srcPath='+encodeURIComponent(selectedEntries[k].name)+'&';
      }
      completeSrcPath = completeSrcPath.substring(0,completeSrcPath.length-1);
      destinationPath = encodeURIComponent(dstEntry.name.substring(0,dstEntry.name.length-1));
      Restangular.all('resource/'+dstResource.id+'/'+destinationPath+'/copy/?srcResource='+srcResource.id+'&'+completeSrcPath).get('').then(function(response){
        var taskPath = response.taskPath;
        var taskId = taskPath.split('/')[3];
        $scope.$emit('SmTaskStart',Restangular.one('task',taskId+'/'),'Copy '+numSrcEntries + ' items to ' + (!dstEntry.basename ? dstResource.name : dstEntry.basename) , false);
      });
    });
  };

  $scope.drop = function(srcEntry, srcResource, dstResource, dstEntry){
    var error, message;
    
    if(srcEntry.mode != null && srcEntry.mode.indexOf('r') < 0 ){
      error = true;
      message = "You do not have permission to copy " + srcEntry.basename;
    }
    else if(dstEntry.mode != null && dstEntry.mode.indexOf('w') < 0 ){
      error = true;
      message = "You do not have permission to copy to " + dstEntry.basename;
    }

    if(error){
      $modal.open({
        templateUrl: 'alert/alert.tpl.html',
        controller: 'AlertCtrl',
        resolve: {
          message: function(){return message;},
          title: function(){return 'Error';}
        }

      });
      $scope.$broadcast('sm-drop-success');
      return;
    }

    var modalInstance = $modal.open({
      templateUrl: 'taskcopydata/copyconfirm.tpl.html',
      controller: 'CopyConfirmCtrl',
      resolve: {
          srcResource: function(){return srcResource.name;},
          dstResource: function(){return dstResource.name;},
          selectedEntries: function(){return [srcEntry];},
          dstEntry: function(){return dstEntry;}
      }

    });

    modalInstance.result.then(function (status) {
      var userid = SmUser.getProperty('id');
      var copyTask = SmTask.getTaskInfo('copy');

      Restangular.configuration.fullResponse = true;

      
      taskApi.post({owner:'/scs/user/'+userid+'/', task_type:'/scs/task_type/'+copyTask.task_type_id+'/', status: "init", name: 'Copy '+srcEntry.basename + ' to ' + (!dstEntry.basename ? dstResource.name : dstEntry.basename),  parametersJson:'{ "input_ids":{"1": {"path": "'+srcEntry.name+'"}, "2": {"path": "'+dstEntry.name+srcEntry.basename +'"}}}'},null,{"Content-Type":"application/json"})
        .then(function(response){


          Restangular.configuration.fullResponse = false;
          var taskUri = response.headers('Location');
          var pathInfo = getLocation(taskUri);
 
          var resource;
          
          var p = taskresourceApi.post({task:taskUri, resource: '/scs/resource/'+srcResource.id+'/', resource_type_task_type: '/scs/resource_type_task_type/'+copyTask.src.id+'/'},null,{"Content-Type":"application/json"});

          p.then(function(){
            taskresourceApi.post({task:taskUri, resource: '/scs/resource/'+dstResource.id+'/', resource_type_task_type: '/scs/resource_type_task_type/'+copyTask.dest.id+'/'},null,{"Content-Type":"application/json"}).then(function(){
              var taskId = pathInfo.pathname.split('/')[3];

              taskApi.get(taskId+'/?action=start').then(function(){
                $scope.$emit('SmTaskStart',Restangular.one('task',taskId+'/'),'Copy '+srcEntry.basename + ' to ' + (!dstEntry.basename ? dstResource.name : dstEntry.basename) , false);
              }, function(error){
                    $modal.open({
                      controller: 'AlertCtrl',
                      templateUrl: 'alert/alert.tpl.html',
                      resolve: {
                        title: function(){ return 'Error';},
                        message: function(){return 'Error performing copy:<br>'+error.data;}
                      }
                    });
              });

            });
          });

        },function(){
          console.log('Error posting task');
      });

    }, function () {
          console.log('Transfer cancelled!');
    });


    $scope.$broadcast('sm-drop-success');

  };  

  $scope.refresh = function(selectedResource){
    SmContainerResource.refresh(selectedResource);  
  };

  $scope.download = function(selectedResource, $event){
    if(!selectedResource.selectedEntry || selectedResource.ext_perms == 'x'){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    SmContainerResource.download(selectedResource);
  };

  $scope.deleteSelection = function(selectedResource, $event){
    if(!selectedResource.selectedEntry || selectedResource.selectedEntry.isRoot){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    var promise = SmContainerResource.deleteSelection(selectedResource);
    if(selectedResource.selectedEntry.isDir){
      promise.then(function(taskInfo){

        if(taskInfo){
          $scope.$emit('SmTaskStart',taskInfo.taskObj, taskInfo.taskName, taskInfo.interactive);
        }
        else{
          console.log('Error when receiving task info from deleteSelection');
        }
        
      });
    }

  };

  $scope.mkdir = function(selectedResource, $event){
    if(selectedResource.selectedEntry && !selectedResource.selectedEntry.isDir){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    SmContainerResource.mkdir(selectedResource);
  };

  $scope.upload = function(selectedResource, $event){
    if(selectedResource.selectedEntry && !selectedResource.selectedEntry.isDir){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    SmContainerResource.upload(selectedResource);
  };

  $scope.filterRootEntries = function(searchText, index){
    var text = searchText.toLowerCase();

    if(!text || text === ''){
      $scope.entriesSearch = 'false';
    }
    else{
      $scope.entriesSearch = 'true';
    }

    $scope.selectedResource[index].entries = filterFilter(rootEntries[index],{name:text},false);
    $scope.selectedResource[index].selectedEntry = null;
  };

  $scope.refreshResources = function(index){
    resourceApi.get().then(function(response){
            

      if(angular.isNumber(selectedResourceIndex[index])){
        $scope.resources[index][selectedResourceIndex[index]].selected = false;
      }


      $scope.resources[index] = filterFilter(response.objects,{nature:"data"},false);

      $scope.resourceSelected[index] = 'false';
      
      $scope.selectedResource[index] = {entries:null};
      $scope.allEntriesSelected[index] = false;
      $scope.selectedEntries[index] = {};
      $scope.searchText[index] = '';
      
    });
  };

  $scope.isSingleSrcAndDestSelected = function(){
    return !singleSelectedFilter($scope.selectedEntries[0]) || !singleSelectedFilter($scope.selectedEntries[1]);
  };

  $scope.isSingleDestSelected = function(srcIndex){
    var destIndex = srcIndex === 0 ? 1 : 0;
    //console.log($scope.selectedEntries[destIndex]);
    return singleSelectedFilter($scope.selectedEntries[destIndex]);
  };

  $scope.rename = function(selectedResource, $event){
    if(!selectedResource.selectedEntry || selectedResource.selectedEntry.isRoot){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    SmContainerResource.rename(selectedResource);
  };

  $scope.filterResources = function(searchText, index){
    var text = searchText.toLowerCase();
    $scope.resources[index] = filterFilter(allResources[index],{name:text},false);
    //$scope.selectedResource[index].selectedEntry = null;
  };

  $scope.favouriteToggle = function(resource){
    if(resource.ext_favourite){
      Restangular.all('favourite_resource/').remove({owner__id:uid, resource__id: resource.id}).then(function(){
        resource.ext_favourite = false;
      });
    }
    else{
      Restangular.all('favourite_resource/').post({owner:'/scs/user/'+uid+'/', resource: '/scs/resource/'+resource.id+'/'}).then(function(){
        resource.ext_favourite = true;
      });

    }
  };

  $scope.openNotesEditor = function(resource){
    var exists = angular.isDefined(resource.ext_note) ? true : false;
    $modal.open({
      controller: 'ExtendedInfoCtrl',
      templateUrl: 'extendedinfo/extendedinfo.tpl.html',
      resolve: {
        tagsObject: function(){ return null;},
        noteObject: function(){ return {typeName: 'resource',typeId: resource.id, ownerId: uid, exists: exists, noteText: resource.ext_note };}
      }
    });
  };

  $scope.editFile = function(selectedResource, $event){
    if(!selectedResource.selectedEntry || selectedResource.selectedEntry.isDir){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    SmContainerResource.editFile(selectedResource);
  };

  $scope.zip = function(selectedResource,$event){
    if(selectedResource.selectedEntry && !selectedResource.selectedEntry.isDir){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    var p = SmContainerResource.zip(selectedResource);

    if(p){
      p.then(function(taskInfo){
        $scope.$emit('SmTaskStart',taskInfo.taskObj, taskInfo.taskName, taskInfo.interactive);
      }, function(response){
        $modal.open({
          controller: 'AlertCtrl',
          templateUrl: 'alert/alert.tpl.html',
          resolve: {
            title: function(){ return 'Error'; },
            message: function(){ return response.data.description; }
          }
        });
        console.log('promise rejected in zip');
      });
    }
    
  };

  $scope.unzip = function(selectedResource, $event){
    if(!selectedResource.selectedEntry || selectedResource.selectedEntry.isDir || selectedResource.selectedEntry.basename.indexOf('.zip') < 0){
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }
    var p = SmContainerResource.unzip(selectedResource);

    if(p){
      p.then(function(taskInfo){
        $scope.$emit('SmTaskStart',taskInfo.taskObj, taskInfo.taskName, taskInfo.interactive);
      },function(){
        console.log('promise rejected in unzip');
      });
    }
  };

  $scope.requestAccess = function(resource){
    var userPermMap, type='resource', val, res = angular.copy(resource);
    
    Restangular.one('resource',resource.id+'/getperm/').get().then(function(map){
      map = map.plain();
      userPermMap = map;
      openRequestAccessForm(resource, userPermMap);
    });
   
    
     
    
  };

  /*STANDARD FUNCTIONS*/

  var openRequestAccessForm = function( res, userPermMap){
    var userPerm = [{name:'read', hasPerm: false},{name:'write', hasPerm: false},{name:'execute', hasPerm: false}];
    if(userPermMap['/scs/user/'+uid+'/']){
      for( var i=userPermMap['/scs/user/'+uid+'/'].length-1;i>=0;i--){
        val = userPermMap['/scs/user/'+uid+'/'][i];
        if(val.indexOf('read') >= 0){
          userPerm[0].hasPerm = true;
        }
        else if(val.indexOf('write') >= 0){
          userPerm[1].hasPerm = true;
        }
        else if(val.indexOf('execute') >= 0){
          userPerm[2].hasPerm = true;
        }
      }
    }

    res.userPerm = userPerm;

    $modal.open({
      controller: 'AccessRequestCtrl',
      templateUrl: 'accessrequest/accessrequest.tpl.html',
      resolve:{
        type: function(){ return 'resource';},
        object: function(){ return res;}
      }
    });

  };

  var getLocation = function (href) {
    var location = document.createElement("a");
    location.href = href;
    // IE doesn't populate all link properties when setting .href with a relative URL,
    // however .href will return an absolute URL which then can be used on itself
    // to populate these additional fields.
    if (location.host === "") {
      location.href = location.href;
    }
    return location;
  };

  var refreshEntries = function(entries, parentName, parentObj, resourceid){
    var entry;
    for(var i = entries.length-1; i>=0 ; i--){
      entry = entries[i];
      if(entry.isDir && entry.entries){
        refreshEntries(entry.entries, parentName+entry.basename,entry,  resourceid);
      }
    }

    resourceApi.getList(resourceid+parentName+'list/').then(function(objects){
      //console.log(parentObj.entries);
      parentObj.entries = objects;
      //console.log(parentObj.entries);
      
    });

  };

  var initiateResources = function(){
    resourceApi.get().then(function(response){
      response.objects = filterFilter(response.objects,{nature:"data"},false);
      var arr1 = new Array(response.objects.length);
      var arr2 = new Array(response.objects.length);
      var obj, json;
      for(var i=response.objects.length-1; i>=0; i--){
        obj = response.objects[i];
        if(obj.personalFolder){
          json = JSON.parse(obj.capacityJson);
          obj.full = json.diskStateUsed === 'full';
          obj.warn = json.diskStateUsed === 'warn';
        }
        arr1[i] = angular.copy(response.objects[i]); arr1[i].smHashData = 0;
        arr2[i] = angular.copy(response.objects[i]); arr2[i].smHashData = 1;
      }
      allResources[0] = arr1;
      allResources[1] = arr2;
      $scope.resources[0] = arr1;
      $scope.resources[1] = arr2;
      //console.log($scope.resources);
    });
  };

  /***EXE***/
  initiateResources();

  $scope.$on('SmCommunityChange', function(event){
    community = SmUser.getProperty('community').name_key;
    resourceApi = Restangular.one('resource/?sortForUser='+uid+'&extForUser='+uid+'&communityFilter='+community+'&limit=0');
    
    $scope.resourceSelected = ['false', 'false'];
    $scope.selectedResource = [{entries:null}, {entries:null}];
    $scope.allEntriesSelected = [false,false];
    $scope.selectedEntries = [{},{}];
    $scope.searchText = ['',''];
    $scope.resourceSearchText = ['',''];
    $scope.entriesSearch = 'false';
    rootEntries = [[],[]];
    initiateResources();
  });
  
}])
.controller('CopyConfirmCtrl', ['$scope', '$modalInstance', 'srcResource', 'dstResource', 'selectedEntries', 'dstEntry','filterFilter',
  function($scope, $modalInstance, srcResource, dstResource, selectedEntries, dstEntry, filterFilter){
    
    /***VARIABLES***/
    /*Standard*/
    var entries;
    /*Scope*/
    $scope.newName = false;

    var i=0, key, arr=[];
    for(key in selectedEntries){
      arr.push(selectedEntries[key]);
      i++;
    }
    $scope.count = i;
    if(i === 1){
      $scope.srcEntryName = arr[0].basename;
      entries = filterFilter(dstEntry.entries,{basename: $scope.srcEntryName},true);

      if(entries && entries.length >0){
        $scope.newName = true;
      }
    }
    else{
      $scope.selectedEntries = arr;
    }


    $scope.srcResource = srcResource;
    $scope.dstResource = dstResource;
    $scope.dstEntryName = dstEntry.basename;
    $scope.ok = function(){
      $modalInstance.close(i);
    };

    $scope.cancel = function(){
      $modalInstance.dismiss('cancel');
    };

}]);

