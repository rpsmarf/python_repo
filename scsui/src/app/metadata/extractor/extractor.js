angular.module('rpsmarf.metadata.extractor', [
	'rpsmarf.config',
	'rpsmarf.user.service',
	'restangular'

])
.factory('RestFullResponse', function(Restangular){
	return Restangular.withConfig(function(RestangularConfigurer){
		RestangularConfigurer.setFullResponse(true);
	});
})
.directive('metadataextractor', function(){
	return {
		restrict: 'E',
		scope:{
			extractor: '=',
			extractorExists: '=',
			extractorApi: '=',
			updateExtractor: '&',
			repos: '='

		},
		//controllerAs: 'extractorCtrl',
		//bindToController: true,
		templateUrl:  'metadata/extractor/extractor.form.tpl.html',
		link: function($scope, $element, $attrs, controller){

			
			
			
		},
		controller: ['$scope','Restangular','SmUser','SmHelpers','RestFullResponse', '$modal','$state', function($scope,Restangular, SmUser, SmHelpers,RestFullResponse, $modal, $state){
			/***STANDARD VARIABLES***/
			var uid = SmUser.getProperty('id');
			

			/***SCOPE VARIABLES***/
			//$scope.extractorExists = false;
			$scope.extractorRepoSet = false;
			$scope.selected = {};
			$scope.extractorTypes = [];
			$scope.resources = [];
			$scope.schema = {hide: true};
			$scope.notification = { email: false};
			$scope.form = {current: null};
			$scope.extractorApi = {};

			/***SCOPE FUNCTIONS***/
			$scope.extractorTypeChanged = function(){
				var ttid = $scope.selected.extractorType.task_type.split('/')[3];
				$scope.schema.hide = true;
				Restangular.all('task_type/'+ttid+'/?extForUser='+uid+'&showComputeResources=true').get('').then(function(response){
					$scope.resources = response.compute_resources;
				});
			};

			$scope.toggleSchema = function(){
				if($scope.schema.hide){
					if(!$scope.selected.schema || !$scope.selected.schema.props){
						var schemaId = $scope.selected.repo.schema.split('/')[3];
						Restangular.all('md_property_type/?schema__id='+schemaId).get('').then(function(response){
							$scope.selected.schema = {props: response.objects};
						});
					}
					

					$scope.schema.hide = false;
				}
				else {
					$scope.schema.hide = true;
				}
				
			};

			/***STANDARD FUNCTIONS***/
			var extract = function(){
				var $form = $scope.form.current;
				var obj;
				$form.$submitted = false;

				if($form.$valid){
					obj =	{
							compute_resource: $scope.selected.resource.resource_uri || '/scs/resource/'+$scope.selected.resource.id+'/',
							extractor_type: $scope.selected.extractorType.resource_uri,
							repo: $scope.selected.repo.resource_uri,
							emailWhenDone: $scope.notification.email

							};
					
						
					return RestFullResponse.all('md_extractor/').post(obj).then(function(response){
						var extractorId = SmHelpers.getObjectIdFromPathName(response.headers('Location'));

						return Restangular.one('md_extractor', extractorId+'/rebuild/').get().then(function(response){
							$scope.extractorExists = true;
							if(!$state.is('extractionlist')){
								$modal.open({
									templateUrl: 'alert/alert.tpl.html',
									controller: 'AlertCtrl',
									resolve: {
										title: function(){ return 'Extraction started';},
										message: function(){ return 'The extraction for this repo has been created and started. You can view the status in the <a href ui-sref="extractionlist">extractions page</a>.';}
									}
								});
							}
							return {id: extractorId};
							
						});
					});
					
				}
				else{
					$form.$submitted = true;
				}
			};

			var remove = function(){
				var modalInstance = $modal.open({
					templateUrl: 'confirm/confirm.tpl.html',
					controller: 'ConfirmCtrl',
					resolve: {
						title: function(){return 'Confirm Deletion'; },
						body: function(){return 'Delete this extraction and all it\'s results? (Cannot be undone)';}
					}
				});

				modalInstance.result.then(function(){
					
					Restangular.one('md_extractor',$scope.extractor.id+'/').remove().then(function(){
						console.log('extractor deleted');
						//$scope.extractor = null;
						//$scope.extractorExists = false;
						resetScopeVariables();
						if($scope.updateExtractor){
							$scope.updateExtractor();
						}
					});
				}, function(){

				});
			};

			
			var normalizeResourceObject = function(r){
				$scope.selected.resource = {id: r.id, description: r.description, locked: r.locked, status: r.status, name: r.name, resource_uri: '/scs/resource/'+r.id+'/'};
			};

			var update = function(){
				Restangular.one('md_extractor', $scope.extractor.id+'/update/').get().then(function(response){
					$modal.open({
						templateUrl: 'alert/alert.tpl.html',
						controller: 'AlertCtrl',
						resolve: {
							title: function(){ return 'Update started';},
							message: function(){ return 'The extraction for this repo is being updated. You can view the status in the <a href ui-sref="extractionlist">extractions page</a>.';}
						}
					});
				});
			};

			var rebuild = function(){

				var modalInstance = $modal.open({
					templateUrl: 'confirm/confirm.tpl.html',
					controller: 'ConfirmCtrl',
					resolve: {
						title: function(){return 'Confirm Rebuild';},
						body: function(){return 'Rebuilding will delete any existing metadata (cannot be undone) for this repo and will run the configured extraction. Do you want to continue?';}
					}
				});

				return modalInstance.result.then(function(){
					var $form = $scope.form.current;
					var obj;
					$form.$submitted = false;

					if($form.$valid){

						obj =	{
								compute_resource: $scope.selected.resource.resource_uri || '/scs/resource/'+$scope.selected.resource.id+'/',
								extractor_type: $scope.selected.extractorType.resource_uri,
								repo: $scope.selected.repo.resource_uri,
								emailWhenDone: $scope.notification.email

								};

						return Restangular.one('md_extractor', $scope.extractor.id+'/').patch(obj).then(function(){
							return Restangular.one('md_extractor', $scope.extractor.id+'/rebuild/').get().then(function(response){
								if(!$state.is('extractionlist')){
									$modal.open({
										templateUrl: 'alert/alert.tpl.html',
										controller: 'AlertCtrl',
										resolve: {
											title: function(){ return 'Rebuild started';},
											message: function(){ return 'The extraction for this repo is being rebuilt. You can view the status in the <a href ui-sref="extractionlist">extractions page</a>.';}
										}
									});

									if($scope.updateExtractor){
										$scope.updateExtractor();
									}
								}
								return {rebuild: true};
							});
						});
					}
					else{
						$form.$submitted = true;
						return {rebuild: false};
					}
				});
					
			};

			var cancel = function(){
				var modalInstance = $modal.open({
					templateUrl: 'confirm/confirm.tpl.html',
					controller: 'ConfirmCtrl',
					resolve: {
						title: function(){return 'Confirm Cancellation';},
						body: function(){return 'Cancel this extraction process?';}
					}
				});

				modalInstance.result.then(function(){
					Restangular.all('md_repo/'+$scope.selected.repo.id+'/?cancel').get('').then(function(response){
						console.log('extraction cancelled');
					});
				});
			};

			var resetScopeVariables = function(){
				$scope.selected = {};
				$scope.schema = {hide: true};
				$scope.notification = { email: false};
				$scope.form.current.$submitted = false;
			};

			/***EXE***/
			$scope.extractorApi.startExtraction = extract;
			$scope.extractorApi.deleteExtractor = remove;
			$scope.extractorApi.rebuildExtractor = rebuild;
			$scope.extractorApi.updateExtractor =  update;
			$scope.extractorApi.cancelExtractor = cancel;

			$scope.$watch('extractor', function(extractor, oldVal){
				var ttid;
				if(extractor){
					

					if($scope.extractorExists){
						switch(extractor.state){
							case 'purging':
							case 'scanning':
							case 'extracting':
							case 'updating':
							case 'cancelling':
								$scope.form.current.readonly = true;
								break;
							default:
								$scope.form.current.readonly = false;
						}
						$scope.selected.repo = extractor.repo;
						$scope.extractorRepoSet = true;

						
						normalizeResourceObject(extractor.compute_resource);
						//$scope.selected.resource = extractor.compute_resource;
						$scope.selected.extractorType = extractor.extractor_type;
						$scope.notification.email = extractor.emailWhenDone;
						ttid = extractor.extractor_type.task_type.split('/')[3];
						Restangular.all('task_type/'+ttid+'/?extForUser='+uid+'&showComputeResources=true').get('').then(function(response){
							$scope.resources = response.compute_resources;
						});
						
					}
					else{
						resetScopeVariables();
						//$scope.extractorExists = false;
						if(extractor.repo){
							$scope.selected.repo = extractor.repo;
							$scope.extractorRepoSet = true;
						}
					}


				}
				
			});

			if(!$scope.repos){
				Restangular.all('md_repo/').get('').then(function(response){
					$scope.repos = response.objects;
				});
			}			

			Restangular.all('md_extractor_type/').get('').then(function(response){
				$scope.extractorTypes = response.objects;
			});
			

		}]
	};

});
//.controller('MetadataExtractorCtrl', ['$scope', function($scope){

	

	//$scope.$watch I suppose?

//}]);