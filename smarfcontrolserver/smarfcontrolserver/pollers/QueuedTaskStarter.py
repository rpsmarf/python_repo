'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Dec 8, 2014

    @author: rpsmarf
'''
import logging
import threading
import time
from sm_controller.models import SmModelTask
from smcommon.utils.date_utils import sm_date_now
from django import db
from sm_controller.api_task import scsApiTaskStartTask
from django.db.models.query_utils import Q

logger = logging.getLogger(__name__)


class ScsQueuedTaskStarterThread(threading.Thread):
    
    def __init__(self, ageOutObject):
        threading.Thread.__init__(self, name="QueuedTaskStarter")
        self.ageOutObject = ageOutObject

    def run(self):
        while not self.ageOutObject.quit:
            try:
                db.close_connection()
                time.sleep(self.ageOutObject.pollTimeInSec)
                self.ageOutObject.doOneCheck()
            except:
                logger.exception("Error during deferred task staring")
                time.sleep(10)
            

class ScsQueuedTaskStarter(object):
    '''
    This class implements a timer to purge files which have been abandoned
    in any resource's .rpsmarf/tmp folder
    '''

    def __init__(self, pollTimeInSec):
        '''
        Constructor
        '''
        self.pollTimeInSec = pollTimeInSec
        self.thread = None
        self.quit = False
        self.checkCount = 0
        
    def doOneCheck(self):
        # busyResources is a set of resource/user pairs which are know
        # to be "busy".  We do this so we can skip through multiple tasks queued 
        # for a single resource very quickly.
        busyResources = set()
        self.checkCount += 1
        taskList = SmModelTask.objects.filter(Q(state="queued") | Q(state="booting")).order_by('-id')
        now = sm_date_now()
        for task in taskList:
            logger.debug("Checking for tasks to run - considering task %d",
                         task.id)
            if task.start_time > now:
                logger.debug("Skipping task %d because its start time is in the future", task.id)
                continue
            checkTag = str(task.computeResource.id) + "/" + str(task.owner.id)
            if checkTag in busyResources:
                logger.debug("Resource already known to be unusable for this owner")
            else:
                busyResources.add(checkTag)
                try:
                    logger.info("Starting task %d", task.id)
                    scsApiTaskStartTask(task, None, True)
                except Exception:
                    logger.warning("Exception starting deferred task %d", task.id, exc_info=True)
                    
    def start(self):
        logger.info("Starting QueuedTaskStarter task with poll time %f seconds", self.pollTimeInSec)
        self.thread = ScsQueuedTaskStarterThread(self)
        self.thread.start()
        
    def stop(self):
        ''' 
        This is only used during unit tests which have a small poll time
        so the "join" is OK
        '''
        logger.info("Stopping QueuedTaskStarter task")
        self.quit = True
        tmp = self.thread.join(2 * self.pollTimeInSec)
        logger.info("Stopped QueuedTaskStarter task")
        return tmp
        