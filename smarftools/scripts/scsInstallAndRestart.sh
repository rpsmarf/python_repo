#!/bin/bash

echo "Updating version list from repo..."
sudo apt-get update -o Dir::Etc::sourcelist="sources.list.d/sw.rpsmarf.list"     -o Dir::Etc::sourceparts="-" -o APT::Get::List-Cleanup="0"
echo "Deleting logs..."
sudo rm -f /var/log/smarf-scs/*
echo "Installing SCS..."
sudo apt-get -y --force-yes install smarf-scs
scsRestart.sh

