'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Aug 13, 2015

    @author: rpsmarf
'''

import logging
from sm_controller.models import SmModelPathList, SmModelScript,\
    SmModelMdExtractor
import sm_controller.api
from threading import Thread
import threading 
from smarfcontrolserver.init.envvar import scsGetEnvVarInt,\
    SCS_ENV_VAR_NUM_FILES_PER_JOB, SCS_ENV_VAR_EXTRACT_DONE_TIMEOUT,\
    scsGetEnvVarFloat, scsGetEnvVar, SCS_ENV_VAR_EXTRACT_POST_URL_BASE,\
    SCS_ENV_VAR_STDOUT_LIMIT_IN_DB, SCS_ENV_VAR_STDERR_LIMIT_IN_DB
from smcommon.utils.date_utils import sm_date_now
from smcommon.tasks.task_desc import SmTaskDesc
import sm_controller.api_task
import json
from queue import Queue, Empty
from smcommon.task_runners.task_runner import TASK_STATE_SUCCESS
from sm_controller.api_base import smRaiseFsOpException, scsMakeExecInfo
from smarfcontrolserver.utils.mailer import scsGetGlobalMailer,\
    EXTRACTION_FAILURE_TEMPLATE, EXTRACTION_FAILURE_SUBJECT,\
    EXTRACTION_SUCCESSFUL_TEMPLATE, EXTRACTION_SUCCESSFUL_SUBJECT
from tastypie.models import ApiKey


logger = logging.getLogger(__name__)


SCS_API_EXTRACTION_STATE_EXTRACTING = "extracting"
SCS_API_EXTRACTION_STATE_RUNNING = "running"
SCS_API_EXTRACTION_STATE_DONE = "done"
SCS_API_EXTRACTION_STATE_INIT = "init"
SCS_API_EXTRACTION_STATE_SCANNING = "scanning"
SCS_API_EXTRACTION_STATE_PURGING = "purging"
SCS_API_EXTRACTION_STATE_CANCELLING = "cancelling"

SCS_API_EXTRACTION_RESULT_OK = "ok"
SCS_API_EXTRACTION_RESULT_FAILED = "failed"
SCS_API_EXTRACTION_RESULT_RUNNING = "running"
SCS_API_EXTRACTION_RESULT_CANCELLED = "cancelled"


def scsExtractorMarkRunning(extractor):
    extractor.last_extract_result = SCS_API_EXTRACTION_RESULT_RUNNING
    extractor.last_extract_start = sm_date_now()
    extractor.last_extract_end = None
    extractor.last_extract_warnings = ""
    extractor.last_extract_errors = ""
    extractor.stdout = ""
    extractor.stderr = ""
    extractor.state = SCS_API_EXTRACTION_STATE_RUNNING
    extractor.save()


def scsExtractorSendDoneEmail(extractor, failure):
    if not extractor.emailWhenDone:
        logger.debug("Not sending email due to emailWhenDone flag")
        return
    
    seconds = sm_date_now() - extractor.last_extract_start
    m, s = divmod(int(seconds.total_seconds()), 60)
    h, m = divmod(m, 60)
    d, h = divmod(h, 24)
    duration_string = str(s) + " seconds"
    if m > 0:
        duration_string = str(m) + " minutes, " + duration_string
    if h > 0:
        duration_string = str(m) + " hours, " + duration_string
    if d > 0:
        duration_string = str(m) + " days, " + duration_string
        
    params = {"%repoName%": extractor.repo.name,
              "%failureReasons%": extractor.last_extract_errors,
              "%duration%": duration_string
              }
    mailer = scsGetGlobalMailer()
    if mailer is not None:
        if failure:
            template = EXTRACTION_FAILURE_TEMPLATE
            subject = EXTRACTION_FAILURE_SUBJECT
        else:
            template = EXTRACTION_SUCCESSFUL_TEMPLATE
            subject = EXTRACTION_SUCCESSFUL_SUBJECT
            
        emailBody = mailer.merge(template, params)
        destEmail = (extractor.owner.first_name + " " 
                     + extractor.owner.last_name 
                     + " <" + extractor.owner.email + ">")
        mailer.sendEmail(destEmail, subject, emailBody)


def scsExtractorMarkExtractionDone(extractor, cancelled=False):
    if extractor.state == SCS_API_EXTRACTION_STATE_DONE:
        return
    
    logger.debug("Marking extraction %d as done", extractor.id)
    extractor.state = SCS_API_EXTRACTION_STATE_DONE
    extractor.last_extract_end = sm_date_now()
    if cancelled:
        extractor.last_extract_result = SCS_API_EXTRACTION_RESULT_CANCELLED
    elif len(extractor.last_extract_errors) > 0:
        extractor.last_extract_result = SCS_API_EXTRACTION_RESULT_FAILED
        scsExtractorSendDoneEmail(extractor, True)
    else:
        extractor.last_extract_result = SCS_API_EXTRACTION_RESULT_OK
        scsExtractorSendDoneEmail(extractor, False)
    extractor.save()


class ScsApiExtractorAsyncResp(object):
    
    def _getExtractorThread(self, taskId):
        # Get the extractor id
        extractor_id_str = taskId.split("-")[1]
        if extractor_id_str not in scsApiExtractorDict:
            logger.warning("Done discarded because extractor thread id %s is not valid",
                           extractor_id_str)
            return None
        
        return scsApiExtractorDict[extractor_id_str]
            
    def done(self, taskId, status, respJson):
        '''
        This method is invoked when a task associated with an extraction 
        completes.
        '''
        extractor_thread = self._getExtractorThread(taskId)
        if extractor_thread is not None:
            resp = {"type": "done",
                    "taskId": taskId,
                    "status": status,
                    "respJson": respJson}
            extractor_thread.done_queue.put(resp)
    
    def taskOutput(self, taskId, outputType, outputData):
        '''
        This method is invoked when a task associated with an extraction 
        completes.
        '''
        extractor_thread = self._getExtractorThread(taskId)
        if extractor_thread is not None:
            resp = {"type": "taskOutput",
                    "taskId": taskId,
                    "outputType": outputType,
                    "outputData": outputData}
            extractor_thread.done_queue.put(resp)
    
    def progress(self, taskId, progressJson):
        pass
    
scsApiExtractorDict = {}

MAX_ERRORS = 10
MAX_WARNINGS = 100

sm_controller.api_base.scsApiExtractorCallbackSet(ScsApiExtractorAsyncResp())


def scsApiExtractErrorAdd(extractor, message):
    '''
    Add a line to the error list for the extractor if it is not already 
    full of errors
    '''
    errors = extractor.last_extract_errors
    error_count = len(errors.split('\n'))
    if error_count > MAX_ERRORS:
        return
    
    if error_count == MAX_ERRORS:
        extractor.last_extract_errors += "Some error messages suppressed\n"
    else:
        extractor.last_extract_errors += message + '\n' 
    extractor.save()
    
    
def scsApiExtractWarningAdd(extractor, message):
    '''
    Add a line to the warnings list for the extractor if it is not already 
    full of warnings
    '''
    warnings = extractor.last_extract_warnings
    warning_count = len(warnings.split('\n'))
    if warning_count > MAX_WARNINGS:
        return
    
    if warning_count == MAX_WARNINGS:
        extractor.last_extract_warnings += "Some warning messages suppressed\n"
    else:
        extractor.last_extract_warnings += message + '\n' 
    extractor.save()    


class ScsApiExtractorThread(Thread):
    '''
    This class contains the main thread for processing a request to do
    an extraction. 
    
    Initially we are starting a thread for each running extraction.
    
    If more scalability is required then we would likely move to distributed
    a queue model with multiple workers pulling jobs out of the queue.
    '''
    
    def __init__(self, op, extractor):
        threading.Thread.__init__(self, name="ScsApiExtractorThread")
        self.op = op
        self.extractor = extractor
        self.NUM_RECORDS_PER_JOB = scsGetEnvVarInt(SCS_ENV_VAR_NUM_FILES_PER_JOB)
        self.EXTRACT_DONE_TIMEOUT = scsGetEnvVarFloat(SCS_ENV_VAR_EXTRACT_DONE_TIMEOUT)
        logger.debug("Extracting %d records with response timeout of %f seconds", 
                     self.NUM_RECORDS_PER_JOB, self.EXTRACT_DONE_TIMEOUT)
        scsApiExtractorDict[str(extractor.id)] = self
        self.done_queue = Queue()
        self.daemon = True
        
    def _addPaths(self, resource, q, name_list, path_object_list):
        # Bulk add to DB
        if path_object_list is not None:
            SmModelPathList.objects.bulk_create(path_object_list)
        job = {}
        job["name_list"] = name_list
        job["resource"] = resource
        q.append(job)
        logger.debug("Queuing job with %d files", len(name_list))

    def scanResource(self, extractor, resource, q, progressDict):
        '''
        Scans a resource looking for files which need processing
        '''
        fileAcc = None
        try:
            fileAcc = sm_controller.api.SmApiResource.makeFileAccFromResource(resource)
            fileAcc.connectSession()
            flist = fileAcc.list("/", 100, 1000000, None, False)
            logger.debug("Scan found %d files", len(flist))
            count = 0
            name_list = []
            path_object_list = []
            for f in flist:
                if not f["isDir"]:
                    name_list.append(f["name"])
                    path_object_list.append(SmModelPathList(repo=extractor.repo,
                                                            resource=resource,
                                                            relative_path=f["name"]))
                    count += 1
                    if count == self.NUM_RECORDS_PER_JOB:
                        self._addPaths(resource, q, name_list, path_object_list)
                        progressDict["files_to_process"] += len(name_list)
                        name_list = []
                        path_object_list = []
                        count = 0
                        
            if count > 0:
                self._addPaths(resource, q, name_list, path_object_list)
                progressDict["files_to_process"] += len(name_list)

        except Exception as e:
            logger.exception("Error during scan")
            smRaiseFsOpException(e, "extraction_scan", "")
        finally:
            if fileAcc:
                fileAcc.disconnectSession()
    
    def queuePathList(self, extractor, resource, q, progressDict):
        '''
        Scans a resource looking for files which need processing
        '''
        query = SmModelPathList.objects.filter(state="created").\
            filter(repo=self.extractor.repo).filter(resource=resource)
        count = 0
        name_list = []
        for pathObj in query:
            name_list.append(pathObj.relative_path)
            count += 1
            if count == self.NUM_RECORDS_PER_JOB:
                self._addPaths(resource, q, name_list, None)
                if progressDict is not None:
                    progressDict["files_to_process"] += len(name_list)
                count = 0
            
        # Cleanup the trailing records        
        if count > 0:
            self._addPaths(resource, q, name_list, None)
            if progressDict is not None:
                progressDict["files_to_process"] += len(name_list)
    
    def _makeUploadUrl(self, resource, user):
        return (scsGetEnvVar(SCS_ENV_VAR_EXTRACT_POST_URL_BASE) + 
                "/scs/bulkupload/?repo=" + str(self.extractor.repo.id) +
                "&resource=" + str(resource.id) +
                "&api_key=" + ApiKey.objects.get(user_id=user).key)

    def _addTaskOutput(self, extractor, result):
        outputType = result["outputType"]
        outputData = result["outputData"]
        if outputType == "stdout":
            maxLen = scsGetEnvVarInt(SCS_ENV_VAR_STDOUT_LIMIT_IN_DB)
        else:
            maxLen = scsGetEnvVarInt(SCS_ENV_VAR_STDERR_LIMIT_IN_DB)
        s = getattr(extractor, outputType) + outputData
        if len(s) > maxLen:
            s = s[-maxLen:]
        
        setattr(extractor, outputType, s)
        extractor.save()
    
    def processJob(self, job):
        '''
        This method processes one "job" of extracting metadata.
        This requires sending the list of URLs to the compute resource 
        along with the URL to use.
        '''
        logger.debug("Processing job with %d files ", len(job["name_list"]))
        task_type = self.extractor.extractor_type.task_type
        ttConfigDict = json.loads(task_type.configurationJson)
        compute_resource = self.extractor.compute_resource
        data_resource = job["resource"]
        
        # Create task descriptor
        taskDesc = SmTaskDesc(task_type.code_module, task_type.code_classname)
        argList = []  # Eventual list of arguments
        # Iterate through the arguments defined in the task type configuration string
        # to build the task descriptor arguments
        argValues = ["unused", "unused", 
                     self._makeUploadUrl(job["resource"], self.extractor.owner), # input id 2
                     json.loads(data_resource.parametersJson)["folder"]] # input id 3

        for argInfoFromTt in ttConfigDict.get("args"):
            input_id = argInfoFromTt.get("input_id")
            # If this argument has an "input_id" then it is specified by a value
            # from the task object (e.g. it is specific to this task instance)
            if input_id is None:
                # Plug in constant value from tasktype.configurationJson field                    
                argType = argInfoFromTt["type"]
                if argType == 'script':
                    # Get script from script table
                    argValue = SmModelScript.objects.get(key=argInfoFromTt["value"]).value
                else:
                    argValue = argInfoFromTt["value"]
                argList.append({"type": argType, "value": argValue})
            else:
                # If the argument type is resource then we write a record
                # about this resource into the argList.  It will be resolved
                # to a file/folder path at the remote agent to allow input
                # or output copying to happen
                if argInfoFromTt["type"] == "resource":
                    dad = sm_controller.api_task.scsApiTaskMakeDad(data_resource, 
                                                                   job["name_list"])
                    
                    if "modifier" in argInfoFromTt:
                        dad.modifier = argInfoFromTt["modifier"]
                    arg = {"type": "dad_file", 
                           "direction": "input", 
                           "dad": dad.toDict()}
                    argList.append(arg)
                elif argInfoFromTt["type"] == "string":
                    argList.append({"type": "string", "value": argValues[input_id]})                    
                else:
                    raise Exception("Unexpected type " + argInfoFromTt["type"])

        taskDesc.addTaskParam("args", argList)
        execInfo = scsMakeExecInfo(compute_resource)
        if "workingDir" in ttConfigDict:
            wDirDict = ttConfigDict["workingDir"]
            execInfo["workingDir"] = {"type": "string", "value": wDirDict["value"]}
        taskDesc.addTaskParam("execInfo", execInfo)           

        # Start task - note that 'done' indication comes back through a 
        # callback to scsExtractorDone()
        logger.debug("Starting extraction task on remote agent")
        try:
            sm_controller.api_task.scsApiTaskRunTaskOnRemoteAgent("extractor-" + str(self.extractor.id), 
                                                                  taskDesc, compute_resource)
            # Wait until done is received or timeout has passed
            try:
                while True:
                    result = self.done_queue.get(True, self.EXTRACT_DONE_TIMEOUT)
                    result_type = result["type"]
                    if result_type == "done":
                        logger.debug("Extraction task completed on remote agent")
                        status = result["status"]
                        if status != TASK_STATE_SUCCESS:
                            scsApiExtractErrorAdd(self.extractor, 
                                                  "Error processing files: " + result["respJson"])
                        break
                    elif result_type == "taskOutput":
                        self._addTaskOutput(self.extractor, result)
                    
            except Empty:
                logger.warning("Timeout on wait for 'extraction done'")
                scsApiExtractErrorAdd(self.extractor, 
                                      "Timeout extracting data from files.  First file in list is " +
                                      job["name_list"][0] + " number of files: " + str(len(job["name_list"])))
        except Exception as e:
            logger.exception("Error on starting task")
            scsApiExtractErrorAdd(self.extractor, 
                                  "Error in starting task: " + str(e))
            
    def run(self):
        '''
        This is started by a rebuild or update command on an extractor.
        It does:
        - deleting any data which needs to go
        - scanning the list of files and adding records to the queue
        - processing the records in the queue to load 
        '''
        try:
            progressDict = {"files_to_process": 0, "files_processed": 0}
            self.extractor.progressJson = json.dumps(progressDict)
            q = []    
            if self.op == "rebuild":
                # Set state to purging
                self.extractor.state = SCS_API_EXTRACTION_STATE_PURGING
                self.extractor.save()
                
                # Delete all pre-existing data (deleting paths will delete property records)
                logger.debug("Purge all records for repo %d", self.extractor.repo.id)
                SmModelPathList.objects.filter(repo=self.extractor.repo).delete()
        
                # Scan each resource adding records to the path table and to the queue
                self.extractor.state = SCS_API_EXTRACTION_STATE_SCANNING
                self.extractor.save()
                for resource in self.extractor.repo.resources.all():
                    self.scanResource(self.extractor, resource, q, progressDict)
            elif self.op == "requeue":
                logger.debug("Requeueing for extractor %d for path records in created state", self.extractor.repo.id)
                self.extractor.state = SCS_API_EXTRACTION_STATE_SCANNING
                self.extractor.save()
                for resource in self.extractor.repo.resources.all():
                    self.queuePathList(self.extractor, resource, q, progressDict)
                    
            # Refresh extractor object in case it was cancelled
            self.extractor = SmModelMdExtractor.objects.get(id=self.extractor.id)
            self.extractor.progressJson = json.dumps(progressDict)
            self.extractor.state = SCS_API_EXTRACTION_STATE_EXTRACTING
            self.extractor.save()
            for job in q:
                if (self.extractor.state == SCS_API_EXTRACTION_STATE_DONE 
                        or self.extractor.state == SCS_API_EXTRACTION_STATE_CANCELLING):
                    break
                self.processJob(job)
                progressDict["files_processed"] += len(job["name_list"])
                self.extractor = SmModelMdExtractor.objects.get(id=self.extractor.id)
                self.extractor.progressJson = json.dumps(progressDict)
                self.extractor.save()
                
            # Get count of metadata-free objects
            path_list = SmModelPathList.objects.filter(repo=self.extractor.repo).filter(state="created")
            if path_list.count() > 0:
                # Throw away any errors
                self.extractor.last_extract_errors = ''
                self.extractor.save()
                # Reprocess any records with no metadata 
                q = []
                for resource in self.extractor.repo.resources.all():
                    self.queuePathList(self.extractor, resource, q, None)
                for job in q:
                    logger.debug("Reprocessing job of %d records", len(job["name_list"]))
                    self.processJob(job)
                path_list = SmModelPathList.objects.filter(repo=self.extractor.repo).filter(state="created")
            progressDict["files_with_no_metadata_at_done"] = path_list.count()

            self.extractor = SmModelMdExtractor.objects.get(id=self.extractor.id)
            self.extractor.progressJson = json.dumps(progressDict)
            scsExtractorMarkExtractionDone(self.extractor,
                                           cancelled=(self.extractor.state == SCS_API_EXTRACTION_STATE_CANCELLING))
            del scsApiExtractorDict[str(self.extractor.id)]
        except Exception as e:
            logger.exception("Error in extraction processing")
            scsApiExtractErrorAdd(self.extractor, str(e))        
            scsExtractorMarkExtractionDone(self.extractor)
            
            
def scsApiExtractRestartExtract(extractor):
    '''
    This method is invoked to restart an extractor - typically after the 
    SCS has rebooted.
    '''
    # If the extractor is in "done" or "init" state then we do nothing and return false
    # to indicate that we are doing nothing
    if extractor.state in [SCS_API_EXTRACTION_STATE_DONE, SCS_API_EXTRACTION_STATE_INIT]:
        return False
    
    # In these states we just restart the whole extraction
    if extractor.state in [SCS_API_EXTRACTION_STATE_RUNNING, 
                           SCS_API_EXTRACTION_STATE_SCANNING,
                           SCS_API_EXTRACTION_STATE_PURGING]:
        thread = sm_controller.api_extract.ScsApiExtractorThread("rebuild", extractor)
        thread.start()
    elif extractor.state == SCS_API_EXTRACTION_STATE_EXTRACTING:
        thread = sm_controller.api_extract.ScsApiExtractorThread("requeue", extractor)
        thread.start()
    else:
        logger.error("Unexpect state %s", extractor.state)
        
    return True


def scsApiExtractRestartAll():
    logger.debug("Checking for extractors to restart")
    for extractor in SmModelMdExtractor.objects.all():
        if scsApiExtractRestartExtract(extractor):
            logger.info("Extractor %d restarted", extractor.id)
    logger.debug("Done checking extractors to restart")
