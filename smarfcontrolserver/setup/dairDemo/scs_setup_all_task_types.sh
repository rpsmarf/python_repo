#!/bin/bash

python scs_agent_container_setup.py

for i in scs_task_type*; do
    python $i
done

python scs_repo_setup.py
python scs_extractor_setup.py

python scs_cloud_storage_aws_setup.py
python scs_cloud_storage_dair_setup.py

scsRestart.sh

echo "Waiting for remote agent to be declared up..."
sleep 15

python scs_extract_run.py
