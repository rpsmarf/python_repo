This demo contains a version of the game Asteroids which is stored in
git (at bitbucket.org) and has multiple versions.

The repo must be cloned from BitBucket by doing:

git clone https://mcgregorandrew@bitbucket.org/mcgregorandrew/asteroids_git_demo.git

Then the game can be run by doing:

wish asteroid.tk
