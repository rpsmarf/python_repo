angular.module('rpsmarf.alert', [
	'ngSanitize',
	'ui.bootstrap'
]).
controller('AlertCtrl', ['$scope', 'message', 'title', '$modalInstance',
	function($scope, message, title, $modalInstance){
		/***VARIABLES***/
		/*Scope*/
		$scope.message = message;
		$scope.title = title ? title : 'Notice';

		/***FUNCTIONS***/
		$scope.ok = function(){
			$modalInstance.close('ok');
		};

	}
]);