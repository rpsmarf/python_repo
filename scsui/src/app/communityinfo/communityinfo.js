/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'rpsmarf.communityinfo', [
  'restangular',
  'ui.router',
  'ui.bootstrap',
  'rpsmarf.resourceconfig',
  'rpsmarf.taskstart',
  'rpsmarf.task.create.service',
  'rpsmarf.user.service',
  'rpsmarf.extendedinfo'
  
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".

 $stateProvider.state('toState', {
  templateUrl:'wokka.html',
  controller:'stateController',
  params: ['referer', 'param2', 'etc']
});
 */
.config(function config( $stateProvider, RestangularProvider ) {
  $stateProvider.state( 'communityinfo', {
    url: '/communityinfo/:communityNameKey/',
    views: {
      "main": {
        controller: 'CommunityinfoCtrl',
        templateUrl: 'communityinfo/communityinfo.tpl.html'
      }
    },
    data:{ pageTitle: 'Community Information' }
  });
})
/*
 * And of course we define a controller for our route.
 */
.controller( 'CommunityinfoCtrl', ['$scope', 'Restangular', '$state', 'SmTaskCreate', 'filterFilter', 'SmUser', '$modal', '$stateParams',
  function ( $scope, Restangular, $state, SmTaskCreate, filterFilter, SmUser, $modal, $stateParams) {

  /***PRE-INIT EXE***/
  if(!$stateParams.communityNameKey || $stateParams.communityNameKey === ''){
    console.log('stateParams!');
    console.log($stateParams);
    $state.go('communities');
    return;
  }
  /***VARIABLES***/
  /*Scope*/
  $scope.community = null;
  $scope.members = [];
  $scope.tooltasks = [];//[{name:"SPPLASH", summary:"Bridge Vibration Analysis Tool"},{name:"WSAT", summary:"Bridge behaviour modeling"},{name:"WSAT", summary:"Bridge behaviour modeling"},{name:"WSAT", summary:"Bridge behaviour modeling"},{name:"WSAT", summary:"Bridge behaviour modeling"},{name:"WSAT", summary:"Bridge behaviour modeling"},{name:"WSAT", summary:"Bridge behaviour modeling"},{name:"WSAT", summary:"Bridge behaviour modeling"}];
  $scope.resources = [];
  $scope.newsitems = [];
  $scope.FAQs =  [];
  $scope.userIsOwner = false;
  
  var communityNameKey = $stateParams.communityNameKey;
  var usercommunity = SmUser.getProperty('community');
  var userid = SmUser.getProperty('id');
  var useremail = SmUser.getProperty('email');
  var userfirstname = SmUser.getProperty('firstname');
  var userlastname = SmUser.getProperty('lastname');
  var community_id = null; //$scope.community.id;
  var community_name_key = null; //$scope.community.name_key;
  var members = null; //Restangular.one('user_setting/?community__name_key=' + community_name_key);
  var resources = null; //Restangular.one('resource/?communityFilter=' + community_name_key);
  var tasktypes = null; //Restangular.one('task_type/?communityFilter=' + community_name_key+'&uiVisible=true');
  var users = Restangular.all('user/');
  var user_setting = Restangular.all('user_setting/');
  var newsitems = null; //Restangular.one('news_item/?community=' + community_id + '&limit=10&order_by=-id');
  var FAQs = null; //Restangular.one('faq/?target=/scs/community/' + community_id+'/');  //"target": "/scs/community/1/"
  var dohide = true;

  $scope.userid = userid;
  $scope.show_join_community = false;//(usercommunity.id != community_id);

  /***FUNCTIONS***/
  /*Scope*/
//onServerMouseover($event)"  ng-mouseleave="onServerMouseout($event)
  $scope.onServerMouseover = function(event){
    $(event.target).css('overflow','visible');
  };
  $scope.onServerMouseout = function(event){
    $(event.target).css('overflow','hidden');
  };
  $scope.onMemberMouseover = function(event){
    //$(event.target).find('.community-info-main-inner-container-hover-section-member').show();
  };
  $scope.onMemberMouseout = function(event){
    //$(event.target).find('.community-info-main-inner-container-hover-section-member').hide();
  };
  $scope.onNewsItemMouseover = function(event){
    $(event.target).find('.news-item-actions-hover').show();
  };
  $scope.onNewsItemMouseout = function(event){
    hidemenu($(event.target).find('.news-item-actions-hover'));
  };
  $scope.onNewsItemMenuMouseout = function(event){
    dohide = true;
    hidemenu($(event.target).closest('.news-item-actions-hover'));
  };
  $scope.onNewsItemMenuMouseover = function(event){
    dohide = false;
  };
  $scope.deleteNewsItem = function (newsitem,event){
    Restangular.one("news_item/"+newsitem.id+'/').remove().then(
      function(){
        newsitemsSaved($(event.target));
        refreshNewsItems();
      },
      function(response) {
        alert("Fail to add the news item! \n\n"+response.data.error_message);
        console.log(response);
      });
    console.log($(event.target));
  };
  $scope.addNewsItem = function (newsitem,event){
    var currentdate = new Date(); 
    var datetime = currentdate.getFullYear() + "-" + (currentdate.getMonth()+1)  + "-" + currentdate.getDate()  + "T" + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds()+'Z';
    $scope.newsitems.unshift({
                "body": "",
                "community": "/scs/community/" + community_id +"/",
                "external_link": "",
                "headline": "",
                "id": -1,
                "owner": "/scs/user/" + userid + "/",
                "ownerid": userid,
                "release_date_time": datetime,
                "ownertitle": "",
                "ownerlastname": userlastname,
                "ownerfirstname": userfirstname
              });
    
    var t = setTimeout(function(){ editModeNewIetm ( $("ul[data-id='-1']").find('li').eq(0) );} ,100);
  };
  $scope.cancelNewsItem = function(newsitem, event){
    if(newsitem.id == -1){
      $scope.newsitems.shift();
    }else{
      newsitemsSaved($(event.target));
    }
  };
  $scope.editNewsItem = function(newsitem, event){
    editModeNewIetm( $(event.target) );
  };

  $scope.saveNewsItem = function(newsitem,event){
    if(newsitem.id == -1){
      Restangular.all("news_item/").post({
        headline: newsitem.headline,
        external_link: newsitem.external_link,
        body: newsitem.body,
        community: newsitem.community,
        owner: newsitem.owner,
        release_date_time: newsitem.release_date_time
      }).then( function() {
          newsitemsSaved($(event.target));
          refreshNewsItems();
        },
          function(response) {
          alert("Fail to add the news item! \n\n"+response.data.error_message);
          console.log(response);
      });
      return;
    }
    Restangular.one("news_item/"+newsitem.id+'/').patch( 
      {
        headline:newsitem.headline,
        external_link:newsitem.external_link,
        body:newsitem.body
      }).then(newsitemsSaved($(event.target)),
        function(response) {
          alert("Fail to save the news item modifications! \n\n"+response.data.error_message);
          console.log(response);
    });
  };
  $scope.linktoforum = function(){
    console.log('link  to forum');
  };
  $scope.movetosection = function(id){
    var position = $( "#"+id ).position();
    window.scrollTo( 0 ,( position.top - 50 ) );
  };

  $scope.deleteCommunity = function(){
    var modalInstance = $modal.open({
      controller: 'ConfirmCtrl',
      templateUrl: 'confirm/confirm.tpl.html',
      resolve: {
        title: function(){ return 'Confirm Community Deletion';},
        body: function(){ return 'Do you want to delete the ' + $scope.community.name + ' community? This cannot be undone.';}
      }
    });

    modalInstance.result.then(function(){
      Restangular.one('community',community_id+'/').remove().then(function(){
        $state.go('communities');
      });
    },function(){
        console.log('community deletion cancelled');
    });
  };

  $scope.editCommunity = function(){
    var communityCopy = angular.copy($scope.community);
    var modalInstance = $modal.open({
      controller: 'EditCommunityCtrl',
      templateUrl: 'communityinfo/communityinfo.edit.modal.tpl.html',
      size: 'lg',
      resolve:{
        community: function(){ return communityCopy; }
      }
    });

    modalInstance.result.then(function(){
      updateCommunityInfo($scope.community, communityCopy);
    }, function(){
      updateCommunityInfo($scope.community, communityCopy);
    });
  };

  $scope.assignResource = function(){
    var obj = {type:'resource', assigned:false};
    var modalInstance = $modal.open({
      controller: 'AssignToCommunityCtrl',
      templateUrl: 'communityinfo/assign.modal.tpl.html',
      resolve: {
        userOwnsCommunity: function(){ return $scope.userIsOwner;},
        object: function(){ return obj;},
        community: function(){ return $scope.community;}

      }
    });

    modalInstance.result.then(function(){
      if(obj.assigned){
        console.log('modal closed assigned');
        refreshResources();
        refreshTools();
      }
    },function(){
      if(obj.assigned){
        console.log('modal dismissed assigned');
        refreshResources();
        refreshTools();
      }
    });
  };

  $scope.assignTool = function(){
    var obj = {type:'task_type', assigned:false};
    var modalInstance = $modal.open({
      controller: 'AssignToCommunityCtrl',
      templateUrl: 'communityinfo/assign.modal.tpl.html',
      resolve: {
        userOwnsCommunity: function(){ return $scope.userIsOwner;},
        object: function(){ return obj;},
        community: function(){ return $scope.community;}
      }
    });

    modalInstance.result.then(function(){
      if(obj.assigned){
        console.log('modal closed assigned');
        refreshTools();
        refreshResources();
      }
    },function(){
      if(obj.assigned){
        console.log('modal dismissed assigned');
        refreshTools();
        refreshResources();
      }
    });
  };

  $scope.removeFromCommunity = function(type, object, index){
    var userOwnsObject = userid == object.owner.split('/')[3];
    var text, modalInstance;

    if(!$scope.userIsOwner && !userOwnsObject){
      text = type === 'task_type' ? 'tool' : 'resource';
      $modal.open({
        controller: 'AlertCtrl',
        templateUrl: 'alert/alert.tpl.html',
        resolve: {
          title: function(){ return 'Error';},
          message: function() { return 'Only the community owner or ' + text + ' owner can remove this ' + text + ' from the community.' ;}
        }
      });
      return;
    }
    else{
      modalInstance = $modal.open({
        controller: 'ConfirmCtrl',
        templateUrl: 'confirm/confirm.tpl.html',
        resolve: {
          title: function(){ return 'Confirm removal';},
          body: function(){ return 'Remove '+object.name + ' from this community?';}
        }
      });
      modalInstance.result.then(function(){
        Restangular.all(type+'_tag/?tag__tag_name=community:'+$scope.community.name_key+'&'+type+'__id='+object.id).remove().then(function(){
          if(type === 'task_type'){
            $scope.tooltasks.splice(index,1);
          }
          else{
            $scope.resources.splice(index,1);
          }
        },function(response){
          console.log('error deleting tag in remove from community');
        });
      },function(){
        console.log('removal cancelled');
      });
    }
    
  };

  $scope.joinCommunity = function(community){

    if(community.id != usercommunity.id){
      Restangular.one('user_setting', userid+'/').patch({community: community.resource_uri}).then(function(){
        SmUser.setProperty('community',community);
      });
    }
        
  };

  /*Standard*/
  var updateCommunityInfo = function(original,copy){
    if(original.name !== copy.name){
      original.name = copy.name;
    }
    if(original.imageUrl !== copy.imageUrl){
      original.imageUrl = copy.imageUrl;
    }
    if(original.shortDescription !== copy.shortDescription){
      original.shortDescription = copy.shortDescription;
    }
    if(original.description !== copy.description){
      original.description = copy.description;
    }
  };

  var refreshNewsItems = function(){
    newsitems.get().then(function(response){
      $scope.newsitems = response.objects;
      for(var i in $scope.newsitems){
        getOwnerInformation($scope.newsitems[i]);
      }
    });
  };

  var editModeNewIetm = function( $obj ){
    $obj.closest('ul').find('li').hide();
    $obj.closest('ul').find('li').eq(1).show();
    $obj.closest('ul').find('li').eq(4).show();
    $container = $obj.closest('div.community-info-main-inner-container-section-news-item');
    $container.find(".edit-mode").show();
    $container.find(".newsitem-view").hide();
  };
  var newsitemsSaved = function( $obj ){ 
      $obj.closest('ul').find('li').show();
      $obj.closest('ul').find('li').eq(1).hide();
      $obj.closest('ul').find('li').eq(4).hide();
      $container = $obj.closest('div.community-info-main-inner-container-section-news-item');
      $container.find(".edit-mode").hide();
      $container.find(".newsitem-view").show();
  };
  var hidemenu = function($obj){
    var t = setTimeout( function(){ if(dohide) {$obj.hide();}  },1000);
  };

  var getOwnerInformation = function($obj){
    var id=String($obj.owner).split('/')[3];
    $obj.ownerid = id;
    users.get(id+'/').then(function(response){ 
      if(response.last_name.trim() === "" && response.first_name.trim() === "") {
        $obj.visible = 0;
      }
      else{ 
        $obj.visible = 1;
      }

      $obj.ownerlastname = response.last_name;
      $obj.ownerfirstname = response.first_name;
      if(response.email.trim() === ''){
        $obj.emailvisible = 0;
      }else{
        $obj.emailvisible = 1;
        $obj.owneremail = response.email;
      }
    });

    user_setting.get(id+'/').then(function(response){ 
      $obj.ownertitle = response.title;
      $obj.owneravatar = response.imageUrl;
      $obj.ownerinfo = response.info;
    });
  };

  var getCommunityMemberCount = function($obj){
    var communityusers = Restangular.one('user_setting/?community__id=' + $obj.id + '&limit=1');
    communityusers.get().then(function(response){
      $obj.memberscount = response.meta.total_count ? response.meta.total_count : '0';
    });
  };

  var getCommunityResourceCount = function($obj){
    var communityresources = Restangular.one('resource/?communityFilter=' + $obj.name_key +'&limit=1');
    communityresources.get().then(function(response){
      $obj.resourcecount = response.meta.total_count ? response.meta.total_count : '0';
    });
  };

  var getCommunityToolCount = function($obj){
    var communitytasktypes = Restangular.one('task_type/?communityFilter=' + $obj.name_key + '&uiVisible=true&limit=1');
    communitytasktypes.get().then(function(response){
      $obj.toolcount = response.meta.total_count ? response.meta.total_count : '0';
    });
  };

  var getLastNewsItemHeadline = function($obj){
    communitynewsitem.get('?community=' + $obj.id + '&limit=1&order_by=-id').then(function(response){
      if(response.objects.length > 0 ){
        $obj.lastnewsitem = response.objects[0].headline;
      }
    });
  };

  var initializeCommunityInfo = function(){
    FAQs.get().then(function(response){
      $scope.FAQs = response.objects;
      for(var i in $scope.FAQs){
        getOwnerInformation($scope.FAQs[i]);
      }
    });

    tasktypes.get().then(function(response){
      $scope.community.toolcount = response.meta.total_count;
      $scope.tooltasks = response.objects;
      for(var i in $scope.tooltasks){
        getOwnerInformation($scope.tooltasks[i]);
      }
    });

    resources.get().then(function(response){
      $scope.community.resourcecount = response.meta.total_count;
      $scope.resources = response.objects;
      for(var i in $scope.resources){
        getOwnerInformation($scope.resources[i]);
      }
    });

    refreshNewsItems();

    members.get().then(function(response){
      $scope.community.memberscount = response.meta.total_count;
      $scope.members = response.objects;
      for(var i in $scope.members){
        getOwnerInformation($scope.members[i]);
      }
    });
  };

  var refreshResources = function(){
    Restangular.one('resource/?communityFilter=' + community_name_key).get().then(function(response){
      $scope.resources = response.objects;
      for(var i in $scope.resources){
        getOwnerInformation($scope.resources[i]);
      }
    });
  };

  var refreshTools = function(){
    Restangular.one('task_type/?communityFilter=' + community_name_key+'&uiVisible=true').get().then(function(response){
      $scope.tooltasks = response.objects;
      for(var i in $scope.tooltasks){
        getOwnerInformation($scope.tooltasks[i]);
      }
    });
  };
  
  /***EXE***/
  //$('.section-marker').click(function(){window.scrollTo( 0 , 0 );});
  $('.see-more').click(function(){
    var $lst = $(this).prev();
    var h = $lst.css('height');
    var x = $lst.attr('save-height');
    $lst.css('height', (x ? x : 'auto') );
    $lst.attr('save-height',h);
    var t = $(this).text();
    if(t.indexOf('See Full List') == -1 )
    {
      $(this).text('See Full List');
    }
    else
    {
      $(this).text('See Short List');
    }
  });

  $(".join-community-old").click(function(){
    var $this = $(this);
    var id = SmUser.getProperty('id');
    user_setting.get(id+'/').then(function(res){
      user_setting.post({
          "community": {
            "description": $scope.community.description,
            "gid": $scope.community.gid,
            "id": $scope.community.id,
            "imageUrl": $scope.community.imageUrl,
            "name": $scope.community.name,
            "name_key": $scope.community.name_key,
            "owner": $scope.community.owner,
            "resource_uri": $scope.community.resource_uri
          },
          "imageUrl": res.imageUrl,
          "info": res.info,
          "owner": ("/scs/user/"+id+"/"),
          "performSetupOnNextLogin": true,
          "resource_uri": res.resource_uri,
          "title": res.title
        }
      ).then(
        function() { $this.hide(); },
        function(response) { alert("Fail to join the community! \n\n"+response.data.error_message); }
      );
    });
  });

  Restangular.all('community/?name_key='+communityNameKey).get('').then(function(response){
    if(response.objects.length === 0){
      $state.go('communities');
      return;
    }

    var obj = response.objects[0];
    $scope.community = obj;
    $scope.userIsOwner = userid == $scope.community.owner.split('/')[3];
    getOwnerInformation($scope.community);
    //getCommunityResourceCount($scope.community);
    //getCommunityToolCount($scope.community);
    //getCommunityMemberCount($scope.community);
    community_id = obj.id; 
    community_name_key = obj.name_key;
    $scope.show_join_community = (usercommunity.id != community_id);
    members = Restangular.one('user_setting/?community__name_key=' + community_name_key);
    resources = Restangular.one('resource/?communityFilter=' + community_name_key);
    tasktypes = Restangular.one('task_type/?communityFilter=' + community_name_key+'&uiVisible=true');
    newsitems = Restangular.one('news_item/?community=' + community_id + '&limit=10&order_by=-release_date_time');
    FAQs = Restangular.one('faq/?target=/scs/community/' + community_id+'/');

    initializeCommunityInfo();

  });
 
}])
.controller('EditCommunityCtrl', ['$scope', '$modalInstance', 'Restangular', '$modal', 'community', '$timeout', function($scope, $modalInstance, Restangular, $modal, community, $timeout){
  /***STANDARD VARIABLES***/
  var communitySaved =false;

  /***SCOPE VARIABLES***/
  $scope.current = {};
  $scope.community = community;
  $scope.showMessage = false;

  /***SCOPE FUNCTIONS***/
  $scope.close = function(){
    $modalInstance.close({saved: communitySaved, object: $scope.community});
  };

  $scope.save = function(form){
    $scope.showMessage = false;
    if(form.$valid){
      var obj = {imageUrl: $scope.community.imageUrl, description: $scope.community.description, shortDescription: $scope.community.shortDescription, name: $scope.community.name};
      Restangular.one('community',$scope.community.id+'/').patch(obj).then(function(response){
        $scope.showMessage = true;
        communitySaved = true;
        $timeout(function(){
          $scope.showMessage = false;
        }, 5000);
      }, function(response){
        $modal.open({
          controller: 'AlertCtrl',
          templateUrl: 'alert/alert.tpl.html',
          resolve: {
            title: function(){ return 'Error';},
            message: function(){ return 'An error occurred while trying to update the community: '+ response.data.error;}
          }
        });

      });
    }
    else {
      form.$submitted = true;
    }
  };

}])
.controller('AssignToCommunityCtrl', ['$scope', '$modalInstance', 'Restangular', '$modal', 'object', 'SmUser','userOwnsCommunity','community','$timeout', function($scope, $modalInstance, Restangular, $modal, object, SmUser, userOwnsCommunity, community, $timeout){
  /***STANDARD VARIABLES***/
  var uid = SmUser.getProperty('id');
  var tag;
  /***SCOPE VARIABLES***/
  $scope.title = object.type == 'resource' ? 'Assign Resource' : 'Assign Tool';
  $scope.objects = [];
  $scope.type = object.type;
  $scope.selected = null;
  $scope.showMessage = false;
  $scope.showErrorMessage = false;

  /***SCOPE FUNCTIONS***/
  $scope.assign = function(){
    $scope.showMessage = false;
    var selectedOwnerId = $scope.selected.owner.split('/')[3];
    var text;
    if(userOwnsCommunity || uid == selectedOwnerId){
      Restangular.all(object.type+'_tag/?'+object.type+'__id='+$scope.selected.id+'&tag__tag_name=community:'+community.name_key).get('').then(function(response){
        if(response.meta.total_count === 0){
          var o = {tag: tag.resource_uri};
          o[object.type] = $scope.selected.resource_uri;
          Restangular.all(object.type+'_tag/').post(o).then(function(){
            $scope.showMessage = true;
            if($scope.objects[$scope.selected.id-1].id === $scope.selected.id){
              $scope.objects.splice($scope.selected.id-1,1);
            }
            $timeout(function(){
              $scope.showMessage = false;
            }, 5000);
            object.assigned = true;
          },function(){
            $modal.open({
              controller: 'AlertCtrl',
              templateUrl: 'alert/alert.tpl.html',
              resolve: {
                title: function(){ return 'Error';},
                message: function() { return 'An error occurred during assignation.';}
              }
            });
          });
        }
        else{
          $scope.showErrorMessage = true;
          $timeout(function(){
              $scope.showErrorMessage = false;
            }, 5000);
        }
      });
        
    }
    else{
      text = object.type === 'task_type' ? 'tool' : 'resource';
      $modal.open({
        controller: 'AlertCtrl',
        templateUrl: 'alert/alert.tpl.html',
        resolve: {
          title: function(){ return 'Error';},
          message: function() { return 'Only the community owner or ' + text + ' owner can complete assignation.';}
        }
      });
    }
  };

  $scope.close = function(){
    $modalInstance.close();
  };

  /***EXE***/
  Restangular.all('tag/?tag_name=community:'+community.name_key).get('').then(function(response){
    tag = response.objects[0];
  });

  Restangular.all(object.type+'/?limit=0&personalFolder=false').get('').then(function(response){
    $scope.objects = response.objects;
    if(object.type === 'resource'){
      Restangular.all('resource/?owner__id='+uid+'&personalFolder=true').get('').then(function(response){
        $scope.objects.unshift(response.objects[0],response.objects[1]);
      });
    }
  });

}]);

