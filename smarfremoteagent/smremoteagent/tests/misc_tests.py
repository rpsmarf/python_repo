'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Oct 15, 2014

    @author: rpsmarf
'''

import unittest
from smremoteagent.init.envvar import sraGetEnvVar, SRA_ENV_VAR_SCS_ICE_PORT,\
    sraGetEnvVarFloat, SRA_ENV_VAR_SCS_ICE_HOSTNAME, sraGetEnvVarInt
from smcommon.utils.net_utils import smGetFreePorts
from smremoteagent.init.sra_ice import SraIceIf
import os
import xmlrunner  # @UnresolvedImport


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testEnvVar(self):
        self.assertEqual(sraGetEnvVar(SRA_ENV_VAR_SCS_ICE_HOSTNAME), "127.0.0.1") 
        self.assertEqual(sraGetEnvVarInt(SRA_ENV_VAR_SCS_ICE_PORT), 8001) 
        self.assertEqual(sraGetEnvVarFloat(SRA_ENV_VAR_SCS_ICE_PORT), 8001.0) 
        os.environ["ABC"] = "123"
        self.assertEqual(sraGetEnvVar("ABC"), "123") 
        pass

    def testIceIf(self):
        ports = smGetFreePorts(2)
        scsPort = ports[0]
        myPort = ports[1]
        iceIf = SraIceIf("localhost", scsPort, myPort, 
                         "SmRemAgAsyncOpResps:tcp -h localhost -p ", None, "guid")
        self.assertIsNotNone(iceIf.getIceCommunicator())
        
        s = iceIf.makeProxy("z1234")
        self.assertIsNotNone(s)
        iceIf.sendHeartbeat("asd", 11)


if __name__ == '__main__':
    unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output='test-reports'),
        # these make sure that some options that are not applicable
        # remain hidden from the help menu.
        failfast=False, buffer=False, catchbreak=False)        
