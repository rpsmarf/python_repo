describe( 'taskcopydata module', function() {
	beforeEach (module('rpsmarf.config'));
	beforeEach(module('rpsmarf.constants'));
	beforeEach(module('rpsmarf.taskcopydata'));
	beforeEach(module('ui.router'));
	beforeEach(module('restangular'));
	beforeEach(module('rpsmarf.task.create.service'));
	beforeEach(module('rpsmarf.user.service'));
	beforeEach(module('taskprogress/taskprogress.tpl.html'));
	beforeEach(module('rpsmarf.taskservice'));
	beforeEach(module('taskcopydata/copyconfirm.tpl.html'));
	beforeEach(module('alert/alert.tpl.html'));

	var $scope, $controller, SmTaskCreate, $state, smarfurl, taskObject, $httpBackend, responseObj,copyTaskJsonPath= 'src/app/taskservice/taskservice.spec.json', jsonPrefix='src/app/resourceconfig/';
	var rt3s, resource_types, task_type, dataObj, resources, SmUser, USER_STATE, Restangular, SmTask, copyTaskInfo, copy_task, r1_entries, r2_entries, user, $modal, $rootScope;
	var rootFolderEntry;

	beforeEach(inject(function(_Restangular_, SMARF_URL){
		smarfurl = SMARF_URL+'/scs/';
		Restangular = _Restangular_;
		Restangular.configuration.baseUrl=smarfurl;
		Restangular.configuration.encodeIds = false;
	}));

	describe('functionality', function(){
		beforeEach(  inject(function(_$controller_, _$rootScope_, _$interval_, _$modal_, _SmTaskCreate_, _$state_, _$httpBackend_, _SmUser_, _USER_STATE_, _SmTask_, DEBUG, ROOT_FOLDER_ENTRY){
		
			$rootScope = _$rootScope_;
			SmTaskCreate = _SmTaskCreate_;
			SmUser = _SmUser_;
			user = SmUser.getObject();
			user.id = 2;
			user.community = {name_key: 'all_resources'};
			
			rootFolderEntry = ROOT_FOLDER_ENTRY;

			SmTask = _SmTask_;
			$httpBackend = _$httpBackend_;
			$scope = $rootScope.$new();
			$state = _$state_;
			$modal = _$modal_;
			copyTaskInfo = JSON.parse(__html__[copyTaskJsonPath]);
			copy_task = copyTaskInfo.copy_task;
			dataObj = JSON.parse(__html__[jsonPrefix+'resourceconfig.spec.json']);
			task_type = dataObj.task_type;
			rt3s = dataObj.rt3s;
			resource_types = dataObj.resource_types;
			r1_entries = dataObj.r1_entries;
			r2_entries = dataObj.r1_l1_entries;
			$controller = _$controller_;
			USER_STATE = _USER_STATE_;
					
			$httpBackend.expect('GET',smarfurl+'task_type/?name_key=copy').respond(200,copy_task);
			$httpBackend.expect('GET',smarfurl+'resource_type_task_type/?task_type__id='+copy_task.objects[0].id).respond(200,copyTaskInfo.rt3s);
			$httpBackend.flush();

			$httpBackend.expect('GET',smarfurl+'resource/?sortForUser='+user.id+'&extForUser='+user.id+'&communityFilter=all_resources&limit=0').respond(200,dataObj);
			
			$httpBackend.when('GET',smarfurl+'resource/1/list/').respond(200,r1_entries);
			$httpBackend.when('GET',smarfurl+'resource/2/list/').respond(200,r2_entries);
			//$httpBackend.expect('GET',smarfurl+'resource/1/l1/list/').respond(200,dataObj.r1_l1_entries);
			
			
			// $scope, Restangular, $modal, SmUser, $state, SmTask, SMARF_URL, SmContainerResource, filterFilter
			$controller('TaskCopyDataCtrl', {$scope:$scope, Restangular:Restangular, SmUser: SmUser, $state: $state, SmTaskCreate: SmTaskCreate, USER_STATE:USER_STATE, SmTask:SmTask});
			$httpBackend.flush();

		})) ;

		

		it('should display the resources for the two \'panes\'', inject([function(){
			
			//Select first resource twice (for first pane)
			$scope.selectResource($scope.resources[0][0],0,0);
			expect($scope.resources[0][0].selected).toBe(true);
			expect($scope.resourceSelected[0]).toEqual('true');
			$scope.selectResource($scope.resources[0][0],0,0);
			expect($scope.resources[0][0].selected).toBe(true);

			//Select second resource (for first pane)
			$scope.selectResource($scope.resources[0][1],0,1);
			expect($scope.resources[0][0].selected).toBe(false);
			expect($scope.resources[0][1].selected).toBe(true);

			//Select first resource (for second pane)
			$scope.selectResource($scope.resources[1][0],1,0);
			expect($scope.resources[0][0].selected).toBe(false);
			expect($scope.resources[1][0].selected).toBe(true);
			expect($scope.resourceSelected[1]).toEqual('true');
			
			


		}]));

		it('should filter root entry results based on the user input', inject([function(){
			
			//First pane actions
			$scope.selectResource($scope.resources[0][0],0,0);
			var o = dataObj.objects[0];
			o.smHashData =0, o.selected = true;
			
			expect($scope.selectedResource[0]).toEqual(o);
			$httpBackend.flush();
			r1_entries.unshift(rootFolderEntry);
			for(var i= r1_entries.length-1; i>=0; i--){
				r1_entries[i].smHashData = 0;
			}
			//angular.copy(r1_entries);
			expect(angular.copy($scope.selectedResource[0].entries)).toEqual(r1_entries);

			$scope.filterRootEntries('navdeep', 0);

			expect($scope.selectedResource[0].entries.length).toEqual(2);
			var m = r1_entries[6],n = r1_entries[7];
			m.smHashData=0,n.smHashData=0;
			expect($scope.selectedResource[0].entries).toEqual([m,n]);

			//Second pane actions
			$scope.selectResource($scope.resources[1][1],1,1);
			
			o = dataObj.objects[1];
			o.smHashData =1, o.selected = true;
			expect($scope.selectedResource[1]).toEqual(o);
			$httpBackend.flush();
			r2_entries.unshift(rootFolderEntry);
			for(i= r2_entries.length-1; i>=0; i--){
				r2_entries[i].smHashData = 1;
			}
			expect(angular.copy($scope.selectedResource[1].entries)).toEqual(angular.copy(r2_entries));

			$scope.filterRootEntries('tes', 1);
			
			expect($scope.selectedResource[1].entries.length).toEqual(1);

			o=r2_entries[3]; o.smHashData =1;
			expect($scope.selectedResource[1].entries).toEqual([o]);

		}]));

		it('should refresh resources', inject([function(){
			//Select second resource (for first pane)
			$scope.selectResource($scope.resources[0][1],0,1);
			expect($scope.resources[0][0].selected).toBeFalsy();
			expect($scope.resources[0][1].selected).toBe(true);

			expect($scope.resourceSelected[0]).toBe('true');
			expect($scope.selectedResource[0]).toBe($scope.resources[0][1]);

			$httpBackend.expect('GET',smarfurl+'resource/?sortForUser='+user.id+'&extForUser='+user.id+'&communityFilter=all_resources&limit=0').respond(200,dataObj);
			$scope.refreshResources(0);
			$httpBackend.flush();
			expect($scope.resourceSelected[0]).toBe('false');
			expect($scope.selectedResource[0]).toEqual({entries:null});


		}]));

		it('should start a copy task when the copy selection button is pressed or drop occurs', inject([function(){
			var tid = 20;
			var copy = SmTask.getTaskInfo('copy');
			copy.task_type_id = 2;
			copy.src = {id:5};
			copy.dest = {id:7};
			console.log(smarfurl);
			var taskPost = {owner:'/scs/user/'+SmUser.getProperty('id')+'/', task_type:'/scs/task_type/'+copy.task_type_id+'/', status: "init", name:"Copy hello to dest test/", parametersJson:'{ "input_ids":{"1": {"path": "hello"}, "2": {"path": "dest test/hello"}}}'};
			

			spyOn($scope, '$emit');
			spyOn($modal, 'open').andCallThrough();
			//Select second resource (for first pane)
			$scope.selectResource($scope.resources[0][1],0,1);
			$httpBackend.flush();
			expect($scope.resources[0][0].selected).toBeFalsy();
			expect($scope.resources[0][1].selected).toBe(true);
			$scope.resources[0][1].selectedEntry = {isDir: false, name:'hello', basename: 'hello'};
			$scope.selectedEntries[0] = {1:{isDir: false, name:'hello', basename: 'hello'}};
			//Select first resource (for second pane)
			$scope.selectResource($scope.resources[1][0],1,0);
			$httpBackend.flush();
			expect($scope.resources[0][0].selected).toBeFalsy();
			expect($scope.resources[1][0].selected).toBe(true);
			$scope.selectedEntries[1] = {3:{isDir: true, name:'dest test/', basename: 'dest test/'}};
			expect($scope.resourceSelected[1]).toEqual('true');


			//$httpBackend.expect('POST',smarfurl+'task/',taskPost, {"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200,{headers:function(val){ return '/scs/task/'+tid+'/'; }},{Location:'/scs/task/'+tid+'/'});
			
			//source resources
			//$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+$scope.resources[0][1].id+'/', 
			//	resource_type_task_type: '/scs/resource_type_task_type/'+copy.src.id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			
			//$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+$scope.resources[1][0].id+'/', 
			//	resource_type_task_type: '/scs/resource_type_task_type/'+copy.dest.id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			$httpBackend.expect('GET',smarfurl+'resource/1/'+encodeURIComponent('dest test')+'/copy/?srcResource=2&srcPath='+encodeURIComponent('hello')).respond(200, {taskPath: '/scs/task/3/'});


			//$scope.copy(1, {preventDefault: function(){},stopPropagation: function(){}});

			//expect($modal.open).toHaveBeenCalled();
			//$rootScope.$apply();
			//angular.element('.modal-footer .btn-primary').click();
			$scope.copy(0);

			expect($modal.open).toHaveBeenCalledWith({
				templateUrl: 'taskcopydata/copyconfirm.tpl.html',
				controller: 'CopyConfirmCtrl',
				resolve: jasmine.any(Object)

			});
			$rootScope.$apply();
			angular.element('.modal-footer .btn-primary').click();

			$httpBackend.flush();

			expect($scope.$emit).toHaveBeenCalledWith('SmTaskStart',jasmine.any(Object),'Copy 1 items to dest test/', false);

			$httpBackend.expect('POST',smarfurl+'task/',taskPost, {"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200,{headers:function(val){ return '/scs/task/'+tid+'/'; }},{Location:'/scs/task/'+tid+'/'});
			
			//source resources
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+$scope.resources[0][1].id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+copy.src.id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			
			$httpBackend.expect('POST',smarfurl+'task_resource/',{task:'/scs/task/'+tid+'/', resource: '/scs/resource/'+$scope.resources[1][0].id+'/', 
				resource_type_task_type: '/scs/resource_type_task_type/'+copy.dest.id+'/'},{"Content-Type":"application/json","Accept":"application/json, text/plain, */*"}).respond(200);

			$httpBackend.expect('GET',smarfurl+'task/'+tid+'/?action=start').respond(200);
			
			$scope.drop($scope.selectedEntries[0][1], $scope.resources[0][1], $scope.resources[1][0], $scope.selectedEntries[1][3]);

			expect($modal.open).toHaveBeenCalledWith({
				templateUrl: 'taskcopydata/copyconfirm.tpl.html',
				controller: 'CopyConfirmCtrl',
				resolve: jasmine.any(Object)

			});
			$rootScope.$apply();
			angular.element('.modal-footer .btn-primary').click();

			$httpBackend.flush();

			expect($scope.$emit).toHaveBeenCalled();


		}]));

		it('should perform the correct calls when a resource is favourited/unfavourited', inject([function(){
		
						
			//Restangular.all('favourite_task_type/').post({owner:uid, task_type: tooltask.id}).then(function(){
			$httpBackend.expect('POST', smarfurl+'favourite_resource/',{owner: '/scs/user/'+user.id+'/', resource: '/scs/resource/'+$scope.resources[0][0].id+'/'}).respond(200);
			$scope.favouriteToggle($scope.resources[0][0]);

			$httpBackend.flush();

			$httpBackend.expect('DELETE', smarfurl+'favourite_resource/?owner__id='+user.id+'&resource__id='+$scope.resources[0][0].id).respond(200);
			$scope.favouriteToggle($scope.resources[0][0]);

			$httpBackend.flush();
			


		}]));

		it('should open the notes editor with the right parameters', inject([function(){
			
			spyOn($modal, 'open');
			$scope.resources[0][0].ext_note = "my note";
			
			$scope.openNotesEditor($scope.resources[0][0]);

			var args = $modal.open.mostRecentCall.args;

			expect(args[0].controller).toBe('ExtendedInfoCtrl');
			expect(args[0].templateUrl).toBe('extendedinfo/extendedinfo.tpl.html');
			var tagsObj = args[0].resolve.tagsObject();
			var noteObj = args[0].resolve.noteObject();

			expect(tagsObj).toBe(null);
			expect(noteObj).toEqual({typeName: 'resource',typeId: $scope.resources[0][0].id, ownerId: user.id, exists: true, noteText: $scope.resources[0][0].ext_note});
			
		}]));

		it('should send the proper values to the accessrequest controller', inject(function(){
			
			spyOn($modal,'open');
			$httpBackend.expect('GET',smarfurl+'resource/1/getperm/').respond(200, { 
						"/scs/user/1/": [ "add_smmodelresource", 
										"change_smmodelresource", 
										"delete_smmodelresource", "read_resource_data", 
										"execute_on_resource", 
										"write_resource_data" 
									], 

						"/scs/user/2/": [ "read_resource_data" ] 
			});
			$scope.requestAccess($scope.resources[0][0]);
			$httpBackend.flush();

			expect($modal.open.mostRecentCall.args[0].resolve.object().userPerm).toEqual([{name:'read', hasPerm: true},{name:'write', hasPerm: false},{name:'execute', hasPerm: false}]);

		}));

	});
	
});

