describe('SmInterval service', function(){

	var $interval, SmInterval;
	beforeEach(module('rpsmarf.interval.service'));

	beforeEach(inject(['$interval','SmInterval',function(_$interval_, _SmInterval_){
		$interval = _$interval_;
		SmInterval = _SmInterval_;
	}]));

	it('should add and stop intervals ', inject([function(){

		var count =0;
		var inc = function(){
			count++;
		};

		SmInterval.addInterval($interval(inc, 100));
		$interval.flush(100);
		expect(count).toEqual(1);
		expect(SmInterval.getNumberOfIntervals()).toEqual(1);
		SmInterval.addInterval($interval(inc, 100));
		expect(SmInterval.getNumberOfIntervals()).toEqual(2);
		$interval.flush(100);
		expect(count).toEqual(3);

		SmInterval.stopAllIntervals();
		$interval.flush(100);

		expect(count).toEqual(3);


	}]));




});