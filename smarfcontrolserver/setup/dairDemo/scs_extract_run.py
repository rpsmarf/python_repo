import sys
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetToJson
import time

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

print("Getting extractor with repo name weather_repo ...")

resp = scsGetToJson("/scs/md_extractor/?repo__name_key=weather_repo")
extractor_path = resp["objects"][0]["resource_uri"]
repo_id = resp["objects"][0]["repo"].split("/")[3]

print("Using extractor path=" + extractor_path)
scsGetToJson(extractor_path + "rebuild/")
print("Extraction started...")

while True:
    resp = scsGetToJson(extractor_path)
    state = resp["state"]
    if state == "done":
        print("Extraction is done")
        print("stdout: " + resp["stdout"])
        print("stderr: " + resp["stderr"])
        break
    else:
        print("State=" + state + " progressJson=" + resp["progressJson"])
        time.sleep(1.0)
        
print("Done")


resp_loaded = scsGetToJson("/scs/md_path/?repo=" + repo_id + "&state=loaded&limit=1")
resp_created = scsGetToJson("/scs/md_path/?repo=" + repo_id + "&state=created&limit=1")

print("Paths in loaded state={}, paths in created state={}".format(
                                                                   resp_loaded["meta"]["total_count"],
                                                                   resp_created["meta"]["total_count"]))

resp_props = scsGetToJson("/scs/md_property/?path__repo=" + repo_id + "&limit=1")
print("Properties in repo {} = {}".format(repo_id, resp_props["meta"]["total_count"]))
