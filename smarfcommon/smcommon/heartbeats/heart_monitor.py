'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import logging
import threading
import time

logger = logging.getLogger(__name__)


class SmHeartNotification(object):
    
    def heartStopped(self, heartId):
        raise NotImplemented()
    
    def heartStarted(self, heartId):
        raise NotImplemented()
        
    def unknownHeart(self, heartId):
        raise NotImplemented()

HEART_STATE_STOPPED = "stopped"
HEART_STATE_WORKING = "working"
HEART_STATE_ADDED = "added"


class SmHeartStatus(object):
    
    def __init__(self, heartId):
        self.lastBeatTime = 0
        self.lastBeatCount = -1
        self.lastRestartCheckBeatCount = -1
        self.beatCountAtLastCheck = -1
        self.heartId = heartId
        self.heartState = HEART_STATE_ADDED
        self.addTime = time.time()
    
    def beatReceived(self, count):
        self.lastBeatTime = time.time()
        self.lastBeatCount = count
        
    def isHeartStopped(self, now, minHeartBeatInterval):
        if (now - self.lastBeatTime) > minHeartBeatInterval:
            self.lastRestartCheckBeatCount = -1
            return True
        else:
            return False
        
    def hasRestarted(self):
        if (self.lastRestartCheckBeatCount > self.lastBeatCount):
            self.lastRestartCheckBeatCount = self.lastBeatCount
            return True

        self.lastRestartCheckBeatCount = self.lastBeatCount
        return False
 
       
class SmHeartMonitor(threading.Thread):
    '''
    This class receives heatbreat events from SMARF remote agents
    and raises an alarm if heartbeats are not received in a timely 
    manner.
    '''

    def __init__(self, minHeartBeatInterval, notif, minTimeAfterAddBeforeAlarm, pollInterval):
        '''
        Constructor
        @param minHeartBeatInterval: float indicating the minimum time between heartbeats before
        declaring that a beating heart has stopped
        @param notif: the object (derived from SmHeartNotification) to callback on heart started, stopped
        or unknown heart events
        @param minTimeAfterAddBeforeAlarm: this is a float with the minimum time after the heart is
        added before we declare an alarm.  This is used to provide additional time during initialization
        to allow remote devices to connect  
        '''
        threading.Thread.__init__(self, name="heartMonitor")
        self.heartList = {}
        self.minHeartBeatInterval = minHeartBeatInterval
        self.minTimeAfterAddBeforeAlarm = minTimeAfterAddBeforeAlarm
        self.notif = notif
        self.pollInterval = pollInterval
        self.daemon = True
        self.quit = False
        
    def heartbeat(self, heartId, beatCount):
        # Find heart or create it
        if (heartId not in self.heartList):
            logger.warning("No record found for agent id " + heartId)
            self.notif.unknownHeart(heartId)
            return -1
        
        logger.debug("Got heartbeat for heart " + heartId)
        heart = self.heartList[heartId]
        heart.beatReceived(beatCount)
        return 0
    
    def getHeartStatus(self, heartId):
        return self.heartList[heartId]
    
    def addHeart(self, heartId):
        logger.info("Added info for agent id " + heartId)
        self.heartList[heartId] = SmHeartStatus(heartId)
        
    def deleteHeart(self, heartId):
        logger.info("Deleted info for agent id " + heartId)
        del self.heartList[heartId]

    def runOnePass(self):
        now = time.time()
        localHeartList = []
        for heartStatus in self.heartList.values():
            localHeartList.append(heartStatus)
        
        for heartStatus in localHeartList:
            if (heartStatus.heartState == HEART_STATE_STOPPED):
                if not heartStatus.isHeartStopped(now, self.minHeartBeatInterval):
                    heartStatus.heartState = HEART_STATE_WORKING
                    logger.warning("Heart with id " + heartStatus.heartId + " started")
                    self.notif.heartStarted(heartStatus.heartId)
            elif (heartStatus.heartState == HEART_STATE_WORKING):
                if heartStatus.isHeartStopped(now, self.minHeartBeatInterval):
                    logger.warning("Heart with id " + heartStatus.heartId + " stopped")
                    heartStatus.heartState = HEART_STATE_STOPPED
                    self.notif.heartStopped(heartStatus.heartId)
                elif heartStatus.hasRestarted():
                    logger.warning("Heart with id " + heartStatus.heartId + " restarted")
                    self.notif.heartStopped(heartStatus.heartId)
                    self.notif.heartStarted(heartStatus.heartId)
            elif (heartStatus.heartState == HEART_STATE_ADDED):
                if not heartStatus.isHeartStopped(now, self.minHeartBeatInterval):
                    heartStatus.heartState = HEART_STATE_WORKING
                    logger.warning("Heart with id %s started", heartStatus.heartId)
                    self.notif.heartStarted(heartStatus.heartId)
                elif (now - heartStatus.addTime) > self.minTimeAfterAddBeforeAlarm:
                    logger.warning("Heart with id " + heartStatus.heartId + " stopped after add. " +
                                   "Add time is " + str(heartStatus.addTime) +
                                   " and threadhold is " + str(self.minTimeAfterAddBeforeAlarm))
                    heartStatus.heartState = HEART_STATE_STOPPED
                    self.notif.heartStopped(heartStatus.heartId)
            else:
                logger.error("Unknown heart state: " + heartStatus.heartState + 
                             " for heartStatus with id " + heartStatus.heartId)

    def run(self):
        ''' 
        Checking for stopped hearts
        '''
        while (not self.quit):
            time.sleep(self.pollInterval)
            try:
                self.runOnePass()
            except:
                logger.exception("Exception during heart monitor", exc_info=True)
                
    def stopMonitor(self):
        self.quit = True
        self.join(self.pollInterval * 2)