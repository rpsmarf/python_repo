'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''

import unittest
from smcommon.utils import net_utils
from smremoteagent.init.sra_ice import SraIceIf
from smcommon.tests.ice_tests import TestAsyncResps
import xmlrunner  # @UnresolvedImport


class Test(unittest.TestCase):

    def testName(self):
        # Start the ICE subsystem
        port = net_utils.smGetFreePort()
        testResp = TestAsyncResps()
        sraIceIf = SraIceIf("localhost", port, port, 
                            "SmRemAgAsyncOpResps:default -p " + str(port),
                            testResp, "LOCAL_AGENT")  # @UnusedVariable
        
        sraIceIf.ice.destroy()


if __name__ == '__main__':
    unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output='test-reports'),
        # these make sure that some options that are not applicable
        # remain hidden from the help menu.
        failfast=False, buffer=False, catchbreak=False)
