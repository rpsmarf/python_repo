import json
import sys
import time
from scripts.scs_task_start import scsTaskStartByPath
from scripts.scs_obj_common import scsScriptGetDefaultUrl
sys.path.append("/opt/smarf-scs/scs")
from scripts.scs_obj_get import scsGetObjectPath
from scripts.scs_obj_create import scsCreateObjectPath

print("Current SCS URL is " + scsScriptGetDefaultUrl() + ".  Do export SCS_URL=http://demo.rpsmarf.ca to change (for example)")

taskParams = {
              "input_ids": {
                            "1": {"path": "dc120_guitar.jpg"},
                            "2": {"path": "netgateOutput-" + time.strftime("%H%M%S")}
                            }
              }

tt_path = scsGetObjectPath("task_type", "name_key=jpeg_to_ascii")
root_path = scsGetObjectPath("user", "name=rpsmarf")
task_path = scsCreateObjectPath("task", {"name": "JPEG to ASCII", 
                                         "task_type": tt_path,
                                         "owner": root_path,
                                         "parametersJson": json.dumps(taskParams),
                                         }
                                )

resource_type_task_type_data_dest_path = scsGetObjectPath("resource_type_task_type", "name_key=output_folder")
resource_type_task_type_data_src_path = scsGetObjectPath("resource_type_task_type", "name_key=image_src")
resource_type_task_type_data_compute_path = scsGetObjectPath("resource_type_task_type", "name_key=ec2_server")


src_resource_path = scsGetObjectPath("resource", "name_key=jpeg_photo_repo")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": src_resource_path,
                                      "resource_type_task_type": resource_type_task_type_data_src_path,
                                      }
                    )

dest_resource_path = scsGetObjectPath("resource", "name_key=private_cloud_a")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": dest_resource_path,
                                      "resource_type_task_type": resource_type_task_type_data_dest_path,
                                      }
                    )

compute_resource_path = scsGetObjectPath("resource", "name_key=dyn_aws_server")
scsCreateObjectPath("task_resource", {"task": task_path,
                                      "resource": compute_resource_path,
                                      "resource_type_task_type": resource_type_task_type_data_compute_path,
                                      }
                    )

print("Task " + task_path + " to do photo negation creating, starting task...")

scsTaskStartByPath(task_path, verbose=True)

