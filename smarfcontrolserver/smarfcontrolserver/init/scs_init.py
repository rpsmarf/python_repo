'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 9, 2014

    @author: rpsmarf
'''

import logging  # @UnusedImport
import time
import threading
import importlib

scsIceInterface = None


def scsInitNonDjangoCodeInNewThread():
    '''
    We have to do a module lookup to avoid doing imports of Django
    code before Django initialization
    '''
    print("scsInitNonDjangoCodeInNewThread was called")
    logger = logging.getLogger(__name__)
    logger.info("scsInitNonDjangoCodeInNewThread was called")
    time.sleep(1)
    logger.info("scsInitNonDjangoCodeInNewThread delay done...")
    # Find module, find function and invoke
    module = importlib.import_module("smarfcontrolserver.init.scs_init_after_django")
    scsInitAfterDjango_func = getattr(module, "scsInitAfterDjango")
    scsInitAfterDjango_func()
    print("scsInitNonDjangoCodeInNewThread DONE")


def scsInitNonDjangoCode():
    global scsIceInterface
    print("In scsInitNonDjangoCode, starting thread")
    threading.Thread(target=scsInitNonDjangoCodeInNewThread).start()
    print("Done scsInitNonDjangoCode")
    