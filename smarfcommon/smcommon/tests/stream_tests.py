'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Oct 27, 2014

    @author: rpsmarf
'''

import logging
import tempfile
import time
import unittest

from smcommon.logging.sm_logger import smLoggingSetupForTest
from smcommon.tests.dad_test_utils import TempFolder
from smcommon.tests.stream_tester import SmStreamTestSource, SmStreamTestSink


logger = logging.getLogger(__name__)


class StreamTest(unittest.TestCase):

    def setUp(self):
        smLoggingSetupForTest()
        self.tempFolder = TempFolder()
        self.fifoName = self.tempFolder.makeFifoDad().getAbsPath()

        # Create named pipe
        logging.debug("Created fifo %s", self.fifoName)
        self.streamSource = None
        self.streamSink = None
        self.sourceProgressCallbackCount = 0
        self.sourceLastOuputSize = 0
        self.sinkProgressCallbackCount = 0
        self.sinkLastOuputSize = 0

    def tearDown(self):
        if self.streamSink:
            self.streamSink.stopConsumer(1.0)
            
        if self.streamSource:
            self.streamSource.stopGenerator(1.0)
            
        self.tempFolder.cleanup()

    def waitUntilDone(self, testObj):
        maxPasses = 20
        count = 0
        while True:
            if testObj.isDone():
                return
            count += 1
            if count > maxPasses:
                raise Exception("Timeout waiting for stream test object to complete")
            time.sleep(0.1)
    
    def sinkDone(self, obj, status):
        logger.debug("Sink done invoked")
        self._sinkDone = True
    
    def sourceDone(self, obj, status):
        logger.debug("Source done invoked")
        self._sourceDone = True
    
    def sourceProgressCallback(self, obj, prog_d):
        logger.debug("sourceProgressCallback with prog %s", str(prog_d))
        self.sourceProgressCallbackCount += 1
        self.sourceLastOuputSize = prog_d["outputSize"]
        
    def sinkProgressCallback(self, obj, prog_d):
        logger.debug("sinkProgressCallback with prog %s", str(prog_d))
        self.sinkProgressCallbackCount += 1
        self.sinkLastOuputSize = prog_d["outputSize"]
    
    def testStreamTester(self):
        # Setup sink
        self.streamSink = SmStreamTestSink(self.fifoName, 30)
        self.streamSink.setCallbacks(self.sinkProgressCallback, self.sinkDone)
        self._sourceDone = False
        self._sinkDone = False
        
        # Setup source
        self.streamSource = SmStreamTestSource(self.fifoName, 5, 0.03, 6)
        self.streamSource.setCallbacks(self.sourceProgressCallback, self.sourceDone)
        self.streamSource.startGenerator()
        self.streamSink.startConsumer()
        
        self.waitUntilDone(self.streamSource)
        self.waitUntilDone(self.streamSink)
        
        self.assertTrue(self.streamSink.isDone())
        self.assertTrue(self.streamSource.isDone())
        self.assertFalse(self.streamSink.errorSeen())
        self.assertEqual(self.streamSource.getBytesTxed(), 30)
        self.assertEqual(self.streamSink.getBytesRxed(), 30)
        self.assertTrue(self._sourceDone)
        self.assertTrue(self._sinkDone)
        self.assertEqual(6, self.sourceProgressCallbackCount)
        self.assertEqual(30, self.sourceLastOuputSize)
        self.assertTrue(self.sinkProgressCallbackCount > 0)
        self.assertEqual(30, self.sinkLastOuputSize)

        # Test again with string pattern
        # Setup sink
        self.fifoName = self.fifoName + "2"
        self.streamSink = SmStreamTestSink(self.fifoName, 24)
        self.streamSink.setExpectedPatternString(b"Big")
        
        # Setup source
        self.streamSource = SmStreamTestSource(self.fifoName, 2, 0.03, 4)
        self.streamSource.setPatternString(b'Big')
        
        self.streamSource.startGenerator()
        self.streamSink.startConsumer()
        
        self.waitUntilDone(self.streamSource)
        self.waitUntilDone(self.streamSink)
        
        self.assertTrue(self.streamSink.isDone())
        self.assertTrue(self.streamSource.isDone())
        self.assertFalse(self.streamSink.errorSeen())
        self.assertEqual(self.streamSource.getBytesTxed(), 24)
        self.assertEqual(self.streamSink.getBytesRxed(), 24)

        # Test again with file pattern
        _, patternFile = tempfile.mkstemp()
        with open(patternFile, "wb") as text_file:
            text_file.write(b"0123456789")

        # Setup sink
        destFile = self.tempFolder.makeLocalDad("dest").getAbsPath()
        self.fifoName = self.fifoName + "2"
        self.streamSink = SmStreamTestSink(self.fifoName, 20)
        self.streamSink.setDestFile(destFile)
        
        # Setup source
        self.streamSource = SmStreamTestSource(self.fifoName, 5, 0.03, 4)
        self.streamSource.setPatternFileRepeating(patternFile)
        
        self.streamSource.startGenerator()
        self.streamSink.startConsumer()
        
        self.waitUntilDone(self.streamSource)
        self.waitUntilDone(self.streamSink)
        
        self.assertTrue(self.streamSink.isDone())
        self.assertTrue(self.streamSource.isDone())
        self.assertFalse(self.streamSink.errorSeen())
        self.assertEqual(self.streamSource.getBytesTxed(), 20)
        self.assertEqual(self.streamSink.getBytesRxed(), 20)
        destData = self.tempFolder.readFile("dest", 1000)
        self.assertEqual(destData, b'01234567890123456789')
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()