#!/usr/bin/env python 
'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import os
import sys
sys.path.insert(1, "/opt/smarf-scs/scs")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "smarfcontrolserver.settings")

from sm_controller.models import SmModelResource, SmModelContainer
from sm_controller.models import SmModelResourceType
from sm_controller.models import SmModelTaskType
from sm_controller.models import SmModelResourceTypeTaskType
from scripts.scsWipeAndInit import resource_types
from scripts.scsWipeAndInit import task_types
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist


def create_resources(num_create, rtype, user, container):
    s = ''
    for i in resource_types:
        s += i + '|'
    s = s[:-1] 
    
    if(num_create <= 0):
        print("Cannot create 0 Resources. Exiting")
        return False
    elif(user is None):
        print("User id must be a positive integer. Exiting")
        return False
    elif rtype not in resource_types:
        print('ERROR: Could not identify your resource type. ' +
              'Must be one of ' +
              s.replace('|', ','))
        return False
    
    num_resources = SmModelResource.objects.count()
    containers = SmModelContainer.objects.all() 
    c = containers.iterator().__next__()
    
    num_resources_start = num_resources
    resource_type_obj = SmModelResourceType.objects.filter(name__icontains=rtype).iterator().__next__()
    print('Creating resources and assigning them to task types.')
    for num_resources in range(num_resources, num_resources + num_create):    
        n = str(num_resources + 1)    
        SmModelResource.objects.create(name='resource' + n, public=True, resource_type=resource_type_obj, 
                                       description='A ' + rtype + ' type resource', owner=user, 
                                       status='ok',
                                       parametersJson='{"folder": ""}',
                                       container=c)

        for i in task_types:
            if rtype in i:
                task_type_obj = SmModelTaskType.objects.filter(name__icontains=i).iterator().__next__()
                SmModelResourceTypeTaskType.objects.create(task_type=task_type_obj, 
                                                           resource_type=resource_type_obj, role="r")
                
    print('Created ' + str(int(n) - num_resources_start) + ' ' + rtype + ' resources for ' + user.username)
    print('There are now ' + n + ' total resources in the database')

if __name__ == "__main__":
    argv = sys.argv
    s = ''
    for i in resource_types:
        s += i + '|'
    s = s[:-1] 
    if len(argv) == 4:
        if not argv[1] in resource_types:
            print('ERROR: Could not identify your resource type. ' +
                  'Must be one of ' +
                  s.replace('|', ','))
            sys.exit()
        elif not argv[2].isdigit():
            print('ERROR: Please enter a positive integer for number to create.')
            sys.exit()
        elif not argv[3].isdigit():
            print('ERROR: Please enter a positive integer for the user\'s id.')
            sys.exit()  
        try:
            u = User.objects.get(id=int(argv[3]))
        except ObjectDoesNotExist: 
            print('ERROR: User with id ' + argv[3] + ' does not exist!')
            sys.exit() 
        rtype = argv[1]    
        num_create = argv[2]

    else:
        print('Usage: scsCreateResource (' + s + ') num_to_create owner_id')
        sys.exit()
        
    container = list(SmModelContainer.objects.all())[0]   
    create_resources(int(num_create), rtype, u, container)
