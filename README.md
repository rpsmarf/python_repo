# README #

This repo contains the main source code for the components of the 
Research Platform for Smart Facilities Management (RPSMARF).

### What is in this repository? ###

* This repo contains source code for the main two components of the RPSMARF platform, namely:
** The SMARF Control Server - which runs in a single instance to coordinate the use of the system
** The SMARF Remote Agent - which runs behind the firewall at one or more remote sites

### How do I get set up? ###

* Summary of set up  
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions


### Who do I talk to? ###

* See andrewmcgregor@sce.carleton.ca or melendez@sce.carleton.ca