'''
    deleteright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a delete of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Oct 1, 2014

    @author: rpsmarf
'''
import logging
import threading
from smcommon.globals.sm_global_context import smGlobalContextGet
from smcommon.exceptions.sm_exc import SmException
from smcommon.exceptions.exec_exception import SmExecutionException
from smcommon.exceptions.file_access_exception import SmFileAccessException

logger = logging.getLogger(__name__)
SM_FILES_TO_DELETE_PER_PASS = 100
THREAD_COUNT = 1 # Used to generate unique names for each delete thread


class SmDeleterThread(threading.Thread):
    
    def __init__(self, iceCopier):
        global THREAD_COUNT
        threading.Thread.__init__(self)
        self.name = "deleteThread-" + str(THREAD_COUNT)
        THREAD_COUNT += 1 
        self.iceCopier = iceCopier
        
    def run(self):
        self.iceCopier._deleteFileOrFolderInThread()


class SmDeleter(object):
    '''
    classdocs
    '''

    def __init__(self, srcDad, doneCallback):
        '''
        Constructor
        '''
        self._done = False
        self.started = False
        self.totalFilesToDelete = 0
        self.filesDeleted = 0
        self._progressCallback = None
        self.doneCallback = doneCallback
        self.srcDad = srcDad
        self.quit = False
        self.state = "unstarted"
         
    def start(self):
        self.thread = SmDeleterThread(self) 
        self.thread.start()
        self.state = "running"

    def setProgressCallback(self, callback):
        '''
        Registers an object to report progress to.
        The callback is of the form:
        <object>.progressCallback(<copier>, <progress>)
        where <progress> is a real number from 0.0 to 100.0
        '''
        self._progressCallback = callback
        
    def _calcProgress(self):
        if (self.totalFilesToDelete == 0):
            return 0
        return (self.filesDeleted * 100.0) / self.totalFilesToDelete

    def _addProgress(self, filesDeleted):
        self.filesDeleted += filesDeleted
        if (self._progressCallback is not None):
            progress = self._calcProgress()
            self._progressCallback(self, {"progress": progress})
            
    def getStatus(self):
        '''
        '''
        return {"progress": (self._calcProgress()),
                "done": self._done,
                "state": self.state,
                }

    def cancel(self):
        self.quit = True
    
    def isDone(self):
        return self._done
    
    def _deleteDone(self, newState, result):
        logger.info("Setting state to %s", newState)
        self.state = newState
        logger.info("Setting done to %s", True)
        self._done = True
        logger.info("Set done to True")
        if self.doneCallback is not None:
            logger.debug("Invoking doneCallback with newState %s and result %s", newState, result)
            self.doneCallback(self, newState, result)
            
    def __str__(self):
        return self.__class__.__name__

    def _deleteFileOrFolderInThread(self):
        copyCon = smGlobalContextGet().copyController
        srcFileAcc = copyCon.makeFileAccessor(self.srcDad.dataContainer)
        srcFileAcc.connectSession()
        
        try:
            if self.srcDad.isFile():
                self.totalFilesToDelete = 1
                srcFileAcc.deletePaths([self.srcDad.path])
                self._addProgress(1)                
            else:  # Folder delete
                # List all files in folder
                flist = srcFileAcc.list(self.srcDad.path + "/", 1000000, 1000000, None, True)
                self.totalFilesToDelete = len(flist)
                revflist = reversed(flist)
                tempList = []
                numInList = 0
                for f in revflist:
                    tempList.append(f["name"])
                    numInList += 1
                    if (numInList >= SM_FILES_TO_DELETE_PER_PASS):
                        logger.debug("Deleting: " + str(tempList))
                        srcFileAcc.deletePaths(tempList)
                        self._addProgress(numInList)
                        numInList = 0
                        tempList = []
                        
                if numInList > 0:
                    logger.debug("Deleting: " + str(tempList))
                    srcFileAcc.deletePaths(tempList)
                    self._addProgress(numInList)
                        
            self._deleteDone("success", {})
        except Exception as e:
            e2 = SmExecutionException.makeException(e)
            self._deleteDone("failed", e2.toDict())
            logger.error("Failure in delete of %s", str(self.srcDad), exc_info=True)
            if isinstance(e, SmFileAccessException):
                raise e
            else:
                raise SmException(422, "EOTHER", "unknown", "Failure in delete of " + str(self.srcDad))
        finally:
            logger.debug("Done delete, disconnecting fileAccessor")
            srcFileAcc.disconnectSession()
            