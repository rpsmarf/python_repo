'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import logging
from smcommon.globals.sm_global_context import smGlobalContextGet
from smcommon.task_runners.task_runner import SmTaskRunnerBase

logger = logging.getLogger(__name__)


class SmTaskRunnerDelete(SmTaskRunnerBase):
    '''
    This implements a "delete task" which can be run via the 
    task framework.
    '''

    def __init__(self, taskId, doneCallback, taskDesc):
        '''
        Constructor
        '''
        SmTaskRunnerBase.__init__(self, taskId, doneCallback, taskDesc)
        srcDad = taskDesc.getArgDad(taskDesc.getArgDict(0))
        self.deleter = smGlobalContextGet().copyController.makeDeleter(srcDad, self.deleteDone)
        self.deleter.setProgressCallback(self.progressCallback)
        logger.debug("SmTaskRunnerDelete initialized with src = %s", srcDad)
        
    def start(self):
        self.setState("running")
        self.deleter.start()
    
    def cancel(self):
        self.deleter.cancel()

    def progressCallback(self, copier, progress):
        logger.debug("Delete task progress for %s to %s", self.taskId, progress)
        self.announceProgress(progress)
        
    def deleteDone(self, deleter, state, result):
        logger.debug("Delete operation done, signalling taskDone")
        self.done(state, result)
        
