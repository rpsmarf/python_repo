angular.module( 'rpsmarf.task.create.service', [
 'rpsmarf.config',
 'rpsmarf.user.service',
 'restangular'
 
])
.service('SmTaskCreate', [ 'DEBUG', 'Restangular', 'SmUser', '$q', 'USER_STATE',
 function(DEBUG, Restangular, SmUser, $q, USER_STATE){

	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var task_type = null;
	var inputs = {valueParameters:[], resourceParameters:[], computeResource: {} };
	var inIdToValParamsIndexMap = {};
	var inIdToResParamsIndexMap = {};
	var rolesToResParamsIndexMap = {};
	var interactive = false;
	var activeResourceParameterId = null;
	var requiresComputeResource = false;
	var taskId = null;
	var reservation = null;
	var version;
	//User supplied name for the task
	var taskName = '';

	var setTaskType = function(tt, reserv){
		reset();
		task_type = tt;	
		decomposeTaskType();
		if(reserv){
			reservation = true;	
		}
		else{
			reservation = false;
		}

		SmUser.setProperty('state', USER_STATE.task_start);
		
		return configureResourceParams();
		
		
	};

	var getTaskType = function(tt){

		return task_type;
	};

	var decomposeTaskType = function(){

		var config = JSON.parse (task_type.configurationJson);
		interactive = config.interactive || false;
		var args = config.args, arg, d;
		for( var i= args.length-1; i>=0; i--){
			arg = args[i];
			if(arg.type === "string" && arg.input_id){

				inputs.valueParameters.push(arg);
				inIdToValParamsIndexMap[arg.input_id] = inputs.valueParameters.length-1;
			}
			else if(arg.type === "resource"){
				arg.task_type_id = task_type.id;
				if(arg.direction && arg.direction === 'output'){
					d = new Date();
					d = d.toISOString();
					d = d.substring(0,d.length-5)+'Z';
					d = d.replace(/-|:/g, '');
					arg.resource = {path: d, touched: false};
				}
				inputs.resourceParameters.push(arg);
				inIdToResParamsIndexMap[arg.input_id] = inputs.resourceParameters.length-1;
				rolesToResParamsIndexMap[arg.role] = inputs.resourceParameters.length-1;
			}
		}

		if(rolesToResParamsIndexMap[config.computeResourceRole] === undefined){
			inputs.computeResource ={name:"Compute Resource", role: config.computeResourceRole, input_id:0, isCompute: true};
			requiresComputeResource = true;
		}

	};

	var configureResourceParams = function(){

		var resourceTypePromises = [];
		return Restangular.one('resource_type_task_type/?task_type__id='+task_type.id).get().then(function(response){
			var objects = response.objects;
			var rt3;
			var resParam;
			var index;
			var p;
			for(var i=objects.length-1; i>=0; i--){
				rt3 = objects[i];
				if(inputs.computeResource.role && rt3.role === inputs.computeResource.role){
					resParam = inputs.computeResource;
				}
				else {
					index = rolesToResParamsIndexMap[rt3.role];
					resParam = inputs.resourceParameters[index];
				}
				
				resParam.rt3_id = rt3.id;
				resParam.resource_type = {};

				resParam.resource_type.id = parseInt(rt3.resource_type.split('/')[3], 10);
				p = getResourceType(resParam);
				resourceTypePromises.push(p);
			}

			return $q.all(resourceTypePromises);
		});

	};

	var getResourceType = function(resourceParam){

		return Restangular.one('resource_type/'+resourceParam.resource_type.id+'/').get().then(function(rt){
			resourceParam.resource_type.name = rt.name;
			resourceParam.resource_type.description = rt.description;
			resourceParam.resource_type.nature = rt.nature;
			resourceParam.resource_type.name_key = rt.name_key;
			if(rt.nature === 'compute'){
				return Restangular.one('resource/?resource_type__id='+rt.id).get().then(function(resources){
					if(resources.objects.length > 0 && !resources.objects[0].locked){
						resourceParam.resource = resources.objects[0];
						//resourceParam.noSelect = true;
					}
				});
			}
			
		});

	};

	var getInputs = function(){
		return inputs;
	};

	var setInput = function(type, value, input_id){

		if(type !== 'compute' && (!input_id || input_id <=0 || !angular.element.isNumeric(input_id))){
			throw "Incorect input_id given when setting input value. input_id value give was: "+input_id;
		}
		if(typeof input_id === 'string'){
			input_id = parseInt(input_id, 10);
		}
		switch(type){
			case 'string':
				inputs.valueParameters[inIdToValParamsIndexMap[input_id]].value = value;
				break;
			case 'resource':
				inputs.resourceParameters[inIdToResParamsIndexMap[input_id]].resource = value;
				break;
			case 'compute':
				inputs.computeResource.resource = value;
				break;
			default:
				throw "Unkown type for setInput given";
		}
	};

	var createAndStartTask = function(startTime, endTime){
		//First create task_resources
		var inp, r, postObj, task_resource_obj, promise, taskResourcePromises = [];
		var task_resource = Restangular.all('task_resource/');
		var task = Restangular.all('task/');
		var uid = SmUser.getProperty('id');
		var ttid = task_type.id;
		var parametersJson = createParametersJson();
		var resParams = inputs.resourceParameters;
		var location;
		Restangular.configuration.fullResponse = true;
		postObj = {owner:'/scs/user/'+uid+'/', task_type:'/scs/task_type/'+ttid+'/', status: "init", parametersJson: parametersJson, name: taskName};
		if(startTime && endTime){
			postObj['start_time'] = startTime;
			postObj['end_time'] = endTime;
		}
		return task.post(postObj,null,{"Content-Type":"application/json"})
			.then(function(response){
				location = getLocation(response.headers('Location'));
				Restangular.configuration.fullResponse = false;
				taskId = location.pathname.split('/')[3];
				//Create task_resource for compute resource
				if(inputs.computeResource.resource){
					inp = inputs.computeResource;
					r = inp.resource;
					promise = task_resource.post({task:'/scs/task/'+taskId+'/', resource: '/scs/resource/'+r.id+'/', resource_type_task_type: '/scs/resource_type_task_type/'+inp.rt3_id+'/'},null,{"Content-Type":"application/json"});
					taskResourcePromises.push(promise);
				}

				//Create task_resource for resourceParameters
				for(var i=resParams.length-1; i>=0; i--){
					inp = resParams[i];
					r = inp.resource;
					promise = task_resource.post({task:'/scs/task/'+taskId+'/', resource: '/scs/resource/'+r.id+'/', resource_type_task_type: '/scs/resource_type_task_type/'+inp.rt3_id+'/'},null,{"Content-Type":"application/json"});
					taskResourcePromises.push(promise);
				}

				return createTaskObjects(task,taskResourcePromises, taskId, true);


			},function(fail){});
		
		
		

	};

	var createTask = function(){
		//First create task_resources
		var inp, r, task_resource_obj, promise, taskResourcePromises = [];
		var task_resource = Restangular.all('task_resource/');
		var task = Restangular.all('task/');
		var uid = SmUser.getProperty('id');
		var ttid = task_type.id;
		var resParams = inputs.resourceParameters;
		var location;
		Restangular.configuration.fullResponse = true;
		return task.post({owner:'/scs/user/'+uid+'/', task_type:'/scs/task_type/'+ttid+'/', status: "init", 
			parametersJson: '{}', name: taskName},null,{"Content-Type":"application/json"})
			.then(function(response){
				location = getLocation(response.headers('Location'));
				Restangular.configuration.fullResponse = false;
				taskId = location.pathname.split('/')[3];
				//Create task_resource for compute resource
				if(inputs.computeResource.resource){
					inp = inputs.computeResource;
					r = inp.resource;
					promise = task_resource.post({task:'/scs/task/'+taskId+'/', resource: '/scs/resource/'+r.id+'/', resource_type_task_type: '/scs/resource_type_task_type/'+inp.rt3_id+'/'},null,{"Content-Type":"application/json"});
					taskResourcePromises.push(promise);
				}

				//Create task_resource for resourceParameters
				for(var i=resParams.length-1; i>=0; i--){
					inp = resParams[i];
					r = inp.resource;
					promise = task_resource.post({task:'/scs/task/'+taskId+'/', resource: '/scs/resource/'+r.id+'/', resource_type_task_type: '/scs/resource_type_task_type/'+inp.rt3_id+'/'},null,{"Content-Type":"application/json"});
					taskResourcePromises.push(promise);
				}

				return createTaskObjects(task,taskResourcePromises, taskId, false);


			},function(fail){});
		

	};

	//parametersJson:'{ "input_ids":{"1": {"path": "'+entryObj.name+'"}, "2": {"path": "'+(dstResource.path+entryObj.basename).substring(1)+'"}}}'
	var createParametersJson = function(){
		var obj = {input_ids: {}};
		var input = null;
		var i;
		//valueParameters
		for(i=inputs.valueParameters.length-1; i>=0; i--){
			input = inputs.valueParameters[i];
			obj.input_ids[input.input_id] = {value: input.value};
		}

		//resourceParameters
		for(i=inputs.resourceParameters.length-1; i>=0; i--){
			input = inputs.resourceParameters[i];
			obj.input_ids[input.input_id] = {path: input.resource.path};	
		}

		//additional parameters i.e version
		if(version){
			obj.version = version;	
		}
		

		return JSON.stringify(obj);
	};

	var createTaskObjects = function(taskApi, promises, taskId, start ){
		var modalInstance, title;
		return $q.all(promises).then(function(){
			if(start){
				return taskApi.get(taskId+'/?action=start').then(function(){
					return {taskId:taskId,interactive:interactive};
				}, function(response){
					response.data.taskInfo = {taskId:taskId, interactive: interactive};
					return $q.reject(response);
				});
			}else{
				return {taskId:taskId,interactive:interactive};
			}
		});
	};

	var reset = function(){
		task_type = null;
		inputs = {valueParameters:[], resourceParameters:[], computeResource: {} };
		inIdToValParamsIndexMap = {};
		inIdToResParamsIndexMap = {};
		rolesToResParamsIndexMap = {};
		interactive = false;
		activeResourceParameterId = null;
		requiresComputeResource = false;
		taskId=null;
		SmUser.setProperty('state',USER_STATE.browse);
		taskName = '';
		version = null;

	};

	var setActiveResourceParameterId = function(input_id){
		activeResourceParameterId = input_id;
	};



	var setValueForActiveResourceParameter = function(value){
		var param;
		var temp, tpath;
		if(activeResourceParameterId === null){
			throw 'ActiveResourceParameterId must be set before setting its value.';
		}
		else if(activeResourceParameterId === 0){
			inputs.computeResource.resource = value;
			inputs.computeResource.error = false;
		}
		else {
			param = inputs.resourceParameters[inIdToResParamsIndexMap[activeResourceParameterId]];
			if(param.direction === 'output' && !param.resource.touched){
				temp  = param.resource.path;
				param.resource = value;
				tpath = param.resource.path;
				param.resource.path = tpath.substring(tpath.length-1) === '/' ? tpath + temp : tpath === '' ? temp : tpath + '/' + temp;
				param.resource.touched = true;
			}else if(param.direction === 'output' && param.resource.touched){
				param.resource = value;
				param.resource.touched = true;
			}
			else{
				param.resource = value;
			}
			param.error = false;
			
		}
	};

	var getActiveResourceParameter = function(){
		if(activeResourceParameterId === null){
			return null;
		}
		else if(activeResourceParameterId === 0){
			return inputs.computeResource;
		}
		else{
			return inputs.resourceParameters[inIdToResParamsIndexMap[activeResourceParameterId]];
		}
	};

	var getRequiresComputeResource = function(){
		return requiresComputeResource;
	};

	var getLocation = function(href) {
		var location = document.createElement("a");
		location.href = href;
		// IE doesn't populate all link properties when setting .href with a relative URL,
		// however .href will return an absolute URL which then can be used on itself
		// to populate these additional fields.
		if (location.host === "") {
			location.href = location.href;
		}
		return location;
	};

	var getTaskObjectId = function(){
		return taskId;
	};

	var getTaskInteractive = function(){
		return interactive;
	};

	var rerunTask = function(taskAggregate, name){
		
		
		var inp,i, r, task_resource_obj, promise, taskResourcePromises = [], pj = JSON.parse(taskAggregate.parametersJson);
		var task_resourceApi = Restangular.all('task_resource/');
		var taskApi = Restangular.all('task/');
		var uid = SmUser.getProperty('id');
		var ttid = taskAggregate.task_type.split('/')[3];
		var parametersJson = JSON.stringify({input_ids: pj.input_ids});
		var resParams = taskAggregate.resParams;
		var location;
		var now = new Date();
		var newTaskName = !name || name === '' ?  pj.task_type_name + '_' + months[now.getMonth()].substring(0,3) + '_' + now.getDate() + '_' + now.getFullYear() : name;

		//set global
		interactive = pj.interactive; 
		
		Restangular.configuration.fullResponse = true;
		return taskApi.post({owner:'/scs/user/'+uid+'/', task_type:'/scs/task_type/'+ttid+'/', status: "init", parametersJson: parametersJson, name: newTaskName},null,{"Content-Type":"application/json"})
			.then(function(response){
				location = getLocation(response.headers('Location'));
				Restangular.configuration.fullResponse = false;
				taskId = location.pathname.split('/')[3];
				

				//Create task_resource for resourceParameters
				for(i=resParams.length-1; i>=0; i--){
					r = resParams[i];
					promise = task_resourceApi.post({task:'scs/task/'+taskId+'/', resource: r.resourcePath, resource_type_task_type: r.resource_type_task_typePath},null,{"Content-Type":"application/json"});
					taskResourcePromises.push(promise);
				}

				return createTaskObjects(taskApi,taskResourcePromises, taskId, true);


			});
	};

	var reconfigureTask = function(task){
		var ttid = task.task_type.split('/')[3];
		var i,p, promises=[];
		return Restangular.one('task_type',ttid+'/').get().then(function(response){
			return setTaskType(response).then(function(){
				if(task.valParams){
					for(i = task.valParams.length-1; i>=0; i--){
						p = task.valParams[i];
						//Should have input_id
						if(p.input_id){
							inputs.valueParameters[inIdToValParamsIndexMap[p.input_id]] = p.value;
						}
					}
				}

				if(task.resParams){
					for(i = task.resParams.length-1; i>=0; i--){
						p = task.resParams[i];
						if(p.input_id){
							promises.push(createPromise(p,inputs.resourceParameters[inIdToResParamsIndexMap[p.input_id]]));
						}
						if(p.nature === 'compute'){
							promises.push(createPromise(p,inputs.computeResource));
						}
					}
				}
				return $q.all(promises);
			});

			
		});


	};

	var decomposeAndRerunTask = function(task, name){
		if(!task.paramsJson){
			task.paramsJson = JSON.parse(task.parametersJson);
		}
		var valParams = task.paramsJson.parameters;
		var resParams = task.paramsJson.resources;
		var inputIds = task.paramsJson.input_ids;
		var vp, rp, p, i;

		if(valParams && valParams.length >0){
			vp = [];
			for(i=0;i<valParams.length;i++){
				p = valParams[i];
				vp.push(p);
			}

			task.valParams = vp;
		}
		
		if(resParams && resParams.length >0){
			rp = [];
			for(i=0;i<resParams.length;i++){
				p = resParams[i];
				if(!p.name || p.name === '' && p.nature === 'compute'){
					p.name = 'Compute';
					p.resourcePathEntry = '--';
				}
				else if(p.path){
					p.resourcePathEntry = p.path;
				}
				rp.push(p);
			}

			task.resParams = rp;
		}

		return rerunTask(task, name);

	};

	var createPromise = function(p, resParam){

		return Restangular.one(p.resourcePath.substring(5),'').get().then(function(resource){
			resParam.resource = resource;
			if(p.path){
				resParam.resource.path = p.path;	
			}
		});
	};

	var isCurrentTaskReservation = function(){
		return reservation;
	};

	var restartTask = function(tid,inter){
		var taskApi = Restangular.all('task/');
		return taskApi.get(tid+'/?action=start').then(function(){
			return {taskId:tid,interactive:inter};
		});
	};

	var restartTaskWithQueueing = function(tid,inter){

		var taskApi = Restangular.all('task/');
		return taskApi.get(tid+'/?action=start&allowQueueing=True').then(function(){
			return {taskId:tid,interactive:inter};
		});
	};

	var setTaskName = function(name){
		taskName = name;
	};

	var setVersionForTaskType = function(v){
		version = v;
	};

	return {
			setTaskType: setTaskType,
			getTaskType: getTaskType,
			getInputs: getInputs,
			setInput: setInput,
			createAndStartTask: createAndStartTask,
			reset: reset,
			setActiveResourceParameterId: setActiveResourceParameterId,
			setValueForActiveResourceParameter: setValueForActiveResourceParameter,
			getActiveResourceParameter: getActiveResourceParameter,
			getRequiresComputeResource: getRequiresComputeResource,
			getTaskObjectId: getTaskObjectId,
			getTaskInteractive: getTaskInteractive,
			rerunTask: rerunTask,
			reconfigureTask: reconfigureTask,
			decomposeAndRerunTask: decomposeAndRerunTask,
			isCurrentTaskReservation: isCurrentTaskReservation,
			restartTask: restartTask,
			restartTaskWithQueueing: restartTaskWithQueueing,
			setTaskName: setTaskName,
			setVersionForTaskType:setVersionForTaskType
			};
}])
;


